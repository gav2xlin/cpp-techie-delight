/*

Given an integer array between 0 and 9, generate a pair using all the array digits that has the maximum sum. The difference in the number of digits of the two numbers should be ± 1.

Input : [4, 6, 2, 7, 9, 8]
Output: (974, 862)

Input : [9, 2, 5, 6, 0, 4]
Output: (952, 640)

The solution can return pair in any order. If a pair cannot be formed, the solution should return the pair (0, 0).

*/

#include <iostream>
#include <vector>
#include <utility>
#include <functional>

using namespace std;

class Solution
{
public:
    pair<int,int> constructMaxSumNumber(vector<int> &nums)
    {
        if (nums.size() <= 1) return {0, 0};

        //sort(nums.begin(), nums.end(), greater<int>{});
        sort(nums.rbegin(), nums.rend());

        int x = 0, y = 0;
        for (int i = 0; i < nums.size(); i += 2) {
            x = x * 10 + nums[i];
        }

        for (int i = 1; i < nums.size(); i += 2) {
            y = y * 10 + nums[i];
        }

        return {x, y};
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    {
        vector<int> nums = {4, 6, 2, 7, 9, 8};
        cout << Solution().constructMaxSumNumber(nums) << endl;
    }

    {
        vector<int> nums = {9, 2, 5, 6, 0, 4};
        cout << Solution().constructMaxSumNumber(nums) << endl;
    }

    return 0;
}
