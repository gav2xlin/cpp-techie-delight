/*

Given a set of activities, along with the starting and finishing time of each activity, find the maximum number of activities performed by a single person assuming that a person can only work on a single activity at a time.

Input : [(1, 4), (3, 5), (0, 6), (5, 7), (3, 8), (5, 9), (6, 10), (8, 11), (8, 12), (2, 13), (12, 14)]
Output: {(1, 4), (5, 7), (8, 11), (12, 14)}

Input : [(3, 7), (1, 3), (2, 9), (2, 7), (1, 2), (7, 8)]
Output: {(1, 3), (3, 7), (7, 8)} or {(1, 2), (3, 7), (7, 8)} or {(1, 2), (2, 7), (7, 8)}

*/

#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Solution
{
public:
    set<pair<int,int>> selectActivity(vector<pair<int,int>> activities)
    {
        set<pair<int,int>> res;

        sort(activities.begin(), activities.end(), [](const auto& l, const auto& r) {
            return l.second < r.second;
        });

        if (!activities.empty()) {
            res.insert(activities[0]);
        }

        int k = 0;
        for (int i = 1; i < activities.size(); ++i) {
            if (activities[i].first >= activities[k].second) {
                res.insert(activities[i]);
                k = i;
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const set<pair<int,int>>& pairs) {
    for (auto& p : pairs) {
        os << p.first << '-' << p.second << ' ';
    }
    return os;
}

int main()
{
    {
        vector<pair<int,int>> activities{{1, 4}, {3, 5}, {0, 6}, {5, 7}, {3, 8}, {5, 9}, {6, 10}, {8, 11}, {8, 12}, {2, 13}, {12, 14}};
        cout << Solution().selectActivity(activities) << endl;
    }

    {
        vector<pair<int,int>> activities{{3, 7}, {1, 3}, {2, 9}, {2, 7}, {1, 2}, {7, 8}};
        cout << Solution().selectActivity(activities) << endl;
    }

    return 0;
}
