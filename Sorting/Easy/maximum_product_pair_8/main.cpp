/*

Given an integer array, find a pair with the maximum product in it.

Each input can have multiple solutions. The output should match with either one of them.

Input : [-10, -3, 5, 6, -2]
Output: (-10, -3) or (-3, -10) or (5, 6) or (6, 5)

Input : [-4, 3, 2, 7, -5]
Output: (3, 7) or (7, 3)

If no pair exists, the solution should return the pair (-1, -1).

Input : [1]
Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <utility>
#include <limits>

using namespace std;

class Solution
{
public:
    /*pair<int,int> findPair(vector<int> const &nums)
    {
        if (nums.size() >=2) {
            int min1 = nums[0], min2 = numeric_limits<int>::max();
            int max1 = nums[0], max2 = numeric_limits<int>::min();

            for (int i = 1; i < nums.size(); ++i) {
                if (nums[i] > max1) {
                    max2 = max1;
                    max1 = nums[i];
                } else if (nums[i] > max2) {
                    max2 = nums[i];
                }

                if (nums[i] < min1) {
                    min2 = min1;
                    min1 = nums[i];
                } else if (nums[i] < min2) {
                    min2 = nums[i];
                }
            }

            return (min1 * min2 < max1 * max2) ? make_pair(max1, max2) : make_pair(min1, min2);
        } else {
            return {-1, -1};
        }
    }*/
    pair<int,int> findPair(vector<int> const &nums)
    {
        int n = nums.size();
        if (n >=2) {
            vector<int> _nums(nums);
            sort(_nums.begin(), _nums.end());

            return (_nums[0] * _nums[1] < _nums[n - 2] * _nums[n - 1]) ? make_pair(_nums[n - 2], _nums[n - 1]) : make_pair(_nums[0], _nums[1]);
        } else {
            return {-1, -1};
        }
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findPair({-10, -3, 5, 6, -2}) << endl;
    cout << Solution().findPair({-4, 3, 2, 7, -5}) << endl;

    return 0;
}
