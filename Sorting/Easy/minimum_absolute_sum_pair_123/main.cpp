/*

Given a sorted integer array, find a pair in it having an absolute minimum sum.

Input : [-6, -5, -3, 0, 2, 4, 9]
Output: (-5, 4)
Explanation: (-5, 4) = abs(-5 + 4) = abs(-1) = 1, which is minimum among all pairs.

• Each input can have multiple solutions. The output should match with either one of them.

Input : [-6, -2, 0, 1, 5]
Output: (-6, 5) or (-2, 1) or (0, 1)

• The solution can return pair in any order. If no pair exists, the solution should return the pair (-1, -1).

Input : [1]
Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <utility>
#include <limits>
#include <cstdlib>

using namespace std;

class Solution
{
public:
    pair<int,int> findPair(vector<int> const &nums)
    {
        if (nums.size() > 1) {
            int min_sum = numeric_limits<int>::max();
            int low = 0, high = nums.size() - 1, i = low, j = high;

            while (low < high) {
                int abs_sum = abs(nums[low] + nums[high]);

                if (abs_sum < min_sum) {
                    min_sum = abs_sum;

                    i = low;
                    j = high;
                }

                if (min_sum == 0) break;

                if (abs(nums[low] + nums[high - 1]) <= abs(nums[low + 1] + nums[high])) {
                    --high;
                } else {
                    ++low;
                }
            }

            return {nums[i], nums[j]};
        } else {
            return {-1, -1};
        }
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findPair({-6, -5, -3, 0, 2, 4, 9}) << endl;
    cout << Solution().findPair({-6, -2, 0, 1, 5}) << endl;
    cout << Solution().findPair({1}) << endl;

    cout << Solution().findPair({1, 1, 3, 3, 5}) << endl;

    return 0;
}
