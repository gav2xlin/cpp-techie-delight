/*

Given a collection of `n` items, each of which has a non-negative integer key whose maximum value is at most `k`, effectively sort it using the counting sort algorithm.

Input : nums[] = [4, 2, 10, 10, 1, 4, 2, 1, 10], k = 10
Output: [1, 1, 2, 2, 4, 4, 10, 10, 10]

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    void sort(vector<int> &nums, int k)
    {
        int max_elm = *max_element(nums.begin(), nums.end());
        vector<int> count(max_elm + 1);

        for (int i = 0; i < nums.size(); ++i) {
            ++count[nums[i]];
        }

        for (int i = 1; i <= max_elm; ++i) {
            count[i] += count[i - 1];
        }

        vector<int> out(nums.size());
        for (int i = 0; i < nums.size(); ++i) {
            out[--count[nums[i]]] = nums[i];
        }

        nums.swap(out);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{4, 2, 10, 10, 1, 4, 2, 1, 10};
    Solution().sort(nums, 10);
    cout << nums << endl;

    return 0;
}
