/*

Given an integer array with many duplicated elements, efficiently sort it in linear time.

Input : [4, 2, 40, 10, 10, 1, 4, 2, 1, 10, 40]
Output: [1, 1, 2, 2, 4, 4, 10, 10, 10, 40, 40]

Constraints:

• The input elements lie in the range [0, 1000].
• The relative order of equal elements doesn't matter.
• The sorting should be done in-place.

*/

#include <iostream>
#include <vector>

using namespace std;

#define RANGE 1000

class Solution
{
public:
    void sort(vector<int> &nums)
    {
        vector<int> freq(RANGE);

        for (int i = 0; i < nums.size(); ++i) {
            ++freq[nums[i]];
        }

        int k = 0;
        for (int i = 0; i < RANGE; ++i)
        {
            while (freq[i]--) {
                nums[k++] = i;
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int>nums{4, 2, 40, 10, 10, 1, 4, 2, 1, 10, 40};

    cout << "before: " << nums << endl;

    Solution().sort(nums);

    cout << "after: " << nums << endl;

    return 0;
}
