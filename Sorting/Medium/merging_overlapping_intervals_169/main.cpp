/*

Given a collection of intervals, return all non-overlapping intervals after merging the overlapping intervals.

Input : [(1, 5), (2, 3), (4, 6), (7, 8), (8, 10), (12, 15)]
Output: {(1, 6), (7, 10), (12, 15)}

*/

#include <iostream>
#include <vector>
#include <set>
#include <utility>
#include <stack>

using namespace std;

class Solution
{
public:
    using Interval = pair<int,int>;

    set<pair<int,int>> mergeIntervals(vector<Interval> intervals)
    {
        sort(intervals.begin(), intervals.end(), [](auto& i1, auto& i2) {
            return i1.first < i2.first;
        });

        stack<Interval> s;

        for (const Interval &curr: intervals)
        {
            if (s.empty() || curr.first > s.top().second) {
                s.push(curr);
            }

            if (s.top().second < curr.second) {
                s.top().second = curr.second;
            }
        }

        set<pair<int,int>> res;
        while (!s.empty())
        {
            res.insert({s.top().first, s.top().second});
            s.pop();
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

ostream& operator<<(ostream& os, const set<pair<int,int>>& pairs) {
    for (auto& p : pairs) {
        os << p << endl;
    }
    return os;
}

int main()
{
    cout << Solution().mergeIntervals({{1, 5}, {2, 3}, {4, 6}, {7, 8}, {8, 10}, {12, 15}}) << endl;

    return 0;
}
