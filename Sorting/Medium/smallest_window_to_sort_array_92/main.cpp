/*

Given an integer array, find the smallest window sorting which will make the entire array sorted in increasing order. The solution should return a pair consisting of the starting and ending index of the smallest window.

Input : [1, 2, 3, 7, 5, 6, 4, 8]
Output: (3, 6)
Explanation: The array can be sorted from index 3 to 6 to get sorted version.

Input : [1, 3, 2, 7, 5, 6, 4, 8]
Output: (1, 6)
Explanation: The array can be sorted from index 1 to 6 to get sorted version.

If the array is already sorted, the solution should return pair (0, 0).

Input : [1, 2, 3, 4, 5]
Output: (0, 0)
Explanation: The array is already sorted.

*/

#include <iostream>
#include <vector>
#include <utility>
#include <climits>

using namespace std;

class Solution
{
public:
    pair<int,int> findSmallestWindow(vector<int> const &nums)
    {
        int leftIndex = -1, rightIndex = -1;

        int max_so_far = INT_MIN;
        for (int i = 0; i < nums.size(); ++i)
        {
            if (max_so_far < nums[i]) {
                max_so_far = nums[i];
            }

            if (nums[i] < max_so_far) {
                rightIndex = i;
            }
        }

        int min_so_far = INT_MAX;
        for (int i = nums.size() - 1; i >= 0; --i)
        {
            if (min_so_far > nums[i]) {
                min_so_far = nums[i];
            }

            if (nums[i] > min_so_far) {
                leftIndex = i;
            }
        }

        if (leftIndex == -1) {
            return {0, 0};
        }

        return {leftIndex, rightIndex};
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    Solution s;

    cout << s.findSmallestWindow({1, 2, 3, 7, 5, 6, 4, 8}) << endl;
    cout << s.findSmallestWindow({1, 3, 2, 7, 5, 6, 4, 8}) << endl;
    cout << s.findSmallestWindow({1, 2, 3, 4, 5}) << endl;

    return 0;
}
