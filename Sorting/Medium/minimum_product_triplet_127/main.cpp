/*

Given an integer array, find the minimum product among all combinations of triplets in the array.

Input : [4, -1, 3, 5, 9]
Output: -45
Explanation: The minimum product triplet is (-1, 5, 9)

Input : [1, 4, 10, -2, 4]
Output: -80
Explanation: The minimum product triplet is (10, 4, -2)

Input : [3, 4, 1, 2, 5]
Output: 6
Explanation: The minimum product triplet is (3, 1, 2)

Input : [1, 2]
Output: 0
Explanation: Invalid input

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMin(vector<int> const &_nums)
    {
        int n = _nums.size();
        if (n <= 2) {
            return 0;
        }

        vector<int> nums(_nums);
        sort(nums.begin(), nums.end());

        return min(nums[n-1] * nums[n-2] * nums[0], nums[0] * nums[1] * nums[2]);
    }
};

int main()
{
    cout << Solution().findMin({4, -1, 3, 5, 9}) << endl;
    cout << Solution().findMin({1, 4, 10, -2, 4}) << endl;
    cout << Solution().findMin({3, 4, 1, 2, 5}) << endl;
    cout << Solution().findMin({1, 2}) << endl;

    return 0;
}
