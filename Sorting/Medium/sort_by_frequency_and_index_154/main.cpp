/*

Given an integer array, in-place sort its element by their frequency and index. If two elements have different frequencies, then the one which has more frequency should come first; otherwise, the one which has less index should come first, i.e., the solution should preserve the relative order of the equal frequency elements.

Input : [3, 3, 1, 1, 1, 8, 3, 6, 8, 7, 8]
Output: [3, 3, 3, 1, 1, 1, 8, 8, 8, 6, 7]

*/

#include <iostream>
#include <vector>
#include <tuple>
#include <unordered_map>
#include <utility>
#include <algorithm>

using namespace std;

bool value_comparer(tuple<int, int, int> const& lhs, tuple<int, int, int> const& rhs)
{
    if (get<1>(lhs) != get<1>(rhs)) {
        return get<1>(lhs) > get<1>(rhs);
    } else {
        return get<2>(lhs) < get<2>(rhs);
    }
}

class Solution
{
public:
    void sortByFrequencyAndIndex(vector<int> &nums)
    {
        int n = nums.size();
        if (n < 2) {
            return;
        }

        unordered_map<int, pair<int, int>> hm;

        for (int i = 0; i < n; ++i)
        {
            if (hm.find(nums[i]) != hm.end()) {
                ++hm[nums[i]].first;
            } else {
                hm[nums[i]] = make_pair(1, i);
            }
        }

        vector<tuple<int, int, int>> tuples;
        for (auto it: hm)
        {
            pair<int, int> p = it.second;
            tuples.push_back(make_tuple(it.first, p.first, p.second));
        }

        sort(tuples.begin(), tuples.end(), value_comparer);

        int a, b, c, k = 0;
        for (auto tup: tuples)
        {
            tie(a, b, c) = tup;
            for (int j = 0; j < b; j++) {
                nums[k++] = a;
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{3, 3, 1, 1, 1, 8, 3, 6, 8, 7, 8};
    cout << "before: " << nums << endl;

    Solution().sortByFrequencyAndIndex(nums);

    cout << "after: " << nums << endl;

    return 0;
}
