/*

Given an unsorted integer array, find a triplet with a given sum `k` in it.

Input : [4, -1, 1, 2, -1], k = 0
Output: [-1, 2, -1]

Input : [4, 5, 4, -1, 3], k = 10
Output: []
Explanation: No triplet exists with sum 10.

If the input contains several triplets with sum `k`, the solution can return any one of them.

Input : nums[] = [2, 7, 4, 0, 9, 5, 1, 3], k = 6
Output: [0, 1, 5] or [0, 2, 4] or [1, 2, 3]

Note: The solution can return the triplet in any order, not necessarily as they appear in the array.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    vector<int> findTriplets(vector<int> const &_nums, int target)
    {
        vector<int> nums(_nums);
        sort(nums.begin(), nums.end());

        int n = nums.size();
        for (int i = 0; i <= n - 3; ++i)
        {
            int k = target - nums[i];

            int low = i + 1, high = n - 1;

            while (low < high)
            {
                if (nums[low] + nums[high] < k) {
                    ++low;
                }else if (nums[low] + nums[high] > k) {
                    --high;
                } else {
                    return {nums[i], nums[low], nums[high]};
                    //++low, --high;
                }
            }
        }

        return {};
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findTriplets({4, -1, 1, 2, -1}, 0) << endl;
    cout << Solution().findTriplets({4, 5, 4, -1, 3}, 10) << endl;
    cout << Solution().findTriplets({2, 7, 4, 0, 9, 5, 1, 3}, 6) << endl;

    return 0;
}
