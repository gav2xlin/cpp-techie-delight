/*

Given two sorted integer arrays `X[]` and `Y[]` of size `m` and `n` each where `m >= n` and `X[]` has exactly `n` vacant cells, merge elements of `Y[]` in their correct position in array `X[]`, i.e., merge `(X, Y)` by keeping the sorted order.

Input : Two arrays X[] and Y[] where vacant cells in X[] is represented by 0.

X[] = [0, 2, 0, 3, 0, 5, 6, 0, 0]
Y[] = [1, 8, 9, 10, 15]

Output: X[] = [1, 2, 3, 5, 6, 8, 9, 10, 15]

*/

#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

class Solution
{
private:
    void mergeInternal(vector<int> &X, vector<int> &Y, int m, int n) {
        int k = m + n + 1;

        while (m >= 0 && n >= 0) {
            if (X[m] > Y[n]) {
                X[k--] = X[m--];
            } else {
                X[k--] = Y[n--];
            }
        }

        while (n >= 0) {
            X[k--] = Y[n--];
        }
    }
public:
    void merge(vector<int> &X, vector<int> &Y)
    {
        if (X.empty()) return;

        int k = 0;
        for (int i = 0; i < X.size(); ++i) {
            if (X[i]) {
                swap(X[i], X[k++]);
            }
        }

        mergeInternal(X, Y, k - 1, Y.size() - 1);

        fill(Y.begin(), Y.end(), 0);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> X{0, 2, 0, 3, 0, 5, 6, 0, 0};
    vector<int> Y{1, 8, 9, 10, 15};

    cout << "before:\n" << X << '\n' << Y << endl;
    Solution().merge(X, Y);
    cout << "after:\n" << X << '\n' << Y << endl;

    return 0;
}
