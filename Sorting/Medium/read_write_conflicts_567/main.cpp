/*

Given a list of database transactions, find all read-write conflicts among them. Assume that there is no strict two-phase locking (Strict 2PL) protocol to prevent read-write conflicts.

Each database transaction is given in the form of a tuple. A tuple (T, A, t, R) indicates that a transaction T accessed a database record A at a time t, and a read operation is performed on the record.

Assume that a data conflict happens when two transactions access the same record in the database within an interval of 5 units. At least one write operation is performed on the record.

Input:

transactions = [
    (T1, A, 0, R),
    (T2, A, 2, W),
    (T3, B, 4, W),
    (T4, C, 5, W),
    (T5, B, 7, R),
    (T6, C, 8, W),
    (T7, A, 9, R)
]

Output: {(T1, T2, RW), (T3, T5, WR), (T4, T6, WW)}

Explanation:

• Transaction T1 and T2 are involved in RW conflict.
• Transaction T3 and T5 are involved in WR conflict.
• Transaction T4 and T6 are involved in WW conflict.

*/

#include <iostream>
#include <vector>
#include <string>
#include <tuple>
#include <set>

using namespace std;

class Transaction
{
public:
    string name;		// Transaction Name
    string record;		// Data object from the database
    int timestamp;  	// Timestamp of the current transaction
    char operation; 	// Operation type: Read/Write

    Transaction() {}
    Transaction(string name, string record, int timestamp, char operation):
        name(name), record(record), timestamp(timestamp), operation(operation) {}
};

class Solution
{
public:

    /* Definition of Transaction class

        class Transaction
        {
        public:
            string name;		// Transaction Name
            string record;		// Data object from the database
            int timestamp;  	// Timestamp of the current transaction
            char operation; 	// Operation type: Read/Write

            Transaction() {}
            Transaction(string name, string record, int timestamp, char operation):
                name(name), record(record), timestamp(timestamp), operation(operation) {}
        };
    */

    set<tuple<string, string, string>> findConflicts(vector<Transaction> &transactions)
    {
        sort(transactions.begin(), transactions.end(), [](const Transaction &x, const Transaction &y) {
            if (x.record != y.record) {
                return x.record < y.record;
            }

            return x.timestamp < y.timestamp;
        });

        set<tuple<string, string, string>> res;
        for (int i = 0; i < transactions.size(); ++i)
        {
            int j = i - 1;

            while (j >= 0 && transactions[i].record == transactions[j].record && transactions[i].timestamp <= transactions[j].timestamp + 5)
            {
                if (transactions[i].operation == 'W' || transactions[j].operation == 'W')
                {
                    string op;
                    op += transactions[j].operation;
                    op += transactions[i].operation;

                    auto v = make_tuple(transactions[j].name, transactions[i].name, op);
                    res.insert(v);
                }

                --j;
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const tuple<string, string, string>& t) {
    os << get<0>(t) << ' ' << get<1>(t) << ' ' << get<2>(t);
    return os;
}

ostream& operator<<(ostream& os, const set<tuple<string, string, string>>& sets) {
    for (auto& s : sets) {
        os << s << endl;
    }
    return os;
}

int main()
{
    vector<Transaction> transactions{
        {"T1", "A", 0, 'R'},
        {"T2", "A", 2, 'W'},
        {"T3", "B", 4, 'W'},
        {"T4", "C", 5, 'W'},
        {"T5", "B", 7, 'R'},
        {"T6", "C", 8, 'W'},
        {"T7", "A", 9, 'R'}
    };

    cout << Solution().findConflicts(transactions) << endl;

    return 0;
}
