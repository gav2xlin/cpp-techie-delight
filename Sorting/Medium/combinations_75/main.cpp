/*

Given an integer array, find all distinct combinations of a given length `k`. The solution should return a set containing all the distinct combinations, while preserving the relative order of elements as they appear in the array.

Input : [2, 3, 4], k = 2
Output: {[2, 3], [2, 4], [3, 4]}

Input : [1, 2, 1], k = 2
Output: {[1, 2], [1, 1], [2, 1]}

Input : [1, 1, 1], k = 2
Output: {[1, 1]}

Input : [1, 2, 3], k = 4
Output: {}

Input : [1, 2], k = 0
Output: {[]}

*/

#include <iostream>
#include <vector>
#include <set>
#include <list>

using namespace std;

class Solution
{
private:
    void findCombinations(vector<int> const &nums, int n, int k, set<vector<int>> &subarrays, list<int> &out)
    {
        if (nums.size() == 0 || k > n) {
            return;
        }

        if (k == 0) {
            subarrays.insert(vector<int>(out.begin(), out.end()));
            return;
        }

        for (int i = n - 1; i >= 0; --i)
        {
            out.push_front(nums[i]);
            findCombinations(nums, i, k - 1, subarrays, out);
            out.pop_front();
        }
    }
public:
    set<vector<int>> findCombinations(vector<int> const &nums, int k)
    {
        set<vector<int>> subarrays;

        list<int> out;
        findCombinations(nums, nums.size(), k, subarrays, out);

        return subarrays;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& sets) {
    for (auto& s : sets) {
        os << s << endl;
    }
    return os;
}

int main()
{
    Solution s;

    cout << s.findCombinations({2, 3, 4}, 2) << endl;
    cout << s.findCombinations({1, 2, 1}, 2) << endl;
    cout << s.findCombinations({1, 1, 1}, 2) << endl;
    cout << s.findCombinations({1, 2, 3}, 4) << endl;
    cout << s.findCombinations({1, 2}, 0) << endl;

    return 0;
}
