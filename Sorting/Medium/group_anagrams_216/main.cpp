/*

Given a list of words, efficiently group all anagrams. The two strings, X and Y, are anagrams if by rearranging X's letters, we can get Y using all the original letters of X exactly once.

Input : ["CARS", "REPAID", "DUES", "NOSE", "SIGNED", "LANE", "PAIRED", "ARCS", "GRAB", "USED", "ONES", "BRAG", "SUED", "LEAN", "SCAR", "DESIGN"]

Output:

{
    ["CARS", "ARCS", "SCAR"],
    ["REPAID", "PAIRED"],
    ["SIGNED", "DESIGN"],
    ["LANE", "LEAN"],
    ["GRAB", "BRAG"],
    ["NOSE", "ONES"],
    ["DUES", "USED", "SUED"]
}


Input : ["CARS", "LANE", "ONES"]
Output: {}

The solution should return a set containing all the anagrams grouped together, irrespective of the order.

*/

#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <unordered_map>
#include <utility>
#include <algorithm>

using namespace std;

class Solution
{
public:
    set<set<string>> groupAnagrams(vector<string> const &words)
    {
        set<set<string>> anagrams;

        vector<string> list(words);
        for (string &s: list) {
            sort(s.begin(), s.end());
        }

        unordered_multimap<string, int> map;
        for (int i = 0; i < words.size(); ++i) {
            map.insert(make_pair(list[i], i));
        }

        auto it = map.begin();
        while (it != map.end())
        {
            set<string> anagram;
            for (auto curr = it; it != map.end() && it->first == curr->first; ++it) {
                anagram.insert(words[it->second]);
            }
            if (anagram.size() > 1) {
                anagrams.insert(anagram);
            }
        }

        return anagrams;
    }
};

ostream& operator<<(ostream& os, const set<string>& strings) {
    for (const auto &s : strings) {
        os << s << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<set<string>>& sets) {
    for (auto& s : sets) {
        os << s << endl;
    }
    return os;
}

int main()
{
    vector<string> words{"CARS", "REPAID", "DUES", "NOSE", "SIGNED", "LANE", "PAIRED", "ARCS", "GRAB", "USED", "ONES", "BRAG", "SUED", "LEAN", "SCAR", "DESIGN"};

    auto groups = Solution().groupAnagrams(words);
    cout << groups << endl;

    return 0;
}
