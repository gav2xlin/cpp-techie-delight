/*

Given an array of positive and negative integers, in-place segregate them without changing the relative order of elements. The output should contain all positive numbers follow negative numbers while maintaining the same relative ordering.

Input : [9, -3, 5, -2, -8, -6, 1, 3]
Output: [-3, -2, -8, -6, 9, 5, 1, 3]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    void merge(vector<int> &arr, vector<int> &aux, int low, int mid, int high)
    {
        int k = low;

        for (int i = low; i <= mid; ++i)
        {
            if (arr[i] < 0) {
                aux[k++] = arr[i];
            }
        }

        for (int j = mid + 1; j <= high; ++j)
        {
            if (arr[j] < 0) {
                aux[k++] = arr[j];
            }
        }

        for (int i = low; i <= mid; ++i)
        {
            if (arr[i] >= 0) {
                aux[k++] = arr[i];
            }
        }

        for (int j = mid + 1; j <= high; ++j)
        {
            if (arr[j] >= 0) {
                aux[k++] = arr[j];
            }
        }

        for (int i = low; i <= high; i++) {
            arr[i] = aux[i];
        }
    }

    void partition(vector<int> &arr, vector<int> &aux, int low, int high)
    {
        if (high <= low) {
            return;
        }

        int mid = (low + high) / 2;

        partition(arr, aux, low, mid);
        partition(arr, aux, mid + 1, high);

        merge(arr, aux, low, mid, high);
    }
public:
    void rearrange(vector<int> &arr)
    {
        vector<int> aux(arr.size());
        partition(arr, aux, 0, arr.size() - 1);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{9, -3, 5, -2, -8, -6, 1, 3};
    cout << "before: " << nums << endl;

    Solution().rearrange(nums);

    cout << "after: " << nums << endl;

    return 0;
}
