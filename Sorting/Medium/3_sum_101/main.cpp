/*

Given an unsorted integer array, find all triplets in it with sum less than or equal to a given number.

Input : nums[] = [2, 7, 4, 9, 5, 1, 3], target = 10
Output: {[1, 2, 3], [1, 2, 4], [1, 2, 5], [1, 2, 7], [1, 3, 4], [1, 3, 5], [1, 4, 5], [2, 3, 4], [2, 3, 5]}

Input : nums[] = [3, 5, 7, 3, 2, 1], target = 5
Output: {}

Since the input can have multiple triplets with sum less than or equal to the target, the solution should return a set containing all the distinct triplets in any order.

*/

#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

class Solution
{
public:
    set<vector<int>> getTriplets(vector<int> const &_nums, int target)
    {
        set<vector<int>> subsets;

        vector<int> nums(_nums);
        sort(nums.begin(), nums.end());

        int n = nums.size();
        for (int i = 0; i <= n - 3; ++i)
        {
            int low = i + 1, high = n - 1;

            while (low < high)
            {
                if (nums[i] + nums[low] + nums[high] > target) {
                    --high;
                } else {
                    for (int x = low + 1; x <= high; ++x) {
                        subsets.insert({nums[i], nums[low], nums[x]});
                    }

                    ++low;
                }
            }
        }

        return subsets;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& sets) {
    for (auto& s : sets) {
        os << s << endl;
    }
    return os;
}

int main()
{
    cout << Solution().getTriplets({2, 7, 4, 9, 5, 1, 3}, 10) << endl;
    cout << Solution().getTriplets({3, 5, 7, 3, 2, 1}, 5) << endl;

    return 0;
}
