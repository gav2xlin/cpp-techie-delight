/*

Find the largest number possible from a set of given numbers, where the numbers append to each other in any order to form the largest number. The solution should return the string representation of the largest number.

Input : [10, 68, 75, 7, 21, 12]
Output: 77568211210

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

class Solution
{
public:
    string findLargestNumber(vector<int> &nums)
    {
        sort(nums.begin(), nums.end(), [](int a, int b) {
            string val1 = to_string(a) + to_string(b);
            string val2 = to_string(b) + to_string(a);
            return val1 > val2;
        });

        string res;
        for (auto v : nums) {
            res += to_string(v);
        }
        return res;
    }
};

int main()
{
    vector<int> nums{10, 68, 75, 7, 21, 12};
    cout << Solution().findLargestNumber(nums) << endl;

    return 0;
}
