/*

Given a list of jobs where each job has a start and finish time, and a profit associated with it, find a maximum profit subset of non-overlapping jobs.

Input: jobs = [(0, 6, 60), (5, 9, 50), (1, 4, 30), (5, 7, 30), (3, 5, 10), (7, 8, 10)]		# (starting time, finishing time, associated profit)
Output: 80
Explanation: The jobs involved in the maximum profit are: (1, 4, 30) and (5, 9, 50).

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Job {
    int start, finish, profit;
};

class Solution
{
private:
    int findLastNonConflictingJob(vector<Job> const &jobs, int n)
    {
        int low = 0, high = n;

        while (low <= high)
        {
            int mid = (low + high) / 2;

            if (jobs[mid].finish <= jobs[n].start)
            {
                if (jobs[mid + 1].finish <= jobs[n].start) {
                    low = mid + 1;
                } else {
                    return mid;
                }
            } else {
                high = mid - 1;
            }
        }

        return -1;
    }
public:

    /* The Job class have three public member variables, start, finish, and profit. */

    int findMaxProfit(vector<Job> jobs)
    {
        int n = jobs.size();
        if (n == 0) {
            return 0;
        }

        sort(jobs.begin(), jobs.end(), [](Job &x, Job &y) {
            return x.finish < y.finish;
        });

        vector<int> maxProfit(n);
        maxProfit[0] = jobs[0].profit;

        for (int i = 1; i < n; ++i)
        {
            int index = findLastNonConflictingJob(jobs, i);

            int incl = jobs[i].profit;
            if (index != -1) {
                incl += maxProfit[index];
            }

            maxProfit[i] = max(incl, maxProfit[i-1]);
        }

        return maxProfit[n - 1];
    }
};

int main()
{
    cout << Solution().findMaxProfit({{0, 6, 60}, {5, 9, 50}, {1, 4, 30}, {5, 7, 30}, {3, 5, 10}, {7, 8, 10}}) << endl;

    return 0;
}
