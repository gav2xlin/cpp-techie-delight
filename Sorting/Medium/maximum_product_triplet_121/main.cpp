/*

Given an integer array, find a triplet having the maximum product.

Input : [-4, 1, -8, 9, 6]
Output: [-4, -8, 9]

If the input contains several triplets with maximum product, the solution can return any one of them.

Input : [5, 2, -10, 4, 5, 1, -2]
Output: [5, 4, 5] or [5, -10, -2]

Note: The solution can return the triplet in any order, not necessarily as they appear in the array.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    vector<int> findTriplet(vector<int> const &_nums)
    {
        int n = _nums.size();
        if (n <= 2) {
            return {};
        }

        vector<int> nums(_nums);
        sort(nums.begin(), nums.end());

        if (nums[n-1] * nums[n-2] * nums[n-3] > nums[0] * nums[1] * nums[n-1])
        {
            return {nums[n-1], nums[n-2], nums[n-3]};
        } else {
            return {nums[0], nums[1], nums[n-1]};
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findTriplet({-4, 1, -8, 9, 6}) << endl;
    cout << Solution().findTriplet({5, 2, -10, 4, 5, 1, -2}) << endl;

    return 0;
}
