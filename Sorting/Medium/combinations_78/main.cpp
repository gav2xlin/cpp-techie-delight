/*

Given an integer array, find all distinct combinations of a given length `k`, where the repetition of elements is allowed.

Input : [1, 2, 3], k = 2
Output: {[1, 1], [1, 2], [1, 3], [2, 2], [2, 3], [3, 3]}

Input : [1, 1, 1], k = 2
Output: {[1, 1]}

Input : [1, 2], k = 0
Output: {[]}

Input : [], k = 1
Output: {}

The solution should consider only distinct combinations. For example, either [1, 1, 2] or [1, 2, 1] or [2, 1, 1] should be considered for the below input. Same goes for [1, 2, 2] or [2, 1, 2] or [2, 2, 1].

Input : [1, 2, 1], k = 3
Output: {[1, 1, 1], [1, 1, 2], [1, 2, 2], [2, 2, 2]}

*/

#include <iostream>
#include <vector>
#include <set>
#include <list>
#include <algorithm>

using namespace std;

class Solution
{
private:
    void findCombinations(vector<int>& arr, vector<int> &out, int k, int i, int n, set<vector<int>>& combinations)
    {
        if (out.size() == k)
        {
            combinations.insert(out);
            return;
        }

        for (int j = i; j < n; ++j)
        {
            out.push_back(arr[j]);
            findCombinations(arr, out, k, j, n, combinations);
            out.pop_back();

            while (j < n - 1 && arr[j] == arr[j + 1]) {
                ++j;
            }
        }
    }
public:
    set<vector<int>> findCombinations(vector<int> const &nums, int k)
    {
        vector<int> arr(nums);
        sort(arr.begin(), arr.end());

        set<vector<int>> combinations;

        vector<int> out;
        findCombinations(arr, out, k, 0, nums.size(), combinations);

        return combinations;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& sets) {
    for (auto& s : sets) {
        os << s << endl;
    }
    return os;
}

int main()
{
    Solution s;

    cout << s.findCombinations({1, 2, 3}, 2) << endl;
    cout << s.findCombinations({1, 1, 1}, 2) << endl;
    cout << s.findCombinations({1, 2}, 0) << endl;
    cout << s.findCombinations({}, 1) << endl;

    return 0;
}
