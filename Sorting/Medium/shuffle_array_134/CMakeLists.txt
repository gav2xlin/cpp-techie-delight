cmake_minimum_required(VERSION 3.5)

project(shuffle_array_134 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(shuffle_array_134 main.cpp)

install(TARGETS shuffle_array_134
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
