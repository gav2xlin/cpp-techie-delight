/*

Given two integer arrays, reorder elements of the first array by the order of elements defined by the second array.

The elements that are not present in the second array but present in the first array should be appended at the end in sorted order. The second array can contain some extra elements which are not part of the first array.

Input:

nums[] = [5, 8, 9, 3, 5, 7, 1, 3, 4, 9, 3, 5, 1, 8, 4]
 pos[] = [3, 5, 7, 2]

Output : [3, 3, 3, 5, 5, 5, 7, 1, 1, 4, 4, 8, 8, 9, 9]

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

class Solution
{
public:
    void shuffle(vector<int> &nums, vector<int> &pos)
    {
        unordered_map<int, int> freq;
        for (int i = 0; i < pos.size(); ++i) {
            freq[pos[i]] = i;
        }

        sort(nums.begin(), nums.end(), [freq](int x, int y) {
            if (freq.count(x) && freq.count(y)) {
                return freq.at(x) < freq.at(y);
            } else if (freq.count(y)) {
                return false;
            } else if (freq.count(x)) {
                return true;
            } else {
                return x < y;
            }
        });
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{5, 8, 9, 3, 5, 7, 1, 3, 4, 9, 3, 5, 1, 8, 4};
    vector<int> pos{3, 5, 7, 2};

    cout << "before:\n" << nums << '\n' << pos << endl;

    Solution().shuffle(nums, pos);

    cout << "after:\n" << nums << endl;

    return 0;
}
