/*

Given a string, find all palindromic permutations of it.

Input : "xyxzwxxyz"
Output: {"xxyzwzyxx", "xxzywyzxx", "xyxzwzxyx", "xyzxwxzyx", "xzxywyxzx", "xzyxwxyzx", "yxxzwzxxy", "yxzxwxzxy", "yzxxwxxzy", "zxxywyxxz", "zxyxwxyxz", "zyxxwxxyz"}

*/

#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
public:
    unordered_set<string> findPalindromicPermutations(string str)
    {
        if (str.empty()) {
            return {};
        }

        unordered_map<char, int> freq;
        for (char ch: str) {
            freq[ch]++;
        }

        int odd = 0;
        string mid, left, right;

        for (auto& itr: freq)
        {
            char ch = itr.first;
            int c = itr.second;

            if ((c & 1))
            {
                if (++odd > 1) {
                    return {};
                }

                --c;
                mid = itr.first;
            }

            c = c / 2;
            while (c--) {
                left += ch;
            }
        }

        sort(left.begin(), left.end());

        unordered_set<string> res;
        while (true)
        {
            right = left;
            reverse(right.begin(), right.end());

            res.insert(left + mid + right);

            if (!next_permutation(left.begin(), left.end())) {
            //if (!prev_permutation(left.begin(), left.end())) {
                break;
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findPalindromicPermutations("xyxzwxxyz") << endl;

    return 0;
}
