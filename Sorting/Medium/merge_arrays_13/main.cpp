/*

Given two sorted integer arrays, `X[]` and `Y[]` of size `m` and `n` each, in-place merge elements of `X[]` with elements of array `Y[]` by maintaining the sorted order, i.e., fill `X[]` with the first `m` smallest elements and fill `Y[]` with remaining elements.

Input :

X[] = [1, 4, 7, 8, 10]
Y[] = [2, 3, 9]

Output:

X[] = [1, 2, 3, 4, 7]
Y[] = [8, 9, 10]

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
public:
    void merge(vector<int> &X, vector<int> &Y)
    {
        for (int i = 0; i < X.size(); ++i) {
            if (X[i] <= Y[0]) continue;
            swap(X[i], Y[0]);

            int j, first = Y[0];
            for (j = 1; j < Y.size() && Y[j] < first; ++j) {
                Y[j - 1] = Y[j];
                //swap(Y[j - 1], Y[j]);
            }
            Y[j - 1] = first;
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> X{1, 4, 7, 8, 10};
    vector<int> Y{2, 3, 9};

    cout << "before:" << endl;
    cout << X << endl;
    cout << Y << endl;

    Solution().merge(X, Y);

    cout << "after:" << endl;
    cout << X << endl;
    cout << Y << endl;

    return 0;
}
