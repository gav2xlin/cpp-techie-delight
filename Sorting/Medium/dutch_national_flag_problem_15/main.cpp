/*

Given an array containing only 0’s, 1’s, and 2’s, in-place sort it in linear time and using constant space.

Input : [0, 1, 2, 2, 1, 0, 0, 2, 0, 1, 1, 0]
Output: [0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2]

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
public:
    void sortArray(vector<int> &nums)
    {
        int start = 0, mid = 0, end = nums.size() - 1, pivot = 1;

        while (mid <= end) {
            if (nums[mid] < pivot) {
                swap(nums[start++], nums[mid++]);
            } else if (nums[mid] > pivot) {
                swap(nums[end--], nums[mid]);
            } else {
                ++mid;
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{0, 1, 2, 2, 1, 0, 0, 2, 0, 1, 1, 0};
    cout << "before: " << nums << endl;
    Solution().sortArray(nums);
    cout << "after: " << nums << endl;

    return 0;
}
