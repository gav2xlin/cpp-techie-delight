/*

Given an integer array, in-place rearrange it such that it contains positive and negative numbers at alternate positions. Assume that all values in the array are non-zero.

• In case the multiple rearrangement exists, the solution can return any one of them.

Input : [9, -3, 5, -2, -8, -6, 1, 3]
Output: [9, -3, 5, -2, 1, -8, 3, -6] or [5, -2, 9, -6, 1, -8, 3, -3] or any other valid combination..

• If the array contains more positive or negative elements, the solution should move them to the end of the array.

Input : [9, -3, 5, -2, -8, -6]
Output: [5, -2, 9, -6, -3, -8] or [-2, 5, -6, 9, -3, -8] or any other valid combination..

Input : [5, 4, 6, -1, 3]
Output: [5, -1, 4, 6, 3] or [-1, 5, 4, 6, 3] or any other valid combination..

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
private:
    int partition(vector<int> &nums) {
        int j = 0;
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] < 0) {
                swap(nums[i], nums[j++]);
            }
        }
        return j;
    }
public:
    void rearrange(vector<int> &nums)
    {
        int p = partition(nums);
        for (int i = 0; i < p && p < nums.size(); i += 2, ++p) {
            swap(nums[i], nums[p]);
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        vector<int> nums{9, -3, 5, -2, -8, -6, 1, 3};
        cout << "before:\n" << nums << endl;
        Solution().rearrange(nums);
        cout << "after:\n" << nums << endl;
    }

    {
        vector<int> nums{9, -3, 5, -2, -8, -6};
        cout << "before:\n" << nums << endl;
        Solution().rearrange(nums);
        cout << "after:\n" << nums << endl;
    }

    {
        vector<int> nums{5, 4, 6, -1, 3};
        cout << "before:\n" << nums << endl;
        Solution().rearrange(nums);
        cout << "after:\n" << nums << endl;
    }

    return 0;
}
