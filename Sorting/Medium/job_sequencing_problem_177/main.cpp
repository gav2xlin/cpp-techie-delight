/*

Given a list of tasks with deadlines and total profit earned on completing a task, find the maximum profit earned by executing the tasks within the specified deadlines. Assume that each task takes one unit of time to complete, and a task can't execute beyond its deadline.

Input: (Task Id, Deadline, Profit)

[(1, 9, 15), (2, 2, 2), (3, 5, 18), (4, 7, 1), (5, 4, 25), (6, 2, 20), (7, 5, 8), (8, 7, 10), (9, 4, 12), (10, 3, 5)]

Output: {1, 3, 4, 5, 6, 7, 8, 9}
Explanation: The maximum profit that can be achieved is 109 by leaving tasks 2 and 10 out.

Constraints:

• Only a single task can be executed at a time.
• The maximum number of tasks are 100.
• The maximum deadline that can be associated with a job is 1000.

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Job
{
public:
    int taskId, deadline, profit;

    bool operator==(const Job &p) const {
        return taskId == p.taskId && deadline == p.deadline && profit == p.profit;
    }
};

#define T 1000

class Solution
{
public:

    /*
        // Defination of Job class
        class Job
        {
        public:
            int taskId, deadline, profit;

            bool operator==(const Job &p) const {
                return taskId == p.taskId && deadline == p.deadline && profit == p.profit;
            }
        };
    */

    unordered_set<int> scheduleJobs(vector<Job> jobs)
    {
        int profit = 0;

        vector<int> slot(T, -1);

        sort(jobs.begin(), jobs.end(),
            [](Job &a, Job &b) {
                return a.profit > b.profit;
            });

        for (const Job &job: jobs)
        {
            for (int j = job.deadline - 1; j >= 0; --j)
            {
                if (j < T && slot[j] == -1)
                {
                    slot[j] = job.taskId;
                    profit += job.profit;

                    break;
                }
            }
        }

        unordered_set<int> res;
        for (int i = 0; i < T; i++)
        {
            if (slot[i] != -1) {
                res.insert(slot[i]);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().scheduleJobs({{1, 9, 15}, {2, 2, 2}, {3, 5, 18}, {4, 7, 1}, {5, 4, 25}, {6, 2, 20}, {7, 5, 8}, {8, 7, 10}, {9, 4, 12}, {10, 3, 5}}) << endl;

    return 0;
}
