/*

Given an unsorted integer array, find all quadruplets (i.e., four elements tuple) having a given sum.

Input : nums = [2, 7, 4, 0, 9, 5, 1, 3], target = 7
Output: {[0, 1, 2, 4]}

Since the input can contain multiple quadruplets that sum to a given target, the solution should return a set containing all the quadruplets.

Input : nums = [2, 7, 4, 0, 9, 5, 1, 3], target = 20
Output: {[0, 4, 7, 9], [1, 3, 7, 9], [2, 4, 5, 9]}

Note: The order of elements doesn't matter in a quadruplet, and any permutation is allowed in the output. For example, the output set can contain quadruplets [9, 1, 7, 3] and [9, 3, 7, 1], but system treats them the same.

*/

#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>
#include <utility>

using namespace std;

class Solution
{
public:
    using Pair = pair<int, int>;

    set<vector<int>> findQuadruplets(vector<int> const &nums, int target)
    {
        set<vector<int>> quadruplets;

        unordered_map<int, vector<Pair>> map;

        int n = nums.size();
        for (int i = 0; i < n - 1; i++)
        {
            for (int j = i + 1; j < n; ++j)
            {
                int val = target - (nums[i] + nums[j]);

                if (map.find(val) != map.end())
                {
                    for (auto pair: map.find(val)->second)
                    {
                        int x = pair.first;
                        int y = pair.second;

                        if ((x != i && x != j) && (y != i && y != j))
                        {
                            quadruplets.insert({nums[i], nums[j], nums[x], nums[y]});
                        }
                    }
                }

                map[nums[i] + nums[j]].push_back({i, j});
            }
        }

        return quadruplets;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& sets) {
    for (auto& s : sets) {
        os << s << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findQuadruplets({2, 7, 4, 0, 9, 5, 1, 3}, 7) << endl;
    cout << Solution().findQuadruplets({2, 7, 4, 0, 9, 5, 1, 3}, 20) << endl;

    return 0;
}
