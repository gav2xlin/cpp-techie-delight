/*

Given a set S, return all distinct subsets of it, i.e., find distinct power set of set S. A power set of any set S is the set of all subsets of S, including the empty set and S itself.

Input : S[] = [1, 2, 3]
Output: [[1, 2, 3], [2, 3], [1, 3], [3], [1, 2], [2], [1], []]

Input : S[] = [1, 2, 1]
Output: [[1, 1, 2], [1, 2], [2], [1, 1], [1], []]

Input : S[] = [1, 1]
Output: [[1, 1], [1], []]

Input : S[] = []
Output: [[]]

Note: The solution can return elements of a subsets in any order.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <set>

using namespace std;

class Solution
{
private:
    void findPowerSet(vector<int> &S, vector<int> &out, int i, set<vector<int>> &powerset)
    {
        if (i < 0)
        {
            powerset.insert(out);
            return;
        }

        out.push_back(S[i]);
        findPowerSet(S, out, i - 1, powerset);

        out.pop_back();

        while (S[i] == S[i-1]) {
            i--;
        }

        findPowerSet(S, out, i - 1, powerset);
    }
public:
    vector<vector<int>> findPowerSet(const vector<int> &_S)
    {
        set<vector<int>> powerset;

        vector<int> S(_S);
        sort(S.begin(), S.end());

        vector<int> out;
        findPowerSet(S, out, S.size() - 1, powerset);

        return {powerset.begin(), powerset.end()};
    }
    /*vector<vector<int>> findPowerSet(const vector<int> &_S)
    {
        set<vector<int>> powerset;

        vector<int> S(_S);
        sort(S.begin(), S.end());

        int N = pow(2, S.size());
        for (int i = 0; i < N; ++i)
        {
            vector<int> set;

            for (int j = 0; j < S.size(); ++j)
            {
                if (i & (1 << j)) {
                    set.push_back(S[j]);
                }
            }

            powerset.insert(set);
        }

        return {powerset.begin(), powerset.end()};
    }*/
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findPowerSet({1, 2, 3}) << endl;
    cout << Solution().findPowerSet({1, 2, 1}) << endl;
    cout << Solution().findPowerSet({1, 1}) << endl;
    cout << Solution().findPowerSet({}) << endl;

    return 0;
}
