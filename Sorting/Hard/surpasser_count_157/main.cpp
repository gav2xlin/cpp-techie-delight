/*

Given an integer array having distinct elements, find the surpasser count for each element in it. In other words, for each array element, find the total number of elements to its right, which are greater than it.

Input : [4, 6, 3, 9, 7, 10]
Output: [4, 3, 3, 1, 1, 0]

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    void merge(vector<int> &arr, vector<int> &aux, int low, int mid, int high, auto &surpasser_count)
    {
        int k = low, i = low, j = mid + 1;
        int count = 0;

        while (i <= mid && j <= high)
        {
            if (arr[i] > arr[j])
            {
                surpasser_count[arr[i]] += count;

                aux[k++] = arr[i++];
            }
            else {
                aux[k++] = arr[j++];
                ++count;
            }
        }

        while (i <= mid)
        {
            surpasser_count[arr[i]] += count;
            aux[k++] = arr[i++];
        }

        /* no need to copy the second half (since the remaining items
           are already in their correct position in the temporary array) */

        for (int i = low; i <= high; ++i) {
            arr[i] = aux[i];
        }
    }

    void merge_sort(vector<int> &arr, vector<int> &aux, int low, int high, auto &surpasser_count)
    {
        if (high <= low) {
            return;
        }

        int mid = (low + high) >> 1;

        merge_sort(arr, aux, low, mid, surpasser_count);
        merge_sort(arr, aux, mid + 1, high, surpasser_count);

        merge(arr, aux, low, mid, high, surpasser_count);
    }
public:
    vector<int> findSurpasserCount(vector<int> const &nums)
    {
        unordered_map<int, int> surpasser_count;

        vector<int> aux(nums);
        vector<int> arr(nums);

        merge_sort(arr, aux, 0, nums.size() - 1, surpasser_count);

        vector<int> res;
        for (auto v : nums) {
            res.push_back(surpasser_count[v]);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findSurpasserCount({4, 6, 3, 9, 7, 10}) << endl;

    return 0;
}
