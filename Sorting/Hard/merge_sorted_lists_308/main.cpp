/*

Given `M` sorted lists of variable length, merge them efficiently in sorted order.

Input:

mat = [
    [10, 20, 30, 40],
    [15, 25, 35],
    [27, 29, 37, 48, 93],
    [32, 33]
]

Output: [10, 15, 20, 25, 27, 29, 30, 32, 33, 35, 37, 40, 48, 93]

*/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Solution
{
private:
    struct Node
    {
        // `val` stores the element,
        // `i` stores the list number of the element,
        // `index` stores the column number of the list from which element was taken
        int val, i, index;
    };

    struct comp
    {
        bool operator()(const Node &lhs, const Node &rhs) const {
            return lhs.val > rhs.val;
        }
    };
public:
    vector<int> mergeSortedLists(vector<vector<int>> const &lists)
    {
        int M = lists.size();

        priority_queue<Node, vector<Node>, comp> pq;

        for (int i = 0; i < M; ++i)
        {
            if (lists[i].size() >= 1) {
                pq.push({lists[i][0], i, 0});
            }
        }

        vector<int> res;
        while (!pq.empty())
        {
            Node min = pq.top();
            pq.pop();

            res.push_back(min.val);

            if (min.index + 1 < lists[min.i].size())
            {
                min.index += 1;
                min.val = lists[min.i][min.index];
                pq.push(min);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<vector<int>> lists
    {
        { 10, 20, 30, 40 },
        { 15, 25, 35 },
        { 27, 29, 37, 48, 93 },
        { 32, 33 }
    };

    cout << Solution().mergeSortedLists(lists) << endl;

    return 0;
}
