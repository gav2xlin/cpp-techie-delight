/*

Given a given sequence, find the length of the longest increasing subsequence (LIS) in it.

The longest increasing subsequence is a subsequence of a given sequence in which the subsequence's elements are in sorted order, lowest to highest, and in which the subsequence is as long as possible.

Input : [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]
Output: 6
Explanation: The longest increasing subsequence is [0, 2, 6, 9, 11, 15] having length 6; the input sequence has no 7–member increasing subsequences.

The longest increasing subsequence is not necessarily unique. For instance, [0, 4, 6, 9, 11, 15] and [0, 4, 6, 9, 13, 15] are other increasing subsequences of equal length in the same input sequence.

*/

#include <iostream>
#include <vector>
#include <set>
#include <stack>
#include <map>

using namespace std;

struct Node
{
    int elem;
    int index;
};

inline bool operator<(const Node &lhs, const Node &rhs) {
    return lhs.elem < rhs.elem;
}

class Solution
{
private:
    void print(vector<int> const &input, auto parent, set<Node> s)
    {
        stack<int> lis;

        int index = s.rbegin()->index;

        int n = s.size();

        while (n--)
        {
            lis.push(input[index]);
            index = parent[index];
        }

        cout << "LIS is ";
        while (!lis.empty())
        {
            cout << lis.top() << " ";
            lis.pop();
        }
    }

    void printLIS(vector<int> const &input)
    {
        if (input.size() == 0) {
            return;
        }

        set<Node> s;
        map<int, int> parent;

        for (int i = 0; i < input.size(); ++i)
        {
            Node curr = {input[i], i};

            if (s.find(curr) != s.end()) {
                continue;
            }

            auto it = s.insert(curr).first;

            if (++it != s.end()) {
                s.erase(it);
            }

            it = s.find(curr);
            parent[i] = (--it)->index;
        }

        print(input, parent, s);
    }
public:
    int findLISLength(vector<int> const &nums)
    {
        if (nums.empty()) {
            return 0;
        }

        set<int> s;

        for (int i = 0; i < nums.size(); ++i)
        {
            if (s.find(nums[i]) != s.end()) {
                continue;
            }

            auto ret = s.insert(nums[i]);

            set<int>::iterator it;
            if (ret.second) {
                it = ret.first;
            }

            if (++it != s.end()) {
                s.erase(it);
            }
        }

        return s.size();
    }
};

int main()
{
    cout << Solution().findLISLength({0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15}) << endl;

    return 0;
}
