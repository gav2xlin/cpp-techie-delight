/*

Given a set of rectangular 3D boxes (cuboids), create a stack of boxes as tall as possible and return the maximum height of the stacked boxes.

• A box can be placed on top of another box only if the dimensions of the 2D base of the lower box is each "strictly" larger than of the 2D base of the higher box. i.e., box i can be placed on top of box j if and only if length[i] < length[j] and width[i] < width[j].

• The solution can include "multiple" instances of the same box, such that a box can be rotated to use any of its sides as the base, and does not have to include the every box to achieve the maximum height.

For example,

Input: [(4, 2, 5), (3, 1, 6), (3, 2, 1), (6, 3, 8)]
Output: 22
Explanation: Each of the given box has dimensions L × W × H:

(4 × 2 × 5)
(3 × 1 × 6)
(3 × 2 × 1)
(6 × 3 × 8)

The valid rotations (length more than the width) of the boxes are:

(4 × 2 × 5), (5 × 4 × 2), (5 × 2 × 4)
(3 × 1 × 6), (6 × 3 × 1), (6 × 1 × 3)
(3 × 2 × 1), (3 × 1 × 2), (2 × 1 × 3)
(6 × 3 × 8), (8 × 6 × 3), (8 × 3 × 6)

The maximum height possible is 22, which can be obtained by arranging the boxes in the following order:

(3 × 1 × 6)
(4 × 2 × 5)
(6 × 3 × 8)
(8 × 6 × 3)

Note that (3 × 2 × 1) box is not included to achieve the maximum height.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Box {
public:
    int length, width, height;

    Box(): length{}, width{}, height{} {}
    Box(int length, int width, int height): length(length), width(width), height(height) {}
};

class Solution
{
private:
    vector<Box> createAllRotations(vector<Box> const &boxes)
    {
        vector<Box> rotations;

        for (const Box &box: boxes)
        {
            // push the original box: L × W × H
            rotations.push_back(box);

            // push the first rotation: max(L, H) × min(L, H) × W
            rotations.push_back({max(box.length, box.height),
                                 min(box.length, box.height),
                                 box.width});

            // push the second rotation: max(W, H) × min(W, H) × L
            rotations.push_back({max(box.width, box.height),
                                 min(box.width, box.height),
                                 box.length});
        }

        return rotations;
    }
public:

    /*
        Defination of Box class

        class Box {
        public:
            int length, width, height;

            Box() {}
            Box(int length, int width, int height): length(length), width(width), height(height) {}
        };
    */

    int findMaxHeight(vector<Box> const &boxes)
    {
        if (boxes.empty()) {
            return 0;
        }

        vector<Box> rotations = createAllRotations(boxes);

        // sort the boxes in descending order of base area (L × W)
        sort(rotations.begin(), rotations.end(),
             [](const Box &x, const Box &y) {
                return x.length * x.width > y.length * y.width;
            });

        vector<int> max_height(rotations.size());

        for (int i = 0; i < rotations.size(); ++i)
        {
            for (int j = 0; j < i; j++)
            {
                // dimensions of the lower box are each strictly larger than those
                // of the higher box
                if (rotations[i].length < rotations[j].length && rotations[i].width < rotations[j].width) {
                    max_height[i] = max(max_height[i], max_height[j]);
                }
            }

            max_height[i] += rotations[i].height;
        }

        return *max_element(max_height.begin(), max_height.end());
    }
};

int main()
{
    vector<Box> boxes {
        { 4, 2, 5 },
        { 3, 1, 6 },
        { 3, 2, 1 },
        { 6, 3, 8 }
    };

    cout << Solution().findMaxHeight(boxes) << endl;

    return 0;
}
