/*

Given a string, find all lexicographically next permutations of it.

The words are arranged in the same order in the lexicographic order as they are presumed to appear in a dictionary. For example, the lexicographically next permutation of string "ABCD" is "ABDC", for string "ABDC" is "ACBD", and for string "ACBD" is "ACDB".

Input : "ABCD"
Output: ["ABCD", "ABDC", "ACBD", "ACDB", "ADBC", "ADCB", "BACD", "BADC", "BCAD", "BCDA", "BDAC", "BDCA", "CABD", "CADB", "CBAD", "CBDA", "CDAB", "CDBA", "DABC", "DACB", "DBAC", "DBCA", "DCAB", "DCBA"]

Input : "CABD"
Output: ["CABD", "CADB", "CBAD", "CBDA", "CDAB", "CDBA", "DABC", "DACB", "DBAC", "DBCA", "DCAB", "DCBA"]

Input : "ABA"
Output: ["ABA", "BAA"]

Input : "AAA"
Output: ["AAA"]

Note that the solution should not print duplicate permutations.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
private:
    bool next_permutation(string &str, int n)
    {
        int i = n - 1;
        while (str[i - 1] >= str[i])
        {
            // if `i` is the first index of the string, that means we are already
            // at the highest possible permutation, i.e., the string is sorted in
            // descending order
            if (--i == 0) {
                return false;
            }
        }

        /* If we reach here, the substring `str[i…n)` is sorted in descending order
           i.e., str[i-1] < str[i] >= str[i+1] >= str[i+2] >= … >= str[n-1] */

        // find the highest index `j` to the right of index `i` such that
        // str[j] > str[i-1]

        int j = n - 1;
        while (j > i && str[j] <= str[i - 1]) {
            j--;
        }

        swap(str[i - 1], str[j]);

        reverse(str.begin() + i, str.end());

        return true;
    }
public:
    /*vector<string> findPermutations(string str)
    {
        if (str.length() == 0) {
            return {};
        }

        vector<string> res;
        while (true)
        {
            res.push_back(str);

            if (!next_permutation(str.begin(), str.end())) {
                break;
            }
        }

        return res;
    }*/
    vector<string> findPermutations(string str)
    {
        int n = str.length();
        if (n == 0) {
            return {};
        }

        if (n == 1) {
            return {str};
        }

        vector<string> res;
        while (true)
        {
            res.push_back(str);

            if (!next_permutation(str, str.length())) {
                break;
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findPermutations("ABCD") << endl;
    cout << Solution().findPermutations("CABD") << endl;
    cout << Solution().findPermutations("ABA") << endl;
    cout << Solution().findPermutations("AAA") << endl;

    return 0;
}
