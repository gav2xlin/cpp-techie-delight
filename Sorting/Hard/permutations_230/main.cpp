/*

Given a string, find all permutations of it.

Input : "ABC"
Output: ["ABC", "ACB", "BAC", "BCA", "CAB", "CBA"]

The solution should handle strings containing duplicate characters and don"t repeat the permutations.

Input : "ABA"
Output: ["ABA", "AAB", "BAA"]

Input : "AAA"
Output: ["AAA"]

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <set>
#include <list>

using namespace std;

class Solution
{
public:
    vector<string> findPermutations(string s)
    {
        int n = s.length();
        if (n == 0) {
            return {};
        }

        if (n == 1) {
            return {s};
        }

        sort(s.begin(), s.end());

        vector<string> res;
        while (true)
        {
            res.push_back(s);

            /* The following code will rearrange the string to the next lexicographically
               ordered permutation (if any) or return if we are already at the
               highest possible permutation */

            int i = n - 1;
            while (s[i-1] >= s[i])
            {
                if (--i == 0) {
                    return res;
                }
            }

            // find the highest index `j` to the right of index `i` such that
            // `s[j] > s[i-1]` (`s[i…n-1]` is sorted in reverse order)

            int j = n - 1;
            while (j > i && s[j] <= s[i - 1]) {
                --j;
            }

            swap(s[i - 1], s[j]);

            reverse (s.begin() + i, s.end());
        }

        return {};
    }
    /*vector<string> findPermutations(string s)
    {
        int n = s.length();

        if (n == 0) {
            return {};
        }

        vector<int> p(n); // Weight index control array

        int i = 1, j = 0;

        set<string> res;
        res.insert(s);

        while (i < n)
        {
            if (p[i] < i)
            {
                // if `i` is odd then `j = p[i]`; otherwise, `j = 0`
                j = (i % 2) * p[i];

                swap(s[j], s[i]);

                res.insert(s);

                ++p[i];
                i = 1;
            }
            else {
                p[i] = 0;

                ++i;
            }
        }

        return {res.begin(), res.end()};
    }*/
    /*vector<string> findPermutations(string s)
    {
        int n = s.length();

        if (n == 0) {
            return {};
        }

        list<string> partial;

        string firstChar(1, s[0]);
        partial.push_back(firstChar);

        for (int i = 1; i < s.length(); ++i)
        {
            for (int j = partial.size() - 1; j >= 0; --j)
            {
                string str = partial.front();
                partial.pop_front();

                // Insert the next character of the specified string, i.e., s[i],
                // in all possible positions of current partial permutation.
                // Then insert each of these newly constructed strings into the list.

                for (int k = 0; k <= str.length(); ++k) {
                    partial.push_back(str.substr(0, k) + s[i] + str.substr(k));
                }
            }
        }

        set<string> res{partial.begin(), partial.end()};
        return {res.begin(), res.end()};
    }*/
};

ostream& operator<<(ostream& os, const vector<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findPermutations("ABC") << endl;
    cout << Solution().findPermutations("ABA") << endl;
    cout << Solution().findPermutations("AAA") << endl;

    return 0;
}
