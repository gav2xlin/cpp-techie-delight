/*

Given a list of edges of a connected and weighted undirected graph, construct a minimum spanning tree (MST) out of it. An MST is a spanning tree of a connected, undirected graph that connects all the vertices with minimal total weighting for its edges.

Input: edges = [(0, 1, 7), (1, 2, 8), (0, 3, 5), (1, 3, 9), (1, 4, 7), (2, 4, 5), (3, 4, 15), (3, 5, 6), (4, 5, 8), (4, 6, 9), (5, 6, 11)]
Here, tuple (x, y, w) represents an edge from x to y having weight w.

Output: {(0, 3, 5), (2, 4, 5), (3, 5, 6), (0, 1, 7), (1, 4, 7), (4, 6, 9)}

Explanation: The minimum spanning tree will have exactly n-1 edges where n is the total number of vertices in the graph, and the sum of weights of edges is as minimum as possible.

*/

#include <iostream>
#include <set>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

class Edge {
public:
    int source, dest, weight;

    // overloaded < and == operator
    bool operator<(const Edge& e) const
    {
        return this->source < e.source ||
                this->dest < e.dest ||
                this->weight < e.weight;
    }

    bool operator==(const Edge& e) const
    {
        return this->source == e.source &&
                this->dest == e.dest &&
                this->weight == e.weight;
    }
};

/*struct compare
{
    bool operator() (Edge const &a, Edge const &b) const {
        return a.weight > b.weight;
    }
};*/

class DisjointSet
{
    unordered_map<int, int> parent;

public:
    void makeSet(int n)
    {
        for (int i = 0; i < n; ++i) {
            parent[i] = i;
        }
    }

    int Find(int k)
    {
        if (parent[k] == k) {
            return k;
        }

        return Find(parent[k]);
    }

    void Union(int a, int b)
    {
        int x = Find(a);
        int y = Find(b);

        parent[x] = y;
    }
};

class Solution
{
private:
    set<Edge> runKruskalAlgorithm(vector<Edge> edges, int n)        // no-ref, no-const
    {
        set<Edge> MST;

        DisjointSet ds;
        ds.makeSet(n);

        auto compare = [](Edge const &a, Edge const &b) {
            return a.weight > b.weight;
        };
        sort(edges.begin(), edges.end(), compare);

        while (MST.size() != n - 1)
        {
            Edge next_edge = edges.back();
            edges.pop_back();

            int x = ds.Find(next_edge.source);
            int y = ds.Find(next_edge.dest);

            if (x != y)
            {
                MST.insert(next_edge);
                ds.Union(x, y);
            }
        }
        return MST;
    }

    int getSize(vector<Edge> const &edges) {
        int n = 0;
        for (auto& e : edges) {
            n = max(n, max(e.source + 1, e.dest + 1));
        }
        return n;
    }
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int source, dest, weight;

            // overloaded < and == operator
        };
    */

    set<Edge> constructMinimumSpanningTree(vector<Edge> const &edges)
    {
        int n = getSize(edges);
        return runKruskalAlgorithm(edges, n);
    }
};

ostream& operator<<(ostream& os, const Edge& e) {
    os << e.source << ':' << e.dest << ':' << e.weight;
    return os;
}

ostream& operator<<(ostream& os, const set<Edge>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        vector<Edge> edges {
            {0, 1, 7}, {1, 2, 8}, {0, 3, 5}, {1, 3, 9}, {1, 4, 7}, {2, 4, 5},
            {3, 4, 15}, {3, 5, 6}, {4, 5, 8}, {4, 6, 9}, {5, 6, 11}
        };

        cout << Solution().constructMinimumSpanningTree(edges) << endl;
    }

    {
        vector<Edge> edges {
            {0, 1, 10}, {0, 4, 3}, {1, 2, 2}, {1, 4, 4}, {2, 3, 9}
        };

        cout << Solution().constructMinimumSpanningTree(edges) << endl;
    }

    return 0;
}
