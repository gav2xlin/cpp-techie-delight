/*

Given a string, find all distinct lexicographic permutations of it where the repetition of characters is allowed.

Input : "AAB"
Output: {"AAA", "AAB", "ABA", "ABB", "BAA", "BAB", "BBA", "BBB"}

*/

#include <iostream>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    /*void findLexicographic(string str, string result, unordered_set<string>& res)
    {
        if (result.length() == str.length())
        {
            res.insert(result);
            return;
        }

        for (unsigned i = 0; i < str.length(); ++i) {
            findLexicographic(str, result + str[i], res);
        }
    }*/

    void findLexicographic(string str, string result, unordered_set<string>& res)
    {
        if (result.length() == str.length())
        {
            res.insert(result);
            return;
        }

        for (unsigned i = 0; i < str.length(); ++i)
        {
            // skip adjacent duplicates
            while (i + 1 < str.length() && str[i] == str[i + 1]) {
                ++i;
            }

            findLexicographic(str, result + str[i], res);
        }
    }
public:
    /*unordered_set<string> findLexicographicPermutations(string str)
    {
        if (str.empty()) {
            return {};
        }

        string result;
        sort(str.begin(), str.end());

        unordered_set<string> res;
        findLexicographic(str, result, res);

        return res;
    }*/
    unordered_set<string> findLexicographicPermutations(string str)
    {
        if (str.length() == 0) {
            return {};
        }

        string result;
        sort(str.begin(), str.end());

        unordered_set<string> res;
        findLexicographic(str, result, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findLexicographicPermutations("AAB") << endl;

    return 0;
}
