cmake_minimum_required(VERSION 3.5)

project(lexicographic_permutations_242 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(lexicographic_permutations_242 main.cpp)

install(TARGETS lexicographic_permutations_242
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
