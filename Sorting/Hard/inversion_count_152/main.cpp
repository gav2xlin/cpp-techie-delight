/*

Given an integer array, find the total number of inversions of it. If `(i < j)` and `(nums[i] > nums[j])`, then pair `(i, j)` is called an inversion of an array `nums`. The solution should provide count of all such pairs in the array.

Input : [1, 9, 6, 4, 5]
Output: 5
Explanation: There are 5 inversions in the array: (9, 6), (9, 4), (9, 5), (6, 4), (6, 5)

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int Merge(vector<int> &arr, vector<int> &aux, int low, int mid, int high)
    {
        int k = low, i = low, j = mid + 1;
        int inversionCount = 0;

        while (i <= mid && j <= high)
        {
            if (arr[i] <= arr[j]) {
                aux[k++] = arr[i++];
            }
            else {
                aux[k++] = arr[j++];
                inversionCount += (mid - i + 1);
            }
        }

        while (i <= mid) {
            aux[k++] = arr[i++];
        }

        /* no need to copy the second half (since the remaining items
           are already in their correct position in the temporary array) */

        for (int i = low; i <= high; ++i) {
            arr[i] = aux[i];
        }

        return inversionCount;
    }

    int MergeSort(vector<int> & arr, vector<int> & aux, int low, int high)
    {
        if (high <= low) {
            return 0;
        }

        int mid = (low + high ) >> 1;
        int inversionCount = 0;

        inversionCount += MergeSort(arr, aux, low, mid);
        inversionCount += MergeSort(arr, aux, mid + 1, high);

        inversionCount += Merge(arr, aux, low, mid, high);

        return inversionCount;
    }
public:
    int findInversionCount(vector<int> const &_arr)
    {
        vector<int> arr(_arr);
        vector<int> aux(arr);
        return MergeSort(arr, aux, 0, arr.size() - 1);
    }
};

int main()
{
    cout << Solution().findInversionCount({1, 9, 6, 4, 5}) << endl;

    return 0;
}
