/*

Given a sequence of numbers between 2 and 9, print all possible combinations of words formed from the mobile keypad which has english alphabets associated with each key.

keypad = {
    2: ['A', 'B', 'C'],
    3: ['D', 'E', 'F'],
    4: ['G', 'H', 'I'],
    5: ['J', 'K', 'L'],
    6: ['M', 'N', 'O'],
    7: ['P', 'Q', 'R', 'S'],
    8: ['T', 'U', 'V'],
    9: ['W', 'X', 'Y', 'Z']
}

Input : [2, 3, 4]

Output: {"CEG", "AEH", "CDH", "CFI", "CEH", "BEI", "AFH", "BFG", "BDI", "ADH", "AEG", "AEI", "BEH", "BFH", "BDH", "CEI", "AFG", "BFI", "ADG", "CDG", "BDG", "CDI", "BEG", "AFI", "CFG", "CFH", "ADI"}

*/

#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution
{
private:
    void findCombinations(auto &keypad, auto const &keys, auto &combinations, string result, int index)
    {
        if (index == -1)
        {
            combinations.insert(result);
            return;
        }

        int digit = keys[index];

        for (char c: keypad[digit]) {
            findCombinations(keypad, keys, combinations, c + result, index - 1);
        }
    }
public:
    unordered_set<string> findCombinations(unordered_map<int,vector<char>> &keypad, vector<int> &keys)
    {
        if (keypad.size() == 0 || keys.size() == 0) {
            return {};
        }

        unordered_set<string> combinations;

        int n = keys.size();
        findCombinations(keypad, keys, combinations, "", n - 1);

        return combinations;
    }
    /*unordered_set<string> findCombinations(unordered_map<int,vector<char>> &keypad, vector<int> &keys)
    {
        vector<string> list;
        for (char c: keypad[keys[0]]) {
            list.push_back(string(1, c));
        }

        for (int i = 1; i < keys.size(); ++i)
        {
            vector<string> vec;

            for (char c: keypad[keys[i]])
            {
                for (string& str: list) {
                    vec.push_back(str + c);
                }
            }

            list = vec;
        }

        return {list.begin(), list.end()};
    }*/
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    unordered_map<int, vector<char>> keypad {
        {}, {},
        {2, { 'A', 'B', 'C' }},
        {3, { 'D', 'E', 'F' }},
        {4, { 'G', 'H', 'I' }},
        {5, { 'J', 'K', 'L' }},
        {6, { 'M', 'N', 'O' }},
        {7, { 'P', 'Q', 'R', 'S'}},
        {8, { 'T', 'U', 'V' }},
        {9, { 'W', 'X', 'Y', 'Z'}}
    };

    vector<int> keys = { 2, 3, 4 };

    cout << Solution().findCombinations(keypad, keys) << endl;

    return 0;
}
