/*

Given a string, find the minimum cuts needed to partition it such that each partition is a palindrome.

Input: s = "BABABCBADCD"
Output: 2
Explanation: The minimum cuts required are 2 as "BAB|ABCBA|DCD".

Input: s = "ABCBA"
Output: 0
Explanation: The minimum cuts required are 0 as "ABCBA" is already a palindrome.

Input: s = "ABCD"
Output: 3
Explanation: The minimum cuts required are 3 as "A|B|C|D".

*/

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <climits>

using namespace std;

/*class Solution
{
private:
    void findAllPalindromes(string X, int n, auto &isPalindrome)
    {
        for (int i = n - 1; i >= 0; --i)
        {
            for (int j = i; j < n; ++j)
            {
                if (i == j) {
                    isPalindrome[i][j] = true;
                }
                else if (X[i] == X[j]) {
                    isPalindrome[i][j] = ((j - i == 1) ? true: isPalindrome[i + 1][j - 1]);
                }
                else {
                    isPalindrome[i][j] = false;
                }
            }
        }
    }

    int findMinCuts(int i, int j, auto &lookup, auto &isPalindrome)
    {
        if (i == j || isPalindrome[i][j]) {
            return 0;
        }

        string key = to_string(i) + "|" + to_string(j);
        if (lookup.find(key) == lookup.end())
        {
            lookup[key] = INT_MAX;
            for (int k = i; k <= j - 1; ++k)
            {
                int count = 1 + findMinCuts(i, k, lookup, isPalindrome) + findMinCuts(k + 1, j, lookup, isPalindrome);

                if (count < lookup[key]) {
                    lookup[key] = count;
                }
            }
        }

        return lookup[key];
    }
public:
    int findMinimumCuts(string X)
    {
        int n = X.length();
        if (n == 0) {
            return 0;
        }

        unordered_map<string, int> lookup;
        vector<vector<bool>> isPalindrome(n + 1, vector<bool>(n + 1));

        findAllPalindromes(X, n, isPalindrome);

        return findMinCuts(0, n - 1, lookup, isPalindrome);
    }
};*/

class Solution
{
private:
    void findAllPalindromes(string X, auto &isPalindrome)
    {
        int n = X.length();

        for (int i = n - 1; i >= 0; --i)
        {
            for (int j = i; j < n; ++j)
            {
                if (i == j) {
                    isPalindrome[i][j] = true;
                }
                else if (X[i] == X[j]) {
                    isPalindrome[i][j] = ((j - i == 1) ? true: isPalindrome[i + 1][j - 1]);
                }
                else {
                    isPalindrome[i][j] = false;
                }
            }
        }
    }

    int findMinCuts(string X, auto &isPalindrome)
    {
        int n = X.length();

        int lookup[n];

        for (int i = n - 1; i >= 0; --i)
        {
            lookup[i] = INT_MAX;

            if (isPalindrome[i][n-1]) {
                lookup[i] = 0;
            }
            else {
                for (int j = n - 2; j >= i; --j)
                {
                    if (isPalindrome[i][j]) {
                        lookup[i] = min(lookup[i], 1 + lookup[j + 1]);
                    }
                }
            }
        }

        return lookup[0];
    }
public:
    int findMinimumCuts(string X)
    {
        int n = X.length();
        if (n == 0) {
            return 0;
        }

        vector<vector<bool>> isPalindrome(n + 1, vector<bool>(n + 1));

        findAllPalindromes(X, isPalindrome);

        return findMinCuts(X, isPalindrome);
    }
};

int main()
{
    cout << Solution().findMinimumCuts("BABABCBADCD") << endl;
    cout << Solution().findMinimumCuts("ABCBA") << endl;
    cout << Solution().findMinimumCuts("ABCD") << endl;

    return 0;
}
