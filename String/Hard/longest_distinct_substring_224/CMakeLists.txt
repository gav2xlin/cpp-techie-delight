cmake_minimum_required(VERSION 3.5)

project(longest_distinct_substring_224 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(longest_distinct_substring_224 main.cpp)

install(TARGETS longest_distinct_substring_224
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
