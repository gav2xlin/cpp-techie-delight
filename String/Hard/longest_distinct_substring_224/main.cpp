/*

Given a string and a positive number `k`, find the longest substring of the string containing `k` distinct characters. If `k` is more than the total number of distinct characters in the string, return the whole string.

Input: s = "abcbdbdbbdcdabd", k = 2
Output: "bdbdbbd"

Input: s = "abcbdbdbbdcdabd", k = 3
Output: "bcbdbdbbdcd"

Input: s = "abcbdbdbbdcdabd", k = 5
Output: "abcbdbdbbdcdabd"

The longest distinct character substring is not guaranteed to be unique. If multiple longest distinct substring exists, the solution should return the one which appear first in the string.

Input: s = "abcd", k = 3
Output: "abc"
Explanation: There are two longest distinct substrings in "abcd" having 3 distinct characters, namely, "abc" and "bcd". The solution returns "abc" as it appears before "bcd" in the string.


*/

#include <iostream>
#include <string>
#include <unordered_set>

using namespace std;

class Solution
{
   static constexpr int CHAR_RANGE = 128;
public:
    string findLongestSubstring(string str, int k)
    {
        int end = 0, begin = 0;
        unordered_set<char> window;
        int freq[CHAR_RANGE] = { 0 };

        for (int low = 0, high = 0; high < str.size(); ++high)
        {
            window.insert(str[high]);
            freq[str[high]]++;

            while (window.size() > k)
            {
                if (--freq[str[low]] == 0) {
                    window.erase(str[low]);
                }

                ++low;
            }

            if (end - begin < high - low)
            {
                end = high;
                begin = low;
            }
        }

        return str.substr(begin, end - begin + 1);
    }
};

int main()
{
    Solution s;
    cout << s.findLongestSubstring("abcbdbdbbdcdabd", 2) << endl;
    cout << s.findLongestSubstring("abcbdbdbbdcdabd", 3) << endl;
    cout << s.findLongestSubstring("abcbdbdbbdcdabd", 5) << endl;
    cout << s.findLongestSubstring("abcd", 3) << endl;

    return 0;
}
