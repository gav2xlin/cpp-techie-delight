cmake_minimum_required(VERSION 3.5)

project(k_palindrome_string_264 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(k_palindrome_string_264 main.cpp)

install(TARGETS k_palindrome_string_264
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
