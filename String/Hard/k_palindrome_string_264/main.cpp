/*

Given a string, check if it is k–palindrome or not. A string is k–palindrome if it becomes a palindrome on removing at-most k characters from it.

Input: s = "ABCDBA", k = 1
Output: true
Explanation: The string becomes a palindrome by removing either C or D from it.

Input: s = "ABCDECA", k = 1
Output: false
Explanation: The string needs at least 2–removals from it to become a palindrome.

*/

#include <iostream>
#include <algorithm>

using namespace std;

/*class Solution
{
private:
    int isKPalindrome(string X, int m, string Y, int n)
    {
        if (m == 0 || n == 0) {
            return n + m;
        }

        if (X[m - 1] == Y[n - 1]) {
            return isKPalindrome(X, m - 1, Y, n - 1);
        }

        int x = isKPalindrome(X, m - 1, Y, n);
        int y = isKPalindrome(X, m, Y, n - 1);

        return min(x, y) + 1;
    }
public:
    bool isKPalindrome(string s, int k)
    {
        string rev = s;
        reverse(rev.begin(), rev.end());

        return isKPalindrome(s, s.length(), rev, s.length()) <= 2*k;
    }
};*/

/*class Solution
{
public:
    bool isKPalindrome(string X, int k)
    {
        string Y = X;
        reverse(Y.begin(), Y.end());

        int m = X.length();
        int n = m;

        int T[m + 1][n + 1];

        for (int i = 0; i <= m; ++i)
        {
            for (int j = 0; j <= n; ++j)
            {
                if (i == 0 || j == 0) {
                    T[i][j] = i + j;
                }
                else if (X[i - 1] == Y[j - 1]) {
                    T[i][j] = T[i - 1][j - 1];
                }
                else {
                    T[i][j] = min(T[i - 1][j], T[i][j - 1]) + 1;
                }
            }
        }

        return T[m][n] <= 2*k;
    }
};*/

class Solution
{
public:
    bool isKPalindrome(string X, int k)
    {
        string Y = X;
        reverse(Y.begin(), Y.end());

        int m = X.length();
        int n = m;

        int T[m + 1][n + 1];

        for (int i = 0; i <= m; ++i)
        {
            for (int j = 0; j <= n; ++j)
            {
                if (i == 0 || j == 0) {
                    T[i][j] = 0;
                }else if (X[i - 1] == Y[j - 1]) {
                    T[i][j] = T[i - 1][j - 1] + 1;
                }else {
                    T[i][j] = max(T[i - 1][j], T[i][j - 1]);
                }
            }
        }

        // T[m][n] contains the length of LCS for 'X' and 'Y'
        // (or longest palindromic subsequence)

        // For the string to be k–palindrome, the difference between the length of
        // longest palindromic subsequence and the string should be <= k
        return (n - T[m][n] <= k);
    }
};

int main()
{
    cout << boolalpha << Solution().isKPalindrome("ABCDBA", 1) << endl;
    cout << Solution().isKPalindrome("ABCDECA", 1) << endl;

    return 0;
}
