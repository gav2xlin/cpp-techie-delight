/*

Given two strings, determine if the first string can be transformed into the second string. The only operation allowed is moving a character from the first string to the front. If the string can be transformed, find the minimum number of operations required for the transformation.

Input: X = "ADCB", Y = "ABCD"
Output: 3
Explanation: The minimum number of operations required to convert the string "ADCB" to string "ABCD" is 3.

ADCB —> CADB (Move 'C' to the front)
CADB —> BCAD (Move 'B' to the front)
BCAD —> ABCD (Move 'A' to the front)

The solution should return -1 if the string cannot be transformed.

*/

#include <iostream>
#include <string>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    bool isTransformable(string first, string second)
    {
        if (first.size() != second.size()) {
            return false;
        }

        unordered_multiset<char> chars1(first.begin(), first.end());
        unordered_multiset<char> chars2(second.begin(), second.end());

        return chars1 == chars2;
    }

    int _getMinimumOperations(string first, string second)
    {
        int count = 0;

        for (int i = first.length() - 1, j = i; i >= 0; --i, --j)
        {
            // if the current character of both strings doesn't match,
            // search for `second[j]` in substring `first[0, i-1]` and
            // increment the count for every move

            while (i >= 0 && first[i] != second[j])
            {
                --i;
                ++count;
            }
        }

        return count;
    }
public:
    int getMinimumOperations(string X, string Y)
    {
        if (isTransformable(X, Y)) {
            return _getMinimumOperations(X, Y);
        } else {
            return -1;
        }
    }
};

int main()
{
    cout << Solution().getMinimumOperations("ADCB", "ABCD") << endl;

    return 0;
}
