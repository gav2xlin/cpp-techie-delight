/*

Given a word and a pattern containing wildcard characters '*' and '?', check if the pattern matches with the complete string or not. Here, '?' can match to any single character in the string and '*' can match to any number of characters including zero characters.

Input: word = "xyxzzxy", pattern = "x***y"
Output: true

Input: word = "xyxzzxy", pattern = "x***x"
Output: false

Input: word = "xyxzzxy", pattern = "x***x?"
Output: true

Input: word = "xyxzzxy", pattern = "*"
Output: true

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    bool isMatch(string word, string pattern, int n, int m, auto &lookup)
    {
        if (m < 0 && n < 0) {
            return true;
        }
        else if (m < 0) {
            return false;
        }
        else if (n < 0)
        {
            for (int i = 0; i <= m; ++i)
            {
                if (pattern[i] != '*') {
                    return false;
                }
            }

            return true;
        }

        if (!lookup[m][n])
        {
            if (pattern[m] == '*')
            {
                // '*' matches with current characters in the input string.

                // Ignore '*' and move to the next character in the pattern
                lookup[m][n] = isMatch(word, pattern, n - 1, m, lookup) || isMatch(word, pattern, n, m - 1, lookup);
            }
            else {
                if (pattern[m] != '?' && pattern[m] != word[n])
                {
                    lookup[m][n] = 0;
                }
                else
                {
                    lookup[m][n] = isMatch(word, pattern, n - 1, m - 1, lookup);
                }
            }
        }

        return lookup[m][n];
    }
public:
    /*bool isMatch(string word, string pattern)
    {
        int n = word.length();
        int m = pattern.length();

        vector<vector<bool>> lookup(m + 1, vector<bool>(n + 1, false));

        return isMatch(word, pattern, n - 1, m - 1, lookup);
    }*/
    bool isMatch(string word, string pattern)
    {
        int n = word.length();
        int m = pattern.length();

        vector<vector<bool>> T(n+1, vector<bool>(m+1));
        T[0][0] = true;

        for (int j = 1; j <= m; ++j)
        {
            if (pattern[j-1] == '*') {
                T[0][j] = T[0][j-1];
            }
        }

        for (int i = 1; i <= n; ++i)
        {
            for (int j = 1; j <= m; ++j)
            {
                if (pattern[j-1] == '*')
                {
                    T[i][j] = T[i-1][j] || T[i][j-1];
                }
                else if (pattern[j-1] == '?' || word[i-1] == pattern[j-1])
                {
                    T[i][j] = T[i-1][j-1];
                }
            }
        }

        return T[n][m];
    }
};

int main()
{
    cout << boolalpha << Solution().isMatch("xyxzzxy", "x***y") << endl;
    cout << Solution().isMatch("xyxzzxy", "x***x") << endl;
    cout << Solution().isMatch("xyxzzxy", "x***x?") << endl;
    cout << Solution().isMatch("xyxzzxy", "*") << endl;

    return 0;
}
