/*

Given a string and a pattern, count the number of times pattern appears in the string as a subsequence.

Input: word = 'subsequence', pattern = 'sue'
Output: 7
Explanation:

'su' bs 'e' quence
'su' bsequ 'e' nce
'su' bsequenc 'e'
's' ubseq 'ue' nce
's' ubseq 'u' enc 'e'
sub 's' eq 'ue' nce
sub 's' eq 'u' enc 'e'

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    int count(string word, string pattern)
    {
        int m = word.size();
        int n = pattern.size();

        // `T[i][j]` stores number of times pattern `Y[0…j)`
        // appears in a given string `X[0…i)` as a subsequence
        int T[m + 1][n + 1];

        for (int i = 0; i <= m; ++i) {
            T[i][0] = 1;
        }

        for (int j = 1; j <= n; ++j) {
            T[0][j] = 0;
        }

        /*
          If the current character of both string and pattern matches,
            1. Exclude current character from both string and pattern
            2. Exclude only the current character from the string

          Otherwise, if the current character of the string and pattern do not match,
          exclude the current character from the string
        */

        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j) {
                T[i][j] = ((word[i - 1] == pattern[j - 1]) ? T[i - 1][j - 1] : 0) + T[i - 1][j];
            }
        }

        return T[m][n];
    }
};

int main()
{
    cout << Solution().count("subsequence", "sue") << endl;

    return 0;
}
