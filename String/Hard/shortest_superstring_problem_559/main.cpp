/*

Input: ["CATGC", "CTAAGT", "GCTA", "TTCA", "ATGCATC"]
Output: "GCTAAGTTCATGCATC"
Explanation: "GCTAAGTTCATGCATC" is the shortest possible string such that it contains every string in the input list as its substring.

GCTAAGTT [CATGC] ATC
G [CTAAGT] TCATGCATC
[GCTA] AGTTCATGCATC
GCTAAG [TTCA] TGCATC
GCTAAGTTC [ATGCATC]

If multiple superstrings of the minimum length is possible, return any one of them.

Input: ["CBC", "CC"]
Output: "CBCC" or "CCBC"

*/

#include <iostream>
#include <vector>
#include <string>
#include <climits>

using namespace std;

class Solution
{
private:
    int findOverlappingPair(string str1,
                        string str2, string &str)
    {
        int max = INT_MIN;
        int len1 = str1.length();
        int len2 = str2.length();

        for (int i = 1; i <= min(len1, len2); ++i)
        {
            if (str1.compare(len1-i, i, str2,
                                    0, i) == 0)
            {
                if (max < i)
                {
                    max = i;
                    str = str1 + str2.substr(i);
                }
            }
        }

        for (int i = 1; i <= min(len1, len2); ++i)
        {
            if (str1.compare(0, i, str2, len2-i, i) == 0)
            {
                if (max < i)
                {
                    max = i;
                    str = str2 + str1.substr(i);
                }
            }
        }

        return max;
    }
public:
    string findShortestSuperstring(vector<string> &arr)
    {
        int len = arr.size();
        while(len != 1)
        {
            int max = INT_MIN;

            int l, r;
            string resStr;

            for (int i = 0; i < len; ++i)
            {
                for (int j = i + 1; j < len; ++j)
                {
                    string str;
                    int res = findOverlappingPair(arr[i], arr[j], str);

                    if (max < res)
                    {
                        max = res;
                        resStr.assign(str);
                        l = i, r = j;
                    }
                }
            }

            --len;

            if (max == INT_MIN)
                arr[0] += arr[len];
            else
            {
                arr[l] = resStr;
                arr[r] = arr[len];
            }
        }

        return arr[0];
    }
};

int main()
{
    vector<string> words = {"CATGC", "CTAAGT", "GCTA", "TTCA", "ATGCATC"};

    cout << Solution().findShortestSuperstring(words) << endl;

    return 0;
}
