/*

Given a string, check if a repeated subsequence is present in it or not. The repeated subsequence should have a length of 2 or more.

Input: "XYBAXB"
Output: true
Explanation: String "XYBAXB" has XB(XBXB) as a repeated subsequence

Input: "XBXAXB"
Output: true
Explanation: String "XBXAXB" has XX(XXX) as a repeated subsequence

Input: "XYBYAXBY"
Output: true
Explanation: String "XYBYAXBY" has XB(XBXB), XY(XYXY), YY(YYY), YB(YBYB), and YBY(YBYBY) as repeated subsequences.

Input: "ABCA"
Output: false
Explanation: String "ABCA" doesn’t have any repeated subsequence

*/

#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    bool isPalindrome(string str, int low, int high)
    {
        if (low >= high) {
            return true;
        }

        return (str[low] == str[high]) && isPalindrome(str, low + 1, high - 1);
    }
public:
    bool hasRepeatedSubsequence(string str)
    {
        if (str.empty()) {
            return false;
        }

        unordered_map<char, int> freq;

        for (int i = 0; i < str.length(); ++i)
        {
            if (++freq[str[i]] >= 3) {
                return true;
            }
        }

        string temp;

        for (int i = 0; i < str.length(); ++i)
        {
            if (freq[str[i]] >= 2) {
                temp += str[i];
            }
        }

        return !isPalindrome(temp, 0, temp.length() - 1);
    }
};

int main()
{
    Solution s;
    cout << boolalpha << s.hasRepeatedSubsequence("XYBAXB") << endl;
    cout << s.hasRepeatedSubsequence("XBXAXB") << endl;
    cout << s.hasRepeatedSubsequence("XYBYAXBY") << endl;
    cout << s.hasRepeatedSubsequence("ABCA") << endl;

    return 0;
}
