/*

Given a string, find all possible distinct palindromic substrings in it.

Input : "google"
Output: {"e", "l", "g", "o", "oo", "goog"}

*/

#include <iostream>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    void expand(string str, int low, int high, auto &res)
    {
        while (low >= 0 && high < str.length() && str[low] == str[high])
        {
            res.insert(str.substr(low, high - low + 1));

            --low, ++high;
        }
    }
public:
    unordered_set<string> findPalindromicSubstrings(string str)
    {
        unordered_set<string> res;

        for (int i = 0; i < str.length(); ++i)
        {
            // find all odd length palindrome with `str[i]` as a midpoint
            expand(str, i, i, res);

            // find all even length palindrome with `str[i]` and `str[i+1]` as
            // its midpoints
            expand(str, i, i + 1, res);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findPalindromicSubstrings("google") << endl;

    return 0;
}
