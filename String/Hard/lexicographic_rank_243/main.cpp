/*

Given a string, calculate its rank among all its lexicographically sorted permutations.

Input : "CBA"
Output: 6
Explanation: The rank of string "DCBA" in the lexicographically sorted permutations ["ABC", "ACB", "BAC", "BCA", "CAB", "CBA"] is 6.

Input : "AABA"
Output: 2
Explanation: The rank of string "AABA" in the lexicographically sorted permutations ["AAAB", "AABA", "ABAA", "BAAA"] is 2.

Input : "DCBA"
Output: 24

*/

#include <iostream>
#include <string>
#include <algorithm>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    /*long findLexicographicRank(string key)
    {
        string str = key;
        int rank = 1;

        sort(str.begin(), str.end());

        while (true)
        {
            if (key == str) {
                return rank;
            }

            if (!next_permutation(str.begin(), str.end())) {
            //if (!prev_permutation(str.begin(), str.end())) {
                break;
            }
            ++rank;
        }

        return 0;
    }*/
    unsigned long factorial(int n)
    {
        static unordered_map<int, unsigned long> mem;
        if (mem.contains(n)) {
            return mem[n];
        }

        unsigned long fact = 1;
        for (int i = 1; i <= n; ++i) {
            fact = fact * i;
        }
        mem.insert({n, fact});

        return fact;
    }

    unsigned long findLexicographicRank(string str)
    {
        unsigned long rank = 1;

        for (int i = 0; i < str.length() - 1; ++i)
        {
            // count all smaller characters than `str[i]` to the right of `i`
            int count = 0;
            for (int j = i + 1; j < str.length(); ++j)
            {
                if (str[i] > str[j]) {
                    count++;
                }
            }

            rank += count * factorial(str.length() - 1 - i);
        }

        return rank;
    }
};

int main()
{
    cout << Solution().findLexicographicRank("CBA") << endl;
    cout << Solution().findLexicographicRank("AABA") << endl;
    cout << Solution().findLexicographicRank("DCBA") << endl;

    return 0;
}
