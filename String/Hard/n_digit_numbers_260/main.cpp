/*

Given a positive number n between 1 and 9, find all n–digit binary numbers with an equal sum of left and right half. The binary number should not start with 0 and for odd numbers, the middle element can be 0 or 1.

Input: n = 6
Output: {"100001", "100010", "101011", "110011", "100100", "101101", "101110", "110101", "110110", "111111"}

Input: n = 7
Output: {"1000001", "1001001", "1000010", "1001010", "1010011", "1011011", "1100011", "1101011", "1000100", "1001100", "1010101", "1011101", "1010110", "1011110", "1100101", "1101101", "1100110", "1101110", "1110111", "1111111"}

*/

#include <iostream>
#include <unordered_set>
#include <string>
#include <cmath>

using namespace std;

class Solution
{
private:
    void binarySeq(string left, string right, int n, int diff, unordered_set<string> &res)
    {
        if (n > 9) {
            return;
        }

        // If the number is less than n–digit, and we can cover the difference
        // with n–digits left

        if (n && (2 * abs(diff) <= n))
        {
            if (n == 1)
            {
                binarySeq(left, "0" + right, 0, diff, res);
                binarySeq(left, "1" + right, 0, diff, res);
                return;
            }

            if (left != "")
            {
                binarySeq(left + "0", right + "0", n - 2, diff, res);
                binarySeq(left + "0", right + "1", n - 2, diff - 1, res);
            }

            binarySeq(left + "1", right + "0", n - 2, diff + 1, res);
            binarySeq(left + "1", right + "1", n - 2, diff, res);
        }
        else if (n == 0 && diff == 0)
        {
            res.insert(left + right);
        }
    }
public:
    unordered_set<string> findNDigitNumbers(int n)
    {
        unordered_set<string> res;

        string left, right;
        int diff = 0;

        binarySeq(left, right, n, diff, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findNDigitNumbers(6) << endl;
    cout << Solution().findNDigitNumbers(7) << endl;

    return 0;
}
