/*

Given an array of single-digit positive integers, return all possible combinations of words formed by replacing the continuous digits with corresponding character in the English alphabet, i.e., subset [1] can be replaced by A, [2] can be replaced by B, [1, 0] can be replaced by J, [2, 1] can be replaced by U, etc.

Input: [1, 2, 2]
Output: ["ABB", "AV", "LB"]
Explanation:

[1, 2, 2] = "ABB"
[1, 22] = "AV"
[12, 2] = "LB"

Input: [1, 2, 2, 1]
Output: ["ABBA", "ABU", "AVA", "LBA", "LU"]
Explanation:

[1, 2, 2, 1] = "ABBA"
[1, 2, 21] = "ABU"
[1, 22, 1] = "AVA"
[12, 2, 1] = "LBA"
[12, 21] = "LU"

*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <memory>

using namespace std;

/*class Solution
{
private:
    void recur(vector<int> const &digits, int i, string str, vector<string> &res)
    {
        int n = digits.size();
        if (i == n)
        {
            res.push_back(str);
            return;
        }

        static string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int sum = 0;

        for (int j = i; j <= min(i + 1, n - 1); ++j)
        {
            sum = (sum * 10) + digits[j];

            // if a valid character can be formed by taking one or both digits,
            // append it to the output and recur for the remaining digits
            if (sum <= 26) {
                recur(digits, j + 1, str + alphabet[sum - 1], res);
            }
        }
    }
public:
    vector<string> findCombinations(vector<int> const &digits)
    {
        if (digits.size() == 0) {
            return {};
        }

        vector<string> res;
        recur(digits, 0, "", res);
        return res;
    }
};*/

class Solution
{
private:
    struct Node
    {
        string key;
        shared_ptr<Node> left, right;

        Node(string key)
        {
            this->key = key;
        }
    };

    void construct(shared_ptr<Node> const &root, vector<int> const &digits, int i)
    {
        int n = digits.size();
        if (root == nullptr || i == n) {
            return;
        }

        static string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        if (i + 1 < n)
        {
            int sum = 10 * digits[i] + digits[i + 1];

            if (sum <= 26) {
                root->left = make_shared<Node>(root->key + alphabet[sum - 1]);
            }

            construct(root->left, digits, i + 2);
        }

        root->right = make_shared<Node>(root->key + alphabet[digits[i] - 1]);

        construct(root->right, digits, i + 1);
    }

    void collect(shared_ptr<Node> const &node, vector<string> &res)
    {
        if (node == nullptr) {
            return;
        }

        if (node->left == nullptr && node->right == nullptr) {
            res.push_back(node->key);
        }
        else {
            collect(node->right, res);
            collect(node->left, res);
        }
    }
public:
    vector<string> findCombinations(vector<int> const &digits)
    {
        if (digits.empty()) {
            return {};
        }

        shared_ptr<Node> root = make_shared<Node>("");
        construct(root, digits, 0);

        vector<string> res;
        collect(root, res);
        return res;
    }
};

ostream& operator<<(ostream& os, const vector<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findCombinations({1, 2, 2}) << endl;
    cout << Solution().findCombinations({1, 2, 2, 1}) << endl;

    return 0;
}
