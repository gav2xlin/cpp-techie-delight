/*

Given a positive number n, return the word representaion of n. Assume that n <= 10000000000.

Input: n = 99
Output: 'Ninety Nine'

Input: n = 1000
Output: 'One Thousand'

Input: n = 751076
Output: 'Seven Lakh, Fifty One Thousand and Seventy Six'

Input: n = 1000000
Output: 'Ten Lakh'

Input: n = 2147483647
Output: 'Two Billion, Fourteen Crore, Seventy Four Lakh, Eighty Three Thousand, Six Hundred and Forty Seven'

Input: n = 10000000000
Output: 'Ten Billion'

*/

#include <iostream>
#include <string>

using namespace std;

const string EMPTY = "";

const string X[] = {EMPTY, "One ", "Two ", "Three ", "Four ", "Five ",
                    "Six ", "Seven ", "Eight ", "Nine ", "Ten ", "Eleven ",
                    "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ",
                    "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen "};

const string Y[] = {EMPTY, EMPTY, "Twenty ", "Thirty ", "Forty ", "Fifty ",
                    "Sixty ", "Seventy ", "Eighty ", "Ninety "};

class Solution
{
private:
    string convertToDigit(unsigned long long n, string suffix)
    {
        if (n == 0) {
            return EMPTY;
        }

        if (n > 19) {
            return Y[n / 10] + X[n % 10] + suffix;
        }
        else
        {
            return X[n] + suffix;
        }
    }
public:
    string convert(unsigned long long n)
    {
        string res;

        res = convertToDigit((n % 100), "");

        if (n > 100 && n % 100) {
            res = "and " + res;
        }

        res = convertToDigit(((n / 100) % 10), "Hundred ") + res;

        res = convertToDigit(((n / 1000) % 100), "Thousand, ") + res;

        res = convertToDigit(((n / 100000) % 100), "Lakh, ") + res;

        res = convertToDigit((n / 10000000) % 100, "Crore, ") + res;

        res = convertToDigit((n / 1000000000) % 100, "Billion, ") + res;

        size_t pos;
        while ((pos = res.find(", and")) != string::npos) {
            res.replace(pos, 1, "");
        }

        res.pop_back();
        if (res[res.size()-1] == ',') {
            res.pop_back();
        }

        return res;
    }
};

int main()
{
    Solution s;
    cout << s.convert(99) << endl;
    cout << s.convert(1000) << endl;
    cout << s.convert(751076) << endl;
    cout << s.convert(1000000) << endl;
    cout << s.convert(2147483647) << endl;
    cout << s.convert(10000000000) << endl;

    return 0;
}
