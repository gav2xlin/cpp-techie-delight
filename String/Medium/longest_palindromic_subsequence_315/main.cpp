/*

Given a sequence, find the longest subsequences of it that is also a palindrome.

Input: "ABBDCACB"
Output: 5
Explanation: The longest palindromic subsequence is "BCACB", having length 5.

*/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
private:
    int LCSLength(string X, string Y, int n, auto &lookup)
    {
        for (int i = 1; i <= n; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1]) {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;
                } else {
                    lookup[i][j] = max(lookup[i - 1][j], lookup[i][j - 1]);
                }
            }
        }

        return lookup[n][n];
    }

    string findLongestPalindrome(string X, string Y, int m, int n, auto &lookup)
    {
        if (m == 0 || n == 0) {
            return string("");
        }

        if (X[m - 1] == Y[n - 1])
        {
            return findLongestPalindrome(X, Y, m - 1, n - 1, lookup) + X[m - 1];
        }

        if (lookup[m - 1][n] > lookup[m][n - 1]) {
            return findLongestPalindrome(X, Y, m - 1, n, lookup);
        } else {
            return findLongestPalindrome(X, Y, m, n - 1, lookup);
        }
    }
public:
    int findLPSLength(string X)
    {
        int m = X.size();

        vector<vector<int>> lookup(m + 1, vector<int>(m + 1));

        string Y = X;
        reverse(Y.begin(), Y.end());

        LCSLength(X, Y, m, lookup);

        return findLongestPalindrome(X, Y, m, m, lookup).size();
    }
};

int main()
{
    cout << Solution().findLPSLength("ABBDCACB") << endl;

    return 0;
}
