/*

Given two sequences, find the length of the longest common subsequence (LCS) present in it. The LCS is the longest sequence which can be obtained from the first sequence by deleting some items and from the second sequence by deleting other items.

Input: X = "ABCBDAB", Y = "BDCABA"
Output: 4
Explanation: The LCS are "BDAB", "BCAB", and "BCBA", having length 4.

*/

#include <iostream>
#include <string>

using namespace std;

class Solution
{
public:
    int findLCSLength(string X, string Y)
    {
        int m = X.size(), n = Y.size();

        int lookup[m + 1][n + 1];

        for (int i = 0; i <= m; ++i) {
            lookup[i][0] = 0;
        }

        for (int j = 0; j <= n; ++j) {
            lookup[0][j] = 0;
        }

        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1]) {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;
                } else  {
                    lookup[i][j] = max(lookup[i - 1][j], lookup[i][j - 1]);
                }
            }
        }

        return lookup[m][n];
    }
};

int main()
{
    cout << Solution().findLCSLength("ABCBDAB", "BDCABA") << endl;

    return 0;
}
