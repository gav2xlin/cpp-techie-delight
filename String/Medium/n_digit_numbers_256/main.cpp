/*

Given a positive number n between 1 and 9, find all n–digit strictly increasing numbers. A number is strictly increasing if every digit is greater than its preceding digit.

Input: n = 8
Output: {12345678, 12345679, 12345689, 12345789, 12346789, 12356789, 12456789, 13456789, 23456789}

Input: n = 7
Output: {1234567, 1234568, 1234569, 1234578, 1234579, 1234589, 1234678, 1234679, 1234689, 1234789, 1235678, 1235679, 1235689, 1235789, 1236789, 1245678, 1245679, 1245689, 1245789, 1246789, 1256789, 1345678, 1345679, 1345689, 1345789, 1346789, 1356789, 1456789, 2345678, 2345679, 2345689, 2345789, 2346789, 2356789, 2456789, 3456789}

*/

#include <iostream>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    //void findStrictlyIncreasingNumbers(string result, int n, char prev, unordered_set<int>& res)
    void findStrictlyIncreasingNumbers(string result, int n, char curr, unordered_set<int>& res)
    {
        if (n)
        {
            /*for (char ch = prev + 1; ch <= '9'; ++ch) {
                findStrictlyIncreasingNumbers(result + ch, n - 1, ch, res);
            }*/
            for (char ch = curr; ch >= '1'; --ch) {
                findStrictlyIncreasingNumbers(ch + result, n - 1, ch - 1, res);
            }
        } else {
            res.insert(stoi(result));
        }
    }
public:
    unordered_set<int> findStrictlyIncreasingNumbers(int n)
    {
        unordered_set<int> res;
        //findStrictlyIncreasingNumbers("", n, '0', res);
        findStrictlyIncreasingNumbers("", n, '9', res);
        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findStrictlyIncreasingNumbers(8) << endl;
    cout << Solution().findStrictlyIncreasingNumbers(7) << endl;

    return 0;
}
