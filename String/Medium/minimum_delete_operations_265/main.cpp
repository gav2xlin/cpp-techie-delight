/*

Given a string, find the minimum number of deletions required to convert it into a palindrome.

Input: s = "ACBCDBAA"
Output: 3
Explanation: The minimum number of deletions required to convert "ACBCDBAA" into a palindrome string "ABCBA" is 3.

*/

#include <iostream>
#include <string>
#include <unordered_map>
#include <algorithm>

using namespace std;

class Solution
{
private:
    int minDeletions(string X, int i, int j, auto &lookup)
    {
        if (i >= j) {
            return 0;
        }

        string key = to_string(i) + "|" + to_string(j);

        if (lookup.find(key) == lookup.end())
        {
            if (X[i] == X[j]) {
                lookup[key] = minDeletions(X, i + 1, j - 1, lookup);
            } else {
                lookup[key] = 1 + min(minDeletions(X, i, j - 1, lookup), minDeletions(X, i + 1, j, lookup));
            }
        }

        return lookup[key];
    }
public:
    int findMinimumDeletions(string X)
    {
        int n = X.length();
        unordered_map<string, int> lookup;
        return minDeletions(X, 0, n - 1, lookup);
    }
};

int main()
{
    cout << Solution().findMinimumDeletions("ACBCDBAA") << endl;

    return 0;
}
