/*

Given two strings, find all substrings of the first string that are anagrams of the second string and return the list of their indices.

Two words, X and Y, are anagrams if by rearranging the letters of X, we can get Y using all the original letters of X exactly once.

Input: X = "XYYZXZYZXXYZ", Y = "XYZ"
Output: [2, 4, 6, 9]

Explanation:

Anagram "YZX" present at index 2
Anagram "XZY" present at index 4
Anagram "YZX" present at index 6
Anagram "XYZ" present at index 9

Note: The solution should return the indices in ascending order.

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <algorithm>

using namespace std;

class Solution
{
public:
    vector<int> findAllAnagrams(string X, string Y)
    {
        /*int m, n;
        if ((m = Y.size()) > (n = X.size())) {
            return {};
        }

        unordered_multiset<char> window;
        unordered_multiset<char> set;

        for (int i = 0; i < m; ++i) {
            set.insert(Y[i]);
        }

        vector<int> res;
        for (int i = 0; i < n; ++i)
        {
            if (i < m) {
                window.insert(X[i]);
            } else {
                if (window == set)
                {
                    res.push_back(i - m);
                }

                auto itr = window.find(X[i - m]);
                if (itr != window.end()) {
                    window.erase(itr);
                }

                window.insert(X[i]);
            }
        }

        if (window == set)
        {
            res.push_back(n - m);
        }

        return res;*/
        int m, n;

        if ((m = Y.length()) > (n = X.length())) {
            return {};
        }

        vector<int> res;
        for (int i = 0; i <= n - m; ++i)
        {
            if (is_permutation(X.begin() + i, X.begin() + i + m, Y.begin()))
            {
                res.push_back(i);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findAllAnagrams("XYYZXZYZXXYZ", "XYZ") << endl;

    return 0;
}
