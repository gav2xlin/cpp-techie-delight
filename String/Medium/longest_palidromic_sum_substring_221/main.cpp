/*

Given a string, find the length of the longest contiguous substring, such that the length of the substring is `2×n` digits and the sum of the leftmost `n` digits is equal to the sum of the rightmost `n` digits. If there is no such substring, return 0.

Input : 13267224
Output: 6
Explanation: 326722 = (3 + 2 + 6) = (7 + 2 + 2) = 11

Input : 546374
Output: 4
Explanation: 4637 = (4 + 6) = (3 + 7) = 10

*/

#include <iostream>
#include <string>

using namespace std;

class Solution
{
private:
    void expand(string str, int low, int high, int &max)
    {
        int len = str.size();
        int leftsum = 0, rightsum = 0;

        while (low >= 0 && high < len)
        {
            leftsum += str[low] - '0';
            rightsum += str[high] - '0';

            if (leftsum == rightsum && (high - low + 1) > max) {
                max = high - low + 1;
            }

            --low, ++high;
        }
    }
public:
    int findLongestPalindrome(string str)
    {
        int max = 0, n = str.size();

        for (int i = 0; i < n - 1; ++i) {
            expand(str, i, i + 1, max);
        }

        return max;
    }
};

int main()
{
    cout << Solution().findLongestPalindrome("13267224") << endl;
    cout << Solution().findLongestPalindrome("546374") << endl;

    return 0;
}
