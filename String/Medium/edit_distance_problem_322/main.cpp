/*

Given two words, find the minimum number of single-character edits required to transform one word into the other. An edit can be insertion, deletion, or substitution and each edit carries a unit cost.

Input: X = "kitten", Y = "sitting"
Output: 3
Explanation: A minimal edit script that transforms the 'kitten' into 'sitting' is:

kitten —> sitten (substitution of 's' for 'k')
sitten —> sittin (substitution of 'i' for 'e')
sittin —> sitting (insertion of 'g' at the end)

*/

#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMinDistance(string X, string Y)
    {
        int m = X.size();
        int n = Y.size();

        int T[m + 1][n + 1];
        memset(T, 0, sizeof T);

        for (int i = 1; i <= m; ++i) {
            T[i][0] = i;
        }

        for (int j = 1; j <= n; ++j) {
            T[0][j] = j;
        }

        int substitutionCost;
        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1]) {
                    substitutionCost = 0;
                }
                else {
                    substitutionCost = 1;
                }
                T[i][j] = min(min(T[i - 1][j] + 1, T[i][j - 1] + 1), T[i - 1][j - 1] + substitutionCost);
            }
        }

        return T[m][n];
    }
};

int main()
{
    cout << Solution().findMinDistance("kitten", "sitting") << endl;

    return 0;
}
