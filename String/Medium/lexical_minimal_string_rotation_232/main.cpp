/*

The lexicographically minimal string rotation (or lexicographically least circular substring) is the problem of finding a string's rotation possessing the lowest lexicographical order among all possible rotations.

Input : "bbaaccaadd"
Output: "aaccaaddbb"

Note that a string can have multiple lexicographically minimal rotations, but this doesn't matter – rotations must be equivalent.

*/

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
public:
    string findLexicalMinRotation(string str)
    {
        string min = str;

        for (int i = 0; i < str.length(); i++)
        {
            rotate(str.begin(), str.begin() + 1, str.end());

            if (str.compare(min) < 0) {
                min = str;
            }
        }

        return min;
    }
};

int main()
{
    cout << Solution().findLexicalMinRotation("bbaaccaadd") << endl;

    return 0;
}
