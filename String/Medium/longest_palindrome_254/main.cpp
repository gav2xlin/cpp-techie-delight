/*

Given a string, find the length of the longest palindrome which can be constructed by shuffling or deleting characters in the string.

Input: "ABBDAB"
Output: 6
Explanation: The longest palindrome is "BABAB" (or "BADAB" or "ABBBA" or "ABDBA")

Input: "ABCDD"
Output: 5
Explanation: The longest palindrome is "DAD" (or "DBD" or "DCD")

*/

#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    int findLongestPalindrome(string str)
    {
        unordered_map<char, int> freq;
        for (char ch: str) {
            freq[ch]++;
        }

        string mid_char, left;

        for (auto p: freq)
        {
            char ch = p.first;
            int count = p.second;

            if (count & 1) {
                mid_char = ch;
            }

            left.append(count / 2, ch);
        }

        string right(left.rbegin(), left.rend());

        return (left + mid_char + right).size();
    }
};

int main()
{
    cout << Solution().findLongestPalindrome("ABBDAB") << endl;
    cout << Solution().findLongestPalindrome("ABCDD") << endl;

    return 0;
}
