/*

Check if a given set of moves is circular or not. A move is circular if its starting and ending coordinates are the same. The moves can contain instructions to move one unit in the same direction (M), to change direction to the left of current direction (L), and to change direction to the right of current direction (R).

Input: "MRMRMRM"
Output: true

Input: "MRMLMRMRMMRMM"
Output: true

Assume that the initial direction is North.

*/

#include <iostream>
#include <string>

using namespace std;

class Solution
{
public:
    bool isCircularMove(string str)
    {
        int x = 0, y = 0;

        char dir = 'N';

        for (int i = 0; i < str.length(); ++i)
        {
            switch (str[i])
            {
                case 'M':
                    if (dir == 'N') {
                        ++y;
                    } else if (dir == 'S') {
                        --y;
                    } else if (dir == 'E') {
                        ++x;
                    } else if (dir == 'W') {
                        --x;
                    }
                    break;
                case 'L':
                    if (dir == 'N') {
                        dir = 'W';
                    } else if (dir == 'W') {
                        dir = 'S';
                    } else if (dir == 'S') {
                        dir = 'E';
                    } else if (dir == 'E') {
                        dir = 'N';
                    }
                    break;
                case 'R':
                    if (dir == 'N') {
                        dir = 'E';
                    } else if (dir == 'E') {
                        dir = 'S';
                    } else if (dir == 'S') {
                        dir = 'W';
                    } else if (dir == 'W') {
                        dir = 'N';
                    }
            }
        }

        return (!x && !y);
    }
};

int main()
{
    Solution s;

    cout << boolalpha << s.isCircularMove("MRMRMRM") << endl;
    cout << s.isCircularMove("MRMLMRMRMMRMM") << endl;

    return 0;
}
