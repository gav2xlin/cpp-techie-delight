cmake_minimum_required(VERSION 3.5)

project(remove_adjacent_duplicates_240 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(remove_adjacent_duplicates_240 main.cpp)

install(TARGETS remove_adjacent_duplicates_240
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
