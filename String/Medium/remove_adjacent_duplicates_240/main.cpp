/*

Given a string, remove all adjacent duplicates from it. The solution should continue removing adjacent duplicates from the string till no duplicate is present in the result.

Input : "DBAABDAB"
Output: "AB"
Explanation: "DBAABDAB" —> "DBBDAB" —> "DDAB" —> "AB"

Input : "ABDAADBDAABB"
Output: "AD"
Explanation: "ABDAADBDAABB" —> "ABDDBD" —> "ABBD" —> "AD"

Input : "ABADB"
Output: "ABADB"

*/

#include <iostream>
#include <string>
#include <stack>

using namespace std;

class Solution
{
public:
    string removeAdjacentDuplicates(string str)
    {
        /*int slow = 0;
        for (int fast = 0; fast < str.size(); ++ fast) {
            if ((slow == 0) || str[slow - 1] != str[fast]) {
                str[slow++] = str[fast];
            } else {
                slow--;
            }
        }
        return str.substr(0, slow);*/

        stack<char> st;
        for (const auto &c: str) {
            if (st.empty()) {
                st.push(c);
            } else if (st.top() == c) {
                st.pop();
            } else {
                st.push(c);
            }
        }

        string s = "";
        while (!st.empty()) {
            s = st.top() + s;
            st.pop();
        }
        return s;

        /*for (int i = 1; i < str.size(); ++i) {
            if (str[i] == str[i - 1]) {
                return removeAdjacentDuplicates(str.substr(0, i - 1) + str.substr(i + 1));
            }
        }
        return str;*/
    }
};

int main()
{
    cout << Solution().removeAdjacentDuplicates("DBAABDAB") << endl;
    cout << Solution().removeAdjacentDuplicates("ABDAADBDAABB") << endl;
    cout << Solution().removeAdjacentDuplicates("ABADB") << endl;

    cout << Solution().removeAdjacentDuplicates("MISSISSIPIE") << endl; // MPIE != MIPIE

    return 0;
}
