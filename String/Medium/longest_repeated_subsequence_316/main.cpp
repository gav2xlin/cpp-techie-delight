/*

Given a sequence, find the length of the longest subsequence of it that occurs at least twice where the repeated characters should holds a different index in the sequence.

Input: 'ATACTCGGA'
Output: 4
Explanation: The longest repeating subsequence is 'ATCG', having length 4.

'A' 'T'  A  'C'  T   C  'G'  G   A
 A   T  'A'  C  'T' 'C'  G  'G'  A

Input: 'YBYXBXBB'
Output: 5
Explanation: The longest repeating subsequence is 'YBXBB', having length 5.

'Y' 'B'  Y  'X' 'B'  X  'B'  B
 Y   B  'Y'  X  'B' 'X' 'B' 'B'

*/


#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
private:
    void LRSLength(string X, int n, auto &lookup)
    {
        for (int i = 1; i <= n; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == X[j - 1] && i != j) {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;
                } else {
                    lookup[i][j] = max(lookup[i - 1][j], lookup[i][j - 1]);
                }
            }
        }
    }

    string LRS(string X, int m, int n, auto &lookup)
    {
        if (m == 0 || n == 0) {
            return string("");
        }

        if (X[m - 1] == X[n - 1] && m != n) {
            return LRS(X, m - 1, n - 1, lookup) + X[m - 1];
        } else {
            if (lookup[m - 1][n] > lookup[m][n - 1]) {
                return LRS(X, m - 1, n, lookup);
            }
            else {
                return LRS(X, m, n - 1, lookup);
            }
        }
    }
public:
    int findLRSLength(string X)
    {
        int n = X.size();
        vector<vector<int>> lookup(n + 1, vector<int>(n + 1));

        LRSLength(X, n, lookup);

        return LRS(X, n, n, lookup).size();
    }
};

int main()
{
    cout << Solution().findLRSLength("ATACTCGGA") << endl;
    cout << Solution().findLRSLength("YBYXBXBB") << endl;

    return 0;
}
