/*

Given an expression consisting of an opening brace '{' and a closing brace '}', find the minimum number of inversions needed to balance the expression.

Input : "{{}{{}{{"
Output: 2
Explanation: Minimum number of inversions needed is 2, as shown below:

{{}{{}{{ ——> {{}{{}}{ ——> {{}{{}}}


Input : "{{{{{{"
Output: 3
Explanation: Minimum number of inversions needed is 3, as shown below:

{{{{{{ ——> {{{}{{ ——> {{{}}{ ——> {{{}}}


The solution should return -1 on invalid input.

*/

#include <iostream>
#include <string>

using namespace std;

class Solution
{
public:
    int findMinimumInversions(string exp)
    {
        if (exp.length() & 1) {
            return -1;
        }

        int inversions = 0;
        int open = 0;

        for (int i = 0; i < exp.length(); ++i)
        {
            if (exp[i] == '{') {
                ++open;
            } else {
                if (open) {
                    --open;
                } else {
                    ++inversions;
                    open = 1;
                }
            }
        }

        // for `n` opened braces, exactly `n/2` inversions are needed
        return inversions + open / 2;
    }
};

int main()
{
    cout << Solution().findMinimumInversions("{{}{{}{{") << endl;
    cout << Solution().findMinimumInversions("{{{{{{") << endl;

    return 0;
}
