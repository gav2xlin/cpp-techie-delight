/*

Given three strings, check if the third string is interleaving the first and second strings. In other words, check if third string is formed from all characters of the first and second string, and the order of characters is preserved.

Input: X = "AB", Y = "CD", S = "ACDB"
Output: true

Input: X = "ABC", Y = "DEF", S = "ADEBCF"
Output: true

Input: X = "ABC", Y = "ACD", S = "ACDABC"
Output: true

Input: X = "ABC", Y = "DE", S = "ACBCD"
Output: false

*/

#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    bool isInterleaving(string X, string Y, string S, auto &T)
    {
        if (!X.length() && !Y.length() && !S.length()) {
            return true;
        }

        if (!S.length()) {
            return false;
        }

        string key = (X + "|" + Y + "|" + S);

        if (T.find(key) == T.end())
        {
            bool x = (X.length() && S[0] == X[0]) && isInterleaving(X.substr(1), Y, S.substr(1), T);

            bool y = (Y.length() && S[0] == Y[0]) && isInterleaving(X, Y.substr(1), S.substr(1), T);

            T[key] = x || y;
        }

        return T[key];
    }
public:
    bool isInterleaving(string X, string Y, string S)
    {
        unordered_map<string, bool> T;
        return isInterleaving(X, Y, S, T);
    }
};

int main()
{
    Solution s;

    cout << boolalpha << s.isInterleaving("AB", "CD", "ACDB") << endl;
    cout << s.isInterleaving("ABC", "DEF", "ADEBCF") << endl;
    cout << s.isInterleaving("ABC", "ACD", "ACDABC") << endl;
    cout << s.isInterleaving("ABC", "DE", "ACBCD") << endl;

    return 0;
}
