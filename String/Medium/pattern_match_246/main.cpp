/*

Given a list of words and a pattern, find all words in the list that follows the same order of characters as that of the pattern.

Input:

words = [leet, abcd, loot, geek, cool, for, peer, dear, seed, meet, noon, otto, mess, loss]
pattern = moon

Output: {leet, loot, geek, cool, peer, seed, meet}

Explanation: Pattern is 4 digits with distinct character at first and last index, and same character at 1st and 2nd index


Input:

words = [leet, abcd, loot, geek, cool, for, peer, dear, seed, meet, noon, otto, mess, loss]
pattern = pqrs

Output: {abcd, dear}

Explanation: Pattern is 4 digits and has all distinct characters

*/

#include <iostream>
#include <unordered_set>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    unordered_set<string> patternMatch(unordered_set<string> const &words, string pattern)
    {
        int n = words.size();
        if (n == 0) {
            return {};
        }

        unordered_set<string> res;
        int len = pattern.size();

        for (auto& word : words)
        {
            unordered_map<char, char> map1, map2;

            if (word.size() == len)
            {
                int i;
                for (i = 0; i < len; ++i)
                {
                    char w = word[i], p = pattern[i];

                    // Check mapping from the current word to the given pattern
                    if (map1.find(w) == map1.end()) {
                        map1[w] = p;
                    } else if (map1[w] != p) {
                        break;
                    }

                    // Check mapping from the given pattern to the current word
                    if (map2.find(p) == map2.end()) {
                        map2[p] = w;
                    } else if (map2[p] != w) {
                        break;
                    }
                }

                if (i == len) {
                    res.insert(word);
                }
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    unordered_set<string> words {"leet", "abcd", "loot", "geek", "cool", "for", "peer", "dear", "seed", "meet", "noon", "otto", "mess", "loss"};

    cout << Solution().patternMatch(words, "moon") << endl;

    return 0;
}
