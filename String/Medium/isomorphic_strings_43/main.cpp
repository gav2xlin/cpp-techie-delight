/*

Given two strings, check if they are isomorphic. Two strings, X and Y, are called isomorphic if all occurrences of each character in X can be replaced with another character to get Y and vice-versa.

Note that mapping from a character to itself is allowed, but the same character may not replace two characters.

Input: X = "ACAB", Y = "XCXY"
Output: true
Explanation: The strings "ACAB" and "XCXY" are isomorphic as we can map 'A' to 'X', 'B' to 'Y', and 'C' to 'C'.

Input: X = "xbxb", Y = "Ybyb"
Output: false

*/

#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    bool isIsomorphic(string X, string Y)
    {
        if (X.length() != Y.length()) {
            return false;
        }

        unordered_map<char, char> map;
        unordered_set<char> set;

        for (int i = 0; i < X.length(); ++i)
        {
            char x = X[i], y = Y[i];

            if (map.find(x) != map.end())
            {
                if (map[x] != y) {
                    return false;
                }
            } else {
                if (set.find(y) != set.end()) {
                    return false;
                }

                map[x] = y;
                set.insert(y);
            }
        }

        return true;
    }
};

int main()
{
    cout << boolalpha << Solution().isIsomorphic("ACAB", "XCXY") << endl;
    cout << Solution().isIsomorphic("xbxb", "Ybyb") << endl;

    return 0;
}
