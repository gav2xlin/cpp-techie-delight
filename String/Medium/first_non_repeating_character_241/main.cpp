/*

Given a string, find the index of the first non-repeating character in it by doing only a single traversal of the string.

Input : "ABCDBAGHC"
Output: 3

The solution should return -1 if the string has all characters repeating.

Input : "AAA"
Output: -1

*/


#include <iostream>
#include <string>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    int findFirstNonRepeating(string str)
    {
        unordered_map<char, pair<int, int>> map;

        for (int i = 0; i < str.length(); ++i)
        {
            map[str[i]].first++;
            map[str[i]].second = i;
        }

        int min_index = -1;

        for (auto it: map)
        {
            auto [count, firstIndex] = it.second;

            if (count == 1 && (min_index == -1 || firstIndex < min_index)) {
                min_index = firstIndex;
            }
        }

        return min_index;
    }
};

int main()
{
    cout << Solution().findFirstNonRepeating("ABCDBAGHC") << endl;
    cout << Solution().findFirstNonRepeating("AAA") << endl;

    return 0;
}
