/*

Given a positive integer n, find all n–digit binary numbers having more 1’s than 0’s for any prefix of the number.

Input : n = 4
Output: {"1111", "1110", "1101", "1100", "1011", "1010"}

Note that 1001 will not form part of the solution as it violates the problem constraints (1001 has 2 zeros and 1 one at third position). The same applies to all other 4–digit binary numbers.

*/

#include <iostream>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    void find(string str, int n, int zeros, int ones, unordered_set<string>& res)
    {
        if (ones < zeros) {
            return;
        }

        if (n == 0)
        {
            res.insert(str);
            return;
        }

        find(str + "1", n - 1, zeros, ones + 1, res);

        find(str + "0", n - 1, zeros + 1, ones, res);
    }

    void recur(string currentNumber, int extraOnes, int remainingPlaces, unordered_set<string>& res)
    {
        if (0 == remainingPlaces)
        {
            res.insert(currentNumber);
            return;
        }

        recur(currentNumber + "1", extraOnes + 1, remainingPlaces - 1, res);

        if (0 < extraOnes) {
            recur(currentNumber + "0", extraOnes - 1, remainingPlaces - 1, res);
        }
    }
public:
    unordered_set<string> findNDigitNumbers(int n)
    {
        unordered_set<string> res;
        //find("", n, 0, 0, res);
        recur("", 0, n, res);
        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& nums) {
    for (auto& v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findNDigitNumbers(4) << endl;

    return 0;
}
