/*

Given a positive number n, find all strings of length n containing balanced parentheses.

Input : n = 4
Output: {"(())", "()()"}

Input : n = 6
Output: {"((()))", "(()())", "(())()", "()(())", "()()()"}

Input : n = 5
Output: {}
Explanation: Since n is odd, balanced parentheses cannot be formed.

*/

#include <iostream>
#include <string>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    void balParenthesis(unordered_set<string>& strings, int n, string str, int open)
    {
        if ((n & 1) && !open) {
            return;
        }

        if (n == 0)
        {
            if (open == 0) {
                strings.insert(str);
            }
            return;
        }

        if (open > n) {
            return;
        }

        balParenthesis(strings, n - 1, str + "(", open + 1);

        if (open > 0) {
            balParenthesis(strings, n - 1, str + ")", open - 1);
        }
    }
public:
    unordered_set<string> findPalindromicPermutations(int n)
    {
        unordered_set<string> strings;
        balParenthesis(strings, n, "", 0);
        return strings;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& strings) {
    for (auto& s : strings) {
        os << s << ' ';
    }
    return os;
}

int main()
{
    Solution s;

    cout << s.findPalindromicPermutations(4) << endl;
    cout << s.findPalindromicPermutations(6) << endl;
    cout << s.findPalindromicPermutations(5) << endl;

    return 0;
}
