/*

Given a string, find the longest substring containing distinct characters.

Input : "longestsubstr"
Output: "longest"

Input : "abbcdafeegh"
Output: "bcdafe"

Input : "aaaaaa"
Output: "a"

The longest distinct character substring is not guaranteed to be unique. If multiple longest distinct substring exists, the solution should return the one which appear first in the string.

Input: "cbabc"
Output: "cba"
Explanation: There are two longest distinct substrings in "cbaabc" of length 3, namely, "cba" and "abc". The solution returns "cba" as it appears before "abc" in the string.

*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

constexpr int CHAR_RANGE = 128;

class Solution
{
public:
    string findLongestSubstring(string str)
    {
        vector<bool> window(CHAR_RANGE);

        int begin = 0, end = 0;

        int n = str.size();
        for (int low = 0, high = 0; high < n; ++high)
        {
            if (window[str[high]])
            {
                while (str[low] != str[high]) {
                    window[str[low++]] = false;
                }

                ++low;
            }
            else {
                window[str[high]] = true;

                if (end - begin < high - low)
                {
                    begin = low;
                    end = high;
                }
            }
        }

        return str.substr(begin, end - begin + 1);
    }
};

int main()
{
    Solution s;

    cout << s.findLongestSubstring("longestsubstr") << endl;
    cout << s.findLongestSubstring("abbcdafeegh") << endl;
    cout << s.findLongestSubstring("aaaaaa") << endl;
    cout << s.findLongestSubstring("cbabc") << endl;

    return 0;
}
