/*

Given a string and a positive integer `k`, print the string in `k` rows in the zigzag form.

Input:

str = "THISPROBLEMISAWESOME"
k = 3

Output: "TPLSSHSRBEIAEOEIOMWM"

Explanation:

(Row 1)		T				P				L				S				S
(Row 2)			H		S		R		B		E		I		A		E		O		E
(Row 3)				I				O				M				W				M


Note: If `k` is more than length of the string, return the whole string.

*/

#include <iostream>
#include <sstream>

using namespace std;

class Solution
{
public:
    string printZigZag(string str, int k)
    {
        if (k == 0) {
            return "";
        }

        if (k == 1)
        {
            return str;
        }

        ostringstream os;
        for (int i = 0; i < str.length(); i += (k - 1) * 2) {
            os << str[i];
        }

        for (int j = 1; j < k - 1; ++j)
        {
            bool down = true;

            for (int i = j; i < str.size();)
            {
                os << str[i];

                if (down) {
                    i += (k - j - 1) * 2;
                }
                else {
                    i += (k - 1) * 2 - (k - j - 1) * 2;
                }

                down = !down;
            }
        }

        for (int i = k - 1; i < str.size(); i += (k - 1) * 2) {
            os << str[i];
        }

        return os.str();
    }
};

int main()
{
    cout << Solution().printZigZag("THISPROBLEMISAWESOME", 3) << endl;

    return 0;
}
