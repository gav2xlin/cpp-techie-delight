/*

Given a string, return all combinations of non-overlapping substrings of it.

Input : "ABC"
Output: {["A", "B", "C"], ["A", "BC"], ["AB", "C"], ["ABC"]}

Input : "ABCD"
Output: {["A", "B", "C", "D"], ["A", "B", "CD"], ["A", "BC", "D"], ["A", "BCD"], ["AB", "C", "D"], ["AB", "CD"], ["ABC", "D"], ["ABCD"]}

*/

#include <iostream>
#include <vector>
#include <set>
#include <string>

using namespace std;

class Solution
{
private:
    void findCombinations(string str, vector<string> &substring, set<vector<string>> &combinations)
    {
        if (str.empty())
        {
            vector<string> output(substring);
            combinations.insert(output);

            return;
        }

        for (int i = 0; i < str.size(); ++i)
        {
            substring.push_back(str.substr(0, i + 1));

            findCombinations(str.substr(i + 1), substring, combinations);

            substring.pop_back();
        }
    }
public:
    set<vector<string>> findCombinations(string s)
    {
        if (s.empty()) {
            return {};
        }

        set<vector<string>> combinations;
        vector<string> substring;

        findCombinations(s, substring, combinations);

        return combinations;
    }
};

ostream& operator<<(ostream& os, const vector<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<string>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findCombinations("ABC") << endl;
    cout << Solution().findCombinations("ABCD") << endl;

    return 0;
}
