/*

Given a string and a pattern, determine whether the string characters follow the specific order defined by the pattern’s characters. You may assume that the pattern contains all distinct characters.

Input: word = "Techie Delight", pattern = "el"
Output: true
Explanation: The pattern characters follow the order [e, e, e, l] in the input string. All e’s appear before 'l'.

Input: word = "Techie Delight", pattern = "ei"
Output: false
Explanation: The pattern characters follow the order [e, i, e, e, i] in the input string. All e’s doesn’t appear before all i’s.

*/

#include <iostream>
#include <string>

using namespace std;

class Solution
{
public:
    bool checkPattern(string word, string pattern)
    {
        if (word.length() < pattern.length()) {
            return false;
        }

        char prev = '\0';

        for (char curr: pattern)
        {
            int last_of = word.find_last_of(prev);
            int first_of = word.find_first_of(curr);

            if (prev != '\0' && (first_of == string::npos || first_of == string::npos || last_of > first_of)) {
                return false;
            }

            prev = curr;
        }

        return true;
    }
};

int main()
{
    cout << boolalpha << Solution().checkPattern("Techie Delight", "el") << endl;
    cout << boolalpha << Solution().checkPattern("Techie Delight", "ei") << endl;

    return 0;
}
