/*

Given a string, check if it is a rotated palindrome or not.

Input: "CBAABCD"
Output: true
Explanation: "CBAABCD" is a rotation of the palindrome "ABCDCBA"

Input: "BAABCC"
Output: true
Explanation: "BAABCC" is a rotation of the palindrome "ABCCBA"

Input: "DCABC"
Output: false

*/

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
private:
    bool isPalindrome(string str, int low, int high)
    {
        return (low >= high) || (str[low] == str[high] && isPalindrome(str, low + 1, high - 1));
    }
public:
    bool isRotatedPalindrome(string str)
    {
        int n = str.size();

        for (int i = 0; i < n; ++i)
        {
            rotate(str.begin(), str.begin() + 1, str.end());
            // rotate(str.rbegin(), str.rbegin() + 1, str.rend());

            if (isPalindrome(str, 0, n - 1)) {
                return true;
            }
        }

        return false;
    }
};

int main()
{
    Solution s;
    cout << boolalpha << s.isRotatedPalindrome("CBAABCD") << endl;
    cout << s.isRotatedPalindrome("BAABCC") << endl;
    cout << s.isRotatedPalindrome("DCABC") << endl;

    return 0;
}
