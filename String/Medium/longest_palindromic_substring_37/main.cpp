/*

Given a string, find the maximum length contiguous substring of it that is also a palindrome.

Input: "bananas"
Output: "anana"

Input: "abdcbcdbdcbbc"
Output: "bdcbcdb"

The longest palindromic substring is not guaranteed to be unique. If multiple longest palindromic substring exists, the solution should return the one which appear first in the string.

Input: "abracadabra"
Output: "aca"
Explanation: There is no palindromic substring in a string "abracadabra" with a length greater than 3. There are two palindromic substrings with length 3, namely, "aca" and "ada". The solution returns "aca" as it appears before "ada" in the string.

Input: "dcabc"
Output: "d"

*/

// https://en.wikipedia.org/wiki/Longest_palindromic_substring#Manacher.27s_algorithm

#include <iostream>
#include <string>

using namespace std;

class Solution
{
private:
    string expand(string str, int low, int high)
    {
        while (low >= 0 && high < str.length() && (str[low] == str[high])) {
            low--, high++;
        }

        return str.substr(low + 1, high - low - 1);
    }
public:
    string longestPalindromicSubstring(string str)
    {
        if (str.empty()) {
            return str;
        }

        string max_str = "", curr_str;
        int max_length = 0, curr_length;

        for (int i = 0; i < str.length(); ++i)
        {
            curr_str = expand(str, i, i);
            curr_length = curr_str.size();

            if (curr_length > max_length)
            {
                max_length = curr_length;
                max_str = curr_str;
            }

            curr_str = expand(str, i, i + 1);
            curr_length = curr_str.size();

            if (curr_length > max_length)
            {
                max_length = curr_length;
                max_str = curr_str;
            }
        }

        return max_str;
    }
};

int main()
{
    Solution s;

    cout << s.longestPalindromicSubstring("bananas") << endl;
    cout << s.longestPalindromicSubstring("abdcbcdbdcbbc") << endl;
    cout << s.longestPalindromicSubstring("abracadabra") << endl;
    cout << s.longestPalindromicSubstring("dcabc") << endl;

    return 0;
}
