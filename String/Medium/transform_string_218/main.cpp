/*

Given two strings, determine whether the first string can be transformed into the second string with a single edit operation. An edit operation can insert, remove, or replace a character in the first string.

Input : X = "xyz", Y = "xz"
Output: true
Explanation: The total number of edits required is 1 (remove 'y' from the first string)

Input : X = "xyz", Y = "xyyz"
Output: true
Explanation: The total number of edits required is 1 (add 'y' in the first string)

Input : X = "xyz", Y = "xyx"
Output: true
Explanation: The total number of edits required is 1 (replace 'z' in the first string by 'x')

Input : X = "xyz", Y = "xxx"
Output: false
Explanation: The total number of edits required are 2 (replace 'y' and 'z' in the first string by 'x')

Input : X = "xyz", Y = "xyz"
Output: false
Explanation: The total number of edits required is 0 (both strings are the same)

*/

#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class Solution
{
public:
    bool isTransformable(string X, string Y)
    {
        int m = X.length();
        int n = Y.length();

        if (abs(m - n) > 1) {
            return false;
        }

        int edits = 0;

        int i = 0, j = 0;

        while (i < m && j < n)
        {
            if (X[i] != Y[j])
            {
                if (m > n) {
                    ++i;
                } else if (m < n) {
                    ++j;
                } else {
                    i++, j++;
                }

                ++edits;
            } else {
                ++i, ++j;
            }
        }

        if (i < m) {
            ++edits;
        }

        if (j < n) {
            ++edits;
        }

        return (edits == 1);
    }
};

int main()
{
    Solution s;

    cout << boolalpha << s.isTransformable("xyz", "xz") << endl;
    cout << boolalpha << s.isTransformable("xyz", "xyyz") << endl;
    cout << boolalpha << s.isTransformable("xyz", "xyx") << endl;
    cout << boolalpha << s.isTransformable("xyz", "xxx") << endl;
    cout << boolalpha << s.isTransformable("xyz", "xyz") << endl;

    return 0;
}
