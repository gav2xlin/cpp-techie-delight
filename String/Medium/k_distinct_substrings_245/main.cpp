/*

Given a string and a positive integer k, find all distinct substrings of any length containing exactly k distinct characters.

Input:  str = "abcbd", k = 3
Output: {"abc", "abcb", "bcbd", "cbd"}

Input:  str = "abcadce", k = 4
Output: {"abcad", "abcadc", "bcad", "bcadc", "cadce", "adce"}

Input:  str = "aa", k = 1
Output: {"a", "aa"}

*/

#include <iostream>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    unordered_set<string> findDistinctSubstrings(string str, int k)
    {
        unordered_set<string> result;

        for (int i = 0; i < str.length() - k + 1; ++i)
        {
            unordered_set<char> chars;

            for (int j = i; j < str.length() && chars.size() <= k; ++j)
            {
                chars.insert(str[j]);

                /*
                    If current character `str[j]` is seen before in the
                    substring `str[i…j-1]`, the count remains the same since
                    the hash set only allows unique values
                */

                if (chars.size() == k)
                {
                    result.insert(str.substr(i, j - i + 1));
                }
            }
        }

        return result;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Solution s;

    cout << s.findDistinctSubstrings("abcbd", 3) << endl;
    cout << s.findDistinctSubstrings("abcadce", 4) << endl;
    cout << s.findDistinctSubstrings("aa", 1) << endl;

    return 0;
}
