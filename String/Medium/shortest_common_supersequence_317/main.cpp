/*

Given two sequences, find the length of the shortest supersequence 'Z' of given sequences 'X' and 'Y' such that both 'X' and 'Y' are subsequences of 'Z'.

Input: X = "ABCBDAB", Y = "BDCABA"
Output: 9
Explanation: The SCS are "ABCBDCABA", "ABDCABDAB", and "ABDCBDABA", having length 9.

*/

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findSCSLength(string X, string Y)
    {
        int m = X.size(), n = Y.size();

        int lookup[m + 1][n + 1];

        for (int i = 0; i <= m; ++i) {
            lookup[i][0] = i;
        }

        for (int j = 0; j <= n; ++j) {
            lookup[0][j] = j;
        }

        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1]) {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;
                } else {
                    lookup[i][j] = min(lookup[i - 1][j] + 1, lookup[i][j - 1] + 1);
                }
            }
        }

        return lookup[m][n];
    }
};

int main()
{
    cout << Solution().findSCSLength("ABCBDAB", "BDCABA") << endl;

    return 0;
}
