/*

Given a lists of words, print all combinations of phrases that can be formed by picking one word from each list.

Input:

lists =
[
    ["John", "Emma"],
    ["Plays", "Hates", "Watches"],
    ["Cricket", "Soccer", "Chess"]
]

Output:

{
    "John Plays Cricket",
    "John Plays Soccer",
    "John Plays Chess",
    "John Hates Cricket",
    "John Hates Soccer",
    "John Hates Chess",
    "John Watches Cricket",
    "John Watches Soccer",
    "John Watches Chess",
    "Emma Plays Cricket",
    "Emma Plays Soccer",
    "Emma Plays Chess",
    "Emma Hates Cricket",
    "Emma Hates Soccer",
    "Emma Hates Chess",
    "Emma Watches Cricket",
    "Emma Watches Soccer",
    "Emma Watches Chess"
}

*/

#include <iostream>
#include <unordered_set>
#include <vector>

using namespace std;

class Solution
{
private:
    void findCombinations(vector<vector<string>> const &lists, string result, int n, unordered_set<string>& res)
    {
        if (lists.empty()) {
            return;
        }

        if (n == lists.size())
        {
            res.insert(result.substr(1));
            return;
        }

        int m = lists[n].size();
        for (int i = 0; i < m; ++i)
        {
            string out = result + " " + lists[n].at(i);

            findCombinations(lists, out, n + 1, res);
        }
    }
public:
    unordered_set<string> findCombinations(vector<vector<string>> const &lists)
    {
        unordered_set<string> res;
        findCombinations(lists, "", 0, res);
        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<vector<string>> lists{
        {"John", "Emma"},
        {"Plays", "Hates", "Watches"},
        {"Cricket", "Soccer", "Chess"}
    };

    cout << Solution().findCombinations(lists) << endl;

    return 0;
}
