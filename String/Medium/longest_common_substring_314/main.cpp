/*

Given two strings, find the longest string that is a substring of both strings.

Input: X = "ABABC", Y = "BABCA"
Output: "BABC"
Explanation: The longest common substring of strings "ABABC" and "BABCA" is "BABC" having length 4. The other common substrings are "ABC", "A", "AB", "B", "BA", "BC", and "C".

*/

#include <iostream>
#include <string>
#include <cstring>

using namespace std;

class Solution
{
public:
    string findLongestCommonSubstring(string X, string Y)
    {
        int m = X.size(), n = Y.size();

        int maxlen = 0;
        int endingIndex = m;

        int lookup[m + 1][n + 1];
        memset(lookup, 0, sizeof(lookup));

        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1])
                {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;

                    if (lookup[i][j] > maxlen)
                    {
                        maxlen = lookup[i][j];
                        endingIndex = i;
                    }
                }
            }
        }

        return X.substr(endingIndex - maxlen, maxlen);
    }
};

int main()
{
    cout << Solution().findLongestCommonSubstring("ABABC", "BABCA") << endl;

    return 0;
}
