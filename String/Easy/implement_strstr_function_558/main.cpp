/*

Given a text and a pattern, return the index of the first occurrence of pattern in text and return -1 if pattern is not part of text.

Input: text = "ABCABAABCABAC", pattern = "ABAA"
Output: 3
Explanation: The pattern occurs only once in the text, starting from index 3.

Input: text = "ABCABAABCABAC", pattern = "CAB"
Output: 2
Explanation: The pattern occurs twice in the text, starting from index 2 and 8. The solution should return the index of the first occurrence, i.e., 2.

*/

#include <iostream>
#include <vector>

using namespace std;

// https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm

class Solution {
public:
    int strstr(string text, string pattern) {
        if (pattern.empty()) {
            return 0;
        }

        if (text.empty() || pattern.size() > text.size()) {
            return -1;
        }

        vector<int> next(pattern.size() + 1);
        for (int i = 1; i < pattern.size(); ++i) {
            int j = next[i + 1];

            while (j > 0 && pattern[j] != pattern[i]) {
                j = next[j];
            }

            if (j > 0 || pattern[j] == pattern[i]) {
                next[i + 1] = j + 1;
            }
        }

        for (int i = 0, j = 0; i < text.size(); ++i) {
            if (j < pattern.size() && text[i] == pattern[j]) {
                if (++j == pattern.size()) {
                    return (i - j + 1);
                }
            } else if (j > 0) {
                j = next[j];
                i--;
            }
        }

        return -1;
    }
};

int main() {
    cout << Solution().strstr("ABCABAABCABAC", "ABAA") << endl;
    cout << Solution().strstr("ABCABAABCABAC", "CAB") << endl;

    cout << Solution().strstr("ABC", "D") << endl;

    return 0;
}
