/*

Run–length encoding (RLE) is a simple form of lossless data compression that runs on sequences with the same value occurring many consecutive times. It encodes the sequence to store only a single value and its count.

Input: "WWWWWWWWWWWWBWWWWWWWWWWWWBBBWWWWWWWWWWWWWWWWWWWWWWWWBWWWWWWWWWWWWWW"
Output: "12W1B12W3B24W1B14W"
Explanation: String can be interpreted as a sequence of twelve W’s, one B, twelve W’s, three B’s, and so on..

*/

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

class Solution
{
public:
    string encode(string s)
    {
        if (s.empty()) return s;

        stringstream ss;

        char last = s[0];
        int num = 1;

        for (int i = 1; i < s.size(); ++i) {
            char ch = s[i];
            if (ch != last) {
                ss << num << last;
                num = 0;
            }
            last = ch;
            ++num;
        }

        ss << num << last;

        return ss.str();
    }
};

int main()
{
    cout << Solution().encode("WWWWWWWWWWWWBWWWWWWWWWWWWBBBWWWWWWWWWWWWWWWWWWWWWWWWBWWWWWWWWWWWWWW") << endl;

    return 0;
}
