/*

Given a string, check if it is a palindrome. A palindromic string is a string that remains the same with its characters reversed.

Input : "ABCBA"
Output: true

Input : "ABCA"
Output: false

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    bool isPalindrome(string s)
    {
        /*int n = s.length(), half = n / 2, high = n - 1;

        for (int i = 0; i < half; ++i) {
            if (s[i] != s[high - i]) {
                return false;
            }
        }
        return true;*/
        if (s.length() <= 1) return true;
        if (s[0] == s[s.length() - 1]) {
            return isPalindrome(s.substr(1, s.length() - 2));
        }
        return false;
    }
};

int main()
{
    cout << boolalpha << Solution().isPalindrome("ABCBA") << endl;
    cout << boolalpha << Solution().isPalindrome("ABCA") << endl;

    return 0;
}
