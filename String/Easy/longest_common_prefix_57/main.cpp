/*

Given a set of strings, find their longest common prefix (LCP).

Input: ["technique", "technician", "technology", "technical"]
Output: "techn"

Input: ["techie delight", "tech", "techie", "technology", "technical"]
Output: "tech"

*/


#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
private:
    string getLCP(string a, string b) {
        int length = min(a.size(), b.size()), j = 0;
        for (int i = 0; i < length; ++i, ++j) {
            if (a[i] != b[i]) {
                break;
            }
        }
        return a.substr(0, j);
    }

    string findLCP(vector<string> const &words, int low, int high)
    {
        if (low < high) {
            int mid = (low + high) / 2;

            string a = findLCP(words, low, mid);
            string b = findLCP(words, mid + 1, high);

            return getLCP(a, b);
        } else {
            return low == high ? words[low] : "";
        }
    }
public:
    string findLCP(vector<string> const &words)
    {
        return findLCP(words, 0, words.size() - 1);
    }
};

int main()
{
    {
        cout << Solution().findLCP({"technique", "technician", "technology", "technical"}) << endl;
    }

    {
        cout << Solution().findLCP({"techie delight", "tech", "techie", "technology", "technical"}) << endl;
    }

    return 0;
}
