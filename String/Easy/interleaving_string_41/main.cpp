/*

Find all interleavings of given strings that can be formed from all the characters of the first and second string where the order of characters is preserved.

Input: X = "ABC", Y = "ACB"
Output: {"ACBABC", "AABCBC", "ACABCB", "ABCACB", "AACBBC", "ABACCB", "ACABBC", "ABACBC", "AACBCB", "AABCCB"}

Input: X = "", Y = ""
Output: {}

*/

#include <iostream>
#include <string>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    void findInterleavingsInternal(unordered_set<string>& strings, string result, string X, string Y) {
        if (X.empty() && Y.empty()) {
            strings.insert(result);
            return;
        }

        if (!X.empty()) {
            findInterleavingsInternal(strings, result + X[0], X.substr(1), Y);
        }

        if (!Y.empty()) {
            findInterleavingsInternal(strings, result + Y[0], X, Y.substr(1));
        }
    }
public:
    unordered_set<string> findInterleavings(string X, string Y)
    {
        unordered_set<string> res;
        if (!X.empty() || !Y.empty()) {
            findInterleavingsInternal(res, "", X, Y);
        }
        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& strings) {
    for (auto& s: strings) {
        os << s << ' ';
    }
    return os;
}


int main()
{
    cout << Solution().findInterleavings("ABC", "ACB") << endl;
    cout << Solution().findInterleavings("", "") << endl;

    return 0;
}
