/*

Given a string, remove all occurrences of "AB" and "C" in a single traversal of it.

Input: "CBAABCAB"
Output: "BA"

The solution should remove of all adjacent, as well as non-adjacent occurrences of string "AB" and "C".

Input: "ADAABCB"
Output: "AD"
Explanation: Removing the first adjacent occurrence of "AB" and "C" results in string "ADAB", which again needs to be processed for adjacent "AB". Therefore, the final output string will be "AD".

Input: "AACBBC"
Output: ""
Explanation: AACBBC -> AABB -> AB -> ""

*/

#include <iostream>
#include <string>
#include <stack>

using namespace std;

class Solution
{
public:
    string remove(string s)
    {
        stack<int> pos;

        for (int i = 0; i < s.size(); ++i) {
            switch (s[i]) {
            case 'C':
                s[i] = '*';
                break;
            case 'A':
                pos.push(i);
                break;
            case 'B':
                if (!pos.empty()) {
                    int j = pos.top();
                    pos.pop();

                    s[i] = '*';
                    s[j] = '*';
                }
                break;
            default:
                if (!pos.empty() && s[i] != 'A' && s[i] != 'B') {
                    stack<int> empty;
                    pos.swap(empty);
                }
            }
        }

        string ns;
        ns.reserve(s.size());

        for (auto c : s) {
            if (c != '*') ns += c;
        }
        return ns;
    }
};

int main()
{
    cout << Solution().remove("CBAABCAB") << endl; // "BA"
    cout << Solution().remove("ADAABCB") << endl; // "AD"
    cout << Solution().remove("AACBBC") << endl; // ""

    cout << Solution().remove("ABACB") << endl; // ""
    cout << Solution().remove("ABCACBCAABB") << endl; // ""
    cout << Solution().remove("AYABBY") << endl; // "AYBY"

    return 0;
}
