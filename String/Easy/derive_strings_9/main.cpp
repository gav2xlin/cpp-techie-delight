/*

Check if a given string can be derived from another string by circularly rotating it. The rotation can be in a clockwise or anti-clockwise rotation.

Input: X = "ABCD", Y = "DABC"
Output: true
Explanation: "DABC" can be derived from "ABCD" by right-rotating it by 1 unit

*/

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
public:
    bool check(string X, string Y)
    {
        for (int i = 0; i < X.size(); ++i) {
            rotate(Y.begin(), Y.begin() + 1, Y.end());
            if (X == Y) return true;
        }

        return false;
    }
};

int main()
{
    cout << boolalpha << Solution().check("ABCD", "DABC") << endl;

    return 0;
}
