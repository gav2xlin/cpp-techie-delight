/*

Given an integer representing the capacity of a cyber cafe and a sequence representing entry/exit logs of customers, find the total number of customers who could not get any computer.

The first occurrence in the given sequence indicates the arrival, and the second occurrence indicates the departure for a customer. A customer cannot be serviced when the cafe capacity is full (when all computers are allocated).

Input:

sequence = "ABCDDCEFFEBGAG"
capacity = 3

Output: 2

Explanation: Customers 'D' and 'F' left unattended


Input:

sequence = "ABCDDCBEFFEGAG"
capacity = 2

Output: 3

Explanation: Customers 'C', 'D', and 'F' left unattended

*/

#include <iostream>
#include <string>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    int findUnattendedCustomers(string sequence, int capacity)
    {
        unordered_set<char> attended, unattended;

        for (char c : sequence) {
            if (attended.find(c) != attended.end()) {
                 attended.erase(c);
            } else if (attended.size() < capacity && unattended.find(c) == unattended.end()) {
                attended.insert(c);
            } else {
                unattended.insert(c);
            }
        }

        return unattended.size();
    }
};

int main()
{
    //cout << Solution().findUnattendedCustomers("ABCDDCEFFEBGAG", 3) << endl;
    //cout << Solution().findUnattendedCustomers("ABCDDCBEFFEGAG", 2) << endl;

    cout << Solution().findUnattendedCustomers("EBCDEDBCAA", 1) << endl;

    return 0;
}
