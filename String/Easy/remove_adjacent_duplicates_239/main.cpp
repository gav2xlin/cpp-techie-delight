/*

Given a string, remove adjacent duplicates characters from it. In other words, remove all consecutive same characters except one.

Input : "AABBBCDDD"
Output: "ABCD"

*/

#include <iostream>
#include <string>

using namespace std;

class Solution
{
public:
    void removeAdjacentDuplicates(string &s)
    {
        if (s.empty()) return;

        string res;
        char last = s[0];

        for (int i = 1; i < s.size(); ++i) {
            char ch = s[i];
            if (ch != last) {
                res += last;
            }
            last = ch;
        }

        res += last;
        s.swap(res);
    }
};

int main(int argc, char** argv)
{
    string s{"AABBBCDDD"};
    Solution().removeAdjacentDuplicates(s);
    cout << s << endl;

    return 0;
}
