/*

Given two strings, determine whether they are anagrams.

Any word that exactly reproduces the letters in another order is an anagram. In other words, X and Y are anagrams if by rearranging the letters of X, we can get Y using all the original letters of X exactly once.

Input: X = "silent", Y = "listen"
Output: true

Input: X = "incest", Y = "insect"
Output: true

*/

#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

class Solution
{
public:
    bool isAnagram(string X, string Y)
    {
        sort(X.begin(), X.end());
        sort(Y.begin(), Y.end());

        return X == Y;
    }
};

int main()
{
    cout << boolalpha << Solution().isAnagram("silent", "listen") << endl;
    cout << boolalpha << Solution().isAnagram("silent", "listen") << endl;

    return 0;
}
