/*

Given a function that produces either 0 or 1 each with 50% probability, generate 0, 1, and 2 with equal probability using that function.

Input : foo() -> Function that returns 0 or 1 each with 50% probability.
Output: Return 0, 1, and 2 with equal probability using foo() function.

*/

#include <iostream>
#include <cstdlib>

using namespace std;

class Solution
{
private:
    int random() {
        return rand() % 2;
    }
public:
    // Return 0, 1, and 2 with equal probability using foo() function
    int generate()
    {
        //int x = random();
        //int y = random();

        // if `x == 1` and `y == 0`, try again
        //return (x == 1 && y == 0)? generate(): (x + y);
        int rand = 2 * random() + random();

        // return rand if it is 0, 1, or 2; otherwise, try again
        return (rand <= 2) ? rand : generate();
    }
};

int main()
{
    cout << Solution().generate() << endl;

    return 0;
}
