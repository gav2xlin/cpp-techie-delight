/*

Given a function that generates random numbers from 1 to 5 with equal probability, generate 0 and 1 with equal probability using that function.

Input : foo() -> Function that returns random numbers from 1 to 5 with equal probability.
Output: Return 0 and 1 with equal probability using foo() function.

*/

#include <iostream>
#include <cstdlib>

using namespace std;

class Solution
{
private:
    int random() {
        return (rand() % 5) + 1;
    }
public:
    // Return 0 and 1 with equal probability using foo() function
    int generate()
    {
        int r;

        do {
            // `r` could be any one of 1, 2, 3, 4, and 5
            r = random();
        } while (r == 5);

        // `r` could any of 1, 2, 3, 4 now

        // since there are 2 odd and 2 even numbers, return the last bit of `r`,
        // which could be 0 or 1 with equal probability
        return r & 1;
    }
};

int main()
{
    cout << Solution().generate() << endl;

    return 0;
}
