/*

Given n disks of different sizes, find the optimal solution to Tower of Hanoi problem.

The Tower of Hanoi is a mathematical puzzle consisting of three rods and n disks of different sizes which can slide onto any rod. The puzzle starts with the disks in a neat stack in ascending order of size on one rod, the smallest at the top, making a conical shape. The objective of the puzzle is to move the entire stack to another rod, obeying the following simple rules:

• Only one disk can be moved at a time.
• Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack, i.e., a disk can only be moved if it is the uppermost disk on a stack.
• No disk may be placed on top of a smaller disk.

The minimum number of moves required to solve a Tower of Hanoi puzzle is 2^n-1, where n is the total number of disks. Note that the solution with the minimal number of moves is unique.

For example,

Input: n = 2
Output: [(1, 2), (1, 3), (2, 3)]
Explanation: Let the 2 disks be 'A' and 'B', with disk 'A' being the smallest and at the top. The optimal unique solution is:

Move disk 'A' from rod 1 —> 2
Move disk 'B' from rod 1 —> 3
Move disk 'A' from rod 2 —> 3

Input: n = 3
Output: [(1, 3), (1, 2), (3, 2), (1, 3), (2, 1), (2, 3), (1, 3)]
Explanation: Let the 3 disks be 'A', 'B', and 'C', with disk 'A' being the smallest and at the top. The optimal unique solution is:

Move disk 'A' from rod 1 —> 3
Move disk 'B' from rod 1 —> 2
Move disk 'A' from rod 3 —> 2
Move disk 'C' from rod 1 —> 3
Move disk 'A' from rod 2 —> 1
Move disk 'B' from rod 2 —> 3
Move disk 'A' from rod 1 —> 3

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
private:
    /*void tower_of_hanoi(vector<pair<int,int>>& moves, int num, int source=1, int dest=2, int helper=3) {
        if (num == 1) {
            moves.push_back({source, dest});
            return;
        }

        tower_of_hanoi(moves, num - 1, source, helper, dest);
        moves.push_back({source, dest});
        tower_of_hanoi(moves, num - 1, helper, dest, source);
    }*/
    void move(vector<pair<int,int>>& moves, int disks, int source=1, int auxiliary=2, int target=3) {
        if (disks > 0) {
            // move `n-1` discs from source to auxiliary using the target as an intermediate pole
            move(moves, disks - 1, source, target, auxiliary);

            // move one disc from source to target
            //cout << "Move disk " << disks << " from " << source << " —> " << target << endl;
            moves.push_back({source, target});

            // move `n-1` discs from auxiliary to target using the source as an intermediate pole
            move(moves, disks - 1, auxiliary, source, target);
        }
    }
public:
    vector<pair<int,int>> findOptimalSolution(int n) {
        vector<pair<int,int>> moves;
        //tower_of_hanoi(moves, n);
        move(moves, n);
        return moves;
    }
};

template<typename U, typename V>
ostream& operator<<(ostream& os, const pair<U,V>& p) {
    os << p.first << ':' << p.second << ' ';
    return os;
}

template<typename T>
ostream& operator<<(ostream& os, const vector<T>& values) {
    for (auto value : values) {
        os << value << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findOptimalSolution(2) << endl;
    cout << Solution().findOptimalSolution(3) << endl;

    return 0;
}
