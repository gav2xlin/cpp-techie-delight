/*

Given a function that produces either 0 or 1 each with 50% probability, generate 0 and 1 with 75% and 25% probability using that function.

Input : foo() -> Function that returns 0 or 1 each with 50% probability.
Output: Return 0 and 1 with 75% and 25% probability using foo() function.

*/

#include <iostream>
#include <cstdlib>

using namespace std;

class Solution
{
private:
    int random() {
        return rand() % 2;
    }
public:
    // Return 0 and 1 with 75% and 25% probability using foo() function
    int generate()
    {
        //int x = random();
        //int y = random();

        //return x & y;
        //return !(x | y);
        return ((random() << 1) ^ random()) == 0;
    }
};

int main()
{
    cout << Solution().generate() << endl;

    return 0;
}
