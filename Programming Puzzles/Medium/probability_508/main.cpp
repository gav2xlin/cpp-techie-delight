/*

Given a function that generates random numbers from 1 to 6 with equal probability, generate random numbers from 1 to 12 with equal probability using that function.

Input : foo() -> Function that returns random numbers from 1 to 6 with equal probability.
Output: Return 1 to 12 with equal probability using foo() function.

*/

#include <iostream>
#include <cstdlib>

using namespace std;

class Solution
{
private:
    int random() {
        return (rand() % 6) + 1;
    }
public:
    // Return 1 to 12 with equal probability using foo() function
    int generate()
    {
        int x = random();
        int y = random();

        return 2*x - (y & 1);
        //return x + (y & 1) * 6;
        //return x + !(y & 1) * 6;
    }
};

int main()
{
    cout << Solution().generate() << endl;

    return 0;
}
