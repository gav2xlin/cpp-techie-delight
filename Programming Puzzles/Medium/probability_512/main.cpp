/*

Given an integer array, return any one of its elements according to the given probabilities.

Input:

nums[] = [1, 2, 3, 4, 5]
probability[] = [30, 10, 20, 15, 25]

Output: The solution should return 1 with 30% probability, 2 with 10% probability, 3 with 20% probability, 4 with 15% probability, and 5 with 25% probability.

Assume that the total probability sums to 100%.

*/


#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

class Solution
{
public:
    int findRandom(vector<int> const &nums, vector<int> const &probability)
    {
        int n = nums.size();
        if (n != probability.size()) {
            return -1;
        }

        vector<int> prob_sum(n, 0);

        prob_sum[0] = probability[0];
        for (int i = 1; i < n; i++) {
            prob_sum[i] = prob_sum.at(i-1) + probability[i];
        }

        int r = (rand() % 100) + 1;

        if (r <= prob_sum[0]) {      // handle 0th index separately
            return nums[0];
        }

        for (int i = 1; i < n; i++)
        {
            if (r > prob_sum.at(i-1) && r <= prob_sum[i]) {
                return nums[i];
            }
        }

        return -1;
    }
};

int main()
{
    cout << Solution().findRandom({1, 2, 3, 4, 5}, {30, 10, 20, 15, 25}) << endl;

    return 0;
}
