/*

Given a function that generates random numbers from 1 and 5 with equal probability, generate random numbers from 1 to 7 with equal probability using that function.

Input : foo() -> Function that returns random numbers from 1 and 5 with equal probability.
Output: Return 1 to 7 with equal probability using foo() function.

*/

#include <iostream>
#include <cstdlib>

using namespace std;

class Solution
{
private:
    int random() {
        return (rand() % 5) + 1;
    }
public:
    // Return 1 to 7 with equal probability using foo() function
    int generate()
    {
        int r;
        do {
            r = 5 * (random() - 1) + random();
        } while (r > 7);

        return r;
    }
};

int main()
{
    cout << Solution().generate() << endl;

    return 0;
}
