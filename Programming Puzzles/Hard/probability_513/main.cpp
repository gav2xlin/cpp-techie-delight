/*

Given a biased function that returns 0 with `p` probability and 1 with `1-p` probability where p != (1-p), generate fair results from it.

Input : biased() -> Function that returns 0 with 0.8 probability and 1 with 0.2 probability.
Output: Return 0 and 1 with equal probability using biased() function.

*/

#include <iostream>
#include <cstdlib>

using namespace std;

#define HEADS 1
#define TAILS 0

class Solution
{
private:
    // A biased function that returns TAILS with probability `p` and
    // HEADS with `1-p` probability
    int biased()
    {
        // generate a random number between 0–99, both inclusive
        int r = rand() % 100;

        // return TAILS if we got a number between [0–79]; otherwise, return HEADS
        return (r <= 79)? TAILS: HEADS;
    }
public:
    // Return 0 and 1 with equal probability using biased() function
    // Return HEADS and TAILS with equal probability using the specified function
    int generate()
    {
        while (1)
        {
            int first = biased();
            int second = biased();

            if (first != second) {
                return first;    // or return second
            }
        }
    }
};

int main()
{
    cout << Solution().generate() << endl;

    return 0;
}
