/*

Given time in hh:mm format, calculate and return the shorter angle between the hour and minute hand in an analog clock.

Assume valid time in 24-hour notation, where hh (0 to 23) is the number of full hours that have passed since midnight, and mm (0 to 59) is the number of minutes that have passed since the last full hour.

Input: hh = 5, mm = 30
Output: 15

Input: hh = 9, mm = 00
Output: 90

Input: hh = 21, mm = 00
Output: 90

Input: hh = 12, mm = 00
Output: 0

*/

/*
Degree(hh) = H×(360/12) + (M×360)/(12×60)
Degree(mm) = M×(360/60)
*/

#include <iostream>
#include <cmath>

using namespace std;

class Solution
{
public:
    int findClockAngle(int hh, int mm)
    {
        hh %= 12;

        int h = hh * 360 / 12 + mm * 360 / (12 * 60);
        int m = mm * 360 / 60;

        int angle = abs(h - m);

        if (angle > 180) {
            angle = 360 - angle;
        }

        return angle;
    }
};


int main()
{
    cout << Solution().findClockAngle(5, 30) << endl;
    cout << Solution().findClockAngle(9, 0) << endl;
    cout << Solution().findClockAngle(21, 0) << endl;
    cout << Solution().findClockAngle(12, 0) << endl;

    return 0;
}
