/*

Given a monotonically increasing function f(x) on positive numbers, find the value of x when f(x) becomes positive for the first time. In other words, find a positive number x such that f(x-1), f(x-2), … are negative and f(x+1), f(x+2), … are positive.

A function is called monotonically increasing if f(x) <= f(y) is true for all x and y, where x <= y. For example,

Input: f(x) = 2x - 100
Output: 51
Explanation: f(x) becomes positive for the first time when x = 51.

Input: f(x) = 3x - 100
Output: 34
Explanation: f(x) becomes positive for the first time when x = 34.

*/

#include <iostream>

using namespace std;

/*int f(int x) {
    return 2 * x - 100;
}*/

int f(int x) {
    return 3 * x - 100;
}

class Solution
{
private:
    int binarySearch(int low, int high)
    {
        /*if (low > high) {
            return -1;
        }

        int mid = (low + high) / 2;

        if (f(mid) > 0)
        {
            if (mid == low || f(mid - 1) <= 0) {
                return mid;
            }

            return binarySearch(low, mid - 1);
        }

        return binarySearch(mid + 1, high);*/
        int mid = -1;
        while (low <= high) {
            mid = (low + high) / 2;

            if (f(mid) > 0) {
                if (mid == low || f(mid - 1) <= 0) {
                    return mid;
                }
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return mid;
    }
public:

    /*
        Return value x where a function f(x) becomes positive for the first time.
    */
    int findValue()
    {
        int i = 1;
        while (f(i) <= 0) {
            i *= 2;
        }
        return binarySearch(i / 2, i);
    }
};

int main()
{
    cout << Solution().findValue() << endl;

    return 0;
}
