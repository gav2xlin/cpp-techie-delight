/*

Given a sorted binary array, efficiently count the total number of 1's in it.

Input: [0, 0, 0, 0, 1, 1, 1]
Output: 3

Input: [0, 0, 1, 1, 1, 1, 1]
Output: 5

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int countOnes(vector<int> const &nums) {
        // binary search: O(log(n))
        int low = 0, high = nums.size() - 1;

        while (low <= high) {
            int mid = (low + high) / 2;
            if (nums[mid] == 0) {
                low = mid + 1;
            } else if (nums[mid] == 1) {
                high = mid - 1;
            }
        }

        return nums.size() - low;
    }
};

int main()
{
    cout << Solution().countOnes({0, 0, 0, 0, 1, 1, 1}) << endl;
    cout << Solution().countOnes({0, 0, 1, 1, 1, 1, 1}) << endl;

    cout << Solution().countOnes({0, 0, 0, 0, 0, 0, 0}) << endl;
    cout << Solution().countOnes({1, 1, 1, 1, 1, 1, 1}) << endl;
    cout << Solution().countOnes({}) << endl;

    return 0;
}
