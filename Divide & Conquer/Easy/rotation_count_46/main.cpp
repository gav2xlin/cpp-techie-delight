/*

Given a circularly sorted array of distinct integers, find the total number of times the array is rotated. You may assume that the rotation is in anti-clockwise direction.

Input: [8, 9, 10, 2, 5, 6]
Output: 3

Input: [2, 5, 6, 8, 9, 10]
Output: 0

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    /*int findRotationCount(vector<int> const &nums)
    {
        int n = nums.size(), low = 0, high = n - 1;

        while (low <= high) {
            //if (nums[low] <= nums[high]) {
            //    return low;
            //}

            int mid = (low + high) / 2;

            int next = (mid + 1) % n;
            int prev = (mid - 1 + n) % n;

            if (nums[mid] <= nums[next] && nums[mid] <= nums[prev]) {
                return mid;
            } else if (nums[mid] <= nums[high]) {
                high = mid - 1;
            } else if (nums[mid] >= nums[low]) {
                low = mid + 1;
            }
        }

        return -1;
    }*/
    int findRotationCount(vector<int> const &nums)
    {
        int n = nums.size();
        if (n <= 1) return 0;

        int low = 0, high = n - 1;
        if (nums[low] <= nums[high]) {
            return low;
        }

        while (low <= high) {
            int mid = (low + high) / 2;

            int next = (mid + 1) % n;
            int prev = (mid - 1 + n) % n;

            if (nums[mid] <= nums[next] && nums[mid] <= nums[prev]) {
                return mid;
            } else if (nums[mid] <= nums[high]) {
                high = mid - 1;
            } else if (nums[mid] >= nums[low]) {
                low = mid + 1;
            }
        }

        return -1;
    }
};

int main()
{
    cout << Solution().findRotationCount({8, 9, 10, 2, 5, 6}) << endl;
    cout << Solution().findRotationCount({2, 5, 6, 8, 9, 10}) << endl;

    return 0;
}
