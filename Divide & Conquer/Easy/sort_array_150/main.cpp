/*

Given an integer array, inplace sort it without using any inbuilt functions.

Input : [6, 3, 4, 8, 2, 9]
Output: [2, 3, 4, 6, 8, 9]

Input : [9, -3, 5, -2, -8, -6]
Output: [-8, -6, -3, -2, 5, 9]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution {
private:
    void merge_sort(vector<int>& nums, int low, int mid, int high, vector<int>& aux) {
        int i = low, j = mid + 1, k = low;

        while (i <= mid && j <= high) {
            if (nums[i] <= nums[j]) {
                aux[k++] = nums[i++];
            } else {
                aux[k++] = nums[j++];
            }
        }

        while (i <= mid) {
            aux[k++] = nums[i++];
        }

        while (j <= high) {
            aux[k++] = nums[j++];
        }

        for (int i = low; i <= high; ++i) {
            nums[i] = aux[i];
        }
    }

    void merge_sort_split(vector<int>& nums, int low, int high, vector<int>& aux) {
        if (low < high) {
            int mid = (low + high) / 2;

            merge_sort_split(nums, low, mid, aux);
            merge_sort_split(nums, mid + 1, high, aux);

            merge_sort(nums, low, mid, high, aux);
        }
    }
public:
    void sort(vector<int>& nums) {
        // O(n*log(n))
        vector<int> aux(nums.size());
        merge_sort_split(nums, 0, nums.size() - 1, aux);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums1{6, 3, 4, 8, 2, 9};
    Solution().sort(nums1);
    cout << nums1 << endl;

    vector<int> nums2{9, -3, 5, -2, -8, -6};
    Solution().sort(nums2);
    cout << nums2 << endl;

    return 0;
}
