/*

Given a sequence of n numbers such that the difference between the consecutive terms is constant, find the missing term in logarithmic time.

Input: [5, 7, 9, 11, 15]
Output: 13

Input: [1, 4, 7, 13, 16]
Output: 10

Input: [4, 8]
Output: 6

Assume valid input and n >= 2. Also, assume that the first and last elements are always part of the input sequence and the missing number lies between index 1 to n-1.

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findMissingNumber(vector<int> const &nums) {
        int n = nums.size(), low = 0, high = n - 1;

        int d = (nums[n - 1] - nums[0]) / n;

        while (low <= high)
        {
            int mid = (low + high) / 2;

            if (mid + 1 < n && nums[mid + 1] - nums[mid] != d) {
                return nums[mid + 1] - d;
            }

            if (mid - 1 >= 0 && nums[mid] - nums[mid - 1] != d) {
                return nums[mid - 1] + d;
            }

            if (nums[mid] - nums[0] != (mid - 0) * d) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }

        return -1;
    }
};

int main()
{
    cout << Solution().findMissingNumber({5, 7, 9, 11, 15}) << endl;
    cout << Solution().findMissingNumber({1, 4, 7, 13, 16}) << endl;
    cout << Solution().findMissingNumber({4, 8}) << endl;

    return 0;
}
