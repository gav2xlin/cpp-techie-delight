/*

Given a sorted array of non-negative distinct integers, find the smallest missing non-negative element in it.

Input: [0, 1, 2, 6, 9, 11, 15]
Output: 3

Input: [1, 2, 3, 4, 6, 9, 11, 15]
Output: 0

Input: [0, 1, 2, 3, 4, 5, 6]
Output: 7

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int findSmallestMissing(vector<int> const &nums, int low, int high) {
        if (low > high) {
            return low;
        }

        int mid = (low + high) / 2;

        if (nums[mid] == mid) {
            return findSmallestMissing(nums, mid + 1, high);
        } else {
            return findSmallestMissing(nums, low, mid - 1);
        }
    }
public:
    int findSmallestMissingNumber(vector<int> const &nums)
    {
        return findSmallestMissing(nums, 0, nums.size() - 1);
    }
};

int main()
{
    cout << Solution().findSmallestMissingNumber({0, 1, 2, 6, 9, 11, 15}) << endl;
    cout << Solution().findSmallestMissingNumber({1, 2, 3, 4, 6, 9, 11, 15}) << endl;
    cout << Solution().findSmallestMissingNumber({0, 1, 2, 3, 4, 5, 6}) << endl;

    return 0;
}
