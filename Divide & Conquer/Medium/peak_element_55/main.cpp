/*

Given an integer array `A`, find the peak element in it. An element `A[i]` is a peak element if it's greater than its neighbor(s). i.e.,

• A[i-1] <= A[i] >= A[i+1]	(for 0 < i < n-1)
• A[i-1] <= A[i]			(if i = n – 1)
• A[i] >= A[i+1]			(if i = 0)


Input: [8, 9, 10, 12, 15]
Output: 15

Input: [10, 8, 6, 5, 3, 2]
Output: 10

• There might be multiple peak elements in an array, the solution should report any peak element.

Input: [8, 9, 10, 2, 5, 6]
Output: 10 or 6

Input: [8, 9, 2, 5, 6, 3]
Output: 9 or 6

• If the peak element is not located, the procedure should return -1.

Input: []
Output: -1

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution {
private:
    int findPeakElement(vector<int> const &nums, int low, int high, int n) {
        int mid = (low + high) / 2;

        if ((mid == 0 || nums[mid - 1] <= nums[mid]) && (mid == n - 1 || nums[mid] >= nums[mid + 1])) {
            return mid;
        }

        if (mid - 1 >= 0 && nums[mid - 1] > nums[mid]) {
            return findPeakElement(nums, low, mid - 1, n);
        }

        return findPeakElement(nums, mid + 1, high, n);
    }
public:
    int findPeakElement(vector<int> const &nums) {
        int n = nums.size();
        if (n == 0) {
            return -1;
        }

        int index = findPeakElement(nums, 0, n - 1, n);
        return nums[index];
    }
};

int main() {
    cout << Solution().findPeakElement({8, 9, 10, 12, 15}) << endl;
    cout << Solution().findPeakElement({10, 8, 6, 5, 3, 2}) << endl;
    cout << Solution().findPeakElement({8, 9, 10, 2, 5, 6}) << endl;
    cout << Solution().findPeakElement({8, 9, 2, 5, 6, 3}) << endl;
    cout << Solution().findPeakElement({}) << endl;

    return 0;
}
