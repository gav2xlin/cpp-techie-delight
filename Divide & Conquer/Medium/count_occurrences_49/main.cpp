/*

Given a sorted integer array containing duplicates, return the count of occurrences of a given number.

Input: nums[] = [2, 5, 5, 5, 6, 6, 8, 9, 9, 9], target = 5
Output: 3
Explanation: Target 5 occurs 3 times

Input: nums[] = [2, 5, 5, 5, 6, 6, 8, 9, 9, 9], target = 6
Output: 2
Explanation: Target 6 occurs 2 times

Input: nums[] = [5, 4, 3, 2, 1], target = 6
Output: 0
Explanation: Target 6 occurs 0 times

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int binarySearch(vector<int> const &nums, int target, bool searchFirst) {
        int low = 0, high = nums.size() - 1;
        int result = -1;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (target < nums[mid]) {
                high = mid - 1;
            } else if (target > nums[mid]) {
                low = mid + 1;
            } else {
                result = mid;

                if (searchFirst) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            }
        }

        return result;
    }
public:
    int countOccurrences(vector<int> const &nums, int target)
    {
        int first = binarySearch(nums,target, true);
        if (first < 0) return 0;
        int last = binarySearch(nums, target, false);
        return last - first + 1;
    }
};

int main()
{
    cout << Solution().countOccurrences({2, 5, 5, 5, 6, 6, 8, 9, 9, 9}, 5) << endl;
    cout << Solution().countOccurrences({2, 5, 5, 5, 6, 6, 8, 9, 9, 9}, 6) << endl;
    cout << Solution().countOccurrences({5, 4, 3, 2, 1}, 6) << endl;

    cout << Solution().countOccurrences({2, 5, 5, 5, 6, 6, 8, 9, 9, 9}, 4) << endl;

    return 0;
}
