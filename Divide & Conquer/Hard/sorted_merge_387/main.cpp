/*

Given k sorted singly-linked lists of integers, merge them into a single list in increasing order, and return it.

Input: [
    1 —> 5 —> 7 —> nullptr,
    2 —> 3 —> 6 —> 9 —> nullptr,
    4 —> 8 —> 10 —> nullptr
]

Output: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    Node *sortedMerge(Node* a, Node* b)
    {
        if (a == nullptr)
        {
            return b;
        }
        else if (b == nullptr)
        {
            return a;
        }

        Node *result;
        if (a->data <= b->data)
        {
            result = a;
            result->next = sortedMerge(a->next, b);
        }
        else
        {
            result = b;
            result->next = sortedMerge(a, b->next);
        }

        return result;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* sortedMerge(vector<Node*> const &_lists)
    {
        vector<Node*> lists(_lists);

        int k = lists.size();
        if (k == 0) {
            return nullptr;
        }

        int last = k - 1;

        while (last != 0)
        {
            int i = 0, j = last;

            while (i < j)
            {
                lists[i] = sortedMerge(lists[i], lists[j]);

                ++i, --j;

                if (i >= j) {
                    last = j;
                }
            }
        }

        return lists[0];
    }
};

void printList(Node* node)
{
    while (node != nullptr)
    {
        cout << node->data << " —> ";
        node = node->next;
    }
    cout << "nullptr" << endl;
}

void cleanupList(Node* node) {
    while (node != nullptr)
    {
        Node* cur = node;
        node = node->next;
        delete cur;
    }
}

int main()
{
    int k = 3;

    vector<Node*> lists(k);

    lists[0] = new Node(1);
    lists[0]->next = new Node(5);
    lists[0]->next->next = new Node(7);

    lists[1] = new Node(2);
    lists[1]->next = new Node(3);
    lists[1]->next->next = new Node(6);
    lists[1]->next->next->next = new Node(9);

    lists[2] = new Node(4);
    lists[2]->next = new Node(8);
    lists[2]->next->next = new Node(10);

    Node* head = Solution().sortedMerge(lists);

    printList(head);
    cleanupList(head);

    return 0;
}
