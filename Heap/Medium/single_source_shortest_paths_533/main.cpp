/*

Given a weighted directed graph with non-negative edge weights and a source vertex, return the shortest path cost from the source vertex to every other reachable vertex in the graph.

Input: Graph [edges = [(0, 1, 10), (0, 4, 3), (1, 2, 2), (1, 4, 4), (2, 3, 9), (3, 2, 7), (4, 1, 1), (4, 2, 8), (4, 3, 2)], n = 5], source = 0
Here, triplet (x, y, w) represents an edge from x to y having weight w.

Output: {[0, 1, 4], [0, 2, 6], [0, 3, 5], [0, 4, 3]}
Here, triplet (s, d, c) indicates that the shortest path from source s to destination d has cost c.

Explanation:

• Shortest path from (0 —> 1) is [0 —> 4 —> 1] with cost 4.
• Shortest path from (0 —> 2) is [0 —> 4 —> 1 —> 2] with cost 6.
• Shortest path from (0 —> 3) is [0 —> 4 —> 3] with cost 5.
• Shortest path from (0 —> 4) is [0 —> 4] with cost 3.

Input: Graph [edges = [(0, 1, 10), (0, 4, 3), (1, 2, 2), (1, 4, 4), (2, 3, 9), (3, 2, 7), (4, 1, 1), (4, 2, 8), (4, 3, 2)], n = 5], source = 1
Output: {[1, 2, 2], [1, 3, 6], [1, 4, 4]}
Explanation:

• Shortest path from (1 —> 0) does not exist.
• Shortest path from (1 —> 2) is [1 —> 2] with cost 2.
• Shortest path from (1 —> 3) is [1 —> 4 —> 3] with cost 6.
• Shortest path from (1 —> 4) is [1 —> 4] with cost 4.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The source vertex is among the set of vertices in the graph.

*/

#include <iostream>
#include <set>
#include <vector>
#include <queue>
#include <utility>
#include <climits>

using namespace std;

class Edge {
public:
    int source, dest, weight;
};

class Graph
{
public:

    // vector of vectors to represent an adjacency list
    vector<vector<pair<int,int>>> adjList;

    // total number of nodes in the graph
    int n;

    Graph(vector<Edge> &edges, int size)
    {
        n = size;
        adjList.resize(n);

        for (Edge &edge: edges) {
            adjList[edge.source].push_back(make_pair(edge.dest, edge.weight));
        }
    }
};

class Solution
{
private:
    struct Node {
        int vertex, weight;
    };

    struct comp
    {
        bool operator()(const Node &lhs, const Node &rhs) const {
            return lhs.weight > rhs.weight;
        }
    };
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int source, dest, weight;
        };

        // Definition for a Graph
        class Graph
        {
        public:

            // vector of vectors to represent an adjacency list
            vector<vector<pair<int,int>>> adjList;

            // total number of nodes in the graph
            int n;

            Graph(vector<Edge> &edges, int size)
            {
                n = size;
                adjList.resize(n);

                for (Edge &edge: edges) {
                    adjList[edge.source].push_back(make_pair(edge.dest, edge.weight));
                }
            }
        };
    */

    set<vector<int>> findShortestPaths(Graph const &graph, int source)
    {
        priority_queue<Node, vector<Node>, comp> min_heap;
        min_heap.push({source, 0});

        int n = graph.n;

        vector<int> dist(n, INT_MAX);
        dist[source] = 0;

        vector<bool> visited(n, false);
        visited[source] = true;

        vector<int> prev(n, -1);

        while (!min_heap.empty())
        {
            Node node = min_heap.top();
            min_heap.pop();

            int u = node.vertex;

            for (auto& i: graph.adjList[u])
            {
                int v = i.first;
                int weight = i.second;

                if (!visited[v] && (dist[u] + weight) < dist[v])
                {
                    dist[v] = dist[u] + weight;
                    prev[v] = u;
                    min_heap.push({v, dist[v]});
                }
            }

            visited[u] = true;
        }

        set<vector<int>> paths;
        for (int i = 0; i < n; ++i)
        {
            if (i != source && dist[i] != INT_MAX)
            {
                paths.insert({source, i, dist[i]});
            }
        }

        return paths;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& vecs) {
    for (auto& v : vecs) {
        os << v << endl;
    }
    return os;
}

int main()
{
    vector<Edge> edges{{0, 1, 10}, {0, 4, 3}, {1, 2, 2}, {1, 4, 4}, {2, 3, 9}, {3, 2, 7}, {4, 1, 1}, {4, 2, 8}, {4, 3, 2}};
    Graph g{edges, 5};

    cout << Solution().findShortestPaths(g, 0) << endl;
    cout << Solution().findShortestPaths(g, 1) << endl;

    return 0;
}
