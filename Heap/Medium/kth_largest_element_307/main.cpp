/*

Given an infinite stream of integers, return the element representing the k'th largest element in the stream.

Input: k = 3, nextInt = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...]
Output : [-1, -1, 1, 2, 3, 4, 5, 6, 7, 8, ...]

Here, `k` is constant for each run and `nextInt` is the next integer in the infinite stream. The solution should return the next k'th largest element, and return -1 when the stream has fewer elements than `k`.

*/

#include <iostream>
#include <queue>
#include <functional>

using namespace std;

class Solution {
private:
    class MinHeap {
    private:
       int k;

        priority_queue<int, vector<int>, greater<int>> pq;
    public:
        MinHeap(int k) {
            this->k = k;
        }

        int add(int n) {
            if (pq.size() < k) {
                pq.push(n);
            } else if (n > pq.top()) {
                pq.pop();
                pq.push(n);
            }

            if (pq.size() == k) {
                return pq.top();
            } else {
                return -1;
            }
        }
    };

    MinHeap* pq{nullptr};
public:
    int findKthLargest(int k, int nextInt)
    {
        if (pq == nullptr) {
            pq = new MinHeap(k);
        }
        return pq->add(nextInt);
    }
};

int main()
{
    Solution s;
    for (auto v : {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) {
        cout << s.findKthLargest(3, v) << endl;
    }

    return 0;
}
