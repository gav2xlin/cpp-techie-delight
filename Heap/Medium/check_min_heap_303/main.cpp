/*

Given an integer array, check if it represents min-heap or not.

Input : [2, 3, 4, 5, 10, 15]
Output: true
Explanation: The input represents a min-heap.

           2
         /   \
        /	  \
       3	   4
      / \	  /
     /   \   /
    5	 10 15

Input : [2, 10, 4, 5, 3, 15]
Output: false
Explanation: The input is not a min-heap, as it violate the heap property.

           2
         /   \
        /	  \
       10	   4
      / \	  /
     /   \   /
    5	  3 15

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    bool checkMinHeap(vector<int> const nums, int i) {
        if (2 * i + 2 > nums.size()) {
            return true;
        }

        bool left = (nums[i] <= nums[2 * i + 1]) && checkMinHeap(nums, 2 * i + 1);
        bool right = (2 * i + 2 == nums.size()) || (nums[i] <= nums[2 * i + 2] && checkMinHeap(nums, 2 * i + 2));

        return left && right;
    }
public:
    bool checkMinHeap(vector<int> const &nums)
    {
        return checkMinHeap(nums, 0);
    }
};

int main()
{
    cout << boolalpha << Solution().checkMinHeap({2, 3, 4, 5, 10, 15}) << endl;
    cout << boolalpha << Solution().checkMinHeap({2, 10, 4, 5, 3, 15}) << endl;

    return 0;
}
