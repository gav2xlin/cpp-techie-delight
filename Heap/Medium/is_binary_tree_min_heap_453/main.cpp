/*

Given the root of a binary tree, check if it is a min-heap or not. In order words, the binary tree must be a complete binary tree where each node has a higher value than its parent's value.

Input:
           2
         /   \
        /	  \
       3	   4
      / \	  / \
     /	 \	 /	 \
    5	  6	8	  10

Output: true

Input:
           5
         /   \
        /	  \
       3	   8
      / \	  / \
     /	 \	 /	 \
    2	  4	6	  10

Output: false

*/

#include <iostream>

using namespace std;

class Node {
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution {
private:
    int getSize(Node* root) {
        return root != nullptr ? getSize(root->left) + getSize(root->right) + 1 : 0;
    }

    bool isHeap(Node* root, int i, int n) {
        if (root == nullptr) {
            return true;
        }

        if (i >= n) {
            return false;
        }

        if ((root->left && root->left->data <= root->data) || (root->right && root->right->data <= root->data)) {
            return false;
        }

        return isHeap(root->left, 2 * i + 1, n) && isHeap(root->right, 2 * i + 2, n);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isMinHeap(Node* root) {
        return isHeap(root, 0, getSize(root));
    }
};

int main() {
    /*Input:
           2
         /   \
        /	  \
       3	   4
      / \	  / \
     /	 \	 /	 \
    5	  6	8	  10

    Output: true*/
    {
        Node n5{5};
        Node n6{6};
        Node n3{3, &n5, &n6};
        Node n8{8};
        Node n10{10};
        Node n4{4, &n8, &n10};
        Node n2{2, &n3, &n4};

        cout << boolalpha << Solution().isMinHeap(&n2) << endl;
    }

    /*Input:
           5
         /   \
        /	  \
       3	   8
      / \	  / \
     /	 \	 /	 \
    2	  4	6	  10

    Output: false*/
    {
        Node n2{2};
        Node n4{4};
        Node n3{3, &n2, &n4};
        Node n6{6};
        Node n10{10};
        Node n8{8, &n6, &n10};
        Node n5{5, &n3, &n8};

        cout << boolalpha << Solution().isMinHeap(&n5) << endl;
    }

    return 0;
}
