/*

Given an integer array representing inorder traversal of a binary tree, construct and return a Cartesian tree from it. A Cartesian tree is a binary tree with the heap property: the parent of any node has a smaller value than the node itself.

Input: [9, 3, 7, 1, 8, 12, 10, 20, 15, 18, 5]

Output: Root of below Cartesian tree

           1
         /	 \
       /	  \
      /		   \
     3			5
    / \		   /
   /   \	  /
  9		7	 8
              \
               \
                10
               /  \
              /	   \
             12	   15
                   / \
                  /   \
                 20	  18

Explanation: Refer below diagram

https://techiedelight.com/practice/images/Cartesian-Tree.png

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int findMinElementIndex(vector<int> const &inorder, int start, int end)
    {
        int minIndex = start;
        for (int i = start + 1; i <= end; ++i)
        {
            if (inorder[minIndex] > inorder[i]) {
                minIndex = i;
            }
        }
        return minIndex;
    }

    Node* constructTree(vector<int> const &inorder, int start, int end)
    {
        if (start > end) {
            return nullptr;
        }

        int index = findMinElementIndex(inorder, start, end);

        Node* root = new Node(inorder[index]);

        root->left = constructTree(inorder, start, index - 1);
        root->right = constructTree(inorder, index + 1, end);

        return root;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBinaryTree(vector<int> const &inorder)
    {
        return constructTree(inorder, 0, inorder.size() - 1);
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    cout << *Solution().constructBinaryTree({9, 3, 7, 1, 8, 12, 10, 20, 15, 18, 5}) << endl;

    return 0;
}
