/*

Given an integer array whose elements may be misplaced by no more than `k` positions from the correct sorted order, efficiently sort it in linear time and constant space.

Input: nums[] = [1, 4, 5, 2, 3, 7, 8, 6, 10, 9], k = 2
Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

*/

#include <iostream>
#include <vector>
#include <queue>
#include <functional>

using namespace std;

class Solution
{
public:
    void sortKSortedArray(vector<int> &nums, int k)
    {
        priority_queue<int, vector<int>, greater<int>> pq;

        for (int i = 0; i <= k; i++) {
            pq.push(nums[i]);
        }

        int index = 0;

        for (int i = k + 1; i < nums.size(); i++) {
            nums[index++] = pq.top();
            pq.pop();

            pq.push(nums[i]);
        }

        while (!pq.empty()) {
            nums[index++] = pq.top();
            pq.pop();
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}
int main()
{
    vector<int> nums{1, 4, 5, 2, 3, 7, 8, 6, 10, 9};
    cout << "before:\n" << nums << endl;

    Solution().sortKSortedArray(nums, 2);

    cout << "after:\n" << nums << endl;

    return 0;
}
