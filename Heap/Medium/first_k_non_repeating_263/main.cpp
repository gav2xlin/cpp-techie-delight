/*

Given a string, find first `k` non-repeating characters in it by doing only a single traversal of it.

Input: s = "ABCDBAGHCHFAC", k = 3
Output: ['D', 'G', 'F']

Input: s = "ABBCDAB", k = 3
Output: ['C', 'D']

If `k` is more than the non-repeating characters count, return all possible non-repeating characters.

Input: s = "YYXBYX", k = 2
Output: ['B']

Input: s = "YYXBYXB", k = 3
Output: []

Note: The solution should return non-repeating characters in the same order as they appear in the string.

*/

#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <utility>
#include <queue>
#include <functional>

using namespace std;

class Solution
{
public:
    vector<char> findFirstKNonRepeating(string s, int k)
    {
        unordered_map<char, pair<int, int>> freq;
        for (int i = 0; i < s.size(); ++i) {
            freq[s[i]].first++;
            freq[s[i]].second = i;
        }

        priority_queue<int, vector<int>, greater<int>> pq;

        for (auto& it: freq) {
            int count = it.second.first;
            int index = it.second.second;

            if (count == 1) {
                pq.push(index);
            }
        }

        vector<char> chars;
        while (k-- && !pq.empty()) {
            int min_index = pq.top();
            pq.pop();

            chars.push_back(s[min_index]);
        }
        return chars;
    }
};

template<typename T>
ostream& operator<<(ostream& os, const vector<T>& values) {
    for (auto value : values) {
        os << value << ' ';
    }
    return os;
}

int main() {
    cout << Solution().findFirstKNonRepeating("ABCDBAGHCHFAC", 3) << endl;
    cout << Solution().findFirstKNonRepeating("ABBCDAB", 3) << endl;
    cout << Solution().findFirstKNonRepeating("YYXBYX", 2) << endl;
    cout << Solution().findFirstKNonRepeating("YYXBYXB", 3) << endl;

    return 0;
}
