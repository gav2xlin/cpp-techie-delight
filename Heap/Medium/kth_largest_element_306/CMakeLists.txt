cmake_minimum_required(VERSION 3.5)

project(kth_largest_element_306 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(kth_largest_element_306 main.cpp)

install(TARGETS kth_largest_element_306
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
