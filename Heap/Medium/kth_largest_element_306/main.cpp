/*

Given an integer array, find k'th largest element in the array where k is a positive integer less than or equal to the length of array.

Input : [7, 4, 6, 3, 9, 1], k = 2
Output: 7
Explanation: The 2nd largest array element is 7

Input : [1, 5, 2, 2, 2, 5, 5, 4], k = 4
Output: 4
Explanation: The 4th largest array element is 4

*/

#include <iostream>
#include <vector>
#include <queue>
#include <functional>

using namespace std;

class Solution
{
public:
    int findKthLargest(vector<int> const &nums, int k)
    {
        if (nums.size() < k) {
            return -1;
        }

        priority_queue<int, vector<int>> pq(less<int>(), nums);

        while (--k) {
            pq.pop();
        }

        return pq.top();
    }
};

int main()
{
    cout << Solution().findKthLargest({7, 4, 6, 3, 9, 1}, 2) << endl;
    cout << Solution().findKthLargest({1, 5, 2, 2, 2, 5, 5, 4}, 4) << endl;

    return 0;
}
