/*

Given an array representing a max-heap, in-place convert it into the min-heap in linear time.

Input: [9, 4, 7, 1, -2, 6, 5]

           9
         /   \
        /	  \
       4	   7
      / \	  / \
     /   \   /   \
    1	 -2 6	  5


Output: [-2, 1, 5, 9, 4, 6, 7]

           -2
         /	  \
        /	   \
       1		5
      / \	   / \
     /   \	  /   \
    9	  4  6	   7		or, any other valid min-heap.

*/

#include <iostream>
#include <vector>
#include <utility>
#include <functional>

using namespace std;

class Solution
{
private:
    void heapify(vector<int> &nums, int i, int n, function<bool(int, int)> compare) {
        int k = i;
        int l = i * 2 + 1;
        int r = i * 2 + 2;

        if (l < n && compare(nums[l], nums[i])) {
            k = l;
        }
        if (r < n && compare(nums[r], nums[k])) { // nums[r] nums[i]
            k = r;
        }

        if (k != i) {
            swap(nums[i], nums[k]);
            heapify(nums, k, n, compare);
        }
    }
public:
    void convert(vector<int> &nums)
    {
        // heap sort
        /*int n = nums.size();
        for (int i = n / 2 - 1; i >= 0; --i) {
            heapify(nums, i, n, greater{});
        }

        for (int i = n - 1; i >= 0; --i) {
            swap(nums[0], nums[i]);

            heapify(nums, 0, i, greater{});
        }*/

        int n = nums.size();
        for (int i = n / 2 - 1; i >= 0; --i) {
            heapify(nums, i, n, less{});
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    //vector nums{9, 4, 7, 1, -2, 6, 5};
    vector nums{10, 8, 5, 2, 3};

    cout << nums << endl;
    Solution().convert(nums);
    cout << nums << endl;

    return 0;
}
