/*

Given the root of a binary search tree (BST), efficiently convert the BST into a min-heap. The solution should convert the given BST into a complete binary tree where each node has a higher value than its parent's value, using the same set of keys. The output binary tree should satisfy the structural and heap-ordering property of the min-heap data structure.

Input:
         5
       /   \
      /		\
     3		 8
    / \		/ \
   /   \   /   \
  2		4 6	   10

Output:

         2
       /   \
      /		\
     3		 4
    / \		/ \
   /   \   /   \
  5		6 8	   10

OR, any other valid min-heap.

*/

#include <iostream>
#include <vector>
#include <queue>
#include <string>
#include <utility>

using namespace std;

class Node
{
public:
    int data;
    Node* left{nullptr}, *right{nullptr};

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    //
    void inorder(Node* root, queue<int> &keys)
    {
        if (root == nullptr) {
            return;
        }

        inorder(root->left, keys);
        keys.push(root->data);
        inorder(root->right, keys);
    }

    void preorder(Node* root, queue<int> &keys)
    {
        if (root == nullptr) {
            return;
        }

        root->data = keys.front();
        keys.pop();

        preorder(root->left, keys);
        preorder(root->right, keys);
    }
    //
    Node* construct(queue<int> &keys)
    {
        queue<Node*> q;

        Node* root = new Node(keys.front());
        keys.pop();

        q.push(root);

        while (!keys.empty())
        {
            Node* parent = q.front();
            q.pop();

            parent->left = new Node(keys.front());
            keys.pop();

            q.push(parent->left);

            if (!keys.empty())
            {
                parent->right = new Node(keys.front());
                keys.pop();

                q.push(parent->right);
            }
        }

        return root;
    }
    //
    void push(Node* node, Node* &headRef)
    {
        if (headRef == nullptr)
        {
            headRef = node;
            headRef->right = nullptr;
            return;
        }

        node->right = headRef;

        headRef = node;
    }

    void convertTreeToList(Node* root, Node* &headRef)
    {
        if (root == nullptr) {
            return;
        }

        convertTreeToList(root->right, headRef);

        push(root, headRef);

        convertTreeToList(root->left, headRef);

        root->left = nullptr;
    }

    void convertListToMinHeap(Node* &heapRef, Node* head)
    {
        if (head == nullptr) {
            return;
        }

        queue<Node*> q;

        heapRef = head;

        q.push(heapRef);

        head = head->right;

        heapRef->right = nullptr;

        while (head != nullptr)
        {
            Node* parent = q.front();
            q.pop();

            Node* next = head;

            q.push(next);

            head = head->right;

            next->right = nullptr;

            parent->left = next;

            if (head != nullptr)
            {
                next = head;

                q.push(next);

                head = head->right;

                next->right = nullptr;

                parent->right = next;
            }
        }
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void convertBSTToMinHeap(Node* &root)
    {
        /*if (root == nullptr) {
            return;
        }

        queue<int> keys;

        inorder(root, keys);

        preorder(root, keys);*/

        /*if (root == nullptr) {
            return;
        }

        queue<int> keys;

        inorder(root, keys);

        root = construct(keys);*/
        if (root == nullptr) {
            return;
        }

        Node* head = nullptr;

        convertTreeToList(root, head);

        convertListToMinHeap(root, head);
    }
};

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data)
    {
        root->left = insert(root->left, key);
    }
    else
    {
        root->right = insert(root->right, key);
    }

    return root;
}

void printLevelOrderTraversal(Node* root)
{
    if (root == nullptr) {
        return;
    }

    queue<Node*> q;
    q.push(root);

    while (!q.empty())
    {
        int n = q.size();
        while (n--)
        {
            Node* front = q.front();
            q.pop();

            cout << front->data << ' ';

            if (front->left) {
                q.push(front->left);
            }

            if (front->right) {
                q.push(front->right);
            }
        }

        cout << endl;
    }
}

int main()
{
    //vector<int> keys = { 5, 3, 2, 4, 8, 6, 10 };
    vector<int> keys = { 5, 3, 2, 4, 8, 10 };

    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    Solution().convertBSTToMinHeap(root);

    printLevelOrderTraversal(root);

    return 0;
}
