/*

Given M sorted lists of variable length, efficiently compute the smallest range, including at least one element from each list.

Input:

mat = [
    [3, 6, 8, 10, 15],
    [1, 5, 12],
    [4, 8, 15, 16],
    [2, 6],
]

Output: (4, 6)

Input:

mat = [
    [2, 3, 4, 8, 10, 15],
    [1, 5, 12],
    [7, 8, 15, 16],
    [3, 6],
]

Output: (4, 7)

If minimum range doesn't exist, the solution should return the pair (-1, -1).

Input : [[], [], []]
Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <climits>
#include <queue>
#include <algorithm>

using namespace std;

class Solution
{
private:
    struct Node
    {
        // `value` stores the element,
        // `list_num` stores the list number of the element,
        // `index` stores the column number of the list from which element was taken
        int value, list_num, index;
    };

    struct comp
    {
        bool operator()(const Node &lhs, const Node &rhs) const {
            return lhs.value > rhs.value;
        }
    };
public:
    pair<int,int> findMinimumRange(vector<vector<int>> const &lists)
    {
        if (lists.empty()) {
            return {-1, -1};
        }

        int M = lists.size();

        int high = INT_MIN;

        pair<int, int> p = {0, INT_MAX};

        priority_queue<Node, vector<Node>, comp> pq;

        for (int i = 0; i < M; ++i)
        {
            if (lists[i].size() == 0) {
                return {-1, -1};
            }

            pq.push({lists[i][0], i, 0});
            high = max(high, lists[i][0]);
        }

        while (true)
        {
            int low = pq.top().value;
            int i = pq.top().list_num;
            int j = pq.top().index;
            pq.pop();

            if (high - low < p.second - p.first) {
                p = {low, high};
            }

            if (j == lists[i].size() - 1) {
                return p;
            }

            pq.push({lists[i][j + 1], i, j + 1});

            high = max(high, lists[i][j + 1]);
        }

        return {-1, -1};
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    {
        vector<vector<int>> lists
        {
            {3, 6, 8, 10, 15},
            {1, 5, 12},
            {4, 8, 15, 16},
            {2, 6}
        };

        cout << Solution().findMinimumRange(lists) << endl;
    }

    {
        vector<vector<int>> lists
        {
            {2, 3, 4, 8, 10, 15},
            {1, 5, 12},
            {7, 8, 15, 16},
            {3, 6}
        };

        cout << Solution().findMinimumRange(lists) << endl;
    }

    {
        vector<vector<int>> lists
        {
            {},
            {},
            {}
        };

        cout << Solution().findMinimumRange(lists) << endl;
    }

    return 0;
}
