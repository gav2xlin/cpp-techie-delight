/*

Given a set of strings with duplicates present, return the maximum occurring word in it. If two words have the same count, return any one of them.

Input: [code, coder, coding, codable, codec, codecs, coded, codeless, codecs, codependence, codex, codify, codependents, codes, code, coder, codesign, codec, codeveloper, codrive, codec, codecs, codiscovered]

Output: codec or codecs

*/

#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>
#include <utility>

using namespace std;

class Solution
{
private:
    struct Trie
    {
        string word;
        int count{0};

        //unordered_map<char, Trie> chars;
        unordered_map<char, Trie*> chars;
    };

    //void insert(Trie& head, string word) {
    //   Trie* cur = &head;
    void insert(Trie* head, string word) {
        Trie* cur = head;

        for (char c : word) {
            if (!cur->chars.count(c)) {
                cur->chars[c] = new Trie();
            }
            //cur = &cur->chars[c];
            cur = cur->chars[c];
        }

        cur->word = word;
        cur->count += 1;
    }

    //Trie createDictionary(vector<string> const &words) {
    Trie* createDictionary(vector<string> const &words) {
        //Trie root;
        Trie* root = new Trie;
        for(auto& word : words) {
            insert(root, word);
        }
        return root;
    }

    //void findMaxFreqWord(Trie& root, pair<string, int>& maxFreq) {
        //for (auto & p : root.chars) {
        /*if (maxFreq.second < p.second.count) {
            maxFreq.second = p.second.count;
            maxFreq.first = p.second.word;
        }*/
    void findMaxFreqWord(Trie* root, pair<string, int>& maxFreq) {
        for (auto & p : root->chars) {
            if (maxFreq.second < p.second->count) {
                maxFreq.second = p.second->count;
                maxFreq.first = p.second->word;
            }

            findMaxFreqWord(p.second, maxFreq);
        }
    }
public:
    string lexicographicSort(vector<string> const &words)
    {
        //Trie root = createDictionary(words);
        Trie* root = createDictionary(words);

        pair<string, int> maxFreq = make_pair("", 0);
        findMaxFreqWord(root, maxFreq);

        return maxFreq.first;
    }
};

int main()
{
    vector<string> words{"code",
                         "coder",
                         "coding",
                         "codable",
                         "codec",
                         "codecs",
                         "coded",
                         "codeless",
                         "codecs",
                         "codependence",
                         "codex",
                         "codify",
                         "codependents",
                         "codes",
                         "code",
                         "coder",
                         "codesign",
                         "codec",
                         "codeveloper",
                         "codrive",
                         "codec",
                         "codecs",
                         "codiscovered"};
    cout << Solution().lexicographicSort(words) << endl;

    return 0;
}
