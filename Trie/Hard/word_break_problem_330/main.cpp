/*

Given a string and a dictionary of words, determine if the string can be segmented into a space-separated sequence of one or more dictionary words.

Input:

dict = ["this", "th", "is", "famous", "Word", "break", "b", "r", "e", "a", "k", "br", "bre", "brea", "ak", "problem"]
word = "Wordbreakproblem"

Output: true

Explanation: The string can be segmented. The segmented strings are:

Word break problem
Word brea k problem
Word bre ak problem
Word bre a k problem
Word br e ak problem
Word br e a k problem
Word b r e ak problem
Word b r e a k problem

*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <unordered_set>

using namespace std;

#define CHAR_SIZE 26

struct Node
{
    bool exist;
    Node* next[CHAR_SIZE];

    Node()
    {
        exist = false;

        for (int i = 0; i < CHAR_SIZE; ++i) {
            next[i] = nullptr;
        }
    }
};

void freeTrie(Node* node)
{
    if (!node) {
        return;
    }

    for (int i = 0; i < CHAR_SIZE; i++) {
        freeTrie(node->next[i]);
    }

    delete node;
}

void insertTrie(Node* const head, string const &s)
{
    Node* node = head;

    for (char ch: s)
    {
        if (node->next[ch - 'a'] == nullptr) {
            node->next[ch - 'a'] = new Node();
        }

        node = node->next[ch - 'a'];
    }

    node->exist = true;
}

class Solution
{
private:
    /*void wordBreak(vector<string> const &dict, string word, string out)
    {
        if (word.empty())
        {
            cout << out << endl;
            return;
        }

        for (int i = 1; i <= word.size(); ++i)
        {
            string prefix = word.substr(0, i);

            if (find(dict.begin(), dict.end(), prefix) != dict.end()) {
                wordBreak(dict, word.substr(i), out + " " + prefix);
            }
        }
    }*/

    /*bool _wordBreak(vector<string> const &dict, string word)
    {
        if (word.empty()) {
            return true;
        }

        for (int i = 1; i <= word.size(); ++i)
        {
            string prefix = word.substr(0, i);

            if (find(dict.begin(), dict.end(), prefix) != dict.end() && wordBreak(dict, word.substr(i))) {
                return true;
            }
        }

        return false;
    }*/

    /*bool wordBreak(vector<string> const &dict, string word, vector<int> &lookup)
    {
        int n = word.size();
        if (n == 0) {
            return true;
        }

        if (lookup[n] == -1)
        {
            lookup[n] = 0;

            for (int i = 1; i <= n; ++i)
            {
                string prefix = word.substr(0, i);

                if (find(dict.begin(), dict.end(), prefix) != dict.end() && wordBreak(dict, word.substr(i), lookup))
                {
                    return lookup[n] = 1;
                }
            }
        }

        return lookup[n];
    }*/

    bool wordBreak(Node* const head, string const &s)
    {
        int n = s.length();

        vector<bool> good(n + 1);
        good[0] = true;

        for (int i = 0; i < n; ++i)
        {
            if (good[i])
            {
                Node* node = head;
                for (int j = i; j < n; ++j)
                {
                    if (!node) {
                        break;
                    }

                    node = node->next[s[j] - 'a'];

                    if (node && node->exist) {
                        good[j + 1] = true;
                    }
                }
            }
        }

        return good[n];
    }
public:
    bool wordBreak(vector<string> const &dict, string word)
    {
        //return wordBreak(dict, word, "");

        //return _wordBreak(dict, word);

        /*vector<int> lookup(word.length() + 1, -1);
        return wordBreak(dict, word, lookup);*/

        Node* trie = new Node();
        for (string const &s: dict) {
            insertTrie(trie, s);
        }

        bool result = wordBreak(trie, word);
        freeTrie(trie);

        return result;
    }
};

int main()
{
    vector<string> dict = { "this", "th", "is", "famous", "Word", "break", "b", "r", "e", "a", "k", "br", "bre", "brea", "ak", "problem" };
    string word = "Wordbreakproblem";

    cout << boolalpha << Solution().wordBreak(dict, word) << endl;

    return 0;
}
