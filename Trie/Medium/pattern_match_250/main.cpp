/*

Given a set of words where each word follows a CamelCase notation and a pattern containing all uppercase characters, find all words that matches the pattern.

CamelCase Notation is the practice of writing compound words or phrases joined without spaces, where each word's first letter is capitalized. For example, PowerPoint, LibreOffice, CinemaScope, etc., are in CamelCase.

Input: words = {Hi, HiTech, HiTechCity, Hello, HelloWorld, HiTechLab}, pattern = HT
Output: {HiTech, HiTechCity, HiTechLab}

Input: words = {Hi, HiTech, HiTechCity, Hello, HelloWorld, HiTechLab}, pattern = HTC
Output: {HiTechCity}

Input: words = {Hi, HiTech, HiTechCity, Hello, HelloWorld, HiTechLab}, pattern = H
Output: {Hi, HiTech, HiTechCity, Hello, HelloWorld, HiTechLab}

*/

#include <iostream>
#include <string>
#include <cctype>
#include <unordered_set>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    struct TrieNode
    {
        unordered_map<char, TrieNode*> map;
        unordered_set<string> words;
        bool isLeaf = false;
    };

    void insert(TrieNode*& head, string word)
    {
        if (head == nullptr) {
            head = new TrieNode();
        }

        TrieNode* curr = head;
        for (char c: word)
        {
            if (isupper(c))
            {
                if (curr->map.find(c) == curr->map.end()) {
                    curr->map[c] = new TrieNode();
                }

                curr = curr->map[c];
            }
        }

        curr->isLeaf = true;

        curr->words.insert(word);
    }

    void getAllWords(TrieNode* root, unordered_set<string>& res)
    {
        if (root == nullptr) return;

        if (root->isLeaf)
        {
            for (const string& s: root->words) {
                res.insert(s);
            }
        }

        for (auto pair: root->map)
        {
            TrieNode* child = pair.second;
            if (child) {
                getAllWords(child, res);
            }
        }
    }

public:
    unordered_set<string> patternMatch(unordered_set<string> const &words, string pattern)
    {
        if (words.empty()) {
            return {};
        }

        TrieNode* head = nullptr;

        for (const string& s: words) {
            insert(head, s);
        }

        if (head == nullptr) return {};

        TrieNode* curr = head;
        for (char c: pattern)
        {
            curr = curr->map[c];

            if (curr == nullptr) {
                return {};
            }
        }

        unordered_set<string> res;
        getAllWords(curr, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& words) {
    for (auto& w : words) {
        os << w << ' ';
    }
    return os;
}

int main()
{
    unordered_set<string> words {
        "Hi", "HiTech", "HiTechCity", "Techie", "TechieDelight",
        "Hello", "HelloWorld", "HiTechLab"
    };

    string pattern = "HT";

    cout << Solution().patternMatch(words, "HT") << endl;
    cout << Solution().patternMatch(words, "HTC") << endl;
    cout << Solution().patternMatch(words, "H") << endl;

    return 0;
}
