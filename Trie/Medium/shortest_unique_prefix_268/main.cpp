/*

Given a list of words in lexicographic order where no word is the prefix of another, find the shortest unique prefix to identify each word in the array uniquely.

Input: ["AND", "BONFIRE", "BOOL", "CASE", "CATCH", "CHAR"]
Output: ["A", "BON", "BOO", "CAS", "CAT", "CH"]

Explanation:

"A" can uniquely identify "AND"
"BON" can uniquely identify "BONFIRE"
"BOO" can uniquely identify "BOOL"
"CAS" can uniquely identify "CASE"
"CAT" can uniquely identify "CATCH"
"CH" can uniquely identify "CHAR"

*/

#include <iostream>
#include <vector>
#include <string>
#include <map>

using namespace std;

class Solution
{
private:
    struct TrieNode
    {
        map<char, TrieNode*> child;
        int freq = 0;
    };

    void insert(TrieNode* &root, string word)
    {
        TrieNode* curr = root;
        for (char c: word)
        {
            if (curr->child.find(c) == curr->child.end()) {
                curr->child[c] = new TrieNode();
            }

            ++curr->child[c]->freq;
            curr = curr->child[c];
        }
    }

    void getShortestPrefix(TrieNode *root, string word_so_far, vector<string> &prefixes)
    {
        if (root == nullptr) {
            return;
        }

        if (root->freq == 1)
        {
            prefixes.push_back(word_so_far);
            return;
        }

        for (auto &child: root->child) {
            getShortestPrefix(child.second, word_so_far + child.first, prefixes);
        }
    }
public:
    vector<string> findShortestUniquePrefix(vector<string> const &words)
    {
        TrieNode* root = new TrieNode();
        for (auto& s: words) {
            insert(root, s);
        }

        vector<string> prefixes;
        getShortestPrefix(root, "", prefixes);
        return prefixes;
    }
};

ostream& operator<<(ostream& os, const vector<string>& words) {
    for (auto& w : words) {
        os << w << ' ';
    }
    return os;
}

int main()
{
    /*
    Input: ["AND", "BONFIRE", "BOOL", "CASE", "CATCH", "CHAR"]
    Output: ["A", "BON", "BOO", "CAS", "CAT", "CH"]
    */
    cout << Solution().findShortestUniquePrefix({"AND", "BONFIRE", "BOOL", "CASE", "CATCH", "CHAR"}) << endl;

    return 0;
}
