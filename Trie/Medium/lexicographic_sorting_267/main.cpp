/*

Given a set of strings, return them in lexicographic order (dictionary/alphabetical order).

Input: [code, coder, coding, coded, codex, codify, codependents, codes, codesign, codeveloper]
Output: [code, coded, codependents, coder, codes, codesign, codeveloper, codex, codify, coding]

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <map>
#include <string>

using namespace std;

class Solution
{
private:
    struct Trie
    {
        string word;

        //map<char, Trie> chars;
        map<char, Trie*> chars;
    };

    //void insert(Trie& head, string word) {
    //   Trie* cur = &head;
    void insert(Trie* head, string word) {
        Trie* cur = head;

        for (char c : word) {
            if (!cur->chars.count(c)) {
                cur->chars[c] = new Trie();
            }
            //cur = &cur->chars[c];
            cur = cur->chars[c];
        }

        cur->word = word;
    }

    //Trie createDictionary(unordered_set<string> const &words) {
    Trie* createDictionary(unordered_set<string> const &words) {
        //Trie root;
        Trie* root = new Trie;
        for(auto& word : words) {
            insert(root, word);
        }
        return root;
    }

    //void getWordOrder(Trie& root, vector<string>& order) {
        //for (auto & p : root.chars) {
        /*  if (!p.second->word.empty()) {
                order.push_back(p.second->word);
            }*/
    void getWordOrder(Trie* root, vector<string>& order) {
        for (auto & p : root->chars) {
            if (!p.second->word.empty()) {
                order.push_back(p.second->word);
            }

            getWordOrder(p.second, order);
        }
    }
public:
    vector<string> lexicographicSort(unordered_set<string> const &words)
    {
        //Trie root = createDictionary(words);
        Trie* root = createDictionary(words);

        vector<string> order;
        //getWordOrder(root, order);
        getWordOrder(root, order);
        return order;
    }
};

ostream& operator<<(ostream& os, const vector<string>& words) {
    for (auto& w : words) {
        os << w << ' ';
    }
    return os;
}

int main()
{
    unordered_set<string> words{"code",
                                "coder",
                                "coding",
                                "codable",
                                "codec",
                                "codecs",
                                "coded",
                                "codeless",
                                "codecs",
                                "codependence",
                                "codex",
                                "codify",
                                "codependents",
                                "codes",
                                "code",
                                "coder",
                                "codesign",
                                "codec",
                                "codeveloper",
                                "codrive",
                                "codec",
                                "codecs",
                                "codiscovered"};
    cout << Solution().lexicographicSort(words) << endl;

    return 0;
}
