/*

Given the root of a binary tree, return the minimum depth of the binary tree. The minimum depth is the total number of nodes along the shortest path in binary tree from the root node down to the nearest leaf node.

Input:

                1
              /   \
            /		\
          /			  \
         2			   3
       /   \		 /   \
      /		\		/	  \
     4		 5	   6	   7
      \		  \			  / \
       \	   \		 /   \
        8		9		10	 11
         \
          \
          12

Output: 3

Explanation: The shortest path is 1 —> 3 —> 6.

*/

#include <iostream>
#include <algorithm>
#include <queue>
#include <limits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findMaximumDepth(Node* root) // findMinimumDepth
    {
        if (root == nullptr) return 0;

        //return min(findMaximumDepth(root->left), findMaximumDepth(root->right)) + 1;
        queue<Node*> q;
        q.push(root);

        int height = 0, minHeight = numeric_limits<int>::max();

        while (!q.empty()) {
            int size = q.size();

            while (size--) {
                Node* node = q.front();
                q.pop();

                if (node->left) {
                    q.push(node->left);
                }
                if (node->right) {
                    q.push(node->right);
                }

                if (node->left == nullptr && node->right == nullptr) {
                    if (height < minHeight) {
                        minHeight = height;
                    }
                }
            }

            ++height;
        }

        return minHeight + 1;
    }
};

int main()
{
    /*
     *                 1
              /   \
            /		\
          /			  \
         2			   3
       /   \		 /   \
      /		\		/	  \
     4		 5	   6	   7
      \		  \			  / \
       \	   \		 /   \
        8		9		10	 11
         \
          \
          12
     * */
    Node n12{12};
    Node n8{8, nullptr, &n12};
    Node n4{4, nullptr, &n8};
    Node n9{9};
    Node n5{5, nullptr, &n9};
    Node n2{2, &n4, &n5};
    Node n6{6};
    Node n10{10};
    Node n11{11};
    Node n7{7, &n10, &n11};
    Node n3{3, &n6, &n7};
    Node n1{1, &n2, &n3};

    cout << Solution().findMaximumDepth(&n1) << endl;

    return 0;
}
