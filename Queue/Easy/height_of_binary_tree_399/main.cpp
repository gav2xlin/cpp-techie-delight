/*

Given the root of a binary tree, return the binary tree's height. The height of the binary tree is the total number of edges or nodes on the longest path from the root node to the leaf node.

The solution should consider the total number of nodes in the longest path. For example, an empty tree's height is 0, and the tree's height with only one node is 1.

Input:
           1
         /   \
        /	  \
       2	   3
      /	\	  / \
     /	 \ 	 /	 \
    4	  5	6	  7

Output: 3


Input:
           1
          /
         /
        2
       /
      /
     3
    /
   /
  4

Output: 4

*/

#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findHeight(Node* root)
    {
        if (root == nullptr) return 0;

        //return max(findHeight(root->left), findHeight(root->right)) + 1;
        queue<Node*> q;
        q.push(root);

        int height = 0;

        while (!q.empty()) {
            int size = q.size();

            while (size--) {
                Node* node = q.front();
                q.pop();

                if (node->left) {
                    q.push(node->left);
                }
                if (node->right) {
                    q.push(node->right);
                }
            }

            ++height;
        }

        return height;
    }
};

int main()
{
    {
        Node n4{4};
        Node n5{5};
        Node n2{2, &n4, &n5};
        Node n6{6};
        Node n7{7};
        Node n3{3, &n6, &n7};
        Node n1{1, &n2, &n3};

        cout << Solution().findHeight(&n1) << endl;
    }

    {
        Node n4{4};
        Node n3{3, &n4, nullptr};
        Node n2{2, &n3, nullptr};
        Node n1{1, &n2, nullptr};

        cout << Solution().findHeight(&n1) << endl;
    }

    return 0;
}
