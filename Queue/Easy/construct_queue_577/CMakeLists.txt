cmake_minimum_required(VERSION 3.5)

project(construct_queue_577 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(construct_queue_577 main.cpp)

install(TARGETS construct_queue_577
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
