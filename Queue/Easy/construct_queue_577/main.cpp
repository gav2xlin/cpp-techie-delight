/*

You're given an empty Queue class. Write code to support standard queue operations like enqueue, dequeue, peek, size, and isEmpty.

• void enqueue(int val): Inserts element 'val' at the rear (right side) of the queue
• int dequeue(): Removes and return the element from the front (left side) of the queue
• int peek(): Returns the element at the front of the queue without removing it
• int size(): Returns the total number of elements in the queue
• bool isEmpty(): Returns true if the queue is empty; false otherwise

For example,

Input: [enqueue(6), enqueue(5), enqueue(7), isEmpty(), size(), peek(), dequeue(), peek(), dequeue(), peek(), dequeue(), isEmpty(), size()]
Output: [null, null, null, false, 3, 7, 7, 5, 5, 6, 6, true, 0]
Explanation:

Queue s
s.enqueue(6)
s.enqueue(5)
s.enqueue(7)
s.isEmpty() -> false
s.size() -> 3
s.peek() -> 7
s.dequeue() -> 7
s.peek() -> 5
s.dequeue() -> 5
s.peek() -> 6
s.dequeue() -> 6
s.isEmpty() -> true
s.size() -> 0


Input: [isEmpty(), enqueue(6), enqueue(7), enqueue(8), enqueue(5), enqueue(3), peek(), dequeue(), enqueue(10), peek(), dequeue(), peek(), dequeue(), isEmpty(), size()]
Output: [true, null, null, null, null, null, 3, 3, null, 10, 10, 5, 5, false, 3]

Constraints:

• The dequeue and peek operations will never be called on an empty queue.
• The maximum capacity of the queue is 1000.

*/

#include <iostream>
#include <stdexcept>

using namespace std;

constexpr int SIZE = 1000;

class Queue
{
private:
    int arr[SIZE];
    int front{-1}, rear{-1};
public:
    Queue() {}

    ~Queue() {}

    int dequeue() {
        if (isEmpty()) {
            throw out_of_range("The queue is empty");
        }

        int val = arr[front];

        if (front == rear) {
            front = rear = -1;
        } else {
            front = (front + 1) % SIZE;
        }

        return val;
    }

    void enqueue(int val) {
        if (isFull()) {
            throw out_of_range("The queue is full");
        }

        if (front < 0) {
            front = 0;
        }

        rear = (rear + 1) % SIZE;
        arr[rear] = val;
    }

    int peek() {
        if (isEmpty()) {
            throw out_of_range("The queue is empty");
        }

        return arr[front];
    }

    int size() {
        if (isEmpty()) return 0;

        if (front <= rear) {
            return rear - front + 1;
        } else {
            return rear + SIZE - front;
        }
    }

    bool isEmpty() {
        return front < 0 && rear < 0;
    }

    bool isFull() {
        return (front == 0 && rear == SIZE - 1) || (rear + 1 == front);
    }
};

int main()
{
    Queue q;

    cout << boolalpha << q.isEmpty() << endl; // true
    q.enqueue(6);
    q.enqueue(7);
    q.enqueue(8);
    q.enqueue(5);
    q.enqueue(3);
    cout << q.peek() << endl;
    q.dequeue();
    q.enqueue(10);
    cout << q.peek() << endl;
    q.dequeue();
    cout << q.peek() << endl;
    q.dequeue();
    cout << q.isEmpty() << endl;
    cout << q.size() << endl;

    return 0;
}
