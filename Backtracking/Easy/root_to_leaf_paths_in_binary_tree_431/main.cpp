/*

Given the root of a binary tree, return all paths from the root node to every leaf node in the binary tree.

Input:

             1
           /   \
         /		 \
        2		  3
      /  \		 /  \
     /	  \		/	 \
    4	   5   6	  7
              /		   \
             /			\
            8			 9

Output: {[1, 2, 4], [1, 2, 5], [1, 3, 6, 8], [1, 3, 7, 9]}

Explanation: The binary tree has four root-to-leaf paths:

1 —> 2 —> 4
1 —> 2 —> 5
1 —> 3 —> 6 —> 8
1 —> 3 —> 7 —> 9

*/

#include <iostream>
#include <set>
#include <vector>

using namespace std;

class Node {
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution {
private:
    void findRootToLeafPathsInternal(Node* root, set<vector<int>>& paths, vector<int>& path) {
        if (root == nullptr) return;
        path.push_back(root->data);

        if (!root->left && !root->right) {
            paths.insert(path);
        } else {
            findRootToLeafPathsInternal(root->left, paths, path);
            findRootToLeafPathsInternal(root->right, paths, path);
        }

        path.pop_back();
    }

public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    set<vector<int>> findRootToLeafPaths(Node* root) {
        set<vector<int>> paths;
        vector<int> path;

        findRootToLeafPathsInternal(root, paths, path);

        return paths;
    }
};

ostream& operator<<(ostream& os, const set<vector<int>>& paths) {
    for (auto& vec : paths) {
        for (auto& v: vec) {
            os << v << ' ';
        }
        os << endl;
    }
    return os;
}

int main()
{
    Node n4{4};
    Node n5{5};
    Node n2{2, &n4, &n5};
    Node n8{8};
    Node n6{6, &n8, nullptr};
    Node n9{9};
    Node n7{7, nullptr, &n9};
    Node n3{3, &n6, &n7};
    Node n1{1, &n2, &n3};

    cout << Solution().findRootToLeafPaths(&n1) << endl;

    return 0;
}
