/*

Given a directed graph and two vertices - source and destination, determine if the destination vertex is reachable from the source vertex. The solution should return true if a path exists from the source vertex to the destination vertex, false otherwise.

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 5), (4, 1)], n = 6], src = 4, dest = 5
Output: true
Explanation: There exist a path [4 —> 1 —> 2 —> 3 —> 5] from vertex 4 to vertex 5.

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 5), (4, 1)], n = 6], src = 5, dest = 1
Output: false
Explanation: There is no path from vertex 5 to any other vertex.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The destination can be reached from the source.

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Graph {
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

class Solution {
private:
    bool isReachableInternal(Graph const &graph, int src, int dest, unordered_set<int>& visited) {
        if (visited.find(src) != visited.end()) return false;
        visited.insert(src);

        if (src == dest) return true;

        for (auto v : graph.adjList[src]) {
            if (isReachableInternal(graph, v, dest, visited)) {
                return true;
            }
        }

        return false;
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool isReachable(Graph const &graph, int src, int dest) {
        unordered_set<int> visited;
        return isReachableInternal(graph, src, dest, visited);
    }
};

int main()
{
    Graph graph1;
    graph1.n = 6;
    graph1.adjList.resize(graph1.n);
    graph1.adjList[0].push_back(1);
    graph1.adjList[1].push_back(2);
    graph1.adjList[2].push_back(3);
    graph1.adjList[3].push_back(5);
    graph1.adjList[4].push_back(1);

    cout << Solution().isReachable(graph1, 4, 5) << endl; // true

    Graph graph2;
    graph2.n = 6;
    graph2.adjList.resize(graph2.n);
    graph2.adjList[0].push_back(1);
    graph2.adjList[1].push_back(2);
    graph2.adjList[2].push_back(3);
    graph2.adjList[3].push_back(5);
    graph2.adjList[4].push_back(1);

    cout << Solution().isReachable(graph2, 5, 1) << endl; // false

    Graph graph3;
    graph3.n = 8;
    graph3.adjList.resize(graph3.n);
    graph3.adjList[0].push_back(3);
    graph3.adjList[1].push_back(0);
    graph3.adjList[1].push_back(2);
    graph3.adjList[1].push_back(4);
    graph3.adjList[2].push_back(7);
    graph3.adjList[3].push_back(4);
    graph3.adjList[3].push_back(5);
    graph3.adjList[4].push_back(3);
    graph3.adjList[4].push_back(6);
    graph3.adjList[5].push_back(6);
    graph3.adjList[6].push_back(7);

    cout << Solution().isReachable(graph3, 0, 7) << endl; // true

    return 0;
}
