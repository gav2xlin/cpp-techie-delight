/*

Given an undirected graph and a positive integer k, check if the graph is k–colorable or not.

The vertex coloring is a way of coloring the vertices of a graph such that no two adjacent vertices share the same color. A coloring using at most k colors is called a (proper) k–coloring, and a graph that can be assigned a (proper) k–coloring is k–colorable.

Input: Graph [edges = [(0, 1), (0, 4), (0, 5), (4, 5), (1, 4), (1, 3), (2, 3), (2, 4)], n = 6], k = 3

 0 ───── 1 ────── 2
 │ \	 │  \   / │
 │   \   │	  X   │
 │	   \ │  /   \ │
 5 ───── 4 ────── 3

Output: true

Explanation: The graph can be colored using 3 colors in several ways as shown below. However, the graph is not 2–colorable.

 ┌─────────────────────────────────────────┐
 │  #0  │  #1  │  #2  │  #3  │  #4  │  #5  │
 │──────│──────│──────│──────│──────│──────│
 │  C1  │  C2  │  C1  │  C3  │  C3  │  C2  │
 │  C1  │  C2  │  C2  │  C1  │  C3  │  C2  │
 │  C1  │  C2  │  C2  │  C3  │  C3  │  C2  │
 │  C1  │  C3  │  C1  │  C2  │  C2  │  C3  │
 │  C1  │  C3  │  C3  │  C1  │  C2  │  C3  │
 │  C1  │  C3  │  C3  │  C2  │  C2  │  C3  │
 │  C2  │  C1  │  C1  │  C2  │  C3  │  C1  │
 │  C2  │  C1  │  C1  │  C3  │  C3  │  C1  │
 │  C2  │  C1  │  C2  │  C3  │  C3  │  C1  │
 │  C2  │  C3  │  C2  │  C1  │  C1  │  C3  │
 │  C2  │  C3  │  C3  │  C1  │  C1  │  C3  │
 │  C2  │  C3  │  C3  │  C2  │  C1  │  C3  │
 │  C3  │  C1  │  C1  │  C2  │  C2  │  C1  │
 │  C3  │  C1  │  C1  │  C3  │  C2  │  C1  │
 │  C3  │  C1  │  C3  │  C2  │  C2  │  C1  │
 │  C3  │  C2  │  C2  │  C1  │  C1  │  C2  │
 │  C3  │  C2  │  C2  │  C3  │  C1  │  C2  │
 │  C3  │  C2  │  C3  │  C1  │  C1  │  C2  │
 └─────────────────────────────────────────┘


Input: Graph [edges = [(0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)], n = 4], k = 3

 0 ───── 1
 │ \   / │
 │   X   │
 │ /   \ │
 2 ───── 3

Output: false
Explanation: The graph cannot be colored using 3 colors. The graph is 4–colorable.


Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The graph is connected, i.e., every node can be reached starting from all other nodes.

*/

#include <iostream>
#include <vector>
#include <string>
#include <iomanip>

using namespace std;

struct Edge {
    int src, dest;
};

class Graph
{
public:
    vector<vector<int>> adjList;
    int n;

    Graph(vector<Edge> const &edges, int n)
    {
        adjList.resize(n);
        this->n = n;

        for (Edge edge: edges)
        {
            int src = edge.src;
            int dest = edge.dest;

            adjList[src].push_back(dest);
            adjList[dest].push_back(src);
        }
    }
};

class Solution
{
private:
    string COLORS[11] = {"", "BLUE", "GREEN", "RED", "YELLOW", "ORANGE", "PINK", "BLACK", "BROWN", "WHITE", "PURPLE"};

    bool isSafe(Graph const &graph, vector<int> color, int v, int c)
    {
        for (int u: graph.adjList[v])
        {
            if (color[u] == c) {
                return false;
            }
        }

        return true;
    }

    bool kColorable(Graph const &graph, vector<int> &color, int k, int v, int n)
    {
        if (v == n)
        {
            /*for (int v = 0; v < n; ++v) {
                cout << setw(8) << left << COLORS[color[v]];
            }
            cout << endl;*/

            return true;
        }

        //bool colorable = false;
        for (int c = 1; c <= k; ++c)
        {
            if (isSafe(graph, color, v, c))
            {
                color[v] = c;

                if (kColorable(graph, color, k, v + 1, n)) {
                    //colorable = true;
                    return true;
                }

                color[v] = 0;
            }
        }

        //return colorable;
        return false;
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool isKColorable(Graph const &graph, int k)
    {
        vector<int> color(graph.n, 0);
        return kColorable(graph, color, k, 0, graph.n);
    }
};

int main()
{
    vector<Edge> edges = {{0, 1}, {0, 4}, {0, 5}, {4, 5}, {1, 4}, {1, 3}, {2, 3}, {2, 4}};

    Graph g(edges, 6);

    cout << boolalpha << Solution().isKColorable(g, 3) << endl;

    return 0;
}
