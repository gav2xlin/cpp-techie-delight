/*

Given an `M × N` integer matrix, find all paths that start at the first cell (0, 0) and ends at the last cell (M-1, N-1). You are allowed to move down, right, or diagonally (down-right), but not up or left.

Input:
[
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

Output:

{
    [1, 4, 7, 8, 9],
    [1, 4, 5, 8, 9],
    [1, 4, 5, 6, 9],
    [1, 4, 5, 9],
    [1, 4, 8, 9],
    [1, 2, 5, 8, 9],
    [1, 2, 5, 6, 9],
    [1, 2, 5, 9],
    [1, 2, 3, 6, 9],
    [1, 2, 6, 9],
    [1, 5, 8, 9],
    [1, 5, 6, 9],
    [1, 5, 9]
}

*/

#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Solution
{
private:
    void findPaths(vector<vector<int>> const &mat, vector<int> &route, int i, int j, set<vector<int>>& res)
    {
        if (mat.size() == 0) {
            return;
        }

        int m = mat.size();
        int n = mat[0].size();

        if (i == m - 1 && j == n - 1)
        {
            route.push_back(mat[i][j]);
            res.insert(route);
            route.pop_back();

            return;
        }

        route.push_back(mat[i][j]);

        if (i + 1 < m) {
            findPaths(mat, route, i + 1, j, res);
        }

        if (j + 1 < n) {
            findPaths(mat, route, i, j + 1, res);
        }

        if (i + 1 < m && j + 1 < n) {
            findPaths(mat, route, i + 1, j + 1, res);
        }

        route.pop_back();
    }
public:
    set<vector<int>> findAllPaths(vector<vector<int>> const &mat)
    {
        set<vector<int>> res;

        vector<int> route;
        findPaths(mat, route, 0, 0, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> mat{
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };

    cout << Solution().findAllPaths(mat) << endl;

    return 0;
}
