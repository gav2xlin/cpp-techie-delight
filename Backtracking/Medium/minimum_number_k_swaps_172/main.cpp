/*

Given string representation of a positive integer, find the minimum number possible by doing at-most `k` swap operations upon its digits.

Input : s = "934651", k = 1
Output: 134659

Input : s = "934651", k = 2
Output: 134569

Input : s = "52341", k = 2
Output: 12345 (Only 1 swap needed)

Input : s = "12345", k = 2
Output: 12345 (no change as all digits are already sorted in increasing order)

*/

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution {
private:
        void findMin(string s, int k, string &min_so_far)
        {
            if (min_so_far.compare(s) > 0) {
                min_so_far = s;
            }

            if (k < 1) {
                return;
            }

            int n = s.length();
            for (int i = 0; i < n - 1; ++i)
            {
                for (int j = i + 1; j < n; ++j)
                {
                    if (s[i] > s[j])
                    {
                        swap(s[i], s[j]);

                        findMin(s, k - 1, min_so_far);

                        swap(s[i], s[j]);
                    }
                }
            }
        }
public:
    string findMinNumber(string s, int k)
    {
        string min = s;
        findMin(s, k, min);
        return min;
    }
};

int main()
{
    Solution s;

    cout << s.findMinNumber("934651", 1) << endl;
    cout << s.findMinNumber("934651", 2) << endl;
    cout << s.findMinNumber("52341", 2) << endl;
    cout << s.findMinNumber("12345", 2) << endl;

    return 0;
}
