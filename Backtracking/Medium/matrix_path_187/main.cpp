/*

Given a `N × N` binary matrix, find the total number of unique paths that a robot can take to reach a given destination from a given source.

Positions in the matrix will either be open or blocked with an obstacle. The value 0 represents a blocked cell and 1 represents an open cell. The robot can only move to positions without obstacles, i.e., the solution should find paths that contain only open cells. Retracing the one or more cells back and forth is not considered a new path. The set of all cells covered in a single path should be unique from other paths. At any given moment, the robot can only move one step in either of the four directions (Top, Down, Left, Right).

Input:

matrix = [
    [1, 1, 1, 1],
    [1, 1, 0, 1],
    [0, 1, 0, 1],
    [1, 1, 1, 1]
]

src  = (0, 0)
dest = (3, 3)

Output: 4

Explanation: The above matrix contains 4 unique paths from source to destination (Marked with x).

[x  x  x  x]	[x  x  1  1]	[x  1  1  1]	[x  x  x  x]
[1  1  0  x]	[1  x  0  1]	[x  x  0  1]	[x  x  0  x]
[0  1  0  x]	[0  x  0  1]	[0  x  0  1]	[0  1  0  x]
[1  1  1  x]	[1  x  x  x]	[1  x  x  x]	[1  1  1  x]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    bool isValidCell(int x, int y, int N) {
        return !(x < 0 || y < 0 || x >= N || y >= N);
    }

    void countPaths(vector<vector<int>> const &maze, int x, int y, pair<int, int> const &dest, vector<vector<bool>> &visited, int& count)
    {
        if (x == dest.first && y == dest.second)
        {
            ++count;

            return;
        }

        visited[x][y] = true;

        int N = maze.size();

        if (isValidCell(x, y, N) && maze[x][y])
        {
            if (x + 1 < N && !visited[x + 1][y]) {
                countPaths(maze, x + 1, y, dest, visited, count);
            }

            if (x - 1 >= 0 && !visited[x - 1][y]) {
                countPaths(maze, x - 1, y, dest, visited, count);
            }

            if (y + 1 < N && !visited[x][y + 1]) {
                countPaths(maze, x, y + 1, dest, visited, count);
            }

            if (y - 1 >= 0 && !visited[x][y - 1]) {
                countPaths(maze, x, y - 1, dest, visited, count);
            }
        }

        visited[x][y] = false;
    }
public:
    int countPaths(vector<vector<int>> const &maze, pair<int,int> const &src, pair<int,int> const &dest)
    {
        int N = maze.size();

        if (N == 0 || maze[src.first][src.second] == 0 || maze[dest.first][dest.second] == 0) {
            return 0;
        }

        int count = 0;

        vector<vector<bool>> visited;
        visited.resize(N, vector<bool>(N));

        countPaths(maze, src.first, src.second, dest, visited, count);

        return count;
    }
};

int main()
{
    vector<vector<int>> mat{
        {1, 1, 1, 1},
        {1, 1, 0, 1},
        {0, 1, 0, 1},
        {1, 1, 1, 1}
    };

    cout << Solution().countPaths(mat, {0, 0}, {3, 3}) << endl;

    return 0;
}
