/*

Given an integer array, return the total number of ways to calculate the specified target from array elements using only the addition and subtraction operator. The use of any other operator is forbidden.

Input: nums = [5, 3, -6, 2], target = 6
Output: 4
Explanation: There are 4 ways to calculate the target of 6 using only + and - operators:

(-)-6 = 6
(+) 5 (+) 3 (-) 2 = 6
(+) 5 (-) 3 (-) -6 (-) 2 = 6
(-) 5 (+) 3 (-) -6 (+) 2 = 6

Input: nums = [5, 3, -6, 2], target = 4
Output: 4
Explanation: There are 4 ways to calculate the target of 4 using only + and - operators:

(-)-6 (-) 2 = 4
(-) 5 (+) 3 (-)-6 = 4
(+) 5 (-) 3 (+) 2 = 4
(+) 5 (+) 3 (+)-6 (+) 2 = 4

*/

#include <iostream>
#include <vector>
#include <list>
#include <utility>

using namespace std;

class Solution
{
private:
    /*void printList(list<pair<int, char>> const &list)
    {
        for (auto i: list) {
            cout << "(" << i.second << ")" << i.first << " ";
        }
        cout << endl;
    }*/

    int getWays(vector<int> const &nums, int n, int sum, int target, list<pair<int, char>> &list)
    {
        if (sum == target)
        {
            //printList(list);
            return 1;
        }

        if (n < 0) {
            return 0;
        }

        int res = getWays(nums, n - 1, sum, target, list);

        list.push_back({nums[n], '+'});
        res += getWays(nums, n - 1, sum + nums[n], target, list);
        list.pop_back();

        list.push_back({nums[n], '-'});
        res += getWays(nums, n - 1, sum - nums[n], target, list);
        list.pop_back();

        return res;
    }
public:
    int countWays(vector<int> const &nums, int target)
    {
        int n = nums.size();
        list<pair<int, char>> list;

        return getWays(nums, n - 1, 0, target, list);
    }
};

int main()
{
    cout << Solution().countWays({5, 3, -6, 2}, 6) << endl;

    return 0;
}
