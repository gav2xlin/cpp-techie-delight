/*

Given an `M × N` integer matrix, find all paths from the first cell to the last cell. You are only allowed to move down or to the right from the current cell.

Input:

[
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

Output:

{
    [1, 2, 3, 6, 9],
    [1, 2, 5, 6, 9],
    [1, 2, 5, 8, 9],
    [1, 4, 5, 6, 9],
    [1, 4, 5, 8, 9],
    [1, 4, 7, 8, 9]
}

*/

#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Solution
{
private:
    bool isValid(int i, int j, int M, int N) {
        return (i >= 0 && i < M && j >= 0 && j < N);
    }

    void findPaths(vector<vector<int>> const &mat, vector<int> &path, int i, int j, set<vector<int>>& res)
    {
        if (mat.empty()) {
            return;
        }

        int M = mat.size();
        int N = mat[0].size();

        if (i == M - 1 && j == N - 1)
        {
            path.push_back(mat[i][j]);
            res.insert(path);
            path.pop_back();

            return;
        }

        path.push_back(mat[i][j]);

        if (isValid(i, j + 1, M, N)) {
            findPaths(mat, path, i, j + 1, res);
        }

        if (isValid(i + 1, j, M, N)) {
            findPaths(mat, path, i + 1, j, res);
        }

        path.pop_back();
    }

public:
    set<vector<int>> findAllPaths(vector<vector<int>> const &mat)
    {
        set<vector<int>> res;

        vector<int> path;
        int x = 0, y = 0;
        findPaths(mat, path, x, y, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> mat{
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };

    cout << Solution().findAllPaths(mat) << endl;

    return 0;
}
