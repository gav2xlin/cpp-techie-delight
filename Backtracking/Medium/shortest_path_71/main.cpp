/*

Given a maze in the form of a binary rectangular matrix, find the length of the shortest path from a given source to a given destination. The path can only be constructed out of cells having value 1, and at any moment, you can only move one step in one of the four directions (Top, Left, Down, Right).

Input:

matrix = [
    [1, 1, 1, 1, 1, 0, 0, 1, 1, 1],
    [0, 0, 1, 0, 1, 1, 0, 1, 0, 1],
    [0, 0, 1, 0, 1, 1, 1, 0, 0, 1],
    [1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
    [0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 0, 0, 1, 1, 0],
    [0, 0, 0, 0, 1, 0, 0, 1, 0, 1],
    [0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [1, 1, 1, 1, 1, 0, 0, 1, 1, 1],
    [0, 0, 1, 0, 0, 1, 1, 0, 0, 1]
]
src  = (0, 0)
dest = (5, 7)

Output: 12

Explanation: The shortest path from (0, 0) to (5, 7) has length 12. The shortest path is marked below with x.

[
    [x, x, x, x, x, 0, 0, 1, 1, 1],
    [0, 0, 1, 0, x, 1, 0, 1, 0, 1],
    [0, 0, 1, 0, x, x, x, 0, 0, 1],
    [1, 0, 1, 1, 1, 0, x, x, 0, 1],
    [0, 0, 0, 1, 0, 0, 0, x, 0, 1],
    [1, 0, 1, 1, 1, 0, 0, x, 1, 0],
    [0, 0, 0, 0, 1, 0, 0, 1, 0, 1],
    [0, 1, 1, 1, 1, 1, 1, 1, 0, 0],
    [1, 1, 1, 1, 1, 0, 0, 1, 1, 1],
    [0, 0, 1, 0, 0, 1, 1, 0, 0, 1]
]

Note: The solution should return -1 if no path is possible.

*/

#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

class Solution
{
private:
    bool isSafe(vector<vector<int>> const &mat, vector<vector<bool>> &visited, int x, int y) {
        return (x >= 0 && x < mat.size() && y >= 0 && y < mat[0].size()) && mat[x][y] == 1 && !visited[x][y];
    }

    void findShortestPathInternal(vector<vector<int>> const &mat, vector<vector<bool>> &visited, int i, int j, int x, int y, int &min_dist, int dist) {
        if (i == x && j == y) {
            min_dist = min(dist, min_dist);
            return;
        }

        visited[i][j] = true;

        if (isSafe(mat, visited, i + 1, j)) {
            findShortestPathInternal(mat, visited, i + 1, j, x, y, min_dist, dist + 1);
        }

        if (isSafe(mat, visited, i, j + 1)) {
            findShortestPathInternal(mat, visited, i, j + 1, x, y, min_dist, dist + 1);
        }

        if (isSafe(mat, visited, i - 1, j)) {
            findShortestPathInternal(mat, visited, i - 1, j, x, y, min_dist, dist + 1);
        }

        if (isSafe(mat, visited, i, j - 1)) {
            findShortestPathInternal(mat, visited, i, j - 1, x, y, min_dist, dist + 1);
        }

        visited[i][j] = false;
    }
public:
    int findShortestPath(vector<vector<int>> const &mat, pair<int,int> const &src, pair<int,int> const &dest)
    {
        if (mat.size() == 0 || mat[0].size() == 0 || mat[src.first][src.second] == 0 || mat[dest.first][dest.second] == 0) {
            return -1;
        }

        int m = mat.size();
        int n = mat[0].size();

        vector<vector<bool>> visited;
        visited.resize(m, vector<bool>(n));

        int min_dist = numeric_limits<int>::max();
        findShortestPathInternal(mat, visited, src.first, src.second, dest.first, dest.second, min_dist, 0);

        if (min_dist != numeric_limits<int>::max()) {
            return min_dist;
        }

        return -1;
    }
};

int main()
{
    vector<vector<int>> maze{
        {1, 1, 1, 1, 1, 0, 0, 1, 1, 1},
        {0, 0, 1, 0, 1, 1, 0, 1, 0, 1},
        {0, 0, 1, 0, 1, 1, 1, 0, 0, 1},
        {1, 0, 1, 1, 1, 0, 1, 1, 0, 1},
        {0, 0, 0, 1, 0, 0, 0, 1, 0, 1},
        {1, 0, 1, 1, 1, 0, 0, 1, 1, 0},
        {0, 0, 0, 0, 1, 0, 0, 1, 0, 1},
        {0, 1, 1, 1, 1, 1, 1, 1, 0, 0},
        {1, 1, 1, 1, 1, 0, 0, 1, 1, 1},
        {0, 0, 1, 0, 0, 1, 1, 0, 0, 1}
    };

    cout << Solution().findShortestPath(maze, {0, 0} , {5, 7}) << endl;

    return 0;
}
