/*

Given a rectangular path in the form of a binary matrix, find the length of the longest possible route from source to destination by moving to only non-zero adjacent positions, i.e., A route can be formed from positions having their value as 1. Note there should not be any cycles in the output path.

Input:

matrix = [
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 1, 1, 1, 0, 1, 1],
    [1, 1, 1, 0, 1, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 1, 0, 0, 1, 0, 0],
    [1, 0, 0, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [1, 0, 0, 0, 1, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 0, 1, 1],
    [1, 1, 0, 0, 1, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 0, 0]
]
src  = (0, 0)
dest = (5, 7)

Output: 22

Explanation: The longest path is:

(0, 0) —> (1, 0) —> (2, 0) —> (2, 1) —> (2, 2) —> (1, 2) —> (0, 2) —> (0, 3) —> (0, 4) —> (1, 4) —> (1, 5) —> (2, 5) —> (2, 4) —> (3, 4) —> (4, 4) —> (5, 4) —> (5, 5) —> (5, 6) —> (4, 6) —> (4, 7) —> (4, 8) —> (5, 8) —> (5, 7)

Note: The solution should return 0 if no path is possible.

*/

#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

class Solution
{
private:
    bool isSafe(vector<vector<int>> const &mat, vector<vector<bool>> &visited, int x, int y)
    {
        return (x >= 0 && x < mat.size() && y >= 0 && y < mat[0].size()) && mat[x][y] == 1 && !visited[x][y];
    }

    void findLongestPathInternal(vector<vector<int>> const &mat, vector<vector<bool>> &visited, int i, int j, int x, int y, int &max_dist, int dist)
    {
        if (mat[i][j] == 0) {
            return;
        }

        if (i == x && j == y)
        {
            max_dist = max(dist, max_dist);
            return;
        }

        visited[i][j] = true;

        if (isSafe(mat, visited, i + 1, j)) {
            findLongestPathInternal(mat, visited, i + 1, j, x, y, max_dist, dist + 1);
        }

        if (isSafe(mat, visited, i, j + 1)) {
            findLongestPathInternal(mat, visited, i, j + 1, x, y, max_dist, dist + 1);
        }

        if (isSafe(mat, visited, i - 1, j)) {
            findLongestPathInternal(mat, visited, i - 1, j, x, y, max_dist, dist + 1);
        }

        if (isSafe(mat, visited, i, j - 1)) {
            findLongestPathInternal(mat, visited, i, j - 1, x, y, max_dist, dist + 1);
        }

        visited[i][j] = false;
    }
public:
    int findLongestPathLen(vector<vector<int>> const &mat, pair<int,int> const &src, pair<int,int> const &dest)
    {
        if (mat.size() == 0 || mat[0].size() == 0 || mat[src.first][src.second] == 0 || mat[dest.first][dest.second] == 0) {
            return 0;
        }

        int m = mat.size();
        int n = mat[0].size();

        vector<vector<bool>> visited;
        visited.resize(m, vector<bool>(n));

        int max_dist = 0;
        findLongestPathInternal(mat, visited, src.first, src.second, dest.first, dest.second, max_dist, 0);

        return max_dist;
    }
};

int main()
{
    vector<vector<int>> maze{
        {1, 0, 1, 1, 1, 1, 0, 1, 1, 1},
        {1, 0, 1, 0, 1, 1, 1, 0, 1, 1},
        {1, 1, 1, 0, 1, 1, 0, 1, 0, 1},
        {0, 0, 0, 0, 1, 0, 0, 1, 0, 0},
        {1, 0, 0, 0, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
        {1, 0, 0, 0, 1, 0, 0, 1, 0, 1},
        {1, 0, 1, 1, 1, 1, 0, 0, 1, 1},
        {1, 1, 0, 0, 1, 0, 0, 0, 0, 1},
        {1, 0, 1, 1, 1, 1, 0, 1, 0, 0}
    };

    cout << Solution().findLongestPathLen(maze, {0, 0} , {5, 7}) << endl;

    return 0;
}
