/*

Given a positive integer `n`, find all combinations of numbers between 1 and `n` having sum `n`.

Input : n = 4
Output: {[4], [1, 3], [2, 2], [1, 1, 2], [1, 1, 1, 1]}

Input : n = 5
Output: {[5], [1, 4], [2, 3], [1, 1, 3], [1, 2, 2], [1, 1, 1, 2], [1, 1, 1, 1, 1]}

The solution should return a set containing all the distinct combinations in increasing order.

*/

#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Solution
{
private:
    void findCombinations(int i, int n, vector<int> &out, set<vector<int>>& combinations)
    {
        // if the sum becomes `n`, print the combination
        if (n == 0) {
            combinations.insert(out);
        }

        for (int j = i; j <= n; ++j)
        {
            out.push_back(j);
            findCombinations(j, n - j, out, combinations);
            out.pop_back();
        }
    }
public:
    set<vector<int>> findCombinations(int n)
    {
        set<vector<int>> combinations;

        vector<int> out;
        findCombinations(1, n, out, combinations);

        return combinations;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}
ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findCombinations(4) << endl;
    cout << Solution().findCombinations(5) << endl;

    return 0;
}
