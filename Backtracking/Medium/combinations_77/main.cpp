/*

Given a positive number `n`, find all combinations of `2×n` elements such that every element from 1 to `n` appears exactly twice and the distance between its two appearances is exactly equal to the value of the element.

Input : n = 3
Output: {[3, 1, 2, 1, 3, 2], [2, 3, 1, 2, 1, 3]}

Input : n = 4
Output: {[4, 1, 3, 1, 2, 4, 3, 2], [2, 3, 4, 2, 1, 3, 1, 4]}

Input : n = 5
Output: {}

*/

#include <iostream>
#include <set>
#include <vector>

using namespace std;

class Solution
{
private:
    void findAllCombinations(vector<int> &arr, int x, int n, set<vector<int>>& combinations)
    {
        if (x > n)
        {
            combinations.insert(arr);

            return;
        }

        for (int i = 0; i < 2 * n; ++i)
        {
            if (arr[i] == -1 && (i + x + 1) < 2 * n && arr[i + x + 1] == -1)
            {
                arr[i] = x;
                arr[i + x + 1] = x;

                findAllCombinations(arr, x + 1, n, combinations);

                arr[i] = -1;
                arr[i + x + 1] = -1;
            }
        }
    }
public:
    set<vector<int>> findCombinations(int n)
    {
        set<vector<int>> combinations;

        vector<int> arr(2*n, -1);
        findAllCombinations(arr, 1, n, combinations);

        return combinations;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}
ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findCombinations(3) << endl;
    cout << Solution().findCombinations(4) << endl;
    cout << Solution().findCombinations(5) << endl;

    return 0;
}
