/*

Given an `N × N` matrix of positive integers, find a path from the first cell of the matrix to its last.

You are allowed to move exactly `k` steps from any cell in the matrix where `k` is the cell's value, i.e., from a cell (i, j) having value `k` in a matrix `M`, you can move to (i+k, j), (i-k, j), (i, j+k), or (i, j-k). The diagonal moves are not allowed.

In case multiple paths exists, the solution can return any one of them.

Input:

M = [
    [7, 1, 3, 5, 3, 6, 1, 1, 7, 5],
    [2, 3, 6, 1, 1, 6, 6, 6, 1, 2],
    [6, 1, 7, 2, 1, 4, 7, 6, 6, 2],
    [6, 6, 7, 1, 3, 3, 5, 1, 3, 4],
    [5, 5, 6, 1, 5, 4, 6, 1, 7, 4],
    [3, 5, 5, 2, 7, 5, 3, 4, 3, 6],
    [4, 1, 4, 3, 6, 4, 5, 3, 2, 6],
    [4, 4, 1, 7, 4, 3, 3, 1, 4, 2],
    [4, 4, 5, 1, 5, 2, 3, 5, 3, 5],
    [3, 6, 3, 5, 2, 2, 6, 4, 2, 1]
]

Output: [(0, 0) (7, 0) (3, 0) (9, 0) (6, 0) (2, 0) (8, 0) (4, 0) (4, 5) (0, 5) (6, 5) (2, 5) (2, 1) (1, 1) (4, 1) (9, 1) (3, 1) (3, 7) (2, 7) (8, 7) (8, 2) (3, 2) (3, 9) (7, 9) (9, 9)]

or

[(0, 0) (7, 0) (3, 0) (9, 0) (6, 0) (2, 0) (8, 0) (4, 0) (4, 5) (8, 5) (6, 5) (2, 5) (2, 9) (4, 9) (8, 9) (3, 9) (7, 9) (9, 9)]

or any other valid path from first cell to last cell.

*/

#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>

using namespace std;

class Solution
{
private:
    using Node = pair<int, int>;

    int row[4] = {-1, 0, 0, 1};
    int col[4] = {0, -1, 1, 0};

    bool isValid(vector<Node> const &path, Node pt, int n)
    {
        return (pt.first >= 0) && (pt.first < n) && (pt.second >= 0) && (pt.second < n) && (find(path.begin(), path.end(), pt) == path.end());
    }

    bool findPath(vector<vector<int>> const &mat, vector<Node> &path, Node &curr)
    {
        if (mat.empty()) {
            return false;
        }

        path.push_back(curr);

        int N = mat.size();

        if (curr.first == N - 1 && curr.second == N - 1) {
            return true;
        }

        int n = mat[curr.first][curr.second];

        for (int i = 0; i < 4; ++i)
        {
            int x = curr.first + row[i] * n;
            int y = curr.second + col[i] * n;

            Node next = make_pair(x, y);

            if (isValid(path, next, N) && findPath(mat, path, next)) {
                return true;
            }
        }

        path.pop_back();

        return false;
    }
public:
    vector<Node> findPath(vector<vector<int>> const &mat)
    {
        vector<Node> path;

        Node source = make_pair(0, 0);
        if (findPath(mat, path, source)) {
            return path;
        } else {
            return {};
        }
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& pair) {
    os << pair.first << ':' << pair.second;
    return os;
}

ostream& operator<<(ostream& os, const vector<pair<int,int>>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<vector<int>> mat={
        {7, 1, 3, 5, 3, 6, 1, 1, 7, 5},
        {2, 3, 6, 1, 1, 6, 6, 6, 1, 2},
        {6, 1, 7, 2, 1, 4, 7, 6, 6, 2},
        {6, 6, 7, 1, 3, 3, 5, 1, 3, 4},
        {5, 5, 6, 1, 5, 4, 6, 1, 7, 4},
        {3, 5, 5, 2, 7, 5, 3, 4, 3, 6},
        {4, 1, 4, 3, 6, 4, 5, 3, 2, 6},
        {4, 4, 1, 7, 4, 3, 3, 1, 4, 2},
        {4, 4, 5, 1, 5, 2, 3, 5, 3, 5},
        {3, 6, 3, 5, 2, 2, 6, 4, 2, 1}
    };

    cout << Solution().findPath(mat) << endl;

    return 0;
}
