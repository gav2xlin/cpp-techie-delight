/*

Given a string, find all permutations of it.

Input : "ABC"
Output: ["ABC", "ACB", "BAC", "BCA", "CAB", "CBA"]

There are exacly n! permutations for a string of length n, and the solution should return all permutations in any order. For strings containing duplicate characters, the solution should return duplicate permutations.

Input : "ABA"
Output: ["ABA", "AAB", "BAA", "BAA", "AAB", "ABA"]

*/

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

using namespace std;

class Solution
{
private:
    /*void permutations(string str, int i, int n, vector<string> &res)
    {
        if (i == n - 1)
        {
            res.push_back(str);
            return;
        }

        for (int j = i; j < n; ++j)
        {
            swap(str[i], str[j]);

            permutations(str, i + 1, n, res);

            swap(str[i], str[j]);
        }
    }*/
    void permutations(string str, int n, string result, vector<string> &res)
    {
        if (n == 1)
        {
            res.push_back(result + str);
            return;
        }

        for (int i = 0; i < n; ++i)
        {
            permutations(str.substr(1), n - 1, result + str[0], res);

            rotate(str.begin(), str.begin() + 1, str.end());
        }
    }
public:
    vector<string> findPermutations(string str)
    {
        vector<string> res;
        //permutations(str, 0, str.length(), res);
        string result;
        permutations(str, str.size(), result, res);
        return res;
    }
};

ostream& operator<<(ostream& os, const vector<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findPermutations("ABC") << endl;
    cout << Solution().findPermutations("ABA") << endl;

    return 0;
}
