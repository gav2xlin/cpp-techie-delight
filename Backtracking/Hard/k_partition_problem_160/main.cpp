/*

Given an array of positive integers, partition it into `k` disjoint subsets that all have an equal sum, and they completely cover the array.

Input : S[] = [7, 3, 5, 12, 2, 1, 5, 3, 8, 4, 6, 4], k = 2
Output: [[5, 3, 8, 4, 6, 4], [7, 3, 5, 12, 2, 1]] or [[4, 5, 6, 7, 8], [1, 2, 3, 3, 4, 5, 12]]
Explanation: S can be partitioned into 2 partitions, each having a sum of 30.

Input : S[] = [7, 3, 5, 12, 2, 1, 5, 3, 8, 4, 6, 4], k = 3
Output: [[2, 1, 3, 4, 6, 4], [7, 5, 8], [3, 5, 12]]
Explanation: S can be partitioned into 3 partitions, each having a sum of 20.

Input : S[] = [7, 3, 5, 12, 2, 1, 5, 3, 8, 4, 6, 4], k = 4
Output: [[1, 4, 6, 4], [2, 5, 8], [12, 3], [7, 3, 5]]
Explanation: S can be partitioned into 4 partitions, each having a sum of 15.

Input : S[] = [7, 3, 5, 12, 2, 1, 5, 3, 8, 4, 6, 4], k = 5
Output: [[2, 6, 4], [8, 4], [3, 1, 5, 3], [12], [7, 5]]
Explanation: S can be partitioned into 5 partitions, each having a sum of 12.

Input : S[] = [3, 5, 7, 3, 2, 1], k = 2
Output: []

*/

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

class Solution
{
private:
    bool checkSum(vector<int> &sumLeft, int k)
    {
        int r = true;
        for (int i = 0; i < k; ++i)
        {
            if (sumLeft[i] != 0) {
                r = false;
            }
        }

        return r;
    }

    // Helper function for solving `k` partition problem.
    // It returns true if there exist `k` subsets with the given sum
    bool subsetSum(const vector<int> &S, int n, vector<int> &sumLeft, vector<int> &A, int k)
    {
        if (checkSum(sumLeft, k)) {
            return true;
        }

        if (n < 0) {
            return false;
        }

        bool result = false;

        // consider current item `S[n]` and explore all possibilities
        // using backtracking
        for (int i = 0; i < k; ++i)
        {
            if (!result && (sumLeft[i] - S[n]) >= 0)
            {
                // mark the current element subset
                A[n] = i + 1;

                // add the current item to the i'th subset
                sumLeft[i] = sumLeft[i] - S[n];

                // recur for remaining items
                result = subsetSum(S, n - 1, sumLeft, A, k);

                // backtrack: remove the current item from the i'th subset
                sumLeft[i] = sumLeft[i] + S[n];
            }
        }

        return result;
    }
public:
    vector<vector<int>> getSubsets(vector<int> const &nums, int k)
    {
        int n = nums.size();
        if (n < k)
        {
            return {};
        }

        int sum = accumulate(nums.begin(), nums.end(), 0);

        vector<int> A(n), sumLeft(k);

        // create an array of size `k` for each subset and initialize it
        // by their expected sum, i.e., `sum/k`
        for (int i = 0; i < k; ++i) {
            sumLeft[i] = sum / k;
        }

        // return true if the sum is divisible by `k` and set `S` can
        // be divided into `k` subsets with equal sum
        bool result = !(sum % k) && subsetSum(nums, n - 1, sumLeft, A, k);

        if (!result)
        {
            return {};
        }

        vector<vector<int>> subsets;
        for (int i = 0; i < k; ++i)
        {
            vector<int> subset;
            for (int j = 0; j < n; ++j)
            {
                if (A[j] == i + 1) {
                    subset.push_back(nums[j]);
                }
            }
            subsets.push_back(subset);
        }

        return subsets;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().getSubsets({7, 3, 5, 12, 2, 1, 5, 3, 8, 4, 6, 4}, 2) << endl;
    cout << Solution().getSubsets({7, 3, 5, 12, 2, 1, 5, 3, 8, 4, 6, 4}, 3) << endl;
    cout << Solution().getSubsets({7, 3, 5, 12, 2, 1, 5, 3, 8, 4, 6, 4}, 4) << endl;
    cout << Solution().getSubsets({7, 3, 5, 12, 2, 1, 5, 3, 8, 4, 6, 4}, 5) << endl;
    cout << Solution().getSubsets({3, 5, 7, 3, 2, 1}, 2) << endl;

    return 0;
}
