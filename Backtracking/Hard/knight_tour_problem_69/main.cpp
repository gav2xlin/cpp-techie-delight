/*

Given a chessboard, return all sequences of moves of a knight on a chessboard such that the knight visits every square only once. The solution should start the tour from the top-leftmost of the board, mark it as 1, and use the next numbers to represent the knight's consecutive moves.

Input: N = 5

Output:

{
    [[1, 18, 7, 12, 25], [8, 13, 2, 19, 6], [3, 20, 17, 24, 11], [14, 9, 22, 5, 16], [21, 4, 15, 10, 23]],
    [[1, 10, 21, 16, 7], [20, 15, 8, 11, 22], [9, 2, 23, 6, 17], [14, 19, 4, 25, 12], [3, 24, 13, 18, 5]],
    [[1, 18, 13, 8, 3], [12, 7, 2, 21, 14], [17, 22, 19, 4, 9], [6, 11, 24, 15, 20], [23, 16, 5, 10, 25]],
    [[1, 18, 13, 8, 21], [12, 7, 20, 3, 14], [19, 2, 17, 22, 9], [6, 11, 24, 15, 4], [25, 16, 5, 10, 23]]

    ...and 300 more tours.
}

*/

#include <iostream>
#include <set>
#include <vector>

using namespace std;

class Solution
{
private:
    int row[9] {2, 1, -1, -2, -2, -1, 1, 2, 2};
    int col[9] {1, 2, 2, 1, -1, -2, -2, -1, 1};

    bool isValid(int x, int y, int n)
    {
        return x >= 0 && y >= 0 && x < n && y < n;
    }

    void knightTour(vector<vector<int>> &visited, int x, int y, int pos, set<vector<vector<int>>> &result)
    {
        visited[x][y] = pos;

        int n = visited.size();
        if (pos >= n * n)
        {
            /*for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j) {
                    cout << visited[i][j] << " ";
                }
                cout << endl;
            }
            cout << endl;*/
            result.insert(visited);

            visited[x][y] = 0;
            return;
        }

        for (int k = 0; k < 8; ++k)
        {
            int newX = x + row[k];
            int newY = y + col[k];

            if (isValid(newX, newY, n) && !visited[newX][newY]) {
                knightTour(visited, newX, newY, pos + 1, result);
            }
        }

        visited[x][y] = 0;
    }
public:
    set<vector<vector<int>>> _knightTour(int n)
    {
        set<vector<vector<int>>> result;

        if (n > 0) {
            vector<vector<int>> visited(n, vector<int>(n));

            knightTour(visited, 0, 0, 1, result);
        }

        return result;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<vector<int>>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution()._knightTour(5) << endl;

    return 0;
}
