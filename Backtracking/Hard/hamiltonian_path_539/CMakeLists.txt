cmake_minimum_required(VERSION 3.5)

project(hamiltonian_path_539 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(hamiltonian_path_539 main.cpp)

install(TARGETS hamiltonian_path_539
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
