/*

Given an undirected graph, return all Hamiltonian paths present in it. The Hamiltonian path is a path that visits each vertex exactly once.

Input: Graph [edges = [(0, 1), (0, 2), (0, 3), (1, 2), (1, 3), (2, 3)], n = 4]
Output: {[0, 1, 2, 3], [0, 1, 3, 2], [0, 2, 1, 3], [0, 2, 3, 1], [0, 3, 1, 2], [0, 3, 2, 1], [1, 0, 2, 3], [1, 0, 3, 2], [1, 2, 0, 3], [1, 2, 3, 0], [1, 3, 0, 2], [1, 3, 2, 0], [2, 0, 1, 3], [2, 0, 3, 1], [2, 1, 0, 3], [2, 1, 3, 0], [2, 3, 0, 1], [2, 3, 1, 0], [3, 0, 1, 2], [3, 0, 2, 1], [3, 1, 0, 2], [3, 1, 2, 0], [3, 2, 0, 1], [3, 2, 1, 0]}

Input: Graph [edges = [(0, 1), (2, 3)], n = 4]
Output: {}
Explanation: The graph is not connected.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.

*/

#include <iostream>
#include <set>
#include <vector>

using namespace std;

struct Edge {
    int src, dest;
};

class Graph
{
public:
    vector<vector<int>> adjList;
    int n;

    Graph(vector<Edge> const &edges, int n)
    {
        adjList.resize(n);
        this->n = n;

        for (Edge edge: edges)
        {
            int src = edge.src;
            int dest = edge.dest;

            adjList[src].push_back(dest);
            adjList[dest].push_back(src);
        }
    }
};

class Solution
{
private:
    void hamiltonianPaths(Graph const &graph, int v, vector<bool> &visited, vector<int> &path, int n, set<vector<int>> &res)
    {
        if (path.size() == n)
        {
            res.insert(path);
            return;
        }

        for (int w: graph.adjList[v])
        {
            if (!visited[w])
            {
                visited[w] = true;
                path.push_back(w);

                hamiltonianPaths(graph, w, visited, path, n, res);

                // backtrack
                visited[w] = false;
                path.pop_back();
            }
        }
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    set<vector<int>> findHamiltonianPaths(Graph const &graph)
    {
        set<vector<int>> res;

        int n = graph.n;
        for (int start = 0; start < n; ++start)
        {
            vector<int> path;
            path.push_back(start);

            vector<bool> visited(n);
            visited[start] = true;

            hamiltonianPaths(graph, start, visited, path, n, res);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    {
        vector<Edge> edges = {
            {0, 1}, {0, 2}, {0, 3}, {1, 2}, {1, 3}, {2, 3}
        };
        Graph graph(edges, 4);

        cout << Solution().findHamiltonianPaths(graph) << endl;
    }

    {
        vector<Edge> edges = {
            {0, 1}, {2, 3}
        };
        Graph graph(edges, 4);

        cout << Solution().findHamiltonianPaths(graph) << endl;
    }

    return 0;
}
