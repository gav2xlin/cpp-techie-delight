/*

Given a list of edges of a directed acyclic graph (DAG), return a set of all topological orderings of the graph.

Input: edges = [(0, 1), (0, 2)]
Output: {[0, 1, 2], [0, 2, 1]}

Input: edges = [(0, 2), (1, 2), (2, 3), (2, 4), (3, 4), (3, 5)]
Output: {[0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 5, 4], [1, 0, 2, 3, 4, 5], [1, 0, 2, 3, 5, 4]}

Constraints:

• The maximum number of nodes in the graph is 100.
• There are no cycles in the graph.

*/

#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <list>

using namespace std;

class Edge {
public:
    int src, dest;
};

class Graph
{
public:
    vector<vector<int>> adjList;
    vector<int> indegree;

    Graph(vector<Edge> const &edges, int n)
    {
        adjList.resize(n);
        indegree.resize(n);

        for (auto &edge: edges)
        {
            adjList[edge.src].push_back(edge.dest);

            indegree[edge.dest]++;
        }
    }
};

class Solution
{
private:
    int getn(vector<Edge> const &edges) {
        int n = 0;
        for (auto &e : edges) {
            n = max(n, max(e.src, e.dest) + 1);
        }
        return n;
    }

    void findAllTopologicalOrderings(Graph &graph, auto &path, auto &discovered, int n, set<vector<int>> &res)
    {
        for (int v = 0; v < n; ++v)
        {
            if (graph.indegree[v] == 0 && !discovered[v])
            {
                for (int u: graph.adjList[v]) {
                    graph.indegree[u]--;
                }

                path.push_back(v);
                discovered[v] = true;

                findAllTopologicalOrderings(graph, path, discovered, n, res);

                for (int u: graph.adjList[v]) {
                    graph.indegree[u]++;
                }

                path.pop_back();
                discovered[v] = false;
            }
        }

        if (path.size() == n) {
            res.insert({path.begin(), path.end()});
        }
    }
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int src, dest;
        };
    */

    set<vector<int>> findAllTopologicalOrdering(vector<Edge> const &edges)
    {
        int n = getn(edges);
        Graph graph(edges, n);

        set<vector<int>> res;

        // n = graph.adjList.size();

        vector<bool> discovered(n);
        list<int> path;

        findAllTopologicalOrderings(graph, path, discovered, n, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    {
        vector<Edge> edges {
            {0, 1}, {0, 2}
        };

        cout << Solution().findAllTopologicalOrdering(edges) << endl;
    }

    {
        vector<Edge> edges {
            {0, 2}, {1, 2}, {2, 3}, {2, 4}, {3, 4}, {3, 5}
        };

        cout << Solution().findAllTopologicalOrdering(edges) << endl;
    }

    /*{
        vector<Edge> edges {
            {0, 6}, {1, 2}, {1, 4}, {1, 6}, {3, 0}, {3, 4}, {5, 1}, {7, 0}, {7, 1}
        };

        cout << Solution().findAllTopologicalOrdering(edges) << endl;
    }*/

    return 0;
}
