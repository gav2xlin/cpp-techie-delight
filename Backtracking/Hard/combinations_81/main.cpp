/*

Given n lists of characters and a number whose digits lie between 1 and n, print all possible combinations by replacing its digits with the characters of the corresponding list. If any digit of the number gets repeated, it should be replaced by the same character considered in its previous occurrence.

Input:

lists =
[
    ['A', 'B', 'C', 'D'],
    ['E', 'F', 'G', 'H', 'I', 'J', 'K'],
    ['L', 'M', 'N', 'O', 'P', 'Q'],
    ['R', 'S', 'T'],
    ['U', 'V', 'W', 'X', 'Y', 'Z']
]

keys = [0, 2, 0]

Output: {"ALA", "AMA", "ANA", "AOA", "APA", "AQA", "BLB", "BMB", "BNB", "BOB", "BPB", "BQB", "CLC", "CMC", "CNC", "COC", "CPC", "CQC", "DLD", "DMD", "DND", "DOD", "DPD", "DQD"}

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    void findCombinations(auto const &list, auto const &keys, auto &combinations, string result, int index, auto map)
    {
        if (index == -1) {
            combinations.insert(result);
            return;
        }

        int digit = keys[index];
        int len = list[digit].size();

        if (map.find(digit) == map.end())
        {
            for (int i = 0; i < len; ++i)
            {
                map[digit] = list[digit][i];

                findCombinations(list, keys, combinations, list[digit][i] + result, index - 1, map);
            }
            return;
        }

        findCombinations(list, keys, combinations, map[digit] + result, index - 1, map);
    }
public:
    unordered_set<string> findCombinations(vector<vector<char>> const &lists, vector<int> const &keys)
    {
        if (lists.size() == 0 || keys.size() == 0) {
            return {};
        }

        unordered_set<string> combinations;
        unordered_map<int, char> map;

        int n = keys.size();
        findCombinations(lists, keys, combinations, "", n - 1, map);

        return combinations;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<vector<char>> lists {
        { 'A', 'B', 'C', 'D'},
        { 'E', 'F', 'G', 'H', 'I', 'J', 'K' },
        { 'L', 'M', 'N', 'O', 'P', 'Q' },
        { 'R', 'S', 'T'},
        { 'U', 'V', 'W', 'X', 'Y', 'Z' }
    };

    vector<int> keys = {0, 2, 0};

    cout << Solution().findCombinations(lists, keys) << endl;

    return 0;
}
