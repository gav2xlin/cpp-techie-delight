/*

Given a string and a pattern, determine whether a string matches with a given pattern.

Input: word = "codesleepcode", pattern = "XYX"
Output: true
Explanation: 'X' can be mapped to "code" and 'Y' can be mapped to "sleep"

Input: word = "codecodecode", pattern = "XXX"
Output: true
Explanation: 'X' can be mapped to "code"

*/

#include <iostream>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    bool isMatch(string word, int i, string pattern, int j, auto &map)
    {
        int n = word.size(), m = pattern.size();

        if (n < m) {
            return false;
        }

        if (i == n && j == m) {
            return true;
        }

        if (i == n || j == m) {
            return false;
        }

        char curr = pattern[j];

        if (map.find(curr) != map.end())
        {
            string s = map[curr];
            int k = s.size();

            if (word.substr(i, k).compare(s)) {
                return false;
            }

            return isMatch(word, i + k, pattern, j + 1, map);
        }

        for (int k = 1; k <= n - i; ++k)
        {
            map[curr] = word.substr(i, k);

            if (isMatch(word, i + k, pattern, j + 1, map)) {
                return true;
            }

            map.erase(curr);
        }

        return false;
    }
public:
    bool checkPattern(string word, string pattern)
    {
        unordered_map<char, string> mapping;
        return isMatch(word, 0, pattern, 0, mapping);
    }
};

int main()
{
    cout << boolalpha << Solution().checkPattern("codesleepcode", "XYX") << endl;
    cout << boolalpha << Solution().checkPattern("codecodecode", "XXX") << endl;

    return 0;
}
