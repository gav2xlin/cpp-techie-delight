/*

Given a positive number n between 2 and 9, find all n–digit numbers with an equal sum of digits at even and odd indices.

Input: n = 3
Output: {110, 121, 132, 143, 154, 165, 176, 187, 198, 220, 231, 242, 253, 264, 275, 286, 297, 330, 341, 352, 363, 374, 385, 396, 440, 451, 462, 473, 484, 495, 550, 561, 572, 583, 594, 660, 671, 682, 693, 770, 781, 792, 880, 891, 990}

Input: n = 5
Output: {10010, 10021, 10032, 10043, 10054, 10065, 10076, 10087, 10098, 10120, 10131, 10142, 10153, 10164, 10175, 10186, 10197, 10230, 10241, 10252, 10263, 10274, 10285, 10296, 10340, 10351, 10362, 10373, 10384, 10395, 10450, 10461, 10472, 10483, 10494, 10560, 10571, 10582, 10593, 10670, 10681, 10692, 10780, 10791, 10890, 11000, 11011, 11022, 11033, 11044, 11055, 11066, 11077, 11088, 11099, 11110, 11121, 11132, 11143, 11154, 11165, 11176, 11187, 11198, 11220}

*/

#include <iostream>
#include <unordered_set>
#include <cmath>
#include <string>

using namespace std;

class Solution
{
private:
    void findNdigitNums(char result[], int index, int n, int diff, unordered_set<int> &res)
    {
        if (index < n)
        {
            char d = '0';

            if (index == 0) {
                d = '1';
            }

            while (d <= '9')
            {
                if (index & 1) {
                    diff += (d - '0');
                }
                else {
                    diff -= (d - '0');
                }

                result[index] = d;
                findNdigitNums(result, index + 1, n, diff, res);

                if (index & 1) {
                    diff -= (d - '0');
                }
                else {
                    diff += (d - '0');
                }

                ++d;
            }
        }
        else if (index == n && abs(diff) == 0)
        {
            res.insert(stoi(result));
        }
    }
public:
    unordered_set<int> findNDigitNumbers(int n)
    {
        char result[n + 1];
        result[n] = '\0';

        unordered_set<int> res;
        findNdigitNums(result, 0, n, 0, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findNDigitNumbers(3) << endl;
    cout << Solution().findNDigitNumbers(5) << endl;

    return 0;
}
