/*

The N–queens puzzle is the problem of placing `N` chess queens on an `N × N` chessboard so that no two queens threaten each other. Thus, the solution requires that no two queens share the same row, column, or diagonal.

The solution should return a Set of each possible solution to the N–Queens problem.

Input: N = 4
Output:
{
    [[0, 0, 1, 0], [1, 0, 0, 0], [0, 0, 0, 1], [0, 1, 0, 0]],
    [[0, 1, 0, 0], [0, 0, 0, 1], [1, 0, 0, 0], [0, 0, 1, 0]]
}

Here 1 represents the position of a queen in chessboard. Note that the solution exists for all natural numbers, except for 2 and 3. The solution should return an empty set for N = 2 and N = 3.

Input: N = 2
Output: {}

*/

#include <iostream>
#include <set>
#include <vector>

using namespace std;

class Solution {
private:
    bool isSafe(vector<vector<int>>& mat, int r, int c)
    {
        for (int i = 0; i < r; ++i)
        {
            if (mat[i][c] != 0) {
                return false;
            }
        }

        for (int i = r, j = c; i >= 0 && j >= 0; --i, --j)
        {
            if (mat[i][j] != 0) {
                return false;
            }
        }

        int n = mat.size();
        for (int i = r, j = c; i >= 0 && j < n; --i, ++j)
        {
            if (mat[i][j] != 0) {
                return false;
            }
        }

        return true;
    }

    void nQueen(vector<vector<int>> mat, int r, set<vector<vector<int>>>& result)
    {
        int n = mat.size();
        if (r == n)
        {
            result.insert(mat);
            return;
        }

        for (int i = 0; i < n; ++i)
        {
            if (isSafe(mat, r, i))
            {
                mat[r][i] = 1;

                nQueen(mat, r + 1, result);

                mat[r][i] = 0;
            }
        }
    }
public:
    set<vector<vector<int>>> solveNQueen(int n)
    {
        set<vector<vector<int>>> result;

        if (n > 0) {
            vector<vector<int>> mat(n, vector<int>(n, 0));
            nQueen(mat, 0, result);
        }

        return result;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<vector<int>>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().solveNQueen(4) << endl;
    cout << Solution().solveNQueen(2) << endl;

    return 0;
}
