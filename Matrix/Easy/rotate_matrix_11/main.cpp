/*

Given an `N × N` integer matrix, rotate the matrix by 90 degrees in a clockwise direction. The transformation should be done in-place and in quadratic time.

Input:

[
    [1,  2,  3,  4],
    [5,  6,  7,  8],
    [9,  10, 11, 12],
    [13, 14, 15, 16],
]

Output:

[
    [13, 9,  5, 1],
    [14, 10, 6, 2],
    [15, 11, 7, 3],
    [16, 12, 8, 4]
]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    void rotateMatrix(vector<vector<int>> &mat)
    {
        int n = mat.size();
        if (!n) return;

        int m = mat[0].size();

        vector<vector<int>> rot(m, vector<int>(n));
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                rot[j][n - i - 1] = mat[i][j];
            }
        }

        mat.swap(rot);
    }
};

ostream& operator<<(ostream& os, const vector<int> &vec) {
    for (auto& col : vec) {
        os << col << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>> &mat) {
    for (auto& row : mat) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> mat{{1,  2,  3,  4},
                            {5,  6,  7,  8},
                            {9,  10, 11, 12},
                            {13, 14, 15, 16}};

    // 3, [0-3] -> [0-3], 0
    // 2, [0-3] -> [0-3], 1
    // 1, [0-3] -> [0-3], 2
    // 0, [0-3] -> [0-3], 3

    cout << mat << endl;
    Solution().rotateMatrix(mat);
    cout << mat << endl;

    return 0;
}
