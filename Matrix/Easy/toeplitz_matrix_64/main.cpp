/*

Given an `M × N` integer matrix, check if it is a Toeplitz matrix or not. A Toeplitz matrix or diagonal-constant matrix is a matrix with each descending diagonal from left to right is constant.

Any `M × N` matrix `mat` is a Toeplitz matrix if mat(i, j) = mat(i+1, j+1) = mat(i+2, j+2), and so on… Here, mat(i, j) denotes the element mat[i][j] in the matrix.

Input:

[
    [3, 7, 0, 9, 8],
    [5, 3, 7, 0, 9],
    [6, 5, 3, 7, 0],
    [4, 6, 5, 3, 7]
]

Output: true

Input:

[
    [3, 7, 0, 9, 8],
    [5, 3, 7, 0, 9],
    [6, 5, 3, 7, 0],
    [4, 5, 5, 3, 7]
]

Output: false

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    bool checkToeplitz(vector<vector<int>> const &mat)
    {
        if (mat.size() == 0 || mat[0].size() == 0) return true;

        int n = mat.size() - 1, m = mat[0].size() - 1;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                if (mat[i][j] != mat[i + 1][j + 1]) return false;
            }
        }

        return true;
    }
};

int main()
{
    {
        vector<vector<int>> mat{
            {3, 7, 0, 9, 8},
            {5, 3, 7, 0, 9},
            {6, 5, 3, 7, 0},
            {4, 6, 5, 3, 7}
        };
        cout << boolalpha << Solution().checkToeplitz(mat) << endl;
    }

    {
        vector<vector<int>> mat{
            {3, 7, 0, 9, 8},
            {5, 3, 7, 0, 9},
            {6, 5, 3, 7, 0},
            {4, 5, 5, 3, 7}
        };
        cout << boolalpha << Solution().checkToeplitz(mat) << endl;
    }

    return 0;
}
