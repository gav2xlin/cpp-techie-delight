/*

Given an `M × N` row-wise and column-wise sorted integer matrix, find the total number of negative numbers in it in linear time.

Input:

[
    [-7, -3, -1, 3, 5],
    [-3, -2,  2, 4, 6],
    [-1,  1,  3, 5, 8],
    [ 3,  4,  7, 8, 9]
]

Output: 6

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int negativeCount(vector<vector<int>> const &mat)
    {
        int res = 0;

        for (int i = 0; i < mat.size(); ++i) {
            vector<int> const &vec = mat[i];
            int low = 0, high = vec.size() - 1;

            while (low <= high) {
               int mid = (low + high) / 2;

               if (vec[mid] < 0) {
                   low = mid + 1;
               } else if (vec[mid] >= 0) {
                   high = mid - 1;
               }
            }

            res += low;
        }

        return res;
    }
};

int main()
{
    vector<vector<int>> mat{
        {-7, -3, -1, 3, 5},
        {-3, -2,  2, 4, 6},
        {-1,  1,  3, 5, 8},
        { 3,  4,  7, 8, 9}
    };

    cout << Solution().negativeCount(mat) << endl;

    return 0;
}
