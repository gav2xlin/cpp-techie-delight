/*

Given an `M × N` integer matrix, return all its diagonal elements having a positive slope.

Input:

[
    [1, 2, 3, 4, 5],
    [2, 3, 4, 5, 6],
    [3, 4, 5, 6, 7],
    [4, 5, 6, 7, 8],
    [5, 6, 7, 8, 9]
]

Output:

[
    [1]
    [2, 2],
    [3, 3, 3],
    [4, 4, 4, 4],
    [5, 5, 5, 5, 5],
    [6, 6, 6, 6],
    [7, 7, 7],
    [8, 8],
    [9]
]

*/

#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

class Solution
{
public:
    vector<vector<int>> diagonalTraversal(vector<vector<int>> const &mat)
    {
        if (mat.empty() || mat[0].empty()) return {};

        int m = mat.size();
        int n = mat[0].size();
        vector<vector<int>> traversal(m + n - 1);

        for (int r = 0; r < m; ++r) {
            for (int i = r, j = 0; j < n && i >= 0; i--, j++) {
                traversal[r].push_back(mat[i][j]);
            }
        }

        for (int c = 1; c < n; c++) {
            for (int i = m - 1, j = c; j < n && i >= 0; i--, j++) {
                traversal[m + c - 1].push_back(mat[i][j]);
            }
        }

        return traversal;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << setw(2) << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> mat{
        {1, 2, 3, 4, 5},
        {2, 3, 4, 5, 6},
        {3, 4, 5, 6, 7},
        {4, 5, 6, 7, 8},
        {5, 6, 7, 8, 9}
    };

    cout << Solution().diagonalTraversal(mat) << endl;

    return 0;
}
