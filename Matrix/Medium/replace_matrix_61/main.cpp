/*

Given an `M × N` binary matrix, replace all occurrences of 0’s by 1’s, which are not completely surrounded by 1’s from all sides, i.e., top, left, bottom, right, top-left, top-right, bottom-left, and bottom-right.

Input:

[
    [1, 1, 1, 1, 0, 0, 1, 1, 0, 1],
    [1, 0, 0, 1, 1, 0, 1, 1, 1, 1],
    [1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 0, 1, 1, 0, 1],
    [1, 1, 1, 1, 0, 0, 0, 1, 0, 1],
    [1, 1, 0, 1, 1, 0, 1, 1, 0, 0],
    [1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 0, 1, 1, 0, 0, 1, 0, 1],
    [1, 1, 1, 0, 1, 0, 1, 0, 0, 1],
    [1, 1, 1, 0, 1, 1, 1, 1, 1, 1]
]

Output:

[
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 0, 0, 1, 1, 1],
    [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 0, 0, 1, 0, 1],
    [1, 1, 1, 1, 1, 0, 1, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int row[8] = {-1, -1, -1, 0, 0, 1, 1, 1};
    int col[8] = {-1, 0, 1, -1, 1, -1, 0, 1};

    bool isValid(int x, int y, int m, int n) {
        return x >= 0 && x < m && y >= 0 && y < n;
    }

    void dfs(vector<vector<int>> &mat, int x, int y)
    {
        int M = mat.size();
        int N = mat[0].size();

        mat[x][y] = 1;  // replace 0 by 1

        for (int k = 0; k < 8; ++k)
        {
            int i = x + row[k];
            int j = y + col[k];

            if (isValid(i, j, M, N) && !mat[i][j]) {
                dfs(mat, i, j);
            }
        }
    }
public:
    void replaceZeros(vector<vector<int>> &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return;
        }

        int m = mat.size();
        int n = mat[0].size();

        for (int i = 0; i < m; ++i)
        {
            if (!mat[i][0]) {
                dfs(mat, i, 0);
            }

            if (!mat[i][n - 1]) {
                dfs(mat, i, n - 1);
            }
        }

        for (int j = 0; j < n - 1; ++j)
        {
            if (!mat[0][j]) {
                dfs(mat, 0, j);
            }

            if (!mat[m - 1][j]) {
                dfs(mat, m - 1, j);
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> matrix{
        {1, 1, 1, 1, 0, 0, 1, 1, 0, 1},
        {1, 0, 0, 1, 1, 0, 1, 1, 1, 1},
        {1, 0, 0, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 0, 0, 1, 1, 0, 1},
        {1, 1, 1, 1, 0, 0, 0, 1, 0, 1},
        {1, 1, 0, 1, 1, 0, 1, 1, 0, 0},
        {1, 1, 0, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 0, 1, 1, 0, 0, 1, 0, 1},
        {1, 1, 1, 0, 1, 0, 1, 0, 0, 1},
        {1, 1, 1, 0, 1, 1, 1, 1, 1, 1}
    };

    cout << "before:\n" << matrix << endl;

    Solution().replaceZeros(matrix);

    cout << "after:\n" << matrix << endl;

    return 0;
}
