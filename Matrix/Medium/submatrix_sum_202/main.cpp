/*

Given an `M × N` integer matrix and two coordinates (p, q) and (r, s) representing top-left and bottom-right coordinates of a submatrix of it, calculate the sum of all elements present in the submatrix.

Input:

mat =
[
    [0, 2, 5, 4, 1],
    [4, 8, 2, 3, 7],
    [6, 3, 4, 6, 2],
    [7, 3, 1, 8, 3],
    [1, 5, 7, 9, 4]
]

(p, q) = (1, 1)
(r, s) = (3, 3)

Output: 38

Explanation: The submatrix formed by coordinates (p, q), (p, s), (r, q), and (r, s) is shown below, having the sum of elements equal to 38.

 8  2  3
 3  4  6
 3  1  8

Assume that `m` lookup calls are made to the matrix; the task is to achieve constant time lookups.

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    vector<vector<int>> preprocess(vector<vector<int>> const &mat)
    {
        int m = mat.size();
        int n = mat[0].size();

        vector<vector<int>> sum(m, vector<int>(n));

        sum[0][0] = mat[0][0];

        for (int j = 1; j < n; ++j) {
            sum[0][j] = mat[0][j] + sum[0][j - 1];
        }

        for (int i = 1; i < m; ++i) {
            sum[i][0] = mat[i][0] + sum[i - 1][0];
        }

        for (int i = 1; i < m; ++i)
        {
            for (int j = 1; j < n; ++j)
            {
                sum[i][j] = mat[i][j] + sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1];
            }
        }

        return sum;
    }
public:
    int findSubmatrixSum(vector<vector<int>> const &mat, int p, int q, int r, int s)
    {
        if (mat.size() == 0) {
            return 0;
        }

        vector<vector<int>> sum = preprocess(mat);

        // `total` is `sum[r][s] - sum[r][q-1] - sum[p-1][s] + sum[p-1][q-1]`
        int total = sum[r][s];

        if (q - 1 >= 0) {
            total -= sum[r][q - 1];
        }

        if (p - 1 >= 0) {
            total -= sum[p - 1][s];
        }

        if (p - 1 >= 0 && q - 1 >= 0) {
            total += sum[p - 1][q - 1];
        }

        return total;
    }
};

int main()
{
    vector<vector<int>> mat{
        {0, 2, 5, 4, 1},
        {4, 8, 2, 3, 7},
        {6, 3, 4, 6, 2},
        {7, 3, 1, 8, 3},
        {1, 5, 7, 9, 4}
    };

    int p = 1, q = 1, r = 3, s = 3;

    cout << Solution().findSubmatrixSum(mat, p, q, r, s) << endl;

    return 0;
}
