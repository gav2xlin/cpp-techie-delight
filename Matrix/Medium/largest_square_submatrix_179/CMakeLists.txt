cmake_minimum_required(VERSION 3.5)

project(largest_square_submatrix_179 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(largest_square_submatrix_179 main.cpp)

install(TARGETS largest_square_submatrix_179
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
