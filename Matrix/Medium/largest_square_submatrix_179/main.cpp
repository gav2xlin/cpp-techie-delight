/*

Given a binary matrix, find size of the largest square submatrix, which is surrounded by all 1’s.

Input:
[
    [1, 1, 1, 1, 1, 1],
    [1, 0, 1, 1, 0, 1],
    [0, 1, 1, 0, 0, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 0, 0],
    [1, 0, 1, 0, 1, 1],
    [1, 1, 1, 0, 1, 1]
]

Output: 4

Explanation: The size of the largest square submatrix in the following binary matrix is 4. The largest square submatrix is formed by cells (0, 2), (3, 2), (0, 5), and (3, 5).

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>

using namespace std;

class Solution
{
public:
    int findLargestSquareSubMatrix(vector<vector<int>> const &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return 0;
        }

        int m = mat.size();
        int n = mat[0].size();

        vector<vector<int>> X(m, vector<int>(n));
        vector<vector<int>> Y(m, vector<int>(n));
        /*int X[m][n];
        int Y[m][n];
        memset(X, 0, sizeof(X));
        memset(Y, 0, sizeof(Y));*/

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < m; ++j)
            {
                if (mat[i][j])
                {
                    Y[i][j] = i != 0 ? Y[i - 1][j] + 1 : 1;

                    X[i][j] = j != 0 ? X[i][j - 1] + 1 : 1;
                }
            }
        }

        int max_length = 0;

        for (int i = m - 1; i >= 0; --i)
        {
            for (int j = n - 1; j >= 0; --j)
            {
                // The minimum of `X[i][j]` and `Y[i][j]` would be the length of the
                // right vertical line and bottom horizontal line of the
                // square matrix ending at cell `(i, j)`

                int len = min(X[i][j], Y[i][j]);
                while (len)
                {
                    // the cell ending at the current cell `(i, j)` forms a square
                    // submatrix if there exists a left vertical line and a
                    // top horizontal line of at least length `len`

                    bool isSquare = Y[i][j - len + 1] >= len && X[i - len + 1][j] >= len;

                    if (isSquare && max_length < len) {
                        max_length = len;
                    }

                    --len;
                }
            }
        }

        return max_length;
    }
};

int main()
{
    /*vector<vector<int>> mat{
        {1, 1, 1, 1, 1, 1},
        {1, 0, 1, 1, 0, 1},
        {0, 1, 1, 0, 0, 1},
        {1, 1, 1, 1, 1, 1},
        {1, 0, 0, 1, 0, 1},
        {1, 0, 1, 1, 0, 0},
        {1, 0, 1, 0, 1, 1},
        {1, 1, 1, 0, 1, 1}
    };*/
    vector<vector<int>> mat{
        {1, 1, 1, 1, 0, 0},
        {1, 0, 0, 1, 1, 0},
        {0, 0, 0, 1, 1, 1},
        {1, 1, 1, 1, 0, 0},
        {1, 1, 1, 1, 0, 0},
        {1, 1, 0, 1, 1, 0},
        {1, 1, 0, 1, 1, 1},
        {1, 1, 0, 1, 1, 0}
    };

    cout << Solution().findLargestSquareSubMatrix(mat) << endl;

    return 0;
}
