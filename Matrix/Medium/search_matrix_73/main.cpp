/*

Given an `M × N` integer matrix, which is row-wise and column-wise sorted (with all strictly increasing elements in any row or column), report the coordinates of all occurrences of a given element in it in linear time.

Input:

matrix =
[
    [-4, -3, -1, 3, 5],
    [-3, -2, 2, 4, 6],
    [-1, 1, 3, 5, 8],
    [3, 4, 7, 8, 9]
]

target = 3

Output: {(0, 3), (2, 2), (3, 0)}

*/

#include <iostream>
#include <vector>
#include <set>
#include <utility>

using namespace std;

class Solution
{
public:
    set<pair<int, int>> findElement(vector<vector<int>> const &mat, int key)
    {
        if (mat.empty() || mat[0].empty()) {
            return {};
        }

        set<pair<int, int>> pairs;

        int m = mat.size();
        int n = mat[0].size();

        int i = 0, j = n - 1;

        while (i <= m - 1 && j >= 0)
        {
            if (mat[i][j] < key) {
                ++i;
            } else if (mat[i][j] > key) {
                --j;
            } else {
                pairs.insert({i, j});

                ++i;
                --j;
            }
        }

        return pairs;
    }
};

ostream& operator<<(ostream& os, const pair<int, int>& pair) {
    os << '(' << pair.first << ',' << pair.second << ')';
    return os;
}

ostream& operator<<(ostream& os, const set<pair<int,int>>& pairs) {
    os << '{';
    bool flag = false;
    for (auto pair = pairs.cbegin(); pair != pairs.cend() ; ++pair) {
        if (pair != pairs.cbegin()) os << ", ";
        os << *pair;
    }
    os << '}';
    return os;
}

int main()
{
    vector<vector<int>> matrix{
        {-4, -3, -1, 3, 5},
        {-3, -2, 2, 4, 6},
        {-1, 1, 3, 5, 8},
        {3, 4, 7, 8, 9}
    };
    cout << Solution().findElement(matrix, 3) << endl;

    return 0;
}
