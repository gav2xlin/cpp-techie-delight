/*

Given an `M × N` binary matrix, replace all occurrences of 0’s by 1’s, which are completely surrounded by 1’s from all sides (top, left, bottom, right, top-left, top-right, bottom-left, and bottom-right).

Input:

[
    [1, 1, 1, 1, 0, 0, 1, 1, 0, 1],
    [1, 0, 0, 1, 1, 0, 1, 1, 1, 1],
    [1, 0, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 0, 0, 1, 1, 0, 1],
    [0, 0, 1, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 0, 1, 1, 0, 1, 1, 0, 0],
    [1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 0, 1, 1, 0, 0, 1, 0, 1],
    [1, 1, 1, 0, 1, 0, 1, 0, 0, 1],
    [1, 1, 1, 0, 1, 1, 1, 1, 1, 1]
]

Output:

[
    [1, 1, 1, 1, 0, 0, 1, 1, 0, 1],
    [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
    [0, 0, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 1, 1, 1, 1, 1, 0, 0],
    [1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 0, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 0, 1, 1, 1, 1, 1, 1]
]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int row[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
    int col[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };

    bool isSafe(vector<vector<int>> &mat, int x, int y, int m, int n, int target)
    {
        return (x >= 0 && x < m && y >= 0 && y < n) && mat[x][y] == target;
    }

    void floodfill(vector<vector<int>> &mat, int x, int y, int m, int n, int replacement) // dfs
    {
        int target = mat[x][y];

        mat[x][y] = replacement;

        for (int k = 0; k < 8; ++k)
        {
            if (isSafe(mat, x + row[k], y + col[k], m, n, target)) {
                floodfill(mat, x + row[k], y + col[k], m, n, replacement);
            }
        }
    }
public:
    void replaceZeros(vector<vector<int>> &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return;
        }

        int m = mat.size();
        int n = mat[0].size();

        for (int i = 0; i < n; ++i)
        {
            if (mat[0][i] == 0) {
                floodfill(mat, 0, i, m, n, -1);
            }

            if (mat[m - 1][i] == 0) {
                floodfill(mat, m - 1, i, m, n, -1);
            }
        }

        for (int i = 0; i < m; ++i)
        {
            if (mat[i][0] == 0) {
                floodfill(mat, i, 0, m, n, -1);
            }

            if (mat[i][n - 1] == 0) {
                floodfill(mat, i, n - 1, m, n, -1);
            }
        }

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; j++)
            {
                if (mat[i][j] == 0) {
                    mat[i][j] = 1;
                }

                if (mat[i][j] == -1) {
                    mat[i][j] = 0;
                }
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> matrix{
        {1, 1, 1, 1, 0, 0, 1, 1, 0, 1},
        {1, 0, 0, 1, 1, 0, 1, 1, 1, 1},
        {1, 0, 0, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 0, 0, 1, 1, 0, 1},
        {1, 1, 1, 1, 0, 0, 0, 1, 0, 1},
        {1, 1, 0, 1, 1, 0, 1, 1, 0, 0},
        {1, 1, 0, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 0, 1, 1, 0, 0, 1, 0, 1},
        {1, 1, 1, 0, 1, 0, 1, 0, 0, 1},
        {1, 1, 1, 0, 1, 1, 1, 1, 1, 1}
    };

    cout << "before:\n" << matrix << endl;

    Solution().replaceZeros(matrix);

    cout << "after:\n" << matrix << endl;

    return 0;
}
