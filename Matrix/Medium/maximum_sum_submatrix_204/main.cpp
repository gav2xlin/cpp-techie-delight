/*

Given an `N × N` matrix, find the maximum sum submatrix present in it.

Input:

mat = [
    [-5, -6, 3, 1, 0],
    [9, 7, 8, 3, 7],
    [-6, -2, -1, 2, -4],
    [-7, 5, 5, 2, -6],
    [3, 2, 9, -5, 1]
]

Output:

[
    [7, 8, 3],
    [-2, -1, 2],
    [5, 5, 2],
    [2, 9, -5]
]

In case the multiple maximum sum submatrix exists, the solution can return any one of them.

*/

#include <iostream>
#include <vector>
#include <iomanip>
#include <climits>

using namespace std;

class Solution
{
public:
    vector<vector<int>> findMaxSumSubmatrix(vector<vector<int>> const &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return {};
        }

        int M = mat.size();
        int N = mat[0].size();

        int S[M+1][N+1];

        for (int i = 0; i <= M; ++i)
        {
            for (int j = 0; j <= N; ++j)
            {
                if (i == 0 || j == 0) {
                    S[i][j] = 0;
                } else {
                    S[i][j] = S[i-1][j] + S[i][j-1] - S[i-1][j-1] + mat[i-1][j-1];
                }
            }
        }

        int maxSum = INT_MIN;
        int rowStart, rowEnd, colStart, colEnd;

        // consider every submatrix formed by row `i` to `j` and column `m` to `n`
        for (int i = 0; i < M; ++i)
        {
            for (int j = i; j < M; ++j)
            {
                for (int m = 0; m < N; ++m)
                {
                    for (int n = m; n < N; ++n)
                    {
                        int submatrix_sum = S[j+1][n+1] - S[j+1][m] - S[i][n+1] + S[i][m];

                        if (submatrix_sum > maxSum)
                        {
                            maxSum = submatrix_sum;

                            rowStart = i, rowEnd = j;
                            colStart = m, colEnd = n;
                        }
                    }
                }
            }
        }

        vector<vector<int>> res;
        if (maxSum != INT_MIN) {
            for (int i = rowStart; i <= rowEnd; ++i) {
                vector<int> row;
                for (int j = colStart; j <= colEnd; ++j) {
                    row.push_back(mat[i][j]);
                }
                res.push_back(row);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << setw(2) << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> mat =
    {
        {-5, -6, 3, 1, 0},
        {9, 7, 8, 3, 7},
        {-6, -2, -1, 2, -4},
        {-7, 5, 5, 2, -6},
        {3, 2, 9, -5, 1}
    };

    cout << Solution().findMaxSumSubmatrix(mat) << endl;

    return 0;
}
