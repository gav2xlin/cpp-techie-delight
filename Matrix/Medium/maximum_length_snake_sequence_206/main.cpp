/*

Given an integer square matrix, find the maximum length snake sequence in it and return its length. A snake sequence is defined as a sequence of numbers where each new number, which can only be located to the right or down of the current number, is either plus or minus one.

You are only allowed to move either right or down from any cell in the matrix, provided that the new cell has value `±1` relative to the current cell.

Input:

grid = [
    [7, 5, 2, 3, 1],
    [3, 4, 1, 4, 4],
    [1, 5, 6, 7, 8],
    [3, 4, 5, 8, 9],
    [3, 2, 2, 7, 6]
]

Output: 7

Explanation: The maximum length snake sequence for the above matrix is [5, 4, 5, 6, 7, 8, 7, 6] or [3, 4, 5, 6, 7, 8, 7, 6].

Note that the length of a snake sequence is one less than the count of numbers in it.

*/

#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

using namespace std;

class Solution
{
private:
    using Point = pair<int, int>;

    vector<Point> constructPath(vector<vector<int>> const &L, vector<vector<int>> const &grid, Point tail)
    {
        vector<Point> path;
        path.push_back(tail);

        int i = tail.first;
        int j = tail.second;

        while (L[i][j])
        {
            if (i - 1 >= 0 && L[i][j] - L[i - 1][j] == 1 && abs(grid[i - 1][j] - grid[i][j]) == 1) // with the top cell
            {
                path.push_back(make_pair(i - 1, j));
                --i;
            }
            else if (j - 1 >= 0 && (L[i][j] - L[i][j - 1] == 1) && abs(grid[i][j - 1] - grid[i][j]) == 1) // with the left cell
            {
                path.push_back(make_pair(i, j - 1));
                --j;
            }
        }

        return path;
    }

    void printSnakeSequence(vector<vector<int>> const &grid, vector<Point> const &path)
    {
        if (path.size() == 0) {
            return;
        }

        auto it = path.rbegin();
        while (it != path.rend()) {
            cout << grid[it->first][it->second];
            if (++it != path.rend()) {
                cout << " — ";
            }
        }

        cout << endl << "The length is " << path.size() - 1;
    }
public:
    int findMaxLengthSnakeSequence(vector<vector<int>> const &grid)
    {
        int n = grid.size();
        if (n == 0) {
            return 0;
        }

        vector<vector<int>> L(n, vector<int>(n));

        int max_so_far = 0;
        Point tail;

        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                L[i][j] = 0;

                if (i - 1 >= 0 && abs(grid[i - 1][j] - grid[i][j]) == 1) // with the top cell
                {
                    L[i][j] = L[i - 1][j] + 1;
                    if (max_so_far < L[i][j])
                    {
                        max_so_far = L[i][j];
                        tail = make_pair(i, j);
                    }
                }

                if (j - 1 >= 0 && abs(grid[i][j - 1] - grid[i][j]) == 1) // with the left cell
                {
                    L[i][j] = max(L[i][j], L[i][j - 1] + 1);

                    if (max_so_far < L[i][j])
                    {
                        max_so_far = L[i][j];
                        tail = make_pair(i, j);
                    }
                }
            }
        }

        vector<Point> path = constructPath(L, grid, tail);

        //printSnakeSequence(grid, path);

        return !path.empty() ? path.size() - 1 : 0;
    }
};

int main()
{
    vector<vector<int>> grid{
        {7, 5, 2, 3, 1},
        {3, 4, 1, 4, 4},
        {1, 5, 6, 7, 8},
        {3, 4, 5, 8, 9},
        {3, 2, 2, 7, 6}
    };

    cout << Solution().findMaxLengthSnakeSequence(grid) << endl;

    return 0;
}
