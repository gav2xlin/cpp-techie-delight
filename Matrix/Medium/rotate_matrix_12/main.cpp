/*

Given an `N × N` integer matrix, rotate the matrix by 180 degrees in a clockwise direction. The transformation should be done in-place in quadratic time.

Input:

[
    [1,  2,  3,  4],
    [5,  6,  7,  8],
    [9,  10, 11, 12],
    [13, 14, 15, 16]
]

Output:

[
    [16, 15, 14, 13],
    [12, 11, 10, 9],
    [8,  7,  6,  5],
    [4,  3,  2,  1]
]

*/

#include <iostream>
#include <vector>
#include <iomanip>
#include <utility>

using namespace std;

class Solution
{
public:
    void rotateMatrix(vector<vector<int>> &mat)
    {
         if (mat.empty() || mat[0].empty()) return;

         int n = mat.size(), m = mat[0].size();
         for (int i = 0; i < n / 2; ++i) {
             for (int j = 0; j < m; ++j) {
                 swap(mat[i][j], mat[n - i - 1][m - j - 1]);
             }
         }

         if (n & 1) {
             for (int j = 0; j < m / 2; ++j) {
                 swap(mat[n / 2][j], mat[n / 2][m - j - 1]);
             }
         }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << setw(2) << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> matrix{
        {1,  2,  3,  4, 5},
        {16, 17, 18, 19, 6},
        {15, 24, 25, 20, 7},
        {14, 23, 22, 21, 8},
        {13, 12, 11, 10, 9}};
    /*vector<vector<int>> matrix{
        {1,  2,  3,  4},
        {5,  6,  7,  8},
        {9,  10, 11, 12},
        {13, 14, 15, 16}
    };*/

    cout << "before:\n" << matrix << endl;

    Solution().rotateMatrix(matrix);

    cout << "after:\n" << matrix << endl;

    return 0;
}
