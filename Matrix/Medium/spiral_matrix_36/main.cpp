/*

Given an integer array containing `N × N` elements, construct an `N × N` matrix from it in spiral order.

Input: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25]

Output:

[
    [ 1   2   3   4  5],
    [16  17  18  19  6],
    [15  24  25  20  7],
    [14  23  22  21  8],
    [13  12  11  10  9]
]

*/

#include <iostream>
#include <vector>
#include <iomanip>
#include <cmath>

using namespace std;

class Solution
{
public:
    vector<vector<int>> createSpiralMatrix(vector<int> const &nums)
    {
        int n = sqrt(nums.size()), k = 0;
        vector<vector<int>> matrix(n, vector<int>(n));

        int top = 0, left = 0, bottom = n - 1, right = n - 1;

        while (true) {
            if (left > right) break;

            for (int i = left; i <= right; ++i) {
                matrix[top][i] = nums[k++];
            }

            ++top;
            if (top > bottom) break;

            for (int i = top; i <= bottom; ++i) {
                 matrix[i][right] = nums[k++];
            }

            --right;
            if (left > right) break;

            for (int i = right; i >= left; --i) {
                matrix[bottom][i] = nums[k++];
            }

            --bottom;
            if (top > bottom) break;

            for (int i = bottom; i >= top; --i) {
                matrix[i][left] = nums[k++];
            }

            ++left;
        }

        return matrix;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << setw(2) << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    cout << Solution().createSpiralMatrix({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25}) << endl;
    return 0;
}
