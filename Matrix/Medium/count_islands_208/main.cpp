/*

Given a binary matrix where 0 represents water and 1 represents land, and connected ones form an island, count the total islands.

Input:

[
    [1, 0, 1, 0, 0, 0, 1, 1, 1, 1],
    [0, 0, 1, 0, 1, 0, 1, 0, 0, 0],
    [1, 1, 1, 1, 0, 0, 1, 0, 0, 0],
    [1, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    [1, 1, 1, 1, 0, 0, 0, 1, 1, 1],
    [0, 1, 0, 1, 0, 0, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 1, 1, 1, 0, 0],
    [0, 0, 0, 1, 0, 0, 1, 1, 1, 0],
    [1, 0, 1, 0, 1, 0, 0, 1, 0, 0],
    [1, 1, 1, 1, 0, 0, 0, 1, 1, 1]
]

Output: 5

Explanation: There are a total of 5 islands present in the above matrix. They are marked by the numbers 1 to 5 in the matrix below.

  1  0  2  0  0  0  3  3  3  3
  0  0  2  0  2  0  3  0  0  0
  2  2  2  2  0  0  3  0  0  0
  2  0  0  2  0  3  0  0  0  0
  2  2  2  2  0  0  0  5  5  5
  0  2  0  2  0  0  5  5  5  5
  0  0  0  0  0  5  5  5  0  0
  0  0  0  4  0  0  5  5  5  0
  4  0  4  0  4  0  0  5  0  0
  4  4  4  4  0  0  0  5  5  5

*/


#include <iostream>
#include <vector>
#include <queue>
#include <queue>

using namespace std;

class Solution
{
private:
    int row[9] = {-1, -1, -1, 0, 1, 0, 1, 1};
    int col[9] = {-1, 1, 0, -1, -1, 1, 0, 1};

    bool isSafe(vector<vector<int>> const &mat, int x, int y, vector<vector<bool>> const &processed)
    {
        return (x >= 0 && x < mat.size()) && (y >= 0 && y < mat[0].size()) && mat[x][y] && !processed[x][y];
    }

    void BFS(vector<vector<int>> const &mat, vector<vector<bool>> &processed, int i, int j)
    {
        queue<pair<int, int>> q;
        q.push(make_pair(i, j));

        processed[i][j] = true;

        while (!q.empty())
        {
            auto [x, y] = q.front();
            q.pop();

            for (int k = 0; k < 8; ++k)
            {
                if (isSafe(mat, x + row[k], y + col[k], processed))
                {
                    processed[x + row[k]][y + col[k]] = true;

                    q.push(make_pair(x + row[k], y + col[k]));
                }
            }
        }
    }
public:
    int countIslands(vector<vector<int>> const &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return 0;
        }

        int m = mat.size();
        int n = mat[0].size();

        vector<vector<bool>> processed(m, vector<bool>(n));

        int island = 0;
        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (mat[i][j] && processed[i][j] == 0)
                {
                    BFS(mat, processed, i, j);
                    ++island;
                }
            }
        }

        return island;
    }
};

int main()
{
    vector<vector<int>> mat{
        {1, 0, 1, 0, 0, 0, 1, 1, 1, 1},
        {0, 0, 1, 0, 1, 0, 1, 0, 0, 0},
        {1, 1, 1, 1, 0, 0, 1, 0, 0, 0},
        {1, 0, 0, 1, 0, 1, 0, 0, 0, 0},
        {1, 1, 1, 1, 0, 0, 0, 1, 1, 1},
        {0, 1, 0, 1, 0, 0, 1, 1, 1, 1},
        {0, 0, 0, 0, 0, 1, 1, 1, 0, 0},
        {0, 0, 0, 1, 0, 0, 1, 1, 1, 0},
        {1, 0, 1, 0, 1, 0, 0, 1, 0, 0},
        {1, 1, 1, 1, 0, 0, 0, 1, 1, 1}
    };

    cout << Solution().countIslands(mat) << endl;

    return 0;
}
