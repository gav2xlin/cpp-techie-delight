/*

Give an `M × N` binary matrix, change all elements of row `i` and column `j` to 0 if cell (i, j) has value 0.

Input:
[
    [1, 1, 0, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 0, 1],
    [1, 1, 1, 1, 1],
    [0, 1, 1, 1, 1]
]

Output:
[
    [0, 0, 0, 0, 0],
    [0, 1, 0, 0, 1],
    [0, 0, 0, 0, 0],
    [0, 1, 0, 0, 1],
    [0, 0, 0, 0, 0]
]

Explanation:

0’s are present at (0, 2), (4, 0), and (2, 3) in the input matrix. Therefore, every element of the following cells is changed to 0:

• row 0 and column 2
• row 4 and column 0
• row 2 and column 3

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    void changeRow(vector<vector<int>> &mat, int r, int cn) {
        for (int i = 0; i < cn; ++i) {
            if (mat[r][i] != 0) {
                mat[r][i] = -1;
            }
        }
    }

    void changeColumn(vector<vector<int>> &mat, int c, int rn) {
        for (int i = 0; i < rn; i++) {
            if (mat[i][c] != 0) {
                mat[i][c] = -1;
            }
        }
    }
public:
    void convertMatrix(vector<vector<int>> &mat)
    {
        if (mat.empty() || mat[0].empty()) return;

        int rn = mat.size(), cn = mat[0].size();

        for (int r = 0; r < rn; ++r) {
            for (int c = 0; c < cn; ++c) {
                if (mat[r][c] == 0) {
                    changeRow(mat, r, cn);
                    changeColumn(mat, c, rn);
                }
            }
        }

        for (int r = 0; r < rn; ++r) {
            for (int c = 0; c < cn; ++c) {
                if (mat[r][c] < 0) {
                    mat[r][c] = 0;
                }
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> mat{
        {1, 1, 0, 1, 1},
        {1, 1, 1, 1, 1},
        {1, 1, 1, 0, 1},
        {1, 1, 1, 1, 1},
        {0, 1, 1, 1, 1}
    };
    cout << "before:\n" << mat << endl;

    Solution().convertMatrix(mat);

    cout << "after:\n" << mat << endl;

    return 0;
}
