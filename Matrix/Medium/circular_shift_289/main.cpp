/*

Given two positive integers n and k, perform a circular shift on the binary representation of n by k positions.

The circular shift can be of two types:

• Left circular shift (moving the final bit to the first position while shifting all other bits to the next position).
• Right circular shift (moving the first bit to the last position while shifting all other bits to the previous position).


Input: n = 127, k = 3, isLeftShift = true
Output: 1016

Explanation:

The binary representation for n = 127 is: 00000000000000000000000001111111
Left shift 127 by 3 positions results in: 00000000000000000000001111111000


Input: n = 127, k = 3, isLeftShift = false
Output: -536870897

Explanation:

The binary representation for n = 127 is : 00000000000000000000000001111111
Right shift 127 by 3 positions results in: 11100000000000000000000000001111


Assume integer size to be 32 bits (4 bytes).

*/

#include <iostream>
#include <bitset>

using namespace std;

constexpr int SIZE_INT = sizeof(int) * 8;

class Solution
{
public:
    int circularShift(int n, int k, bool isLeftShift) {
        if (isLeftShift) {
            return (n << k) | (n >> (SIZE_INT - k));
        } else {
            return (n >> k) | (n << (SIZE_INT - k));
        }
    }
};

int main()
{
    Solution s;
    unsigned n = 127;
    int shift = 3;

    cout << "No Shift    " << bitset<32>(n) << endl;
    cout << "Left Shift  " << bitset<32>(s.circularShift(n, shift, true)) << endl;
    cout << "Right Shift " << bitset<32>(s.circularShift(n, shift, false)) << endl;

    return 0;
}
