/*

Find duplicate rows present in a given binary matrix by traversing the matrix only once.

Input:

mat = [
    [1, 0, 0, 1, 0],
    [0, 1, 1, 0, 0],
    [1, 0, 0, 1, 0],
    [0, 0, 1, 1, 0],
    [0, 1, 1, 0, 0]
]

Output: {3, 5}

Explanation: Row #3 is duplicate of row #1 and row #5 is duplicate of row #2

*/

#include <iostream>
#include <unordered_set>
#include <vector>
#include <cmath>

using namespace std;

class Solution
{
public:
    unordered_set<int> findDuplicateRows(vector<vector<int>> const &mat)
    {
        unordered_set<int> rows;

        unordered_set<unsigned> set;
        for (int i = 0; i < mat.size(); i++)
        {
            unsigned decimal = 0;

            for (int j = 0; j < mat[0].size(); ++j) {
                //decimal += mat[i][j] * pow(2, j);
                decimal += mat[i][j] * (1 << j);
            }

            if (set.count(decimal)) {
                rows.insert(i + 1);
            } else {
                set.insert(decimal);
            }
        }

        return rows;
    }
};

ostream& operator<<(ostream& os, const unordered_set<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<vector<int>> mat{
        {1, 0, 0, 1, 0},
        {0, 1, 1, 0, 0},
        {1, 0, 0, 1, 0},
        {0, 0, 1, 1, 0},
        {0, 1, 1, 0, 0}
    };
    cout << Solution().findDuplicateRows(mat) << endl;

    return 0;
}
