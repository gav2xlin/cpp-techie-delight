cmake_minimum_required(VERSION 3.5)

project(fill_matrix_65 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(fill_matrix_65 main.cpp)

install(TARGETS fill_matrix_65
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
