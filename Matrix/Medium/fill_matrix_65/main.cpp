/*

Given an `M × N` binary matrix, fill it with alternating rectangles of 1’s and 0’s.

Input: 10 × 8 matrix
Output:

[
    [1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1]
]


Input: 3 × 5 matrix
Output:

[
    [1, 1, 1, 1, 1],
    [1, 0, 0, 0, 1],
    [1, 1, 1, 1, 1]
]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    void fillMatrix(vector<vector<int>> &mat)
    {
        int m = mat.size();
        int n = mat[0].size();

        int top = 0, bottom = m - 1;
        int left = 0, right = n - 1;
        int flag = 1;

        while (true)
        {
            if (left > right) {
                break;
            }

            for (int i = left; i <= right; ++i) {
                mat[top][i] = flag;
            }

            ++top;
            if (top > bottom) {
                break;
            }

            for (int i = top; i <= bottom; ++i) {
                mat[i][right] = flag;
            }

            --right;
            if (left > right) {
                break;
            }

            for (int i = right; i >= left; --i) {
                mat[bottom][i] = flag;
            }

            --bottom;
            if (top > bottom) {
                break;
            }

            for (int i = bottom; i >= top; i--) {
                mat[i][left] = flag;
            }

            ++left;

            flag = flag == 1 ? 0 : 1;
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{

    vector<vector<int>> mat(10, vector<int>(8));
    Solution().fillMatrix(mat);
    cout << mat << endl;

    return 0;
}
