/*

Flood fill is an algorithm that determines the area connected to a given node in a multi-dimensional array. It is used in the "bucket" fill tool of a paint program to fill connected, similarly colored areas with a different color and in games such as Go and Minesweeper for determining which pieces are cleared. When applied on an image to fill a particular bounded area with color, it is also known as boundary fill.

The flood fill algorithm takes three parameters: a start node, a target color, and a replacement color. The algorithm looks for all nodes in the matrix that are connected to the start node by a path of the target color and changes them to the replacement color.

Input:

Matrix = [
    [Y, Y, Y, G, G, G, G, G, G, G],
    [Y, Y, Y, Y, Y, Y, G, V, V, V],
    [G, G, G, G, G, G, G, V, V, V],
    [W, W, W, W, W, G, G, G, G, V],
    [W, R, R, R, R, R, G, V, V, V],
    [W, W, W, R, R, G, G, V, V, V],
    [W, B, W, R, R, R, R, R, R, V],
    [W, B, B, B, B, R, R, V, V, V],
    [W, B, B, V, B, B, B, B, V, V],
    [W, B, B, V, V, V, V, V, V, V]
]

(x, y) = (3, 9)				(Start node, having a target color `V`)
Replacement Color = O

Output:
[
    [Y, Y, Y, G, G, G, G, G, G, G],
    [Y, Y, Y, Y, Y, Y, G, O, O, O],
    [G, G, G, G, G, G, G, O, O, O],
    [W, W, W, W, W, G, G, G, G, O],
    [W, R, R, R, R, R, G, O, O, O],
    [W, W, W, R, R, G, G, O, O, O],
    [W, B, W, R, R, R, R, R, R, O],
    [W, B, B, B, B, R, R, O, O, O],
    [W, B, B, O, B, B, B, B, O, O],
    [W, B, B, O, O, O, O, O, O, O]
]

*/

#include <iostream>
#include <vector>
#include <iomanip>
#include <queue>

using namespace std;

class Solution
{
private:
    int row[9] = {-1, -1, -1, 0, 0, 1, 1, 1};
    int col[9] = {-1, 0, 1, -1, 1, -1, 0, 1};

    bool isSafe(vector<vector<char>> const &mat, int x, int y, char target)
    {
        return (x >= 0 && x < mat.size()) && (y >= 0 && y < mat[0].size()) && mat[x][y] == target;
    }
public:
    /*void floodfill(vector<vector<char>> &mat, int x, int y, char replacement)
    {
        if (mat.empty() || mat[0].empty()) {
            return;
        }

        queue<pair<int, int>> q;
        q.push({x, y});

        char target = mat[x][y];

        if (target == replacement) {
            return;
        }

        while (!q.empty())
        {
            pair<int, int> node = q.front();
            q.pop();

            int x = node.first, y = node.second;

            mat[x][y] = replacement;

            for (int k = 0; k < 8; ++k)
            {
                if (isSafe(mat, x + row[k], y + col[k], target))
                {
                    q.push({x + row[k], y + col[k]});
                }
            }
        }
    }*/
    void floodfill(vector<vector<char>> &mat, int x, int y, char replacement)
    {
        if (mat.empty() || mat[0].empty()) {
            return;
        }

        char target = mat[x][y];

        if (target == replacement) {
            return;
        }

        mat[x][y] = replacement;

        for (int k = 0; k < 8; ++k)
        {
            if (isSafe(mat, x + row[k], y + col[k], target)) {
                floodfill(mat, x + row[k], y + col[k], replacement);
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<char>& nums) {
    for (auto v : nums) {
        os << setw(3) << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<char>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<char>> mat{
        {'Y', 'Y', 'Y', 'G', 'G', 'G', 'G', 'G', 'G', 'G'},
        {'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'G', 'X', 'X', 'X'},
        {'G', 'G', 'G', 'G', 'G', 'G', 'G', 'X', 'X', 'X'},
        {'W', 'W', 'W', 'W', 'W', 'G', 'G', 'G', 'G', 'X'},
        {'W', 'R', 'R', 'R', 'R', 'R', 'G', 'X', 'X', 'X'},
        {'W', 'W', 'W', 'R', 'R', 'G', 'G', 'X', 'X', 'X'},
        {'W', 'B', 'W', 'R', 'R', 'R', 'R', 'R', 'R', 'X'},
        {'W', 'B', 'B', 'B', 'B', 'R', 'R', 'X', 'X', 'X'},
        {'W', 'B', 'B', 'X', 'B', 'B', 'B', 'B', 'X', 'X'},
        {'W', 'B', 'B', 'X', 'X', 'X', 'X', 'X', 'X', 'X'}
    };

    cout << "before:\n" << mat << endl;

    Solution().floodfill(mat, 3, 9, 'C');

    cout << "after:\n" << mat << endl;

    return 0;
}
