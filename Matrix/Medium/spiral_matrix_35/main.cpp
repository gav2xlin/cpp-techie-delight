/*

Given a positive number `N`, return an `N × N` spiral matrix containing numbers from 1 to `N × N` in a counterclockwise direction.

Input: N = 5

Output:
[
    [25, 24, 23, 22, 21],
    [10, 9,  8,  7,  20],
    [11, 2,  1,  6,  19],
    [12, 3,  4,  5,  18],
    [13, 14, 15, 16, 17]
]

*/

#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

class Solution
{
public:
    vector<vector<int>> spiralTraversal(int n)
    {
        int k = n * n;
        vector<vector<int>> matrix(n, vector<int>(n));

        int top = 0, left = 0, bottom = n - 1, right = n - 1;

        while (true) {
            if (left > right) break;

            for (int i = left; i <= right; ++i) {
                matrix[top][i] = k--;
            }

            ++top;
            if (top > bottom) break;

            for (int i = top; i <= bottom; ++i) {
                 matrix[i][right] = k--;
            }

            --right;
            if (left > right) break;

            for (int i = right; i >= left; --i) {
                matrix[bottom][i] = k--;
            }

            --bottom;
            if (top > bottom) break;

            for (int i = bottom; i >= top; --i) {
                matrix[i][left] = k--;
            }

            ++left;
        }

        return matrix;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << setw(2) << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    cout << Solution().spiralTraversal(5) << endl;

    return 0;
}
