/*

Given an `M × N` binary matrix, find the size of the largest square submatrix of 1’s in it.

Input:

[
    [0, 0, 1, 0, 1, 1],
    [0, 1, 1, 1, 0, 0],
    [0, 0, 1, 1, 1, 1],
    [1, 1, 0, 1, 1, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 1],
    [1, 1, 1, 0, 1, 1]
]

Output: 3

Explanation: The largest square submatrix of 1’s is marked below by `x`.

    0  0  1  0  1  1
    0  1  1  1  0  0
    0  0  1  x  x  x
    1  1  0  x  x  x
    1  1  1  x  x  x
    1  1  0  1  0  1
    1  0  1  1  1  1
    1  1  1  0  1  1

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findLargestSquareSubmatrix(vector<vector<int>> const &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return 0;
        }

        int m = mat.size();
        int n = mat[0].size();

        int lookup[m][n];
        int max = 0;

        for (int i = 0; i < m; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                lookup[i][j] = mat[i][j];

                if (i && j && mat[i][j])
                {
                    // largest square submatrix ending at `mat[i][j]` will be
                    // 1 plus minimum of largest square submatrix ending at
                    // mat[i][j-1], mat[i-1][j] and mat[i-1][j-1]

                    lookup[i][j] = min(min(lookup[i][j - 1], lookup[i - 1][j]), lookup[i - 1][j - 1]) + 1;
                }

                if (max < lookup[i][j]) {
                    max = lookup[i][j];
                }
            }
        }

        return max;
    }
};

int main()
{
    vector<vector<int>> mat{
        {0, 0, 1, 0, 1, 1},
        {0, 1, 1, 1, 0, 0},
        {0, 0, 1, 1, 1, 1},
        {1, 1, 0, 1, 1, 1},
        {1, 1, 1, 1, 1, 1},
        {1, 1, 0, 1, 0, 1},
        {1, 0, 1, 1, 1, 1},
        {1, 1, 1, 0, 1, 1}
    };

    cout << Solution().findLargestSquareSubmatrix(mat) << endl;

    return 0;
}
