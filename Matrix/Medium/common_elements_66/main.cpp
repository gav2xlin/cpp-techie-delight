/*

Given an `M × N` integer matrix, find the common elements present in all rows of the matrix. The solution should traverse the matrix once and return the common elements.

Input:
[
    [7, 1, 3, 5, 3, 6],
    [2, 3, 6, 1, 1, 6],
    [6, 1, 7, 2, 1, 4],
    [6, 6, 7, 1, 3, 3],
    [5, 5, 6, 1, 5, 4],
    [3, 5, 6, 2, 7, 1],
    [4, 1, 4, 3, 6, 4],
    [4, 6, 1, 7, 4, 3]
]

Output: {1, 6}

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    unordered_set<int> findCommonElements(vector<vector<int>> const &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return {};
        }

        int m = mat.size();
        int n = mat[0].size();

        unordered_map<int, int> map;

        for (int c = 0; c < n; c++) {
            map.insert(pair<int, int>(mat[0][c], 1));
        }

        for (int r = 1; r < m; ++r)
        {
            for (int c = 0; c < n; ++c)
            {
                int curr = mat[r][c];

                if (map.count(curr) && map[curr] == r) {
                    map[curr] = r + 1;
                }
            }
        }

        unordered_set<int> res;
        for (auto it: map)
        {
            if (it.second == m) {
                res.insert(it.first);
            }
        }
        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<vector<int>> matrix{
        {7, 1, 3, 5, 3, 6},
        {2, 3, 6, 1, 1, 6},
        {6, 1, 7, 2, 1, 4},
        {6, 6, 7, 1, 3, 3},
        {5, 5, 6, 1, 5, 4},
        {3, 5, 6, 2, 7, 1},
        {4, 1, 4, 3, 6, 4},
        {4, 6, 1, 7, 4, 3}
    };
    cout << Solution().findCommonElements(matrix) << endl;

    return 0;
}
