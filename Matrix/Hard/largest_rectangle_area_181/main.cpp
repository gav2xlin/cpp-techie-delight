/*

Given a rectangular binary matrix, calculate the area of the largest rectangle of 1’s in it. Assume that a rectangle can be formed by swapping any number of columns with each other.

Input:

[
    [0, 1, 0, 1, 1],
    [1, 1, 0, 0, 1],
    [1, 1, 0, 1, 1],
    [1, 1, 1, 1, 1]
]

Output: 9

Explanation: The area of the largest rectangle of 1’s is 9. The largest rectangle of 1’s can be formed by swapping column 3 with column 5.

[0, 1, 1, 1, 0]
[1, 1, 1, 0, 0]
[1, 1, 1, 1, 0]
[1, 1, 1, 1, 1]


Input:

[
    [0, 1, 1, 0],
    [1, 0, 0, 1],
    [1, 1, 0, 1],
    [1, 1, 1, 1]
]

Output: 6

Explanation: The area of the largest rectangle of 1’s is 6. The largest rectangle of 1’s can be formed by swapping column 2 with column 4 or swapping column 3 with column 4.

[0, 0, 1, 1]
[1, 1, 0, 0]
[1, 1, 0, 1]
[1, 1, 1, 1]

OR

[0, 1, 0, 1]
[1, 0, 1, 0]
[1, 1, 1, 0]
[1, 1, 1, 1]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    void resetMatrix(vector<vector<int>> &mat)
    {
        int M = mat.size();
        int N = mat[0].size();

        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j)
            {
                if (mat[i][j] != 0) {
                    mat[i][j] = 1;
                }
            }
        }
    }
public:
    int findLargestRectangleArea(vector<vector<int>> &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return 0;
        }

        int M = mat.size();
        int N = mat[0].size();

        for (int j = 0; j < N; ++j)
        {
            for (int i = M - 2; i >= 0; --i)
            {
                if (mat[i][j] == 1) {
                    mat[i][j] = mat[i+1][j] + 1;
                }
            }
        }

        int maxArea = 0;
        for (int i = 0; i < M; i++)
        {
            vector<int> count(M + 1);

            for (int j = 0; j < N; j++)
            {
                if (mat[i][j] > 0)
                {
                    count[mat[i][j]] += 1;

                    // the area can be calculated by multiplying the current
                    // element `mat[i][j]` with the corresponding value
                    // in the count array `count[mat[i][j]]`

                    maxArea = max(maxArea, mat[i][j] * count[mat[i][j]]);
                }
            }
        }

        resetMatrix(mat);

        return maxArea;
    }
};

int main()
{
    {
        vector<vector<int>> mat{
            {0, 1, 0, 1, 1},
            {1, 1, 0, 0, 1},
            {1, 1, 0, 1, 1},
            {1, 1, 1, 1, 1}
        };

        cout << Solution().findLargestRectangleArea(mat) << endl;
    }
    {
        vector<vector<int>> mat {
            {0, 1, 1, 0},
            {1, 0, 0, 1},
            {1, 1, 0, 1},
            {1, 1, 1, 1}
        };

        cout << Solution().findLargestRectangleArea(mat) << endl;
    }

    return 0;
}
