/*

Given an `M × N` integer matrix, calculate the maximum sum submatrix of size `k × k` in it.

Input:

mat = [
    [ 3, -4,  6, -5,  1],
    [ 1, -2,  8, -4, -2],
    [ 3, -8,  9,  3,  1],
    [-7,  3,  4,  2,  7],
    [-3,  7, -5,  7, -6]
]

• If k = 2, then

Output:
[
    [9, 3],
    [4, 2]
]

• If k = 3, then

Output:
[
    [8, -4, -2],
    [9,  3,  1],
    [4,  2,  7]
]

In case the multiple `k × k` submatrix exists with the maximum sum, the solution can return any one of them.

*/

#include <iostream>
#include <vector>
#include <climits>
#include <utility>

using namespace std;

class Solution
{
private:
    using Point = pair<int, int>;

    void printVector(vector<int> const &input)
    {
        cout << "[";
        for (int i = 0; i < input.size(); ++i) {
            cout << input[i];
            if (i < input.size() - 1) {
                cout << ", ";
            }
        }
        cout << "]\n";
    }

    vector<vector<int>> preprocess(vector<vector<int>> const &mat, int M, int N)
    {
        vector<vector<int>> sum(M, vector<int>(N));
        sum[0][0] = mat[0][0];

        for (int j = 1; j < N; ++j) {
            sum[0][j] = mat[0][j] + sum[0][j - 1];
        }

        for (int i = 1; i < M; ++i) {
            sum[i][0] = mat[i][0] + sum[i - 1][0];
        }

        for (int i = 1; i < M; ++i)
        {
            for (int j = 1; j < N; ++j)
            {
                sum[i][j] = mat[i][j] + sum[i - 1][j] + sum[i][j - 1] - sum[i - 1][j - 1];
            }
        }

        return sum;
    }
public:
    vector<vector<int>> findMaxSumSubmatrix(vector<vector<int>> const &mat, int k)
    {
        if (mat.empty() || mat[0].empty()) {
            return {};
        }

        int M = mat.size();
        int N = mat[0].size();

        vector<vector<int>> sum = preprocess(mat, M, N);

        int max = INT_MIN;

        Point p;

        for (int i = k - 1; i < M; ++i)
        {
            for (int j = k - 1; j < N; ++j)
            {
                // Note that (i, j) is the bottom-right corner coordinates of the
                // square submatrix of size `k`

                int total = sum[i][j];
                if (i - k >= 0) {
                    total = total - sum[i - k][j];
                }

                if (j - k >= 0) {
                    total = total - sum[i][j - k];
                }

                if (i - k >= 0 && j - k >= 0) {
                    total = total + sum[i - k][j - k];
                }

                if (total > max) {
                    max = total, p = make_pair(i, j);
                }
            }
        }

        vector<vector<int>> res;
        for (int i = 0; i < k; ++i)
        {
            vector<int> row;
            for (int j = 0; j < k; ++j) {
                row.push_back(mat[i + p.first - k + 1][j + p.second - k + 1]);
            }
            //printVector(row);
            res.push_back(row);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    vector<vector<int>> mat {
        {3, -4, 6, -5, 1},
        {1, -2, 8, -4, -2},
        {3, -8, 9, 3, 1},
        {-7, 3, 4, 2, 7},
        {-3, 7, -5, 7, -6}
    };

    cout << Solution().findMaxSumSubmatrix(mat, 2) << endl;
    cout << Solution().findMaxSumSubmatrix(mat, 3) << endl;

    return 0;
}
