/*

Given an `M × N` matrix of characters, find all occurrences of a given string in it and return a set of its coordinates. You are allowed to search the string in all eight possible directions, i.e., North, West, South, East, North-East, North-West, South-East, South-West. There should not be any cycles in the output path.

Input:

[
    ['D', 'E', 'M', 'X', 'B'],
    ['A', 'O', 'E', 'P', 'E'],
    ['D', 'D', 'C', 'O', 'D'],
    ['E', 'B', 'E', 'D', 'S'],
    ['C', 'P', 'Y', 'E', 'N']
]

word = "CODE"

Output:

{
    [(2, 2), (2, 3), (2, 4), (1, 4)],
    [(2, 2), (1, 1), (2, 1), (3, 2)],
    [(2, 2), (1, 1), (2, 1), (3, 0)],
    [(2, 2), (2, 3), (3, 3), (4, 3)],
    [(2, 2), (1, 1), (2, 1), (1, 2)],
    [(2, 2), (1, 1), (0, 0), (0, 1)],
    [(2, 2), (1, 1), (2, 0), (3, 0)],
    [(2, 2), (2, 3), (3, 3), (3, 2)]
}

*/

#include <iostream>
#include <set>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
private:
    int row[8]{-1, -1, -1, 0, 0, 1, 1, 1};
    int col[8]{-1, 0, 1, -1, 1, -1, 0, 1};

    bool isValid(pair<int,int> const &next, vector<pair<int,int>> const &path,
        vector<vector<char>> const &mat)
    {
        return (next.first >= 0 && next.first < mat.size()) &&
                (next.second >= 0 && next.second < mat[0].size()) &&
                find(path.begin(), path.end(), next) == path.end();
    }

    void DFS(vector<vector<char>> const &mat, string word, pair<int,int> const &next,
             vector<pair<int,int>> path, int index, set<vector<pair<int,int>>>& paths)
    {
        int i = next.first;
        int j = next.second;

        if (mat[i][j] != word[index]) {
            return;
        }

        path.push_back({i, j});

        if (index == word.size() - 1)    {
            //printVectorOfPairs(path);
            paths.insert(path);

            return;
        }

        for (int k = 0; k < 8; ++k)
        {
            pair<int,int> next = {i + row[k], j + col[k]};

            if (isValid(next, path, mat)) {
                DFS(mat, word, next, path, index + 1, paths);
            }
        }
    }

    /*template <typename T>
    void printVectorOfPairs(vector<pair<T,T>> const &input)
    {
        cout << "[";
        int n = input.size();
        for (int i = 0; i < n; i++) {
            cout << '(' << input[i].first << ", " << input[i].second << ')';
            if (i < n - 1) {
                cout << ", ";
            }
        }
        cout << "]\n";
    }*/
public:
    set<vector<pair<int,int>>> searchMatrix(vector<vector<char>> const &mat, string word)
    {
        set<vector<pair<int,int>>> paths;

        for (int i = 0; i < mat.size(); ++i)
        {
            for (int j = 0; j < mat[0].size(); ++j) {
                DFS(mat, word, make_pair(i, j), {}, 0, paths);
            }
        }

        return paths;
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ':' << p.second;
    return os;
}

ostream& operator<<(ostream& os, const vector<pair<int,int>>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<pair<int,int>>>& matrix) {
    for (auto& row : matrix) {
        os << row << endl;
    }
    return os;
}

int main()
{
    vector<vector<char>> mat {
        {'D', 'E', 'M', 'X', 'B'},
        {'A', 'O', 'E', 'P', 'E'},
        {'D', 'D', 'C', 'O', 'D'},
        {'E', 'B', 'E', 'D', 'S'},
        {'C', 'P', 'Y', 'E', 'N'}
    };

    cout << Solution().searchMatrix(mat, "CODE") << endl;

    return 0;
}
