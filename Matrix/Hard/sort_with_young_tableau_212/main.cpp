/*

Given an integer array, in-place sort it using Young tableau. You are given a utility class YoungTableau with the following public member functions:

1. vector<vector<int>> construct(vector<int> &nums): Construct and return an `N × N` Young tableau from the list `nums`. Note that `N` is determined dynamically by taking ceil of the square root of `nums` length.

2. int extractMin(vector<vector<int>> &tableau): Extract the next minimum element from the Young tableau `tableau`.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <climits>
#include <cmath>

using namespace std;

class Solution
{
private:
    void fixTableau(vector<vector<int>> &tableau, int i, int j)
    {
        int N = tableau.size();

        int bottom = (i + 1 < N) ? tableau[i + 1][j] : INT_MAX;
        int right = (j + 1 < N) ? tableau[i][j + 1] : INT_MAX;

        if (bottom == INT_MAX && right == INT_MAX) {
            return;
        }

        if (bottom < right)
        {
            swap(tableau[i][j], tableau[i + 1][j]);
            fixTableau(tableau, i + 1, j);
        }
        else
        {
            swap(tableau[i][j], tableau[i][j + 1]);
            fixTableau(tableau, i, j + 1);
        }
    }

    void insert(vector<vector<int>> &tableau, int i, int j)
    {
        if (i == 0 && j == 0) {
            return;
        }

        if (i == 0)
        {
            if (tableau[i][j] < tableau[i][j - 1])
            {
                swap(tableau[i][j], tableau[i][j - 1]);
                insert(tableau, i, j - 1);
            }
            return;
        }

        if (j == 0)
        {
            if (tableau[i][j] < tableau[i - 1][j])
            {
                swap(tableau[i][j], tableau[i - 1][j]);
                insert(tableau, i - 1, j);
            }
            return;
        }

        if (tableau[i][j] < tableau[i - 1][j])
        {
            swap(tableau[i][j], tableau[i - 1][j]);
            insert(tableau, i - 1, j);
        }

        if (tableau[i][j] < tableau[i][j - 1])
        {
            swap(tableau[i][j], tableau[i][j - 1]);
            insert(tableau, i, j - 1);
        }
    }
public:
    vector<vector<int>> construct(vector<int> &keys)
    {
        int N = (int) ceil(sqrt(keys.size()));
        vector<vector<int>> tableau(N, vector<int>(N, INT_MAX));

        for (int key: keys)
        {
            if (tableau[N - 1][N - 1] != INT_MAX) {
                break;
            }

            tableau[N - 1][N - 1] = key;

            insert(tableau, N - 1, N - 1);
        }

        return tableau;
    }

    int extractMin(vector<vector<int>> &tableau)
    {
        int min = tableau[0][0];

        tableau[0][0] = INT_MAX;

        fixTableau(tableau, 0, 0);

        return min;
    }

    void sort(vector<int> &keys)
    {
        if (keys.size() == 0) {
            return;
        }

        vector<vector<int>> tableau = construct(keys);

        for (int i = 0; i < keys.size(); i++) {
            keys[i] = extractMin(tableau);
        }
    }
};

int main()
{
    vector<int> keys { 6, 4, 8, 7, 2, 3, 1, 5 };

    Solution().sort(keys);

    for (int i: keys) {
        cout << i << ' ';
    }
    cout << endl;

    return 0;
}
