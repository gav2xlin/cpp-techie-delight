/*

Given an integer array containing n elements, construct an `N × N` Young tableau from it where N is ceil of square root of n. i.e., N = ceil(sqrt(n)).

An `N × N` Young tableau is an `N × N` matrix such that the entries of each row are sorted from left to right and the entries of each column are sorted from top to bottom. Some entries of a Young tableau may be infinity, which indicates an empty entry.

Input: [12, 10, 20, 22, 25, 30, 34, 11, 44, 27, 16, 40, 35, 15, 18, 45]

Output: [
    [10, 11, 12, 15],
    [16, 18, 20, 22],
    [25, 27, 30, 34],
    [35, 40, 44, 45]
]

OR [
    [10, 12, 15, 16],
    [11, 18, 20, 25],
    [22, 27, 30, 35],
    [34, 40, 44, 45]
]

OR, any other valid `N × N` Young tableau.


Input: [12, 10, 20, 22, 25, 30, 34, 11, 27, 16]

Output: [
    [10,  11,  12,  16],
    [20,  22,  25,  27],
    [30,  34,  inf, inf],
    [inf, inf, inf, inf]
]

Here, inf = INT_MAX

OR, any other valid `N × N` Young tableau.

*/

#include <iostream>
#include <vector>
#include <cmath>
#include <climits>
#include <utility>

using namespace std;

class Solution
{
private:
    void insert(vector<vector<int>> &tableau, int i, int j)
    {
        if (i == 0 && j == 0) {
            return;
        }

        if (i == 0)
        {
            if (tableau[i][j] < tableau[i][j-1])
            {
                swap(tableau[i][j], tableau[i][j-1]);
                insert(tableau, i, j - 1);
            }
            return;
        }

        if (j == 0)
        {
            if (tableau[i][j] < tableau[i-1][j])
            {
                swap(tableau[i][j], tableau[i-1][j]);
                insert(tableau, i - 1, j);
            }
            return;
        }

        if (tableau[i][j] < tableau[i-1][j])
        {
            swap(tableau[i][j], tableau[i-1][j]);
            insert(tableau, i - 1, j);
        }

        if (tableau[i][j] < tableau[i][j-1])
        {
            swap(tableau[i][j], tableau[i][j-1]);
            insert(tableau, i, j - 1);
        }
    }
    //
    void fixTableau(vector<vector<int>> &tableau, int i, int j)
    {
        int M = tableau.size();
        int N = tableau[0].size();

        int bottom = (i + 1 < M) ? tableau[i + 1][j] : INT_MAX;
        int right = (j + 1 < N) ? tableau[i][j + 1] : INT_MAX;

        if (bottom == INT_MAX && right == INT_MAX) {
            return;
        }

        if (bottom < right)
        {
            swap(tableau[i][j], tableau[i + 1][j]);
            fixTableau(tableau, i + 1, j);
        }
        else
        {
            swap(tableau[i][j], tableau[i][j + 1]);
            fixTableau(tableau, i, j + 1);
        }
    }
public:
    //
    vector<vector<int>> constructYoungTableau(vector<int> const &keys)
    {
        int M = ceil(sqrt(keys.size()));
        int N = M;

        vector<vector<int>> tableau(M, vector<int>(N, INT_MAX));

        for (int key: keys)
        {
            if (tableau[M-1][N-1] != INT_MAX)
            {
                // cout << "Young tableau is full. Skipping key " << key << endl;
            }
            else
            {
                tableau[M-1][N-1] = key;

                insert(tableau, M-1, N-1);
            }
        }

        return tableau;
    }
    //
    /*bool search(vector<vector<int>> &tableau, int key)
    {
        if (tableau.size() == 0) {
            return false;
        }

        int M = tableau.size();
        int N = tableau[0].size();

        int i = 0, j = N - 1;

        while (i < M && j >= 0)
        {
            if (tableau[i][j] < key)
            {
                ++i;
            }
            else if (tableau[i][j] > key)
            {
                --j;
            }
            else
            {
                return true;
            }
        }

        return false;
    }*/
    void search(vector<vector<int>> &tableau, int key, int value)
    {
        if (tableau.size() == 0) {
            return;
        }

        int M = tableau.size();
        int N = tableau[0].size();

        int i = 0, j = N - 1;

        // run till tableau boundary is reached
        while (i < M && j >= 0)
        {
            if (tableau[i][j] < key)
            {
                ++i;
            }
            else if (tableau[i][j] > key)
            {
                --j;
            }
            else
            {
                replace(tableau, i, j, value);
                return;
            }
        }
    }
    //
    int replaceInYoungTableau(vector<vector<int>> &tableau)
    {
        if (tableau.size() == 0) {
            exit(-1);
        }

        int min = tableau[0][0];

        tableau[0][0] = INT_MAX;

        fixTableau(tableau, 0, 0);

        return min;
    }
    //
    void deletion(vector<vector<int>> &tableau, int i, int j)
    {
        if (tableau.size() == 0 || i >= tableau.size() || j >= tableau[0].size()) {
            return;
        }

        tableau[i][j] = INT_MAX;

        fixTableau(tableau, i, j);
    }
    //
    void replace(vector<vector<int>> &tableau, int i, int j, int key)
    {
        tableau[i][j] = INT_MAX;

        fixTableau(tableau, i, j);

        int M = tableau.size();
        int N = tableau[0].size();

        tableau[M-1][N-1] = key;

        insert(tableau, M-1, N-1);
    }
};

void printTableau(vector<vector<int>> const &tableau)
{
    for (int i = 0; i < tableau.size(); ++i)
    {
        for (int j = 0; j < tableau[0].size(); ++j) {
            cout << tableau[i][j] << ' ';
        }
        cout << endl;
    }
}

int main()
{
    vector<int> keys = { 12, 10, 20, 22, 25, 30, 34, 11,
                            44, 27, 16, 40, 35, 15, 18, 45 };

    vector<vector<int>> tableau = Solution().constructYoungTableau(keys);

    printTableau(tableau);

    return 0;
}
