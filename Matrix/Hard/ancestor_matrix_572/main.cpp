/*

Given an N × N ancestor matrix, whose cell (i, j) has the value true if i is the ancestor of j in a binary tree, construct a binary tree from the ancestor matrix where binary tree nodes are labeled from 0 to N-1.

Please note that several binary trees can be constructed out of a single matrix since the ancestor matrix doesn't specify which child is left and which is right. The solution should return any one of them.

Input: [
    [0, 0, 0, 0, 0],
    [1, 0, 0, 0, 0],
    [0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0],
    [1, 1, 1, 1, 0]
]

Output: Any one of the following trees

     4               4              4
   /   \           /   \          /   \
  1     2   OR    2     1    OR  1     2  OR any other valid binary tree…
 /     /         /       \        \   /
0     3         3         0        0 3

*/

#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructTree(vector<vector<bool>> const &mat)
    {
        if (mat.empty() || mat[0].empty()) {
            return nullptr;
        }

        int N = mat.size();

        multimap<int, int> multimap;

        for (int i = 0; i < N; ++i)
        {
            int sum = 0;
            for (int j = 0; j < N; ++j) {
                sum += (int)mat[i][j];
            }

            multimap.insert({sum, i});
        }

        vector<Node*> node(N);
        vector<bool> parent(N);

        int row{};
        for (auto it: multimap)
        {
            row = it.second;

            node[row] = new Node(row);

            if (it.first == 0) {
                continue;
            }

            for (int i = 0; i < N; ++i)
            {
                if (parent[i] == false && mat[row][i])
                {
                    if (node[row]->left == nullptr) {
                        node[row]->left = node[i];
                    }
                    else
                    {
                        node[row]->right = node[i];
                    }

                    parent[i] = true;
                }
            }
        }

        return node[row];
    }
};

void inorder(Node* node)
{
    if (node != nullptr)
    {
        inorder(node->left);
        cout << node->data << ' ';
        inorder(node->right);
    }
}

void cleanup(Node* node) {
    if (node == nullptr) return;
    cleanup(node->left);
    cleanup(node->right);
    delete node;
}

int main()
{
    vector<vector<bool>> mat {
        { 0, 0, 0, 0, 0 },
        { 1, 0, 0, 0, 0 },
        { 0, 0, 0, 1, 0 },
        { 0, 0, 0, 0, 0 },
        { 1, 1, 1, 1, 0 }
    };

    Node* root = Solution().constructTree(mat);

    inorder(root);
    cleanup(root);

    return 0;
}
