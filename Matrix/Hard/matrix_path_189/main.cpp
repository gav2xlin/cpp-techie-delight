/*

Given an `M × N` matrix of integers, count the number of different ways to reach the bottom-right corner of a matrix from its top-left corner with exactly `k` turn allowed and using only the directions right or down.

A turn is defined as a down move immediately followed by a right move, or a right move immediately followed by a down move.

Input : M = 3, N = 3, k = 1
Output: 2
Explanation: Total number of paths are 2, as shown below:

(0, 0) —> (0, 1) —> (0, 2) —> (1, 2) —> (2, 2)
(0, 0) —> (1, 0) —> (2, 0) —> (2, 1) —> (2, 2)


Input : M = 3, N = 3, k = 2
Output: 2
Explanation: Total number of paths are 2, as shown below:

(0, 0) —> (0, 1) —> (1, 1) —> (2, 1) —> (2, 2)
(0, 0) —> (1, 0) —> (1, 1) —> (1, 2) —> (2, 2)


Input : M = 3, N = 3, k = 4
Output: 0

*/

#include <iostream>

using namespace std;

class Solution
{
private:
    bool isValid(int i, int j, int M, int N) {
        return i >= 0 && i < M && j >= 0 && j < N;
    }

    int totalWays(int M, int N, int i, int j, int k, bool isCol)
    {
        if (k == -1 || !isValid(i, j, M, N)) {
            return 0;
        }

        if (k == 0 && i == M - 1 && j == N - 1) {
            return 1;
        }

        if (isCol)
        {
            return totalWays(M, N, i + 1, j, k, isCol) + totalWays(M, N, i, j + 1, k - 1, !isCol);
        }

        return totalWays(M, N, i, j + 1, k, isCol) + totalWays(M, N, i + 1, j, k - 1, !isCol);
    }
public:
    int findTotalWays(int M, int N, int k)
    {
        int i = 0, j = 0;

        return totalWays(M, N, i + 1, j, k, true) + totalWays(M, N, i, j + 1, k, false);
    }
};

int main()
{
    cout << Solution().findTotalWays(3, 3, 1) << endl;
    cout << Solution().findTotalWays(3, 3, 2) << endl;
    cout << Solution().findTotalWays(3, 3, 4) << endl;

    return 0;
}
