/*

Given an `M × N` boggle board, find a list of all possible words that can be formed by a sequence of adjacent characters on the board.

You are allowed to search a word in all eight possible directions, i.e., North, West, South, East, North-East, North-West, South-East, South-West, but a word should not have multiple instances of the same cell.

Input:

board =
[
    ['M', 'S', 'E'],
    ['R', 'A', 'T'],
    ['L', 'O', 'N']
]

words = ["STAR", "NOTE", "SAND", "STONE"]

Output: {"STAR", "NOTE"}

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    int row[9]{-1, -1, -1, 0, 1, 0, 1, 1};
    int col[9]{-1, 1, 0, -1, -1, 1, 0, 1};

    bool isSafe(int x, int y, auto &processed) {
        return (x >= 0 && x < processed.size()) && (y >= 0 && y < processed[0].size()) &&
            !processed[x][y];
    }

    void searchBoggle(auto const &board, auto const &words, auto &result, auto &processed,
                    int i, int j, string path)
    {
        processed[i][j] = true;

        path += board[i][j];

        if (words.find(path) != words.end()) {
            result.insert(path);
        }

        for (int k = 0; k < 8; ++k)
        {
            if (isSafe(i + row[k], j + col[k], processed)) {
                searchBoggle(board, words, result, processed, i + row[k], j + col[k], path);
            }
        }

        processed[i][j] = false;
    }
public:
    unordered_set<string> searchBoggle(vector<vector<char>> const &board, unordered_set<string> const &words)
    {
        unordered_set<string> result;

        if (board.empty()) {
            return result;
        }

        int M = board.size();
        int N = board[0].size();

        vector<vector<bool>> processed(M, vector<bool>(N));

        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j) {
                // consider each character as a starting point and run DFS
                searchBoggle(board, words, result, processed, i, j, "");
            }
        }

        return result;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<vector<char>> board {
        {'M', 'S', 'E', 'F'},
        {'R', 'A', 'T', 'D'},
        {'L', 'O', 'N', 'E'}
    };

    unordered_set<string> words = { "START", "NOTE", "SAND", "STONED" };

    cout << Solution().searchBoggle(board, words) << endl;

    return 0;
}
