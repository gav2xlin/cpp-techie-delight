/*

Given a maze in the form of a rectangular matrix, filled with either 'O', 'X', or 'M', where 'O' represents an open cell, 'X' represents a blocked cell, and 'M' represents landmines in the maze.

The task is to find the shortest distance of every open cell in the maze from its nearest mine. You are only allowed to travel in either of the four directions, and diagonal moves are not allowed. The cells with landmines have a distance of 0, and blocked/unreachable cells have a distance of -1.

Input:

mat = [
    ['O', 'M', 'O', 'O', 'X'],
    ['O', 'X', 'X', 'O', 'M'],
    ['O', 'O', 'O', 'O', 'O'],
    ['O', 'X', 'X', 'X', 'O'],
    ['O', 'O', 'M', 'O', 'O'],
    ['O', 'X', 'X', 'M', 'O']
]

Here, O (Open cell), X (Blocked Cell), and M (Landmine).

Output:

[
    [1,  0,  1,  2, -1],
    [2, -1, -1,  1,  0],
    [3,  4,  3,  2,  1],
    [3, -1, -1, -1,  2],
    [2,  1,  0,  1,  2],
    [3, -1, -1,  0,  1]
]

*/

#include <iostream>
#include <vector>
#include <queue>
#include <iomanip>

using namespace std;

class Solution
{
private:
    struct Node
    {
        int x, y, distance;
    };

    bool isValid(int i, int j, int M, int N) {
        return (i >= 0 && i < M) && (j >= 0 && j < N);
    }

    bool isSafe(int i, int j, vector<vector<char>> const &mat,
            vector<vector<int>> const &result) {
        return mat[i][j] == 'O' && result[i][j] == -1;
    }

    int row[4]{0, -1, 0, 1};
    int col[4]{-1, 0, 1, 0};
public:
    vector<vector<int>> updateShortestDistance(vector<vector<char>> const &mat)
    {
        if (mat.empty()) {
            return {};
        }

        int M = mat.size();
        int N = mat[0].size();

        vector<vector<int>> result(M, vector<int>(N));

        queue<Node> q;

        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j)
            {
                if (mat[i][j] == 'M')
                {
                    q.push({i, j, 0});

                    result[i][j] = 0;
                }
                else
                {
                    result[i][j] = -1;
                }
            }
        }

        while (!q.empty())
        {
            int x = q.front().x;
            int y = q.front().y;
            int distance = q.front().distance;

            q.pop();

            for (int i = 0; i < 4; ++i)
            {
                if (isValid(x + row[i], y + col[i], M, N) &&
                    isSafe(x + row[i], y + col[i], mat, result))
                {
                    result[x + row[i]][y + col[i]] = distance + 1;
                    q.push({x + row[i], y + col[i], distance + 1});
                }
            }
        }

        return result;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << setw(3) << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    vector<vector<char>> mat {
        {'O', 'M', 'O', 'O', 'X'},
        {'O', 'X', 'X', 'O', 'M'},
        {'O', 'O', 'O', 'O', 'O'},
        {'O', 'X', 'X', 'X', 'O'},
        {'O', 'O', 'M', 'O', 'O'},
        {'O', 'X', 'X', 'M', 'O'}
    };

    cout << Solution().updateShortestDistance(mat) << endl;

    return 0;
}
