/*

Given a rectangular field with few sensors present, cross it by taking the shortest safe route without activating the sensors.

The rectangular field is in the form of an `M × N` matrix, find the shortest path from any cell in the first column to any cell in the last column of the matrix. The sensors are marked by the value 0 in the matrix, and all its eight adjacent cells can also activate the sensors. The path can only be constructed out of cells having value 1, and at any given moment, you are only allowed to move one step in either of the 4 directions - Up, Left, Down, Right.

Input:

mat = [
    [0, 1, 1, 1, 0, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [1, 1, 1, 1, 1, 0, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]

Output: 11

The shortest safe path has a length of 11, and the route is marked in green below.

https://techiedelight.com/practice/images/Shortest-Path.png

The solution should return -1 if there is no safe route to reach the destination.

*/

#include <iostream>
#include <vector>
#include <climits>
#include <queue>

using namespace std;

class Solution
{
private:
    bool isSafe(vector<vector<int>> const &field, vector<vector<bool>> const &visited, int x, int y) {
        return field[x][y] != 0 && !visited[x][y];
    }

    bool isValid(int x, int y, int M, int N) {
        return (x < M && y < N) && (x >= 0 && y >= 0);
    }

    struct Node {
        int x, y, dist;
    };

    int row[4]{-1, 0, 0, 1};
    int col[4]{0, -1, 1, 0};

    int BFS(vector<vector<int>> const &field)
    {
        // `M × N` matrix
        int M = field.size();
        int N = field[0].size();

        vector<vector<bool>> visited;
        visited.resize(M, vector<bool>(N));

        queue<Node> q;

        for (int r = 0; r < M; ++r)
        {
            if (field[r][0] == 1)
            {
                q.push({r, 0, 0});
                visited[r][0] = true;
            }
        }

        while (!q.empty())
        {
            int i = q.front().x;
            int j = q.front().y;
            int dist = q.front().dist;
            q.pop();

            if (j == N - 1) {
                return dist;
            }

            for (int k = 0; k < 4; ++k)
            {
                if (isValid(i + row[k], j + col[k], M, N) &&
                    isSafe(field, visited, i + row[k], j + col[k]))
                {
                    visited[i + row[k]][j + col[k]] = true;
                    q.push({i + row[k], j + col[k], dist + 1});
                }
            }
        }

        return INT_MAX;
    }

    int r[8]{-1, -1, -1, 0, 0, 1, 1, 1};
    int c[8]{-1, 0, 1, -1, 1, -1, 0, 1};
public:
    int findShortestDistance(vector<vector<int>> &mat)
    {
        if (mat.empty()) {
            return 0;
        }

        int M = mat.size();
        int N = mat[0].size();

        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j)
            {
                for (int k = 0; k < 8; k++) {
                    if (!mat[i][j] && isValid(i + r[k], j + c[k], M, N) && mat[i + r[k]][j + c[k]]) {
                        mat[i + r[k]][j + c[k]] = INT_MAX;
                    }
                }
            }
        }

        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j)
            {
                if (mat[i][j] == INT_MAX) {
                    mat[i][j] = 0;
                }
            }
        }

        int path = BFS(mat);
        return path != INT_MAX ? path : -1;
    }
};

int main()
{
    vector<vector<int>> mat {
        {0, 1, 1, 1, 0, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 0, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 0, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 0, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
        {1, 1, 1, 1, 1, 0, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
    };

    cout << Solution().findShortestDistance(mat) << endl;

    return 0;
}
