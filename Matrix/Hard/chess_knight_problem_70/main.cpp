/*

Given a chessboard, find the shortest distance (minimum number of steps) taken by a knight to reach a given destination from a given source.

Input:

N = 8 			(8 x 8 matrix)
src  = (0, 7)	(Source coordinates)
dest = (7, 0)	(Destination coordinates)

Output: 4

Explanation: The minimum number of steps required is 6. The knight's movement is illustrated in the following figure:

https://techiedelight.com/practice/images/Chess-Board.png


The solution should return -1 if the path is not possible.

*/

#include <iostream>
#include <utility>
#include <queue>
#include <set>

using namespace std;

class Solution
{
private:
    int row[8] { 2, 2, -2, -2, 1, 1, -1, -1 };
    int col[8] { -1, 1, 1, -1, 2, -2, 2, -2 };

    bool isValid(int x, int y, int N) {
        return (x >= 0 && x < N) && (y >= 0 && y < N);
    }

    struct Node
    {
        int x, y, dist;

        Node(int x, int y, int dist = 0): x(x), y(y), dist(dist) {}

        bool operator<(const Node& o) const {
            return x < o.x || (x == o.x && y < o.y);
        }
    };
public:
    int findShortestDistance(int N, pair<int,int> const &src, pair<int,int> const &dest)
    {
        set<Node> visited;

        queue<Node> q;
        q.push({src.first, src.second});

        while (!q.empty())
        {
            Node node = q.front();
            q.pop();

            int x = node.x;
            int y = node.y;
            int dist = node.dist;

            if (x == dest.first && y == dest.second) {
                return dist;
            }

            if (!visited.count(node))
            {
                visited.insert(node);

                for (int i = 0; i < 8; ++i)
                {
                    int x1 = x + row[i];
                    int y1 = y + col[i];

                    if (isValid(x1, y1, N)) {
                        q.push({x1, y1, dist + 1});
                    }
                }
            }
        }

        return -1;
    }
};

int main()
{
    cout << Solution().findShortestDistance(8, {0, 7}, {7, 0}) << endl;

    return 0;
}
