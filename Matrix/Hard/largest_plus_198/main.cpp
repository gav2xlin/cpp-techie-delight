/*

Given a square matrix of 0’s and 1’s, calculate the size of the largest plus formed by 1’s.

Input:

grid = [
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 1, 1, 1, 0, 1, 1],
    [1, 1, 1, 0, 1, 1, 0, 1, 0, 1],
    [0, 0, 0, 0, 1, 0, 0, 1, 0, 0],
    [1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 0],
    [1, 0, 0, 0, 1, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 0, 1, 1],
    [1, 1, 0, 0, 1, 0, 1, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 0, 0]
]

Output: 17

Explanation: The largest plus of 1’s is marked below, having size 17.


    1  0  1  1  1  1  0  1  1  1
    1  0  1  0  |  1  1  0  1  1
    1  1  1  0  |  1  0  1  0  1
    0  0  0  0  |  0  0  1  0  0
    1  1  1  0  |  1  1  1  1  1
    –  –  –  –  |  –  –  –  –  0
    1  0  0  0  |  0  0  1  0  1
    1  0  1  1  |  1  0  0  1  1
    1  1  0  0  |  0  1  0  0  1
    1  0  1  1  |  1  0  1  0  0


Input:

[
    [1, 1, 1, 1, 1, 1],
    [1, 0, 1, 1, 0, 1],
    [0, 1, 1, 0, 0, 1],
    [1, 1, 1, 1, 1, 1],
    [1, 0, 0, 1, 0, 1],
    [1, 0, 1, 1, 0, 0]
]

Output: 0

Explanation: No largest plus of 1’s can be constructed.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findLargestPlus(vector<vector<int>> const &grid)
    {
        if (grid.empty() || grid[0].empty()) {
            return 0;
        }

        int n = grid.size();

        vector<vector<int>> left(n, vector<int>(n));
        vector<vector<int>> right(n, vector<int>(n));
        vector<vector<int>> top(n, vector<int>(n));
        vector<vector<int>> bottom(n, vector<int>(n));

        for (int i = 0; i < n; ++i)
        {
            top[0][i] = grid[0][i];
            bottom[n - 1][i] = grid[n - 1][i];
            left[i][0] = grid[i][0];
            right[i][n - 1] = grid[i][n - 1];
        }

        for (int i = 0; i < n; ++i)
        {
            for (int j = 1; j < n; ++j)
            {
                if (grid[i][j] == 1) {
                    left[i][j] = left[i][j - 1] + 1;
                }

                if (grid[j][i] == 1) {
                    top[j][i] = top[j - 1][i] + 1;
                }

                if (grid[n - 1 - j][i] == 1) {
                    bottom[n - 1 - j][i] = bottom[n - j][i] + 1;
                }

                if (grid[i][n - 1 - j] == 1) {
                    right[i][n - 1 - j] = right[i][n - j] + 1;
                }
            }
        }

        int bar = 0;
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                int len = min({top[i][j], bottom[i][j], left[i][j], right[i][j]});

                if (len - 1 > bar) {
                    bar = len - 1;
                }
            }
        }

        return bar ? 4 * bar + 1 : 0;
    }
};

int main()
{
    vector<vector<int>> grid {
        {1, 0, 1, 1, 1, 1, 0, 1, 1, 1},
        {1, 0, 1, 0, 1, 1, 1, 0, 1, 1},
        {1, 1, 1, 0, 1, 1, 0, 1, 0, 1},
        {0, 0, 0, 0, 1, 0, 0, 1, 0, 0},
        {1, 1, 1, 0, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
        {1, 0, 0, 0, 1, 0, 0, 1, 0, 1},
        {1, 0, 1, 1, 1, 1, 0, 0, 1, 1},
        {1, 1, 0, 0, 1, 0, 1, 0, 0, 1},
        {1, 0, 1, 1, 1, 1, 0, 1, 0, 0}
    };

    cout << Solution().findLargestPlus(grid) << endl;

    return 0;
}
