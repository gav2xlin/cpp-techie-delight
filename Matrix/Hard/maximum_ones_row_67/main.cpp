/*

Given a binary `M × N` row-wise sorted matrix, find a row that contains the maximum number of 1’s in linear time.

Input:
[
    [0, 0, 0, 1, 1],
    [0, 0, 1, 1, 1],
    [0, 0, 0, 0, 0],
    [0, 1, 1, 1, 1],
    [0, 0, 0, 0, 1]
]

Output: 4
Explanation: The maximum 1’s are present in row 4


If multiple rows have same maximum number of 1’s, the solution should return the first row number. If no 1’s are present in the matrix, the solution should return 0.

Input:
[
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
]

Output: 1

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findRow(vector<vector<int>> const &mat)
    {
        if (mat.empty()) {
            return 0;
        }

        int M = mat.size();
        int N = mat[0].size();

        int row = -1, i = 0, j = N - 1;

        while (i <= M - 1 && j >= 0)
        {
            if (mat[i][j]) {
                --j, row = i;
            }
            else
            {
                ++i;
            }
        }

        return row + 1;
    }
};

int main()
{
    {
        vector<vector<int>> mat {
            {0, 0, 0, 1, 1},
            {0, 0, 1, 1, 1},
            {0, 0, 0, 0, 0},
            {0, 1, 1, 1, 1},
            {0, 0, 0, 0, 1}
        };

        cout << Solution().findRow(mat) << endl;
    }

    {
        vector<vector<int>> mat {
            {1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1}
        };

        cout << Solution().findRow(mat) << endl;
    }

    return 0;
}
