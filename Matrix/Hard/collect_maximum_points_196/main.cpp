/*

Given an `M × N` matrix where each cell can have a value of 1, 0, or -1, where -1 denotes an unsafe cell, collect the maximum number of ones starting from the first cell and by visiting only safe cells (i.e., 0 or 1). If the row is odd, you are only allowed to go left or down from the current cell; if the row is even, you can go right or down.

Input:

[
    [ 1,  1, -1,  1,  1],
    [ 1,  0,  0, -1,  1],
    [ 1,  1,  1,  1, -1],
    [-1, -1,  1,  1,  1],
    [ 1,  1, -1, -1,  1]
]

Output: 9

Explanation: The maximum value that can be collected is 9, as marked below.


    1 — 1  -1   1   1
        |
    1 — 0   0  -1   1
    |
    1 — 1 — 1 — 1  -1
                |
   -1  -1   1 — 1   1

    1   1  -1  -1   1

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
private:
    bool isSafe(vector<vector<int>> const &mat, int i, int j) {
        return !(i < 0 || i >= mat.size() || j < 0 || j >= mat[0].size() || mat[i][j] == -1);
    }

    int findMaximum(vector<vector<int>> const &mat, int i, int j)
    {
        if (mat.empty()) {
            return 0;
        }

        if (!isSafe(mat, i, j)) {
            return 0;
        }

        if (i & 1)
        {
            return mat[i][j] + max(findMaximum(mat, i, j - 1), findMaximum(mat, i + 1, j));
        }
        else
        {
            return mat[i][j] + max(findMaximum(mat, i, j + 1), findMaximum(mat, i + 1, j));
        }
    }
public:
    int findMaximumPoints(vector<vector<int>> const &mat)
    {
        return findMaximum(mat, 0, 0);
    }
};

int main()
{
    vector<vector<int>> mat {
        {1, 1, -1, 1, 1},
        {1, 0, 0, -1, 1},
        {1, 1, 1, 1, -1},
        {-1, -1, 1, 1, 1},
        {1, 1, -1, -1, 1}
    };

    cout << Solution().findMaximumPoints(mat) << endl;

    return 0;
}
