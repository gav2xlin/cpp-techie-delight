/*

Given an `M × N` matrix of integers whose each cell can contain a negative, zero, or a positive value, determine the minimum number of passes required to convert all negative values in the matrix positive.

Only a non-zero positive value at cell (i, j) can convert negative values present at its adjacent cells (i-1, j), (i+1, j), (i, j-1), and (i, j+1), i.e., up, down, left and right.

Input:

mat = [
    [-1, -9,  0, -1,  0],
    [-8, -3, -2,  9, -7],
    [ 2,  0,  0, -6,  0],
    [ 0, -7, -3,  5, -4]
]

Output: 3


Input:

mat = [
    [1, 9, 1],
    [8, 3, 2],
    [7, 3, 4]
]

Output: 0


The solution should return -1 if conversion is not possible.

Input:

mat = [
    [-1, -9, -1],
    [-8, -3, -2],
    [-7, -3, -4]
]

Output: -1

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

class Solution
{
private:
    bool isValid(int i, int j, vector<vector<int>> &mat) {
        return (i >= 0 && i < mat.size()) && (j >= 0 && j < mat[0].size());
    }

    int row[4] {-1, 0, 0, 1};
    int col[4] {0, -1, 1, 0};

    bool hasNegative(vector<vector<int>> &mat)
    {
        for (int i = 0; i < mat.size(); ++i)
        {
            for (int j = 0; j < mat[0].size(); ++j)
            {
                if (mat[i][j] < 0) {
                    return true;
                }
            }
        }
        return false;
    }

    struct Point {
        int x, y;
    };
public:
    int findMinPasses(vector<vector<int>> &mat)
    {
        if (mat.empty()) {
            return 0;
        }

        queue<Point> Q;

        for (int i = 0; i < mat.size(); ++i)
        {
            for (int j = 0; j < mat[0].size(); ++j)
            {
                if (mat[i][j] > 0) {
                    Q.push({i, j});
                }
            }
        }

        int passes = 0;

        while (!Q.empty())
        {
            // use two queues to separate positive numbers involved in the
            // previous pass with positive numbers involved in the current pass
            queue<Point> q;

            swap(Q, q);

            while (!q.empty())
            {
                auto [x, y] = q.front();
                q.pop();

                for (int k = 0; k < 4; ++k)
                {
                    if (isValid(x + row[k], y + col[k], mat) && mat[x + row[k]][y + col[k]] < 0)
                    {
                        mat[x + row[k]][y + col[k]] = -mat[x + row[k]][y + col[k]];

                        Q.push({x + row[k], y + col[k]});
                    }
                }
            }

            ++passes;
        }

        return hasNegative(mat) ? -1 : (passes - 1);
    }
};

int main()
{
    {
        vector<vector<int>> mat {
            {-1, -9, 0, -1, 0},
            {-8, -3, -2, 9, -7},
            {2, 0, 0, -6, 0},
            {0, -7, -3, 5, -4}
        };

        cout << Solution().findMinPasses(mat) << endl;
    }

    {
        vector<vector<int>> mat {
            {1, 9, 1},
            {8, 3, 2},
            {7, 3, 4}
        };

        cout << Solution().findMinPasses(mat) << endl;
    }

    {
        vector<vector<int>> mat {
            {-1, -9, -1},
            {-8, -3, -2},
            {-7, -3, -4}
        };

        cout << Solution().findMinPasses(mat) << endl;
    }

    return 0;
}
