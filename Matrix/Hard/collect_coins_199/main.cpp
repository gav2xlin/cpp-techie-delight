/*

Given an `M × N` matrix of non-negative integers where each cell contains a coin of some denomination, collect the maximum value of coins by traversing the grid.

The first traversal starts from the top-left corner of the matrix and ends in the bottom-left corner, and the second traversal starts from the top-right corner and ends in the bottom-right corner. From any cell (i, j) in the matrix, you are allowed to move to cell (i+1, j+1) or (i+1, j-1) or (i+1, j). If both traversals pass through the same cell, only one can collect coins from that cell.

Input:

[
    [0, 2, 4, 1],
    [4, 8, 3, 7],
    [2, 3, 6, 2],
    [9, 7, 8, 3],
    [1, 5, 9, 4]
]

Output: 47

Explanation:

First traversal : [0, 8, 3, 9, 1]
Second traversal: [1, 7, 6, 8, 4]

*/

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

class Solution
{
private:
    int isValid(int i, int x, int y, int M, int N) {
        return i < M && x >= 0 && x < N && y >= 0 && y < N;
    }

    int getMaxCoins(vector<vector<int>> const &mat, int i, int x, int y)
    {
        int M = mat.size();
        int N = mat[0].size();

        if (!isValid(i, x, y, M, N)) {
            return INT_MIN;
        }

        if (i == M - 1)
        {
            if (x == 0 && y == N - 1) {
                return (x == y) ? mat[i][x] : mat[i][x] + mat[i][y];
            }

            return INT_MIN;
        }

        int coins = INT_MIN;

        /*
          Recur for all possible ways:
            (i, x) —> (i+1, x-1) or (i+1, x) or (i+1, x+1)
            (i, y) —> (i+1, y-1) or (i+1, y) or (i+1, y+1)
        */

        coins = max(coins, getMaxCoins(mat, i + 1, x - 1, y - 1));
        coins = max(coins, getMaxCoins(mat, i + 1, x - 1, y));
        coins = max(coins, getMaxCoins(mat, i + 1, x - 1, y + 1));

        coins = max(coins, getMaxCoins(mat, i + 1, x, y - 1));
        coins = max(coins, getMaxCoins(mat, i + 1, x, y));
        coins = max(coins, getMaxCoins(mat, i + 1, x, y + 1));

        coins = max(coins, getMaxCoins(mat, i + 1, x + 1, y - 1));
        coins = max(coins, getMaxCoins(mat, i + 1, x + 1, y));
        coins = max(coins, getMaxCoins(mat, i + 1, x + 1, y + 1));

        if (x == y) {
            return mat[i][x] + coins;
        }
        else {
            return (mat[i][x] + mat[i][y]) + coins;
        }
    }
public:
    int getMaxCoins(vector<vector<int>> const &mat)
    {
        if (mat.empty()) {
            return 0;
        }

        int N = mat[0].size();

        return getMaxCoins(mat, 0, 0, N - 1);
    }
};

int main()
{
    vector<vector<int>> mat {
        { 0, 2, 4, 1 },
        { 4, 8, 3, 7 },
        { 2, 3, 6, 2 },
        { 9, 7, 8, 3 },
        { 1, 5, 9, 4 }
    };

    cout << Solution().getMaxCoins(mat) << endl;

    return 0;
}
