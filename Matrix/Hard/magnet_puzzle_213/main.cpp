/*

We are given a set of bipolar magnets, each domino-shaped. The objective is to place magnets on an `M × N` board, which satisfies a set of conditions where both M and N are not odd.

For instance, the following problem has the solution on its right:

https://techiedelight.com/practice/images/Magnet-Puzzle.png

Each `2 × 1` or `1 × 2` grid in the board can contain a magnet or empty. The blank entry will be indicated by X’s, and the magnet will be represented by `+` and `-` (For the positive and negative end, respectively). The digits along the board’s left and top sides represent the count of `+` squares in corresponding rows or columns. Similarly, those along the right and bottom show the total number of `-` signs in particular rows or columns. Rows and columns for which no number is mentioned can have any number of `+` or `-` signs. The puzzle solution must also satisfy the constraint that no two adjacent squares can have the same sign. But diagonally joined squares can have the same sign.

Example 1:

The top[], bottom[], left[], right[] arrays indicates the count of `+` or `-` along the top (+), bottom (-), left (+), and right (-) edges, respectively. The value of -1 indicate any number of `+` or `-` signs.

top[] = [1, -1, -1, 2, 1, -1]
bottom[] = [2, -1, -1, 2, -1, 3]
left[] = [2, 3, -1, -1, -1]
right[] = [-1, -1, -1, 1, -1]

The rules[][] matrix can contain any T, B, L, or R character. T indicates its top end for a vertical slot in the board, and B indicates the bottom end. L indicates the left end, and R indicates the right end for a horizontal slot in the board.

rules[][] =
[
    [L, R, L, R, T, T],
    [L, R, L, R, B, B],
    [T, T, T, T, L, R],
    [B, B, B, B, T, T],
    [L, R, L, R, B, B]
]

Output:
[
    [+, -, +, -, X, -],
    [-, +, -, +, X, +],
    [X, X, +, -, +, -],
    [X, X, -, +, X, +],
    [-, +, X, X, X, -]
]


Example 2:

top[] = [2, -1, -1]
bottom[] = [-1, -1, 2]
left[] = [-1, -1, 2, -1]
right[] = [0, -1, -1, -1]

rules[][] =
[
    [T, T, T],
    [B, B, B],
    [T, L, R],
    [B, L, R]
]

Output:
[
    [+, X, +],
    [-, X, -],
    [+, -, +],
    [-, +, -]
]

The solution should return an empty vector if the solution does not exist.

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    void printSolution(vector<vector<char>> const &board)
    {
        int M = board.size();
        int N = board[0].size();

        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j) {
                cout << board[i][j] << " ";
            }
            cout << endl;
        }
    }

    int countInColumns(vector<vector<char>> const &board, char ch, int j)
    {
        int M = board.size();

        int count = 0;
        for (int i = 0; i < M; ++i)
        {
            if (board[i][j] == ch) {
                count++;
            }
        }

        return count;
    }

    int countInRow(vector<vector<char>> const &board, char ch, int i)
    {
        int N = board[i].size();

        int count = 0;
        for (int j = 0; j < N; ++j)
        {
            if (board[i][j] == ch) {
                count++;
            }
        }

        return count;
    }

    bool isSafe(vector<vector<char>> const &board, int row, int col, char ch,
                vector<int> const &top, vector<int> const &left,
                vector<int> const &bottom, vector<int> const &right)
    {
        int M = board.size();
        int N = board[0].size();

        if ((row - 1 >= 0 && board[row - 1][col] == ch) ||
                (col + 1 < N && board[row][col + 1] == ch) ||
                (row + 1 < M && board[row + 1][col] == ch) ||
                (col - 1 >= 0 && board[row][col - 1] == ch)) {
            return false;
        }

        int rowCount = countInRow(board, ch, row);

        int colCount = countInColumns(board, ch, col);

        if (ch == '+')
        {
            if (top[col] != -1 && colCount >= top[col]) {
                return false;
            }

            if (left[row] != -1 && rowCount >= left[row]) {
                return false;
            }
        }

        if (ch == '-')
        {
            if (bottom[col] != -1 && colCount >= bottom[col]) {
                return false;
            }

            if (right[row] != -1 && rowCount >= right[row]) {
                return false;
            }
        }

        return true;
    }

    bool validateConfiguration(vector<vector<char>> const &board,
                               vector<int> const &top, vector<int> const &left,
                               vector<int> const &bottom, vector<int> const &right)
    {
        int M = board.size();
        int N = board[0].size();

        for (int i = 0; i < N; ++i)
        {
            if (top[i] != -1 && countInColumns(board, '+', i) != top[i]) {
                return false;
            }
        }

        for (int j = 0; j < M; ++j)
        {
            if (left[j] != -1 && countInRow(board, '+', j) != left[j]) {
                return false;
            }
        }

        for (int i = 0; i < N; ++i)
        {
            if (bottom[i] != -1 && countInColumns(board, '-', i) != bottom[i]) {
                return false;
            }
        }

        for (int j = 0; j < M; ++j)
        {
            if (right[j] != -1 && countInRow(board, '-', j) != right[j]) {
                return false;
            }
        }

        return true;
    }

    bool solveMagnetPuzzle(vector<vector<char>> &board, int row, int col,
                           vector<int> const &top, vector<int> const &left,
                           vector<int> const &bottom, vector<int> const &right,
                           vector<vector<char>> const &rules)
    {
        int M = board.size();
        int N = board[0].size();

        if (row >= M - 1 && col >= N - 1)
        {
            if (validateConfiguration(board, top, left, bottom, right)) {
                return true;
            }

            return false;
        }

        if (col >= N)
        {
            col = 0;
            row = row + 1;
        }

        if (rules[row][col] == 'R' || rules[row][col] == 'B')
        {
            if (solveMagnetPuzzle(board, row, col + 1, top,
                                    left, bottom, right, rules)) {
                return true;
            }
        }

        if (rules[row][col] == 'L' && rules[row][col + 1] == 'R')
        {
            if (isSafe(board, row, col, '+', top, left, bottom, right) &&
                isSafe(board, row, col + 1, '-', top, left, bottom, right))
            {
                board[row][col] = '+';
                board[row][col + 1] = '-';

                if (solveMagnetPuzzle(board, row, col + 2,
                                top, left, bottom, right, rules)) {
                    return true;
                }

                // if it doesn't lead to a solution, backtrack
                board[row][col] = 'X';
                board[row][col + 1] = 'X';
            }

            if (isSafe(board, row, col, '-', top, left, bottom, right) &&
                isSafe(board, row, col + 1, '+', top, left, bottom, right))
            {
                board[row][col] = '-';
                board[row][col + 1] = '+';

                if (solveMagnetPuzzle(board, row, col + 2,
                                top, left, bottom, right, rules)) {
                    return true;
                }

                board[row][col] = 'X';
                board[row][col + 1] = 'X';
            }
        }

        if (rules[row][col] == 'T' && rules[row + 1][col] == 'B')
        {
            if (isSafe(board, row, col, '+', top, left, bottom, right) &&
                isSafe(board, row + 1, col, '-', top, left, bottom, right))
            {
                board[row][col] = '+';
                board[row + 1][col] = '-';

                if (solveMagnetPuzzle(board, row, col + 1,
                                top, left, bottom, right, rules)) {
                    return true;
                }

                board[row][col] = 'X';
                board[row + 1][col] = 'X';
            }

            if (isSafe(board, row, col, '-', top, left, bottom, right) &&
                isSafe(board, row + 1, col, '+', top, left, bottom, right))
            {
                board[row][col] = '-';
                board[row + 1][col] = '+';

                if (solveMagnetPuzzle(board, row, col + 1,
                                top, left, bottom, right, rules)) {
                    return true;
                }

                board[row][col] = 'X';
                board[row + 1][col] = 'X';
            }
        }

        if (solveMagnetPuzzle(board, row, col + 1, top, left, bottom, right, rules)) {
            return true;
        }

        return false;
    }
public:
    vector<vector<char>> solveMagnetPuzzle(vector<vector<char>> const &rules,
                                           vector<int> const &top, vector<int> const &bottom,
                                           vector<int> const &left, vector<int> const &right)
    {
        int M = rules.size();
        int N = rules[0].size();

        vector<vector<char>> board(M, vector<char>(N, 'X'));

        if (!solveMagnetPuzzle(board, 0, 0, top, left, bottom, right, rules))
        {
            return {};
        }

        printSolution(board);

        return board;
    }
};

ostream& operator<<(ostream& os, const vector<char>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<char>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    {
        vector<int> top { 1, -1, -1, 2, 1, -1 };
        vector<int> bottom { 2, -1, -1, 2, -1, 3 };
        vector<int> left { 2, 3, -1, -1, -1 };
        vector<int> right { -1, -1, -1, 1, -1 };

        vector<vector<char>> rules =
        {
            { 'L', 'R', 'L', 'R', 'T', 'T' },
            { 'L', 'R', 'L', 'R', 'B', 'B' },
            { 'T', 'T', 'T', 'T', 'L', 'R' },
            { 'B', 'B', 'B', 'B', 'T', 'T' },
            { 'L', 'R', 'L', 'R', 'B', 'B' }
        };

        cout << Solution().solveMagnetPuzzle(rules, top, bottom, left, right) << endl;
    }

    {
        vector<int> top {2, -1, -1};
        vector<int> bottom {-1, -1, 2};
        vector<int> left {-1, -1, 2, -1};
        vector<int> right {0, -1, -1, -1};

        vector<vector<char>> rules =
        {
            {'T', 'T', 'T'},
            {'B', 'B', 'B'},
            {'T', 'L', 'R'},
            {'B', 'L', 'R'}
        };

        cout << Solution().solveMagnetPuzzle(rules, top, bottom, left, right) << endl;
    }

    return 0;
}
