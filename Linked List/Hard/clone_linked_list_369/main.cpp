/*

Given a singly-linked list of integers with each node containing an additional random pointer, efficiently clone it. The random pointer can point to any random node of the linked list or nullptr.

The solution should return a new linked list which is identical to the given list in terms of its structure and contents, and it should not use any nodes of the list.

              ┌──────
              ⮟		│
Input : 1 ——⮞ 2 ——⮞ 3 ——⮞ 4 ——⮞ 5 ——⮞ nullptr		// Original linked list
        │				   ⮝
        └──────────────────┘

              ┌──────
              ⮟		│
Output: 1 ——⮞ 2 ——⮞ 3 ——⮞ 4 ——⮞ 5 ——⮞ nullptr		// Cloned linked list
        │				   ⮝
        └──────────────────┘

*/

#include <iostream>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* next = nullptr; 		// pointer to the next node
    Node* random = nullptr;		// pointer to the random node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next, Node *random): data(data), next(next), random(random) {}
};

class Solution
{
private:
        Node* clone(Node* head, unordered_map<Node*, Node*> &map)
        {
            if (head == nullptr) {
                return nullptr;
            }

            map[head] = new Node(head->data);

            map[head]->next = clone(head->next, map);

            return map[head];
        }

        void updateRandom(Node* head, unordered_map<Node*, Node*> &map)
        {
            if (map[head] == nullptr) {
                return;
            }

            map[head]->random = map[head->random];

            updateRandom(head->next, map);
        }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* next = nullptr; 		// pointer to the next node
            Node* random = nullptr;		// pointer to the random node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next, Node *random): data(data), next(next), random(random) {}
        };
    */

    /*Node* clone(Node* head)
    {
        unordered_map<Node*, Node*> map;

        clone(head, map);

        updateRandom(head, map);

        return map[head];
    }*/
    Node* clone(Node* head)
    {
        Node* curr = head;
        while (curr != nullptr)
        {
            Node* next = curr->next;

            Node* dup = new Node(curr->data);

            curr->next = dup;
            dup->next = next;

            curr = next;
        }

        curr = head;
        while (curr != nullptr)
        {
            if (curr->random != nullptr) {
                (curr->next)->random = (curr->random)->next;
            }

            curr = (curr->next)->next;
        }

        Node dummy;
        Node* tail = &dummy;

        curr = head;
        while (curr != nullptr)
        {
            Node* next = curr->next->next;

            Node* dup = curr->next;
            tail->next = dup;
            tail = dup;

            curr->next = next;
            curr = next;
        }

        return dummy.next;
    }
};

void traverse(Node* head)
{
    if (head == nullptr)
    {
        cout << "null" << endl;
        return;
    }

    if (head->random) {
        cout << head->data << "(" << head->random->data << ") —> ";
    }
    else {
        cout << head->data << "(" << "X" << ") —> ";
    }

    traverse(head->next);
}

int main()
{
    Node* head = new Node(1);
    head->next = new Node(2);
    head->next->next = new Node(3);
    head->next->next->next = new Node(4);
    head->next->next->next->next = new Node(5);

    head->random = head->next->next->next;
    head->next->next->random = head->next;

    cout << "original:\n";
    traverse(head);

    Node* clone = Solution().clone(head);

    cout << "\nclone:\n";
    traverse(clone);

    return 0;
}
