/*

Given a singly-linked list of integers and a positive number k, swap the k'th node from the beginning with the k'th node from the end. The swapping should be done so that only links between the nodes are exchanged, and no data is swapped.

Input:

Linked List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> nullptr
k = 2

Output: 1 —> 7 —> 3 —> 4 —> 5 —> 6 —> 2 —> 8 —> nullptr


Input:

Linked List: 1 —> 2 —> nullptr
k = 2

Output: 2 —> 1 —> nullptr


Assume that k is less than or equal to the length of linked list.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void swapNodes(Node* &head, int k)
    {
        Node *x, *y, *prev_x = nullptr, *prev_y = head;

        Node* curr = head;
        for (int i = 1; i < k && curr; i++)
        {
            prev_x = curr;
            curr = curr->next;
        }
        x = curr;

        if (curr == nullptr) {
            return;
        }

        Node* ptr = head;
        while (curr->next)
        {
            prev_y = ptr;
            ptr = ptr->next;
            curr = curr->next;
        }
        y = ptr;

        if (x->next == y)
        {
            x->next = y->next;
            y->next = x;

            if (prev_x != nullptr && prev_x != x) {
                prev_x->next = y;
            } else {
                head = y;
            }
        } else if (y->next == x) {
            y->next = x->next;
            x->next = y;

            if (prev_y != nullptr && prev_y != y) {
                prev_y->next = x;
            } else {
                head = x;
            }
        } else if (x == head) {
            head = y;
            y->next = x->next;
            prev_y->next = x;
            x->next = nullptr;
        } else if (y == head) {
            head = x;
            x->next = y->next;
            prev_x->next = y;
            y->next = nullptr;
        } else {
            ptr = y->next;
            y->next = x->next;
            x->next = ptr;

            prev_x->next = y;
            prev_y->next = x;
        }
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8});
        cout << "before: " << head << endl;

        Solution().swapNodes(head, 2);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        Node* head = construct({1, 2});
        cout << "before: " << head << endl;

        Solution().swapNodes(head, 2);

        cout << "after: " << head << endl;
        clean(head);
    }

    return 0;
}
