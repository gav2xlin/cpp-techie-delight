/*

Given a singly-linked list of integers, split it into two lists containing alternating elements from the original list.

Input: 1 —> 2 —> 3 —> 4 —> nullptr
Output: [[1 —> 3 —> nullptr], [2 —> 4 —> nullptr]]

Input: 1 —> 2 —> 3 —> 4 —> 5 —> nullptr
Output: [[1 —> 3 —> 5 —> nullptr], [2 —> 4 —> nullptr]]

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void moveNode(Node* &destRef, Node* &sourceRef)
    {
        if (sourceRef == nullptr) {
            return;
        }

        Node* newNode = sourceRef;
        sourceRef = sourceRef->next;
        newNode->next = destRef;
        destRef = newNode;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    vector<Node*> frontBackSplit(Node* &head)
    {
        Node aDummy{0, nullptr};
        Node* aTail = &aDummy;

        Node bDummy{0, nullptr};
        Node* bTail = &bDummy;

        Node* current = head;
        while (current != nullptr)
        {
            moveNode(aTail->next, current);
            aTail = aTail->next;

            if (current != nullptr)
            {
                moveNode(bTail->next, current);
                bTail = bTail->next;
            }
        }

        return {aDummy.next, bDummy.next};
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        Node* head = construct({1, 2, 3, 4});
        cout << "before: " << head << endl;

        auto res = Solution().frontBackSplit(head);

        cout << "after:" << endl;
        for (auto& vec : res) {
            cout << vec << endl;
        }
        clean(head);
    }

    {
        Node* head = construct({1, 2, 3, 4, 5});
        cout << "before: " << head << endl;

        auto res = Solution().frontBackSplit(head);

        cout << "after:" << endl;
        for (auto& vec : res) {
            cout << vec << endl;
        }
        clean(head);
    }

    return 0;
}
