/*

Given a singly-linked list and two positive numbers m and n where m <= n, reverse the portion of list from position m to n. The swapping of data is not allowed, only links should be changed.

Input:

Linked List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> nullptr

m = 2 (start position)
n = 5 (end position)

Output: 1 —> 5 —> 4 —> 3 —> 2 —> 6 —> 7 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void reverse(Node* &head, int m, int n)
    {
        if (m > n) {
            return;
        }

        Node* prev = nullptr;
        Node* curr = head;

        for (int i = 1; curr != nullptr && i < m; ++i)
        {
            prev = curr;
            curr = curr->next;
        }

        Node* start = curr;
        Node* end = nullptr;

        for (int i = 1; curr != nullptr && i <= n - m + 1; ++i)
        {
            Node* next = curr->next;

            curr->next = end;
            end = curr;

            curr = next;
        }

        if (start)
        {
            start->next = curr;
            if (prev != nullptr) {
                prev->next = end;
            } else {
                head = end;
            }
        }
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os <<node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    /*
    Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> nullptr
    m = 2 (start position)
    n = 5 (end position)
    Output: 1 —> 5 —> 4 —> 3 —> 2 —> 6 —> 7 —> nullptr
    */
    Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    cout << "before: " << head << endl;

    Solution().reverse(head, 2, 5);

    cout << "after: " << head << endl;
    clean(head);

    return 0;
}
