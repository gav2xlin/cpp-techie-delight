/*

Given a singly-linked list representation of two positive numbers, calculate and store their sum in a new list without using any extra space.

Input: X = 5 —> 7 —> 3 —> 4 —> nullptr, Y = 9 —> 4 —> 6 —> nullptr
Output: 6 —> 6 —> 8 —> 0 —> nullptr
Explanation: 5734 + 946 = 6680

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void reverse(Node* &head) {
        if (head->next == nullptr) return;

        Node *cur = head, *prev = nullptr, *next;
        while (cur != nullptr) {
            Node* next = cur->next;
            cur->next = prev;

            prev = cur;
            cur = next;
        }
        head = prev;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* addDigit(Node* X, Node* Y)
    {
        Node *Z = nullptr, *prev = nullptr;

        reverse(X);
        reverse(Y);

        int carry = 0;
        while (X != nullptr || Y != nullptr) {
            int sum = 0;
            if (X != nullptr) {
                sum += X->data;
                X = X->next;
            }
            if (Y != nullptr) {
                sum += Y->data;
                Y = Y->next;
            }
            sum += carry;

            carry = sum / 10;
            sum %= 10;

            Node* node = new Node{sum};
            if (Z == nullptr) {
                Z = node;
                prev = Z;
            } else {
                prev->next = node;
                prev = node;
            }
        }

        if (carry) {
            prev->next = new Node{carry};
        }

        reverse(Z);

        return Z;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* X = construct({5, 7, 3, 4});
    Node* Y = construct({9, 4, 6});

    cout << "before:\n" << X << '\n' << Y << endl;

    Node* Z = Solution().addDigit(X, Y);

    cout << "after:\n" << Z << endl;

    clean(X);
    clean(Y);
    clean(Z);

    return 0;
}
