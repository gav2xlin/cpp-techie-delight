/*

Given a singly-linked list of integers, move every even node to the end of the list in reverse order.

Input : 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> nullptr
Output: 1 —> 3 —> 5 —> 7 —> 6 —> 4 —> 2 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void moveNode(Node* &destRef, Node* &sourceRef)
    {
        if (sourceRef == nullptr) {
            return;
        }

        Node* newNode = sourceRef;
        sourceRef = sourceRef->next;
        newNode->next = destRef;
        destRef = newNode;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void rearrange(Node* head)
    {
        if (head == nullptr) {
            return;
        }

        Node* odd = head;
        Node *even = nullptr, *prev = nullptr;

        while (odd != nullptr && odd->next != nullptr)
        {
            moveNode(even, odd->next);

            // update `prev` and move to the next odd node
            prev = odd;
            odd = odd->next;
        }

        if (odd != nullptr) {
            odd->next = even;
        }
        else {
            prev->next = even;
        }
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* head = construct({1, 2, 3, 4, 5, 6, 7});
    cout << "before: " << head << endl;

    Solution().rearrange(head);

    cout << "after: " << head << endl;
    clean(head);

    return 0;
}
