/*

Given a singly-linked list of integers, rearrange its nodes such that alternate positions are filled with nodes starting from the beginning and end of the list.

Input : 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> nullptr
Output: 1 —> 6 —> 2 —> 5 —> 3 —> 4 —> nullptr

Input : 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> nullptr
Output: 1 —> 7 —> 2 —> 6 —> 3 —> 5 —> 4 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    Node* findMiddle(Node* head)
    {
        Node *prev = nullptr;
        Node *slow = head, *fast = head;

        while (fast != nullptr && fast->next != nullptr)
        {
            prev = slow;
            slow = slow->next;
            fast = fast->next->next;
        }

        if (fast != nullptr && fast->next == nullptr)
        {
            prev = slow;
            slow = slow->next;
        }

        prev->next = nullptr;

        return slow;
    }

    void reverse(Node* &head)
    {
        Node* result = nullptr;
        Node* current = head;

        while (current != nullptr)
        {
            Node* next = current->next;

            current->next = result;
            result = current;

            current = next;
        }

        head = result;
    }

    Node* shuffleMerge(Node* a, Node* b)
    {
        if (a == nullptr) {
            return b;
        }

        if (b == nullptr) {
            return a;
        }

        Node* recur = shuffleMerge(a->next, b->next);

        Node* result = a;
        a->next = b;
        b->next = recur;

        return result;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void rearrange(Node* head)
    {
        if (head == nullptr) {
            return;
        }

        Node* mid = findMiddle(head);

        reverse(mid);

        shuffleMerge(head, mid);
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        Node* head = construct({1, 2, 3, 4, 5, 6});
        cout << "before: " << head << endl;

        Solution().rearrange(head);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        Node* head = construct({1, 2, 3, 4, 5, 6, 7});
        cout << "before: " << head << endl;

        Solution().rearrange(head);

        cout << "after: " << head << endl;
        clean(head);
    }

    return 0;
}
