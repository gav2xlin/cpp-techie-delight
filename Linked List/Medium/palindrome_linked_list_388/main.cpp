/*

Given a singly-linked list of integers, determine whether the linked list is a palindrome.

Input: 1 —> 2 —> 3 —> 2 —> 1 —> nullptr
Output: true

Input: 1 —> 2 —> 3 —> 3 —> 1 —> nullptr
Output: false

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    bool checkPalindrome(Node* &left, Node* right)
    {
        if (right == nullptr) {
            return true;
        }

        bool result = checkPalindrome(left, right->next) && (left->data == right->data);
        left = left->next;

        return result;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    bool isPalindrome(Node* head)
    {
        return checkPalindrome(head, head);
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }
    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        /*
        Input: 1 —> 2 —> 3 —> 2 —> 1 —> nullptr
        Output: true
        */
        Node *head = construct({1, 2, 3, 2, 1});

        cout << head << ' ' << boolalpha << Solution().isPalindrome(head) << endl;

        clean(head);
    }

    {
        /*
        Input: 1 —> 2 —> 3 —> 3 —> 1 —> nullptr
        Output: false
         */
        Node *head = construct({1, 2, 3, 3, 1});

        cout << head << ' ' << boolalpha << Solution().isPalindrome(head) << endl;

        clean(head);
    }

    return 0;
}
