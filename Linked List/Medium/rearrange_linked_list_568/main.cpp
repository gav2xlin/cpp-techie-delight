/*

Given a singly-linked list of integers, rearrange it such that every second node of the linked list is greater than its left and right nodes. In other words, rearrange the linked list node in alternating high-low.

Assume that no duplicate nodes are present in the linked list. Since several lists might satisfy the constraints, the solution should return any one of them in single traveral of the linked list.

Input : 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> nullptr
Output: 1 —> 3 —> 2 —> 5 —> 4 —> 7 —> 6 —> nullptr

Input : 9 —> 6 —> 8 —> 3 —> 7 —> nullptr
Output: 6 —> 9 —> 3 —> 8 —> 7 —> nullptr

Input : 6 —> 9 —> 2 —> 5 —> 1 —> 4 —> nullptr
Output: 6 —> 9 —> 2 —> 5 —> 1 —> 4 —> nullptr

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void rearrange(Node* &head)
    {
        if (head == nullptr) {
            return;
        }

        Node* prev = head;
        Node* curr = head->next;

        while (curr != nullptr)
        {
            if (prev->data > curr->data) {
                swap(prev, curr);
            }

            if (curr->next && curr->next->data > curr->data) {
                swap(curr->next->data, curr->data);
            }

            prev = curr->next;

            if (curr->next == nullptr) {
                break;
            }

            curr = curr->next->next;
        }
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        Node* head = construct({1, 2, 3, 4, 5, 6, 7});
        cout << "before: " << head << endl;

        Solution().rearrange(head);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        Node* head = construct({9, 6, 8, 3, 7});
        cout << "before: " << head << endl;

        Solution().rearrange(head);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        Node* head = construct({6, 9, 2, 5, 1, 4});
        cout << "before: " << head << endl;

        Solution().rearrange(head);

        cout << "after: " << head << endl;
        clean(head);
    }

    return 0;
}
