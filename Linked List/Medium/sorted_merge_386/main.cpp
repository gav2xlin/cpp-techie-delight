/*

Given two sorted singly-linked lists of integers, merge them without modifying the links of the first list.

If m and n are the total number of nodes in the first and second list, then the first m smallest nodes in both lists combined should become part of the first list, and the remaining nodes should become part of the second list. The solution should preserve the sorted order of elements in both lists.

Input:

X: 2 —> 6 —> 9 —> 10 —> 15 —> nullptr
Y: 1 —> 4 —> 5 —> 20 —> nullptr

Output:

X: 1 —> 2 —> 4 —> 5 —> 6 —> nullptr
Y: 9 —> 10 —> 15 —> 20 —> nullptr

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void sortedInsert(Node* &head, Node *newNode)
    {
        if (head == nullptr || head->data >= newNode->data)
        {
            newNode->next = head;
            head = newNode;
            return;
        }

        Node* current = head;
        while (current->next != nullptr && current->next->data < newNode->data) {
            current = current->next;
        }

        newNode->next = current->next;
        current->next = newNode;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void sortedMerge(Node* X, Node* &Y)
    {
        while (X != nullptr && Y != nullptr)
        {
            if (X->data > Y->data)
            {
                swap(X->data, Y->data);

                Node* front = Y;
                Y = Y->next;

                sortedInsert(Y, front);
            }

            X = X->next;
        }
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* X = construct({2, 6, 9, 10, 15});
    Node* Y = construct({1, 4, 5, 20});
    cout << "before: X=" << X << " Y=" << Y << endl;

    Solution().sortedMerge(X, Y);

    cout << "before: X=" << X << " Y=" << Y << endl;
    clean(X);
    clean(Y);

    return 0;
}
