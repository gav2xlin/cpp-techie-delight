cmake_minimum_required(VERSION 3.5)

project(sorted_merge_386 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(sorted_merge_386 main.cpp)

install(TARGETS sorted_merge_386
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
