/*

Given a singly-linked list of integers, reverse it. The swapping of data is not allowed, only links should be changed.

Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> nullptr
Output: 6 —> 5 —> 4 —> 3 —> 2 —> 1 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void reverseList(Node* &head)
    {
        Node* previous = nullptr;
        Node* current = head;

        while (current != nullptr)
        {
            Node* next = current->next;

            current->next = previous;

            previous = current;
            current = next;
        }

        head = previous;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os <<node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* head = construct({1, 2, 3, 4, 5, 6});
    cout << "before: " << head << endl;

    Solution().reverseList(head);

    cout << "after: " << head << endl;
    clean(head);

    return 0;
}
