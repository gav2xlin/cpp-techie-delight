/*

Given a singly-linked list containing 0’s, 1’s, and 2’s, sort the linked list by doing a single traversal of it.

Input : 0 —> 1 —> 2 —> 2 —> 1 —> 0 —> 0 —> 2 —> 0 —> 1 —> 1 —> 0 —> nullptr
Output: 0 —> 0 —> 0 —> 0 —> 0 —> 1 —> 1 —> 1 —> 1 —> 2 —> 2 —> 2 —> nullptr

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void sort(Node* &head)
    {
        if (head == nullptr || head->next == nullptr) {
            return;
        }

        Node dummyZero, dummyOne, dummyTwo;
        dummyZero.next = dummyOne.next = dummyTwo.next = nullptr;

        Node* zero = &dummyZero, *one = &dummyOne, *two = &dummyTwo;
        Node* curr = head;

        while (curr != nullptr)
        {
            if (curr->data == 0) {
                zero->next = curr;
                zero = zero->next;
            } else if (curr->data == 1) {
                one->next = curr;
                one = one->next;
            } else {
                two->next = curr;
                two = two->next;
            }
            curr = curr->next;
        }

        zero->next = dummyOne.next ? dummyOne.next: dummyTwo.next;
        one->next = dummyTwo.next;
        two->next = nullptr;

        head = dummyZero.next;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node n12{0};
    Node n11{1, &n12};
    Node n10{1, &n11};
    Node n9{0, &n10};
    Node n8{2, &n9};
    Node n7{0, &n8};
    Node n6{0, &n7};
    Node n5{1, &n6};
    Node n4{2, &n5};
    Node n3{2, &n4};
    Node n2{1, &n3};
    Node n1{0, &n2};

    /*
    Input : 0 —> 1 —> 2 —> 2 —> 1 —> 0 —> 0 —> 2 —> 0 —> 1 —> 1 —> 0 —> nullptr
    Output: 0 —> 0 —> 0 —> 0 —> 0 —> 1 —> 1 —> 1 —> 1 —> 2 —> 2 —> 2 —> nullptr
     */
    Node *head = &n1;
    cout << "before: " << head << endl;

    Solution().sort(head);

    cout << "after: " << head << endl;

    return 0;
}
