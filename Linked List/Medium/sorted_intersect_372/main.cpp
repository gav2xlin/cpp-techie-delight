/*

Given two sorted singly-linked lists of integers, return a new list representing their intersection.

Input:

X: 1 —> 4 —> 7 —> 10 —> nullptr
Y: 2 —> 4 —> 6 —> 8 —> 10 —> nullptr

Output: 4 —> 10 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void push(Node* &head, int data)
    {
        Node* newNode = new Node{data, head};
        head = newNode;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* sortedIntersect(Node* X, Node* Y)
    {
        Node dummy{-1, nullptr};
        Node* tail = &dummy;

        while (X != nullptr && Y != nullptr)
        {
            if (X->data == Y->data)
            {
                push(tail->next, X->data);
                tail = tail->next;

                X = X->next;
                Y = Y->next;
            } else if (X->data < Y->data) {
                X = X->next;
            } else {
                Y = Y->next;
            }
        }

        return dummy.next;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main() {
    Node* X = construct({1, 4, 7, 10});
    Node* Y = construct({2, 4, 6, 8, 10});
    cout << "before: X=" << X << " Y=" << Y << endl;

    Node* Z = Solution().sortedIntersect(X, Y);

    cout << "after: " << Z << endl;
    clean(X);
    clean(Y);
    clean(Z);

    return 0;
}
