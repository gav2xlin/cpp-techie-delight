/*

Given a singly-linked list and a positive number k, reverse every alternate group of k nodes. The swapping of data is not allowed, only links should be changed.


Input:

Linked List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr
k = 2

Output: 2 —> 1 —> 3 —> 4 —> 6 —> 5 —> 7 —> 8 —> 10 —> 9 —> nullptr


Input:

Linked List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr
k = 3

Output: 3 —> 2 —> 1 —> 4 —> 5 —> 6 —> 9 —> 8 —> 7 —> 10 —> nullptr


Input:

Linked List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr
k >= 10

Output: 10 —> 9 —> 8 —> 7 —> 6 —> 5 —> 4 —> 3 —> 2 —> 1 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    Node* _reverseAlternatingKNodes(Node* head, int k)
    {
        if (head == nullptr) {
            return nullptr;
        }

        Node* originalHead = head;

        Node* curr = head;
        head = reverse(curr, k);

        originalHead->next = curr;

        skipKNodes(curr, k - 1);

        if (curr != nullptr) {
            curr->next = _reverseAlternatingKNodes(curr->next, k);
        }

        return head;
    }

    void skipKNodes(Node* &curr, int k)
    {
        while (curr && k--) {
            curr = curr->next;
        }
    }

    Node* reverse(Node* &curr, int k)
    {
        Node* prev = nullptr;

        while (curr && k--)
        {
            Node* next = curr->next;

            curr->next = prev;


            prev = curr;
            curr = next;
        }

        return prev;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void reverseAlternatingKNodes(Node* &head, int k)
    {
        head = _reverseAlternatingKNodes(head, k);
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os <<node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        /*
        Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr, k = 2
        Output: 2 —> 1 —> 3 —> 4 —> 6 —> 5 —> 7 —> 8 —> 10 —> 9 —> nullptr
        */
        Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        cout << "before: " << head << endl;

        Solution().reverseAlternatingKNodes(head, 2);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        /*
        Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr, k = 3
        Output: 3 —> 2 —> 1 —> 4 —> 5 —> 6 —> 9 —> 8 —> 7 —> 10 —> nullptr
        */
        Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        cout << "before: " << head << endl;

        Solution().reverseAlternatingKNodes(head, 3);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        /*
        Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr, k >= 10
        Output: 10 —> 9 —> 8 —> 7 —> 6 —> 5 —> 4 —> 3 —> 2 —> 1 —> nullptr
        */
        Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        cout << "before: " << head << endl;

        Solution().reverseAlternatingKNodes(head, 10);

        cout << "after: " << head << endl;
        clean(head);
    }

    return 0;
}
