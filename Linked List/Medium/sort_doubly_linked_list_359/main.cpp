/*

Given a doubly-linked list of integers, rearrange its nodes to be sorted in increasing order.

Input : 6 ⇔ 3 ⇔ 4 ⇔ 8 ⇔ 2 ⇔ 9 ⇔ nullptr
Output: 2 ⇔ 3 ⇔ 4 ⇔ 6 ⇔ 8 ⇔ 9 ⇔ nullptr

Input : 9 ⇔ -3 ⇔ 5 ⇔ -2 ⇔ -8 ⇔ -6 ⇔ nullptr
Output: -8 ⇔ -6 ⇔ -3 ⇔ -2 ⇔ 5 ⇔ 9 ⇔ nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* prev = nullptr; 	// pointer to the previous node
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *prev, Node *next): data(data), prev(prev), next(next) {}
};

class Solution
{
private:
    void split(struct Node* head, struct Node** a, struct Node** b) {
        Node* slow = head;
        Node* fast = head->next;

        while (fast != nullptr) {
            fast = fast->next;
            if (fast != nullptr) {
                slow = slow->next;
                fast = fast->next;
            }
        }

        *b = slow->next;
        slow->next = nullptr;
    }

    struct Node* merge(struct Node* a, struct Node* b) {
        if (a == nullptr) {
            return b;
        }

        if (b == nullptr) {
            return a;
        }

        if (a->data <= b->data) {
            a->next = merge(a->next, b);
            a->next->prev = a;
            a->prev = nullptr;
            return a;
        } else {
            b->next = merge(a, b->next);
            b->next->prev = b;
            b->prev = nullptr;
            return b;
        }
    }

    void mergesort(struct Node** head)
    {
        if (*head == nullptr || (*head)->next == nullptr) {
            return;
        }

        Node* a = *head, *b = nullptr;
        split(*head, &a, &b);

        mergesort(&a);
        mergesort(&b);

        *head = merge(a, b);
    }
public:

    /*
        A doubly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* prev = nullptr; 	// pointer to the previous node
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *prev, Node *next): data(data), prev(prev), next(next) {}
        };
    */

    void sort(Node* &head)
    {
        mergesort(&head);
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], nullptr, next};
        if (next != nullptr) next->prev = head;
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        Node* head = construct({6, 3, 4, 8, 2, 9});
        cout << "before: " << head << endl;
        Solution().sort(head);
        cout << "after: " << head << endl;
        clean(head);
    }

    {
        Node* head = construct({9, -3, 5, -2, -8, -6});
        cout << "before: " << head << endl;
        Solution().sort(head);
        cout << "after: " << head << endl;
        clean(head);
    }

    return 0;
}
