/*

Given a singly-linked list of integers, rearrange its nodes to be sorted in increasing order.

Input : 6 —> 3 —> 4 —> 8 —> 2 —> 9 —> nullptr
Output: 2 —> 3 —> 4 —> 6 —> 8 —> 9 —> nullptr

Input : 9 —> -3 —> 5 —> -2 —> -8 —> -6 —> nullptr
Output: -8 —> -6 —> -3 —> -2 —> 5 —> 9 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data{};				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void sortedInsert(struct Node** head, struct Node* newNode) {
        Node dummy;
        Node* current = &dummy;
        dummy.next = *head;

        while (current->next != nullptr && current->next->data < newNode->data) {
            current = current->next;
        }

        newNode->next = current->next;
        current->next = newNode;
        *head = dummy.next;
    }

    void insertSort(struct Node** head) {
        Node* result = nullptr;
        Node* current = *head;
        Node* next;

        while (current != nullptr) {
            next = current->next;

            sortedInsert(&result, current);
            current = next;
        }

        *head = result;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void sort(Node* &head)
    {
        insertSort(&head);
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        Node* head = construct({6, 3, 4, 8, 2, 9});
        cout << "before: " << head << endl;
        Solution().sort(head);
        cout << "after: " << head << endl;
        clean(head);
    }

    {
        Node* head = construct({9, -3, 5, -2, -8, -6});
        cout << "before: " << head << endl;
        Solution().sort(head);
        cout << "after: " << head << endl;
        clean(head);
    }

    return 0;
}
