/*

Given a singly-linked list of integers, rearrange it by separating odd nodes from even ones. The solution should return a list containing all even nodes followed by all odd nodes, where the relative order of even and odd nodes is maintained.

Input : 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> nullptr
Output: 2 —> 4 —> 6 —> 1 —> 3 —> 5 —> 7 —> nullptr

Input : 2 —> 4 —> 6 —> nullptr
Output: 2 —> 4 —> 6 —> nullptr

Input : 1 —> 3 —> 5 —> 7 —> nullptr
Output: 1 —> 3 —> 5 —> 7 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void rearrange(Node* &head)
    {
        Node odd, even;
        Node* oddTail = &odd, *evenTail = &even;

        Node* curr = head;

        while (curr != nullptr)
        {
            if (curr->data & 1)
            {
                oddTail->next = curr;
                oddTail = oddTail->next;
            } else {
                evenTail->next = curr;
                evenTail = curr;
            }
            curr = curr->next;
        }

        evenTail->next = odd.next;
        oddTail->next = nullptr;
        head = even.next;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        Node* head = construct({1, 2, 3, 4, 5, 6, 7});
        cout << "before: " << head << endl;

        Solution().rearrange(head);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        Node* head = construct({2, 4, 6});
        cout << "before: " << head << endl;

        Solution().rearrange(head);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        Node* head = construct({1, 3, 5, 7});
        cout << "before: " << head << endl;

        Solution().rearrange(head);

        cout << "after: " << head << endl;
        clean(head);
    }

    return 0;
}
