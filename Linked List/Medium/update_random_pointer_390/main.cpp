/*

Given a singly-linked list of integers with each node having an additional random pointer, update the random pointer of each node to point to a maximum value node to its right.

Input : 5(X) —⮞ 10(X) —⮞ 7(X) —⮞ 9(X) —⮞ 4(X) —⮞ 3(X) —⮞ X
Output: 5(10) —⮞ 10(9) —⮞ 7(9) —⮞ 9(4) —⮞ 4(3) —⮞ 3(X) —⮞ X

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* next = nullptr; 		// pointer to the next node
    Node* random = nullptr;		// pointer to the random node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next, Node *random): data(data), next(next), random(random) {}
};

class Solution
{
private:
    Node* setRandomNodes(Node* head)
    {
        if (head == nullptr) {
            return nullptr;
        }

        if (head->next == nullptr)
        {
            head->random = nullptr;
            return head;
        }

        Node* max = setRandomNodes(head->next);

        head->random = max;

        return (max->data > head->data) ? max: head;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* next = nullptr; 		// pointer to the next node
            Node* random = nullptr;		// pointer to the random node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next, Node *random): data(data), next(next), random(random) {}
        };
    */

    void updateRandomPointer(Node* head)
    {
        setRandomNodes(head);
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next, nullptr};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }
    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << '(';
            if (node->random != nullptr) {
                os << node->random->data;
            } else {
                os << 'X';
            }
            os << ")->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* head = construct({5, 10, 7, 9, 4, 3});
    cout << "before: " << head << endl;

    Solution().updateRandomPointer(head);

    cout << "after: " << head << endl;
    clean(head);

    return 0;
}
