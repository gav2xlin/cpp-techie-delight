/*

Given a singly-linked list that stores a path formed by cells of a matrix, remove the redundant nodes in that path. The path can be both vertical and horizontal, but never diagonal. To determine the complete path, you need the endpoints of all vertical and horizontal paths; middle nodes don't provide any value and are therefore redundant. So, the resultant list should contain coordinates of only endpoints of all vertical and horizontal paths.

Input : (0, 1) → (0, 5) → (0, 8)
                            ↓
                          (2, 8)
                            ↓
                          (5, 8)
                            ↓
                          (7, 8) → (7, 10) → (7, 12) → nullptr

Output: (0, 1) → (0, 8)
                   ↓
                 (7, 8) → (7, 12) → nullptr

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Node
{
public:
    int x;					// x-coordinate
    int y;					// y-coordinate
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int x, int y): x(x), y(y) {}
    Node(int x, int y, Node *next): x(x), y(y), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int x;					// x-coordinate
            int y;					// y-coordinate
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int x, int y): x(x), y(y) {}
            Node(int x, int y, Node *next): x(x), y(y), next(next) {}
        };
    */

    void removeNodes(Node* &head)
    {
        Node *node = head;
        while (node != nullptr && node->next != nullptr && node->next->next != nullptr) {
            Node* tmp = node->next->next;
            if ((node->x == node->next->x && node->x == tmp->x) || (node->y == node->next->y && node->y == tmp->y)) {
                delete node->next;
                node->next = tmp;
            } else {
                node = node->next;
            }
        }
    }
};

Node* construct(vector<pair<int, int>> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i].first, keys[i].second, next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << '(' << node->x << ',' << node->y << ")->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* head = construct({{0, 1}, {0, 5}, {0, 8}, {2, 8}, {5, 8}, {7, 8}, {7, 10}, {7, 12}});

    cout << "before:\n" << head << endl;

    Solution().removeNodes(head);

    cout << "after:\n" << head << endl;

    clean(head);

    return 0;
}
