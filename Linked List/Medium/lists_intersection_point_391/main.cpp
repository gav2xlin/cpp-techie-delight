/*

Given two non-empty singly-linked lists, where the tail of the second list points to a node in the first list, return the node where both lists intersect. If the lists does not intersect, return nullptr.

Input:

1 ——⮞ 2 ——⮞ 3 ——⮞ 4 ——⮞ 5 ——⮞ nullptr		// first linked list
                   ⮝
1 ——⮞ 2 ——⮞ 3 ────┘							// second linked list

Output: Node 4

Explanation: The tail of the second list is connected to the fourth node of the first list. The solution should return a pointer to node 4 as the intersection point.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    // Floyd’s cycle detection algorithm
    Node* identifyCycle(Node* head)
    {
        Node *slow = head, *fast = head;

        while (fast != nullptr && fast->next != nullptr)
        {
            slow = slow->next;

            fast = fast->next->next;

            if (slow == fast) {
                return slow;
            }
        }

        return nullptr;
    }

    Node* removeCycle(Node* loopNode, Node* head)
    {
        int k = 1;
        for (Node* ptr = loopNode; ptr->next != loopNode; ptr = ptr->next) {
            ++k;
        }

        Node* curr = head;
        for (int i = 0; i < k; ++i) {
            curr = curr->next;
        }

        while (curr != head)
        {
            curr = curr->next;
            head = head->next;
        }

        return curr;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* findIntersectionPoint(Node* first, Node* second)
    {
        Node* prev = nullptr;
        Node* curr = first;

        while (curr != nullptr)
        {
            prev = curr;
            curr = curr->next;
        }

        if (prev != nullptr) {
            prev->next = first;
        }

        Node* slow = identifyCycle(second);

        Node* addr = nullptr;
        if (slow != nullptr) {
            addr = removeCycle(slow, second);
        }

        if (prev != nullptr) {
            prev->next = nullptr;
        }

        return addr;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }
    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* first = construct({1, 2, 3, 4, 5});
    Node* second = construct({1, 2, 3});
    second->next->next->next = first->next->next->next;

    Node* addr = Solution().findIntersectionPoint(first, second);
    cout << (addr ? addr : nullptr) << endl;

    second->next->next->next = nullptr;
    clean(first);
    clean(second);

    return 0;
}
