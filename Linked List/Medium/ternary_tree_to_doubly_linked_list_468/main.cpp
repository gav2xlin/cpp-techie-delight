/*

Given the root of a ternary tree, in-place convert the ternary tree into a doubly-linked list. A ternary tree is a tree data structure in which each node has three child nodes distinguished as left, mid, and right.

The conversion should be done such that the left child pointer of a ternary tree node should act as a previous pointer for the doubly linked list node, the right child pointer should serve as the next pointer for the doubly linked list node, and the mid-child pointer should point to nothing. The conversion should be done by only exchanging ternary tree node pointers without allocating any extra memory for the nodes of the doubly linked list.

The solution should push the root node before its children into the doubly linked list and each node should recursively follows this in the subtree rooted at its left child, mid-child, and right child, in that order.

Input:
              1
            / | \
          /   |   \
        /	  |		\
       2	  9		 12
     / | \   / \	 |  \
    3  6  8 10  11  13   16
    |   \		   /  \   |
    4	 7		  14  15  17
     \
      5

Output: 1 ⇔ 2 ⇔ 3 ⇔ 4 ⇔ 5 ⇔ 6 ⇔ 7 ⇔ 8 ⇔ 9 ⇔ 10 ⇔ 11 ⇔ 12 ⇔ 13 ⇔ 14 ⇔ 15 ⇔ 16 ⇔ 17 ⇔ nullptr

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child
    Node* mid = nullptr;		// pointer to the mid child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right, Node *mid): data(data), left(left), right(right), mid(mid) {}
};

class Solution
{
private:
    void push(Node* node, Node* &headRef)
    {
        headRef->left = node;
        node->right = headRef;

        node->left = node->mid = nullptr;

        headRef = node;
    }

    void ternaryTreeToDoublyLinkedList(Node* root, Node* &headRef)
    {
        if (root == nullptr) {
            return;
        }

        ternaryTreeToDoublyLinkedList(root->right, headRef);
        ternaryTreeToDoublyLinkedList(root->mid, headRef);
        ternaryTreeToDoublyLinkedList(root->left, headRef);

        if (headRef == nullptr) {
            headRef = root;
        }
        else {
            push(root, headRef);
        }
    }
public:

    /*
        A ternary tree node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child
            Node* mid = nullptr;		// pointer to the mid child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right, Node *mid): data(data), left(left), right(right), mid(mid) {}
        };
    */

    void ternaryTreeToDoublyLinkedList(Node* root)
    {
        Node* head = nullptr;
        ternaryTreeToDoublyLinkedList(root, head);
    }
};

void printDoublyLinkedList(Node* ptr)
{
    while (ptr != nullptr)
    {
        cout << ptr->data << " —> ";
        ptr = ptr->right;
    }
    cout << "nullptr" << endl;
}

int main()
{
    Node* root = new Node(1);

    root->left = new Node(2);
    root->mid = new Node(9);
    root->right = new Node(12);

    root->left->left = new Node(3);
    root->left->mid = new Node(6);
    root->left->right = new Node(8);

    root->mid->left = new Node(10);
    root->mid->right = new Node(11);

    root->right->mid = new Node(13);
    root->right->right = new Node(16);

    root->left->left->mid = new Node(4);
    root->left->left->mid->right = new Node(5);
    root->left->mid->right = new Node(7);

    root->right->mid->left = new Node(14);
    root->right->mid->right = new Node(15);
    root->right->right->mid = new Node(17);

    Solution().ternaryTreeToDoublyLinkedList(root);

    printDoublyLinkedList(root);

    return 0;
}
