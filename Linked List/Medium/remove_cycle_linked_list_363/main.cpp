/*

Given a singly-linked list of integers, which may contain a cycle, remove the cycle if present.

Input: 1 → 2 → 3 → 4 ─┐
           └──────────┘
Output: 1 → 2 → 3 → 4 → nullptr


Input : 1 → 2 → 3 → 4 → nullptr
Output: 1 → 2 → 3 → 4 → nullptr

*/

// https://en.wikipedia.org/wiki/Cycle_detection#Floyd's_tortoise_and_hare
// https://en.wikipedia.org/wiki/Cycle_detection#Brent's_algorithm
// https://en.wikipedia.org/wiki/Cycle_detection#Gosper's_algorithm

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    // Floyd’s cycle detection algorithm
    Node* identifyCycle(Node* head)
    {
        Node *slow = head, *fast = head;

        while (fast != nullptr && fast->next != nullptr)
        {
            slow = slow->next;

            fast = fast->next->next;

            if (slow == fast) {
                return slow;
            }
        }

        return nullptr;
    }

    void removeCycle(Node* slow, Node* head)
    {
        int k = 1;
        for (Node* ptr = slow; ptr->next != slow; ptr = ptr->next) {
            ++k;
        }

        Node* curr = head;
        for (int i = 0; i < k; ++i) {
            curr = curr->next;
        }

        while (curr != head)
        {
            curr = curr->next;
            head = head->next;
        }

        while (curr->next != head) {
            curr = curr->next;
        }

        curr->next = nullptr;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    /*void removeCycle(Node* &head) {
        Node* slow = identifyCycle(head);

        if (slow != nullptr)
        {
            removeCycle(slow, head);
        }
    }*/
    void removeCycle(Node* head)
    {
        Node* prev = nullptr;
        Node* curr = head;

        unordered_set<Node*> set;

        while (curr != nullptr)
        {
            if (set.count(curr))
            {
                prev->next = nullptr;
                return;
            }

            set.insert(curr);

            prev = curr;
            curr = curr->next;
        }
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }
    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    {
        Node* head = construct({1, 2, 3, 4});
        head->next->next->next->next = head->next;

        Solution().removeCycle(head);
        cout << head << endl;

        head->next->next->next->next = nullptr;
        clean(head);
    }

    {
        Node* head = construct({1, 2, 3, 4});

        Solution().removeCycle(head);
        cout << head << endl;

        clean(head);
    }

    return 0;
}
