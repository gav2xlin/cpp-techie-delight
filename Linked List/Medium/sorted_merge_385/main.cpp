/*

Given two sorted singly-linked lists of integers, merge them into a single list in decreasing order, and return it. In other words, merge two sorted linked lists from their end.

Input:

X: 1 —> 3 —> 5 —> nullptr
Y: 2 —> 6 —> 7 —> 10 —> nullptr

Output: 10 —> 7 —> 6 —> 5 —> 3 —> 2 —> 1 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void moveNode(Node* &destRef, Node* &sourceRef)
    {
        if (sourceRef == nullptr) {
            return;
        }

        Node* newNode = sourceRef;
        sourceRef = sourceRef->next;
        newNode->next = destRef;
        destRef = newNode;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* sortedMerge(Node* X, Node* Y)
    {
        Node* result = nullptr;

        while (X != nullptr && Y != nullptr)
        {
            if (X->data < Y->data) {
                moveNode(result, X);
            }
            else {
                moveNode(result, Y);
            }
        }

        while (Y != nullptr) {
            moveNode(result, Y);
        }

        while (X != nullptr) {
            moveNode(result, X);
        }

        return result;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* X = construct({1, 3, 5});
    Node* Y = construct({2, 6, 7, 10});
    cout << "before: X=" << X << " Y=" << Y << endl;

    Node* Z = Solution().sortedMerge(X, Y);

    cout << "after: " << Z << endl;
    clean(Z);

    return 0;
}
