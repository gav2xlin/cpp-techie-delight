/*

Given two sorted singly-linked lists of integers, merge them into a single list in increasing order, and return it.

Input:

X: 1 —> 3 —> 5 —> 7 —> nullptr
Y: 2 —> 4 —> 6 —> nullptr

Output: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void moveNode(Node* &destRef, Node* &sourceRef)
    {
        if (sourceRef == nullptr) {
            return;
        }

        Node* newNode = sourceRef;
        sourceRef = sourceRef->next;
        newNode->next = destRef;
        destRef = newNode;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* sortedMerge(Node* X, Node* Y)
    {
        Node dummy{-1, nullptr};

        Node* tail = &dummy;

        while (true)
        {
            if (X == nullptr)
            {
                tail->next = Y;
                break;
            } else if (Y == nullptr) {
                tail->next = X;
                break;
            }

            if (X->data <= Y->data) {
                moveNode(tail->next, X);
            } else {
                moveNode(tail->next, Y);
            }

            tail = tail->next;
        }

        return dummy.next;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* X = construct({1, 3, 5, 7});
    Node* Y = construct({2, 4, 6});
    cout << "before: X=" << X << " Y=" << Y << endl;

    Node* Z = Solution().sortedMerge(X, Y);

    cout << "after: " << Z << endl;
    clean(Z);

    return 0;
}
