/*

Given a singly-linked list of integers, pairwise swap its adjacent nodes. The swapping of data is not allowed, only links should be changed.

Input : 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> nullptr
Output: 2 —> 1 —> 4 —> 3 —> 6 —> 5 —> 8 —> 7 —> nullptr

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void rearrange(Node* &head)
    {
        if (head == nullptr || head->next == nullptr) {
            return;
        }

        Node* curr = head, *prev = nullptr;

        while (curr != nullptr && curr->next != nullptr)
        {
            Node* tmp = curr->next;
            curr->next = tmp->next;
            tmp->next = curr;

            if (prev == nullptr) {
                head = tmp;
            } else {
                prev->next = tmp;
            }

            prev = curr;
            curr = curr->next;
        }
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8});
    cout << "before: " << head << endl;

    Solution().rearrange(head);

    cout << "after: " << head << endl;
    clean(head);

    return 0;
}
