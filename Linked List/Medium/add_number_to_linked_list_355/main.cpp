/*

Given a single-digit number k and a singly-linked list whose nodes stores digits of a non-negative number, add k to the linked list.

Input: List = 9 —> 9 —> 9 —> 3 —> nullptr, k = 7
Output: 1 —> 0 —> 0 —> 0 —> 0 —> nullptr
Explanation: The input linked list represents the number 9993. Adding a single-digit number 7 results in linked list corresponding to the number 10000.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void reverse(Node* &head) {
        if (head == nullptr || head->next == nullptr) return;

        Node *cur = head, *prev = nullptr, *next;
        while (cur != nullptr) {
            Node* next = cur->next;
            cur->next = prev;

            prev = cur;
            cur = next;
        }
        head = prev;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void addDigit(Node* &head, int k)
    {
        if (head != nullptr) {
            reverse(head);

            int carry = k;
            Node *cur = head;

            while (carry) {
                int sum = cur->data + carry;
                cur->data = sum % 10;
                carry = sum / 10;

                if (cur->next == nullptr) break;
                cur = cur->next;
            }

            if (carry) {
                cur->next = new Node{carry};
            }

            reverse(head);
        } else {
            head = new Node{k};
        }
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }
    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* head = construct({9, 9, 9, 3});

    cout << "before: " << head << endl;

    Solution().addDigit(head, 7);

    cout << "after: " << head << endl;

    clean(head);

    return 0;
}
