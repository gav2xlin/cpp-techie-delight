/*

Given two singly-linked lists of integers, merge their nodes into the first list by taking nodes alternately between the two lists. If the first list runs out, the remaining nodes of the second list should not be moved.

Input:

X: 1 —> 2 —> 3 —> nullptr
Y: 4 —> 5 —> 6 —> 7 —> 8 —> nullptr

Output:

X: 1 —> 4 —> 2 —> 5 —> 3 —> 6 —> nullptr
Y: 7 —> 8 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void shuffleMerge(Node* &X, Node* &Y)
    {
        Node dummy;
        Node* tail = &dummy;
        dummy.next = nullptr;

        while (true)
        {
            if (X == nullptr)
            {
                tail->next = nullptr;
                break;
            } else if (Y == nullptr) {
                tail->next = X;
                break;
            } else {
                tail->next = X;
                tail = X;
                X = X->next;

                tail->next = Y;
                tail = Y;
                Y = Y->next;
            }
        }

        X = dummy.next;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* X = construct({1, 2, 3});
    Node* Y = construct({4, 5, 6, 7, 8});
    cout << "before: X=" << X << " Y=" << Y << endl;

    Solution().shuffleMerge(X, Y);

    cout << "after: X=" << X << " Y=" << Y << endl;
    clean(X);
    clean(Y);

    return 0;
}
