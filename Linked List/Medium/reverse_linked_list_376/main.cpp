/*

Given a singly-linked list and a positive number k, reverse every adjacent group of k nodes. The swapping of data is not allowed, only links should be changed.

Input:

Linked List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> nullptr
k = 3

Output: 3 —> 2 —> 1 —> 6 —> 5 —> 4 —> 8 —> 7 —> nullptr


Input:

Linked List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> nullptr
k = 2

Output: 2 —> 1 —> 4 —> 3 —> 6 —> 5 —> 8 —> 7 —> nullptr


Input:

Linked List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> nullptr
k >= 8

Output: 8 —> 7 —> 6 —> 5 —> 4 —> 3 —> 2 —> 1 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    Node* reverseK(Node* &current, int k)
    {
        Node* prev = nullptr;
        int count = 0;

        while (current != nullptr && count++ < k)
        {
            Node* next = current->next;

            current->next = prev;

            prev = current;
            current = next;
        }

        return prev;
    }

    void reverseInGroups(Node* &head, int k)
    {
        if (head == nullptr) {
            return;
        }

        Node* current = head;

        Node* prev = reverseK(current, k);

        reverseInGroups(current, k);

        head->next = current;
        head = prev;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void reverse(Node* &head, int k)
    {
        reverseInGroups(head, k);
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* &head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os <<node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{

    {
        /*
        Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> nullptr, k = 3
        Output: 3 —> 2 —> 1 —> 6 —> 5 —> 4 —> 8 —> 7 —> nullptr
        */
        Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8});
        cout << "before: " << head << endl;

        Solution().reverse(head, 3);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        /*
        Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> nullptr, k = 2
        Output: 2 —> 1 —> 4 —> 3 —> 6 —> 5 —> 8 —> 7 —> nullptr
        */
        Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8});
        cout << "before: " << head << endl;

        Solution().reverse(head, 2);

        cout << "after: " << head << endl;
        clean(head);
    }

    {
        /*
        Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> nullptr, k >= 8
        Output: 8 —> 7 —> 6 —> 5 —> 4 —> 3 —> 2 —> 1 —> nullptr
        */
        Node* head = construct({1, 2, 3, 4, 5, 6, 7, 8});
        cout << "before: " << head << endl;

        Solution().reverse(head, 8);

        cout << "after: " << head << endl;
        clean(head);
    }

    return 0;
}
