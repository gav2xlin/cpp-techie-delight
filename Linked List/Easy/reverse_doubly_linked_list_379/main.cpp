/*

Given a doubly-linked list of integers, reverse it. The swapping of data is not allowed, only links should be changed.

Input : 1 ⇔ 2 ⇔ 3 ⇔ 4 ⇔ 5 ⇔ nullptr
Output: 5 ⇔ 4 ⇔ 3 ⇔ 2 ⇔ 1 ⇔ nullptr

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* prev = nullptr; 	// pointer to the previous node
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *prev, Node *next): data(data), prev(prev), next(next) {}
};

class Solution
{
public:

    /*
        A doubly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* prev = nullptr; 	// pointer to the previous node
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *prev, Node *next): data(data), prev(prev), next(next) {}
        };
    */

    void reverse(Node* &head)
    {
        if (head == nullptr) return;

        Node *node = head;
        while (node != nullptr) {
            head = node;

            Node* tmp = node;
            node = node->next;
            swap(tmp->prev, tmp->next);
        }
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], nullptr, next};
        if (next != nullptr) next->prev = head;
        next = head;
    }

    return head;
}

int main()
{
    Node* head = construct({1, 2, 3, 4, 5});
    Solution().reverse(head);
    cout << head << endl;

    return 0;
}
