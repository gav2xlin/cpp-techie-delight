/*

Given a singly-linked list of integers and two positive numbers, m and n, delete every n nodes after skipping m nodes.

Input: List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr, m = 1, n = 3
Output: 1 —> 5 —> 9 —> nullptr

Input: List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> 7 —> 8 —> 9 —> 10 —> nullptr, m = 2, n = 2
Output: 1 —> 2 —> 5 —> 6 —> 9 —> 10 —> nullptr

Input: List: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> nullptr, m = 4, n = 6
Output: 1 —> 2 —> 3 —> 4 —> nullptr

Input: List: 1 —> 2 —> 3 —> nullptr, m = 4, n = 2
Output: 1 —> 2 —> 3 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void deleteNodes(Node* &head, int m, int n)
    {
        if (head == nullptr || m < 0 || n <= 0) return;

        Node* thead = head;
        while (true) {
            int tm = m, tn = n;

            Node *prev{nullptr}, *cur = thead;
            if (tm > 0) {
                do {
                    prev = cur;
                    cur = cur->next;
                } while (cur != nullptr && --tm > 0);
            }

            if (cur == nullptr) return;

            cur = prev != nullptr ? prev->next : head;
            do {
                Node* tmp = cur;
                cur = cur->next;
                delete tmp;

                if (prev != nullptr) prev->next = cur;
            } while (cur != nullptr && --tn > 0);

            if (cur == nullptr) break;
            thead = cur;
        }

        if (m == 0) head = nullptr;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    Node* list1 = construct({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    cout << "before: " << list1 << endl;
    Solution().deleteNodes(list1, 1, 3);
    cout << "after(" << 1 << ',' << 3 << "): " << list1 << endl;

    Node* list2 = construct({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    cout << "before: " << list2 << endl;
    Solution().deleteNodes(list2, 2, 2);
    cout << "after(" << 2 << ',' << 2 << "): " << list2 << endl;

    Node* list3 = construct({1, 2, 3, 4, 5, 6});
    cout << "before: " << list3 << endl;
    Solution().deleteNodes(list3, 4, 6);
    cout << "after(" << 4 << ',' << 6 << "): " << list3 << endl;

    Node* list4 = construct({1, 2, 3});
    cout << "before: " << list4 << endl;
    Solution().deleteNodes(list4, 4, 2);
    cout << "after(" << 4 << ',' << 2 << "): " << list4 << endl;

    Node* list5 = construct({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    cout << "before: " << list5 << endl;
    Solution().deleteNodes(list5, 2, 2);
    cout << "after(" << 2 << ',' << 2 << "): " << list5 << endl;

    Node* list6 = construct({1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    cout << "before: " << list6 << endl;
    Solution().deleteNodes(list6, 0, 2);
    cout << "after(" << 0 << ',' << 2 << "): " << list6 << endl;

    return 0;
}
