/*

Given an array of integers, construct a linked list out of the array keys. The solution should create a new node for every key and efficiently insert it onto the list's tail.

Input : [1, 2, 3, 4, 5]
Output: 1 —> 2 —> 3 —> 4 —> 5 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* construct(vector<int> const &keys)
    {
        Node *head{nullptr}, *prev{nullptr};

        for (auto key : keys) {
            Node* node = new Node{key};

            if (head == nullptr) {
                head = node;
            }
            if (prev != nullptr) {
                prev->next = node;
            }

            prev = node;
        }

        return head;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* head = Solution().construct({1, 2, 3, 4, 5});
    cout << head << endl;

    return 0;
}
