cmake_minimum_required(VERSION 3.5)

project(kth_node_from_end_352 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(kth_node_from_end_352 main.cpp)

install(TARGETS kth_node_from_end_352
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
