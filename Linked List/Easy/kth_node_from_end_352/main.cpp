/*

Given a non-empty linked list and a positive integer k, return the value of k'th node from the end of the list.

Input: List = 1 —> 2 —> 3 —> 4 —> 5 —> nullptr, k = 3
Output: 3

Assume that k is less than or equal to number of nodes in the linked list.

*/

#include <iostream>
#include <vector>
#include <stdexcept>
#include <sstream>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    int findLength(Node* head) {
        Node* node = head;
        int length = 0;
        while (node != nullptr) {
            ++length;
            node = node->next;
        }
        return length;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    int findKthNode(Node* head, int k)
    {
        if (head == nullptr) {
            throw invalid_argument("head is nullptr");
        }
        if (k < 0) {
            throw range_error("negative index");
        }

        int length = findLength(head);
        if (k > length) {
            stringstream ss;
            ss << k << " is out of range" << length;
            throw range_error(ss.str());
        }

        Node* node = head;
        int p = length - k;
        while (node != nullptr) {
            if (p == 0) return node->data;
            --p;

            node = node->next;
        }

        return 0; // never executed
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    {
        Node* head = construct({2, 4, 6, 8});
        cout << Solution().findKthNode(head, 2) << endl;
    }

    {
        Node* head = construct({1, 2, 3, 4, 5});
        cout << Solution().findKthNode(head, 3) << endl;
    }

    return 0;
}
