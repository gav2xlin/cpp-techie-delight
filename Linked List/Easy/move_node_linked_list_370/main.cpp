/*

Given two singly-linked lists of integers, move front node of the second list in front of the first list.

Input:

X: 1 —> 2 —> 3 —> nullptr
Y: 4 —> 5 —> 6 —> nullptr

Output:

X: 4 —> 1 —> 2 —> 3 —> nullptr
Y: 5 —> 6 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void moveNode(Node* &X, Node* &Y)
    {
        if (Y == nullptr) return;

        Node* nextY = Y->next;
        Y->next = X;
        X = Y;
        Y = nextY;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    Node* x = construct({1, 2, 3});
    Node* y = construct({4, 5, 6});

    Solution().moveNode(x, y);

    cout << x << endl;
    cout << y << endl;

    return 0;
}
