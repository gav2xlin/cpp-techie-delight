/*

Given a singly-linked list of integers, split it into two lists where each list contains alternating elements from the list. The solution should join the two lists back together and return it.

Input : 1 —> 2 —> 3 —> 4 —> nullptr
Output: 1 —> 3 —> 2 —> 4 —> nullptr

Input : 1 —> 2 —> 3 —> 4 —> 5 —> nullptr
Output: 1 —> 3 —> 5 —> 2 —> 4 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void rearrange(Node* head)
    {
        if (head == nullptr || head->next == nullptr) return;

        Node *node = head, *half = head->next;

        Node *first = nullptr, *second = nullptr;
        while (node != nullptr) {
            Node* tmp = node;
            node = node->next;

            if (first != nullptr) {
                first->next = tmp;
            }
            first = tmp;

            if (node == nullptr) break;

            if (second != nullptr) {
                second->next = node;
            }
            second = node;

            node = node->next;
        }

        first->next = half;
        second->next = nullptr;
    }
};

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

ostream& operator<<(ostream& os, Node* node) {
    while (node != nullptr) {
        os << node->data << ' ';
        node = node->next;
    }
    return os;
}

int main()
{
    Node* head = construct({1, 2, 3, 4, 5});
    cout << head << endl;
    Solution().rearrange(head);
    cout << head << endl;

    return 0;
}
