/*

Given two singly-linked lists of integers, merge their nodes to make one list, taking nodes alternately between the two lists.

Input: 1 —> 2 —> 3 —> nullptr, 4 —> 5 —> 6 —> nullptr
Output: 1 —> 4 —> 2 —> 5 —> 3 —> 6 —> nullptr

If either list runs out, all the nodes should be taken from the other list.

Input: 1 —> 2 —> nullptr, 3 —> 4 —> 5 —> nullptr
Output: 1 —> 3 —> 2 —> 4 —> 5 —> nullptr

Input: 1 —> 2 —> 3 —> nullptr, 4 —> nullptr
Output: 1 —> 4 —> 2 —> 3 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* shuffleMerge(Node* X, Node* Y)
    {
        if (X == nullptr) return Y;
        if (Y == nullptr) return X;

        Node *curX = X, *curY = Y, *prevX = nullptr, *prevY = nullptr;

        while (curX != nullptr && curY != nullptr) {
            prevX = curX;
            prevY = curY;

            Node *tmpX = curX->next, *tmpY = curY->next;
            curX->next = curY;
            curY->next = tmpX;

            curX = tmpX;
            curY = tmpY;
        }

        if (prevY != nullptr) {
            if (curX != nullptr) prevY->next = curX;
            if (curY != nullptr) prevY->next = curY;
        }

        return X;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    // Input: 1 —> 2 —> 3 —> nullptr, 4 —> 5 —> 6 —> nullptr
    // Output: 1 —> 4 —> 2 —> 5 —> 3 —> 6 —> nullptr
    {
        Node* x = construct({1, 2, 3});
        Node* y = construct({4, 5, 6});
        Node* res = Solution().shuffleMerge(x, y);
        cout << res << endl;
    }

    // Input: 1 —> 2 —> nullptr, 3 —> 4 —> 5 —> nullptr
    // Output: 1 —> 3 —> 2 —> 4 —> 5 —> nullptr
    {
        Node* x = construct({1, 2});
        Node* y = construct({3, 4, 5});
        Node* res = Solution().shuffleMerge(x, y);
        cout << res << endl;
    }

    // Input: 1 —> 2 —> 3 —> nullptr, 4 —> nullptr
    // Output: 1 —> 4 —> 2 —> 3 —> nullptr
    {
        Node* x = construct({1, 2, 3});
        Node* y = construct({4});
        Node* res = Solution().shuffleMerge(x, y);
        cout << res << endl;
    }

    return 0;
}
