/*

Given a singly-linked list of integers, which may contain a cycle, detect and report the cycle if present.

Input: 1 → 2 → 3 → 4 ─┐
           └──────────┘
Output: true

Input: 1 → 2 → 3 → 4 → nullptr
Output: false

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    bool detectCycle(Node* head)
    {
        // two pointers
        if (head != nullptr && head->next != nullptr) {
            Node *turtle = head, *rabbit = head->next;
            while (turtle != nullptr && rabbit != nullptr) {
                if (turtle == rabbit || turtle == turtle->next || rabbit == rabbit->next) return true;

                if (rabbit->next == nullptr) break;

                turtle = turtle->next;
                rabbit = rabbit->next->next;
            }
        }
        return false;
    }
};

int main()
{
    {
        Node n4{4};
        Node n3{3, &n4};
        Node n2{2, &n3};
        Node n1{1, &n1};
        n4.next = &n2;
        cout << boolalpha << Solution().detectCycle(&n1) << endl;
    }
    {
        Node n4{4};
        Node n3{3, &n4};
        Node n2{2, &n3};
        Node n1{1, &n2};
        cout << boolalpha << Solution().detectCycle(&n1) << endl;
    }
    {
        Node n4{4};
        Node n3{3, &n4};
        Node n2{2, &n3};
        Node n1{1, &n2};
        n4.next = &n2;
        cout << boolalpha << Solution().detectCycle(&n1) << endl;
    }

    return 0;
}
