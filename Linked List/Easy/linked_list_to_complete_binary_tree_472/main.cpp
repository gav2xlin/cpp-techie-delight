/*

Given a singly-linked list of integers, construct a complete binary tree out of it. Assume that the order of elements present in the linked list is the same as that in the complete tree's array representation. i.e., for a tree node at position i (position starting from 1) in the linked list, the left child is present at the position 2×i, and the right child is present at the position 2×i + 1.

Input: 1 —> 2 —> 3 —> 4 —> 5 —> 6 —> None
Output:

            1
          /   \
         /	   \
        2		3
       / \	   /
      /	  \	  /
     4	   5 6

*/

#include <iostream>
//#include <vector>
#include <queue>

using namespace std;

class TreeNode
{
public:
    int data;						// data field
    TreeNode* left = nullptr;		// pointer to the left child
    TreeNode* right = nullptr;		// pointer to the right child

    TreeNode() {}
    TreeNode(int data): data(data) {}
    TreeNode(int data, TreeNode *left, TreeNode *right): data(data), left(left), right(right) {}
};

class ListNode
{
public:
    int data;						// data field
    ListNode* next = nullptr; 		// pointer to the next node

    ListNode() {}
    ListNode(int data): data(data) {}
    ListNode(int data, ListNode *next): data(data), next(next) {}
};

class Solution
{
/*private:
    int findLength(ListNode* node) {
        int length = 0;
        while (node != nullptr) {
            ++length;
            node = node->next;
        }
        return length;
    }*/
public:

    /*
        A binary tree node is defined as:

        class TreeNode
        {
        public:
            int data;						// data field
            TreeNode* left = nullptr;		// pointer to the left child
            TreeNode* right = nullptr;		// pointer to the right child

            TreeNode() {}
            TreeNode(int data): data(data) {}
            TreeNode(int data, TreeNode *left, TreeNode *right): data(data), left(left), right(right) {}
        };

        A singly-linked list node is defined as:

        class ListNode
        {
        public:
            int data;						// data field
            ListNode* next = nullptr; 		// pointer to the next node

            ListNode() {}
            ListNode(int data): data(data) {}
            ListNode(int data, ListNode *next): data(data), next(next) {}
        };
    */

    TreeNode* convertListToBinaryTree(ListNode* head)
    {
        /*if (head == nullptr) return nullptr;

        int length = findLength(head);
        if (length == 0) return nullptr;

        vector<TreeNode*> nodes(length);

        ListNode* node = head;
        for (int i = 0; i < length; ++i) {
            int l = 2 * i + 1;
            int r = 2 * i + 2;

            if (nodes[i] == nullptr) {
                nodes[i] = new TreeNode{};
            }

            if (l < length) {
                nodes[l] = new TreeNode{};
                int p = (l - 1) / 2; // i
                nodes[p]->left = nodes[l];
            }

            if (r < length) {
                nodes[r] = new TreeNode{};
                int p = (r - 1) / 2; // i
                nodes[p]->right = nodes[r];
            }

            nodes[i]->data = node->data;

            node = node->next;
        }

        return nodes[0];*/
        if (head == nullptr) return nullptr;

        ListNode* node = head;

        TreeNode* root = new TreeNode{node->data};
        node = node->next;

        queue<TreeNode*> q;
        q.push(root);

        while (node != nullptr) {
            TreeNode* parent = q.front();
            q.pop();

            TreeNode* left = new TreeNode{node->data};
            node = node->next;

            parent->left = left;
            q.push(left);

            if (node != nullptr) {
                TreeNode* right = new TreeNode{node->data};
                node = node->next;

                parent->right = right;
                q.push(right);
            }
        }

        return root;
    }
};

ListNode* construct(vector<int> const &keys)
{
    ListNode *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new ListNode{keys[i], next};
        next = head;
    }

    return head;
}

ostream& operator<<(ostream& os, const ListNode* head) {
    if (head != nullptr) {
        for (const ListNode* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

ostream& operator<<(ostream& os, const TreeNode* root) {
    auto print = [&os](const TreeNode* root, int level) -> void {
        auto print_impl = [&os](const TreeNode* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(root, 0);

    return os;
}

int main()
{
    ListNode* head = construct({1, 2, 3, 4, 5, 6});
    cout << head << endl;

    TreeNode* root = Solution().convertListToBinaryTree(head);
    cout << root << endl;

    return 0;
}
