/*

Given a singly-linked list of integers, delete its head node, and advance the head pointer to point at the next node in line.

Input : 1 -> 2 —> 3 —> 4 —> nullptr
Output: 2 —> 3 —> 4 —> nullptr

Input : 1 —> nullptr
Output: nullptr

Input : nullptr
Output: nullptr

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void pop(Node* &head)
    {
        if (head != nullptr) {
            Node* tmp = head;
            head = head->next;
            delete tmp;
        }
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    Node* n4 = new Node{4};
    Node* n3 = new Node{3, n4};
    Node* n2 = new Node{2, n3};
    Node* n1 = new Node{1, n2};

    Node* head = n1;

    cout << "before: " << head << endl;
    Solution().pop(head);
    cout << "after: " << head << endl;

    head = new Node{1};

    cout << "before: " << head << endl;
    Solution().pop(head);
    cout << "after: " << head << endl;

    head = nullptr;

    cout << "before: " << head << endl;
    Solution().pop(head);
    cout << "after: " << head << endl;

    return 0;
}
