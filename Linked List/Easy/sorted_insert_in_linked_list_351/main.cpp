/*

Given a sorted list in increasing order and a single node, insert the node into its correct sorted position in the list.

Input: List = 2 —> 4 —> 6 —> 8 —> nullptr, Node = 9
Output: 2 —> 4 —> 6 —> 8 —> 9 —> nullptr

Input: List = 2 —> 4 —> 6 —> 8 —> nullptr, Node = 1
Output: 1 —> 2 —> 4 —> 6 —> 8 —> nullptr

Input: List = 1 —> 2 —> 4 —> 6 —> 8 —> 9 —> nullptr, Node = 5
Output: 1 —> 2 —> 4 —> 5 —> 6 —> 8 —> 9 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void sortedInsert(Node* &head, Node* node)
    {
        Node *next = head, *prev = nullptr;
        while (next != nullptr) {
            if (next->data >= node->data) {
                break;
            }
            prev = next;
            next = next->next;
        }

        if (prev == nullptr) {
            node->next = head;
            head = node;
        } else {
            prev->next = node;
            node->next = next;
        }
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    Node* list1 = construct({2, 4, 6, 8});
    cout << "before: " << list1 << endl;
    Solution().sortedInsert(list1, new Node{9});
    cout << "after: " << list1 << endl;

    Node* list2 = construct({2, 4, 6, 8});
    cout << "before: " << list2 << endl;
    Solution().sortedInsert(list2, new Node{1});
    cout << "after: " << list2 << endl;

    Node* list3 = construct({1, 2, 4, 6, 8, 9});
    cout << "before: " << list3 << endl;
    Solution().sortedInsert(list3, new Node{5});
    cout << "after: " << list3 << endl;

    return 0;
}
