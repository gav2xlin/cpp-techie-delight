/*

Given a singly-linked list of integers, move its last node to the front.

Input : 1 —> 2 —> 3 —> 4 —> nullptr
Output: 4 —> 1 —> 2 —> 3 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void moveNode(Node* &head)
    {
        if (head == nullptr || head->next == nullptr) return;

        Node *cur = head, *prev1 = nullptr, *prev2 = nullptr;
        while (cur != nullptr) {
            prev2 = prev1;
            prev1 = cur;
            cur = cur->next;
        }

        prev1->next = head;
        prev2->next = nullptr;
        head = prev1;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    {
        Node* head = construct({1, 2, 3, 4});
        Solution().moveNode(head);
        cout << head << endl;
    }

    {
        Node* head = construct({1, 2});
        Solution().moveNode(head);
        cout << head << endl;
    }

    {
        Node* head = construct({1});
        Solution().moveNode(head);
        cout << head << endl;
    }

    {
        Node* head = nullptr;
        Solution().moveNode(head);
        cout << head << endl;
    }

    return 0;
}
