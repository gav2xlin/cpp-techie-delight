/*

Given an array of integers, implement a linked list out of the array keys. The solution should create a new node for every key and insert it onto the list's front.

Input : [1, 2, 3, 4, 5]
Output: 1 —> 2 —> 3 —> 4 —> 5 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* clone(Node* head)
    {
        Node *node = head, *c_prev = nullptr, *clone = nullptr;

        while (node != nullptr) {
            Node* c_node = new Node{node->data};
            if (clone == nullptr) {
                clone = c_node;
            }
            if (c_prev != nullptr) {
                c_prev->next = c_node;
            }
            c_prev = c_node;

            node = node->next;
        }

        return clone;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

void clean(Node* head) {
    if (head == nullptr) return;

    Node* node = head;
    while (node != nullptr) {
        Node* tmp = node;
        node = node->next;
        delete tmp;
    }

    head = nullptr;
}

int main()
{
    Node* head = construct({1, 2, 3, 4, 5});

    Node* clone = Solution().clone(head);
    cout << clone << endl;

    clean(head);
    clean(clone);

    return 0;
}
