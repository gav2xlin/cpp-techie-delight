/*

Given a singly-linked list of integers, split it into two sublists – one for the front half and one for the back half. The solution should return a vector containing the front half and the back half.

Input: 2 —> 3 —> 5 —> 7 —> nullptr
Output: [[2 —> 3 —> nullptr], [5 —> 7 —> nullptr]]

If the total number of elements in the list is odd, the extra element should go in the front list.

Input: 2 —> 3 —> 5 —> 7 —> 9 —> nullptr
Output: [[2 —> 3 —> 5 —> nullptr], [7 —> 9 —> nullptr]]

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    int findLength(Node* head) {
        Node* node = head;
        int length = 0;
        while (node != nullptr) {
            node = node->next;
            ++length;
        }
        return length;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    vector<Node*> frontBackSplit(Node* &head)
    {
        vector<Node*> result{head, head};

        int length = findLength(head);

        Node *node = head, *prev = nullptr;
        int half = (length / 2) + (length % 2);
        for (int i = 0; i < half; ++i) {
            prev = node;
            node = node->next;
        }

        if (prev != nullptr) {
            prev->next = nullptr;
        }
        result[1] = node;

        return result;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    {
        Node* list = construct({2, 3, 5, 7});
        vector<Node*> res = Solution().frontBackSplit(list);
        cout << res[0] << endl;
        cout << res[1] << endl;
    }

    {
        Node* list = construct({2, 3, 5, 7, 9});
        vector<Node*> res = Solution().frontBackSplit(list);
        cout << res[0] << endl;
        cout << res[1] << endl;
    }

    {
        Node* list = construct({2});
        vector<Node*> res = Solution().frontBackSplit(list);
        cout << res[0] << endl;
        cout << res[1] << endl;
    }

    {
        Node* list = construct({});
        vector<Node*> res = Solution().frontBackSplit(list);
        cout << res[0] << endl;
        cout << res[1] << endl;
    }

    {
        Node* list = nullptr;
        vector<Node*> res = Solution().frontBackSplit(list);
        cout << res[0] << endl;
        cout << res[1] << endl;
    }

    return 0;
}
