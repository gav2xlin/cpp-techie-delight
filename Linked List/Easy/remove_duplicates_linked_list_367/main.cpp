/*

Given an unsorted linked list of integers, remove duplicate nodes from it by traversing the list only once. The solution should preserve the order of elements appearing in the list.

Input: 5 —> 3 —> 4 —> 2 —> 5 —> 4 —> 1 —> 3 —> nullptr
Output: 5 —> 3 —> 4 —> 2 —> 1 —> nullptr

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void removeDuplicates(Node* &head)
    {
        unordered_set<int> visited;

        Node *node = head, *prev = nullptr;
        while (node != nullptr) {
            if (visited.find(node->data) == visited.end()) {
                visited.insert(node->data);
                prev = node;
                node = node->next;
            } else if (prev != nullptr) {
                prev->next = node->next;
                delete node;
                node = prev->next;
            }
        }
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    Node* head = construct({1, 2, 2, 2, 3, 4, 4, 5});
    Solution().removeDuplicates(head);
    cout << head << endl;

    return 0;
}
