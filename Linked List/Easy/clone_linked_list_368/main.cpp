/*

Given a singly linked list of integers, return a complete copy of it. The solution should return a new linked list which is identical to the given list in terms of its structure and contents, and it should not use any nodes of the list.

Input : 1 —> 2 —> 3 —> 4 —> 5 —> nullptr
Output: 1 —> 2 —> 3 —> 4 —> 5 —> nullptr

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    Node* clone(Node* head)
    {
        return nullptr;
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

int main()
{
    cout << "Hello World!" << endl;

    return 0;
}
