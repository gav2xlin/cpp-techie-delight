/*

Given a singly-linked list of integers sorted in increasing order, remove duplicates from it by traversing the list only once.

Input: 1 —> 2 —> 2 —> 2 —> 3 —> 4 —> 4 —> 5 —> nullptr
Output: 1 —> 2 —> 3 —> 4 —> 5 —> nullptr

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;				// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *next): data(data), next(next) {}
};

class Solution
{
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            int data;				// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *next): data(data), next(next) {}
        };
    */

    void removeDuplicates(Node* &head)
    {
        if (head == nullptr) return;

        Node *node = head, *prev = nullptr;
        while (node != nullptr) {
            prev = node;
            node = node->next;

            if (node != nullptr && prev->data == node->data) {
                prev->next = node->next;
                delete node;
                node = prev;
            }
        }
    }
};

ostream& operator<<(ostream& os, const Node* head) {
    if (head != nullptr) {
        for (const Node* node = head; node != nullptr; node = node->next) {
            os << node->data << "->";
        }
    }
    os << "nullptr";

    return os;
}

Node* construct(vector<int> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    Node* head = construct({1, 2, 2, 2, 3, 4, 4, 5});
    Solution().removeDuplicates(head);
    cout << head << endl;

    return 0;
}
