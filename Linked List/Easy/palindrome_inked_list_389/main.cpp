/*

Given a singly-linked list of strings, check whether the concatenation of all values in the list together forms a palindrome. It is not permissible to construct a string out of the linked list nodes and check that string for palindrome.

Input: "AA" —> "XYZ" —> "CD" —> "C" —> "ZYX" —> "AA" —> nullptr
Output: true
Explanation: The string "AAXYZCDCZYXAA" is palindrome

Input: "A" —> "B" —> "C" —> "DC" —> "B" —> nullptr
Output: false
Explanation: The string "ABCDCB" is not a palindrome


Note that Punctuation, capitalization, and spaces should not be ignored.

Input: "step on" —> "no pets" —> None
Output: true
Explanation: The string "step onno pets" is a palindrome

*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Node
{
public:
    string data;			// data field
    Node* next = nullptr; 	// pointer to the next node

    Node() {}
    Node(string data): data(data) {}
    Node(string data, Node *next): data(data), next(next) {}
};

class Solution
{
private:
    void concat(Node* head, string& a, string& b) {
        if (head == nullptr) return;

        string s = head->data;
        a += s;

        concat(head->next, a, b);

        reverse(s.begin(), s.end());
        b += s;
    }
public:

    /*
        A singly-linked list node is defined as:

        class Node
        {
        public:
            string data;			// data field
            Node* next = nullptr; 	// pointer to the next node

            Node() {}
            Node(string data): data(data) {}
            Node(string data, Node *next): data(data), next(next) {}
        };
    */

    bool isPalindrome(Node* head)
    {
        string a, b;
        concat(head, a, b);
        return a == b;
    }
};

Node* construct(vector<string> const &keys)
{
    Node *head{nullptr}, *next{nullptr};

    for (int i = keys.size() - 1; i >= 0; --i) {
        head = new Node{keys[i], next};
        next = head;
    }

    return head;
}

int main()
{
    {
        auto* node = construct({"AA", "XYZ", "CD", "C", "ZYX", "AA"});
        cout << boolalpha << Solution().isPalindrome(node) << endl;
    }

    {
        auto* node = construct({"A", "B", "C", "DC", "B"});
        cout << boolalpha << Solution().isPalindrome(node) << endl;
    }

    {
        auto* node = construct({"step on", "no pets"});
        cout << boolalpha << Solution().isPalindrome(node) << endl;
    }

    return 0;
}
