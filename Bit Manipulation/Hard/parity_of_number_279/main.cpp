/*

Given a number n, compute its parity. The parity is related to the total number of 1's in the binary number. The odd parity (encoded as 1) means an odd number of 1's and even parity (encoded as 0) means an even number of 1's.

The solution should return true if the number has odd parity and false if the number has even parity.

Input: 127
Output: true
Explanation: 127 is 01111111 in binary and contains odd number of bits.

Input: 17
Output: false
Explanation: 17 is 00010001 in binary and contains even number of bits.

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution
{
public:
    bool findParity(int x)
    {
        // recursively divide the (32–bit) integer into two equal
        // halves and take their XOR until only 1 bit is left

        x ^= x >> 16;
        x ^= x >> 8;
        x ^= x >> 4;
        x ^= x >> 2;
        x ^= x >> 1;

        // return 1 if the last bit is set; otherwise, return 0
        return x & 1;
    }

    /*#define P2(n) n, n^1, n^1, n
    #define P4(n) P2(n), P2(n^1), P2(n^1), P2(n)
    #define P6(n) P4(n), P4(n^1), P4(n^1), P4(n)
    #define FIND_PARITY P6(0), P6(1), P6(1), P6(0)

    // lookup table to store the parity of each index of the table.
    // The macro `FIND_PARITY` generates the table
    unsigned int lookup[256] = { FIND_PARITY };

    bool findParity(int x)
    {*/
        // print lookup table (parity of integer `i`)

        /*
        for (int i = 0; i < 256; i++) {
            cout << "The parity of " << i << " is " << lookup[i] << endl;
        }
        */

        // Assuming 32–bit (4 bytes) integer, break the integer into
        // 16–bit chunks and take their XOR
        /*x ^= x >> 16;

        // Again split the 16–bit chunk into 8–bit chunks and take their XOR
        x ^= x >> 8;

        // Note mask used 0xff is 11111111 in binary
        return lookup[x & 0xff];
    }*/
};

int main()
{
    {
        int x = 127;
        cout << x << " in binary is " << bitset<8>(x) << endl;

        cout << boolalpha << Solution().findParity(x) << endl;
    }

    {
        int x = 17;
        cout << x << " in binary is " << bitset<8>(x) << endl;

        cout << boolalpha << Solution().findParity(x) << endl;
    }

    return 0;
}
