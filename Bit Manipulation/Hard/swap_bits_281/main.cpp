/*

Given an integer `n`, swap consecutive `b` bits starting from the given positions `p` and `q` in a binary representation of an integer. The bits to be swapped should not overlap with each other.

Input:

n = 15
p = 2, q = 5	(3rd and 6th bit from the right)
b = 2			(Total number of consecutive bits in each sequence)

Output: 99

Explanation:

15 in binary is 00001111
99 in binary is 01100011

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution
{
public:
    int swapBits(int n, int p, int q, int b)
    {
        // take XOR of bits to be swapped
        int x = ((n >> p) ^ (n >> q));

        // only consider the last b–bits of `x`
        x = x & ((1 << b) - 1);

        // replace the bits to be swapped with the XORed bits
        // and take its XOR with `n`
        return n ^ ((x << p) | (x << q));
    }
};

int main()
{
    int n = 15;
    int p = 2, q = 5;   // 3rd and 6th bit from the right
    int b = 2;          // total number of consecutive bits in each sequence

    cout << n << " in binary is " << bitset<8>(n) << endl;
    n = Solution().swapBits(n, p, q, b);
    cout << n << " in binary is " << bitset<8>(n) << endl;

    return 0;
}
