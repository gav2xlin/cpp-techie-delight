/*

Given two integers, find their XOR without using the XOR operator.

Input: x = 65, y = 80
Output: 17
Explanation: x in binary is 01000001 and y in binary is 01010000. Their XOR is 00010001, which is 17 in decimal.

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution
{
public:
    int findXOR(int x, int y)
    {
        return (x | y) - (x & y);
    }
};

int main()
{
    int x = 65, y = 80;
    cout << bitset<8>(x) << endl;
    cout << bitset<8>(y) << endl;
    cout << bitset<8>(Solution().findXOR(x, y)) << endl;

    return 0;
}
