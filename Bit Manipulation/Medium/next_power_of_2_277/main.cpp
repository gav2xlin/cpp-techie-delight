/*

Given a positive number n, find the next highest power of 2. If n itself is a power of 2, return n.

Input: n = 20
Output: 32

Input: n = 16
Output: 16

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    int nextPowerOf2(unsigned int n)
    {
        int k = 2;
        --n;

        while (n >>= 1) {
            k <<= 1;
        }

        return k;
    }
};

int main()
{
    cout << Solution().nextPowerOf2(20) << endl;
    cout << Solution().nextPowerOf2(16) << endl;

    cout << Solution().nextPowerOf2(2) << endl;
    cout << Solution().nextPowerOf2(1) << endl;
    cout << Solution().nextPowerOf2(0) << endl;

    return 0;
}
