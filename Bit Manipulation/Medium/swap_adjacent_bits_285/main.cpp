/*

Given an integer, swap adjacent bits of it. In other words, swap bits present at even positions with those present in odd positions.

Input:  761622921
Output: 513454662

Explanation:

761622921 (00 10 11 01 01 10 01 01 01 11 00 01 10 00 10 01)
513454662 (00 01 11 10 10 01 10 10 10 11 00 10 01 00 01 10)

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution
{
public:
    inline int swapAdjacentBits(int n) {
        return (((n & 0xAAAAAAAA) >> 1) | ((n & 0x55555555) << 1));
    }
};

int main()
{
    int n = 761622921;

    cout << "before: " << bitset<32>(n) << endl;

    n = Solution().swapAdjacentBits(n);

    cout << " after: " << bitset<32>(n) << endl;

    cout << "mask A: " << bitset<32>(0xAAAAAAAA) << endl;
    cout << "mask 5: " << bitset<32>(0x55555555) << endl;

    return 0;
}
