/*

Given an integer, swap two bits at given positions in a binary representation of it.

Input:

n = 31
p = 2, q = 6 (3rd and 7th bit from the right)

Output: 91

Explanation:

31 in binary is 00011111
91 in binary is 01011011

*/

#include <iostream>

using namespace std;

// xor
// 0 ^ 0 = 0
// 0 ^ 1 = 1
// 1 ^ 0 = 1
// 1 ^ 1 = 0

class Solution
{
public:
    int swapBits(int n, int p, int q)
    {
        if (((n & (1 << p)) >> p) ^ ((n & (1 << q)) >> q))
        {
            n ^= (1 << p);
            n ^= (1 << q);
        }
        return n;
    }
};

int main()
{
    cout << Solution().swapBits(31, 2, 6) << endl;

    return 0;
}
