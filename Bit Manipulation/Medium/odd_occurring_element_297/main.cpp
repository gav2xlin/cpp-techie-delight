/*

Given an integer array, duplicates appear in it an even number of times except for two elements, which appear an odd number of times. Find both odd appearing elements without using any extra memory.

Input: [4, 3, 6, 2, 4, 2, 3, 4, 3, 3]
Output: (4, 6)
Explanation: The odd occurring elements are 4 and 6 as

6 appears once.
2 appears twice.
4 appears thrice.
3 appears 4 times.

Note: The solution can return a pair of odd appearing elements in any order. Assume valid input.

*/

#include <iostream>

using namespace std;

#include <vector>
#include <utility>
#include <cmath>

class Solution
{
public:
    pair<int,int> findOddOccuringElements(vector<int> const &nums)
    {
        int result = 0;
        for (int i: nums) {
            result = result ^ i;
        }

        int k = log2(result & -result); // the rightmost set bit (-1: 0001 & 1111, -2: 0010 & 1110, ...)

        int x = 0, y = 0;

        for (int i: nums)
        {
            if (i & (1 << k)) {
                x = x ^ i;
            } else {
                y = y ^ i;
            }
        }

        return make_pair(x, y);
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findOddOccuringElements({4, 3, 6, 2, 4, 2, 3, 4, 3, 3}) << endl;

    return 0;
}
