/*

Given an integer array where every element appears an even number of times, except one element which appears an odd number of times. If the identical elements appear in pairs in the array and there cannot be more than two consecutive occurrences of an element, find the odd occurring element in logarithmic time and constant space.

Input: [2, 2, 3, 3, 2, 2, 4, 4, 3, 1, 1]
Output: 3

Assume valid input. For instance, both arrays [1, 2, 1] and [1, 1, 2, 2, 2, 3, 3] are invalid. The first one doesn't have identical elements appear in pairs, and the second one contains three consecutive instances of an element.

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findOddOccuringElement(vector<int> const &nums)
    {
        int _xor = 0;
        for (int i = 0; i < nums.size(); ++i) {
            _xor = _xor ^ nums[i];
        }

        return _xor;
    }
};

int main()
{
    cout << Solution().findOddOccuringElement({2, 2, 3, 3, 2, 2, 4, 4, 3, 1, 1}) << endl;

    return 0;
}
