/*

Given a positive number n, find the previous power of 2. If n itself is a power of 2, return n.

Input: n = 20
Output: 16

Input: n = 16
Output: 16

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    int previousPowerOf2(unsigned int n)
    {
        int k = 1;

        while (n >>= 1) {
            k <<= 1;
        }

        return k;
    }
};

int main()
{
    cout << Solution().previousPowerOf2(20) << endl;
    cout << Solution().previousPowerOf2(16) << endl;

    cout << Solution().previousPowerOf2(2) << endl;
    cout << Solution().previousPowerOf2(1) << endl;
    cout << Solution().previousPowerOf2(0) << endl;

    return 0;
}
