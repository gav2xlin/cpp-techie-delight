/*

Given a number, check if it is a power of 8 or not.

Input: 512
Output: true

Input: 56
Output: false

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    bool isPowerOf8(int n)
    {
        return n && !(n & (n - 1)) && !(n & 0xB6DB6DB6);
    }
};

int main()
{
    cout << boolalpha << Solution().isPowerOf8(512) << endl;
    cout << Solution().isPowerOf8(56) << endl;

    return 0;
}
