/*

Given a set S, return all subsets of it, i.e., find the power set of S. A power set of a set S is the set of all subsets of S, including the empty set and S itself.

Input : S[] = [1, 2, 3]
Output: [[3, 2, 1], [3, 2], [3, 1], [3], [2, 1], [2], [1], []]

Input : S[] = [1, 2, 1]
Output: [[1, 2, 1], [1, 2], [1, 1], [1], [2, 1], [2], [1], []]

Input : S[] = [1, 1]
Output: [[1, 1], [1], [1], []]

Input : S[] = []
Output: [[]]

Note: The solution can return elements of a subsets in any order.

*/

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Solution
{
public:
    vector<vector<int>> findPowerSet(vector<int> const &S)
    {
        vector<vector<int>> powerset;

        int n = pow(2, S.size());

        for (int i = 0; i < n; ++i)
        {
            vector<int> input;
            for (int j = 0; j < S.size(); ++j)
            {
                if (i & (1 << j)) {
                    input.push_back(S[j]);
                }
            }
            powerset.push_back(input);
        }

        return powerset;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& vectors) {
    for (auto& vec : vectors) {
        os << vec << endl;
    }
    return os;
}

int main()
{
    Solution s;

    cout << s.findPowerSet({1, 2, 3}) << endl;
    cout << s.findPowerSet({1, 2, 1}) << endl;
    cout << s.findPowerSet({1, 1}) << endl;
    cout << s.findPowerSet({}) << endl;

    return 0;
}
