cmake_minimum_required(VERSION 3.5)

project(power_set_291 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(power_set_291 main.cpp)

install(TARGETS power_set_291
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
