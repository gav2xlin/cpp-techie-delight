/*

Given a positive number, check if it is a power of 4 or not.

Input: 256
Output: true

Input: 25
Output: false

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    bool isPowerOf4(int n)
    {
        return n && !(n & (n - 1)) && !(n & 0xAAAAAAAA);
        //return !(n & (n - 1)) && (n % 3 == 1);
    }
};

int main()
{
    cout << boolalpha << Solution().isPowerOf4(256) << endl;
    cout << Solution().isPowerOf4(25) << endl;

    return 0;
}
