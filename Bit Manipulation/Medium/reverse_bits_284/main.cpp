/*

Given an integer, reverse its bits using binary operators.

Input: -100
Output: 973078527
Explanation: -100 in binary is 11111111111111111111111110011100. Reversing its bits results in number 973078527, which is 00111001111111111111111111111111 in binary.

*/

#include <iostream>
#include <bitset>

using namespace std;

constexpr int INT_SIZE = sizeof(int) * 8;

class Solution
{
public:
    int reverseBits(int n)
    {
        int pos = INT_SIZE - 1;

        int reverse = 0;

        while (pos >= 0 && n)
        {
            if (n & 1) {
                reverse = reverse | (1 << pos);
            }

            n >>= 1;
            --pos;
        }

        return reverse;
    }
};

int main()
{
    int n = -100;

    cout << "before: " << bitset<32>(n) << endl;
    cout << " after: " << bitset<32>(Solution().reverseBits(n)) << endl;

    return 0;
}
