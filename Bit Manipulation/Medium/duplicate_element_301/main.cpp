/*

Given an integer array of size `n+2` containing elements between 1 and `n` with two element repeating, find both duplicate elements without using any extra memory in linear time.

Input: [4, 3, 6, 5, 2, 4, 1, 1]
Output: (1, 4)

Input: [2, 1, 1, 2]
Output: (1, 2)

Note: The solution can return a pair of duplicate elements in any order. Assume valid input.

*/


#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

using namespace std;

class Solution
{
public:
    pair<int,int> findDuplicateElements(vector<int> const &nums)
    {
        int n = nums.size() - 2;
        if (n < 0) {
            return {-1, -1};
        }

        int result = nums[0] ^ nums[n+1];
        for (int i = 1; i <= n; ++i) {
            result = result ^ nums[i] ^ i;
        }

        int x = 0, y = 0;

        int k = log2(result & -result);

        for (int i = 0; i < n + 2; ++i)
        {
            if (nums[i] & (1 << k)) {
                x = x ^ nums[i];
            } else {
                y = y ^ nums[i];
            }
        }

        for (int i = 1; i <= n; ++i)
        {
            if (i & (1 << k)) {
                x = x ^ i;
            } else {
                y = y ^ i;
            }
        }

        return make_pair(x, y);
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findDuplicateElements({4, 3, 6, 5, 2, 4, 1, 1}) << endl;
    cout << Solution().findDuplicateElements({2, 1, 1, 2}) << endl;

    return 0;
}
