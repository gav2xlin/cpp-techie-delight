/*

Given an integer array of size `n`, with all its elements between 1 and `n` and one element occurring twice and one element missing. Find the missing number and the duplicate element in linear time and without using any extra memory.

Input: [4, 3, 6, 5, 2, 4]
Output: (4, 1)
Explanation: The duplicate element is 4 and the missing element is 1.

Input: [4, 2, 2, 1]
Output: (2, 3)
Explanation: The duplicate element is 2 and the missing element is 3.

Note: The solution should return (duplicate element, missing element) pair. Assume valid input.

*/

#include <iostream>
#include <vector>
#include <utility>
#include <cmath>

using namespace std;

class Solution
{
public:
    pair<int,int> findMissingAndDuplicate(vector<int> const &nums)
    {
        int n = nums.size();

        int result = n;
        for (int i = 0; i < n; ++i) {
            result = result ^ nums[i] ^ i;
        }

        int x = 0, y = 0;

        int k = log2(result & -result);

        for (int val: nums)
        {
            if (val & (1 << k)) {
                x = x ^ val;
            } else {
                y = y ^ val;
            }
        }

        for (int i = 1; i <= n; ++i)
        {
            if (i & (1 << k)) {
                x = x ^ i;
            } else {
                y = y ^ i;
            }
        }

        if (find(nums.begin(), nums.end(), x) == nums.end()) {
            return make_pair(y, x);
        } else {
            return make_pair(x, y);
        }
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findMissingAndDuplicate({4, 3, 6, 5, 2, 4}) << endl;
    cout << Solution().findMissingAndDuplicate({4, 2, 2, 1}) << endl;

    return 0;
}
