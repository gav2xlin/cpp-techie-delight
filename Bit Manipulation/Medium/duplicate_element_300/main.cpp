/*

Given an integer array of size `n` containing elements between 1 and `n-1` with one element repeating, find the duplicate number in it without using any extra space.

Input: [1, 2, 3, 4, 4]
Output: 4

Input: [1, 2, 3, 4, 2]
Output: 2

Input: [1, 1]
Output: 1

Assume valid input.

*/

#include <iostream>
#include <vector>

using namespace std;

// same elements will cancel each other as a ^ a = 0,
// 0 ^ 0 = 0 and a ^ 0 = a

class Solution
{
public:
    int findDuplicateElement(vector<int> const &nums)
    {
        int n = nums.size();

        int _xor = 0;

        for (int i = 0; i < n; ++i) {
            _xor ^= nums[i];
        }

        for (int i = 1; i <= n - 1; ++i) {
            _xor ^= i;
        }

        return _xor;
    }
};

int main()
{
    cout << Solution().findDuplicateElement({1, 2, 3, 4, 4}) << endl;
    cout << Solution().findDuplicateElement({1, 2, 3, 4, 2}) << endl;
    cout << Solution().findDuplicateElement({1, 1}) << endl;

    return 0;
}
