/*

Given an array having elements between 0 and 31, find elements that occur an odd number of times without using the extra space.

Input : [5, 8, 2, 5, 8, 2, 8, 5, 1, 8, 2]
Output: {5, 2, 1}

Explanation:

1 occurs once.
2 and 5 occurs thrice.
8 occurs four times.

Assume valid input.

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    unordered_set<int> findOddOccuringElements(vector<int> const &nums)
    {
        unordered_set<int> res;
        int n = nums.size();

        int _xor = 0;
        for (int i = 0; i < n; ++i) {
            _xor ^= (1 << nums[i]);
        }

        for (int i = 0; i < n; ++i)
        {
            if (_xor & (1 << nums[i]))
            {
                _xor ^= (1 << nums[i]);     // to avoid duplicates

                res.insert(nums[i]);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findOddOccuringElements({5, 8, 2, 5, 8, 2, 8, 5, 1, 8, 2}) << endl;

    return 0;
}
