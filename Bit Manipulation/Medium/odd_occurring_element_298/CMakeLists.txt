cmake_minimum_required(VERSION 3.5)

project(odd_occurring_element_298 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(odd_occurring_element_298 main.cpp)

install(TARGETS odd_occurring_element_298
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
