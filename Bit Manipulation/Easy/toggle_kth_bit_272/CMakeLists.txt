cmake_minimum_required(VERSION 3.5)

project(toggle_kth_bit_272 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(toggle_kth_bit_272 main.cpp)

install(TARGETS toggle_kth_bit_272
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
