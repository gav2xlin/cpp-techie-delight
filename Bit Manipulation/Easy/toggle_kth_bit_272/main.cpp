/*

Given an integer n and a positive number k, toggle k'th bit of n.

Input: n = 20, k = 3
Output: 16
Explanation: 20 in binary is 00010100 and 16 in binary is 00010000

Input: n = 16, k = 3
Output: 20

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution {
public:
    int toggleKthBit(int n, int k) {
        /*int mask = 1 << --k;
        return n ^ mask;*/
        bitset<sizeof(int) << 3> d{static_cast<unsigned long long>(n)};
        bitset<sizeof(int) << 3> m{static_cast<unsigned long long>(1 << --k)};
        return (d ^ m).to_ulong();
    }
};

int main() {
    cout << Solution().toggleKthBit(20, 3) << endl;
    cout << Solution().toggleKthBit(16, 3) << endl;

    return 0;
}
