/*

Given two integers x and y, find the total number of bits needed to be flipped for converting x to y.

Input: x = 65, y = 80
Output: 2
Explanation: The total number of bits to be flipped is 2 since 65 is 01000001 in binary and 80 is 01010000 in binary.

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution
{
public:
    int findBits(int x, int y)
    {
        unsigned int n = 1 << (sizeof(int) * 8 - 1);

        int res = 0;
        while (n != 0) {
            if ((x & n) != (y & n)) {
                ++res;
            }
            n >>= 1;
        }
        return res;
        /*constexpr int n = sizeof(int) << 3;
        bitset<n> xb(x);
        bitset<n> yb(y);

        int res = 0;
        for (int i = 0; i < n; ++i) {
            if (xb.test(i) != yb.test(i)) {
                ++res;
            }
        }
        return res;*/
    }
};

int main()
{
    //cout << Solution().findBits(65, 80) << endl;
    cout << Solution().findBits(5810, 8348) << endl; // 8

    return 0;
}
