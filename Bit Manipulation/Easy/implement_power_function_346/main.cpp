/*

Given two integers, `x` and `n`, where `n` is non-negative, efficiently compute the power function `pow(x, n)`.

Input: x = -2, n = 10
Output: 1024
Explanation: pow(-2, 10) = 1024

Input: x = -3, n = 4
Output: 81
Explanation: pow(-3, 4) = 81

Input: x = 5, n = 0
Output: 1
Explanation: pow(5, 0) = 1

Input: x = -2, n = 3
Output: -8
Explanation: pow(-2, 3) = -8

*/

// x^n = x^(n/2) * x^(n/2) // n is even
// x^n = x * x^(n/2) * x^(n/2) // n is odd

#include <iostream>

using namespace std;

class Solution
{
public:
    long power(int x, unsigned int n)
    {
        long res = 1L;
        long lx = static_cast<long>(x);

        while (n != 0) {
            if (n & 1) {
                res *= lx;
            }

            lx *= lx;
            n >>= 1;
        }

        return res;
    }
};

int main()
{
    /*cout << Solution().power(-2, 10) << endl;
    cout << Solution().power(-3, 4) << endl;
    cout << Solution().power(5, 0) << endl;
    cout << Solution().power(-2, 3) << endl;*/
    cout << Solution().power(3398, 5) << endl;

    return 0;
}
