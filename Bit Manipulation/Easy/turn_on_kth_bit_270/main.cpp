/*

Given an integer n and a positive number k, turn on k'th bit in n.

Input: n = 20, k = 4
Output: 28
Explanation:

20 in binary is 00010100
28 in binary is 00011100


Input: n = -24, k = 3
Output: -20
Explanation:

-24 in binary is 1111111111101000
-20 in binary is 1111111111101100

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution {
public:
    int turnOnKthBit(int n, int k) {
        int mask = 1 << --k;
        return n | mask;
        /*bitset<sizeof(int) << 3> b{static_cast<unsigned long long>(n)};
        b.set(k - 1);
        return static_cast<int>(b.to_ulong());*/
    }
};

int main() {
    cout << Solution().turnOnKthBit(20, 4) << endl;
    cout << Solution().turnOnKthBit(-24, 3) << endl;

    return 0;
}
