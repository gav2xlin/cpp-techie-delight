/*

Given an integer, find its 32-bit binary representation without using built-in functions.

Input:  20
Output: "00000000000000000000000000010100"

Input:  64
Output: "00000000000000000000000001000000"

Input:  127
Output: "00000000000000000000000001111111"

Input:  -1
Output: "11111111111111111111111111111111"

*/

#include <iostream>
#include <bitset>
#include <sstream>

using namespace std;

class Solution
{
public:
    string toBinary(int n)
    {
        /*unsigned int m = 1 << (sizeof(int) * 8 - 1);

        stringstream ss;
        while (m != 0) {
            ss << (static_cast<unsigned int>(n) & m ? '1' : '0');
            m >>= 1;
        }
        return ss.str();*/
        constexpr int sz = sizeof(int) << 3;
        bitset<sz> b(n);

        stringstream ss;
        for (int i = sz - 1; i >= 0;--i) {
            ss << (b.test(i) ? '1' : '0');
        }
        return ss.str();
    }
};

int main()
{
    cout << Solution().toBinary(20) << endl;
    cout << Solution().toBinary(64) << endl;
    cout << Solution().toBinary(127) << endl;
    cout << Solution().toBinary(-1) << endl;

    return 0;
}
