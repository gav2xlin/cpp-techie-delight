/*

Given an integer, count its set bits.

Input: n = 16
Output: 1
Explanation: The binary representation of 16 is 00000000000000000000000000001000.

Input: n = -1
Output: 32
Explanation: The binary representation of -1 is 11111111111111111111111111111111.

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution
{
public:
    int countSetBits(int n)
    {
        /*unsigned int d = static_cast<unsigned int>(n);

        int res = 0;
        while (d != 0) {
            if (d & 1) ++res;
            d >>= 1;
        }
        return res;*/
        bitset<sizeof(int) << 3> d{static_cast<unsigned long long>(n)};

        int res = 0;
        while (d != 0) {
            if (d.test(0)) ++res;
            d >>= 1;
        }
        return res;
    }
};

int main()
{
    cout << Solution().countSetBits(16) << endl;
    cout << Solution().countSetBits(-1) << endl;

    return 0;
}
