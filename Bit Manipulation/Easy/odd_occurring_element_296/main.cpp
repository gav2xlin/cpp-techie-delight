/*

Given an integer array, duplicates are present in it in a way that all duplicates appear an even number of times except one which appears an odd number of times. Find that odd appearing element in linear time and without using any extra memory.

Input: [4, 3, 6, 2, 6, 4, 2, 3, 4, 3, 3]
Output: 4

Assume valid input.

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findOddOccuringElement(vector<int> const &nums)
    {
        int res = 0;
        for (auto v : nums) {
            res ^= v;
        }
        return res;
    }
};

int main()
{
    cout << Solution().findOddOccuringElement({4, 3, 6, 2, 6, 4, 2, 3, 4, 3, 3}) << endl;

    return 0;
}
