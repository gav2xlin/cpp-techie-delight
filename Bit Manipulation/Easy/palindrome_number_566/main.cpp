/*

Given a non-negative number n, check if the binary representation of n is a palindrome. A palindromic number is a number that remains the same when its digits are reversed.

The solution should consider the binary representation starting from the LSB (least significant bit) till the last set bit.

Input: n = 9
Output: true
Explanation: The binary representation of 9 is 1001, which is symmetric.

Input: n = 11
Output: false
Explanation: The binary representation of 11 is 1011, which is not symmetric.

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution
{
public:
    bool isPalindrome(int n)
    {
        /*unsigned int k = static_cast<unsigned int>(n);
        int rev = 0;

        while (k) {
            rev <<= 1;
            if (k & 1) {
                rev |= 1;
            }
            k >>= 1;
        }

        return n == rev;*/
        constexpr int sz = sizeof(int) << 3;
        bitset<sz> k{static_cast<unsigned long long>(n)};
        bitset<sz> rev{};

        while (k != 0) {
            rev <<= 1;
            if (k.test(0)) {
                rev.set(0);
            }
            k >>= 1;
        }

        return static_cast<unsigned long long>(n) == rev.to_ulong();
    }
};

int main()
{
    cout << boolalpha << Solution().isPalindrome(9) << endl;
    cout << Solution().isPalindrome(11) << endl;

    return 0;
}
