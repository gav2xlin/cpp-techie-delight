/*

Given an integer n and a positive number k, check if k'th bit is set or not.

Input: n = 20, k = 3
Output: true
Explanation: 20 in binary is 00010100

Input: n = 16, k = 3
Output: false
Explanation: 16 in binary is 00010000

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution {
public:
    bool isKthBitSet(int n, int k) {
        /*int mask = 1 << --k;
        return n & mask;*/
        bitset<sizeof(int) << 3> b{static_cast<unsigned long long>(n)};
        return b.test(k - 1);
    }
};

int main() {
    cout << boolalpha << Solution().isKthBitSet(20, 3) << endl;
    cout << Solution().isKthBitSet(16, 3) << endl;

    return 0;
}
