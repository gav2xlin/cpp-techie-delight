/*

Given two integers, add their binary representation.

Input: x = 12731, y = 38023
Output: "00000000000000001100011001000010"

Explanation:

x (12731) in binary is 00000000000000000011000110111011
y (38023) in binary is 00000000000000001001010010000111
x + y is 00000000000000001100011001000010

*/

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
private:
    string bin_to_str(int v) {
        string res;
        for (int i = 0; i < sizeof(int) << 3; ++i) {
            res = (v & 1 ? '1' : '0') + res;
            v >>= 1;
        }
        return res;
    }
public:
    string add(int x, int y)
    {
        string sx = bin_to_str(x), sy = bin_to_str(y), res{};

        reverse(sx.begin(), sx.end());
        reverse(sy.begin(), sy.end());

        bool carry = false;
        for (int i = 0; i < sizeof(int) << 3; ++i) {
            char bx = sx[i], by = sy[i];

            if (bx == '0' && by == '0') {
                res = (carry ? '1' : '0') + res;
                carry = false;
            } else if (bx == '1' && by == '1') {
                res = (carry ? '1' : '0') + res;
                carry = true;
            } else if ((bx == '1' && by == '0') || (bx == '0' && by == '1')) {
                res = (carry ? '0' : '1') + res;
            }
        }

        return res;
    }
};

int main()
{
    cout << Solution().add(12731, 38023) << endl;
    return 0;
}
