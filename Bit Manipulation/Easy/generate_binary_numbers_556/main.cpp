/*

Given a positive number n, efficiently generate binary numbers between 1 and n in linear time.

Input: n = 16
Output: {"1", "10", "11", "100", "101", "110", "111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111", "10000"}

*/

#include <iostream>
#include <string>
#include <unordered_set>
#include <queue>

using namespace std;

class Solution
{
public:
    unordered_set<string> generate(int n)
    {
        unordered_set<string> digits;

        queue<string> q;
        q.push("1");

        while (n-- > 0) {
            string s = q.front();

            digits.insert(s);

            q.push(s + "0");
            q.push(s + "1");
            q.pop();
        }

        return digits;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& digits) {
    for (auto& digit : digits) {
        os << digit << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().generate(16) << endl;

    return 0;
}
