/*

Given a number, check if adjacent bits are set in the binary representation of it.

Input : 67
Output: true
Explanation: 67 in binary is 01000011 and has adjacent pair of set bits.

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution
{
public:
    bool isAdjacentBitsSet(int n)
    {
        constexpr size_t sz = sizeof(int) << 3;
        //bitset<sz> bn{static_cast<unsigned int>(n)};
        int p = 0;
        bool res = false;

        for (int b = sz - 1; b >= 0; --b) {
            int mask = 1 << b;
            if (n & mask) {
            //if (bn.test(b)) {
                ++p;
                if (p > 1) {
                    res = true;
                    break;
                }
            } else {
                p = 0;
            }
        }

        return res;
    }
};

int main()
{
    //cout << boolalpha << Solution().isAdjacentBitsSet(67) << endl; // true
    //cout << boolalpha << Solution().isAdjacentBitsSet(256) << endl; // false
    cout << boolalpha << Solution().isAdjacentBitsSet(17) << endl; // false

    return 0;
}
