/*

Given an integer, find position of the rightmost set bit in it.

Input: n = 20
Output: 3
Explanation: 20 in binary is 00010100

*/

#include <iostream>
#include <cstddef>
#include <bitset>

using namespace std;

class Solution
{
public:
    int positionOfRightmostSetBit(int n)
    {
        constexpr size_t sz = sizeof(int) << 3;
        bitset<sz> bn{static_cast<unsigned int>(n)};
        int p = -1;

        for (int b = sz - 1; b >= 0; --b) {
            /*int mask = 1 << b;
            if (n & mask) {*/
            if (bn.test(b)) {
                p = b;
            }
        }

        return p + 1;
    }
};


int main()
{
    cout << Solution().positionOfRightmostSetBit(20) << endl;

    return 0;
}
