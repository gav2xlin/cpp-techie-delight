/*

Given a non-negative number, determine whether it is a palindrome. A palindromic number is a number that remains the same when its digits are reversed.

Input: n = 16461
Output: true
Explanation: 16461 is symmetrical.

Input: n = 1121
Output: false
Explanation: 1121 is non-symmetrical.

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    bool isPalindrome(int n)
    {
        int rev = 0, tmp = n;
        while (tmp) {
            rev = rev * 10 + tmp % 10;
            tmp = tmp / 10;
        }
        return rev == n;
    }
};

int main()
{
    cout << boolalpha << Solution().isPalindrome(16461) << endl;
    cout << Solution().isPalindrome(1121) << endl;

    return 0;
}
