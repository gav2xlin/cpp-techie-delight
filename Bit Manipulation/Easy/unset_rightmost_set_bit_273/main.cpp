/*

Given an integer, unset its rightmost set bit.

Input: n = 20
Output: 16
Explanation:

20 in binary is 00010100
16 in binary is 00010000

*/

#include <iostream>
#include <cstddef>
#include <bitset>

using namespace std;

class Solution
{
public:
    int unsetRightmostSetBit(int n)
    {
        constexpr size_t sz = sizeof(int) << 3;
        //bitset<sz> bn{static_cast<unsigned int>(n)};
        int p = -1;

        for (int b = sz - 1; b >= 0; --b) {
            int mask = 1 << b;
            if (n & mask) {
            //if (bn.test(b)) {
                p = b;
            }
        }

        if (p >= 0) {
            //bn.reset(p);
            int mask = ~(1 << p);
            n &= mask;
        }

        //return bn.to_ulong();
        return n;
    }
};

int main()
{
    cout << Solution().unsetRightmostSetBit(20) << endl;

    return 0;
}
