/*

Given an integer n and a positive number k, turn off k'th bit in n.

Input: n = 20, k = 3
Output: 16
Explanation:

20 in binary is 00010100
16 in binary is 00010000

Input: n = -20, k = 3
Output: -24
Explanation:

-20 in binary is 1111111111101100
-24 in binary is 1111111111101000

*/

#include <iostream>
#include <bitset>

using namespace std;

class Solution {
public:
    int turnOffKthBit(int n, int k) {
        //int mask = ~(1 << --k);
        //return n & mask;
        bitset<sizeof(int) << 3> b{static_cast<unsigned long long>(n)};
        b.reset(k - 1);
        return static_cast<int>(b.to_ulong());
    }
};

int main() {
    cout << Solution().turnOffKthBit(20, 3) << endl;
    cout << Solution().turnOffKthBit(-20, 3) << endl;
    cout << Solution().turnOffKthBit(-1, 5) << endl;

    return 0;
}
