/*

Given a postfix expression, construct a binary expression tree from it and return its root. The binary expression tree is a binary tree whose leaves are operands, such as constants or variable names, and the other nodes contain operators.

Input: ab+cde+**
Output: Root of the following expression tree.

             *
           /   \
         /		 \
        +		  *
       / \		 / \
      /	  \		/	\
     a	   b   c	 +
                    / \
                   /   \
                  d		e

*/

#include <iostream>

using namespace std;

class Node
{
public:
    char data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(char data): data(data) {}
    Node(char data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node* constructExpressionTreeInternal(string postfix, int& pos)
    {
        if (pos < 0) return nullptr;

        Node* node = new Node{postfix[pos]};

        char op = postfix[pos];
        if (op == '+' || op == '-' || op == '*' || op == '/') {
            node->right = constructExpressionTreeInternal(postfix, --pos);
            node->left = constructExpressionTreeInternal(postfix, --pos);
        }

        return node;
    }

public:

    /*
        An expression tree node is defined as:

        class Node
        {
        public:
            char data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(char data): data(data) {}
            Node(char data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructExpressionTree(string postfix)
    {
        if (postfix.empty()) return nullptr;

        int pos = postfix.size() - 1;
        return constructExpressionTreeInternal(postfix, pos);
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    Node* root = Solution().constructExpressionTree("ab+cde+**");

    if (root != nullptr) {
        cout << *root << endl;
    }

    return 0;
}
