/*

Given the root of a binary expression tree representing algebraic expressions, evaluate it and return its value. A binary expression tree is a binary tree, where the operators are stored in the tree's internal nodes, and the leaves contain constants.

Assume that each node of the binary expression tree has zero or two children. The supported operators are +(addition), −(subtraction), *(multiplication), ÷(division) and ^(exponentiation).

Input:

            (+)
           /   \
         /		 \
       (*)		 (/)
       / \		 / \
      /	  \		/	\
    (-)	   5   21	 7
    / \
   /   \
  10	5

Output: 28

Explanation: The corresponding infix notation is ((10-5)*5)+(21/7) = 28 which can be produced by traversing the expression tree in an inorder fashion.

*/

#include <iostream>
#include <string>

using namespace std;

class Node
{
public:
    string data;								// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(string data): data(data) {}
    Node(string data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        An expression tree node is defined as:

        class Node
        {
        public:
            string data;								// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(string data): data(data) {}
            Node(string data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    double evaluate(Node* root)
    {
        if (root == nullptr) return 0;

        string data = root->data;
        if (!data.empty()) {
            switch (data[0]) {
            case '+':
                return evaluate(root->left) + evaluate(root->right);
            case '-':
                return evaluate(root->left) - evaluate(root->right);
            case '*':
                return evaluate(root->left) * evaluate(root->right);
            case '/':
                return evaluate(root->left) / evaluate(root->right);
            default:
                return stod(data);
            }
        }

        return 0;
    }
};

int main()
{
    Node n6{"10"};
    Node n7{"5"};
    Node n4{"-", &n6, &n7};
    Node n5{"5"};
    Node n2{"*", &n4, &n5};
    Node n8{"21"};
    Node n9{"7"};
    Node n3{"/", &n8, &n9};
    Node n1{"+", &n2, &n3};

    cout << Solution().evaluate(&n1) << endl;

    return 0;
}
