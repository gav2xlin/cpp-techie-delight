/*

Given the root of a binary tree, in-place replace each node's value to the sum of all elements present in its left and right subtree. You may assume the value of an empty child node to be 0.

Input:

       1
     /   \
    /	  \
   2	   3

Output:

       5
     /   \
    /	  \
   0	   0


Input:

       1
     /	 \
    /	  \
   /	   \
  2			3
   \	   / \
    \	  /	  \
     4   5	   6
        / \
       /   \
      7		8

Output:

       35
     /	  \
    /	   \
   /		\
  4			26
   \	   /  \
    \	  /	   \
     0   15		0
        /  \
       /	\
      0		 0

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int transformInternal(Node* root)
    {
        if (root == nullptr) return 0;

        int prev = root->data;
        root->data = transformInternal(root->left) + transformInternal(root->right);
        return root->data + prev;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void transform(Node* root)
    {
        if (root == nullptr) return;

        //int prev = root->data;
        root->data = transformInternal(root->left) + transformInternal(root->right);
        //root->data += prev;
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    {
        Node n2{2};
        Node n3{3};
        Node n1{1, &n2, &n3};

        Solution().transform(&n1);
        cout << n1 << endl;
    }

    {
        Node n4{4};
        Node n2{2, nullptr, &n4};
        Node n7{7};
        Node n8{8};
        Node n5{5, &n7, &n8};
        Node n6{6};
        Node n3{3, &n5, &n6};
        Node n1{1, &n2, &n3};

        Solution().transform(&n1);
        cout << n1 << endl;
    }

    return 0;
}
