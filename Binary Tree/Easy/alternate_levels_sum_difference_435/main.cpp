/*

Given the root of a binary tree, return the difference between the sum of all nodes present at odd levels and the sum of all nodes present at even level.

Input:
           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4	  	5	  6
           / \
          /	  \
         7	   8

Output: -4

Explanation: The difference is (1 + 4 + 5 + 6) - (2 + 3 + 7 + 8) = -4

*/

#include <iostream>
#include <queue>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findDifference(Node* root)
    {
        if (root == nullptr) return 0;

        queue<Node*> q;
        q.push(root);

        int level = 0, odd = 0, even = 0;

        while (!q.empty()) {
            int size = q.size();

            ++level;

            while (size--) {
                Node* node = q.front();
                q.pop();

                if (level & 1) {
                    odd += node->data;
                } else {
                    even += node->data;
                }

                if (node->left) {
                    q.push(node->left);
                }
                if (node->right) {
                    q.push(node->right);
                }
            }
        }

        return odd - even;
    }
};

int main()
{
    Node n4{4};
    Node n2{2, &n4, nullptr};
    Node n7{7};
    Node n8{8};
    Node n5{5, &n7, &n8};
    Node n6{6};
    Node n3{3, &n5, &n6};
    Node n1{1, &n2, &n3};

    cout << Solution().findDifference(&n1) << endl;

    return 0;
}
