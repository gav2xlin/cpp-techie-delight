/*

Given root of two binary trees, x and y, determine if x can be converted into y by doing any number of swaps of its right and left branches. Two binary trees are identical if they have identical structure and same nodes' values.

Input: Two binary trees

                6
              /   \
            /		\
          /			  \
         3			   8
        / \			  / \
       /   \		 /   \
      1		7		4	  2
                   / \	   \
                  /   \		\
                 7	   1	 3

                6
              /   \
            /		\
          /			  \
         8			   3
        / \			  / \
       /   \		 /   \
      2		4		7	  1
     /	   / \
    /	  /	  \
   3	 1	   7

Output: true

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool canTransformed(Node* x, Node* y)
    {
        if (x == y) return true;

        return (x != nullptr && y != nullptr) && (x->data == y->data) &&
                ((canTransformed(x->left, y->left) && canTransformed(x->right, y->right)) ||
                 (canTransformed(x->left, y->right) && canTransformed(x->right, y->left)));
    }
};

int main()
{
    /*
                 6
              /   \
            /		\
          /			  \
         3			   8
        / \			  / \
       /   \		 /   \
      1		7		4	  2
                   / \	   \
                  /   \		\
                 7	   1	 3

                6
              /   \
            /		\
          /			  \
         8			   3
        / \			  / \
       /   \		 /   \
      2		4		7	  1
     /	   / \
    /	  /	  \
   3	 1	   7
     */
    {
        Node n1_11{1};
        Node n1_17{7};
        Node n1_13{3, &n1_11, &n1_17};
        Node n1_27{7};
        Node n1_21{1};
        Node n1_4{4, &n1_27, &n1_21};
        Node n1_3{3};
        Node n1_2{2, nullptr, &n1_3};
        Node n1_8{8, &n1_4, &n1_2};
        Node n1_6{6, &n1_13, &n1_8};

        Node n2_11{1};
        Node n2_17{7};
        Node n2_13{3, &n2_17, &n2_11};
        Node n2_27{7};
        Node n2_21{1};
        Node n2_4{4, &n2_21, &n2_27};
        Node n2_3{3};
        Node n2_2{2, &n2_3, nullptr};
        Node n2_8{8, &n2_4, &n2_2};
        Node n2_6{6, &n2_8, &n2_13};

        cout << boolalpha << Solution().canTransformed(&n1_6, &n2_6) << endl;
    }

    return 0;
}
