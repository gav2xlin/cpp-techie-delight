/*

Given the root of two binary trees, x and y, check if x is identical to y. Two binary trees are identical if they have identical structure and their contents are also the same.

Input:
           1						1
         /   \					  /   \
        /	  \					 /	   \
       2	   3				2		3
      /	\	  / \			   / \	   / \
     /	 \ 	 /	 \			  /	  \	  /	  \
    4	  5	6	  7			 4	   5 6	   7

Output: true
Explanation: Both binary trees have the same structure and contents.

Input:
           1						1
         /   \					  /   \
        /	  \					 /	   \
       2	   3				2		3
      /	\	  / \			   / \	   /
     /	 \ 	 /	 \			  /	  \	  /
    4	  5	6	  7			 4	   5 6

Output: false
Explanation: Both binary trees have different structures.

Input:
           1						1
         /   \					  /   \
        /	  \					 /	   \
       2	   3				2		3
      /	\	  / \			   / \	   / \
     /	 \ 	 /	 \			  /	  \	  /	  \
    4	  5	6	  7			 4	   5 6	   8

Output: false
Explanation: Both binary trees have the same structure but differ in nodes' values.

*/

#include <iostream>
#include <stack>
#include <utility>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isIdentical(Node* x, Node* y)
    {
        if (x == nullptr && y == nullptr) return true;
        if (x == nullptr || y == nullptr) return false;

        stack<pair<Node*, Node*>> st;
        st.push({x, y});

        while (!st.empty()) {
            pair<Node*, Node*> p = st.top();
            st.pop();

            if (p.first != nullptr && p.second != nullptr) {
                if (p.first->data != p.second->data) return false;

                st.push({p.first->left, p.second->left});
                st.push({p.first->right, p.second->right});
            } else if (p.first != nullptr || p.second != nullptr) {
                return false;
            }
        }

        return true;
    }
};

int main()
{
    {
        Node n14{4};
        Node n15{5};
        Node n12{2, &n14, &n15};
        Node n16{6};
        Node n17{7};
        Node n13{3, &n16, &n17};
        Node n11{1, &n12, &n13};

        Node n24{4};
        Node n25{5};
        Node n22{2, &n24, &n25};
        Node n26{6};
        Node n27{7};
        Node n23{3, &n26, &n27};
        Node n21{1, &n22, &n23};

        cout << boolalpha << Solution().isIdentical(&n11, &n21) << endl;
    }

    {
        Node n14{4};
        Node n15{5};
        Node n12{2, &n14, &n15};
        Node n16{6};
        Node n17{7};
        Node n13{3, &n16, &n17};
        Node n11{1, &n12, &n13};

        Node n24{4};
        Node n25{5};
        Node n22{2, &n24, &n25};
        Node n26{6};
        Node n23{3, &n26, nullptr};
        Node n21{1, &n22, &n23};

        cout << Solution().isIdentical(&n11, &n21) << endl;
    }

    {
        Node n14{4};
        Node n15{5};
        Node n12{2, &n14, &n15};
        Node n16{6};
        Node n17{7};
        Node n13{3, &n16, &n17};
        Node n11{1, &n12, &n13};

        Node n24{4};
        Node n25{5};
        Node n22{2, &n24, &n25};
        Node n26{6};
        Node n28{8};
        Node n23{3, &n26, &n28};
        Node n21{1, &n22, &n23};

        cout << boolalpha << Solution().isIdentical(&n11, &n21) << endl;
    }

    return 0;
}
