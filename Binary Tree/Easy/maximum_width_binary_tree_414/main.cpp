/*

Given the root of a binary tree, return the maximum number of nodes at any level in the binary tree.

Input:
           1
         /   \
        /	  \
       2	   3
      /	\	  / \
     /	 \ 	 /	 \
    4	  5	6	  7

Output: 4

Input:
           1
          /
         /
        2
       /
      /
     3
    /
   /
  4

Output: 1

*/

#include <iostream>
#include <queue>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findMaximumWidth(Node* root)
    {
        if (root == nullptr) return 0;

        queue<Node*> q;
        q.push(root);

        int max_width = 0;

        while (!q.empty()) {
            int size = q.size();

            if (size > max_width) {
                max_width = size;
            }

            while (size--) {
                Node* node = q.front();
                q.pop();

                if (node->left) {
                    q.push(node->left);
                }
                if (node->right) {
                    q.push(node->right);
                }
            }
        }

        return max_width;
    }
};

int main()
{
    {
        Node n6{6};
        Node n7{7};
        Node n3{3, &n6, &n7};
        Node n5{5};
        Node n2{2, &n3, &n5};
        Node n1{1, &n2, &n3};

        cout << Solution().findMaximumWidth(&n1) << endl;
    }

    {
        Node n4{4};
        Node n3{3, &n4, nullptr};
        Node n2{2, &n3, nullptr};
        Node n1{1, &n2, nullptr};

        cout << Solution().findMaximumWidth(&n1) << endl;
    }

    return 0;
}
