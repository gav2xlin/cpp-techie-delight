/*

Given the root of a binary tree, return a clone of it. The solution should return a new binary tree which is identical to the given binary tree in terms of its structure and contents, and it should not use the nodes of the binary tree.

Input:
           1
         /   \
        /	  \
       2	   3
      /	\	  / \
     /	 \ 	 /	 \
    4	  5	6	  7

Output:

           1
         /   \
        /	  \
       2	   3
      /	\	  / \
     /	 \ 	 /	 \
    4	  5	6	  7

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* clone(Node* root)
    {
        if (root == nullptr) return nullptr;

        Node* new_root = new Node{root->data};

        new_root->left = clone(root->left);
        new_root->right = clone(root->right);

        return new_root;
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    Node n4{4};
    Node n5{5};
    Node n2{2, &n4, &n5};
    Node n6{6};
    Node n7{7};
    Node n3{3, &n5, &n6};
    Node n1{1, &n2, &n3};

    Node* clone = Solution().clone(&n1);
    cout << *clone << endl;

    return 0;
}
