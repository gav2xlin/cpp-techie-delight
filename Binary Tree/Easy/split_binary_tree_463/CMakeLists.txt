cmake_minimum_required(VERSION 3.5)

project(split_binary_tree_463 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(split_binary_tree_463 main.cpp)

install(TARGETS split_binary_tree_463
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
