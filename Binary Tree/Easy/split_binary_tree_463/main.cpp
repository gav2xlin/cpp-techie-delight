/*

Given the root of a binary tree, check if removing an edge can split the binary tree into two binary trees of equal size.

Input:
           1
         /   \
        /	  \
       2	   3
      / \		\
     /	 \	 	 \
    4	  5		  7

Output: true
Explanation: Removing the edge 1 —> 2 from the binary tree splits it into two binary trees of size 3.

Input:
           1
         /   \
        /	  \
       2	   3
              / \
             /	 \
            5	  6
           / \
          /   \
         7	   8

Output: false
Explanation: There is no edge whose removal splits the binary tree into two equal-size binary trees.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int getSize(Node* root) {
        return root != nullptr ? 1 + getSize(root->left) + getSize(root->right) : 0;
    }

    bool checkSize(Node* root, int n)
    {
        if (root == nullptr) return false;

        if (getSize(root) == n / 2) {
            return true;
        }

        return checkSize(root->left, n) || checkSize(root->right, n);
    }

public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool splitBinaryTree(Node* root)
    {
        if (root == nullptr) return false;

        int n = getSize(root);

        return (n % 2 == 0) && (checkSize(root->left, n) || checkSize(root->right, n));
        //return (n % 2 == 0) && checkSize(root, n);
    }
};

int main()
{
    {
        Node n4{4};
        Node n5{5};
        Node n2{2, &n4, &n5};
        Node n7{7};
        Node n3{3, nullptr, &n7};
        Node n1{1, &n2, &n3};

        cout << boolalpha << Solution().splitBinaryTree(&n1) << endl;
    }

    {
        Node n2{2};
        Node n7{7};
        Node n8{8};
        Node n5{5, &n7, &n8};
        Node n6{6};
        Node n3{3, &n5, &n6};
        Node n1{1, &n2, &n3};

        cout << boolalpha << Solution().splitBinaryTree(&n1) << endl;
    }

    return 0;
}
