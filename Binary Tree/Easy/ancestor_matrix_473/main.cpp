/*

Given the root of a binary tree whose nodes are labeled from 0 to N-1, construct and return an N × N ancestor matrix from the binary tree. An ancestor matrix is a boolean matrix, whose cell (i, j) is true if i is an ancestor of j in the binary tree.

Input:
           4
         /   \
        /	  \
       3	   1
      / \		\
     /   \		 \
    2	  0		  5

Output:

[
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0],
    [1, 0, 1, 0, 0, 0],
    [1, 1, 1, 1, 0, 1],
    [0, 0, 0, 0, 0, 0]
]

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int getSize(Node* root) {
        if (root == nullptr) {
            return 0;
        }

        return getSize(root->left) + getSize(root->right) + 1;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void constructAncestorMatrix(Node* root, unordered_set<Node*> &ancestors, vector<vector<bool>> &ancestorMatrix) {
        if (root == nullptr) {
            return;
        }

        for (Node* node: ancestors) {
            ancestorMatrix[node->data][root->data] = true;
        }

        ancestors.insert(root);

        constructAncestorMatrix(root->left, ancestors, ancestorMatrix);
        constructAncestorMatrix(root->right, ancestors, ancestorMatrix);

        ancestors.erase(root);
    }

    // Function to construct an ancestor matrix from a given binary tree
    vector<vector<bool>> constructAncestorMatrix(Node* root) {
        int n = getSize(root);

        vector<vector<bool>> ancestorMatrix(n, vector<bool>(n));

        unordered_set<Node*> ancestors;
        constructAncestorMatrix(root, ancestors, ancestorMatrix);

        return ancestorMatrix;
    }
};

ostream& operator<<(ostream& os, const vector<vector<bool>>& matrix) {
    for(auto& r : matrix) {
        for(auto c : r) {
            os << c << ' ';
        }
        os << '\n';
    }
    return os;
}

int main()
{
    Node n2{2};
    Node n0{0};
    Node n3{3, &n2, &n0};
    Node n5{5};
    Node n1{1, nullptr, &n5};
    Node n4{4, &n3, &n1};

    cout << Solution().constructAncestorMatrix(&n4) << endl;

    return 0;
}
