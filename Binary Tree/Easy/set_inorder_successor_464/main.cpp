/*

Given the root of a special binary tree with each node containing an additional next pointer, set it to the inorder successor for all binary tree nodes.

Input:

             1(X)
            /	 \
           /	  \
          /		   \
        2(X)	  3(X)
        /		  /  \
       /		 /	  \
     4(X)	   5(X)  6(X)
               /  \
              /	   \
            7(X)  8(X)

Output:

             1(7)
            /	 \
           /	  \
          /		   \
        2(1)	  3(6)
        /		  /  \
       /		 /	  \
     4(2)	   5(8)  6(X)
               /  \
              /	   \
            7(5)  8(3)

Explanation:

• The inorder successor of node 4 is node 2
• The inorder successor of node 2 is node 1
• The inorder successor of node 1 is node 7
• The inorder successor of node 7 is node 5
• The inorder successor of node 5 is node 8
• The inorder successor of node 8 is node 3
• The inorder successor of node 3 is node 6
• The inorder successor of node 6 doesn't exist.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child
    Node* next = nullptr;						// next pointer

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right, Node *next): data(data), left(left), right(right), next(next) {}
};

class Solution
{
private:
    void setInorderSuccessorInternal(Node* root, vector<Node*>& nodes) {
        if (root != nullptr) {
            setInorderSuccessorInternal(root->left, nodes);
            nodes.push_back(root);
            setInorderSuccessorInternal(root->right, nodes);
        }
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child
            Node* next = nullptr;						// next pointer

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right, Node *next): data(data), left(left), right(right), next(next) {}
        };
    */

    void setInorderSuccessor(Node* root)
    {
        if (root != nullptr) {
            vector<Node*> nodes;
            setInorderSuccessorInternal(root, nodes);

            Node* prev{nullptr};
            for (auto& node : nodes) {
                if (prev != nullptr) {
                    prev->next = node;
                }
                prev = node;
            }
        }
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << '(';

                if (root->next != nullptr) {
                    os << root->next->data;
                } else {
                    os << "X";
                }

                os << ')' << endl;

                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    Node n4{4};
    Node n2{2, &n4, nullptr, nullptr};
    Node n7{7};
    Node n8{8};
    Node n5{5, &n7, &n8, nullptr};
    Node n6{6};
    Node n3{3, &n5, &n6, nullptr};
    Node n1{1, &n2, &n3, nullptr};

    Solution().setInorderSuccessor(&n1);

    cout << n1 << endl;

    return 0;
}
