/*

Given the root of a binary tree and two positive numbers m and n where m >= n, return values of all nodes between level m and level n. The nodes for each level should be processed from left and right.

Input: Below binary tree, m = 2, n = 3

           1
         /   \
        /	  \
       2	   3
              / \
             /	 \
            4	  5
           / \	   \
          /   \		\
         6	   7	 8

Output: [2, 3, 4, 5]

Note: If n is more than the number of levels in the binary tree, the solution return nodes till last level. For example, if the starting level is 2 and the ending level is 7, the solution should return [2, 3, 4, 5, 6, 7, 8] for above binary tree.

*/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findNodes(Node* root, int m, int n)
    {
        vector<int> out;
        if (root == nullptr) return out;

        queue<Node*> q;
        q.push(root);

        int height = 0;

        while (!q.empty()) {
            int size = q.size();
            ++height;

            while (size--) {
                Node* node = q.front();
                q.pop();

                if (m <= height && height <= n) {
                    out.push_back(node->data);
                }

                if (node->left) {
                    q.push(node->left);
                }
                if (node->right) {
                    q.push(node->right);
                }
            }
        }

        return out;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node n6{6};
    Node n7{7};
    Node n4{4, &n6, &n7};
    Node n8{8};
    Node n5{5, nullptr, &n8};
    Node n2{2};
    Node n3{3, &n4, &n5};
    Node n1{1, &n2, &n3};

    cout << Solution().findNodes(&n1, 2, 3) << endl;

    return 0;
}
