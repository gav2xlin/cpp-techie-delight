/*

Given the root of a binary tree, determine if the binary tree holds children-sum property. For a tree to satisfy the children-sum property, each node's value should be equal to the sum of values at its left and right subtree. The value of an empty node is considered as 0.

Input:

          25
        /	 \
       /	  \
      /		   \
     12		   13
    /  \	  /  \
   /	\	 /	  \
  7		 5	6	   7

Output: true

Explanation: All non-leaf nodes follows the children-sum property, as shown below:

          25 (12+13)
        /	 \
       /	  \
      /		   \
     12 (7+5)  13 (6+7)
    /  \	  /  \
   /	\	 /	  \
  7		 5	6	   7

*/


#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool checkChildrenSumProperty(Node* root)
    {
        if (root == nullptr) return true;

        if (root->left != nullptr || root->right != nullptr) {
            int leftSum = root->left != nullptr ? root->left->data : 0;
            int rightSum = root->right != nullptr ? root->right->data : 0;

            if (root->data != leftSum + rightSum) {
                return false;
            }
        }

        bool isLeft = checkChildrenSumProperty(root->left);
        bool isRight = checkChildrenSumProperty(root->right);

        return isLeft && isRight;
    }
};

int main()
{
    Node n12_7{7};
    Node n12_5{5};
    Node n12{12, &n12_7, &n12_5};
    Node n13_6{6};
    Node n13_7{7};
    Node n13{13, &n13_6, &n13_7};
    Node n25{25, &n12, &n13};

    cout << boolalpha << Solution().checkChildrenSumProperty(&n25) << endl;

    return 0;
}
