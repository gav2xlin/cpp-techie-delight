/*

Given the root of a binary tree, check if the binary tree has a symmetric structure or not, i.e., left and right subtree mirror each other.

Input:
           1
         /   \
        /	  \
       2	   3
        \	  /
         \	 /
          4	5

Output: true

Input:
           1
         /   \
        /	  \
       2	   3
      / \	  / \
     /   \	 /	 \
    7	  8	5	  6

Output: true

Input:
           1
         /   \
        /	  \
       2	   3
      /		  /
     /		 /
    7		5

Output: false

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isSymmetric(Node* left, Node* right)
    {
        if (left != nullptr && right != nullptr) {
            return isSymmetric(left->left, right->right) && isSymmetric(left->right, right->left);
        } else {
            return left == nullptr && right == nullptr;
        }
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isSymmetric(Node* root)
    {
        if (root == nullptr) return true;

        return isSymmetric(root->left, root->right);
    }
};

int main()
{
    {
        Node n4{4};
        Node n2{2, nullptr, &n4};
        Node n5{5};
        Node n3{3, &n5, nullptr};
        Node n1{1, &n2, &n3};

        cout << boolalpha << Solution().isSymmetric(&n1) << endl;
    }

    {
        Node n7{7};
        Node n8{8};
        Node n2{2, &n7, &n8};
        Node n5{5};
        Node n6{6};
        Node n3{3, &n5, &n6};
        Node n1{1, &n2, &n3};

        cout << boolalpha << Solution().isSymmetric(&n1) << endl;
    }

    {
        Node n7{7};
        Node n2{2, &n7, nullptr};
        Node n5{5};
        Node n3{3, &n5, nullptr};
        Node n1{1, &n2, &n3};

        cout << boolalpha << Solution().isSymmetric(&n1) << endl;
    }

    {
        Node n4{4};
        Node n2{2, &n4, nullptr};
        Node n5{5};
        Node n3{3, nullptr, &n5};
        Node n1{1, &n2, &n3};

        cout << boolalpha << Solution().isSymmetric(&n1) << endl;
    }

    return 0;
}
