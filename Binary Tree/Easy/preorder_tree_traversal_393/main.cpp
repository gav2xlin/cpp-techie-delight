/*

Given the root of a binary tree, return the preorder traversal of its nodes' values.

Input:
           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4		5	  6
           / \
          /   \
         7	   8

Output: [1, 2, 4, 3, 5, 7, 8, 6]

*/

#include <iostream>
#include <vector>

using namespace std;

struct Node {
    int data;
    Node *left{nullptr}, *right{nullptr};
};

class Solution
{
private:
    void findPreorderTraversalInternal(Node* node, vector<int>& nums) {
        if (node == nullptr) return;
        nums.push_back(node->data);
        findPreorderTraversalInternal(node->left, nums);
        findPreorderTraversalInternal(node->right, nums);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findPreorderTraversal(Node* root)
    {
        vector<int> nums;
        findPreorderTraversalInternal(root, nums);
        return nums;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node n4{4};
    Node n2{2, &n4};
    Node n7{7};
    Node n8{8};
    Node n5{5, &n7, &n8};
    Node n6{6};
    Node n3{3, &n5, &n6};
    Node n1{1, &n2, &n3};

    cout << Solution().findPreorderTraversal(&n1) << endl;

    return 0;
}
