/*

Given the root of a binary tree, return the reverse level order traversal of its nodes' values. The solution should consider the binary tree nodes level by level in bottom-up order from left to right, i.e., process all nodes of the last level first, followed by all nodes of the second last level, and so on.

Input:
           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4		5	  6
           / \
          /   \
         7	   8

Output: [7, 8, 4, 5, 6, 2, 3, 1]

*/

#include <iostream>
#include <vector>

using namespace std;

struct Node {
    int data;
    Node *left{nullptr}, *right{nullptr};
};

class Solution
{
private:
    void findReverseLevelOrderTraversalInternal(Node* node, vector<vector<int>>& nums, int level) {
        if (node == nullptr) return;
        if (level == nums.size()) nums.resize(level + 1);

        nums[level].push_back(node->data);
        findReverseLevelOrderTraversalInternal(node->left, nums, level + 1);
        findReverseLevelOrderTraversalInternal(node->right, nums, level + 1);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findReverseLevelOrderTraversal(Node* root)
    {
        vector<vector<int>> nums;
        findReverseLevelOrderTraversalInternal(root, nums, 0);

        vector<int> res;
        for (auto it = nums.rbegin(); it != nums.rend(); ++it) {
            for (auto v : *it) {
                res.push_back(v);
            }
        }
        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node n4{4};
    Node n2{2, &n4};
    Node n7{7};
    Node n8{8};
    Node n5{5, &n7, &n8};
    Node n6{6};
    Node n3{3, &n5, &n6};
    Node n1{1, &n2, &n3};

    cout << Solution().findReverseLevelOrderTraversal(&n1) << endl;

    return 0;
}
