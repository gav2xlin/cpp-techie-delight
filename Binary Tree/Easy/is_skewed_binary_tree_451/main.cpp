/*

Given the root of a binary tree, check if each node has exactly one child or not. In other words, check whether the binary tree is skewed or not.

Input:
          1
         /
        /
       2
      /
     /
    3

Output: true

Input:
           1
         /   \
        /	  \
       2	   3

Output: false

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */
    /*int getSize(Node* root) {
        return root != nullptr ? 1 + getSize(root->left) + getSize(root->right) : 0;
    }

    int getHeight(Node* root) {
        return root != nullptr ? 1 + max(getHeight(root->left), getHeight(root->right)) : 0;
    }*/

    bool isSkewed(Node* root)
    {
        if (root == nullptr) return true;

        if (root->left != nullptr && root->right != nullptr) {
            return false;
        } else if (root->left == nullptr && root->right == nullptr) {
            return true;
        } else {
            return isSkewed(root->left) && isSkewed(root->right);
        }
        //return getSize(root) == getHeight(root);
    }
};

int main()
{
    {
        Node n3{3};
        Node n2{2, &n3, nullptr};
        Node n1{1, &n2, nullptr};

        cout << boolalpha << Solution().isSkewed(&n1) << endl;
    }

    {
        Node n3{3};
        Node n2{2};
        Node n1{1, &n2, &n3};

        cout << boolalpha << Solution().isSkewed(&n1) << endl;
    }

    return 0;
}
