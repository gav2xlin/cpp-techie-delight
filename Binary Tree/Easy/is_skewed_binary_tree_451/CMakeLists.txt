cmake_minimum_required(VERSION 3.5)

project(is_skewed_binary_tree_451 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(is_skewed_binary_tree_451 main.cpp)

install(TARGETS is_skewed_binary_tree_451
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
