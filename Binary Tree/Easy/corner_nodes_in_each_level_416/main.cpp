/*

Given the root of a binary tree, return corner nodes' values for each level in it.

Input:
           1
         /   \
        /	  \
       2	   3
              / \
             /	 \
            4	  5
           / \	   \
          /   \		\
         6	   7	 8

Output: [1, 2, 3, 4, 5, 6, 8]

*/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findCornerNodes(Node* root)
    {
        vector<int> out;
        if (root == nullptr) return out;

        queue<Node*> q;
        q.push(root);

        while (!q.empty()) {
            int size = q.size(), high = size - 1;

            while (size--) {
                Node* node = q.front();
                q.pop();

                if (size == high || size == 0) {
                    out.push_back(node->data);
                }

                if (node->left) {
                    q.push(node->left);
                }
                if (node->right) {
                    q.push(node->right);
                }
            }
        }

        return out;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node n6{6};
    Node n7{7};
    Node n4{4, &n6, &n7};
    Node n8{8};
    Node n5{5, nullptr, &n8};
    Node n2{2};
    Node n3{3, &n4, &n5};
    Node n1{1, &n2, &n3};

    cout << Solution().findCornerNodes(&n1) << endl;

    return 0;
}
