/*

Given two integer arrays representing inorder and postorder traversal of a binary tree, return the preorder traversal of the corresponding binary tree.

Input:

inorder[]   = [4, 2, 1, 7, 5, 8, 3, 6]
postorder[] = [4, 2, 7, 8, 5, 6, 3, 1]

Output: [1, 2, 4, 3, 5, 7, 8, 6]

Explanation: The inorder and postorder traversal represents the following binary tree.

           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4		5	  6
           / \
          /   \
         7	   8

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <stack>

using namespace std;

class Solution
{
private:
    void printPreorder(int start, int end,
                    vector<int> const &postorder, int &pIndex,
                    unordered_map<int, int> &map, stack<int> &stack)
    {
        if (start > end) {
            return;
        }

        int value = postorder[pIndex--];
        int index = map[value];

        printPreorder(index + 1, end, postorder, pIndex, map, stack);
        printPreorder(start, index - 1, postorder, pIndex, map, stack);

        stack.push(value);
    }
public:
    vector<int> findPreorder(vector<int> const &inorder, vector<int> const &postorder)
    {
        unordered_map<int, int> map;

        for (int i = 0; i < inorder.size(); ++i) {
            map[inorder[i]] = i;
        }

        int lastIndex = inorder.size() - 1;
        stack<int> stack;

        printPreorder(0, lastIndex, postorder, lastIndex, map, stack);

        vector<int> res;
        while (!stack.empty())
        {
            res.push_back(stack.top());
            stack.pop();
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> inorder {4, 2, 1, 7, 5, 8, 3, 6};
    vector<int> postorder {4, 2, 7, 8, 5, 6, 3, 1};

    cout << Solution().findPreorder(inorder, postorder) << endl;

    return 0;
}
