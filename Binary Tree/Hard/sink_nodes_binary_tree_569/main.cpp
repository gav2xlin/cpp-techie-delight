/*

Given the root of a binary tree containing many zero nodes, sink nodes having zero value at the bottom of the subtree rooted at that node. In other words, the resultant binary tree should not contain any node having zero value that is the parent of the node having a non-zero value.

Since several binary tree might satisfy the constraints, the solution should return any one of them.

Input:

       0
     /   \
    /	  \
   1	   0

Output:

       1
     /   \
    /	  \
   0	   0

Input:

       0
     /	 \
    /	  \
   /	   \
  1			0
           / \
          /	  \
         0	   2
        / \
       /   \
      3		4

Output:

       1
     /	 \
    /	  \
   /	   \
  0			3
           / \
          /	  \
         4	   2
        / \
       /   \
      0		0

or any other valid binary tree.

*/

#include <iostream>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void sink(Node* root)
    {
        if (root == nullptr) {
            return;
        }

        if (root->left && root->left->data != 0)
        {
            swap(root->data, root->left->data);

            sink(root->left);
        }
        else if (root->right && root->right->data != 0)
        {
            swap(root->data, root->right->data);

            sink(root->right);
        }
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void sinkNodes(Node* &root)
    {
        if (root == nullptr) {
            return;
        }

        sinkNodes(root->left);
        sinkNodes(root->right);

        if (root->data == 0) {
            sink(root);
        }
    }
};

void inorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorder(root->left);
    cout << root->data << " ";
    inorder(root->right);
}

int main()
{
    Node* root = new Node(0);
    root->left = new Node(1);
    root->right = new Node(0);
    root->right->left = new Node(0);
    root->right->right = new Node(2);
    root->right->left->left = new Node(3);
    root->right->left->right = new Node(4);

    Solution().sinkNodes(root);

    inorder(root);

    return 0;
}
