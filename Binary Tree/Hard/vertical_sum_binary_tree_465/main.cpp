/*

Given the root of a binary tree, return the sum of all nodes for each vertical level. In a vertical traversal, nodes of a binary tree are processed in vertical order from left to right. Assume that the left and right child makes a 45–degree angle with the parent.

Input:
           1
         /	 \
        /	  \
       2	   3
             /   \
            /	  \
           5	   6
         /   \
        /	  \
       7	   8

Output: [9, 6, 11, 6]

Explanation: The binary tree has four vertical levels:

[2, 7]
[1, 5]
[3, 8]
[6]

*/

#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    /*void findVerticalSum(Node* root, int dist, auto &map)
    {
        if (root == nullptr) {
            return;
        }

        map[dist] += root->data;

        findVerticalSum(root->left, dist - 1, map);
        findVerticalSum(root->right, dist + 1, map);
    }*/

    struct ListNode
    {
        int data;
        ListNode *prev, *next;

        ListNode(int data, ListNode* prev, ListNode* next)
        {
            this->data = data;
            this->prev = prev;
            this->next = next;
        }
    };

    void updateDLLwithVerticalSum(Node* root, ListNode* curr)
    {
        if (!root) {
            return;
        }

        curr->data += root->data;

        if (root->left && !curr->prev) {
            curr->prev = new ListNode(0, nullptr, curr);
        }

        if (root->right && !curr->next) {
            curr->next = new ListNode(0, curr, nullptr);
        }

        updateDLLwithVerticalSum(root->left, curr->prev);
        updateDLLwithVerticalSum(root->right, curr->next);
    }

    void fill(ListNode* mid, vector<int> &res)
    {
        while (mid && mid->prev) {
            mid = mid->prev;
        }

        ListNode* head = mid;
        while (head)
        {
            res.push_back(head->data);
            head = head->next;
        }
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    /*vector<int> findVerticalSum(Node* root)
    {
        map<int, int> map;

        findVerticalSum(root, 0, map);

        vector<int> res;
        res.reserve(map.size());

        for (auto it: map) {
            res.push_back(it.second);
        }

        return res;
    }*/
    vector<int> findVerticalSum(Node* root)
    {
        if (root == nullptr) {
            return {};
        }

        ListNode* curr = new ListNode(0, nullptr, nullptr);

        updateDLLwithVerticalSum(root, curr);

        vector<int> res;
        fill(curr, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    cout << Solution().findVerticalSum(root) << endl;

    return 0;
}
