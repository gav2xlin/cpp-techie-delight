/*

Given the root of a binary tree and a positive number k, return all nodes present at distance k from any leaf node. The solution should report only those nodes that are present in the root-to-leaf path for that leaf.

For example, consider the following binary tree.

                1
              /   \
            /		\
          /			  \
         2			   3
       /   \		 /   \
      /		\		/	  \
     4		 5	   6	   7
                 /
                /
               8

Input: k = 1
Output: {2, 6, 3}

Input: k = 2
Output: {1, 3}

Input: k = 3
Output: {1}

Note: If k is more than the number of levels in the binary tree, the solution return an empty set.

*/

#include <iostream>
#include <unordered_set>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isLeaf(Node* node) {
        return (node->left == nullptr && node->right == nullptr);
    }

    void leafNodeDistance(Node* node, vector<Node*> path, unordered_set<Node*> &set, int dist)
    {
        if (node == nullptr) {
            return;
        }

        if (isLeaf(node) && path.size() >= dist)
        {
            set.insert(path.at(path.size() - dist));
            return;
        }

        path.push_back(node);

        leafNodeDistance(node->left, path, set, dist);
        leafNodeDistance(node->right, path, set, dist);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    unordered_set<Node*> findNodes(Node* node, int dist)
    {
        vector<Node*> path;

        unordered_set<Node*> set;

        leafNodeDistance(node, path, set, dist);

        return set;
    }
};

ostream& operator<<(ostream& os, const unordered_set<Node*>& values) {
    for (auto& v : values) {
        os << v->data << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(15);
    root->left = new Node(10);
    root->right = new Node(20);
    root->left->left = new Node(8);
    root->left->right = new Node(12);
    root->right->left = new Node(16);
    root->right->right = new Node(25);
    root->right->left->left = new Node(18);

    cout << Solution().findNodes(root, 1) << endl;

    return 0;
}
