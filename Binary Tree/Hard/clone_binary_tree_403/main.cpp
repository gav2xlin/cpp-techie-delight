/*

Given the root of a special binary tree with each node containing an additional random pointer, return a clone of it. The random pointer can point to any node of the binary tree or can be null.

The solution should return a new binary tree which is identical to the given binary tree in terms of its structure and contents, and it should not use the nodes of the binary tree.

Input:
              1(6)
            /	   \
          /			 \
        2(X)		 3(X)
       /   \		/   \
      /		\	   /	 \
    4(3)   5(1)  6(4)	 7(X)


Here, random pointer of:

• Node 1 points to Node 6
• Node 2 points to nullptr
• Node 3 points to nullptr
• Node 4 points to Node 3
• Node 5 points to Node 1
• Node 6 points to Node 4
• Node 7 points to nullptr

Output:

              1(6)
            /	   \
          /			 \
        2(X)		 3(X)
       /   \		/   \
      /		\	   /	 \
    4(3)   5(1)  6(4)	 7(X)

*/

#include <iostream>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child
    Node* random = nullptr;						// random pointer

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right, Node *random): data(data), left(left), right(right), random(random) {}
};

class Solution
{
private:
    void updateRandom(Node* root, unordered_map<Node*, Node*> &map)
    {
        if (map[root] == nullptr) {
            return;
        }

        map[root]->random = map[root->random];

        updateRandom(root->left, map);
        updateRandom(root->right, map);
    }

    Node* cloneLeftRight(Node* root, unordered_map<Node*, Node*> &map)
    {
        if (root == nullptr) {
            return nullptr;
        }

        map[root] = new Node(root->data);

        map[root]->left = cloneLeftRight(root->left, map);
        map[root]->right = cloneLeftRight(root->right, map);

        return map[root];
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child
            Node* random = nullptr;						// random pointer

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right, Node *random): data(data), left(left), right(right), random(random) {}
        };
    */

    Node* clone(Node* root)
    {
        if (root == nullptr) {
            return nullptr;
        }

        unordered_map<Node*, Node*> map;

        cloneLeftRight(root, map);

        updateRandom(root, map);

        return map[root];
    }
};

void preorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    cout << root->data << " ——> (";

    (root->left) ? cout << root->left->data    << ", " : cout << "X" << ", ";
    (root->right) ? cout << root->right->data << ", " : cout << "X" << ", ";
    (root->random) ? cout << root->random->data << ")\n": cout << "X" << ")\n";

    preorder(root->left);
    preorder(root->right);
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    root->left->left->random = root->right;
    root->left->right->random = root;
    root->right->left->random = root->left->left;
    root->random = root->left;

    cout << "Preorder traversal of the original tree:\n";
    preorder(root);

    Node* clone = Solution().clone(root);

    cout << "\nPreorder traversal of the cloned tree:\n";
    preorder(clone);

    return 0;
}
