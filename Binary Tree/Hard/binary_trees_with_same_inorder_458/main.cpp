/*

Given an integer array representing inorder sequence of a binary tree, return count of all possible binary trees having that same inorder traversal.

Input: [1, 2, 3]
Output: 5
Explanation: There are 5 binary trees with inorder traversal [1, 2, 3], as shown below:

  1			1		  2			3		   3
   \		 \		 / \	   /		  /
    2		  3		1   3	  1		  	 2
     \		 /				   \		/
      3		2					2	   1

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    struct Node
    {
        int data;
        Node *left, *right;

        Node(int data, Node *left, Node *right)
        {
            this->data = data;
            this->left = left;
            this->right = right;
        }
    };

    vector<Node*> generateBinaryTrees(vector<int> const &in, int start, int end)
    {
        vector<Node*> trees;

        if (start > end)
        {
            trees.push_back(nullptr);
            return trees;
        }

        for (int i = start; i <= end; ++i)
        {
            vector<Node*> left_subtrees = generateBinaryTrees(in, start, i - 1);
            vector<Node*> right_subtrees = generateBinaryTrees(in, i + 1, end);

            for (Node* l: left_subtrees)
            {
                for (Node* r: right_subtrees)
                {
                    Node* tree = new Node(in[i], l, r);

                    trees.push_back(tree);
                }
            }
        }

        return trees;
    }

    void preorder(Node* root)
    {
        if (root == nullptr) {
            return;
        }

        cout << root->data << ' ';
        preorder(root->left);
        preorder(root->right);
    }
public:
    int countBinaryTrees(vector<int> const &inorder)
    {
        vector<Node*> trees = generateBinaryTrees(inorder, 0, inorder.size() - 1);

        /*for (Node* tree: trees)
        {
            preorder(tree);
            cout << endl;
        }*/

        return trees.size();
    }
};

int main()
{
    vector<int> inorder = {1, 2, 3};

    cout << Solution().countBinaryTrees(inorder) << endl;

    return 0;
}
