/*

Given the root of two binary trees, check whether the leaf traversals of both trees are the same or not.

Input:
           1						1
         /   \					  /   \
        /	  \					 /	   \
       2	   3				2		3
      /	\	  /					 \	   / \
     /	 \ 	 /				  	  \	  /	  \
    4	  5	6				 	   4 5	   6

Output: true
Explanation: The leaf traversal of both binary trees is [4, 5, 6].

Input:
           1						1
         /   \					  /   \
        /	  \					 /	   \
       2	   3				2		3
      /	\					   		   / \
     /	 \			  				  /	  \
    4	  5							 4	   5

Output: false
Explanation: Both binary trees have different leaf traversals.

*/

#include <iostream>
#include <stack>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    // iterative
    /*bool isLeaf(Node* node) {
        return node->left == nullptr && node->right == nullptr;
    }

    Node* getNextLeafNode(stack<Node*> &s)
    {
        Node* curr = s.top();
        s.pop();

        while (curr != nullptr && !isLeaf(curr))
        {
            if (curr->right != nullptr) {
                s.push(curr->right);
            }

            if (curr->left != nullptr) {
                s.push(curr->left);
            }

            curr = s.top();
            s.pop();
        }

        return curr;
    }*/
    bool isLeaf(Node* node) {
        return node->left == nullptr && node->right == nullptr;
    }

    void connectLeafNodes(Node* root, Node* &head, Node* &prev)
    {
        if (root == nullptr) {
            return;
        }

        if (isLeaf(root))
        {
            if (prev == nullptr) {
                head = root;
            }
            else {
                prev->right = root;
            }
            prev = root;
        }

        connectLeafNodes(root->left, head, prev);
        connectLeafNodes(root->right, head, prev);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    // iterative
    /*bool isLeafTraversalSame(Node* x, Node* y)
    {
        stack<Node*> first;
        stack<Node*> second;

        first.push(x);
        second.push(y);

        while (!first.empty() && !second.empty())
        {
            Node* xLeaf = getNextLeafNode(first);
            Node* yLeaf = getNextLeafNode(second);

            if (xLeaf == nullptr && yLeaf == nullptr) {
                return true;
            }

            if (xLeaf == nullptr || yLeaf == nullptr || xLeaf->data != yLeaf->data) {
                return false;
            }
        }

        return first.empty() && second.empty();
    }*/
    bool isLeafTraversalSame(Node* x, Node* y)
    {
        Node* first = nullptr;
        Node* prev = nullptr;
        connectLeafNodes(x, first, prev);

        Node* second = nullptr;
        prev = nullptr;
        connectLeafNodes(y, second, prev);

        while (first && second && first->data == second->data)
        {
            first = first->right;
            second = second->right;
        }

        return !first && !second;
    }
};

int main()
{
    Node* x = new Node(1);
    x->left = new Node(2);
    x->right = new Node(3);
    x->left->left = new Node(4);
    x->left->right = new Node(5);
    x->right->left = new Node(6);

    Node* y = new Node(1);
    y->left = new Node(2);
    y->right = new Node(3);
    y->left->right = new Node(4);
    y->right->left = new Node(5);
    y->right->right = new Node(6);

    cout << boolalpha << Solution().isLeafTraversalSame(x, y) << endl;

    return 0;
}
