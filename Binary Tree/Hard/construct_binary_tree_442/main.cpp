/*

Given an integer array representing preorder traversal of a binary tree, and a boolean array that determines if the value at the corresponding index in the preorder sequence is a leaf node or an internal node, construct and return the full binary tree. A full binary tree is a tree in which every node has either 0 or 2 children.

Input:

preorder[] = [1, 2, 4, 5, 3, 6, 8, 9, 7]
isLeaf[]   = [0, 0, 1, 1, 0, 0, 1, 1, 1]	 (1 represents a leaf node, and 0 represents an internal node)

Output: Root of below binary tree

           1
         /   \
        /	  \
       2	   3
      /	\	  / \
     /	 \ 	 /	 \
    4	  5	6	  7
           / \
          /   \
         8	   9

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node *construct(vector<int> const &preorder, vector<bool> const &isLeaf, int &pIndex)
    {
        if (pIndex == preorder.size()) {
            return nullptr;
        }

        Node* node = new Node(preorder[pIndex]);
        bool isInternalNode = !isLeaf[pIndex];
        ++pIndex;

        if (isInternalNode)
        {
            node->left = construct(preorder, isLeaf, pIndex);
            node->right = construct(preorder, isLeaf, pIndex);
        }

        return node;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBinaryTree(vector<int> const &preorder, vector<bool> const &isLeaf)
    {
        int pIndex = 0;
        return construct(preorder, isLeaf, pIndex);
    }
};

void preorderTraversal(Node* root)
{
    if (root == nullptr) {
        return;
    }

    cout << root->data << ' ';
    preorderTraversal(root->left);
    preorderTraversal(root->right);
}

int main()
{
    vector<int> preorder = { 1, 2, 4, 5, 3, 6, 8, 9, 7 };
    vector<bool> isLeaf = { 0, 0, 1, 1, 0, 0, 1, 1, 1 };

    Node* root = Solution().constructBinaryTree(preorder, isLeaf);

    cout << "Preorder traversal of the constructed tree is ";
    preorderTraversal(root);
    cout << endl;

    return 0;
}
