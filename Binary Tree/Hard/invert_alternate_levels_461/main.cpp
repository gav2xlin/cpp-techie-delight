/*

Given the root of a perfect binary tree, invert alternate levels of it.

Input:
                    1
                  /	   \
               /		 \
            /			   \
          2					 3
        /   \				/  \
      /		  \			  /		 \
     4		   5		 6		  7
    / \		  / \		/ \		 / \
   /   \	 /   \	   /   \	/   \
  8		9   10   11   12   13  14   15

Output:
                    1
                  /	   \
               /		 \
            /			   \
          3					 2
        /   \				/  \
      /		  \			  /		 \
     4		   5		 6		  7
    / \		  / \		/ \		 / \
   /   \	 /   \	   /   \	/   \
  15   14   13   12   11   10  9	 8

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <utility>
#include <queue>
#include <stack>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    // inorder 1
    /*void pushOddLevelNodes(Node* root, stack<int> &s, bool level)
    {
        if (root == nullptr) {
            return;
        }

        pushOddLevelNodes(root->left, s, !level);

        if (level) {
            s.push(root->data);
        }

        pushOddLevelNodes(root->right, s, !level);
    }

    void invertBinaryTree(Node* root, stack<int> &s, bool level)
    {
        if (root == nullptr) {
            return;
        }

        invertBinaryTree(root->left, s, !level);

        if (level)
        {
            root->data = s.top();
            s.pop();
        }

        invertBinaryTree(root->right, s, !level);
    }*/

    // inorder 2
    /*void pushOddLevelNodes(Node* root, queue<int> &q, bool level)
    {
        if (root == nullptr) {
            return;
        }

        pushOddLevelNodes(root->right, q, !level);

        if (level) {
            q.push(root->data);
        }

        pushOddLevelNodes(root->left, q, !level);
    }

    void invertBinaryTree(Node* root, queue<int> &q, bool level)
    {
        if (root == nullptr) {
            return;
        }

        invertBinaryTree(root->left, q, !level);

        if (level)
        {
            root->data = q.front();
            q.pop();
        }

        invertBinaryTree(root->right, q, !level);
    }*/

    // preorder
    void invertBinaryTree(Node* first, Node* second, bool level)
    {
        if (!first || !second) {
            return;
        }

        if (level) {
            swap(first->data, second->data);
        }

        invertBinaryTree(first->left, second->right, !level);
        invertBinaryTree(first->right, second->left, !level);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    // level order
    /*void invertAlternateLevels(Node* root)
    {
        if (root == nullptr) {
            return;
        }

        queue<Node*> q;
        q.push(root);

        bool level = false;

        queue<Node*> level_nodes;

        stack<int> level_data;

        while (!q.empty())
        {
            int n = q.size();

            while (n--)
            {
                Node* curr = q.front();
                q.pop();

                if (level)
                {
                    level_nodes.push(curr);

                    level_data.push(curr->data);
                }

                if (n == 0)
                {
                    level = !level;

                    while (!level_nodes.empty())
                    {
                        Node* front = level_nodes.front();
                        front->data = level_data.top();

                        level_nodes.pop();
                        level_data.pop();
                    }
                }

                if (curr->left) {
                    q.push(curr->left);
                }

                if (curr->right) {
                    q.push(curr->right);
                }
            }
        }
    }*/

    // inorder 1
    /*void invertAlternateLevels(Node* root)
    {
        stack<int> s;
        pushOddLevelNodes(root, s, false);

        invertBinaryTree(root, s, false);
    }*/

    // inorder 2
    /*void invertAlternateLevels(Node* root)
    {
        queue<int> q;
        pushOddLevelNodes(root, q, false);

        invertBinaryTree(root, q, false);
    }*/

    // preorder
    void invertAlternateLevels(Node* root)
    {
        if (root == nullptr) {
            return;
        }

        invertBinaryTree(root->left, root->right, true);
    }
};

void levelOrderTraversal(Node* root)
{
    if (root == nullptr) {
        return;
    }

    queue<Node*> queue;
    queue.push(root);

    Node* curr = nullptr;

    while (queue.size())
    {
        curr = queue.front();
        queue.pop();

        cout << curr->data << " ";

        if (curr->left) {
            queue.push(curr->left);
        }

        if (curr->right) {
            queue.push(curr->right);
        }
    }
    cout << endl;
}

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);
    root->left->left->left = new Node(8);
    root->left->left->right = new Node(9);
    root->left->right->left = new Node(10);
    root->left->right->right = new Node(11);
    root->right->left->left = new Node(12);
    root->right->left->right = new Node(13);
    root->right->right->left = new Node(14);
    root->right->right->right = new Node(15);

    cout << "before:\n" << *root << endl;
    levelOrderTraversal(root);

    Solution().invertAlternateLevels(root);

    cout << "after:\n" << *root << endl;
    levelOrderTraversal(root);

    return 0;
}
