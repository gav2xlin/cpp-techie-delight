cmake_minimum_required(VERSION 3.5)

project(invert_alternate_levels_461 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(invert_alternate_levels_461 main.cpp)

install(TARGETS invert_alternate_levels_461
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
