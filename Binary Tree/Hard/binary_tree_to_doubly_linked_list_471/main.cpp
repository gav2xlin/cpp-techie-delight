/*

Given the root of a binary tree, in-place convert the binary tree into a doubly-linked list in the spiral order.

The conversion should be done so that the left child pointer of a binary tree node should act as a previous pointer for a doubly-linked list node, and the right child pointer should act as the next pointer for a doubly-linked list node. The conversion should also be done by only exchanging the pointers without allocating any memory for the doubly linked list's nodes.

Input:
             1
           /   \
         /		 \
        2		  3
       / \		 / \
      /	  \		/	\
     4	   5   6	 7

Output: 1 ⇔ 2 ⇔ 3 ⇔ 7 ⇔ 6 ⇔ 5 ⇔ 4

*/

#include <iostream>
#include <unordered_map>
#include <list>
#include <stack>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

/*class Solution
{
private:
    void push(Node* node, Node* &headRef)
    {
        if (headRef == nullptr)
        {
            headRef = node;
            headRef->left = headRef->right = nullptr;
            return;
        }

        headRef->left = node;
        node->right = headRef;

        node->left = nullptr;

        headRef = node;
    }

    void preorder(Node* root, int level, auto &map)
    {
        if (root == nullptr) {
            return;
        }

        if (level & 1) {
            map[level].push_front(root);
        }
        else {
            map[level].push_back(root);
        }

        preorder(root->left, level + 1, map);
        preorder(root->right, level + 1, map);
    }
public:*/

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    /*void binaryTreeToDoublyLinkedList(Node* root)
    {
        unordered_map<int, list<Node*>> map;

        preorder(root, 0, map);

        int n = map.size();
        Node* head = nullptr;
        for (int i = n - 1; i >=0; --i)
        {
            for (Node* node: map[i]) {
                push(node, head);
            }
        }
    }
};*/

class Solution
{
private:
    void push(Node* node, Node* &headRef)
    {
        if (headRef == nullptr)
        {
            headRef = node;
            headRef->left = headRef->right = nullptr;
            return;
        }

        headRef->left = node;
        node->right = headRef;

        node->left = nullptr;

        headRef = node;
    }
public:
    void binaryTreeToDoublyLinkedList(Node* root)
    {
        if (root == nullptr) {
            return;
        }

        list<Node*> deque;
        deque.push_front(root);

        bool flag = false;

        stack<Node*> s;

        while (!deque.empty())
        {
            int nodeCount = deque.size();

            if (flag)
            {
                while (nodeCount)
                {
                    Node* curr = deque.front();
                    deque.pop_front();

                    if (curr->left != nullptr) {
                        deque.push_back(curr->left);
                    }

                    if (curr->right != nullptr) {
                        deque.push_back(curr->right);
                    }

                    s.push(curr);
                    --nodeCount;
                }
            }
            else
            {
                while (nodeCount)
                {
                    Node* curr = deque.back();
                    deque.pop_back();

                    if (curr->right != nullptr) {
                        deque.push_front(curr->right);
                    }

                    if (curr->left != nullptr) {
                        deque.push_front(curr->left);
                    }

                    s.push(curr);
                    nodeCount--;
                }
            }

            flag = !flag;
        }

        Node* head = nullptr;
        while (!s.empty())
        {
            push(s.top(), head);
            s.pop();
        }
    }
};

void printDoublyLinkedList(Node* ptr)
{
    while (ptr != nullptr)
    {
        cout << ptr->data << " —> ";
        ptr = ptr->right;
    }
    cout << "nullptr" << endl;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    Solution().binaryTreeToDoublyLinkedList(root);

    printDoublyLinkedList(root);

    return 0;
}
