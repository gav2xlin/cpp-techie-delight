/*

Given the root of a binary tree, return the maximum sum of a path between any two leaves in the binary tree. You may assume that the binary tree is not skewed and contains at-least two nodes.

Input:

          1
        /   \
       /	 \
      2		  3
       \	 / \
       -4   5   6
           / \
          7   8

Output: 22

Explanation: The maximum sum path between two leaves is [8, 5, 3, 6].

*/

#include <iostream>
#include <climits>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int findMaxSumPath(Node* root, int &max_sum)
    {
        if (root == nullptr) {
            return 0;
        }

        int left = findMaxSumPath(root->left, max_sum);

        int right = findMaxSumPath(root->right, max_sum);

        if (root->left == nullptr) {
            return right + root->data;
        }

        if (root->right == nullptr) {
            return left + root->data;
        }

        int cur_sum = left + right + root->data;

        // update the maximum sum path found so far (Note that maximum sum path
        // "excluding" the current node in the subtree rooted at the current node
        // is already updated as we are doing postorder traversal)

        max_sum = max(cur_sum, max_sum);

        return max(left, right) + root->data;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findMaximumSum(Node* root)
    {
        int max_sum = INT_MIN;
        findMaxSumPath(root, max_sum);

        return max_sum;
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->right = new Node(-4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    cout << Solution().findMaximumSum(root) << endl;

    return 0;
}
