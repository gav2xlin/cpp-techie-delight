/*

Given the root of a non-empty binary tree, return the maximum path sum between any two nodes in the binary tree. The path can start and end at any node in the tree and need not go through the root.

Input:

             1
           /   \
         /		 \
        2		  10
       / \		 / \
      /	  \		/	\
     -1	  -4   -5	 -6
          /	  / \
         /	 /	 \
        3	7	  4
             \
              \
              -2

Output: 15

Explanation: The maximum sum path is [2, 1, 10, -5, 7].

*/

#include <iostream>
#include <limits>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int findMaxPathSum(Node* node, int &result)
    {
        if (node == nullptr) {
            return 0;
        }

        int left = findMaxPathSum(node->left, result);

        int right = findMaxPathSum(node->right, result);

        result = max(result, node->data);
        result = max(result, node->data + left);
        result = max(result, node->data + right);
        result = max(result, node->data + left + right);

        return max(node->data, node->data + max(left, right));
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findMaximumSum(Node* root)
    {
        int result = numeric_limits<int>::min();
        findMaxPathSum(root, result);
        return result;
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(10);
    root->left->left = new Node(-1);
    root->left->right = new Node(-4);
    root->right->left = new Node(-5);
    root->right->right = new Node(-6);
    root->left->right->left = new Node(4);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(4);
    root->right->left->left->right = new Node(-2);

    cout << Solution().findMaximumSum(root) << endl;

    return 0;
}
