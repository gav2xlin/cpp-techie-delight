/*

Given two integer arrays representing inorder and preorder traversal of a binary tree, construct and return the binary tree.

Input:

inorder[]  = [4, 2, 1, 7, 5, 8, 3, 6]
preorder[] = [1, 2, 4, 3, 5, 7, 8, 6]

Output: Root of below binary tree

           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4		5	  6
           / \
          /   \
         7	   8

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node* construct(int start, int end, vector<int> const &preorder, int &pIndex, unordered_map<int, int> &map)
    {
        if (start > end) {
            return nullptr;
        }

        Node *root = new Node(preorder[pIndex++]);

        int index = map[root->data];

        root->left = construct(start, index - 1, preorder, pIndex, map);
        root->right = construct(index + 1, end, preorder, pIndex, map);

        return root;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBinaryTree(vector<int> const &inorder, vector<int> const &preorder)
    {
        int n = inorder.size();

        unordered_map<int, int> map;
        for (int i = 0; i < n; ++i) {
            map[inorder[i]] = i;
        }

        int pIndex = 0;
        return construct(0, n - 1, preorder, pIndex, map);
    }
};

void inorderTraversal(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorderTraversal(root->left);
    cout << root->data << ' ';
    inorderTraversal(root->right);
}

void preorderTraversal(Node* root)
{
    if (root == nullptr) {
        return;
    }

    cout << root->data << ' ';
    preorderTraversal(root->left);
    preorderTraversal(root->right);
}

int main()
{
    vector<int> inorder = { 4, 2, 1, 7, 5, 8, 3, 6 };
    vector<int> preorder = { 1, 2, 4, 3, 5, 7, 8, 6 };

    Node* root = Solution().constructBinaryTree(inorder, preorder);

    cout << "The inorder traversal is ";
    inorderTraversal(root);

    cout << "\nThe preorder traversal is ";
    preorderTraversal(root);
    cout << endl;

    return 0;
}
