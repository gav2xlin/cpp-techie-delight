/*

Given the root of a special binary tree with each node containing an additional next pointer, link nodes at the same level using the next pointer in the form of a linked list like structure.

Input:
           1
         /   \
        /	  \
       2	   3
      / \		\
     /	 \	 	 \
    4	  5		  6
                 / \
                /	\
               7	 8

Output:
           1
         /   \
        /	  \
       2 ----> 3
      / \		\
     /	 \	 	 \
    4 --> 5 ----> 6
                 / \
                /	\
               7 --> 8

*/

#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child
    Node* next = nullptr;		// pointer to the next child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right, Node *next): data(data), left(left), right(right), next(next) {}
};

class Solution
{
private:
    void linkNodes(Node* root, int level, auto &map)
    {
        if (root == nullptr) {
            return;
        }

        map[level].push_back(root);

        linkNodes(root->left, level + 1, map);
        linkNodes(root->right, level + 1, map);
    }

    Node* findNextNode(Node* root)
    {
        if (root == nullptr || root->next == nullptr) {
            return nullptr;
        }

        if (root->next->left) {
            return root->next->left;
        }

        if (root->next->right) {
            return root->next->right;
        }

        return findNextNode(root->next);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child
            Node* next = nullptr;		// pointer to the next child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right, Node *next): data(data), left(left), right(right), next(next) {}
        };
    */

    void linkNodes(Node* root)
    {
        unordered_map<int, vector<Node*>> map;

        linkNodes(root, 1, map);

        for (auto& it: map)
        {
            Node* prev = nullptr;
            for (Node* curr: it.second)
            {
                if (prev) {
                    prev->next = curr;
                }
                prev = curr;
            }

            if (prev) {
                prev->next = nullptr;
            }
        }
    }
    /*void linkNodes(Node* root)
    {
        if (root == nullptr) {
            return;
        }

        linkNodes(root->next);

        if (root->left) {
            root->left->next = (root->right)? root->right: findNextNode(root);
        }

        if (root->right) {
            root->right->next = findNextNode(root);
        }

        linkNodes(root->left);
        linkNodes(root->right);
    }*/
};

void printList(Node* head)
{
    while (head != nullptr)
    {
        cout << head->data << " —> ";
        head = head->next;
    }

    cout << "null" << endl;
}

Node* findNextNode(Node* root)
{
    if (root == nullptr || root->next == nullptr) {
        return nullptr;
    }

    if (root->next->left) {
        return root->next->left;
    }

    if (root->next->right) {
        return root->next->right;
    }

    return findNextNode(root->next);
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->right = new Node(6);
    root->left->left->right = new Node(7);
    root->right->right->left = new Node(8);

    Solution().linkNodes(root);

    Node* node = root;
    while (node)
    {
        printList(node);

        if (node->left) {
            node = node->left;
        }
        else if (node->right) {
            node = node->right;
        }
        else {
            node = findNextNode(node);
        }
    }

    return 0;
}
