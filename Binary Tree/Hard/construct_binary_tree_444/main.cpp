/*

Given an integer array representing a binary tree, where the parent-child relationship is defined by (A[i], i) for every index i in the array A, build a binary tree out of it. The root node's value is i if -1 is present at index i in the array.

Input: [-1, 0, 0, 1, 2, 2, 4, 4]

Output:

           0
         /   \
        /	  \
       1	   2
      /		  / \
     /	  	 /	 \
    3		4	  5
           / \
          /   \
         6	   7

Explanation:

• -1 is present at index 0, which implies that the binary tree root is node 0.
• 0 is present at index 1 and 2, which implies that the left and right children of node 0 are 1 and 2.
• 1 is present at index 3, which implies that the left or the right child of node 1 is 3.
• 2 is present at index 4 and 5, which implies that the left and right children of node 2 are 4 and 5.
• 4 is present at index 6 and 7, which implies that the left and right children of node 4 are 6 and 7.

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBinaryTree(vector<int> const &parent)
    {
        int n = parent.size();

        unordered_map<int, Node*> map;

        for (int i = 0; i < n; ++i) {
            map[i] = new Node(i);
        }

        Node* root = nullptr;

        for (int i = 0; i < n; ++i)
        {
            if (parent[i] == -1) {
                root = map[i];
            }
            else
            {
                Node* ptr = map[parent[i]];

                if (ptr->left)
                {
                    ptr->right = map[i];
                }
                else {
                    ptr->left = map[i];
                }
            }
        }

        return root;
    }
};

void inorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorder(root->left);
    cout << root->data << " ";
    inorder(root->right);
}

int main()
{
    vector<int> parent = { -1, 0, 0, 1, 2, 2, 4, 4 };

    Node* root = Solution().constructBinaryTree(parent);
    inorder(root);
    cout << endl;

    return 0;
}
