/*

Given two integer arrays representing preorder and postorder traversal of a binary tree, construct and return the full binary tree. A full binary tree is a tree in which every node has either 0 or 2 children.

Input:

preorder[]  = [1, 2, 4, 5, 3, 6, 8, 9, 7]
postorder[] = [4, 5, 2, 8, 9, 6, 7, 3, 1]

Output: Root of below binary tree

           1
         /   \
        /	  \
       2	   3
      /	\	  / \
     /	 \ 	 /	 \
    4	  5	6	  7
           / \
          /   \
         8	   9

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node* buildTree(auto &preorder, int &pIndex, int start, int end, auto &map)
    {
        Node* root = new Node(preorder[pIndex]);

        ++pIndex;

        if (pIndex == preorder.size()) {
            return root;
        }

        int index = map[preorder[pIndex]];

        if (start <= index && index + 1 <= end - 1)
        {
            root->left = buildTree(preorder, pIndex, start, index, map);
            root->right = buildTree(preorder, pIndex, index + 1, end - 1, map);
        }

        return root;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBinaryTree(vector<int> const &preorder, vector<int> const &postorder)
    {
        if (postorder.size() == 0) {
            return nullptr;
        }

        unordered_map<int, int> map;
        for (int i = 0; i < postorder.size(); ++i) {
            map[postorder[i]] = i;
        }

        int pIndex = 0;

        int start = 0;
        int end = preorder.size() - 1;

        return buildTree(preorder, pIndex, start, end, map);
    }
};

void inorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorder(root->left);
    cout << root->data << ' ';
    inorder(root->right);
}

int main()
{
    vector<int> preorder = { 1, 2, 4, 5, 3, 6, 8, 9, 7 };
    vector<int> postorder = { 4, 5, 2, 8, 9, 6, 7, 3, 1 };

    Node* root = Solution().constructBinaryTree(preorder, postorder);

    cout << "Inorder traversal is ";
    inorder(root);
    cout << endl;

    return 0;
}
