/*

Given the root of a binary tree and two tree nodes, x and y, return the distance between x and y in the binary tree. The distance between two nodes is defined as the total number of edges in the shortest path from one node to other.

For example, consider the following binary tree.

          1
        /   \
       /	 \
      2		  3
       \	 / \
        4   5   6
           /	 \
          7		  8

Input: x = Node 7, y = Node 6
Output: 3

Input: x = Node 4, y = Node 8
Output: 5

Input: x = Node 5, y = Node 5
Output: 0

Note: The solution should return a negative value if either x or y is not the actual node in the tree.

*/

#include <iostream>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isNodePresent(Node* root, Node* node)
    {
        if (root == nullptr) {
            return false;
        }

        if (root == node) {
            return true;
        }

        return isNodePresent(root->left, node) || isNodePresent(root->right, node);
    }

    int findLevel(Node* root, Node* node, int level)
    {
        if (root == nullptr) {
            return INT_MIN;
        }

        if (root == node) {
            return level;
        }

        int left = findLevel(root->left, node, level + 1);

        if (left != INT_MIN) {
            return left;
        }

        return findLevel(root->right, node, level + 1);
    }

    Node* findLCA(Node* root, Node* x, Node* y)
    {
        if (root == nullptr) {
            return nullptr;
        }

        if (root == x || root == y) {
            return root;
        }

        Node* left = findLCA(root->left, x, y);

        Node* right = findLCA(root->right, x, y);

        if (left && right) {
            return root;
        }

        if (left) {
            return left;
        }

        if (right) {
            return right;
        }

        return nullptr;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findDistance(Node* root, Node* x, Node* y)
    {
        Node* lca = nullptr;

        if (isNodePresent(root, y) && isNodePresent(root, x)) {
            lca = findLCA(root, x, y);
        }
        else {
            return INT_MIN;
        }

        return findLevel(lca, x, 0) + findLevel(lca, y, 0);

        /*
            The above statement is equivalent to the following:

            return findLevel(root, x, 0) + findLevel(root, y, 0) - 2 * findLevel(root, lca, 0);

            We can avoid calling the `isNodePresent()` function by using
            return values of the `findLevel()` function to check if
            `x` and `y` are present in the tree or not.
        */
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->right = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->right->right = new Node(8);

    cout << Solution().findDistance(root, root->right->left->left, root->right->right) << endl; // 7 and 6
    cout << Solution().findDistance(root, root->left->right, root->right->right->right) << endl; // 4 and 8
    cout << Solution().findDistance(root, root->right->left, root->right->left) << endl; // 5 and 5

    return 0;
}
