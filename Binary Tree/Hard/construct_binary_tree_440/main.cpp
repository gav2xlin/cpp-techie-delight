/*

Given two integer arrays representing inorder and postorder traversal of a binary tree, construct and return the binary tree.

Input:

inorder[]   = [4, 2, 1, 7, 5, 8, 3, 6]
postorder[] = [4, 2, 7, 8, 5, 6, 3, 1]

Output: Root of below binary tree

           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4		5	  6
           / \
          /   \
         7	   8

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node* construct(int start, int end,
                    vector<int> const &postorder, int &pIndex,
                    unordered_map<int, int> &map)
    {
        if (start > end) {
            return nullptr;
        }

        Node* root = new Node(postorder[pIndex--]);

        int index = map[root->data];

        root->right = construct(index + 1, end, postorder, pIndex, map);
        root->left = construct(start, index - 1, postorder, pIndex, map);

        return root;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBinaryTree(vector<int> const &inorder, vector<int> const &postorder)
    {
        int n = inorder.size();

        unordered_map<int, int> map;
        for (int i = 0; i < inorder.size(); ++i) {
            map[inorder[i]] = i;
        }

        int pIndex = n - 1;
        return construct(0, n - 1, postorder, pIndex, map);
    }
};

void inorderTraversal(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorderTraversal(root->left);
    cout << root->data << ' ';
    inorderTraversal(root->right);
}

void postorderTraversal(Node* root)
{
    if (root == nullptr) {
        return;
    }

    postorderTraversal(root->left);
    postorderTraversal(root->right);
    cout << root->data << ' ';
}

int main()
{
    vector<int> inorder = { 4, 2, 1, 7, 5, 8, 3, 6 };
    vector<int> postorder = { 4, 2, 7, 8, 5, 6, 3, 1 };

    Node* root = Solution().constructBinaryTree(inorder, postorder);

    cout << "Inorder traversal is ";
    inorderTraversal(root);

    cout << "\nPostorder traversal is ";
    postorderTraversal(root);
    cout << endl;

    return 0;
}
