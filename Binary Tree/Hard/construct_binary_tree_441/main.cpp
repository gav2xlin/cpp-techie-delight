/*

Given two integer arrays representing inorder and level order traversal of a binary tree, construct and return the binary tree.

Input:

inorder[]    = [4, 2, 1, 7, 5, 8, 3, 6]
levelorder[] = [1, 2, 3, 4, 5, 6, 7, 8]

Output: Root of below binary tree

           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4		5	  6
           / \
          /   \
         7	   8

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node* buildTree(vector<int> const &inorder, int start, int end,
                    unordered_map<int, int> map)
    {
        if (start > end) {
            return nullptr;
        }

        int index = start;
        for (int j = start + 1; j <= end; ++j)
        {
            if (map[inorder[j]] < map[inorder[index]]) {
                index = j;
            }
        }

        Node* root = new Node(inorder[index]);

        root->left = buildTree(inorder, start, index - 1, map);
        root->right = buildTree(inorder, index + 1, end, map);

        return root;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBinaryTree(vector<int> const &inorder, vector<int> const &level)
    {
        int n = inorder.size();

        unordered_map<int, int> map;
        for (int i = 0; i < n; ++i) {
            map[level[i]] = i;
        }

        return buildTree(inorder, 0, n - 1, map);
    }
};

void inorderTraversal(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorderTraversal(root->left);
    cout << root->data << " ";
    inorderTraversal(root->right);
}

int main()
{
    vector<int> inorder = { 4, 2, 5, 1, 6, 3, 7 };
    vector<int> level    = { 1, 2, 3, 4, 5, 6, 7 };

    Node* root = Solution().constructBinaryTree(inorder, level);

    cout << "Inorder traversal of the constructed tree is ";
    inorderTraversal(root);
    cout << endl;

    return 0;
}
