/*

Given the root of a perfect binary tree, return the values of alternating left and right nodes for each level.

Input:
                    1
                  /	   \
               /		 \
            /			   \
          2					 3
        /   \				/  \
      /		  \			  /		 \
     4		   5		 6		  7
    / \		  / \		/ \		 / \
   /   \	 /   \	   /   \	/   \
  8		9   10   11   12   13  14   15

Output: [1, 2, 3, 4, 7, 5, 6, 8, 15, 9, 14, 10, 13, 11, 12]

*/

#include <iostream>
#include <vector>
#include <queue>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> traverse(Node* root)
    {
        if (root == nullptr) {
            return {};
        }

        vector<int> res{root->data};

        queue<Node*> q1, q2;

        if (root->left && root->right)
        {
            q1.push(root->left);
            q2.push(root->right);
        }

        while (!q1.empty())
        {
            int n = q1.size();

            while (n--)
            {
                Node* x = q1.front();
                q1.pop();

                res.push_back(x->data);

                if (x->left) {
                    q1.push(x->left);
                }

                if (x->right) {
                    q1.push(x->right);
                }

                Node* y = q2.front();
                q2.pop();

                res.push_back(y->data);

                if (y->right) {
                    q2.push(y->right);
                }

                if (y->left) {
                    q2.push(y->left);
                }
            }
        }

        return res;
    }
    /*vector<int> traverse(Node* root)
    {
        if (root == nullptr) {
            return {};
        }

        int level = 1;

        unordered_map<int, vector<int>> map;

        map[level].push_back(root->data);

        queue<Node*> q1, q2;

        if (root->left && root->right)
        {
            q1.push(root->left);
            q2.push(root->right);
        }

        while (!q1.empty())
        {
            ++level;

            int n = q1.size();

            while (n--)
            {
                Node* x = q1.front();
                q1.pop();

                map[level].push_back(x->data);

                if (x->left) {
                    q1.push(x->left);
                }

                if (x->right) {
                    q1.push(x->right);
                }

                Node* y = q2.front();
                q2.pop();

                map[level].push_back(y->data);

                if (y->right) {
                    q2.push(y->right);
                }

                if (y->left) {
                    q2.push(y->left);
                }
            }
        }

        vector<int> res;
        for (int i = map.size(); i > 0; --i)
        {
            for (int j: map[i]) {
                res.push_back(j);
            }
        }

        return res;
    }*/
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);
    root->left->left->left = new Node(8);
    root->left->left->right = new Node(9);
    root->left->right->left = new Node(10);
    root->left->right->right = new Node(11);
    root->right->left->left = new Node(12);
    root->right->left->right = new Node(13);
    root->right->right->left = new Node(14);
    root->right->right->right = new Node(15);

    cout << Solution().traverse(root) << endl;

    return 0;
}
