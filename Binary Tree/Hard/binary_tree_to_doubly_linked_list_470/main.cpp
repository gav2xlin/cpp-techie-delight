/*

Given the root of a binary tree, in-place convert the binary tree into a doubly-linked list.

The conversion should be done such that the left and right pointers of binary tree nodes should act as previous and next pointers in a doubly-linked list, and the doubly linked list nodes should follow the same order of nodes as inorder traversal on the tree.

Input:

             1
           /   \
         /		 \
        2		  3
       / \		 / \
      /	  \		/	\
     4	   5   6	 7

Output: 4 ⇔ 2 ⇔ 5 ⇔ 1 ⇔ 6 ⇔ 3 ⇔ 7

The solution should return the head of the doubly-linked list constructed from the tree nodes.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void convert(Node* root, Node* &head)
    {
        if (root == nullptr) {
            return;
        }

        convert(root->left, head);
        root->left = nullptr;

        Node* right = root->right;

        root->right = head;
        if (head != nullptr) {
            head->left = root;
        }

        head = root;

        convert(right, head);
    }

    void reverse(Node*& head)
    {
        Node* prev = nullptr;
        Node* current = head;

        while (current != nullptr)
        {
            swap(current->left, current->right);
            prev = current;
            current = current->left;
        }

        if (prev != nullptr) {
            head = prev;
        }
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* binaryTreeToDoublyLinkedList(Node* root)
    {
        Node* head = nullptr;

        convert(root, head);
        reverse(head);

        return head;
    }
};

void printDLL(Node* &head)
{
    Node* curr = head;
    while (curr != nullptr)
    {
        cout << curr->data << " ";
        curr = curr->right;
    }
    cout << endl;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    Node* head = Solution().binaryTreeToDoublyLinkedList(root);

    printDLL(head);

    return 0;
}
