/*

Given the root of a binary tree and a tree node x, return the next node at the same level as the node x.

For example, consider the following binary tree.

           1
         /   \
        /	  \
       2	   3
      / \		\
     /	 \		 \
    4	  5		  6
                 / \
                /   \
               7	 8

Input: Node 2
Output: Node 3
Explanation: The next node of 2 is node 3

Input: Node 5
Output: Node 6
Explanation: The next node of 5 is node 6

Input: Node 8
Output: nullptr
Explanation: The next node of 8 doesn't exist.

Note: The solution should return nullptr if x is not the actual tree node.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node* findNextNode(Node* root, Node* node, int level, int &node_level)
    {
        if (root == nullptr) {
            return nullptr;
        }

        if (root == node)
        {
            node_level = level;
            return nullptr;
        } else if (node_level && level == node_level) {
            return root;
        }

        Node* left = findNextNode(root->left, node, level + 1, node_level);

        if (left != nullptr) {
            return left;
        }

        return findNextNode(root->right, node, level + 1, node_level);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* findNextNode(Node* root, Node* node)
    {
        int node_level = 0;
        return findNextNode(root, node, 1, node_level);
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    Node* right = Solution().findNextNode(root, root->left->right);

    if (right) {
        cout << "Right node is " << right->data << endl;
    }
    else {
        cout << "Right node doesn't exist" << endl;
    }

    return 0;
}
