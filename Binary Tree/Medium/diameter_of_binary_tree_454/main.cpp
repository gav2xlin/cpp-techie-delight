/*

Given the root of a binary tree, return the diameter of binary tree. A binary tree diameter equals the total number of nodes on the longest path between any two leaves in it.

Input:

       1
     /   \
    /	  \
   2	   3
    \	  / \
     \	 /	 \
      4	5	  6
       / \
      /	  \
     7	   8

Output: 6

Explanation: The nodes involved in binary tree's diameter are [4, 2, 1, 3, 5, 7]. Note that the diameter passes through the root node.


Input:

        1
         \
          \
           2
          / \
         /	 \
        3	  4
       / \	   \
      /	  \		\
     5	   6	 7

Output: 5

Explanation: The nodes involved in binary tree's diameter are [5, 3, 2, 4, 7]. Note that the diameter does not pass through the root node.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int getDiameter(Node* root, int &diameter)
    {
        if (root == nullptr) {
            return 0;
        }

        int left_height = getDiameter(root->left, diameter);
        int right_height = getDiameter(root->right, diameter);

        int max_diameter = left_height + right_height + 1;

        diameter = max(diameter, max_diameter);

        return max(left_height, right_height) + 1;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findDiameter(Node* root)
    {
        int diameter = 0;
        getDiameter(root, diameter);
        return diameter;
    }
};

int main()
{
    {
        /*Input:
               1
             /   \
            /	  \
           2	   3
            \	  / \
             \	 /	 \
              4	5	  6
               / \
              /	  \
             7	   8

        Output: 6*/
        Node n4{4};
        Node n2{2, nullptr, &n4};
        Node n7{7};
        Node n8{8};
        Node n5{5, &n7, &n8};
        Node n6{6};
        Node n3{3, &n5, &n6};
        Node n1{1, &n2, &n3};

        cout << Solution().findDiameter(&n1) << endl;
    }

    {
        /*Input:
                1
                 \
                  \
                   2
                  / \
                 /	 \
                3	  4
               / \	   \
              /	  \		\
             5	   6	 7

        Output: 5*/
        Node n5{5};
        Node n6{6};
        Node n3{3, &n5, &n6};
        Node n7{7};
        Node n4{4, nullptr, &n7};
        Node n2{2, &n3, &n4};
        Node n1{1, nullptr, &n2};

        cout << Solution().findDiameter(&n1) << endl;
    }

    return 0;
}
