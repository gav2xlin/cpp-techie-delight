cmake_minimum_required(VERSION 3.5)

project(height_of_binary_tree_400 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(height_of_binary_tree_400 main.cpp)

install(TARGETS height_of_binary_tree_400
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
