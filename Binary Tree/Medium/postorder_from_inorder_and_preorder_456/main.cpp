/*

Given two integer arrays representing inorder and preorder traversal of a binary tree, return the postorder traversal of the corresponding binary tree.

Input:

inorder[]  = [4, 2, 1, 7, 5, 8, 3, 6]
preorder[] = [1, 2, 4, 3, 5, 7, 8, 6]

Output: [4, 2, 7, 8, 5, 6, 3, 1]

Explanation: The inorder and preorder traversal represents the following binary tree.

           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4		5	  6
           / \
          /   \
         7	   8

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    void findPostorder(int start, int end, vector<int> const &preorder, int &pIndex, unordered_map<int, int> &map, vector<int>& res)
    {
        if (start > end) {
            return;
        }

        int value = preorder[pIndex++];

        if (start == end)
        {
            res.push_back(value);

            return;
        }

        int i = map[value];

        findPostorder(start, i - 1, preorder, pIndex, map, res);
        findPostorder(i + 1, end, preorder, pIndex, map, res);

        res.push_back(value);
    }
public:
    vector<int> findPostorder(vector<int> const &inorder, vector<int> const &preorder)
    {
        unordered_map<int, int> map;

        for (int i = 0; i < inorder.size(); ++i) {
            map[inorder[i]] = i;
        }

        vector<int> res;
        int pIndex = 0;
        findPostorder(0, inorder.size() - 1, preorder, pIndex, map, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> inorder = { 4, 2, 1, 7, 5, 8, 3, 6 };
    vector<int> preorder = { 1, 2, 4, 3, 5, 7, 8, 6 };

    cout << "inorder: " << inorder << endl;
    cout << "preorder: " << preorder << endl;

    auto postorder = Solution().findPostorder(inorder, preorder);

    cout << "postorder: " << postorder << endl;

    return 0;
}
