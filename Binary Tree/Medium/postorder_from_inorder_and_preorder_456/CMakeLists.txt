cmake_minimum_required(VERSION 3.5)

project(postorder_from_inorder_and_preorder_456 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(postorder_from_inorder_and_preorder_456 main.cpp)

install(TARGETS postorder_from_inorder_and_preorder_456
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
