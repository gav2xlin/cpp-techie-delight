/*

Given the root of a binary tree, fix the children-sum property in the binary tree. For a tree to satisfy the children-sum property, each node's value should be equal to the sum of values at its left and right subtree. The value of an empty node is considered as 0. The only operation allowed is an increment operation on the node's value.

Since several binary tree might satisfy the constraints, the solution should return any one of them.

Input:

          25
        /	 \
       /	  \
      /		   \
     8		   10
    / \		  /  \
   /   \	 /	  \
  4		5	6	   7

Output:

          25
        /	 \
       /	  \
      /		   \
     12		   13
    /  \	  /  \
   /	\	 /	  \
  7		 5	6	   7

or any other valid binary tree.

*/

#include <iostream>
#include <cmath>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int findChildrenSum(Node* node)
    {
        int left = node->left ? node->left->data : 0;
        int right = node->right ? node->right->data : 0;

        return left + right;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void fixBinaryTree(Node* &root)
    {
        /*if (!root || !root->left && !root->right) {
            return;
        }

        fixBinaryTree(root->left);
        fixBinaryTree(root->right);

        int diff = root->data - findChildrenSum(root);

        if (diff < 0) {
            root->data += abs(diff);
        } else if (diff > 0) {
            Node* subtree = root->left ? root->left : root->right;
            subtree->data += diff;
            fixBinaryTree(subtree);
        }*/
        if (!root || !root->left && !root->right) {
            return;
        }

        int diff = root->data - ((root->left ? root->left->data : 0) + (root->right ? root->right->data : 0));

        if (diff > 0) {
            (root->left ? root->left : root->right)->data += diff;
        }

        fixBinaryTree(root->left);
        fixBinaryTree(root->right);

        root->data = (root->left ? root->left->data : 0) + (root->right ? root->right->data : 0);
    }
};

void preorder(Node* node)
{
    if (node == nullptr) {
        return;
    }

    cout << node->data << ' ';
    preorder(node->left);
    preorder(node->right);
}

int main()
{
    Node* root = new Node(25);
    root->left = new Node(8);
    root->right = new Node(10);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    Solution().fixBinaryTree(root);

    preorder(root);

    return 0;
}
