/*

Given the root of a binary tree, return the boundary traversal of its nodes' values. The solution should process the boundary nodes starting from the tree's root, in an anti-clockwise direction, without any duplicates.

Input:
                1
              /   \
            /		\
          /			  \
         2			   3
       /   \		 /   \
      /		\		/	  \
     4		 5	   6	   7
    / \		  \			  /
   /   \	   \		 /
  8		9	   10	   11
       / \			  /
      /   \			 /
     12   13		14

Output: [1, 2, 4, 8, 12, 13, 10, 6, 14, 11, 7, 3]

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isLeaf(Node* node) {
            return node->left == nullptr && node->right == nullptr;
    }

    void findBoundaryTraversal(Node* root, vector<int>& res)
    {
        if (root == nullptr) {
            return;
        }

        res.push_back(root->data);

        findLeftBoundary(root->left, res);

        if (!isLeaf(root)) {
            findLeafNodes(root, res);
        }

        findRightBoundary(root->right, res);
    }

    void findLeafNodes(Node* root, vector<int>& res)
    {
        if (root == nullptr) {
            return;
        }

        findLeafNodes(root->left, res);

        if (isLeaf(root)) {
            res.push_back(root->data);
        }

        findLeafNodes(root->right, res);
    }

    void findLeftBoundary(Node* root, vector<int>& res)
    {
        if (!root) {
            return;
        }

        Node* node = root;

        while (!isLeaf(node))
        {
            res.push_back(node->data);

            node = (node->left != nullptr) ? node->left : node->right;
        }
    }

    void findRightBoundary(Node* root, vector<int>& res)
    {
        if (!root || isLeaf(root)) {
            return;
        }

        findRightBoundary(root->right != nullptr ? root->right: root->left, res);

        res.push_back(root->data);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findBoundaryTraversal(Node* root)
    {
        vector<int> res;
        findBoundaryTraversal(root, res);
        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);
    root->left->left->left = new Node(8);
    root->left->left->right = new Node(9);
    root->left->right->right = new Node(10);
    root->right->right->left = new Node(11);
    root->left->left->right->left = new Node(12);
    root->left->left->right->right = new Node(13);
    root->right->right->left->left = new Node(14);

    cout << Solution().findBoundaryTraversal(root) << endl;

    return 0;
}
