/*

Given the root of a binary tree, return the right view of its nodes' values. Assume the left and right child of a node makes a 45–degree angle with the parent.

Input:
           1
         /	 \
        /	  \
       2	   3
             /   \
            /	  \
           5	   6
         /   \
        /	  \
       7	   8

Output: [1, 3, 6, 8]

Input:

      1
    /   \
   /	 \
  2		  3
   \	 /
    \   /
     4 5

Output: [1, 3, 5]

*/


#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void findRightView(Node* root, int level, auto &map)
    {
        if (root == nullptr) {
            return;
        }

        map[level] = root->data;

        findRightView(root->left, level + 1, map);
        findRightView(root->right, level + 1, map);
    }

    void findRightView(Node* root, int level, int &last_level, vector<int>& res)
    {
        if (root == nullptr) {
            return;
        }

        if (last_level < level)
        {

            res.push_back(root->data);
            last_level = level;
        }

        findRightView(root->right, level + 1, last_level, res);
        findRightView(root->left, level + 1, last_level, res);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findRightView(Node* root)
    {
        /*unordered_map<int, int> map;
        findRightView(root, 1, map);

        vector<int> res;
        for (int i = 1; i <= map.size(); ++i) {
            cout << map[i] << " ";
        }

        return res;*/
        vector<int> res;

        int last_level = 0;
        findRightView(root, 1, last_level, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->right = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    cout << Solution().findRightView(root) << endl;

    return 0;
}
