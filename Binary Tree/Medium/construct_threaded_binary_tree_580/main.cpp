/*

Given the root of a binary tree, convert it into a single-threaded binary tree. In a single-threaded binary tree, all empty right child pointers in a binary tree point to the in-order successor of the node, if it exists.

Input: A binary tree with each node containing extra boolean field `isThreaded` (initially set to False).

          5
        /   \
       /     \
      2       7
     / \     / \
    /   \   /   \
   1     4 6     9
        /       / \
       /       /   \
      3       8    10

Output: Above binary tree with empty right child points to their inorder successor. i.e,

• Right child of node 1 should point to node 2.
• Right child of node 3 should point to node 4.
• Right child of node 4 should point to node 5.
• Right child of node 6 should point to node 7.
• Right child of node 8 should point to node 9.

Also, `isThreaded` field is set to true for all nodes having a threaded link. i.e., nodes 1, 3, 4, 6, 8.

Note: The solution should convert the tree in-place and should not create any new tree nodes.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    // true if the right pointer of the node points to its inorder successor
    bool isThreaded{false};

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void populateNext(Node* curr, Node* &prev)
    {
        if (curr == nullptr) {
            return;
        }

        populateNext(curr->left, prev);

        // if the current node is not the root node of a binary tree
        // and has a null right child
        if (prev && !prev->right)
        {
            prev->right = curr;

            prev->isThreaded = true;
        }

        prev = curr;

        populateNext(curr->right, prev);
    }
public:

    /*
        A Threaded binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            // true if the right pointer of the node points to its inorder successor
            bool isThreaded;

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void convertToThreaded(Node* root)
    {
        Node* prev = nullptr;
        populateNext(root, prev);
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

Node *leftMostNode(Node* root)
{
    Node* node = root;
    while (node != nullptr && node->left) {
        node = node->left;
    }

    return node;
}

void traverse(Node* root)
{
    if (root == nullptr) {
        return;
    }

    Node* curr = leftMostNode(root);
    while (curr != nullptr)
    {
        cout << curr->data << " ";

        if (curr->isThreaded) {
            curr = curr->right;
        } else {
            curr = leftMostNode(curr->right);
        }
    }
}

int main()
{
    Node* root = new Node(5);
    root->left = new Node(2);
    root->right = new Node(7);
    root->left->left = new Node(1);
    root->left->right = new Node(4);
    root->right->left = new Node(6);
    root->right->right = new Node(9);
    root->left->right->left = new Node(3);
    root->right->right->left = new Node(8);
    root->right->right->right = new Node(10);

    //cout << "before:\n" << *root << endl;

    Solution().convertToThreaded(root);

    traverse(root);

    //cout << "\nafter:\n" << *root << endl;

    return 0;
}
