/*

Given the root of a binary tree, return the count all subtrees in it such that every node in the subtree has the same value.

Input:

           1
         /   \
        /	  \
       2	   3
      /		  / \
     /		 /	 \
    4		5	  6
   /	   / \	   \
  /		  /   \		\
 4		 5	   5	 7

Output: 6

Explanation: Six subtrees have the same data, as shown below:

     4		   5		4		5		5		7
    /		 /   \
   /		/	  \
  4		   5	   5

*/

#include <iostream>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int countSubtrees(Node* root, int &count)
    {
        if (root == nullptr) {
            return INT_MIN;
        }

        if (root->left == nullptr && root->right == nullptr)
        {
            ++count;
            return root->data;
        }

        int left = countSubtrees(root->left, count);
        int right = countSubtrees(root->right, count);

        if ((left == INT_MIN && right == root->data) || (right == INT_MIN && left == root->data) || (left == right && left == root->data))
        {
            ++count;
            return root->data;
        }

        return INT_MAX;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findSubtrees(Node* root)
    {
        int count = 0;
        countSubtrees(root, count);

        return count;
    }
};


int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->left->left->left = new Node(4);
    root->right->left->left = new Node(5);
    root->right->left->right = new Node(5);
    root->right->right->right = new Node(7);

    cout << Solution().findSubtrees(root) << endl;

    return 0;
}
