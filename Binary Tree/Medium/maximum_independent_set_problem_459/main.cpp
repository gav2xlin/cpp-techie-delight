/*

Given the root of a binary tree, return the size of the Maximum Independent Set (MIS) in it.

An independent set is a set of nodes in a binary tree, no two of which are adjacent, i.e., there is no edge connecting any two. The size of an independent set is the total number of nodes it contains. The maximum independent set problem is finding an independent set of the largest possible size for a given binary tree.

Input:
           1
         /   \
        /	  \
       /	   \
      2			3
       \	   / \
        \	  /	  \
         4	 5	   6
            / \
           /   \
          7		8

Output: 5

Explanation: The Maximum Independent Set (MIS) is [1, 4, 6, 7, 8].

*/

#include <iostream>
#include <unordered_map>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int findMISSize(Node* root, unordered_map<Node*, int> map)
    {
        if (root == nullptr) {
            return 0;
        }

        if (map.find(root) != map.end()) {
            return map[root];
        }

        int excl = findMISSize(root->left, map) + findMISSize(root->right, map);

        int incl = 1;

        if (root->left)
        {
            incl += findMISSize(root->left->left, map) + findMISSize(root->left->right, map);
        }

        if (root->right)
        {
            incl += findMISSize(root->right->left, map) + findMISSize(root->right->right, map);
        }

        map[root] = max(excl, incl);

        return map[root];
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findMISSize(Node* root)
    {
        unordered_map<Node*, int> map;
        return findMISSize(root, map);
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    cout << Solution().findMISSize(root) << endl;

    return 0;
}
