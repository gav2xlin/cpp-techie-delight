cmake_minimum_required(VERSION 3.5)

project(is_complete_binary_tree_450 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(is_complete_binary_tree_450 main.cpp)

install(TARGETS is_complete_binary_tree_450
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
