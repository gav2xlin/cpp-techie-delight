/*

Given the root of a binary tree, check if it is a complete binary tree or not. A complete binary tree is a binary tree in which every level, except possibly the last, is filled, and all nodes are as far left as possible.

Input:
           1
         /   \
        /	  \
       2	   3
      / \	  /
     /	 \	 /
    4	 5	6

Output: true

Input:
           1
         /   \
        /	  \
       2	   3
      /		  / \
     /		 /	 \
    4		5	  6

Output: false

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int size(Node* root)
    {
        if (root == nullptr) {
            return 0;
        }

        return 1 + size(root->left) + size(root->right);
    }

    bool isComplete(Node* root, int i, int n)
    {
        if (root == nullptr) {
            return true;
        }

        if ((root->left && 2*i + 1 >= n) || !isComplete(root->left, 2*i + 1, n)) {
            return false;
        }

        if ((root->right && 2*i + 2 >= n) || !isComplete(root->right, 2*i + 2, n)) {
            return false;
        }

        return true;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isComplete(Node* root)
    {
        return isComplete(root, 0, size(root));
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    cout << boolalpha << Solution().isComplete(root) << endl;

    return 0;
}
