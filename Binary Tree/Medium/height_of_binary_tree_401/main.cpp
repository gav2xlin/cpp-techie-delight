/*

Given the root of a binary tree with leaf nodes forming a circular doubly linked list, return the binary tree's height. Note that the leaf node's left and right pointers will act as a previous and next pointer of the circular doubly linked list, respectively.

The height of the binary tree is the total number of edges or nodes on the longest path from the root node to the leaf node. The solution should consider the total number of nodes in the longest path. For example, an empty tree's height is 0, and the tree's height with only one node is 1.

Input: Below binary tree (The leaf nodes [7, 5, 6] are connected using the left and right pointers and forming a circular doubly linked list)

           1
         /   \
        /	  \
       2	   3
      /	\		\
     /	 \ 	 	 \
    4	  5		  6
   /
  /
 7

Output: 4

*/

#include <iostream>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findHeight(Node* node)
    {
        if (node == nullptr) {
            return 0;
        }

        if ((node->left && node->left->right == node) && (node->right && node->right->left == node)) {
            return 1;
        }

        return 1 + max(findHeight(node->left), findHeight(node->right));
    }
};

int main()
{
    Node* root = nullptr;

    root = new Node{1};
    root->left = new Node{2};
    root->right = new Node{3};
    root->left->left = new Node{4};
    root->left->right = new Node{5};
    root->right->right = new Node{6};
    root->left->left->left = new Node{7};

    Node* first = root->left->left->left;
    Node* second = root->left->right;
    Node* third = root->right->right;

    first->left = third;
    first->right = second;

    second->left = first;
    second->right = third;

    third->left = second;
    third->right = first;

    cout << "The height of the binary tree is " << Solution().findHeight(root) << endl;

    return 0;
}
