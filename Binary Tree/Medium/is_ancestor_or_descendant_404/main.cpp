/*

Given the root of a binary tree and two tree nodes, x and y, check if they lie on the same root-to-leaf path in the binary tree. In other words, determine whether x is an ancestor of y or x is a descendant of y.

For example, consider the following binary tree.

                     1
                   /   \
                 /		 \
                2		  3
              /  \		 /  \
             /	  \		/	 \
            4	   5   6	  7
          /   \		\		 /
         /	   \	 \		/
        8		9	 10	   11
              /   \		  /
             /	   \	 /
            12	   13	14

Input: x = Node 3, y = Node 14
Output: true
Explanation: Node 3 is an ancestor of Node 14

Input: x = Node 12, y = Node 2
Output: true
Explanation: Node 12 is a direct descendant of Node 2

Input: x = Node 4, y = Node 5
Output: false
Explanation: Node 4 is a neither an ancestor nor a descendant of Node 5

Note: The solution should return false if either x or y is not the actual node in the tree.

*/

#include <iostream>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution {
private:
        void performDFS(Node* root,
                        unordered_map<Node*, int> &arrival,
                        unordered_map<Node*, int> &departure,
                        int &time)
        {
            if (root == nullptr) {
                return;
            }

            arrival[root] = ++time;

            performDFS(root->left, arrival, departure, time);
            performDFS(root->right, arrival, departure, time);

            departure[root] = ++time;
        }

        void printTree(Node* root,
                    unordered_map<Node*, int> &arrival,
                    unordered_map<Node*, int> &departure)
        {
            if (root == nullptr) {
                return;
            }

            cout << "Node " << root->data << " (" << arrival[root] << ", " << departure[root] << ")" << endl;

            printTree(root->left, arrival, departure);
            printTree(root->right, arrival, departure);
        }

        bool isAncestorOrDescendant(Node* x, Node* y,
                                    unordered_map<Node*, int> &arrival,
                                    unordered_map<Node*, int> &departure)
        {
            if (!arrival[x] || !arrival[y]) {
                return false; // not the actual node in the tree
            }

            bool isAncestor = arrival[x] < arrival[y] && departure[x] > departure[y];
            bool isDescendant = arrival[y] < arrival[x] && departure[y] > departure[x];

            return isAncestor || isDescendant;
        }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isAncestorOrDescendant(Node* root, Node* x, Node* y)
    {
        unordered_map<Node*, int> arrival;
        unordered_map<Node*, int> departure;

        int time = 0;
        performDFS(root, arrival, departure, time);

        return isAncestorOrDescendant(x, y, arrival, departure);
    }
};

int main()
{
    Node* root = new Node{1};
    root->left = new Node{2};
    root->right = new Node{3};
    root->left->left = new Node{4};
    root->left->right = new Node{5};
    root->right->left = new Node{6};
    root->right->right = new Node{7};
    root->left->left->left = new Node{8};
    root->left->left->right = new Node{9};
    root->left->right->right = new Node{10};
    root->right->right->left = new Node{11};
    root->left->left->right->left = new Node{12};
    root->left->left->right->right = new Node{13};
    root->right->right->left->left = new Node{14};

    Solution s;

    cout << boolalpha << s.isAncestorOrDescendant(root, root->right, root->right->right->left->left);
    cout << boolalpha << s.isAncestorOrDescendant(root, root->left->left->right->left, root->left);
    cout << boolalpha << s.isAncestorOrDescendant(root, root->left->left, root->left->right);
    cout << boolalpha << s.isAncestorOrDescendant(root, new Node(root->left->left->data), root->left->right);

    return 0;
}
