/*

Given the root of a binary tree, return the maximum sum path from the root node to any leaf node in the binary tree.

Input:
                1
              /   \
            /		\
          /			  \
         2			   3
       /   \		 /   \
      /		\		/	  \
     8		 4	   5	   6
            /	  / \		\
           /	 /   \		 \
         10		7	  9		  5

Output: [1, 3, 5, 9]

Explanation: The maximum sum path is [1 -> 3 -> 5 -> 9] having sum 18.


In case multiple paths exists with the maximum sum, the solution can return any one of them.

Input:

              -1
            /	 \
          /		   \
         2		   -3
       /   \		 \
      /		\		  \
     4		-5		  -6
      \				  /
       \			 /
       -7			8

Output: [-1, 2, 4, -7] or [-1, -3, -6, 8]

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool getPath(Node* root, int sum, vector<int>& res)
    {
        if (sum == 0 && root == nullptr) {
            return true;
        }

        if (root == nullptr) {
            return false;
        }

        bool left = getPath(root->left, sum - root->data, res);
        bool right = false;

        if (!left) {
            right = getPath(root->right, sum - root->data, res);
        }

        if (left || right) {
            res.push_back(root->data);
        }

        return left || right;
    }

    int getRootToLeafSum(Node* root)
    {
        if (root == nullptr) {
            return INT_MIN;
        }

        if (root->left == nullptr && root->right == nullptr) {
            return root->data;
        }

        int left = getRootToLeafSum(root->left);
        int right = getRootToLeafSum(root->right);

        return (left > right? left : right) + root->data;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findMaxSumRootToLeafPath(Node* root)
    {
        int sum = getRootToLeafSum(root);

        vector<int> res;
        getPath(root, sum, res);

        return {res.rbegin(), res.rend()};
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(8);
    root->left->right = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->left->right->left = new Node(10);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(9);
    root->right->right->right = new Node(5);

    cout << Solution().findMaxSumRootToLeafPath(root) << endl;

    return 0;
}
