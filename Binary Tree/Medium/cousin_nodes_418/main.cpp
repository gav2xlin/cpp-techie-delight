/*

Given the root of a binary tree and a tree node x, return values of all cousin nodes of x in the binary tree. Two nodes of a binary tree are cousins of each other only if they have different parents, but they are at the same level.

For example, consider the following binary tree.

             1
           /   \
         /		 \
        2		  3
      /  \		 /  \
     /	  \		/	 \
    4	   5   6	  7


Input: x = Node 6
Output: [4, 5]

Input: x = Node 2
Output: []

Input: x = Node 4
Output: [6, 7]

Note: The solution should return an empty list if x is not the actual node in the tree.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void findLevel(Node* root, Node* x, int index, int &level)
    {
        if (root == nullptr || level) {
            return;
        }

        if (root == x) {
            level = index;
        }

        findLevel(root->left, x, index + 1, level);
        findLevel(root->right, x, index + 1, level);
    }

    void findCousins(Node* root, Node* node, int level, vector<int>& res)
    {
        if (root == nullptr) {
            return;
        }

        if (level == 1)
        {
            res.push_back(root->data);

            return;
        }

        if (!(root->left && root->left == node || root->right && root->right == node))
        {
            findCousins(root->left, node, level - 1, res);
            findCousins(root->right, node, level - 1, res);
        }
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findCousins(Node* root, Node* node)
    {
        if (root == nullptr || root == node) {
            return {};
        }

        int level = 0;
        findLevel(root, node, 1, level);

        vector<int> res;
        findCousins(root, node, level, res);

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    cout << Solution().findCousins(root, root->right->left) << endl;

    return 0;
}
