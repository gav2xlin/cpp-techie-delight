/*

Given the root of a binary tree, check if it satisfies the height-balanced property of the red–black tree or not.

The red–black tree's height-balanced property states that the path from the root to the farthest leaf is no more than twice as long as a path from the root to the nearest leaf. In other words, the maximum height of any node in a tree is not greater than twice its minimum height.

Input:
               1
            /	 \
           2	   3
         /		 /   \
       4		5	   6
              /   \
             7	   8
           /   \
          9	   10

Output: true

Input:
               1
            /	 \
           2	   3
         /		 /   \
       4		5	   6
              /   \
             7	   8
           /   \
          9		10
              /	   \
             11	   12

Output: false

Explanation: The tree violates the red–black tree property at node 3.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isHeightBalanced(Node* root, int &rootMax)
    {
        if (root == nullptr) {
            return true;
        }

        int leftMax = 0, rightMax = 0;

        if (isHeightBalanced(root->left, leftMax) && isHeightBalanced(root->right, rightMax))
        {
            int rootMin = min(leftMax, rightMax) + 1;
            rootMax = max(leftMax, rightMax) + 1;

            return (rootMax <= 2*rootMin);
        }

        return false;
    }

public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isHeightBalanced(Node* root)
    {
        int rootMax = 0;
        return isHeightBalanced(root, rootMax);
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);
    root->right->left->left->left = new Node(9);
    root->right->left->left->right = new Node(10);

    cout << boolalpha << Solution().isHeightBalanced(root) << endl;

    return 0;
}
