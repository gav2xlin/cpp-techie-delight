cmake_minimum_required(VERSION 3.5)

project(height_balanced_binary_tree_422 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(height_balanced_binary_tree_422 main.cpp)

install(TARGETS height_balanced_binary_tree_422
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
