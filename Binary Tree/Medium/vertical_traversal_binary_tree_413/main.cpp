/*

Given the root of a binary tree, return the vertical traversal of its nodes' values. In a vertical traversal, nodes of a binary tree are processed in vertical order from left to right. Assume that the left and right child makes a 45–degree angle with the parent.

Input:
           1
         /	 \
        /	  \
       2	   3
             /   \
            /	  \
           5	   6
         /   \
        /	  \
       7	   8

Output: [2, 7, 1, 5, 3, 8, 6]

Explanation: The binary tree has four vertical levels:

[2, 7]
[1, 5]
[3, 8]
[6]

*/

#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <queue>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void findVerticalTraversal(Node* node, int dist, auto &map)
    {
        if (node == nullptr) {
            return;
        }

        map.insert(make_pair(dist, node->data));

        findVerticalTraversal(node->left, dist - 1, map);
        findVerticalTraversal(node->right, dist + 1, map);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findVerticalTraversal(Node* root)
    {
        /*multimap<int, int> map; // map<int, vector<int>>

        findVerticalTraversal(root, 0, map);

        vector<int> res;
        int temp = 0;
        for (auto it = map.begin(); it != map.end(); ++it)
        {
            if (temp != it->first)
            {
                temp = it->first;
            }
            res.push_back(it->second);
        }

        return res;*/
        if (root == nullptr) {
            return {};
        }

        multimap<int, int> map;

        queue<pair<Node*, int>> q;
        q.push(make_pair(root, 0));

        while (!q.empty())
        {
            Node* node = q.front().first;
            int dist = q.front().second;
            q.pop();

            map.insert(make_pair(dist, node->data));

            if (node->left != nullptr) {
                q.push(make_pair(node->left, dist - 1));
            }

            if (node->right != nullptr) {
                q.push(make_pair(node->right, dist + 1));
            }
        }

        vector<int> res;
        int val = 0;
        for (auto it = map.begin(); it != map.end(); it++)
        {
            if (val != it->first)
            {
                cout << endl;
                val = it->first;
            }
            res.push_back(it->second);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);
    root->right->left->right->left = new Node(9);
    root->right->left->right->right = new Node(10);

    cout << Solution().findVerticalTraversal(root) << endl;

    return 0;
}
