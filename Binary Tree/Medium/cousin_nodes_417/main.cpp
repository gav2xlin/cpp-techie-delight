/*

Given the root of a binary tree and two tree nodes, x and y, check if x and y are cousins of each other. Two nodes of a binary tree are cousins of each other if they have different parents, but they are at the same level.

For example, consider the following binary tree.

             1
           /   \
         /		 \
        2		  3
      /  \		 /  \
     /	  \		/	 \
    4	   5   6	  7


Input: x = Node 4, y = Node 6
Output: true

Input: x = Node 5, y = Node 6
Output: true

Input: x = Node 2, y = Node 3
Output: false

Input: x = Node 4, y = Node 3
Output: false

Note: The solution should return false if either x or y is not the actual node in the tree.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    struct NodeInfo
    {
        Node* node;
        int level;
        Node* parent;
    };

    void updateLevelandParent(Node* root, Node* parent, int level, NodeInfo &x, NodeInfo &y)
    {
        if (root == nullptr) {
            return;
        }

        updateLevelandParent(root->left, root, level + 1, x, y);

        if (root == x.node)
        {
            x.level = level;
            x.parent = parent;
        }

        if (root == y.node)
        {
            y.level = level;
            y.parent = parent;
        }

        updateLevelandParent(root->right, root, level + 1, x, y);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isCousin(Node* root, Node* node1, Node* node2)
    {
        if (root == nullptr) {
            return false;
        }

        int level = 1;
        Node* parent = nullptr;

        NodeInfo x = {node1, level, parent};
        NodeInfo y = {node2, level, parent};

        updateLevelandParent(root, nullptr, 1, x, y);

        return x.level == y.level && x.parent != y.parent;
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    cout << boolalpha << Solution().isCousin(root, root->left->right, root->right->left) << endl;

    return 0;
}
