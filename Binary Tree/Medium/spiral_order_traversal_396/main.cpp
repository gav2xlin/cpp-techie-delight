/*

Given the root of a binary tree, return the spiral level order traversal of its nodes' values. The solution should consider the binary tree nodes level by level in spiral order, i.e., all nodes present at level 1 should be processed first from left to right, followed by nodes of level 2 from right to left, followed by nodes of level 3 from left to right and so on… In other words, odd levels should be processed from left to right, and even levels should be processed from right to left.

Input:
           1
         /   \
        /	  \
       2	   3
      /		  / \
     /	  	 /	 \
    4		5	  6
           / \
          /   \
         7	   8

Output: [1, 3, 2, 4, 5, 6, 8, 7]

*/

#include <iostream>
#include <vector>
#include <list>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findSpiralOrderTraversal(Node* root)
    {
        if (root == nullptr) {
            return {};
        }

        vector<int> order;

        list<Node*> dequeue;
        dequeue.push_back(root);

        Node* node;
        bool level = true;

        while (!dequeue.empty()) {
            int size = dequeue.size();

            while (size--) {
                if (level) {
                    node = dequeue.front();
                    dequeue.pop_front();
                } else {
                    node = dequeue.back();
                    dequeue.pop_back();
                }

                order.push_back(node->data);

                if (level) {
                    if (node->left != nullptr) {
                        dequeue.push_back(node->left);
                    }
                    if (node->right != nullptr) {
                        dequeue.push_back(node->right);
                    }
                } else {
                    if (node->right != nullptr) {
                        dequeue.push_front(node->right);
                    }
                    if (node->left != nullptr) {
                        dequeue.push_front(node->left);
                    }
                }
            }

            level = !level;
        }

        return order;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node n7{7};
    Node n8{8};
    Node n5{5, &n7, &n8};
    Node n6{6};
    Node n3{3, &n5, &n6};
    Node n4{4};
    Node n2{2, &n4, nullptr};
    Node n1{1, &n2, &n3};

    cout << Solution().findSpiralOrderTraversal(&n1) << endl;
    return 0;
}
