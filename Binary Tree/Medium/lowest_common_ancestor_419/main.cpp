/*

Given the root of a binary tree and two tree nodes, x and y, return the lowest common ancestor (LCA) of x and y in the binary tree.

The lowest common ancestor (LCA) of two nodes x and y in a binary tree is the lowest (i.e., deepest) node that has both x and y as descendants, where each node can be a descendant of itself (so if x is reachable from w, w is the LCA). In other words, the LCA of x and y is the shared ancestor of x and y that is located farthest from the root.

For example, consider the following binary tree.

           1
         /   \
        /	  \
       2	   3
       \	  / \
        \	 /	 \
         4	5	  6
           / \
          /   \
         7	   8

Input: x = Node 6, y = Node 7
Output: Node 3
Explanation: The common ancestors of nodes 6 and 7 are 1 and 3. Out of nodes 1 and 3, the LCA is 3 as it is farthest from the root.

Input: x = Node 5, y = Node 8
Output: Node 5
Explanation: Node 8 itself is descendant of node 5 (and node 5 can be a descendant of itself).

Input: x = Node 2, y = Node 5
Output: Node 1

Note: The solution should return nullptr if either x or y is not the actual node in the tree.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isNodePresent(Node* root, Node* node)
    {
        if (root == nullptr) {
            return false;
        }

        if (root == node) {
            return true;
        }

        return isNodePresent(root->left, node) || isNodePresent(root->right, node);
    }

    bool findLCA(Node* root, Node* &lca, Node* x, Node* y)
    {
        if (root == nullptr) {
            return false;
        }

        if (root == x || root == y)
        {
            lca = root;
            return true;
        }

        bool left = findLCA(root->left, lca, x, y);

        bool right = findLCA(root->right, lca, x, y);

        if (left && right) {
            lca = root;
        }

        return left || right;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* findLCA(Node* root, Node* x, Node* y)
    {
        Node* lca = nullptr;

        if (isNodePresent(root, y) && isNodePresent(root, x)) {
            findLCA(root, lca, x, y);
        }

        return lca;
    }
};

ostream& operator<<(ostream& os, const Node* node) {
    if (node != nullptr) {
        os << node->data;
    } else {
        os << "nullptr";
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->right = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    Solution s;

    cout << s.findLCA(root, root->right->left->left, root->right->right) << endl;
    cout << s.findLCA(root, root->right->left->left, new Node(10)) << endl;
    cout << s.findLCA(root, root->right->left->left, root->right->left->left) << endl;
    cout << s.findLCA(root, root->right->left->left, root->right->left) << endl;
    cout << s.findLCA(root, root->left, root->right->left) << endl;

    return 0;
}
