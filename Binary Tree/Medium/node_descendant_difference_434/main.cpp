/*

Given the root of a binary tree, return the maximum difference between a node and its descendants in the binary tree. You may assume that the binary tree contains at-least two nodes.

Input:
           6
         /   \
        /	  \
       3	   8
              / \
             /	 \
            2	  4
           / \
          /	  \
         1	   7

Output: 7

Explanation: The maximum difference between a node and its descendants is 8 - 1 = 7.

*/

#include <iostream>
#include <climits>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int findMaxDifference(Node* root, int &diff)
    {
        if (root == nullptr) {
            return INT_MAX;
        }

        int left = findMaxDifference(root->left, diff);
        int right = findMaxDifference(root->right, diff);

        int d = INT_MIN;
        if (min(left, right) != INT_MAX) {
            d = root->data - min(left, right);
        }

        diff = max(diff, d);

        return min(min(left, right), root->data);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int findMaximumDifference(Node* root)
    {
        int diff = INT_MIN;
        findMaxDifference(root, diff);

        return diff;
    }
};

int main()
{
    Node* root = new Node(6);
    root->left = new Node(3);
    root->right = new Node(8);
    root->right->left = new Node(2);
    root->right->right = new Node(4);
    root->right->left->left = new Node(1);
    root->right->left->right = new Node(7);

    cout << Solution().findMaximumDifference(root) << endl;

    return 0;
}
