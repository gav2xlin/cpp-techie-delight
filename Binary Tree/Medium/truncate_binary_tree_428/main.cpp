/*

Given the root of a binary tree, convert it into a full tree by removing half nodes i.e., nodes having one child. A full binary tree is a tree in which every node other than the leaves has two children.

Input:
             0
           /   \
          /		\
         1		 2
        /		/
       /	   /
      3		  4
     /		 / \
    /		/   \
   5	   6	 7

Output:

         0
       /   \
      /	 	\
     5		 4
            / \
           /   \
          6	 	7

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isLeaf(Node* node) {
        return (node->left == nullptr && node->right == nullptr);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* truncate(Node* root)
    {
        if (root == nullptr) {
            return nullptr;
        }

        root->left = truncate(root->left);
        root->right = truncate(root->right);

        if ((root->left && root->right) || isLeaf(root)) {
            return root;
        }

        Node* child = (root->left) ? root->left: root->right;
        delete root;

        return child;
    }
};

void inorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorder(root->left);
    cout << root->data << " ";
    inorder(root->right);
}

int main()
{
    Node* root = new Node(0);
    root->left = new Node(1);
    root->right = new Node(2);
    root->left->left = new Node(3);
    root->right->left = new Node(4);
    root->left->left->left = new Node(5);
    root->right->left->left = new Node(6);
    root->right->left->right = new Node(7);

    Solution().truncate(root);

    inorder(root);

    return 0;
}
