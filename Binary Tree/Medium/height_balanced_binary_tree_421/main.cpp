/*

Given the root of a binary tree, check if it is height-balanced or not. In a height-balanced tree, the absolute difference between the height of the left and right subtree for every node is 0 or 1.

Input:
           1
         /   \
        /	  \
       2	   3
       \	  / \
        \	 /	 \
         4	5	  6
           / \
          /   \
         7	   8

Output: true

Input:
           1
         /   \
        /	  \
       2	   3
              / \
             /	 \
            5	  6
           / \
          /   \
         7	   8

Output: false

*/

#include <iostream>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int isHeightBalanced(Node* root, bool &isBalanced)
    {
        if (root == nullptr || !isBalanced) {
            return 0;
        }

        int left_height = isHeightBalanced(root->left, isBalanced);
        int right_height = isHeightBalanced(root->right, isBalanced);

        if (abs(left_height - right_height) > 1) {
            isBalanced = false;
        }

        return max(left_height, right_height) + 1;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isHeightBalanced(Node* root)
    {
        bool isBalanced = true;
        isHeightBalanced(root, isBalanced);
        return isBalanced;
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);

    cout << boolalpha << Solution().isHeightBalanced(root) << endl;

    return 0;
}
