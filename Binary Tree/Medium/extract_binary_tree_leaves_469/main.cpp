/*

Given the root of a binary tree, extract all its leaves into a doubly-linked list, i.e., remove all leaf nodes from the binary tree and construct a doubly linked list out of them.

The solution should process the left child before its right child for each tree node. The extraction should be by rearranging the pointers of the binary tree such that the left pointer should act as the previous pointer, and the right pointer should serve as the next pointer for the doubly linked list node.

Input:

             1
           /   \
         /		 \
        2		  3
       / \		 / \
      /	  \		/	\
     4	   5   6	 7
    / \		  / \
   /   \	 /   \
  8		9	10	 11

Output:

             1
           /   \
         /		 \
        2		  3
       /		 /
      /			/
     4		   6

8 ⇔ 9 ⇔ 5 ⇔ 10 ⇔ 11 ⇔ 7

The solution should return the head of the doubly-linked list and detach the tree from all its leaf nodes.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

void preorder(Node* root);
void printDDL(Node* head);

class Solution
{
private:
    bool isLeaf(Node* root) {
        return root != nullptr && root->left == nullptr && root->right == nullptr;
    }

    Node* constructInOrder(Node* root, Node* &head, Node* &tail)
    {
        if (root == nullptr) {
            return nullptr;
        }

        bool is_leaf = isLeaf(root);

        root->left = constructInOrder(root->left, head, tail);

        if (is_leaf)
        {
            if (head == nullptr)
            {
                head = tail = root;
            }
            else
            {
                root->left = tail;

                tail->right = root;

                tail = root;
            }

            return nullptr;
        }

        root->right = constructInOrder(root->right, head, tail);

        return root;
    }

    Node* constructReverseInOrder(Node* root, Node* &headRef)
    {
        if (root == nullptr) {
            return nullptr;
        }

        bool is_leaf = isLeaf(root);

        root->right = constructReverseInOrder(root->right, headRef);

        if (is_leaf)
        {
            root->right = headRef;

            if (headRef != nullptr) {
                headRef->left = root;
            }

            headRef = root;

            return nullptr;
        }

        root->left = constructReverseInOrder(root->left, headRef);

        return root;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* extractLeaves(Node* &root)
    {
        Node* head = nullptr;
        Node* tail = nullptr;

        root = constructInOrder(root, head, tail);
        //root = constructReverseInOrder(root, head);

        /*cout << "Extracted doubly linked list is ";
        printDDL(head);

        cout << "\nPreorder traversal of the final tree is ";
        preorder(root);
        cout << endl;*/

        return head;
    }
};

void preorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    cout << root->data << ' ';
    preorder(root->left);
    preorder(root->right);
}

void printDDL(Node* head)
{
    while (head)
    {
        cout << head->data << ' ';
        head = head->right;
    }
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    root->left->left->left = new Node(8);
    root->left->left->right = new Node(9);

    root->right->left->left = new Node(10);
    root->right->left->right = new Node(11);

    cout << Solution().extractLeaves(root) << endl;

    return 0;
}
