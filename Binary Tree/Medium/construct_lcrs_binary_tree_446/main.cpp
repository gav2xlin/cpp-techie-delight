/*

Given the root of a binary tree, convert it into a Left–child right–sibling (LC–RS) binary tree.

Each node in the LC–RS binary tree has two pointers: one to the node's left child and one to its next sibling in the original binary tree. So starting with the root, each node's leftmost child in the original tree is made its left child in the LC–RS binary tree, and its nearest sibling to the right in the original tree is made its right child in the binary tree.

Input:

             1
           /   \
         /		 \
        2		  3
      /  \		 /
     /	  \		/
    4	   5   6
              / \
             /	 \
            7	  8

Output:

            1
          /
        /
       2
     /   \
   /	   \
  4			3
   \	   /
    \	  /
     5   6
        /
       /
      7
       \
        \
         8

Explanation:

Rewrite the binary tree shown by putting the left child node to one level below its parents and by placing the sibling next to the left child at the same level.

             1
           /
         /
        2 ------- 3
      /  \
     /	  \
    4 ---- 5 -- 6
               /
              /
             7 --- 8

Then transform this tree into a LC-RS binary tree by turning each sibling 45° clockwise.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void buildLCRSBinaryTree(Node* root)
    {
        if (root == nullptr) {
            return;
        }

        buildLCRSBinaryTree(root->left);
        buildLCRSBinaryTree(root->right);

        if (root->left == nullptr)
        {
            root->left = root->right;
            root->right = nullptr;
        } else {
            root->left->right = root->right;
            root->right = nullptr;
        }
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    cout << "before:\n" << *root << endl;

    Solution().buildLCRSBinaryTree(root);

    cout << "after:\n" << *root << endl;

    return 0;
}
