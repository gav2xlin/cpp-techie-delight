/*

Given the root of two binary trees, x and y, determine whether y is a subtree of x. A subtree of a tree T is a tree consisting of a node in T and all of its descendants in T.

Input:
           1						3
         /   \					  /   \
        /	  \					 /	   \
       2	   3				6		7
      /	\	  / \
     /	 \ 	 /	 \
    4	  5	6	  7

Output: true
Explanation: y is a subtree of x as y = x.right.

Input:
           1						2
         /   \					  /   \
        /	  \					 /	   \
       2	   3				4		5
      /	\	  / \			   /
     /	 \ 	 /	 \			  /
    4	  5	6	  7			 8

Output: false

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void inorder(Node* node, vector<int> &vc)
    {
        if (node == nullptr) {
            return;
        }

        inorder(node->left, vc);
        vc.push_back(node->data);
        inorder(node->right, vc);
    }

    void postorder(Node* node, vector<int> &vc)
    {
        if (node == nullptr) {
            return;
        }

        postorder(node->left, vc);
        postorder(node->right, vc);
        vc.push_back(node->data);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isSubtree(Node* tree, Node* subtree)
    {
        if (tree == subtree) {
            return true;
        }

        if (tree == nullptr) {
            return false;
        }

        vector<int> first, second;
        inorder(tree, first);
        inorder(subtree, second);

        auto it = search(first.begin(), first.end(), second.begin(), second.end());
        if (it == first.end()) {
            return false;
        }

        first.erase(first.begin(), first.end());
        second.erase(second.begin(), second.end());

        postorder(tree, first);
        postorder(subtree, second);

        it = search(first.begin(), first.end(), second.begin(), second.end());
        if (it == first.end()) {
            return false;
        }

        return true;
    }
};

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    cout << boolalpha << Solution().isSubtree(root, root->right) << endl;

    return 0;
}
