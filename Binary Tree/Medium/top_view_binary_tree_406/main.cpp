/*

Given the root of a binary tree, return the top view of its nodes' values. Assume the left and right child of a node makes a 45–degree angle with the parent.

Input:
           1
         /	 \
        /	  \
       2	   3
             /   \
            /	  \
           5	   6
         /   \
        /	  \
       7	   8

Output: [2, 1, 3, 6]

Input:

      1
    /   \
   /	 \
  2		  3
   \
    \
     4
      \
       \
        5

Output: [2, 1, 3]

*/

#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void findTopView(Node* root, int dist, int level, auto &map)
    {
        if (root == nullptr) {
            return;
        }

        if (map.find(dist) == map.end() || level < map[dist].second)
        {
            map[dist] = { root->data, level };
        }
        findTopView(root->left, dist - 1, level + 1, map);
        findTopView(root->right, dist + 1, level + 1, map);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findTopView(Node* root)
    {
        map<int, pair<int, int>> map;
        findTopView(root, 0, 0, map);

        vector<int> res;
        for (auto it: map) {
            res.push_back(it.second.first);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->right = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    cout << Solution().findTopView(root) << endl;

    return 0;
}
