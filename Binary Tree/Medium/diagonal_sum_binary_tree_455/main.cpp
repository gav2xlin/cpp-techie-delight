/*

Given the root of a binary tree, return the sum of all nodes for each diagonal having negative slope `\`. Assume that the left and right child of a node makes a 45–degree angle with the parent.

Input:
                 1
             .		 .
           .		   .
         2				 3
       .			   .	.
     .				 .		  .
   4			   5			6
                 .   .
               .	   .
             7			 8

Output: [10, 15, 11]

Explanation: The binary tree has three diagonals - [1, 3, 6], [2, 5, 8], and [4, 7]. The sum of diagonals is 10, 15, and 11 respectively.

*/


#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void findDiagonalSum(Node* root, int diagonal, auto &map)
    {
        if (root == nullptr) {
            return;
        }

        map[diagonal] += root->data;

        findDiagonalSum(root->left, diagonal + 1, map);
        findDiagonalSum(root->right, diagonal, map);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findDiagonalSum(Node* root)
    {
        unordered_map<int, int> map;

        findDiagonalSum(root, 0, map);

        vector<int> res;
        for (int i = 0; i < map.size(); ++i) {
            res.push_back(map[i]);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->right->left = new Node(5);
    root->right->right = new Node(6);
    root->right->left->left = new Node(7);
    root->right->left->right = new Node(8);

    cout << Solution().findDiagonalSum(root) << endl;

    return 0;
}
