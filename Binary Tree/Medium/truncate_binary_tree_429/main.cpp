/*

Given the root of a binary tree and a positive number k, remove nodes from the tree which lie on a complete path having a sum less than k. Since a node can be part of multiple paths, delete it only if all paths from it have a sum less than k.

A complete path in a binary tree is defined as a path from the root to a leaf. The sum of all nodes on that path is defined as the sum of that path.


Input: Below binary tree, k = 20

         6
       /   \
      /		\
     3		 8
           /   \
          /		\
         4		 2
       /   \	  \
      /		\	   \
     1		 7		3

Output:

      6
       \
        \
         8
        /
       /
      4
       \
        \
         7

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isLeaf(Node* node) {
        return (node->left == nullptr && node->right == nullptr);
    }

    void truncate(Node* &curr, int k, int target)
    {
        if (curr == nullptr) {
            return;
        }

        target = target + (curr->data);

        truncate(curr->left, k, target);
        truncate(curr->right, k, target);

        if (target < k && isLeaf(curr))
        {
            delete(curr);

            curr = nullptr;
        }
    };
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void truncate(Node* &root, int k)
    {
        int target = 0;
        truncate(root, k, target);
    }
};

void inorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorder(root->left);
    cout << root->data << " ";
    inorder(root->right);
}

int main()
{
    Node* root = new Node(6);
    root->left = new Node(3);
    root->right = new Node(8);
    root->right->left = new Node(4);
    root->right->right = new Node(2);
    root->right->left->left = new Node(1);
    root->right->left->right = new Node(7);
    root->right->right->right = new Node(3);

    Solution().truncate(root, 20);

    inorder(root);

    return 0;
}
