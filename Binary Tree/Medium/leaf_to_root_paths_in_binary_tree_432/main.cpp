/*

Given the root of a binary tree, return all paths from every leaf node to the root node in the binary tree.

Input:

             1
           /   \
         /		 \
        2		  3
       / \		 / \
      /	  \		/	\
     4	   5   6	 7
              / \
             /	 \
            8	  9

Output: {[4, 2, 1], [5, 2, 1], [8, 6, 3, 1], [9, 6, 3, 1], [7, 3, 1]}

Explanation: The binary tree has five leaf-to-root paths:

4 —> 2 —> 1
5 —> 2 —> 1
8 —> 6 —> 3 —> 1
9 —> 6 —> 3 —> 1
7 —> 3 —> 1

*/

#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void findLeafToRootPaths(Node* node, vector<int> &path, set<vector<int>>& paths)
    {
        if (node == nullptr) {
            return;
        }

        path.push_back(node->data);

        if (isLeaf(node)) {
            paths.insert({path.rbegin(), path.rend()});
        }

        findLeafToRootPaths(node->left, path, paths);
        findLeafToRootPaths(node->right, path, paths);

        path.pop_back();
    }

    bool isLeaf(Node* node) {
        return (node->left == nullptr && node->right == nullptr);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    set<vector<int>> findLeafToRootPaths(Node* node)
    {
        set<vector<int>> paths;

        vector<int> path;
        findLeafToRootPaths(node, path, paths);

        return paths;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& sets) {
    for (auto& s : sets) {
        os << s << endl;
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);
    root->right->left->left = new Node(8);
    root->right->left->right = new Node(9);

    cout << Solution().findLeafToRootPaths(root) << endl;
    return 0;
}
