/*

Given the root of a binary tree, check if the binary tree is a sum tree or not. In a sum tree, each non-leaf node's value is equal to the sum of all elements present in its left and right subtree. The value of a leaf node can be anything and the value of an empty child node is considered to be 0.

Input:

             44
           /	\
          /		 \
         9		  13
       /   \	 /  \
      /		\	/	 \
     4		 5 6	  7

Output: true

Explanation: All non-leaf nodes follows the sum tree property, as shown below:

             44 (4+5+9)+(6+7+13)
           /	\
          /		 \
   (4+5) 9		  13 (6+7)
       /   \	 /  \
      /		\	/	 \
     4		 5 6	  7

*/

#include <iostream>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    int _isSumTree(Node* root)
    {
        if (root == nullptr) {
            return 0;
        }

        if (root->left == nullptr && root->right == nullptr) {
            return root->data;
        }

        int left = _isSumTree(root->left);
        int right = _isSumTree(root->right);

        // if the root's value is equal to the sum of all elements present in its
        // left and right subtree
        if (left != INT_MIN && right != INT_MIN && root->data == left + right) {
            return 2 * root->data;
        }

        return INT_MIN;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isSumTree(Node* root)
    {
        return _isSumTree(root) != INT_MIN;
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    Node* root = new Node(44);
    root->left = new Node(9);
    root->right = new Node(13);
    root->left->left = new Node(4);
    root->left->right = new Node(5);
    root->right->left = new Node(6);
    root->right->right = new Node(7);

    cout << boolalpha << Solution().isSumTree(root) << endl;

    return 0;
}
