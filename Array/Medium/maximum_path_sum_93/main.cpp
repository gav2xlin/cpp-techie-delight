/*

Given two sorted arrays of integers, find a maximum sum path involving elements of both arrays whose sum is maximum. You can start from either array, but can switch between arrays only through its common elements.

Input:

X = [3, 6, 7, 8, 10, 12, 15, 18, 100]
Y = [1, 2, 3, 5, 7, 9, 10, 11, 15, 16, 18, 25, 50]

Output: The maximum sum is 199

Explanation: The maximum sum path is 1 —> 2 —> 3 —> 6 —> 7 —> 9 —> 10 —> 12 —> 15 —> 16 —> 18 —> 100

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaxPathSum(vector<int> const &X, vector<int> const &Y)
    {
        int sum = 0;
        int sum_x = 0, sum_y = 0;

        int i = 0, j = 0, m = X.size(), n = Y.size();
        while (i < m && j < n)
        {
            while (i < m-1 && X[i] == X[i+1]) {
                sum_x += X[i++];
            }

            while (j < n-1 && Y[j] == Y[j+1]) {
                sum_y += Y[j++];
            }

            if (Y[j] < X[i])
            {
                sum_y += Y[j];
                j++;
            } else if (X[i] < Y[j])
            {
                sum_x += X[i];
                i++;
            } else {
                sum += max(sum_x, sum_y) + X[i];

                ++i, ++j;
                sum_x = 0, sum_y = 0;
            }
        }

        while (i < m) {
            sum_x += X[i++];
        }

        while (j < n) {
            sum_y += Y[j++];
        }

        sum += max(sum_x, sum_y);
        return sum;
    }
};

int main()
{
    cout << Solution().findMaxPathSum({3, 6, 7, 8, 10, 12, 15, 18, 100}, {1, 2, 3, 5, 7, 9, 10, 11, 15, 16, 18, 25, 50}) << endl;

    return 0;
}
