/*

Given an integer array, find k'th smallest element in the array where k is a positive integer less than or equal to the length of array.

Input : [7, 4, 6, 3, 9, 1], k = 3
Output: 4
Explanation: The 3rd smallest array element is 4

Input : [1, 5, 2, 2, 2, 5, 5, 4], k = 5
Output: 4
Explanation: The 5th smallest array element is 4

*/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Solution
{
public:
    int findKthSmallest(vector<int> const &nums, int k)
    {
        /*if (nums.size() < k) {
            exit(-1);
        }

        priority_queue<int, vector<int>> pq(nums.begin(), nums.begin() + k); // max-heap

        for (int i = k; i < nums.size(); ++i)
        {
            if (nums[i] < pq.top())
            {
                pq.pop();
                pq.push(nums[i]);
            }
        }

        return pq.top();*/
        if (nums.size() < k) {
            exit(-1);
        }

        priority_queue<int, vector<int>, greater<int>> pq(nums.begin(), nums.end()); // min-heap

        while (--k) {
            pq.pop();
        }

        return pq.top();
    }
};

int main()
{
    cout << Solution().findKthSmallest({7, 4, 6, 3, 9, 1}, 3) << endl;
    cout << Solution().findKthSmallest({1, 5, 2, 2, 2, 5, 5, 4}, 5) << endl;

    return 0;
}
