/*

Given an array of distinct integer, in-place shuffle the array. The solution should produce an unbiased permutation, i.e., every permutation is equally likely.

Input: [1, 2, 3, 4, 5]
Output: [5, 4, 2, 1, 3] or [4, 1, 5, 3, 2] or any other unbiased permutation.

*/

#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <utility>

using namespace std;

class Solution
{
public:
    void shuffle(vector<int> &nums)
    {
        int n = nums.size();
        for (int i = n - 1; i >= 1; i--)
        {
            int j = rand() % (i + 1);

            swap(nums[i], nums[j]);
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    srand(time(nullptr));

    vector<int> nums{1, 2, 3, 4, 5};

    cout << "before: " << nums << endl;

    Solution().shuffle(nums);

    cout << "after: " << nums << endl;

    return 0;
}
