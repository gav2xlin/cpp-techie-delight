/*

Given an unsorted integer array, find all non-overlapping pairs in it having the same sum.

Input : [3, 7, 6, 2]
Output: {{(7, 2), (3, 6)}}
Explanation: The pairs (7, 2) and (3, 6) are non-overlapping (having different indices) and have the same sum.

Input : [3, 4, 7, 4]
Output: {}
Explanation: No non-overlapping pairs are present in the array. The pairs (3, 4) and (3, 4) are overlapping as the index of 3 is the same in both pairs.

• The input can contain multiple non-overlapping pairs with the same sum, the solution should return a set containing all the distinct pairs.

Input : [3, 4, 7, 6, 1]
Output: {{(6, 1), (3, 4)}, {(4, 6), (3, 7)}}

Input : [3, 7, 6, 2, 4, 5]
Output: {{(7, 2), (3, 6)}, {(6, 2), (3, 5)}, {(6, 4), (3, 7)}, {(6, 5), (7, 4)}, {(2, 5), (3, 4)}, {(4, 5), (3, 6)}, {(4, 5), (7, 2)}}

Note: The solution should return all pairs in the same order as they appear in the array.

*/

#include <iostream>
#include <set>
#include <vector>
#include <unordered_map>
#include <utility>

using namespace std;

class Solution
{
public:
    set<set<pair<int,int>>> findPairs(vector<int> const &nums)
    {
        set<set<pair<int,int>>> result;

        // key —> sum of a pair of elements in the array
        // value —> vector storing an index of every pair having that sum
        unordered_map<int, vector<pair<int,int>>> map;

        // consider every pair (nums[i], nums[j]), where `j > i`
        int n = nums.size();
        for (int i = 0; i < n - 1; ++i)
        {
            for (int j = i + 1; j < n; ++j)
            {
                int sum = nums[i] + nums[j];

                if (map.find(sum) != map.end())
                {
                    for (auto& p: map.find(sum)->second)
                    {
                        int m = p.first, n = p.second;

                        if ((m != i && m != j) && (n != i && n != j))
                        {
                            set<pair<int,int>> s;
                            s.insert(make_pair(nums[i], nums[j]));
                            s.insert(make_pair(nums[m], nums[n]));
                            result.insert(s);
                            //return;
                        }
                    }
                }

                map[sum].push_back({i, j});
            }
        }

        return result;
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ':' << p.second;
    return os;
}

ostream& operator<<(ostream& os, const set<pair<int,int>>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<set<pair<int,int>>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findPairs({3, 7, 6, 2}) << endl;
    cout << Solution().findPairs({3, 4, 7, 4}) << endl;
    cout << Solution().findPairs({3, 4, 7, 6, 1}) << endl;
    cout << Solution().findPairs({3, 7, 6, 2, 4, 5}) << endl;

    return 0;
}
