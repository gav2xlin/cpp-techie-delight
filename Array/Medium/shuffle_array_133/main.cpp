/*

Given an array of distinct integers `nums`, in-place shuffle it according to order specified by another array `pos`. For every index `i` in `nums`, the solution should update `nums[pos[i]] = nums[i]`.

Input:

nums[] = [1, 2, 3, 4, 5]
 pos[] = [3, 2, 4, 1, 0]

Output: [5, 4, 2, 1, 3]

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    void shuffle(vector<int> &nums, vector<int> &pos)
    {
        unordered_map<int, int> map;

        for (int i = 0; i < nums.size(); ++i) {
            map[pos[i]] = nums[i];
        }

        for (auto &p: map) {
            nums[p.first] = p.second;
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{1, 2, 3, 4, 5};
    vector<int> pos{3, 2, 4, 1, 0};

    cout << "before:\nnums = " << nums << "\npos = " << pos << endl;

    Solution().shuffle(nums, pos);

    cout << "after:\nnums = " << nums << endl;

    return 0;
}
