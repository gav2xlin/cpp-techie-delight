/*

Given an integer array `X`, which is constructed by taking the sum of every distinct pair from another array `Y`, decode `X` to get the original array back. If the original array is `Y[0], Y[1], … , Y[n-1]`, then the input array `X` is:

[
  (Y[0] + Y[1]), (Y[0] + Y[2]), … (Y[0] + Y[n-1]),
  (Y[1] + Y[2]), (Y[1] + Y[3]), … (Y[1] + Y[n-1]),
  …
  …
  (Y[i] + Y[i+1]), (Y[i] + Y[i+2]), … (Y[i] + Y[n-1]),
  …
  …
  (Y[n-2] + Y[n-1])
]

Input : [3, 4, 5, 5, 6, 7]
Output: [1, 2, 3, 4]
Explanation: The input array [3, 4, 5, 5, 6, 7] is formed by the sum of every distinct pair of the output array [(1 + 2), (1 + 3), (1 + 4), (2 + 3), (2 + 4), (3 + 4)]

Input : [3, 4, 5, 6, 5, 6, 7, 7, 8, 9]
Output: [1, 2, 3, 4, 5]

Input : [3, 4, 5]
Output: [1, 2, 3]


Note: Assume valid input and input size > 2

*/

#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class Solution
{
public:
    vector<int> decode(vector<int> const &inp)
    {
        int m = inp.size();
        if (m == 0 || m == 2) {
            return {};
        }

        int n = (sqrt(8 * m + 1) + 1) / 2;

        vector<int> A(n);

        if (n == 1 || m == 1) {
            A[0] = inp[0];
        } else if (n == 2) {
            A[0] = inp[0] - inp[1];
        } else {
            A[0] = (inp[0] + inp[1] - inp[n - 1]) / 2;
        }

        for (int i = 1; i < n; ++i) {
            A[i] = inp[i - 1] - A[0];
        }

        return A;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto& v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().decode({3, 4, 5, 5, 6, 7}) << endl;
    cout << Solution().decode({3, 4, 5, 6, 5, 6, 7, 7, 8, 9}) << endl;
    cout << Solution().decode({3, 4, 5}) << endl;

    return 0;
}
