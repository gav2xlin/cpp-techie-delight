/*

Given an integer array `nums` and non-negative integers i, j, and m, reverse every group of consecutive `m` elements in subarray `[i, j]`.

Input : nums[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], m = 3, i = 1, j = 7
Output: [1, 4, 3, 2, 7, 6, 5, 8, 9, 10]

Input : nums[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], m = 3, i = 1, j = 9
Output: [1, 4, 3, 2, 7, 6, 5, 10, 9, 8]

Input : nums[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], m = 3, i = 3, j = 5
Output: [1, 2, 3, 6, 5, 4, 7, 8, 9, 10]

Input : nums[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], m = 3, i = 3, j = 4
Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
private:
    void reverse_subarray(vector<int> &arr, int i, int j)
    {
        while (i < j)
        {
            swap(arr[i], arr[j]);
            ++i, --j;
        }
    }
public:
    void reverseInGroup(vector<int> &nums, int m, int b, int e)
    {
        if (m <= 1) {
            return;
        }

        if (m > e - b + 1) {
            return;
        }

        for (int i = b; i <= e; i += m)
        {
            if (i + m - 1 <= e) {
                //reverse_subarray(nums, i, i + m - 1);
                reverse(nums.begin() + i, nums.begin() + i + m);
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        vector<int> nums{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        cout << "before: " << nums << endl;
        Solution().reverseInGroup(nums, 3, 1, 7);
        cout << "after: " << nums << endl;
    }

    {
        vector<int> nums{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        cout << "before: " << nums << endl;
        Solution().reverseInGroup(nums, 3, 1, 9);
        cout << "after: " << nums << endl;
    }

    {
        vector<int> nums{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        cout << "before: " << nums << endl;
        Solution().reverseInGroup(nums, 3, 3, 5);
        cout << "after: " << nums << endl;
    }

    {
        vector<int> nums{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        cout << "before: " << nums << endl;
        Solution().reverseInGroup(nums, 3, 3, 4);
        cout << "after: " << nums << endl;
    }

    return 0;
}
