/*

Given an integer array and a positive integer `k`, find the minimum sum contiguous subarray of size `k`.

Input : nums[] = [10, 4, 2, 5, 6, 3, 8, 1], k = 3
Output: [4, 2, 5]

Input : nums[] = [1, 4, 5, 3, 8], k = 6
Output: [1, 4, 5, 3, 8]

Note: Since an input can contain several minimum sum subarrays of size `k`, the solution can return any one of them.

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Solution
{
public:
    vector<int> findMinSumSubarray(vector<int> const &nums, int k)
    {
        int n = nums.size();
        if (n == 0 || n <= k) {
            return nums;
        }

        int window_sum = 0;
        int min_window = INT_MAX;
        int last = 0;

        for (int i = 0; i < n; ++i)
        {
            window_sum += nums[i];

            if (i + 1 >= k)
            {
                if (min_window > window_sum)
                {
                    min_window = window_sum;
                    last = i;
                }

                window_sum -= nums[i + 1 - k];
            }
        }

        return {nums.begin() + last - k + 1, nums.begin() + last + 1};
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findMinSumSubarray({10, 4, 2, 5, 6, 3, 8, 1}, 3) << endl;
    cout << Solution().findMinSumSubarray({1, 4, 5, 3, 8}, 5) << endl;

    return 0;
}
