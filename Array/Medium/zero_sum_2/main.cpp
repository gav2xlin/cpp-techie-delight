/*

Given an integer array, check if it contains a contiguous subarray having zero-sum.

Input : [3, 4, -7, 3, 1, 3, 1, -4, -2, -2]
Output: true
Explanation: The subarrays with zero-sum are

[3, 4, -7]
[4, -7, 3]
[-7, 3, 1, 3]
[3, 1, -4]
[3, 1, 3, 1, -4, -2, -2]
[3, 4, -7, 3, 1, 3, 1, -4, -2, -2]

Input : [4, -7, 1, -2, -1]
Output: false
Explanation: The subarray with zero-sum doesn't exist.

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    bool hasZeroSumSubarray(vector<int> const &nums)
    {
        unordered_set<int> sums;
        sums.insert(0);

        int sum = 0;
        for (auto v : nums) {
            sum += v;
            if (sums.count(sum)) {
                return true;
            } else {
                sums.insert(sum);
            }
        }

        return false;
    }
};

int main()
{
    cout << boolalpha << Solution().hasZeroSumSubarray({3, 4, -7, 3, 1, 3, 1, -4, -2, -2}) << endl;
    cout << Solution().hasZeroSumSubarray({4, -7, 1, -2, -1}) << endl;

    return 0;
}
