/*

Given an integer array, in-place rearrange it such that every second element becomes greater than its left and right elements.

• Assume that no duplicate elements are present in the input array.
• The solution should perform single traveral of the array.
• In case the multiple rearrangement exists, the solution can return any one of them.

Input : [1, 2, 3, 4, 5, 6, 7]
Output: [1, 3, 2, 5, 4, 7, 6] or [1, 5, 2, 6, 3, 7, 4], or any other valid combination..

Input : [6, 9, 2, 5, 1, 4]
Output: [6, 9, 2, 5, 1, 4] or [1, 5, 2, 6, 4, 9], or any other valid combination..

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    void rearrange(vector<int> &nums)
    {
        int n = nums.size();
        for (int i = 1; i < nums.size(); i += 2)
        {
            if (nums[i - 1] > nums[i]) {
                swap(nums[i - 1], nums[i]);
            }

            if (i + 1 < n && nums[i + 1] > nums[i]) {
                swap(nums[i + 1], nums[i]);
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Solution s;

    {
        vector<int> nums{1, 2, 3, 4, 5, 6, 7};
        cout << "before: " << nums << endl;
        s.rearrange(nums);
        cout << "after: " << nums << endl;
    }

    {
        vector<int> nums{6, 9, 2, 5, 1, 4};
        cout << "before: " << nums << endl;
        s.rearrange(nums);
        cout << "after: " << nums << endl;
    }

    return 0;
}
