/*

Given an unsorted integer array, find a triplet that sum to zero.

Input : [4, -1, 1, 2, -1]
Output: [-1, 2, -1]

Input : [4, 5, 4, -1, 3]
Output: []
Explanation: No triplet exists with zero-sum.

If the input contains several triplets with zero-sum, the solution can return any one of them.

Input : nums[] = [-5, 2, 4, 0, 9, 5, 1, -3]
Output: [0, -5, 5] or [-5, 4, 1] or [1, 2, -3]

Note: The solution can return the triplet in any order, not necessarily as they appear in the array.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    vector<int> findTriplets(vector<int> const &_nums)
    {
        vector<int> nums(_nums);
        sort(nums.begin(), nums.end());

        int n = nums.size(), target = 0;
        for (int i = 0; i <= n - 3; ++i)
        {
            int k = target - nums[i];
            int low = i + 1, high = n - 1;

            while (low < high)
            {
                if (nums[low] + nums[high] < k) {
                    ++low;
                } else if (nums[low] + nums[high] > k) {
                    --high;
                } else {
                    return {nums[low], nums[high], nums[i]};
                }
            }
        }

        return {};
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findTriplets({4, -1, 1, 2, -1}) << endl;
    cout << Solution().findTriplets({4, 5, 4, -1, 3}) << endl;
    cout << Solution().findTriplets({-5, 2, 4, 0, 9, 5, 1, -3}) << endl;

    return 0;
}
