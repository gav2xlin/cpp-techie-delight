/*

Given a set of pairs of integers, find all symmetric pairs, i.e., pairs that mirror each other. For instance, pairs `(x, y)` and `(y, x)` are mirrors of each other.

Input : {(1, 2), (5, 2), (3, 4), (7, 10), (4, 3), (2, 5)}
Output: {{(2, 5), (5, 2)}, {(3, 4), (4, 3)}}
Explanation: The pairs (2, 5) and (5, 2) mirror each other. Similarly, pairs (3, 4) and (4, 3) are symmetric.

Input : {(1, 2), (5, 2), (3, 4)}
Output: {}
Explanation: No pairs mirror each other

*/

#include <iostream>
#include <set>
#include <utility>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    set<set<pair<int,int>>> findSymmetricPairs(set<pair<int,int>> const &pairs)
    {
        set<set<pair<int,int>>> result;

        unordered_set<string> set;
        for (auto& p : pairs)
        {
            set.insert("{" + to_string(p.first) + ", " + to_string(p.second) + "}");

            if (set.find("{" + to_string(p.second) + ", " + to_string(p.first) + "}") != set.end()) {
                result.insert({make_pair(p.first, p.second), make_pair(p.second, p.first)});
            }
        }

        return result;
    }
};

ostream& operator<<(ostream& os, const set<pair<int,int>>& values) {
    for (auto& v : values) {
        os << v.first << ' ' << v.second << endl;
    }
    return os;
}

ostream& operator<<(ostream& os, const set<set<pair<int,int>>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    {
        set<pair<int,int>> pairs {{1, 2}, {5, 2}, {3, 4}, {7, 10}, {4, 3}, {2, 5}};

        cout << Solution().findSymmetricPairs(pairs) << endl;
    }

    {
        set<pair<int,int>> pairs {{1, 2}, {5, 2}, {3, 4}};

        cout << Solution().findSymmetricPairs(pairs) << endl;
    }

    return 0;
}
