/*

Given an integer array, find a contiguous subarray having sum `k` in it where `k` is an integer.

Input : nums[] = [2, 6, 0, 9, 7, 3, 1, 4, 1, 10], k = 15
Output: [6, 0, 9]

Input : nums[] = [0, 5, -7, 1, -4, 7, 6, 1, 4, 1, 10], k = -3
Output: [1, -4] or [-7, 1, -4, 7]

Note: Since an input can contain several subarrays having sum `k`, the solution can return any one of them.

Input : nums[] = [0, 5, -7, 1, -4, 7, 6, 1, 4, 1, 10], k = 15
Output: [1, -4, 7, 6, 1, 4] or [4, 1, 10]

*/

#include <iostream>
#include <vector>
#include <cstdio>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    vector<int> findSubarray(vector<int> const &nums, int target)
    {
        /*int windowSum = 0;
        int low = 0, high = 0;

        int n = nums.size();
        for (low = 0; low < n; ++low)
        {
            while (windowSum < target && high < n)
            {
                windowSum += nums[high];
                ++high;
            }

            if (windowSum == target)
            {
                //printf("Subarray found [%d–%d]\n", low, high - 1);
                //return;
                if (low < high) {
                    return {nums.begin() + low, nums.begin() + high};
                } else {
                    return {nums.begin() + high, nums.begin() + low - 1};
                }
            }

            windowSum -= nums[low];
        }

        return {};*/
        unordered_map<int, int> map;

        // insert (0, -1) pair into the set to handle the case when a
        // subarray with the given sum starts from index 0
        map.insert(pair<int, int>(0, -1));

        int sum_so_far = 0;

        int n = nums.size();
        for (int i = 0; i < n; ++i)
        {
            sum_so_far += nums[i];

            if (map.find(sum_so_far - target) != map.end())
            {
                //cout << "Subarray found [" << map[sum_so_far - target] + 1 << "–" << i << "]" << endl;
                return {nums.begin() + map[sum_so_far - target] + 1, nums.begin() + i + 1};
            }

            map.insert(pair<int, int>(sum_so_far, i));
        }

        return {};
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findSubarray({2, 6, 0, 9, 7, 3, 1, 4, 1, 10}, 15) << endl;
    cout << Solution().findSubarray({0, 5, -7, 1, -4, 7, 6, 1, 4, 1, 10}, -3) << endl;
    cout << Solution().findSubarray({0, 5, -7, 1, -4, 7, 6, 1, 4, 1, 10}, 15) << endl;

    return 0;
}
