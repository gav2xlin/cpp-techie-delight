/*

Given a sorted array of distinct positive integers, return all triplets that forms an arithmetic progression with an integral common difference. An arithmetic progression is a sequence of numbers such that the difference between the consecutive terms is constant.

Input : [5, 8, 9, 11, 12, 15]
Output: {[5, 8, 11], [9, 12, 15]}

Input : [1, 3, 5, 6, 8, 9, 15]
Output: {[1, 3, 5], [1, 5, 9], [3, 6, 9], [1, 8, 15], [3, 9, 15]}

*/

#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Solution
{
public:
    set<vector<int>> findTriplets(vector<int> const &nums)
    {
        set<vector<int>> triplets;

        int n = nums.size();
        for (int j = 1; j < n - 1; ++j)
        {
            int i = j - 1, k = j + 1;

            while (i >= 0 && k < n)
            {
                if (nums[i] + nums[k] == 2 * nums[j])
                {
                    triplets.insert({nums[i], nums[j], nums[k]});

                    ++k, --i;
                } else if (nums[i] + nums[k] < 2 * nums[j]) {
                    ++k;
                } else {
                    --i;
                }
            }
        }

        return triplets;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}
ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findTriplets({5, 8, 9, 11, 12, 15}) << endl;
    cout << Solution().findTriplets({1, 3, 5, 6, 8, 9, 15}) << endl;

    return 0;
}
