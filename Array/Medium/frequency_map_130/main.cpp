/*

Given an unsorted integer array whose elements lie in the range 0 to `n-1` where `n` is the array size, calculate the frequency of all array elements in linear time and using constant space.

Input : [2, 3, 1, 3, 1, 1]
Output: {1: 3, 2: 1, 3: 2}

Explanation:

Element 1 appears thrice.
Element 2 appears once.
Element 3 appears twice.

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    unordered_map<int, int> findFrequency(vector<int> &nums)
    {
        unordered_map<int, int> freq;

        for (int i = 0; i < nums.size(); i++) {
            freq[nums[i]]++;
        }
        /*int n = nums.size();
        for (int i = 0; i < n; ++i) {
            int j = nums[i] % n;
            nums[j] += n;
        }

        for (int i = 0; i < n; i++)
        {
            if (nums[i] / n != 0) {
                freq[i] = nums[i] / n;
            }
        }

        for (int i = 0; i < n; ++i) {
            nums[i] = nums[i] % n;
        }*/

        return freq;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const unordered_map<int, int> & values) {
    for (auto& v : values) {
        os << v.first << ':' << v.second << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{2, 3, 1, 3, 1, 1};
    cout << Solution().findFrequency(nums) << endl;

    return 0;
}
