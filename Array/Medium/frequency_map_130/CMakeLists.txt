cmake_minimum_required(VERSION 3.5)

project(frequency_map_130 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(frequency_map_130 main.cpp)

install(TARGETS frequency_map_130
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
