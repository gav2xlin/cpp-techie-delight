/*

Given a sequence, find a subsequence of it such that the subsequence sum is as high as possible and the subsequence's elements are sorted in ascending order.

Input: [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11]
Output: 34
Explanation: The maximum sum increasing subsequence is [8, 12, 14] which has sum 34.

Input: [-4, -3, -2, -1]
Output: 0
Explanation: The maximum sum increasing subsequence is [] which has sum 0.

*/

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

class Solution
{
private:
    void printMSIS(vector<int> const &nums)
    {
        int n = nums.size();
        if (n == 0) {
            return;
        }

        // `MSIS[i]` stores the increasing subsequence having the maximum sum
        // that ends with `nums[i]`
        vector<int> MSIS[n];
        MSIS[0].push_back(nums[0]);

        // `sum[i]` stores the maximum sum of the increasing subsequence
        // that ends with `nums[i]`
        vector<int> sum(n);
        sum[0] = nums[0];

        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                // find increasing subsequence with the maximum sum that ends with
                // `nums[j]`, where `nums[j]` is less than the current element `nums[i]`

                if (sum[i] < sum[j] && nums[i] > nums[j])
                {
                    MSIS[i] = MSIS[j];
                    sum[i] = sum[j];
                }
            }

            MSIS[i].push_back(nums[i]);

            sum[i] += nums[i];
        }

        // uncomment the following code to print contents of `MSIS`
        /* for (int i = 0; i < n; i++)
        {
            cout << "MSIS[" << i << "] — ";
            for (int j: MSIS[i]) {
                cout << j << " ";
            }
            cout << endl;
        } */

        // `j` will contain the index of MSIS
        int j = 0;
        for (int i = 1; i < n;++i)
        {
            if (sum[i] > sum[j]) {
                j = i;
            }
        }

        for (int i: MSIS[j]) {
            cout << i << " ";
        }
    }

    int MSIS(vector<int> const &nums, int i, int prev, int sum)
    {
        if (i == nums.size()) {
            return sum;
        }

        // case 1: exclude the current element and process the
        // remaining elements
        int excl = MSIS(nums, i + 1, prev, sum);

        // case 2: include the current element if it is greater
        // than the previous element
        int incl = sum;
        if (nums[i] > prev) {
            incl = MSIS(nums, i + 1, nums[i], sum + nums[i]);
        }

        return max(incl, excl);
    }
public: 
    int findMSISLength(vector<int> const &nums)
    {
        //return MSIS(nums, 0, INT_MIN, 0);
        if (nums.empty()) {
            return 0;
        }

        // array to store subproblem solutions. `sum[i]` stores the maximum
        // sum of the increasing subsequence that ends with `nums[i]`
        vector<int> sum(nums.size());
        sum[0] = nums[0];

        for (int i = 1; i < nums.size(); ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                // find increasing subsequence with the maximum sum that ends with
                // `nums[j]`, where `nums[j]` is less than the current element `nums[i]`

                if (sum[i] < sum[j] && nums[i] > nums[j]) {
                    sum[i] = sum[j];
                }
            }

            sum[i] += nums[i];
        }

        int msis = INT_MIN;
        for (int x: sum) {
            msis = max(msis, x);
        }

        return msis;
    }
};

int main()
{
    cout << Solution().findMSISLength({0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11}) << endl;
    cout << Solution().findMSISLength({-4, -3, -2, -1}) << endl;

    return 0;
}
