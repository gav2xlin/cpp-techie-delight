/*

Given a sorted array of distinct positive integers, return all triplets that forms a geometric progression with an integral common ratio. A geometric progression is a sequence of numbers where each term after the first is found by multiplying the previous one by a fixed, non-zero number called the common ratio.

Input : [1, 2, 6, 10, 18, 54]
Output: {[2, 6, 18], [6, 18, 54]}

Input : [2, 8, 10, 15, 16, 30, 32, 64]
Output: {[2, 8, 32], [8, 16, 32], [16, 32, 64]}

Input : [1, 2, 6, 18, 36, 54]
Output: {[2, 6, 18], [1, 6, 36], [6, 18, 54]}

Input : [1, 2, 4, 16]
Output: {[1, 2, 4], [1, 4, 16]}

Input : [1, 2, 3, 6, 18, 22]
Output: {[2, 6, 18]}

*/


#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Solution
{
public:
    set<vector<int>> findTriplets(vector<int> const &nums)
    {
        int n = nums.size();
        if (n < 3) {
            return {};
        }

        set<vector<int>> triplets;

        for (int j = 1; j < n - 1; ++j)
        {
            int i = j - 1, k = j + 1;

            while (true)
            {
                while (i >= 0 && k < n && (nums[j] % nums[i] == 0) && (nums[k] % nums[j] == 0) && (nums[j] / nums[i] == nums[k] / nums[j]))
                {
                    triplets.insert({nums[i], nums[j], nums[k]});

                    ++k , --i;
                }

                if (i < 0 || k >= n) {
                    break;
                }

                if (nums[j] % nums[i] == 0 && nums[k] % nums[j] == 0)
                {
                    if (nums[j] / nums[i] < nums[k] / nums[j]) {
                        --i;
                    }
                    else {
                        ++k;
                    }
                } else if (nums[j] % nums[i] == 0) {
                    ++k;
                }
                else {
                    --i;
                }
            }
        }

        return triplets;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}
ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findTriplets({1, 2, 6, 10, 18, 54}) << endl;
    cout << Solution().findTriplets({2, 8, 10, 15, 16, 30, 32, 64}) << endl;
    cout << Solution().findTriplets({1, 2, 6, 18, 36, 54}) << endl;
    cout << Solution().findTriplets({1, 2, 4, 16}) << endl;
    cout << Solution().findTriplets({1, 2, 3, 6, 18, 22}) << endl;

    return 0;
}
