/*

Given a set of positive integers, check if it can be divided into two subsets with equal sum.

Input: S = [3, 1, 1, 2, 2, 1]
Output: true
Explanation: S can be partitioned into two partitions, each having a sum of 5.

S1 = [1, 1, 1, 2]
S2 = [2, 3]

Note that this solution is not unique. Here’s another solution.

S1 = [3, 1, 1]
S2 = [2, 2, 1]

*/

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

class Solution
{
private:
    bool subsetSum(vector<int> const &nums, int sum)
    {
        int n = nums.size();

        bool T[n + 1][sum + 1];

        for (int j = 1; j <= sum; ++j) {
            T[0][j] = false;
        }

        for (int i = 0; i <= n; ++i) {
            T[i][0] = true;
        }

        for (int i = 1; i <= n; ++i)
        {
            for (int j = 1; j <= sum; ++j)
            {
                if (nums[i - 1] > j) {
                    T[i][j] = T[i - 1][j];
                }
                else
                {
                    T[i][j] = T[i - 1][j] || T[i - 1][j - nums[i - 1]];
                }
            }
        }

        return T[n][sum];
    }
public:
    bool partition(vector<int> const &nums)
    {
        int sum = accumulate(nums.begin(), nums.end(), 0);
        return !(sum & 1) && subsetSum(nums, sum/2);
    }
};

int main()
{
    cout << boolalpha << Solution().partition({3, 1, 1, 2, 2, 1}) << endl;

    return 0;
}
