/*

Given an integer array, count the total number of strictly increasing contiguous subarrays in it.

Input: [1, 2, 4, 4, 5]
Output: 4
Explanation: The total number of strictly increasing subarrays are [1, 2], [1, 2, 4], [2, 4], [4, 5]

Input: [1, 3, 2]
Output: 1
Explanation: The total number of strictly increasing subarrays is [1, 3]

Input: [5, 4, 3, 2, 1]
Output: 0
Explanation: The total number of strictly increasing subarrays is 0

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int getCount(vector<int> const &nums)
    {
        int count = 0, len = 1;

        int n = nums.size();
        for (int i = 1; i < n; ++i)
        {
            if (nums[i - 1] < nums[i])
            {
                count += (len++);
            }
            else
            {
                len = 1;
            }
        }

        return count;
    }
};

int main()
{
    cout << Solution().getCount({1, 2, 4, 4, 5}) << endl;
    cout << Solution().getCount({1, 3, 2}) << endl;
    cout << Solution().getCount({5, 4, 3, 2, 1}) << endl;

    return 0;
}
