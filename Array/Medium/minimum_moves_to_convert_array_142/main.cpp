/*

Find the minimum number of moves required for converting an array of zeroes to a given array of non-negative integers using only increment and double operations. The increment operation increases the value of an array element by 1 and the double operation doubles the value of each array element.

Input: [8, 9, 8]
Output: 7

Explanation: The optimal sequence to convert an array [0, 0, 0] to [8, 9, 8] requires 3 increment operations, followed by 3 double operations, and a single increment operation:

[0, 0, 0] —> [1, 0, 0] —> [1, 1, 0] —> [1, 1, 1] —> [2, 2, 2] —> [4, 4, 4] —> [8, 8, 8] —> [8, 9, 8]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findMinMoves(vector<int> const &_nums)
    {
        int n = _nums.size();
        vector<int> nums(_nums);

        int min_moves = 0;

        while (true)
        {
            int no_of_zeroes = 0;

            for (int i = 0; i < n; ++i)
            {
                // convert all odd numbers to even by reducing their value by 1
                // for each odd value, increment the number of moves required
                if (nums[i] % 2 == 1)
                {
                    --nums[i];
                    ++min_moves;
                }

                if (nums[i] == 0) {
                    ++no_of_zeroes;
                }
            }

            if (no_of_zeroes == n) {
                break;
            }

            // Since each array element is even at this point,
            // divide each element by 2
            for (int j = 0; j < n; ++j) {
                nums[j] /= 2;
            }

            ++min_moves;
        }

        return min_moves;
    }
};

int main()
{
    cout << Solution().findMinMoves({8, 9, 8}) << endl;

    return 0;
}
