/*

Given a list of non-negative integers, find the minimum number of merge operations to make it a palindrome.

Input : [6, 1, 3, 7]
Output: 1
Explanation: [6, 1, 3, 7] —> Merge 6 and 1 —> [7, 3, 7]

Input : [6, 1, 4, 3, 1, 7]
Output: 2
Explanation: [6, 1, 4, 3, 1, 7] —> Merge 6 and 1 —> [7, 4, 3, 1, 7] —> Merge 3 and 1 —> [7, 4, 4, 7]

Input : [1, 3, 3, 1]
Output: 0
Explanation: The list is already a palindrome

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findMin(vector<int> const &_nums)
    {
        vector<int> nums(_nums);
        int count = 0, n = nums.size();

        int i = 0, j = n - 1;
        while (i < j)
        {
            if (nums[i] < nums[j]) {
                nums[i + 1] += nums[i];
                ++i, ++count;
            } else if (nums[i] > nums[j]) {
                nums[j - 1] += nums[j];
                --j, ++count;
            } else {
                ++i, --j;
            }
        }

        return count;
    }
};

int main()
{
    cout << Solution().findMin({6, 1, 3, 7}) << endl;
    cout << Solution().findMin({6, 1, 4, 3, 1, 7}) << endl;
    cout << Solution().findMin({1, 3, 3, 1}) << endl;

    return 0;
}
