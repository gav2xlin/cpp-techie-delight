/*

Given an array of positive integers and a positive integer k, find the smallest contiguous subarray length whose sum of elements is greater than k.

Input : [1, 2, 3, 4, 5, 6, 7, 8], k = 7
Output: 1
Explanation: The smallest subarray with sum > 20 is [8]

Input : [1, 2, 3, 4, 5, 6, 7, 8], k = 20
Output: 3
Explanation: The smallest subarray with sum > 20 is [6, 7, 8]

Input : [1, 2, 3, 4, 5, 6, 7, 8], k = 21
Output: 4
Explanation: The smallest subarray with sum > 20 is [4, 5, 6, 7]

Input : [1, 2, 3, 4, 5, 6, 7, 8], k = 40
Output: 0
Explanation: No subarray exists

*/

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findSmallestSubarray(vector<int> const &nums, int k)
    {
        int windowSum = 0;

        int len = INT_MAX;

        int left = 0;

        int n = nums.size();
        for (int right = 0; right < n; ++right)
        {
            windowSum += nums[right];

            while (windowSum > k && left <= right)
            {
                len = min(len, right - left + 1);

                windowSum -= nums[left];
                ++left;
            }
        }

        if (len == INT_MAX) {
            return 0;
        }

        return len;
    }
};

int main()
{
    cout << Solution().findSmallestSubarray({1, 2, 3, 4, 5, 6, 7, 8}, 7) << endl;
    cout << Solution().findSmallestSubarray({1, 2, 3, 4, 5, 6, 7, 8}, 20) << endl;
    cout << Solution().findSmallestSubarray({1, 2, 3, 4, 5, 6, 7, 8}, 21) << endl;
    cout << Solution().findSmallestSubarray({1, 2, 3, 4, 5, 6, 7, 8}, 40) << endl;

    return 0;
}
