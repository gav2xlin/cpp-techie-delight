/*

Given a binary array containing 0’s and 1’s, find the largest contiguous subarray with equal numbers of 0’s and 1’s.

Input : [0, 0, 1, 0, 1, 0, 0]
Output: [0, 1, 0, 1] or [1, 0, 1, 0]

Input : [0, 0, 0, 0]
Output: []

Note: Since an input can contain several largest subarrays with equal numbers of 0’s and 1’s, the code should return any one of them.

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    vector<int> findLargestSubarray(vector<int> const &nums)
    {
        unordered_map<int, int> map;
        map[0] = -1;

        int len = 0, sum = 0, ending_index = -1;

        for (int i = 0; i < nums.size(); ++i) {
            sum += (nums[i] == 0)? -1 : 1;

            if (map.find(sum) != map.end()) {
                if (len < i - map[sum]) {
                    len = i - map[sum];
                    ending_index = i;
                }
            } else {
                map[sum] = i;
            }
        }

        vector<int> largestSubarray;

        if (ending_index != -1) {
            for (int i = ending_index - len + 1; i <= ending_index; ++i) {
                largestSubarray.push_back(nums[i]);
            }
        }

        return largestSubarray;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findLargestSubarray({0, 0, 1, 0, 1, 0, 0}) << endl;
    cout << Solution().findLargestSubarray({0, 0, 0, 0}) << endl;

    return 0;
}
