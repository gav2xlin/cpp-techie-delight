/*

The Longest Bitonic Subarray (LBS) problem is to find a contiguous subarray of a given sequence in which the subarray’s elements are first sorted in increasing order, then in decreasing order, and the subarray is as long as possible.

Input : [3, 5, 8, 4, 5, 9, 10, 8, 5, 3, 4]
Output: [4, 5, 9, 10, 8, 5, 3]

In case the multiple bitonic subarrays of maximum length exists, the solution can return any one of them.

Input : [-5, -1, 0, 2, 1, 6, 5, 4, 2]
Output: [-5, -1, 0, 2, 1] or [1, 6, 5, 4, 2]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    vector<int> findBitonicSubarray(vector<int> const &nums)
    {
        int n = nums.size();
        if (n == 0) {
            return {};
        }

        vector<int> I(n);
        I[0] = 1;
        for (int i = 1; i < n; ++i)
        {
            I[i] = 1;
            if (nums[i - 1] < nums[i]) {
                I[i] = I[i - 1] + 1;
            }
        }

        vector<int> D(n);
        D[n - 1] = 1;
        for (int i = n - 2; i >= 0; --i)
        {
            D[i] = 1;
            if (nums[i] > nums[i + 1]) {
                D[i] = D[i + 1] + 1;
            }
        }

        int lbs_len = 1;
        int beg = 0, end = 0;

        for (int i = 0; i < n; ++i)
        {
            if (lbs_len < I[i] + D[i] - 1)
            {
                lbs_len = I[i] + D[i] - 1;
                beg = i - I[i] + 1;
                end = i + D[i] - 1;
            }
        }

        return {nums.begin() + beg, nums.begin() + end + 1};
        /*int end_index = 0, max_len = 1, i = 0, n = nums.size();

        while (i + 1 < n)
        {
            int len = 1;

            while (i + 1 < n && nums[i] < nums[i + 1]) {
                ++i, ++len;
            }

            while (i + 1 < n && nums[i] > nums[i + 1]) {
                ++i, ++len;
            }

            while (i + 1 < n && nums[i] == nums[i + 1]) {
                ++i;
            }
            if (len > max_len)
            {
                max_len = len;
                end_index = i;
            }
        }

        return {nums.begin() + end_index - max_len + 1, nums.begin() + end_index + 1};*/
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Solution s;

    cout << s.findBitonicSubarray({3, 5, 8, 4, 5, 9, 10, 8, 5, 3, 4}) << endl;
    cout << s.findBitonicSubarray({-5, -1, 0, 2, 1, 6, 5, 4, 2}) << endl;

    return 0;
}
