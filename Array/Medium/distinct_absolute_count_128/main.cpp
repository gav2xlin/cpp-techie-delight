/*

Given an array of sorted integers that may contain several duplicate elements, count the total number of distinct absolute values in it.

Input : [-1, -1, 0, 1, 1, 1]
Output: 2
Explanation: There are 2 distinct absolutes in the array [0, 1]

Input : [-2, -1, 0, 1, 2, 3]
Output: 4
Explanation: There are 4 distinct absolutes in the array [0, 1, 2, 3]

Input : [-1, -1, -1, -1]
Output: 1
Explanation: There is only 1 distinct absolute in the array [1]

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <cstdlib>

using namespace std;

class Solution
{
public:
    int findDistinctCount(vector<int> const &nums)
    {
        /*unordered_set<int> set;
        for (int const &i: nums) {
            set.insert(abs(i));
        }

        return set.size();*/

        int distinct_count = nums.size();

        int left = 0;
        int right = nums.size() - 1;

        while (left < right)
        {
            while (left < right && nums[left] == nums[left + 1])
            {
                --distinct_count;
                ++left;
            }

            while (right > left && nums[right] == nums[right - 1])
            {
                --distinct_count;
                --right;
            }

            if (left == right) {
                break;
            }

            int sum = nums[left] + nums[right];

            if (sum == 0)
            {
                --distinct_count;
                ++left;
                --right;
            } else if (sum < 0) {
                ++left;
            } else {
                --right;
            }
        }

        return distinct_count;
    }
};

int main()
{
    cout << Solution().findDistinctCount({-1, -1, 0, 1, 1, 1}) << endl;
    cout << Solution().findDistinctCount({-2, -1, 0, 1, 2, 3}) << endl;
    cout << Solution().findDistinctCount({-1, -1, -1, -1}) << endl;

    return 0;
}
