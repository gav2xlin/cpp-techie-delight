/*

Given an integer array `nums`, efficiently find a sorted triplet such that `nums[i] < nums[j] < nums[k]` where `i < j < k`.

Input : [7, 3, 4, 2, 6]
Output: (3, 4, 6)

• If the input contains several sorted triplets, the solution can return any one of them.

Input : [5, 4, 3, 7, 6, 1, 9]
Output: (5, 7, 9) or (4, 7, 9) or (3, 7, 9) or (5, 6, 9) or (4, 6, 9) or (3, 6, 9)

• If no triplet exists, return tuple (0, 0, 0).

Input : [5, 4, 3]
Output: (0, 0, 0)

*/

#include <iostream>
#include <tuple>
#include <vector>

using namespace std;

class Solution
{
private:
    bool findTriplet(vector<int> const &arr, auto &tuple)
    {
        int n = arr.size();

        if (n < 3) {
            return false;
        }

        int min_index = 0;
        int low, mid = -1;

        for (int i = 1; i < n; ++i)
        {
            if (arr[i] <= arr[min_index]) {
                min_index = i;
            } else if (mid == -1) {
                low = min_index;
                mid = i;
            } else if (arr[i] <= arr[mid]) {
                low = min_index;
                mid = i;
            } else {
                tuple = make_tuple(low, mid, i);
                return true;
            }
        }

        return false;
    }
public:
    tuple<int, int, int> findSortedTriplet(vector<int> const &nums)
    {
        tuple<int, int, int> triplet;
        if (findTriplet(nums, triplet))
        {
            return {nums[get<0>(triplet)], nums[get<1>(triplet)], nums[get<2>(triplet)]};
        } else {
            return {0, 0, 0};
        }
    }
};

ostream& operator<<(ostream& os, const tuple<int, int, int>& t) {
    os << get<0>(t) << ' ' << get<1>(t) << ' ' << get<2>(t);
    return os;
}

int main()
{
    cout << Solution().findSortedTriplet({7, 3, 4, 2, 6}) << endl;
    cout << Solution().findSortedTriplet({5, 4, 3, 7, 6, 1, 9}) << endl;
    cout << Solution().findSortedTriplet({5, 4, 3}) << endl;

    return 0;
}
