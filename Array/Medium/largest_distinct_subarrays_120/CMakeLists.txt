cmake_minimum_required(VERSION 3.5)

project(largest_distinct_subarrays_120 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(largest_distinct_subarrays_120 main.cpp)

install(TARGETS largest_distinct_subarrays_120
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
