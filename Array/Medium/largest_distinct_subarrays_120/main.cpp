/*

Given an integer array, find all maximum size contiguous subarrays having all distinct elements in them.

Input : [5, 2, 3, 5, 4, 3]
Output: {[5, 2, 3], [2, 3, 5, 4], [5, 4, 3]}

Input : [1, 2, 3, 3]
Output: {[1, 2, 3], [3]}

Input : [1, 2, 3, 4]
Output: {[1, 2, 3, 4]}

Note: Since an input can have multiple subarrays with all distinct elements, the solution should return a set containing all the distinct subarrays.

*/

#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    void getSubarray(set<vector<int>> &subarrays, vector<int> const &nums, int i, int j)
    {
        int n = nums.size();
        if (i < 0 || i > j || j >= n) {        // invalid input
            return;
        }

        vector<int> res;
        for (int index = i; index < j; ++index) {
            res.push_back(nums[index]);
        }

        res.push_back(nums[j]);

        subarrays.insert(res);
    }
public:
    set<vector<int>> findDistinctSubarrays(vector<int> const &nums)
    {
        set<vector<int>> subarrays;

        unordered_map<int, bool> visited;

        int right = 0, left = 0;

        int n = nums.size();
        while (right < n)
        {
            // keep increasing the window size if all elements in the
            // current window are distinct
            while (right < n && !visited[nums[right]])
            {
                visited[nums[right]] = true;
                ++right;
            }

            getSubarray(subarrays, nums, left, right - 1);

            // As soon as a duplicate is found (`A[right]`),
            // terminate the above loop, and reduce the window's size
            // from its left to remove the duplicate
            while (right < n && visited[nums[right]])
            {
                visited[nums[left]] = false;
                ++left;
            }
        }

        return subarrays;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findDistinctSubarrays({5, 2, 3, 5, 4, 3}) << endl;
    cout << Solution().findDistinctSubarrays({1, 2, 3, 3}) << endl;
    cout << Solution().findDistinctSubarrays({1, 2, 3, 4}) << endl;

    return 0;
}
