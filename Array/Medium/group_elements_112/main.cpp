/*

Given an unsorted integer array containing many duplicate elements, rearrange it such that the same element appears together and the relative order of the first occurrence of each element remains unchanged.

Input : [1, 2, 3, 1, 2, 1]
Output: [1, 1, 1, 2, 2, 3]

Input : [5, 4, 5, 5, 3, 1, 2, 2, 4]
Output: [5, 5, 5, 4, 4, 3, 1, 2, 2]

Input : [7, 0, 4, 2, 5, 0, 6, 4, 0, 5]
Output: [7, 0, 0, 0, 4, 4, 2, 5, 5, 6]

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    vector<int> sortArray(vector<int> const &nums)
    {
        vector<int> res;

        unordered_map<int, int> freq;

        int n = nums.size();
        for (int i = 0; i < n; ++i) {
            freq[nums[i]]++;
        }

        for (int i = 0; i < n; ++i)
        {
            if (freq.count(nums[i]))
            //if (freq.contains(nums[i]))
            {
                int k = freq[nums[i]];
                while (k--) {
                    res.push_back(nums[i]);
                }

                freq.erase(nums[i]);
            }
        }

        return res;
    }
};

template<typename T>
ostream& operator<<(ostream& os, const vector<T>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        vector<int> nums{1, 2, 3, 1, 2, 1};
        cout << "before: " << nums << endl;
        cout << "after: " << Solution().sortArray(nums) << endl;
    }

    {
        vector<int> nums{5, 4, 5, 5, 3, 1, 2, 2, 4};
        cout << "before: " << nums << endl;
        cout << "after: " << Solution().sortArray(nums) << endl;
    }

    {
        vector<int> nums{7, 0, 4, 2, 5, 0, 6, 4, 0, 5};
        cout << "before: " << nums << endl;
        cout << "after: " << Solution().sortArray(nums) << endl;
    }

    return 0;
}
