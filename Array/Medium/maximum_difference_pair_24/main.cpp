/*

Given an integer array, find the maximum difference between two elements in it such that the smaller element appears before the larger element. If no such pair exists, return any negative number.

Input : [2, 7, 9, 5, 1, 3, 5]
Output: 7
Explanation: The pair with the maximum difference is (2, 9).

Input : [5, 4, 3, 2, 1]
Output: -1 (or any other negative number)
Explanation: No pair with the maximum difference exists.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

class Solution
{
public:
    int findMaxDiff(vector<int> const &nums)
    {
        int n = nums.size();
        if (n == 0) {
            return -1;
        }

        int diff = INT_MIN;
        int max_so_far = nums[n-1];

        for (int i = n - 2; i >= 0; --i)
        {
            if (nums[i] >= max_so_far) {
                max_so_far = nums[i];
            } else {
                diff = max(diff, max_so_far - nums[i]);
            }
        }

        return diff != INT_MIN ? diff : -1;
    }
};

int main()
{
    cout << Solution().findMaxDiff({2, 7, 9, 5, 1, 3, 5}) << endl;
    cout << Solution().findMaxDiff({5, 4, 3, 2, 1}) << endl;

    return 0;
}
