/*

Given a set S of positive integers, determine if it can be partitioned into three disjoint subsets that all have the same sum, and they cover S.

Input: S = [7, 3, 2, 1, 5, 4, 8]
Output: true
Explanation: S can be partitioned into three partitions [[7, 3], [5, 4, 1], [8, 2]], each having a sum of 10.

Note that there can be multiple solutions to a single set.

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <numeric>
#include <string>

using namespace std;

class Solution
{
private:
    bool subsetSum(vector<int> const &S, int n, int a, int b, int c, auto &lookup)
    {
        if (a == 0 && b == 0 && c == 0) {
            return true;
        }

        if (n < 0) {
            return false;
        }

        string key = to_string(a) + "|" + to_string(b) + "|" + to_string(c) + "|" + to_string(n);

        if (lookup.find(key) == lookup.end())
        {
            bool A = false;
            if (a - S[n] >= 0) {
                A = subsetSum(S, n - 1, a - S[n], b, c, lookup);
            }

            bool B = false;
            if (!A && (b - S[n] >= 0)) {
                B = subsetSum(S, n - 1, a, b - S[n], c, lookup);
            }

            bool C = false;
            if ((!A && !B) && (c - S[n] >= 0)) {
                C = subsetSum(S, n - 1, a, b, c - S[n], lookup);
            }

            lookup[key] = A || B || C;
        }

        return lookup[key];
    }
public:
    bool partition(vector<int> const &S)
    {
        int n = S.size();
        if (n < 3) {
            return false;
        }

        unordered_map<string, bool> lookup;

        int sum = accumulate(S.begin(), S.end(), 0);

        return !(sum % 3) && subsetSum(S, n - 1, sum/3, sum/3, sum/3, lookup);
    }
};

int main()
{
    cout << boolalpha << Solution().partition({7, 3, 2, 1, 5, 4, 8}) << endl;

    return 0;
}
