cmake_minimum_required(VERSION 3.5)

project(consecutive_numbers_124 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(consecutive_numbers_124 main.cpp)

install(TARGETS consecutive_numbers_124
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
