/*

Given an integer array, check if only consecutive integers form the array.

Input : [-1, 5, 4, 2, 0, 3, 1]
Output: true
Explanation: The array contains consecutive integers from -1 to 5.

Input : [4, 2, 4, 3, 1]
Output: false
Explanation: The array does not contain consecutive integers as element 4 is repeated.

Input : [2, 5, 3, 1]
Output: false
Explanation: The array does not contain consecutive integers as element 4 is missing.

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <set>

using namespace std;

class Solution
{
public:
    /*bool isConsecutive(vector<int> const &nums)
    {
        int n = nums.size();
        if (n <= 1) {
            return true;
        }

        int min = nums[0], max = nums[0];

        for (int i = 1; i < n; ++i)
        {
            if (nums[i] < min) {
                min = nums[i];
            }

            if (nums[i] > max) {
                max = nums[i];
            }
        }

        if (max - min != n - 1) {
            return false;
        }

        unordered_set<int> visited;
        for (int i = 0; i < n; ++i)
        {
            if (visited.find(nums[i]) != visited.end()) {
                return false;
            }

            visited.insert(nums[i]);
        }

        return true;
    }*/

    bool isConsecutive(vector<int> const &nums)
    {

        set<int> set;

        int n = nums.size();
        for (int i = 0; i < n; ++i)
        {
            if (set.find(nums[i]) != set.end()) {
                return false;
            }

            set.insert(nums[i]);
        }

        int prev{0};
        for (int curr: set)
        {
            if (curr != *set.begin() && (curr != prev + 1)) {
                return false;
            }

            prev = curr;
        }

        return true;
    }
};

int main()
{
    cout << boolalpha << Solution().isConsecutive({-1, 5, 4, 2, 0, 3, 1}) << endl;
    cout << Solution().isConsecutive({4, 2, 4, 3, 1}) << endl;
    cout << Solution().isConsecutive({2, 5, 3, 1}) << endl;

    return 0;
}
