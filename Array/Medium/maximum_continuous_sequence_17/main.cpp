/*

Given a binary array and a positive integer `k`, return the indices of the maximum sequence of continuous 1’s that can be formed by replacing at most `k` zeroes by ones.

• The solution should return a pair of the starting and the ending index of the maximum sequence.
• For invalid inputs, the solution should return pair (-1, -1).
• In case multiple sequence of continuous 1’s of maximum length exists, the solution can return any one of them.

Input : nums[] = [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0], k = 0
Output: (6, 9)
Explanation: The longest sequence of continuous 1’s is formed by index 6 to 9.

Input : nums[] = [1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0], k = 1
Output: (3, 9)
Explanation: The longest sequence of continuous 1’s is formed by index 3 to 9 on replacing zero at index 5.

Input : nums[] = [1, 1, 1, 1, 1], k = 1
Output: (0, 4)

Input : nums[] = [1, 0, 1, 1, 0, 0, 1, 1, 0, 1], k = 1
Output: (0, 3) or (6, 9)

Input : nums[] = [], k = 1
Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
public:
    pair<int,int> findLongestSequence(vector<int> const &nums, int k)
    {
        int left = 0, count = 0, window = 0, leftIndex = 0;

        for (int right = 0; right < nums.size(); ++right)
        {
            if (nums[right] == 0) {
                ++count;
            }

            while (count > k)
            {
                if (nums[left] == 0) {
                    --count;
                }

                ++left;
            }

            if (right - left + 1 > window)
            {
                window = right - left + 1;
                leftIndex = left;
            }
        }

        if (window == 0) {
            return {-1, -1};
        } else {
            return {leftIndex, leftIndex + window - 1};
        }
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    Solution s;

    cout << s.findLongestSequence({1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0}, 0) << endl;
    cout << s.findLongestSequence({1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0}, 1) << endl;
    cout << s.findLongestSequence({1, 1, 1, 1, 1}, 1) << endl;
    cout << s.findLongestSequence({1, 0, 1, 1, 0, 0, 1, 1, 0, 1}, 1) << endl;
    cout << s.findLongestSequence({}, 1) << endl;

    return 0;
}
