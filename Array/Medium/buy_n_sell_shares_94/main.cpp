/*

Given a list containing future prediction of share prices, find the maximum profit earned by buying and selling shares any number of times with the constraint, a new transaction can only start after the previous transaction is complete, i.e., you can only hold at most one share at a time.

Input : [1, 5, 2, 3, 7, 6, 4, 5]
Output: 10
Explanation: Total profit earned is 10

Buy on day 1 and sell on day 2
Buy on day 3 and sell on day 5
Buy on day 7 and sell on day 8


Input : [10, 8, 6, 5, 4, 2]
Output: 0

*/

#include <iostream>
#include <vector>
#include <cstdio>

using namespace std;

class Solution
{
public:
    int findMaxProfit(vector<int> const &price)
    {
        int profit = 0;

        int j = 0;

        int n = price.size();
        for (int i = 1; i < n; i++)
        {
            if (price[i - 1] > price[i]) {
                j = i;
            }

            // sell shares if the current element is the peak,
            // i.e., (`previous <= current > next`)
            if (price[i - 1] <= price[i] && (i + 1 == n || price[i] > price[i + 1]))
            {
                profit += (price[i] - price[j]);
                //printf("Buy on day %d and sell on day %d\n", j + 1, i + 1);
            }
        }

        return profit;
    }
};

int main()
{
    cout << Solution().findMaxProfit({1, 5, 2, 3, 7, 6, 4, 5}) << endl;
    cout << Solution().findMaxProfit({10, 8, 6, 5, 4, 2}) << endl;

    return 0;
}
