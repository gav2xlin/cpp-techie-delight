/*

Given an integer array, find all contiguous subarrays with zero-sum.

Input : [4, 2, -3, -1, 0, 4]
Output: {[-3, -1, 0, 4], [0]}

Input : [3, 4, -7, 3, 1, 3, 1, -4, -2, -2]
Output: {[3, 4, -7], [4, -7, 3], [-7, 3, 1, 3], [3, 1, -4], [3, 1, 3, 1, -4, -2, -2], [3, 4, -7, 3, 1, 3, 1, -4, -2, -2]}

Input : [0, 0]
Output: {[0], [0, 0]}

Input : [1, 2, 3]
Output: {}

Note: Since an input can have multiple subarrays with zero-sum, the solution should return a set containing all the distinct subarrays.

*/

#include <iostream>
#include <set>
#include <vector>
#include <unordered_map>
#include <utility>

using namespace std;

class Solution
{
public:
    set<vector<int>> getAllZeroSumSubarrays(vector<int> const &nums)
    {
        set<vector<int>> subarrays;

        unordered_multimap<int, int> sums;
        sums.insert(pair<int, int>(0, -1));

        int sum = 0;
        for (int i = 0; i < nums.size(); ++i) {
            sum += nums[i];

            if (sums.find(sum) != sums.end()) {
                auto it = sums.find(sum);

                while (it != sums.end() && it->first == sum)
                {
                    vector<int> subarray(nums.begin() + it->second + 1, nums.begin() + i + 1);
                    subarrays.insert(subarray);

                    ++it;
                }
            }

            sums.insert(pair<int, int>(sum, i));
        }

        return subarrays;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    if (!nums.empty()) {
        os << nums[0];
        for (int i = 1; i < nums.size(); ++i) {
            os << ", " << nums[i];
        }
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& nums) {
    for (auto& v : nums) {
        os << '[' << v << "] ";
    }
    return os;
}

int main()
{
    cout << Solution().getAllZeroSumSubarrays({4, 2, -3, -1, 0, 4}) << endl;
    cout << Solution().getAllZeroSumSubarrays({3, 4, -7, 3, 1, 3, 1, -4, -2, -2}) << endl;
    cout << Solution().getAllZeroSumSubarrays({0, 0}) << endl;
    cout << Solution().getAllZeroSumSubarrays({1, 2, 3}) << endl;

    return 0;
}
