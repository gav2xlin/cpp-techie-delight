cmake_minimum_required(VERSION 3.5)

project(zero_sum_3 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(zero_sum_3 main.cpp)

install(TARGETS zero_sum_3
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
