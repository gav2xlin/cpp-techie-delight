/*

Given two sorted integer arrays, `X[]` and `Y[]`, and an integer `k`, find a pair `(x, y)` whose sum is closest to `k`, where the pair consists of elements from each array. The solution should return the pair `(x, y)` `x` is taken from the first array, and `y` from the second array.

Input:

X[] = [1, 8, 10, 12]
Y[] = [2, 4, 9, 15]
k = 11

Output: (1, 9)


• For some inputs, the multiple pairs are feasible. For such cases, the solution can return any matching pair. For example,

Input:

X[] = [2, 4, 6, 8, 10]
Y[] = [1, 3, 5, 7, 9]
k = 4

Output: (2, 1) or (2, 3)


• If no pair exists, the solution should return the pair (-1, -1).

Input:

X[] = [1, 2, 3]
Y[] = []
k = 4

Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <utility>
#include <cstdlib>

using namespace std;

class Solution
{
public:
    pair<int,int> findClosestPair(vector<int> const &first, vector<int> const &second, int target)
    {
        int m = first.size(), n = second.size();
        if (m == 0 || n == 0) {
            return {-1, -1};
        }

        int x = 0;
        int y = n - 1;

        for (int i = 0, j = n - 1; i < m && j >= 0;)
        {
            if (abs(first[i] + second[j] - target) < abs(first[x] + second[y] - target))
            {
                x = i;
                y = j;
            }

            if (first[i] + second[j] < target) {
                ++i;
            } else if (first[i] + second[j] > target) {
                --j;
            } else {
                ++i, --j;
            }
        }

        return{first[x], second[y]};
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findClosestPair({1, 8, 10, 12}, {2, 4, 9, 15}, 11) << endl;
    cout << Solution().findClosestPair({2, 4, 6, 8, 10}, {1, 3, 5, 7, 9}, 4) << endl;
    cout << Solution().findClosestPair({1, 2, 3}, {}, 4) << endl;

    return 0;
}
