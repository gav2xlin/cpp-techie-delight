/*

Given a circularly sorted integer array, find a pair with a given sum. Assume there are no duplicates in the array, and the rotation is in an anti-clockwise direction around an unknown pivot.

• The solution can return pair in any order.

Input : nums[] = [10, 12, 15, 3, 6, 8, 9], target = 18
Output: (3, 15) or (15, 3)

Input : nums[] = [5, 8, 3, 2, 4], target = 12
Output: (4, 8) or (8, 4)

• If no pair with the given sum exists, the solution should return the pair (-1, -1).

Input : nums[] = [9, 15, 2, 3, 7], target = 20
Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
private:
    int findPivot(vector<int> const &nums, int n)
    {
        for (int i = 0; i < n - 1; ++i)
        {
            if (nums[i] > nums[i + 1]) {
                return i + 1;
            }
        }

        return n;
    }
public:
    pair<int,int> findPair(vector<int> const &nums, int target)
    {
        int n = nums.size();
        if (n <= 1) {
            return {-1, -1};
        }

        int pivot = findPivot(nums, n);

        int high = pivot - 1;
        int low = pivot % n;

        while (low != high)
        {
            if (nums[low] + nums[high] == target)
            {
                return {nums[low], nums[high]};
            }

            if (nums[low] + nums[high] < target) {
                low = (low + 1) % n;
            } else {
                high = (high - 1 + n) % n;
            }
        }

        return {-1, -1};
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findPair({10, 12, 15, 3, 6, 8, 9}, 18) << endl;
    cout << Solution().findPair({5, 8, 3, 2, 4}, 12) << endl;
    cout << Solution().findPair({9, 15, 2, 3, 7}, 20) << endl;

    return 0;
}
