/*

Given an integer array, in-place replace each element with the product of every other element without using the division operator.

Input : [1, 2, 3, 4, 5]
Output: [120, 60, 40, 30, 24]

Input : [5, 3, 4, 2, 6, 8]
Output: [1152, 1920, 1440, 2880, 960, 720]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    void rearrange(vector<int> &nums) {
        int n = nums.size();
        if (n == 0) {
            return;
        }

        int left[n], right[n];

        left[0] = 1;
        for (int i = 1; i < n; ++i) {
            left[i] = nums[i - 1] * left[i - 1];
        }

        right[n - 1] = 1;
        for (int j = n - 2; j >= 0; --j) {
            right[j] = nums[j + 1] * right[j + 1];
        }

        for (int i = 0; i < n; i++) {
            nums[i] = left[i] * right[i];
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        /*
        Input : [1, 2, 3, 4, 5]
        Output: [120, 60, 40, 30, 24]
        */
        vector<int> nums{1, 2, 3, 4, 5};
        cout << "before:\n" << nums << endl;
        Solution().rearrange(nums);
        cout << "after:\n" << nums << endl;
    }

    {
        /*
        Input : [5, 3, 4, 2, 6, 8]
        Output: [1152, 1920, 1440, 2880, 960, 720]
         */
        vector<int> nums{5, 3, 4, 2, 6, 8};
        cout << "before:\n" << nums << endl;
        Solution().rearrange(nums);
        cout << "after:\n" << nums << endl;
    }

    return 0;
}
