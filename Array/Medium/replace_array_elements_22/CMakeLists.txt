cmake_minimum_required(VERSION 3.5)

project(replace_array_elements_22 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(replace_array_elements_22 main.cpp)

install(TARGETS replace_array_elements_22
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
