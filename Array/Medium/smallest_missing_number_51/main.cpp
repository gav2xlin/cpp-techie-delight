/*

Given an unsorted integer array, find the smallest missing positive integer in it.

Input: [1, 4, 2, -1, 6, 5]
Output: 3

Input: [1, 2, 3, 4]
Output: 5

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>

using namespace std;

class Solution
{
private:
    int partition(vector<int> &nums)
    {
        int pIndex = 0;


        for (int i = 0; i < nums.size(); ++i)
        {
            if (nums[i] > 0)
            {
                swap(nums[i], nums[pIndex]);
                pIndex++;
            }
        }

        return pIndex;
    }
public:
    int findSmallestMissingNumber(vector<int> const &_nums)
    {
        vector<int> nums(_nums);
        int k = partition(nums);

        for (int i = 0; i < k; ++i)
        {
            int val = abs(nums[i]);

            if (val-1 < k && nums[val-1] >= 0) {
                nums[val-1] = -nums[val-1];
            }
        }

        for (int i = 0; i < k; ++i)
        {
            if (nums[i] > 0) {
                return i + 1;
            }
        }

        return k + 1;
    }
};

int main()
{
    cout << Solution().findSmallestMissingNumber({1, 4, 2, -1, 6, 5}) << endl;
    cout << Solution().findSmallestMissingNumber({1, 2, 3, 4}) << endl;

    return 0;
}
