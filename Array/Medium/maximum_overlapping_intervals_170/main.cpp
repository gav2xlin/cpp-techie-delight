/*

Consider an event where a log register is maintained containing the guest's arrival and departure times. Given an array of arrival and departure times from entries in the log register, find the first occurrence when maximum guests were present in the event.

Input:

arrival = [1, 2, 4, 7, 8, 12]
departure = [2, 7, 8, 12, 10, 15]

Output: 3

Explanation: Maximum number of guests is 3, present at time 7 as shown below:

https://techiedelight.com/practice/images/Maximum-Overlapping-Interval.png


Note: If an arrival and departure event coincides, the arrival time is preferred over the departure time.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    /*int findMaxGuests(vector<int> const &_arrival, vector<int> const &_departure)
    {
        vector<int> arrival(_arrival);
        vector<int> departure(_departure);

        sort(arrival.begin(), arrival.end());
        sort(departure.begin(), departure.end());

        int checked_in = 0;
        int max_checked_in = 0;
        //int max_event_tm = 0;

        for (int i = 0, j = 0; i < arrival.size() && j < departure.size(); )
        {
            if (arrival[i] <= departure[j])
            {
                ++checked_in;

                if (max_checked_in < checked_in)
                {
                    max_checked_in = checked_in;
                    //max_event_tm = arrival[i];
                }

                ++i;
            }
            else {
                --checked_in;

                ++j;
            }
        }

        //cout << "Event Time: " << max_event_tm << endl;
        //cout << "The Maximum number of guests is " << max_checked_in << endl;
        return max_checked_in;
    }*/
    int findMaxGuests(vector<int> const &arrival, vector<int> const &departure)
    {
        int t = *max_element(departure.begin(), departure.end());

        vector<int> count(t + 2);

        for (int i = 0; i < arrival.size(); ++i)
        {
            count[arrival[i]]++;
            count[departure[i] + 1]--;
        }

        int max_event_tm = count[0];

        for (int i = 1; i <= t; i++)
        {
            count[i] += count[i-1];
            if (count[max_event_tm] < count[i]) {
                max_event_tm = i;
            }
        }

        //cout << "Event Time: " << max_event_tm << endl;
        //cout << "The Maximum number of guests is " << count[max_event_tm] << endl;
        return count[max_event_tm];
    }
};

int main()
{
    cout << Solution().findMaxGuests({1, 2, 4, 7, 8, 12}, {2, 7, 8, 12, 10, 15}) << endl;

    return 0;
}
