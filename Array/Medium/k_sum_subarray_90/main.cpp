/*

Given an integer array, find all contiguous subarrays with a given sum `k`.

Input : nums[] = [3, 4, -7, 1, 3, 3, 1, -4], k = 7
Output: {[3, 4], [3, 4, -7, 1, 3, 3], [1, 3, 3], [3, 3, 1]}

Since the input can have multiple subarrays with sum `k`, the solution should return a set containing all the distinct subarrays.

*/

#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>
#include <iterator>
#include <algorithm>

using namespace std;

class Solution
{
private:
    void getSubarray(vector<int> const &nums, int index, vector<int> &input, set<vector<int>>& subarrays)
    {
        for (int j: input)
        {
            /*cout << "[" << (j + 1) << "…" << index << "] —— { ";
            copy(nums.begin() + j + 1, nums.begin() + index + 1, ostream_iterator<int>(cout, " "));
            cout << "}\n";*/
            subarrays.insert({nums.begin() + j + 1, nums.begin() + index + 1});
        }
    }
public:
    set<vector<int>> getAllSubarrays(vector<int> const &nums, int target)
    {
        set<vector<int>> subarrays;

        unordered_map<int, vector<int>> map;

        // To handle the case when the subarray with the given sum starts
        // from the 0th index
        map[0].push_back(-1);

        int sum_so_far = 0;

        int n = nums.size();
        for (int index = 0; index < n; ++index)
        {
            sum_so_far += nums[index];

            auto itr = map.find(sum_so_far - target);
            if (itr != map.end())
            {
                getSubarray(nums, index, map[itr->first], subarrays);
            }

            map[sum_so_far].push_back(index);
        }

        return subarrays;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().getAllSubarrays({3, 4, -7, 1, 3, 3, 1, -4}, 7) << endl;

    return 0;
}
