cmake_minimum_required(VERSION 3.5)

project(k_sum_subarray_90 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(k_sum_subarray_90 main.cpp)

install(TARGETS k_sum_subarray_90
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
