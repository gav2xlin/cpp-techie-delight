/*

Given an integer array, find the maximum length contiguous subarray having a given sum.

Input : nums[] = [5, 6, -5, 5, 3, 5, 3, -2, 0], target = 8
Output: [-5, 5, 3, 5]
Explanation: The subarrays with sum 8 are [[-5, 5, 3, 5], [3, 5], [5, 3]]. The longest subarray is [-5, 5, 3, 5] having length 4.

Note: Since an input can contain several maximum length subarrays with given sum, the solution should return any one of the maximum length subarray.

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    vector<int> findMaxLenSubarray(vector<int> const &nums, int S)
    {
        unordered_map<int, int> map;

        map[0] = -1;

        int target = 0, len = 0, ending_index = -1;

        for (int i = 0; i < nums.size(); ++i)
        {
            target += nums[i];

            if (map.find(target) == map.end()) {
                map[target] = i;
            }

            if (map.find(target - S) != map.end() && len < i - map[target - S])
            {
                len = i - map[target - S];
                ending_index = i;
            }
        }

        return {nums.begin() + ending_index - len + 1, nums.begin() + ending_index + 1};
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findMaxLenSubarray({5, 6, -5, 5, 3, 5, 3, -2, 0}, 8) << endl;

    return 0;
}
