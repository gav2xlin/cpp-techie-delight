/*

Given an integer array, find a contiguous subarray within it that has the maximum sum.

Input : [-2, 1, -3, 4, -1, 2, 1, -5, 4]
Output: [4, -1, 2, 1]

Input : [-7, -3, -2, -4]
Output: [-2]

In case multiple subarrays exists with the largest sum, the solution can return any one of them.

Input : [-2, 2, -1, 2, 1, 6, -10, 6, 4, -8]
Output: [2, -1, 2, 1, 6] or [6, 4] or [2, -1, 2, 1, 6, -10, 6, 4]

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

// https://en.wikipedia.org/wiki/Maximum_subarray_problem#Kadane's_algorithm

class Solution
{
public:
    vector<int> findMaxSumSubarray(vector<int> const &nums)
    {
        int n = nums.size();
        if (n <= 0) {
            return {};
        }

        int max_so_far = INT_MIN, max_ending_here = 0;
        int start = 0, end = 0, beg = 0;

        for (int i = 0; i < n; ++i)
        {
            max_ending_here = max_ending_here + nums[i];

            if (max_ending_here < nums[i])
            {
                max_ending_here = nums[i];
                beg = i;
            }

            if (max_so_far < max_ending_here)
            {
                max_so_far = max_ending_here;
                start = beg;
                end = i;
            }
        }

        return {nums.begin() + start, nums.begin() + end + 1};
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findMaxSumSubarray({-2, 1, -3, 4, -1, 2, 1, -5, 4}) << endl;
    cout << Solution().findMaxSumSubarray({-7, -3, -2, -4}) << endl;
    cout << Solution().findMaxSumSubarray({-2, 2, -1, 2, 1, 6, -10, 6, 4, -8}) << endl;

    return 0;
}
