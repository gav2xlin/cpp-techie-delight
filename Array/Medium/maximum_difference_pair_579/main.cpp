/*

Given an integer array `nums`, find the maximum value of `j-i` such that `nums[j] > nums[i]`.

For example,

Input: nums = [9, 10, 2, 6, 7, 12, 8, 1]
Output: 5
Explanation: The maximum difference is 5 for i = 0, j = 5

Input: nums = [9, 2, 1, 6, 7, 3, 8]
Output: 5
Explanation: The maximum difference is 5 for i = 1, j = 6

Input: nums = [8, 7, 5, 4, 2, 1]
Output: -1 (or any other negative number)
Explanation: Array is sorted in decreasing order.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>

using namespace std;

class Solution
{
public:
    int findMaxDiff(vector<int> &nums)
    {
        int diff = numeric_limits<int>::min();

        int n = nums.size();
        if (n == 0) {
            return diff;
        }

        int aux[n];

        aux[n - 1] = nums[n - 1];
        for (int j = n - 2; j >= 0; --j) {
            aux[j] = max(nums[j], aux[j + 1]);
        }

        // Find maximum `j-i` using the auxiliary array
        for (int i = 0, j = 0; i < n && j < n;)
        {
            if (nums[i] < aux[j])
            {
                diff = max(diff, j - i);
                ++j;
            } else {
                ++i;
            }
        }

        return diff;
    }
};

int main()
{
    cout << "Hello World!" << endl;
    return 0;
}
