/*

Given an integer array and a positive integer `k`, find the count of distinct elements in every contiguous subarray of size `k`.

Input : nums[] = [2, 1, 2, 3, 2, 1, 4, 5], k = 5
Output: [3, 3, 4, 5]
Explanation:

The count of distinct elements in subarray [2, 1, 2, 3, 2] is 3
The count of distinct elements in subarray [1, 2, 3, 2, 1] is 3
The count of distinct elements in subarray [2, 3, 2, 1, 4] is 4
The count of distinct elements in subarray [3, 2, 1, 4, 5] is 5


Input : nums[] = [1, 1, 2, 1, 3], k = 3
Output: [2, 2, 3]
Explanation:

The count of distinct elements in subarray [1, 1, 2] is 2
The count of distinct elements in subarray [1, 2, 1] is 2
The count of distinct elements in subarray [2, 1, 3] is 3

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    vector<int> findDistinctCount(vector<int> const &nums, int k)
    {
        vector<int> res;
        /*for (int i = 0; i < nums.size() - (k - 1); ++i)
        {
            unordered_set<int> distinct(nums.begin() + i, nums.begin() + i + k);

            res.push_back(distinct.size());
        }*/
        unordered_map<int, int> freq;

        int distinct = 0;

        for (int i = 0; i < nums.size(); ++i)
        {
            if (i >= k)
            {
                freq[nums[i - k]]--;

                if (freq[nums[i - k]] == 0) {
                    --distinct;
                }
            }

            freq[nums[i]]++;

            if (freq[nums[i]] == 1) {
                ++distinct;
            }

            if (i >= k - 1)
            {
                res.push_back(distinct);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findDistinctCount({2, 1, 2, 3, 2, 1, 4, 5}, 5) << endl;
    cout << Solution().findDistinctCount({1, 1, 2, 1, 3}, 3) << endl;

    return 0;
}
