/*

Given an integer array, trim it such that its maximum element becomes less than twice the minimum, return the minimum number of removals required for the conversion.

Input : [4, 6, 1, 7, 5, 9, 2]
Output: 4
Explanation: The trimmed array is [7, 5, 9] where 9 < 2 × 5.

Input : [4, 2, 6, 4, 9]
Output: 3
Explanation: The trimmed array is [6, 4] where 6 < 2 × 4.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMin(vector<int> const &nums)
    {
        int max_range = 0;

        int min, max;

        int n = nums.size();
        for (int i = 0; i < n && n - i > max_range; ++i)
        {
            min = max = nums[i];

            /*
                Subarray invariant: max < 2×min
            */

            for (int j = i; j < n; ++j)
            {
                min = std::min(min, nums[j]);
                max = std::max(max, nums[j]);

                if (2 * min <= max) {
                    break;
                }

                max_range = std::max(max_range, j - i + 1);
            }
        }

        return n - max_range;
    }
};

int main()
{
    cout << Solution().findMin({4, 6, 1, 7, 5, 9, 2}) << endl;
    cout << Solution().findMin({4, 2, 6, 4, 9}) << endl;

    return 0;
}
