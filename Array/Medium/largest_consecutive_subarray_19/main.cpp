/*

Given an integer array, find the largest contiguous subarray formed by consecutive integers. The subarray should contain all distinct values.

Input : [2, 0, 2, 1, 4, 3, 1, 0]
Output: [0, 2, 1, 4, 3]

In case the multiple consecutive subarrays of maximum length exists, the solution can return any one of them.

Input : [-5, -1, 0, 2, 1, 6, 5, 8, 7]
Output: [-1, 0, 2, 1] or [6, 5, 8, 7]

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// segmentation fault

class Solution
{
private:
    bool isConsecutive(vector<int> const &nums, int i, int j, int min, int max)
    {
        // for an array to contain consecutive integers, the difference
        // between the maximum and minimum element in it should be exactly `j-i`
        if (max - min != j - i) {
            return false;
        }

        vector<bool> visited(j - i + 1);

        for (int k = i; k <= j; ++k)
        {
            if (visited[nums[k] - min]) {
                return false;
            }

            visited[nums[k] - min] = true;
        }

        return true;
    }
public:
    vector<int> findLargestSubarray(vector<int> const &nums)
    {
        int len = 1;
        int start = 0, end = 0;

        int n = nums.size();
        for (int i = 0; i < n - 1; ++i)
        {
            int min_val = nums[i], max_val = nums[i];

            for (int j = i + 1; j < n; ++j)
            {
                min_val = min(min_val, nums[j]);
                max_val = max(max_val, nums[j]);

                // check if subarray `A[i…j]` is formed by consecutive integers
                if (isConsecutive(nums, i, j, min_val, max_val))
                {
                    if (len < max_val - min_val + 1)
                    {
                        len = max_val - min_val + 1,
                        start = i, end = j;
                    }
                }
            }
        }

        return {nums.begin() + start, nums.begin() + end + 1};
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findLargestSubarray({2, 0, 2, 1, 4, 3, 1, 0}) << endl;
    cout << Solution().findLargestSubarray({-5, -1, 0, 2, 1, 6, 5, 8, 7}) << endl;

    return 0;
}
