/*

Given an integer array, determine whether it can be divided into pairs such that the sum of elements in each pair is divisible by a given positive integer `k`.

Input: nums[] = [3, 1, 2, 6, 9, 4], k = 5
Output: true
Explanation: Array can be divided into pairs {(3, 2), (1, 9), (4, 6)}, where the sum of elements in each pair is divisible by 5.

Input: nums[] = [2, 9, 4, 1, 3, 5], k = 6
Output: true
Explanation: Array can be divided into pairs {(2, 4), (9, 3), (1, 5)}, where the sum of elements in each pair is divisible by 6.

Input: nums[] = [3, 1, 2, 6, 9, 4], k = 6
Output: false
Explanation: Array cannot be divided into pairs where the sum of elements in each pair is divisible by 6.

*/

#include <iostream>
#include <vector>

using namespace std;

// Floating point exception

class Solution
{
public:
    bool hasPairs(vector<int> const &nums, int k)
    {
        int n = nums.size();
        if (n & 1) {
            return false;
        }

        int freq[k];

        for (int i = 0; i < n; ++i) {
            freq[i] = 0;
        }

        for (int i = 0; i < n; ++i)
        {
            int r = nums[i] % k;

            /*if (r < 0) { // negative numbers
                r += k;
            }*/
            freq[r]++;
        }

        if (freq[0] % 2 != 0) {
            return 0;
        }

        // for each element with remainder `r`, there should be an element with
        // remainder `k-r`
        for (int r = 1; r <= k / 2; r++)
        {
            if (freq[r] != freq[k - r]) {
                return false;
            }
        }

        return true;
    }
};

int main()
{
    cout << boolalpha << Solution().hasPairs({3, 1, 2, 6, 9, 4}, 5) << endl;
    cout << boolalpha << Solution().hasPairs({2, 9, 4, 1, 3, 5}, 6) << endl;
    cout << boolalpha << Solution().hasPairs({3, 1, 2, 6, 9, 4}, 6) << endl;

    return 0;
}
