/*

Given a square matrix of integers, find the maximum value of `M[c][d] - M[a][b]` over every choice of indexes such that `c > a` and `d > b` in a single traversal of the matrix.

Input:

[
    [ 1,  2, -1, -4, -20],
    [-8, -3,  4,  2,  1],
    [ 3,  8,  6,  1,  3],
    [-4, -1,  1,  7, -6],
    [ 0, -4, 10, -5,  1]
]

Output: 18

Explanation: The maximum value is 18 as M[4][2] – M[1][0] has maximum difference.

Assume: matrix size > 1

*/

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaxDiff(vector<vector<int>> const &M)
    {
        if (M.size() == 0) {
            return 0;
        }

        int n = M.size();
        int K[n][n];

        K[n-1][n-1] = M[n-1][n-1];
        int max = M[n-1][n-1];

        for (int j = n-2; j >= 0; --j)
        {
            if (M[n-1][j] > max) {
                max = M[n-1][j];
            }
            K[n-1][j] = max;
        }

        max = M[n-1][n-1];
        for (int i = n-2; i >= 0; --i)
        {
            if (M[i][n-1] > max) {
                max = M[i][n-1];
            }
            K[i][n-1] = max;
        }

        max = INT_MIN;
        for (int i = n-2; i >= 0; --i)
        {
            for (int j = n-2; j >= 0; --j)
            {
                if (K[i+1][j+1] - M[i][j] > max) {
                    max = K[i+1][j+1] - M[i][j];
                }

                K[i][j] = std::max(M[i][j], std::max(K[i][j+1], K[i+1][j]));
            }
        }

        return max;
    }
};

int main()
{
    vector<vector<int>> mat{
        {1, 2, -1, -4, -20},
        {-8, -3, 4, 2, 1},
        {3, 8, 6, 1, 3},
        {-4, -1, 1, 7, -6},
        {0, -4, 10, -5, 1}
    };

    cout << Solution().findMaxDiff(mat) << endl;

    return 0;
}
