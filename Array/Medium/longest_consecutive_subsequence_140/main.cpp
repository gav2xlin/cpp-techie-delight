/*

Given an integer array, find the length of the longest subsequence formed by the consecutive integers. The subsequence should contain all distinct values, and the character set should be consecutive, irrespective of its order.

Input : [2, 0, 6, 1, 5, 3, 7]
Output: 4
Explanation: The longest subsequence formed by the consecutive integers is [2, 0, 1, 3]. It has distinct values and length 4.

Input : [1, 4, 4, 0, 2, 3]
Output: 5
Explanation: The longest subsequence formed by the consecutive integers is [1, 4, 4, 0, 2, 3]. The distinct subsequence is [1, 4, 0, 2, 3] having length 5.

Input : [2, 4, 6, 3, 7, 4, 8, 1]
Output: 4
Explanation: The longest subsequence formed by the consecutive integers is [2, 4, 3, 4, 1]. The distinct subsequence is [2, 4, 3, 1] having length 4.

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    int findMaxLenSubseq(vector<int> const &nums)
    {
        unordered_set<int> S(nums.begin(), nums.end());

        int max_len = 0;

        for (int e: nums)
        {
            if (S.find(e - 1) == S.end())
            {
                int len = 1;

                // check for presence of elements `e+1`, `e+2`, `e+3`, … ,`e+len` in `S`
                while (S.find(e + len) != S.end()) {
                    len++;
                }

                max_len = max(max_len, len);
            }
        }

        return max_len;
    }
};

int main()
{
    cout << Solution().findMaxLenSubseq({2, 0, 6, 1, 5, 3, 7}) << endl;
    cout << Solution().findMaxLenSubseq({1, 4, 4, 0, 2, 3}) << endl;
    cout << Solution().findMaxLenSubseq({2, 4, 6, 3, 7, 4, 8, 1}) << endl;

    return 0;
}
