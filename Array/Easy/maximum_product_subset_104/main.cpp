/*

Given an integer array, find the maximum product of its elements among all its subsets.

Input : [-6, 4, -5, 8, -10, 0, 8]
Output: 15360
Explanation: The subset with the maximum product of its elements is [-6, 4, 8, -10, 8]

Input : [4, -8, 0, 8]
Output: 32
Explanation: The subset with the maximum product of its elements is [4, 8]

Input : []
Output: 0

*/

#include <iostream>
#include <vector>
#include <limits>

using namespace std;

class Solution
{
public:
    int findMaxProduct(vector<int> const &nums){
        int n = nums.size();
        if (n == 0) {
            return 0;
        }

        if (n == 1) {
            return nums[0];
        }

        int product = 1;

        int abs_min_so_far = numeric_limits<int>::max();

        int negative = 0;
        int positive = 0;

        bool contains_zero = false;

        for (int i = 0; i < n; ++i) {
            if (nums[i] < 0) {
                ++negative;
                abs_min_so_far = min(abs_min_so_far, abs(nums[i]));
            } else if (nums[i] > 0) {
                ++positive;
            }

            if (nums[i] == 0) {
                contains_zero = true;
            } else {
                product = product * nums[i];
            }
        }

        if (negative & 1) {
            product = product / -abs_min_so_far;
        }

        if (negative == 1 && !positive && contains_zero) {
            product = 0;
        }

        if (!negative && !positive && contains_zero) {
            product = 0;
        }

        return product;
    }
};

int main()
{
    cout << Solution().findMaxProduct({-6, 4, -5, 8, -10, 0, 8}) << endl;
    cout << Solution().findMaxProduct({4, -8, 0, 8}) << endl;
    cout << Solution().findMaxProduct({}) << endl;

    return 0;
}
