/*

Given an integer array of size `n`, return the element which appears more than `n/2` times. Assume that the input always contain the majority element.

Input : [2, 8, 7, 2, 2, 5, 2, 3, 1, 2, 2]
Output: 2

Input : [1, 3, 1, 1]
Output: 1

*/

// + https://en.wikipedia.org/wiki/Boyer–Moore_majority_vote_algorithm

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    int findMajorityElement(vector<int> &nums)
    {
        int n = nums.size();

        unordered_map<int, int> freq;
        for (auto v : nums) {
            ++freq[v];
        }

        for (auto v : nums) {
            if (freq[v] > n / 2) {
                return v;
            }
        }

        return -1;
    }
};

int main()
{
    {
        vector nums{2, 8, 7, 2, 2, 5, 2, 3, 1, 2, 2};
        cout << Solution().findMajorityElement(nums) << endl;
    }

    {
        vector nums{1, 3, 1, 1};
        cout << Solution().findMajorityElement(nums) << endl;
    }

    return 0;
}
