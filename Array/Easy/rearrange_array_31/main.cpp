/*

Given an array of positive and negative integers, segregate them in linear time and constant space. The output should contain all negative numbers, followed by all positive numbers.

Input : [9, -3, 5, -2, -8, -6, 1, 3]
Output: [-3, -2, -8, -6, 9, 5, 1, 3] or [-3, -2, -8, -6, 9, 5, 1, 3] or any other valid combination.

Input : [-4, -2, -7, -9]
Output: [-4, -2, -7, -9] or any other valid combination.

Input : [2, 4, 3, 1, 5]
Output: [2, 4, 3, 1, 5] or any other valid combination.

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
public:
    void rearrange(vector<int> &nums)
    {
        if (nums.size() <= 1) return;

        /*vector<int> res;
        res.reserve(nums.size());

        for (auto v : nums) {
            if (v < 0) {
                res.push_back(v);
            }
        }

        for (auto v : nums) {
            if (v >= 0) {
                res.push_back(v);
            }
        }

        nums.swap(res);*/
        int index = 0;
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] < 0) {
                swap(nums[i], nums[index++]);
            }
        }
    }
};

ostream& operator<<(ostream& os, const vector<int> &nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        vector<int> nums{9, -3, 5, -2, -8, -6, 1, 3};
        Solution().rearrange(nums);
        cout << nums << endl;
    }

    {
        vector<int> nums{-4, -2, -7, -9};
        Solution().rearrange(nums);
        cout << nums << endl;
    }

    {
        vector<int> nums{2, 4, 3, 1, 5};
        Solution().rearrange(nums);
        cout << nums << endl;
    }

    return 0;
}
