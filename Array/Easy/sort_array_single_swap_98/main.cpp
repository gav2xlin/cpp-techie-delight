/*

Given an integer array where all its elements are sorted in increasing order except two swapped elements, sort it in linear time. Assume there are no duplicates in the array.

Input : [3, 8, 6, 7, 5, 9] or [3, 5, 6, 9, 8, 7] or [3, 5, 7, 6, 8, 9]
Output: [3, 5, 6, 7, 8, 9]

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
public:
    void sortArray(vector<int> &nums)
    {
        if (nums.size() <= 1) return;

        int i = -1, j = -1;
        int n = nums.size();

        for (int k = 1; k < n; ++k) {
            if (nums[k - 1] > nums[k]) {
                if (i < 0) {
                    i = k - 1;
                } else if (j < 0) {
                    j = k;
                } else {
                    break;
                }
            }
        }

        if (i >=0 && j >= 0) {
            swap(nums[i], nums[j]);
        } else if (i >= 0) {
            swap(nums[i], nums[i + 1]);
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto& num : nums) {
        os << num << ' ';
    }
    return os;
}

int main()
{
    {
        vector<int> vec{3, 8, 6, 7, 5, 9};
        Solution().sortArray(vec);
        cout << vec << endl;
    }

    {
        vector<int> vec{3, 5, 6, 9, 8, 7};
        Solution().sortArray(vec);
        cout << vec << endl;
    }

    {
        vector<int> vec{3, 5, 7, 6, 8, 9};
        Solution().sortArray(vec);
        cout << vec << endl;
    }

    return 0;
}
