/*

Given an integer array and a positive number `k`, check whether the array contains any duplicate elements within the range `k`. If `k` is more than the array’s size, the solution should check for duplicates in the complete array.

Input: nums[] = [5, 6, 8, 2, 4, 6, 9], k = 4
Output: true
Explanation: Element 6 repeats at distance 4 which is <= k

Input: nums[] = [5, 6, 8, 2, 4, 6, 9], k = 2
Output: false
Explanation: Element 6 repeats at distance 4 which is > k

Input: nums[] = [1, 2, 3, 2, 1], k = 7
Output: true
Explanation: Element 1 and 2 repeats at distance 4 and 2, respectively which are both <= k

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
public:
    bool hasDuplicate(vector<int> const &nums, int k)
    {
        unordered_map<int, int> pos;

        for (int i = 0; i < nums.size(); ++i) {
            int d = nums[i];
            if (pos.find(d) != pos.end()) {
                if (i - pos[d] <= k) {
                    return true;
                }
            }
            pos[d] = i;
        }

        return false;
    }
};

int main()
{
    cout << boolalpha << Solution().hasDuplicate({5, 6, 8, 2, 4, 6, 9}, 4) << endl;
    cout << Solution().hasDuplicate({5, 6, 8, 2, 4, 6, 9}, 2) << endl;
    cout << Solution().hasDuplicate({1, 2, 3, 2, 1}, 7) << endl;

    return 0;
}
