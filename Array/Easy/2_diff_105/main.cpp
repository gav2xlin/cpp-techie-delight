/*

Given an unsorted integer array, find all pairs with a given difference `k` in it without using any extra space.

Input : nums = [1, 5, 2, 2, 2, 5, 5, 4], k = 3
Output: {(2, 5), (1, 4)}

Note:

• The solution should return the pair in sorted order. For instance, among pairs (5, 2) and (2, 5), only pair (2, 5) appeared in the output.

• Since the input can contain multiple pairs with a given difference, the solution should return a set containing all the distinct pairs. For instance, the pair (2, 5) appeared only once in the output.

*/

#include <iostream>
#include <vector>
#include <set>
#include <unordered_set>
#include <algorithm>

using namespace std;

class Solution
{
private:
    void check_pair(int v, int d, set<pair<int,int>>& pairs, unordered_set<int>& visited) {
        if (visited.find(d) != visited.end()) {
            int x = min(v, d), y = max(v, d);
            pairs.insert({x, y});
        }
    }
public:
    set<pair<int,int>> findPairs(vector<int> const &nums, int k)
    {
        set<pair<int,int>> pairs;
        unordered_set<int> visited;

        for (auto v : nums) {
            if (visited.find(v) != visited.end()) continue;
            visited.insert(v);

            check_pair(v, v - k, pairs, visited);
            check_pair(v, v + k, pairs, visited);
        }

        return pairs;
    }
};

ostream& operator<<(ostream& os, const set<pair<int,int>>& pairs) {
    for (auto& p : pairs) {
        os << p.first << '[' << p.second << "] ";
    }
    return os;
}

int main()
{
    cout << Solution().findPairs({1, 5, 2, 2, 2, 5, 5, 4}, 3) << endl;

    return 0;
}
