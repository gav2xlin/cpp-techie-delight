/*

Given two integer arrays, each of which is sorted in increasing order, merge them into a single array in increasing order, and return it.

Input: X = [1, 3, 5, 7], Y = [2, 4, 6]
Output: [1, 2, 3, 4, 5, 6, 7]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    vector<int> merge(vector<int> const &X, vector<int> const &Y)
    {
        vector<int> Z(X.size() + Y.size());
        int i = 0, j = 0, k = 0;

        while (i < X.size() && j < Y.size()) {
            if (X[i] <= Y[j]) {
                Z[k++] = X[i++];
            } else {
                Z[k++] = Y[j++];
            }
        }

        while (i < X.size()) {
            Z[k++] = X[i++];
        }

        while (j < Y.size()) {
            Z[k++] = Y[j++];
        }

        return Z;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    auto res = Solution().merge({1, 3, 5, 7}, {2, 4, 6});
    cout << res << endl;

    return 0;
}
