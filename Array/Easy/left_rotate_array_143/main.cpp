/*

Given an integer array, left-rotate it by `k` positions, where `k` is a postive integer.

Input: nums[] = [1, 2, 3, 4, 5], k = 2
Output: [3, 4, 5, 1, 2]

Input: nums[] = [1, 2, 3, 4, 5], k = 6
Output: [1, 2, 3, 4, 5]

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
public:
    void leftRotate(vector<int> &nums, int k)
    {
        int n = nums.size();
        if (k <= 0 || k >= n) return;

        vector<int> res(n);
        for (int i = 0; i < n; ++i) {
            int j = i - k;
            if (j < 0) {
                j += n;
            }
            res[j] = nums[i];
        }

        nums.swap(res);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        vector<int> nums{1, 2, 3, 4, 5};

        cout << nums << endl;
        Solution().leftRotate(nums, 2);
        cout << nums << endl;
    }

    // reverse(nums, 0, k - 1); // low, high
    // reverse(nums, k, n - 1);
    // reverse(nums, 0, n - 1);

    return 0;
}
