/*

Given an integer array of size `n` and containing elements between 1 and `n+1` with one element missing, find the missing number without using any extra space.

Input: [3, 2, 4, 6, 1]
Output: 5

Input: [3, 2, 4, 5, 6]
Output: 1

Input: [3, 2, 4, 5, 1]
Output: 6

Assume valid input.

*/

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

class Solution
{
public:
    int findMissingNumber(vector<int> const &nums)
    {
        int n = nums.size() + 1;
        int sum = accumulate(nums.begin(), nums.end(), 0);
        return n * (n + 1) / 2 - sum;
    }
};

int main()
{
    cout << Solution().findMissingNumber({3, 2, 4, 6, 1}) << endl;
    cout << Solution().findMissingNumber({3, 2, 4, 5, 6}) << endl;
    cout << Solution().findMissingNumber({3, 2, 4, 5, 1}) << endl;

    return 0;
}
