/*

Given an unsorted integer array, find a pair with the given sum in it.

• Each input can have multiple solutions. The output should match with either one of them.

Input : nums[] = [8, 7, 2, 5, 3, 1], target = 10
Output: (8, 2) or (7, 3)

• The solution can return pair in any order. If no pair with the given sum exists, the solution should return the pair (-1, -1).

Input : nums[] = [5, 2, 6, 8, 1, 9], target = 12
Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <utility>
#include <unordered_set>
#include <algorithm>

using namespace std;

class Solution {
public:
    pair<int,int> findPair(vector<int> const &nums, int target) {
        // brute-force O(n^2)
        /*int n = nums.size();
        for (int i = 0; i < n - 1; ++i) {
            int a = nums[i];
            for (int j = i + 1; j < n; ++j) {
                int b = nums[j];
                if (a + b == target) {
                    return {a, b};
                }
            }
        }*/

        // hash table O(n)
        unordered_set<int> visited;
        for (auto a : nums) {
            int b = target - a;
            if (visited.find(b) != visited.end()) {
                return {a, b};
            }
            visited.insert(a);
        }

        // two pointer O(n * log(n))
        /*int front = 0, rear = nums.size() - 1;

        vector<int> vec(nums);
        sort(vec.begin(), vec.end());

        while (front < rear) {
            int sum = vec[front] + vec[rear];
            if (sum == target) {
                return {vec[front], vec[rear]};
            } else if (sum < target) {
                ++front;
            } else {
                --rear;
            }
        }*/

        return {-1, -1};
    }
};

template<typename U, typename V>
ostream& operator<<(ostream& os, const pair<U, V>& p) {
    os << '(' << p.first << ", " << p.second << ')';
    return os;
}

int main()
{
    cout << Solution().findPair({8, 7, 2, 5, 3, 1}, 10) << endl;
    cout << Solution().findPair({5, 2, 6, 8, 1, 9}, 12) << endl;

    return 0;
}
