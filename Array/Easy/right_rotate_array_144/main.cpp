/*

Given an integer array, right-rotate it by `k` positions, where `k` is a postive integer.

Input: nums[] = [1, 2, 3, 4, 5], k = 2
Output: [4, 5, 1, 2, 3]

Input: nums[] = [1, 2, 3, 4, 5, 6, 7], k = 3
Output: [5, 6, 7, 1, 2, 3, 4]

Input: nums[] = [1, 2, 3, 4, 5], k = 6
Output: [1, 2, 3, 4, 5]

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
public:
    void rightRotate(vector<int> &nums, int k)
    {
        int n = nums.size();
        if (k <= 0 || k >= n) return;

        vector<int> res(n);
        for (int i = 0; i < n; ++i) {
            int j = i + k;
            if (j >= n) {
                j -= n;
            }
            res[j] = nums[i];
        }

        nums.swap(res);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        vector<int> nums{1, 2, 3, 4, 5};

        cout << nums << endl;
        Solution().rightRotate(nums, 2);
        cout << nums << endl;
    }

    // reverse(nums, n - k, n - 1); // low, high
    // reverse(nums, 0, n - k - 1);
    // reverse(nums, 0, n - 1);

    return 0;
}
