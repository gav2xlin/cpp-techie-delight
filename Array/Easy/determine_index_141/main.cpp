/*

Given an integer array, determine the index of an element before which all elements are smaller and after which all are greater.

Input: [4, 2, 3, 5, 1, 6, 9, 7]
Output: 5
Explanation: All elements before index 5 are smaller, and all elements after index 5 are greater.

• In case multiple indices satisfies the problem constraints, the solution should return the minimum index.

Input: [1, 2, 3, 4, 5]
Output: 1
Explanation: Index 1, 2, and 3 satisfies the problem constraints. The solution should return the minimum index, i.e., 1.

• For invalid inputs, the solution should return -1.

Input: [] or [1] or [1, 2] or [5, 4, 3, 2, 1]
Output: -1

*/

#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findIndex(vector<int> const &nums)
    {
        int n = nums.size();
        if (n <= 2) return -1;

        vector<int> left(nums.size()), right(nums.size());

        left[0] = numeric_limits<int>::min();
        for (int i = 1; i < n; ++i) {
            left[i] = max(left[i - 1], nums[i - 1]);
        }

        right[n - 1] = numeric_limits<int>::max();
        for (int i = n - 2; i >= 0; --i) {
            right[i] = min(right[i + 1], nums[i + 1]);
        }

        for (int i = 1; i < n - 1; ++i) {
            if (left[i] < nums[i] && nums[i] < right[i]) {
                return i;
            }
        }

        return -1;
    }
};

int main()
{
    cout << Solution().findIndex({4, 2, 3, 5, 1, 6, 9, 7}) << endl;
    cout << Solution().findIndex({1, 2, 3, 4, 5}) << endl;
    cout << Solution().findIndex({}) << endl;
    cout << Solution().findIndex({1}) << endl;
    cout << Solution().findIndex({1, 2}) << endl;
    cout << Solution().findIndex({5, 4, 3, 2, 1}) << endl;

    return 0;
}
