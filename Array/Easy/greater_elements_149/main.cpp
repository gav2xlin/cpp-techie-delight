/*

Given an unsorted integer array, print all greater elements than all elements present to their right.

Input : [10, 4, 6, 3, 5]
Output: [10, 6, 5]
Explanation: The elements that are greater than all elements to their right are 10, 6, and 5.

Note: The solution should return the elements in the same order as they appear in the input array.

*/

#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    vector<int> findGreaterElements(vector<int> const &nums)
    {
        vector<int> res;

        int maxElm = numeric_limits<int>::min();
        for (int i = nums.size() - 1; i >= 0; --i) {
            if (nums[i] >= maxElm) {
                maxElm = nums[i];
                res.push_back(maxElm);
            }
        }

        reverse(res.begin() , res.end());

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int> &nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findGreaterElements({10, 4, 6, 3, 5}) << endl;

    return 0;
}
