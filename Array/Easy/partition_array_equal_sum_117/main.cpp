/*

Given an integer array, partition it into two contiguous subarrays having the same sum of elements and return the index that points to the starting of the second subarray. Return -1 if no partition is possible.

Input : [6, -4, -3, 2, 3]
Output: 2
Explanation: The subarrays [6, -4] and [-3, 2, 3] have equal sum of 2. The second subarray starts from index 2.

Input : [6, -5, 2, -4, 1]
Output: -1
Explanation: No equal sum partition possible.

In case multiple solutions is possible, return the index of the first occurrence. For example,

Input : [1, -1, 1, -1, 1, -1]
Output: 0

*/

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

class Solution
{
public:
    int findPartitionIndex(vector<int> const &nums)
    {
        int sum = 0, total = accumulate(nums.begin(), nums.end(), 0);
        for (int i = 0; i < nums.size(); ++i) {
            if (sum == total - sum) {
                return i;
            }
            sum += nums[i];
        }

        return -1;
    }
};

int main()
{
    cout << Solution().findPartitionIndex({6, -4, -3, 2, 3}) << endl;
    cout << Solution().findPartitionIndex({6, -5, 2, -4, 1}) << endl;
    cout << Solution().findPartitionIndex({1, -1, 1, -1, 1, -1}) << endl;
    cout << Solution().findPartitionIndex({1, 2, 3}) << endl;

    return 0;
}
