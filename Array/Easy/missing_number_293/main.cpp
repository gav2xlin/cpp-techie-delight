/*

Given an array of `n-1` distinct integers in the range of 1 to `n`, find the missing number in it in linear time.

Input: [1, 2, 3, 4, 5, 7, 8, 9, 10]
Output: 6
Explanation: All elements are in the range 1 to 10. The missing number is 6.

Input: [1, 2, 3, 4]
Output: 5

Assume valid input.

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    int findMissingNumber(vector<int> const &nums)
    {
        int n = nums.size();
        unordered_set<int> s(n + 1);
        for (int i = 1; i <= n + 1; ++i) {
            s.emplace_hint(s.end(), i);
        }
        for (auto v : nums) {
            s.erase(v);
        }
        return *s.begin();
    }
};

int main()
{
    cout << Solution().findMissingNumber({1, 2, 3, 4, 5, 7, 8, 9, 10}) << endl;
    cout << Solution().findMissingNumber({1, 2, 3, 4}) << endl;

    return 0;
}
