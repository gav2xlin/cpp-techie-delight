cmake_minimum_required(VERSION 3.5)

project(floor_and_ceil_52 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(floor_and_ceil_52 main.cpp)

install(TARGETS floor_and_ceil_52
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
