/*

Given a sorted integer array, find the floor and ceiling of a given number in it. For a given number x, floor(x) is the largest integer in the array less than or equal to x, and ceil(x) is the smallest integer in the array greater than or equal to x.

The solution should return the (floor, ceil) pair. If the floor or ceil doesn't exist, consider it to be -1.

Input: nums[] = [1, 4, 6, 8, 9], x = 2
Output: (1, 4)
Explanation: The floor is 1 and ceil is 4

Input: nums[] = [1, 4, 6, 8, 9], x = 6
Output: (6, 6)
Explanation: The floor is 6 and ceil is 6

Input: nums[] = [-2, 0, 1, 3], x = 4
Output: (3, -1)
Explanation: The floor is 3 and ceil doesn't exist

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
private:
    int findFloor(vector<int> const &nums, int x) {
        int low = 0, high = nums.size() -1, floor = -1;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (nums[mid] < x) {
                floor = nums[mid];
                low = mid + 1;
            } else if (nums[mid] > x) {
                high = mid - 1;
            } else {
                floor = x;
                break;
            }
        }

        return floor;
    }

    int findCeil(vector<int> const &nums, int x) {
        int low = 0, high = nums.size() -1, ceil = -1;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (nums[mid] < x) {
                low = mid + 1;
            } else if (nums[mid] > x) {
                ceil = nums[mid];
                high = mid - 1;
            } else {
                ceil = x;
                break;
            }
        }

        return ceil;
    }
public:
    pair<int,int> findFloorAndCeil(vector<int> const &nums, int x)
    {
        return {findFloor(nums, x), findCeil(nums, x)};
    }
};

ostream& operator<<(ostream& os, const pair<int, int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findFloorAndCeil({1, 4, 6, 8, 9}, 2) << endl;
    cout << Solution().findFloorAndCeil({1, 4, 6, 8, 9}, 6) << endl;
    cout << Solution().findFloorAndCeil({-2, 0, 1, 3}, 4) << endl;

    return 0;
}
