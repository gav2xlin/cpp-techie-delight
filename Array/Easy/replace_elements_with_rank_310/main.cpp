/*

Given an array of distinct integers, replace each array element by its corresponding rank in the array. The minimum array element has the rank 1; the second minimum element has a rank of 2, and so on.

Input : [10, 8, 15, 12, 6, 20, 1]
Output: [4, 3, 6, 5, 2, 7, 1]

Input : [0, 1, -1]
Output: [2, 3, 1]

*/

#include <iostream>
#include <vector>
#include <set>
#include <iterator>

using namespace std;

class Solution
{
public:
    void transform(vector<int> &nums)
    {
        set<int> rank(nums.begin(), nums.end());

        vector<int> res;
        res.reserve(nums.size());
        for (auto v : nums) {
            int d = distance(rank.begin(), rank.find(v)) + 1;
            res.push_back(d);
        }
        nums.swap(res);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        vector<int> nums{{10, 8, 15, 12, 6, 20, 1}};
        Solution().transform(nums);
        cout << nums << endl;
    }

    {
        vector<int> nums{{0, 1, -1}};
        Solution().transform(nums);
        cout << nums << endl;
    }

    return 0;
}
