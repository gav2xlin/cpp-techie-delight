/*

Given a non-empty integer array, find the minimum and maximum element in the array by making minimum comparisons, and return the (min, max) element pair.

Input: [5, 7, 2, 4, 9, 6]
Output: (2, 9)

Explanation:

The minimum array element is 2
The maximum array element is 9

*/

#include <iostream>
#include <utility>
#include <vector>

using namespace std;

class Solution
{
public:
    pair<int,int> findMinAndMax(vector<int> const &nums)
    {
        int min = nums[0], max = nums[0];

        for (int i = 1; i < nums.size(); ++i) {
            if (nums[i] > max) {
                max = nums[i];
            } else if (nums[i] < min) {
                min = nums[i];
            }
        }

        return {min, max};
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findMinAndMax({5, 7, 2, 4, 9, 6}) << endl;

    return 0;
}
