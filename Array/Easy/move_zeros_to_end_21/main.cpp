/*

Given an integer array, move all zeros present in it to the end. The solution should maintain the relative order of items in the array and should not use constant space.

Input : [6, 0, 8, 2, 3, 0, 4, 0, 1]
Output: [6, 8, 2, 3, 4, 1, 0, 0, 0]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    void rearrange(vector<int> &nums) {
        // using swap
        vector<int> tmp;
        tmp.reserve(nums.size());
        for (auto d : nums) {
            if (d != 0) {
                tmp.push_back(d);
            }
        }
        tmp.resize(nums.size());
        nums.swap(tmp);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto d : nums) {
        os << d << ' ';
    }
    return os;
}

int main() {
    vector<int> nums{6, 0, 8, 2, 3, 0, 4, 0, 1};
    Solution().rearrange(nums);
    cout << nums << endl;

    return 0;
}
