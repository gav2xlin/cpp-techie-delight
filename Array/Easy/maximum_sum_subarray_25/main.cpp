/*

Given an integer array, find the maximum sum among all its subarrays.

Input : [-2, 1, -3, 4, -1, 2, 1, -5, 4]
Output: 6
Explanation: The maximum sum subarray is [4, -1, 2, 1]

Input : [-7, -3, -2, -4]
Output: -2
Explanation: The maximum sum subarray is [-2]

Input : [-2, 2, -1, 2, 1, 6, -10, 6, 4, -8]
Output: 10
Explanation: The maximum sum subarray is [2, -1, 2, 1, 6] or [6, 4] or [2, -1, 2, 1, 6, -10, 6, 4]

*/

#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaxSubarraySum(vector<int> const &nums)
    {
        int curSum = 0, maxSum = numeric_limits<int>::min();

        int n = nums.size();
        for (int i = 0; i < n; ++i) {
            int currSum = 0;
            for (int j = i; j < n; ++j) {
                currSum += nums[j];
                if (currSum > maxSum) {
                    maxSum = currSum;
                }
            }
        }
        // Kadane’s Algorithm
        /*for (int i = 0; i < n; ++i) {
            curSum += nums[i];
            maxSum = max(maxSum, curSum);
            curSum = max(curSum, 0);
        }*/

        return maxSum;
    }
};

int main()
{
    cout << Solution().findMaxSubarraySum({-2, 1, -3, 4, -1, 2, 1, -5, 4}) << endl;
    cout << Solution().findMaxSubarraySum({-7, -3, -2, -4}) << endl;
    cout << Solution().findMaxSubarraySum({-2, 2, -1, 2, 1, 6, -10, 6, 4, -8}) << endl;

    return 0;
}
