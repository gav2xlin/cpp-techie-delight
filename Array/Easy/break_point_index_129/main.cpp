/*

Given an integer array, find an index that divides it into two non-empty contiguous subarrays having an equal sum.

Input : [-1, 6, 3, 1, -2, 3, 3]
Output: 2
Explanation: The element 3 at index 2 divides the array into two non-empty subarrays `[-1, 6]` and `[1, -2, 3, 3]` having the same sum.

• The solution should return -1 if no partition is possible.

Input : [6, -5, 2, -4, 1]
Output: -1
Explanation: No equal sum partition possible.

• In case multiple partitions is possible, return the index of the first partition.

Input : [-1, -2, 2, -3]
Output: 1
Explanation: The index 1 and 2 divides the array into two equal sum subarrays. The solution should return index 1.

Input : [4, 2, -3, 4, -1, 0, 4]
Output: 1
Explanation: The index 1 and 3 divides the array into two equal sum subarrays. The solution should return index 1.

*/

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

class Solution
{
public:
    int findBreakPoint(vector<int> const &nums)
    {
        if (nums.size() <= 2) return -1;

        int lsum = nums[0], rsum = accumulate(nums.begin() + 1, nums.end(), 0);
        for (int i = 1; i < nums.size() - 1; ++i) {
            rsum -= nums[i];
            if (rsum == lsum) {
                return i;
            }
            lsum += nums[i];
        }

        return -1;
    }
};

int main()
{
    cout << Solution().findBreakPoint({-1, 6, 3, 1, -2, 3, 3}) << endl; // 2
    cout << Solution().findBreakPoint({6, -5, 2, -4, 1}) << endl; // -1
    cout << Solution().findBreakPoint({-1, -2, 2, -3}) << endl; // 1
    cout << Solution().findBreakPoint({4, 2, -3, 4, -1, 0, 4}) << endl; // 1

    return 0;
}
