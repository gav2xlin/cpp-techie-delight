/*

Given an integer array `nums` and two integers `x` and `y` present in it, find the minimum absolute difference between indices of `x` and `y` in a single traversal of the array.

Input : nums[] = [1, 3, 5, 4, 8, 2, 4, 3, 6, 5], x = 3, y = 2
Output: 2
Explanation: Element 3 is present at index 1 and 7, and element 2 is present at index 5. Their minimum absolute difference is min(abs(1-5), abs(7-5)) = 2

Input : nums[] = [1, 3, 5, 4, 8, 2, 4, 3, 6, 5], x = 2, y = 5
Output: 3
Explanation: Element 2 is present at index 5, and element 5 is present at index 2 and 9. Their minimum absolute difference is min(abs(5-2), abs(5-9)) = 3

Input : nums[] = [], x = 0, y = 1
Output: 0

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>

using namespace std;

class Solution
{
public:
    int findDifference(vector<int> const &nums, int x, int y)
    {
        int n = nums.size();
        if (n <= 1) {
            return 0;
        }

        int x_index = n, y_index = n;
        int min_diff = numeric_limits<int>::max();

        for (int i = 0; i < n; i++)
        {
            if (nums[i] == x)
            {
                x_index = i;
                min_diff = min(min_diff, abs(x_index - y_index));
            }

            if (nums[i] == y)
            {
                y_index = i;
                min_diff = min(min_diff, abs(x_index - y_index));
            }
        }

        return min_diff;
    }
};

int main()
{
    cout << Solution().findDifference({1, 3, 5, 4, 8, 2, 4, 3, 6, 5}, 3, 2) << endl;
    cout << Solution().findDifference({1, 3, 5, 4, 8, 2, 4, 3, 6, 5}, 2, 5) << endl;
    cout << Solution().findDifference({}, 0, 1) << endl;

    return 0;
}
