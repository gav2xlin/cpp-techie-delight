/*

Given a sorted integer array, find the index of a given number's first and last occurrence.

Input: nums[] = [2, 5, 5, 5, 6, 6, 8, 9, 9, 9], target = 5
Output: (1, 3)
Explanation: The first and last occurrence of element 5 is located at index 1 and 3, respectively.

• If the target is not present in the array, the solution should return the pair (-1, -1).

Input: nums[] = [2, 5, 5, 5, 6, 6, 8, 9, 9, 9], target = 4
Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
private:
    int findFirst(vector<int> const &nums, int target) {
        int low = 0, high = nums.size() - 1, result = -1;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (nums[mid] < target) {
                low = mid + 1;
            } else if (nums[mid] > target) {
                high = mid - 1;
            } else {
                result = mid;
                high = mid - 1;
            }
        }

        return result;
    }

    int findLast(vector<int> const &nums, int target) {
        int low = 0, high = nums.size() - 1, result = -1;

        while (low <= high) {
            int mid = (low + high) / 2;

            if (nums[mid] < target) {
                low = mid + 1;
            } else if (nums[mid] > target) {
                high = mid - 1;
            } else {
                result = mid;
                low = mid + 1;
            }
        }

        return result;
    }
public:
    pair<int,int> findPair(vector<int> const &nums, int target)
    {
        return {findFirst(nums, target), findLast(nums, target)};
    }
};

ostream& operator<<(ostream& os, const pair<int, int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findPair({2, 5, 5, 5, 6, 6, 8, 9, 9, 9}, 5) << endl;
    cout << Solution().findPair({2, 5, 5, 5, 6, 6, 8, 9, 9, 9}, 4) << endl;

    return 0;
}
