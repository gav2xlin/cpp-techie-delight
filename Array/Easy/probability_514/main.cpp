/*

Given a non-empty integer array, return the index of the maximum occurring element with an equal probability.

Input: nums[] = [4, 3, 6, 8, 4, 6, 2, 4, 5, 9, 7, 4]
Output: The solution should return any one of [0, 4, 7, 11] with equal probability.
Explanation: The maximum occurring element, 4, occurs at index 0, 4, 7, and 11.

If there are two maximum occurring elements in the array, the solution should consider the first occurring maximum element.

Input: nums[] = [4, 3, 6, 5, 2, 4, 1, 1]
Output: The solution should return any one of [0, 5] with equal probability.
Explanation: The maximum occurring elements are 4 and 1. Element 4 appears before element 1, at indices 0 and 5.

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <cstdlib>
#include <unistd.h>

using namespace std;

class Solution
{
public:
    int findIndex(vector<int> const &nums)
    {
        if (nums.empty()) return -1;

        unordered_map<int, int> freq;
        for (auto v : nums) {
            ++freq[v];
        }

        int max_elm = nums[0];
        for (auto v : nums) {
            if (freq[max_elm] < freq[v]) {
                max_elm = v;
            }
        }
        /*for(auto &p : freq) {
            if (freq[max_elm] < p.second) {
                max_elm = p.first;
            }
        }*/

        // srand(time(nullptr)); // disable randomization to have an equal probability
        int k = (rand() % freq[max_elm]) + 1;

        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] == max_elm) {
                --k;
                if (!k) {
                    return i;
                }
            }
        }

        return nums.size() - 1;
    }
};

int main()
{
    for (int i = 0; i < 5; i++)
    {
        sleep(1);
        cout << Solution().findIndex({4, 3, 6, 8, 4, 6, 2, 4, 5, 9, 7, 4}) << endl;
    }
    //cout << Solution().findIndex({4, 3, 6, 5, 2, 4, 1, 1}) << endl;

    return 0;
}
