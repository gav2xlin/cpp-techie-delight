/*

Given a circularly sorted array of distinct integers, find the total number of times the array is rotated. You may assume that the rotation is in anti-clockwise direction.

Input: [8, 9, 10, 2, 5, 6]
Output: 3

Input: [2, 5, 6, 8, 9, 10]
Output: 0

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findInversionCount(vector<int> const &nums)
    {
        int inversionCount = 0;

        int n = nums.size();
        for (int i = 1; i < n - 1; ++i) {
            int greater = 0;
            for (int j = 0; j < i; ++j) {
                if (nums[j] > nums[i]) {
                    ++greater;
                }
            }

            int smaller = 0;
            for (int k = i + 1; k < n; ++k) {
                if (nums[k] < nums[i]) {
                    ++smaller;
                }
            }

            inversionCount += greater * smaller;
        }

        return inversionCount;
    }
};

ostream& operator<<(ostream& os, const vector<int> &nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findInversionCount({1, 9, 6, 4, 5}) << endl;
    cout << Solution().findInversionCount({9, 4, 3, 5, 1}) << endl;

    return 0;
}
