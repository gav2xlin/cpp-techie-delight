/*

Given two arrays of positive integers, add their elements into a new array. The solution should add both arrays, one by one starting from the 0'th index, and split the sum into individual digits if it is a 2–digit number.

Input : [23, 5, 2, 7, 87], [4, 67, 2, 8]
Output: [2, 7, 7, 2, 4, 1, 5, 8, 7]

Input : [], [4, 67, 2, 8]
Output: [4, 6, 7, 2, 8]

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    vector<int> add(vector<int> const &X, vector<int> const &Y)
    {
        int n = X.size(), m = Y.size();

        vector<int> res;
        int k = max(n, m);
        for (int i = 0; i < k; ++i) {
            int sum = 0;
            if (i < n) {
                sum += X[i];
            }
            if (i < m) {
                sum += Y[i];
            }

            if (sum >= 10) {
                res.push_back(sum / 10);
            }
            if (sum > 0) {
                res.push_back(sum % 10);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().add({23, 5, 2, 7, 87}, {4, 67, 2, 8}) << endl; // [2 7] [7 2] [4] [1 5] [8 7]
    cout << Solution().add({}, {4, 67, 2, 8}) << endl; // [4] [6 7] [2] [8]
    cout << Solution().add({0, 2, 0, 3, 0, 5, 6, 0, 0}, {1, 8, 9, 10, 15}) << endl; // [1] [1 0] [9] [1 3] [1 5] [5] [6]

    return 0;
}
