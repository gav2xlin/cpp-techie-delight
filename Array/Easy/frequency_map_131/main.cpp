/*

Given a sorted array containing duplicates, efficiently find each element's frequency without traversing the whole array.

Input: [2, 2, 2, 4, 4, 4, 5, 5, 6, 8, 8, 9]
Output: {2: 3, 4: 3, 5: 2, 6: 1, 8: 2, 9: 1}

Explanation:

2 and 4 occurs thrice
5 and 8 occurs twice
6 and 9 occurs once

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    void findFrequencyInternal(vector<int> const &nums, unordered_map<int, int>& freq, int low, int high) {
        if (low > high) return;

        int low_val = nums[low], high_val = nums[high];
        if (low_val == high_val) {
            freq[low_val] += high - low + 1;
            return;
        }

        int mid = (low + high) / 2;
        findFrequencyInternal(nums, freq, low, mid);
        findFrequencyInternal(nums, freq, mid + 1, high);
    }
public:
    unordered_map<int, int> findFrequency(vector<int> const &nums)
    {
        unordered_map<int, int> freq;
        findFrequencyInternal(nums, freq, 0, nums.size() - 1);
        return freq;
    }
};

ostream& operator<<(ostream& os, const unordered_map<int, int>& freq) {
    for (auto& p : freq) {
        os << p.first << '[' << p.second << "] ";
    }
    return os;
}

int main()
{
    cout << Solution().findFrequency({2, 2, 2, 4, 4, 4, 5, 5, 6, 8, 8, 9}) << endl;

    return 0;
}
