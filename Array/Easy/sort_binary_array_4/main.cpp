/*

Given a binary array, in-place sort it in linear time and constant space. The output should contain all zeroes, followed by all ones.

Input : [1, 0, 1, 0, 1, 0, 0, 1]
Output: [0, 0, 0, 0, 1, 1, 1, 1]

Input : [1, 1]
Output: [1, 1]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    void sortArray(vector<int> &nums)
    {
        if (nums.size() <= 1) return;

        int n = 0;
        for (auto v : nums) {
            if (!v) ++n;
        }

        for (int i = 0; i < nums.size(); ++i) {
            nums[i] = (i >= n) ? 1 : 0;
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{1, 0, 1, 0, 1, 0, 0, 1};
    Solution().sortArray(nums);
    cout << nums << endl;

    nums = {1, 0, 1, 0, 1, 0, 0, 1, 1, 1};
    Solution().sortArray(nums);
    cout << nums << endl;

    nums = {1, 0, 1, 0, 1, 0, 0, 0, 0, 0};
    Solution().sortArray(nums);
    cout << nums << endl;

    nums = {1, 0};
    Solution().sortArray(nums);
    cout << nums << endl;

    nums = {1};
    Solution().sortArray(nums);
    cout << nums << endl;

    nums = {};
    Solution().sortArray(nums);
    cout << nums << endl;

    return 0;
}
