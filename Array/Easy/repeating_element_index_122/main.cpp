/*

Given an integer array, find the minimum index of a repeating element in linear time and doing just a single traversal of the array.

Input : [5, 6, 3, 4, 3, 6, 4]
Output: 1
Explanation: The minimum index of the repeating element is 1

Input : [1, 2, 3, 4, 5, 6]
Output: -1
Explanation: Input doesn't contain any repeating element

*/

#include <iostream>
#include <vector>
#include <unordered_set>

using namespace std;

class Solution
{
public:
    int findMinIndex(vector<int> const &nums)
    {
        int minIndex = -1;

        unordered_set<int> visited;
        for (int i = nums.size() - 1; i >= 0; --i) {
            if (visited.count(nums[i])) {
                minIndex = i;
            }
            visited.insert(nums[i]);
        }

        return minIndex;
    }
};

int main()
{
    cout << Solution().findMinIndex({5, 6, 3, 4, 3, 6, 4}) << endl;
    cout << Solution().findMinIndex({1, 2, 3, 4, 5, 6}) << endl;

    return 0;
}
