/*

Find all combinations of positive integers in increasing order that sum to a given positive number `N`.

Input : N = 3
Output: {[1, 1, 1], [1, 2], [3]}

Input : N = 4
Output: {[1, 1, 1, 1], [1, 1, 2], [1, 3], [2, 2], [4]}

Input : N = 5
Output: {[1, 1, 1, 1, 1], [1, 1, 1, 2], [1, 1, 3], [1, 2, 2], [1, 4], [2, 3], [5]}

*/

#include <iostream>
#include <set>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
private:
    void getArray(vector<int> const &sums, int n, auto &combinations)
    {
        vector<int> res(n + 1);
        copy(sums.begin(), sums.begin() + n + 1, res.begin());
        combinations.insert(res);
    }

    void findCombinations(vector<int> &sums, int i, int sum, int sum_left, auto &combinations)
    {
        // to maintain the increasing order, start the loop from the
        // previous number stored in `nums[]`
        int prev_sum = (i > 0) ? sums[i - 1] : 1;
        for (int k = prev_sum; k <= sum; ++k)
        {
            sums[i] = k;

            if (sum_left > k) {
                findCombinations(sums, i + 1, sum, sum_left - k, combinations);
            }

            if (sum_left == k) {
                getArray(sums, i, combinations);
            }
        }
    }
public:
    set<vector<int>> findCombinations(int sum)
    {
        set<vector<int>> combinations;

        vector<int> sums(sum);

        int starting_index = 0;
        findCombinations(sums, starting_index, sum, sum, combinations);

        return combinations;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().findCombinations(3) << endl;
    cout << Solution().findCombinations(4) << endl;
    cout << Solution().findCombinations(5) << endl;

    return 0;
}
