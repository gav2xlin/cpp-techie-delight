/*

Given an array `A`, maximize the value of expression `A[s] - A[r] + A[q] - A[p]`, where p, q, r, and s are indices of the array and s > r > q > p.

Input: [3, 9, 10, 1, 30, 40]
Output: 46
Explanation: The expression (40 - 1 + 10 - 3) results in maximum value.

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Solution
{
public:
    int maximizeValue(vector<int> const &A)
    {
        int n = A.size();
        if (n < 4) {
            exit(-1);
        }

        vector<int> first(n + 1), second(n), third(n - 1), fourth(n - 2);

        for (int i = 0; i <= n - 3; ++i) {
            first[i] = second[i] = third[i] = fourth[i] = INT_MIN;
        }

        first[n - 2] = second[n - 2] = third[n - 2] = INT_MIN;
        first[n - 1] = second[n - 1] = first[n] = INT_MIN;

        // `first[]` stores the maximum value of `A[l]`
        for (int i = n - 1; i >= 0; --i) {
            first[i] = max(first[i + 1], A[i]);
        }

        // `second[]` stores the maximum value of `A[l] - A[k]`
        for (int i = n - 2; i >= 0; --i) {
            second[i] = max(second[i + 1], first[i + 1] - A[i]);
        }

        // `third[]` stores the maximum value of `A[l] - A[k] + A[j]`
        for (int i = n - 3; i >= 0; --i) {
            third[i] = max(third[i + 1], second[i + 1] + A[i]);
        }

        // `fourth[]` stores the maximum value of `A[l] - A[k] + A[j] - A[i]`
        for (int i = n - 4; i >= 0; --i) {
            fourth[i] = max(fourth[i + 1], third[i + 1] - A[i]);
        }

        return fourth[0];
    }
};

int main()
{
    cout << Solution().maximizeValue({ 3, 9, 10, 1, 30, 40 }) << endl;

    return 0;
}
