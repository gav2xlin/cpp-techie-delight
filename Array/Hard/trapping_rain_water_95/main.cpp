/*

Find the maximum amount of water that can be trapped within a given set of bars where each bar’s width is 1 unit.

Input : [7, 0, 4, 2, 5, 0, 6, 4, 0, 5]
Output: 25
Explanation: The maximum amount of water that can be trapped is 25.

Pictorial representation: https://techiedelight.com/practice/images/TrappingRainWater.png


Input : [10, 8, 6, 5, 4, 2]
Output: 0

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Solution
{
public:
    int trapWater(vector<int> const &bars)
    {
        int n = bars.size();
        if (n <= 2) {
            return 0;
        }

        int water = 0;

        vector<int> left(n-1);
        left[0] = INT_MIN;

        for (int i = 1; i < n - 1; ++i) {
            left[i] = max(left[i-1], bars[i-1]);
        }

        /*
        int right[n];
        right[n - 1] = INT_MIN;
        for (int i = n - 2; i >= 0; i--) {
            right[i] = max(right[i+1], bars[i+1]);
        }

        for (int i = 1; i < n - 1; i++)
        {
            if (min(left[i], right[i]) > bars[i]) {
                water += min(left[i], right[i]) - bars[i];
            }
        }
        */

        int right = INT_MIN;

        for (int i = n - 2; i >= 1; --i)
        {
            right = max(right, bars[i+1]);

            if (min(left[i], right) > bars[i]) {
                water += min(left[i], right) - bars[i];
            }
        }

        return water;
        /*int n = bars.size();

        int left = 0, right = n - 1, water = 0;

        int maxLeft = bars[left];
        int maxRight = bars[right];

        while (left < right)
        {
            if (bars[left] <= bars[right])
            {
                ++left;
                maxLeft = max(maxLeft, bars[left]);
                water += (maxLeft - bars[left]);
            }
            else {
                --right;;
                maxRight = max(maxRight, bars[right]);
                water += (maxRight - bars[right]);
            }
        }

        return water;*/
    }
};

int main()
{
    cout << Solution().trapWater({7, 0, 4, 2, 5, 0, 6, 4, 0, 5}) << endl;
    cout << Solution().trapWater({10, 8, 6, 5, 4, 2}) << endl;

    return 0;
}
