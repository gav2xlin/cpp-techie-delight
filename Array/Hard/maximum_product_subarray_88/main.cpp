/*

Given an integer array, find the contiguous subarray that has the maximum product of its elements. The solution should return the maximum product of elements among all possible subarrays.

Input : [-6, 4, -5, 8, -10, 0, 8]
Output: 1600
Explanation: The maximum product subarray is [4, -5, 8, -10] which has product 1600.

Input : [40, 0, -20, -10]
Output: 200
Explanation: The maximum product subarray is [-20, -10] which has product 200.

Input : [10]
Output: 10

Input : []
Output: 0

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaxProduct(vector<int> const &arr)
    {
        int n = arr.size();
        if (n == 0) {
            return 0;
        }

        int max_ending = arr[0], min_ending = arr[0];
        int max_so_far = arr[0];

        for (int i = 1; i < n; ++i)
        {
            int temp = max_ending;

            max_ending = max(arr[i], max(arr[i] * max_ending, arr[i] * min_ending));

            min_ending = min(arr[i], min(arr[i] * temp, arr[i] * min_ending));

            max_so_far = max(max_so_far, max_ending);
        }

        return max_so_far;
    }
};

int main()
{
    cout << Solution().findMaxProduct({-6, 4, -5, 8, -10, 0, 8}) << endl;
    cout << Solution().findMaxProduct({40, 0, -20, -10}) << endl;
    cout << Solution().findMaxProduct({10}) << endl;
    cout << Solution().findMaxProduct({}) << endl;

    return 0;
}
