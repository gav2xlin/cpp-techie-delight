/*

Given two binary arrays, `X` and `Y`, find the length of the longest continuous sequence that starts and ends at the same index in both arrays and have the same sum. In other words, find `max(j-i+1)` for every `j >= i`, where the sum of subarray `X[i, j]` is equal to the sum of subarray `Y[i, j]`.

Input:

X[]: [0, 0, 1, 1, 1, 1]
Y[]: [0, 1, 1, 0, 1, 0]

Output: 5
Explanation: The length of the longest continuous sequence with the same sum is 5 as

X[0, 4]: [0, 0, 1, 1, 1]	(sum = 3)
Y[0, 4]: [0, 1, 1, 0, 1]	(sum = 3)

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaxSubarrayLength(vector<int> const &X, vector<int> const &Y)
    {
        unordered_map<int, int> map;
        map[0] = -1;

        int result = 0;

        int sum_x = 0, sum_y = 0;

        int n = min(X.size(), Y.size());
        for (int i = 0; i < n; ++i)
        {
            sum_x += X[i];
            sum_y += Y[i];

            int diff = sum_x - sum_y;

            if (map.find(diff) == map.end()) {
                map[diff] = i;
            }
            else
            {
                result = max(result, i - map[diff]);
            }
        }

        return result;
    }
};

int main()
{
    vector<int> X {0, 0, 1, 1, 1, 1};
    vector<int> Y {0, 1, 1, 0, 1, 0};

    cout << Solution().findMaxSubarrayLength(X, Y) << endl;

    return 0;
}
