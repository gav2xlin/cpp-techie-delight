/*

Given a binary array, find the index of 0 to be replaced with 1 to get the maximum length sequence of continuous ones. The solution should return the index of first occurence of 0, when multiple continuous sequence of maximum length is possible.

Input : [0, 0, 1, 0, 1, 1, 1, 0, 1, 1]
Output: 7
Explanation: Replace index 7 to get the continuous sequence of length 6 containing all 1’s.

Input : [0, 1, 1, 0, 0]
Output: 0
Explanation: Replace index 0 or 3 to get the continuous sequence of length 3 containing all 1’s. The solution should return the first occurence.

Input : [1, 1]
Output: -1
Explanation: Invalid Input (all 1’s)

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findIndexofZero(vector<int> const &nums)
    {
        int max_count = 0;
        int max_index = -1;

        int prev_zero_index = -1;
        int count = 0;

        for (int i = 0; i < nums.size(); ++i)
        {
            if (nums[i] == 1) {
                ++count;
            }
            else
            {
                // reset count to 1 + number of ones to the left of current 0
                count = i - prev_zero_index;

                prev_zero_index = i;
            }

            if (count > max_count)
            {
                max_count = count;
                max_index = prev_zero_index;
            }
        }

        return max_index;
    }
};

int main()
{
    cout << Solution().findIndexofZero({0, 0, 1, 0, 1, 1, 1, 0, 1, 1}) << endl;
    cout << Solution().findIndexofZero({0, 1, 1, 0, 0}) << endl;
    cout << Solution().findIndexofZero({1, 1}) << endl;

    return 0;
}
