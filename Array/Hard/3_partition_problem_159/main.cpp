/*

Given an array of positive integers, which can be partitioned into three disjoint subsets having the same sum, return the partitions.

Input: S = [7, 3, 2, 1, 5, 4, 8]
Output: [[7, 3], [5, 4, 1], [8, 2]]
Explanation: S can be partitioned into three partitions, each having a sum of 10.

Note that there can be multiple solutions to a single set, the procedure can return any one of them.

*/

#include <iostream>
#include <vector>
#include <numeric>

using namespace std;

class Solution
{
private:
    bool isSubsetExist(vector<int> const &S, int n, int a, int b, int c, vector<int> &arr)
    {
        if (a == 0 && b == 0 && c == 0) {
            return true;
        }

        if (n < 0) {
            return false;
        }

        bool A = false;
        if (a - S[n] >= 0)
        {
            arr[n] = 1;
            A = isSubsetExist(S, n - 1, a - S[n], b, c, arr);
        }

        bool B = false;
        if (!A && (b - S[n] >= 0))
        {
            arr[n] = 2;
            B = isSubsetExist(S, n - 1, a, b - S[n], c, arr);
        }

        bool C = false;
        if ((!A && !B) && (c - S[n] >= 0))
        {
            arr[n] = 3;
            C = isSubsetExist(S, n - 1, a, b, c - S[n], arr);
        }

        return A || B || C;
    }
public:
    vector<vector<int>> partition(vector<int> const &S)
    {
        int sum = accumulate(S.begin(), S.end(), 0);

        int n = S.size();

        // `arr[i] = k` represents i'th item of `S` is part of k'th subset
        vector<int> arr(n);

        // set result to true if the sum is divisible by 3 and the set `S` can
        // be divided into three subsets with an equal sum
        bool result = (n >= 3) && !(sum % 3) && isSubsetExist(S, n - 1, sum / 3, sum / 3, sum / 3, arr);

        if (!result)
        {
            return {};
        }

        vector<vector<int>> res;
        for (int i = 0; i < 3; ++i)
        {
            vector<int> vec;
            for (int j = 0; j < n; ++j)
            {
                if (arr[j] == i + 1) {
                    vec.push_back(S[j]);
                }
            }
            res.push_back(vec);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const vector<vector<int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    cout << Solution().partition({7, 3, 2, 1, 5, 4, 8}) << endl;

    return 0;
}
