/*

Given an integer array, find the maximum absolute difference between the sum of elements of two non-overlapping contiguous subarrays in linear time.

Input : [-3, -2, 6, -3, 5, -9, 3, 4, -1, -8, 2]
Output: 19
Explanation: The two subarrays are [6, -3, 5] and [-9, 3, 4, -1, -8] whose sum of elements are 8 and -11, respectively. So, abs(8-(-11)) or abs(-11-8) = 19.

Input : [6, 1, 3, 7]
Output: 9
Explanation: The two subarrays are [1] and [3, 7] whose sum of elements are 1 and 10, respectively. The maximum absolute difference is abs(10-1) = 9.

Input : [2]
Output: 2

Input : []
Output: 0

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <climits>
#include <functional>

using namespace std;

class Solution
{
private:
        void findMaxSumSubarray(vector<int> const& A, vector<int>& aux, int i, int j, int diff)
        {
            int max_so_far = A[i];
            int max_ending_here = A[i];
            aux[i] = A[i];

            for (int k = i + diff; k != j; k += diff)
            {
                max_ending_here = max(A[k], max_ending_here + A[k]);
                max_so_far = max(max_so_far, max_ending_here);
                aux[k] = max_so_far;
            }
        }

        void fillArrays(vector<int> &A, vector<int> &left_max, vector<int> &right_max, vector<int> &left_min, vector<int> &right_min, int n)
        {
            findMaxSumSubarray(A, left_max, 0, n - 1, 1);
            findMaxSumSubarray(A, right_max, n - 1, 0, -1);

            transform(A.begin(), A.begin() + n, A.begin(), negate<int>());

            // `transform()` is equivalent to

            /* for (int i = 0; i < n; i++) {
                A[i] = -A[i];
            } */

            findMaxSumSubarray(A, left_min, 0, n - 1, 1);
            findMaxSumSubarray(A, right_min, n - 1, 0, -1);

            transform(left_min.begin(), left_min.begin() + n, left_min.begin(), negate<int>());
            transform(right_min.begin(), right_min.begin() + n, right_min.begin(), negate<int>());

            transform(A.begin(), A.begin() + n, A.begin(), negate<int>());
        }
public:
    int findMaxAbsDiff(vector<int> const &_A)
    {
        int n = _A.size();
        if (n == 0) {
            return 0;
        }

        if (n == 1) {
            return _A[0];
        }

        vector<int> A(_A);

        // `left_max[i]` stores maximum sum of subarray in `A(0, i)`
        // `right_max[i]` stores maximum sum of subarray in `A(i, n-1)`
        // `left_min[i]` stores minimum sum of subarray in `A(0, i)`
        // `right_min[i]` stores minimum sum of subarray in `A(i, n-1)`

        vector<int> left_max(n), right_max(n), left_min(n), right_min(n);
        fillArrays(A, left_max, right_max, left_min, right_min, n);

        int max_abs_diff = INT_MIN;

        for (int i = 0; i < n - 1; ++i)
        {
            max_abs_diff = max(max_abs_diff, max(abs(left_max[i] - right_min[i+1]), abs(left_min[i] - right_max[i+1])));
        }

        return max_abs_diff;
    }
};

int main()
{
    cout << Solution().findMaxAbsDiff({-3, -2, 6, -3, 5, -9, 3, 4, -1, -8, 2}) << endl;
    cout << Solution().findMaxAbsDiff({6, 1, 3, 7}) << endl;
    cout << Solution().findMaxAbsDiff({2}) << endl;
    cout << Solution().findMaxAbsDiff({}) << endl;

    return 0;
}
