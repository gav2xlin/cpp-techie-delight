/*

Given a circular integer array, find a contiguous subarray with the largest sum in it.

Input : [2, 1, -5, 4, -3, 1, -3, 4, -1]
Output: 6
Explanation: Subarray with the largest sum is [4, -1, 2, 1] with sum 6.

Input : [8, -7, -3, 5, 6, -2, 3, -4, 2]
Output: 18
Explanation: Subarray with the largest sum is [5, 6, -2, 3, -4, 2, 8] with sum 18.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

class Solution
{
private:
    int kadane(vector<int> const &arr)
    {
        int max_so_far = 0;
        int max_ending_here = 0;

        for (int i = 0; i < arr.size(); ++i)
        {
            // update the maximum sum of subarray "ending" at index `i` (by adding the
            // current element to maximum sum ending at previous index `i-1`)
            max_ending_here = max_ending_here + arr[i];

            // if the maximum sum is negative, set it to 0 (which represents
            // an empty subarray)
            max_ending_here = max(max_ending_here, 0);

            // update result if the current subarray sum is found to be greater
            max_so_far = max(max_so_far, max_ending_here);
        }

        return max_so_far;
    }

    int runCircularKadane(vector<int> arr)
    {
        int n = arr.size();
        if (n == 0) {
            return 0;
        }

        int max_num = *max_element(arr.begin(), arr.end());

        if (max_num < 0) {
            return max_num;
        }

        for (int i = 0; i < n; ++i) {
            arr[i] = -arr[i];
        }

        int neg_max_sum = kadane(arr);

        for (int i = 0; i < n; ++i) {
            arr[i] = -arr[i];
        }

        /* Return the maximum of the following:
            1. Sum returned by Kadane’s algorithm on the original array.
            2. Sum returned by Kadane’s algorithm on the modified array +
               the sum of all the array elements.
        */

        return max(kadane(arr), accumulate(arr.begin(), arr.end(), 0) + neg_max_sum);
    }
public:
    int findMaxSubarray(vector<int> const &nums)
    {
        return runCircularKadane({nums.begin(), nums.end()});
    }
};

int main()
{
    cout << Solution().findMaxSubarray({2, 1, -5, 4, -3, 1, -3, 4, -1}) << endl;
    cout << Solution().findMaxSubarray({8, -7, -3, 5, 6, -2, 3, -4, 2}) << endl;

    return 0;
}
