/*

Given three sorted arrays of variable length, efficiently compute the minimum range with at least one element from each array.

Input : [[3, 6, 8, 10, 15], [1, 5, 12], [4, 8, 15, 16]]
Output: (3, 5)

Input : [[2, 3, 4, 8, 10, 15], [1, 5, 12], [7, 8, 15, 16]]
Output: (4, 7)

Input : [[1], [1, 2], [0, 1]]
Output: (1, 1)

If minimum range doesn't exist, the solution should return the pair (-1, -1).

Input : [[], [], []]
Output: (-1, -1)

*/

#include <iostream>
#include <vector>
#include <limits>
#include <utility>
#include <limits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    pair<int,int> findMinRange(vector<int> const &a, vector<int> const &b, vector<int> const &c)
    {
        pair<int, int> pair;

        int diff = numeric_limits<int>::max();

        int i = 0, j = 0, k = 0;

        while (i < a.size() && j < b.size() && k < c.size())
        {
            int low = min(min(a[i], b[j]), c[k]);
            int high = max(max(a[i], b[j]), c[k]);

            if (diff > high - low)
            {
                pair = make_pair(low, high);
                diff = high - low;
            }

            if (a[i] == low)
            {
                ++i;
            }
            else if (b[j] == low)
            {
                ++j;
            }
            else {
                ++k;
            }
        }

        return pair;
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    vector<int> a = { 2, 3, 4, 8, 10, 15 };
    vector<int> b = { 1, 5, 12 };
    vector<int> c = { 7, 8, 15, 16 };

    auto pair = Solution().findMinRange(a, b, c);
    cout << pair << endl;

    return 0;
}
