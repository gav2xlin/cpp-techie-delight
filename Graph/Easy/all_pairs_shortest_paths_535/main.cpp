/*

Given an adjacency matrix representation of a directed graph where its edge weights can be negative, return the cost matrix containing shortest path weights for every vertex to all other vertices present in the graph.

Input: Adjacency matrix for the graph

adj = [
    [0,   inf, -2,  inf],
    [4,   0,   3,   inf],
    [inf, inf, 0,   2],
    [inf, -1,  inf, 0]
]

Here, adj[i][j] = c indicates that the edge (i, j) has weight c. If there is no edge from vertex i to vertex j, adj[i][j] = INT_MAX.

Output: Cost matrix containing the shortest distance

cost = [
    [0, -1, -2, 0],
    [4,  0,  2, 4],
    [5,  1,  0, 2],
    [3, -1,  1, 0]
]

Here, cost[i][j] = c indicates that the shortest path from the source vertex i to the destination vertex j has cost c.

Explanation:

• Shortest path from (0 —> 1) is [0 —> 2 —> 3 —> 1]
• Shortest path from (0 —> 2) is [0 —> 2]
• Shortest path from (0 —> 3) is [0 —> 2 —> 3]
• Shortest path from (1 —> 0) is [1 —> 0]
• Shortest path from (1 —> 2) is [1 —> 0 —> 2]
• Shortest path from (1 —> 3) is [1 —> 0 —> 2 —> 3]
• Shortest path from (2 —> 0) is [2 —> 3 —> 1 —> 0]
• Shortest path from (2 —> 1) is [2 —> 3 —> 1]
• Shortest path from (2 —> 3) is [2 —> 3]
• Shortest path from (3 —> 0) is [3 —> 1 —> 0]
• Shortest path from (3 —> 1) is [3 —> 1]
• Shortest path from (3 —> 2) is [3 —> 1 —> 0 —> 2]


If the graph contains a negative-weight cycle, the solution should return an empty matrix.

infnput: adj = [
    [0,   -4, -2,   inf],
    [-1,  0,   3,   inf],
    [inf, inf, 0,   2],
    [inf, -1,  inf, 0]
]

Output: []

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

// Floyd–Warshall algorithm

class Solution {
public:
    vector<vector<int>> findShortestPaths(vector<vector<int>> const &adjMatrix) {
        int n = adjMatrix.size();
        if (n == 0) {
            return {};
        }

        vector<vector<int>> cost(n, vector<int>(n));
        vector<vector<int>> path(n, vector<int>(n));

        for (int v = 0; v < n; ++v) {
            for (int u = 0; u < n; ++u) {
                cost[v][u] = adjMatrix[v][u];

                if (v == u) {
                    path[v][u] = 0;
                } else if (cost[v][u] != INT_MAX) {
                    path[v][u] = v;
                } else {
                    path[v][u] = -1;
                }
            }
        }

        for (int k = 0; k < n; k++) {
            for (int v = 0; v < n; v++) {
                for (int u = 0; u < n; u++)
                {
                    if (cost[v][k] != INT_MAX && cost[k][u] != INT_MAX && cost[v][k] + cost[k][u] < cost[v][u]) {
                        cost[v][u] = cost[v][k] + cost[k][u];
                        path[v][u] = path[k][u];
                    }
                }

                if (cost[v][v] < 0) { // Negative-weight cycle found
                    return {};
                }
            }
        }

        return cost;
    }
};

ostream& operator<<(ostream& os, const vector<vector<int>> &adjMatrix) {
    for (auto& line : adjMatrix) {
        for (auto val : line) {
            os << val << ' ';
        }
        os << '\n';
    }
    return os;
}

int main()
{
    vector<vector<int>> adjMatrix =
    {
        { 0, INT_MAX, -2, INT_MAX },
        { 4, 0, 3, INT_MAX },
        { INT_MAX, INT_MAX, 0, 2 },
        { INT_MAX, -1, INT_MAX, 0 }
    };
    cout << Solution().findShortestPaths(adjMatrix) << endl;

    return 0;
}
