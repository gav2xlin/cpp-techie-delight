/*

Given a map of departure and arrival airports, find the itinerary in order. It may be assumed that departure is scheduled from every airport except the final destination, and each airport is visited only once, i.e., there are no cycles in the route.

Input: {'HKG': 'DXB', 'FRA': 'HKG', 'DEL': 'FRA'}
Output: ['DEL', 'FRA', 'HKG', 'DXB']
Explanation: The input represents the following tickets [HKG —> DXB, FRA —> HKG, DEL —> FRA]. The ticket itinerary is [DEL —> FRA —> HKG —> DXB].

Input: {'LAX': 'DXB', 'DFW': 'JFK', 'LHR': 'DFW', 'JFK': 'LAX'}
Output: ['LHR', 'DFW', 'JFK', 'LAX', 'DXB']

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    string findSource(unordered_map<string, string> const &tickets) {
        unordered_set<string> destinations;
        for (auto& t : tickets) {
            destinations.insert(t.second);
        }

        for (auto& t : tickets) {
            if (destinations.find(t.first) == destinations.end()) {
                return t.first;
            }
        }

        return "";
    }
public:
    vector<string> findItinerary(unordered_map<string, string> const &tickets)
    {
        string src = findSource(tickets);

        vector<string> res;
        if (!src.empty()) {
            for (string dest = src; dest != ""; dest = tickets.count(dest) ? tickets.at(dest) : "") {
                res.push_back(dest);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<string>& strings) {
    for (auto& s : strings) {
        os << s << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findItinerary({{"HKG", "DXB"}, {"FRA", "HKG"}, {"DEL", "FRA"}}) << endl;
    cout << Solution().findItinerary({{"LAX", "DXB"}, {"DFW", "JFK"}, {"LHR", "DFW"}, {"JFK", "LAX"}}) << endl;

    return 0;
}
