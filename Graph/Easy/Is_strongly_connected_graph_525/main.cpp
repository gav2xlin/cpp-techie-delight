/*

Given a directed graph, determine whether it is strongly connected or not. A directed graph is said to be strongly connected if every vertex is reachable from every other vertex.

Input: Graph [edges = [(0, 1), (1, 2), (2, 0)], n = 3]
Output: true

Input: Graph [edges = [(0, 1), (1, 2), (0, 2)], n = 3]
Output: false

Input: Graph [edges = [(0, 1)], n = 2]
Output: false

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <queue>
#include <stack>

using namespace std;

class Graph
{
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

class Solution
{
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool isStronglyConnected(Graph const &graph)
    {
        for (int i = 0; i < graph.n; ++i) {
            unordered_set<int> visited;
            //queue<int> q; // DFS
            stack<int> s; // BFS

            //q.push(i);
            s.push(i);
            visited.insert(i);

            /*while (!q.empty()) {
                int v = q.front();
                q.pop();*/
            while (!s.empty()) {
                int v = s.top();
                s.pop();

                for (int u : graph.adjList[v]) {
                    if (visited.find(u) == visited.end()) {
                        visited.insert(u);

                        //q.push(u);
                        s.push(u);
                    }
                }
            }

            if (visited.size() != graph.n) {
                return false;
            }
        }

        return true;
    }
};

int main()
{
    {
        Graph g{{{1}, {2}, {0}}, 3};
        cout << boolalpha << Solution().isStronglyConnected(g) << endl;
    }

    {
        Graph g{{{1, 2}, {2}, {}}, 3};
        cout << boolalpha << Solution().isStronglyConnected(g) << endl;
    }

    {
        Graph g{{{1}}, 2};
        cout << boolalpha << Solution().isStronglyConnected(g) << endl;
    }

    return 0;
}
