/*

The transitive closure for a digraph G is a digraph G’ with an edge (i, j) corresponding to each directed path from i to j in G. The resultant digraph G’ representation in the form of the adjacency matrix is called the connectivity matrix.

Given a directed graph, return its connectivity matrix. The value of a cell C[i][j] in connectivity matrix C is 1 only if a directed path exists from vertex i to vertex j.

Input: Graph [edges = [(0, 2), (1, 0), (3, 1)], n = 4]
Output: [
    [1, 0, 1, 0],
    [1, 1, 1, 0],
    [0, 0, 1, 0],
    [1, 1, 1, 1]
]

Note that all diagonal elements in the connectivity matrix are 1 since a path exists from every vertex to itself.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.

*/

#include <iostream>
#include <vector>

using namespace std;

class Graph
{
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

class Solution
{
private:
    void DFS(Graph const &graph, vector<vector<int>> &C, int root, int descendant) {
        for (int child: graph.adjList[descendant]) {
            if (!C[root][child]) {
                C[root][child] = 1;
                DFS(graph, C, root, child);
            }
        }
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    vector<vector<int>> findConnectivityMatrix(Graph const &graph)
    {
        vector<vector<int>> C(graph.n, vector<int>(graph.n, 0));

        // consider each vertex and start DFS from it
        for (int v = 0; v < graph.n; v++)
        {
            C[v][v] = 1;
            DFS(graph, C, v, v);
        }

        return C;
    }
};

ostream& operator<<(ostream& os, const vector<vector<int>>& matrix) {
    for(auto& r : matrix) {
        for(auto c : r) {
            os << c << ' ';
        }
        os << '\n';
    }
    return os;
}

int main()
{
    /*
    Input: Graph [edges = [(0, 2), (1, 0), (3, 1)], n = 4]
    Output: [
        [1, 0, 1, 0],
        [1, 1, 1, 0],
        [0, 0, 1, 0],
        [1, 1, 1, 1]
    ]
    */
    Graph g{{{2}, {0}, {}, {1}}, 4};

    cout << Solution().findConnectivityMatrix(g) << endl;

    return 0;
}
