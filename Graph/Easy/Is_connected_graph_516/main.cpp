/*

Given an undirected graph, determine whether it is connected or not. A graph is said to be connected when a path exists between every pair of vertices in the graph.

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 5), (4, 1)], n = 6]
Output: true

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 5), (4, 6), (4, 8), (7, 8)], n = 9]
Output: false

Input: Graph [edges = [(0, 1), (1, 3), (2, 3), (3, 5)], n = 6]
Output: false

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.

*/

#include <iostream>
#include <vector>
#include <unordered_set>
#include <queue>
#include <stack>

using namespace std;

class Graph
{
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

class Solution
{
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool isConnected(Graph const &graph)
    {
        unordered_set<int> visited;
        //queue<int> q; // DFS
        stack<int> s; // BFS

        //q.push(0);
        s.push(0);
        visited.insert(0);

        /*while (!q.empty()) {
            int v = q.front();
            q.pop();*/
        while (!s.empty()) {
            int v = s.top();
            s.pop();

            for (int u : graph.adjList[v]) {
                if (visited.find(u) == visited.end()) {
                    visited.insert(u);

                    //q.push(u);
                    s.push(u);
                }
            }
        }

        return visited.size() == graph.n;
    }
};

int main()
{
    {
        // Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 5), (4, 1)], n = 6]
        // Output: true
        Graph g{{
                {1}, // (0, 1)
                {0, 2, 4}, // (1, 0) (1, 2) (1, 4)
                {1, 3}, // (2, 1) (2, 3)
                {2, 5}, // (3, 2) (3, 5)
                {1}, // (4, 1)
                {3} // (5, 3)
                },6};
        cout << boolalpha << Solution().isConnected(g) << endl;
    }

    {
        // Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 5), (4, 6), (4, 8), (7, 8)], n = 9]
        // Output: false
        Graph g{{
                {1}, // (0, 1)
                {0, 2}, // (1, 0) (1, 2)
                {1, 3}, // (2, 1) (2, 3)
                {2, 5}, // (3, 2) (3, 5)
                {6, 8}, // (4, 6) (4, 8)
                {3}, // (5, 3)
                {4}, // (6, 4)
                {8}, // (7, 8)
                {4, 7} // (8, 4) (8, 7)
                }, 9};
        cout << boolalpha << Solution().isConnected(g) << endl;
    }

    {
        // Input: Graph [edges = [(0, 1), (1, 3), (2, 3), (3, 5)], n = 6]
        // Output: false
        Graph g{{
                {1}, // (0, 1)
                {3}, // (1, 3)
                {3}, // (2, 3)
                {1, 2, 5}, // (3, 1) (3, 2) (3, 5)
                {},
                {3} // (5, 3)
                }, 6};
        cout << boolalpha << Solution().isConnected(g) << endl;
    }

    return 0;
}
