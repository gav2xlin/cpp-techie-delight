/*

Given an undirected connected graph, return a set of all bridges in the graph. A bridge is an edge of a graph whose removal disconnects the graph.

Input: Graph [edges = [(0, 1), (1, 2), (2, 3)], n = 4]
Output: {(0, 1), (1, 2), (2, 3)}
Explanation: The graph has 3 bridges {(0, 1), (1, 2), (2, 3)}, since removal of any of it disconnects the graph.

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 0)], n = 4]
Output: {}
Explanation: The graph is 2–edge connected. i.e., it remains connected on removal of any edge.

Note: The solution should return edges in increasing order of node's value. For instance, both (1, 2) and (2, 1) represent the same edge in an undirected graph. The solution should return edge (1, 2).

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The graph is connected, i.e., every node can be reached starting from all other nodes.

*/

#include <iostream>
#include <vector>
#include <utility>
#include <set>

using namespace std;

using Edge = pair<int, int>;

class Graph
{
public:
    vector<vector<int>> adjList;
    int n;

    Graph(vector<Edge> const &edges, int n)
    {
        adjList.resize(n);
        this->n = n;

        for (auto &edge: edges)
        {
            adjList[edge.first].push_back(edge.second);
            adjList[edge.second].push_back(edge.first);
        }
    }
};

class Solution
{
private:
    int DFS(Graph const &graph, int v, vector<bool> visited, vector<int> &arrival, int parent, int &time, auto &bridges)
    {
        arrival[v] = ++time;

        visited[v] = true;

        int t = arrival[v];

        for (int w: graph.adjList[v])
        {
            if (!visited[w]) {
                t = min(t, DFS(graph, w, visited, arrival, v, time, bridges));
            }
            else if (w != parent)
            {
                // If vertex `w` is already visited, there is a back edge starting
                // from `v`. Note that as visited[u] is already
                // true, arrival[u] is already defined
                t = min(t, arrival[w]);
            }
        }

        // if the value of `t` remains unchanged, i.e., it is equal
        // to the arrival time of vertex `v`, and if `v` is not the root node,
        // then (parent[v] —> v) forms a bridge
        if (t == arrival[v] && parent != -1) {
            bridges.insert({parent, v});
        }

        return t;
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    set<Edge> findBridges(Graph const &graph)
    {
        int n = graph.n;

        vector<bool> visited(n);
        vector<int> arrival(n);

        int start = 0, parent = -1, time = 0;

        set<Edge> bridges;

        DFS(graph, start, visited, arrival, parent, time, bridges);

        return bridges;
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ':' << p.second;
    return os;
}

ostream& operator<<(ostream& os, const set<pair<int,int>>& values) {
    for (auto& v : values) {
        os << v << endl;
    }
    return os;
}

int main()
{
    {
        vector<Edge> edges {
            {0, 1}, {1, 2}, {2, 3}
        };
        Graph graph(edges, 4);

        auto bridges = Solution().findBridges(graph);

        cout << bridges << endl;
    }

    {
        vector<Edge> edges {
            {0, 1}, {1, 2}, {2, 3}, {3, 0}
        };
        Graph graph(edges, 4);

        auto bridges = Solution().findBridges(graph);

        cout << bridges << endl;
    }

    return 0;
}
