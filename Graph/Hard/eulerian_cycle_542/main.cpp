/*

Given a list of edges of a directed graph, check if it is possible to construct a cycle that visits each edge exactly once, i.e., check whether the graph has an Eulerian cycle or not.

Input: edges = [(0, 1), (1, 2), (1, 4), (2, 3), (3, 1), (4, 3), (3, 0)]
Output: true
Explanation: The graph has an Eulerian cycle [0 -> 1 -> 2 -> 3 -> 1 -> 4 -> 3 -> 0]

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Edge {
public:
    int src, dest;
};

class Graph
{
public:
    vector<vector<int>> adjList;
    vector<int> in;
    int n{0};

    Graph(vector<Edge> const &edges = {})
    {
        for (auto &edge: edges) {
            addEdge(edge.src, edge.dest);
        }
    }

    void addEdge(int u, int v)
    {
        n = max(n, max(u, v) + 1);

        if (adjList.size() < n) {
            adjList.resize(n);
            in.resize(n);
        }

        adjList[u].push_back(v);
        in[v]++;
    }
};

class Solution
{
private:
    void DFS(Graph const &graph, int u, vector<bool> &visited)
    {
        visited[u] = true;

        for (int v: graph.adjList[u])
        {
            if (!visited[v]) {
                DFS(graph, v, visited);
            }
        }
    }

    Graph buildTranspose(Graph const &graph, int n)
    {
        Graph g;

        for (int u = 0; u < n; ++u)
        {
            for (int v: graph.adjList[u]) {
                g.addEdge(v, u);
            }
        }

        return g;
    }

    bool isVisited(Graph const &graph, const vector<bool> &visited)
    {
        for (int i = 0; i < visited.size(); ++i)
        {
            if (graph.adjList[i].size() && !visited[i]) {
                return false;
            }
        }

        return true;
    }

    bool isSC(Graph const &graph)
    {
        int n = graph.n;

        vector<bool> visited(n);

        int i;
        for (i = 0; i < n; ++i)
        {
            if (graph.adjList[i].size())
            {
                DFS(graph, i, visited);
                break;
            }
        }

        if (!isVisited(graph, visited)) {
            return false;
        }

        fill(visited.begin(), visited.end(), false);

        Graph g = buildTranspose(graph, n);

        DFS(g, i, visited);

        return isVisited(g, visited);
    }
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int src, dest;
        };
    */

    bool hasEulerianCycle(vector<Edge> const &edges)
    {
        Graph graph(edges);

        for (int i = 0; i < graph.n; ++i)
        {
            if (graph.adjList[i].size() != graph.in[i]) {
                return false;
            }
        }

        return isSC(graph);
    }
};

int main()
{
    vector<Edge> edges = {
        {0, 1}, {1, 2}, {2, 3}, {3, 1}, {1, 4}, {4, 3}, {3, 0}
    };

    cout << boolalpha << Solution().hasEulerianCycle(edges) << endl;

    return 0;
}
