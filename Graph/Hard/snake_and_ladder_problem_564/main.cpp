/*

Given a 10 x 10 board of snake and ladder game, marked with numbers in the range [1-100], find the minimum number of throws required to win it (reach board #1 to board #100).

Input: Map of snakes and ladders

Ladder = {80=99, 1=38, 51=67, 4=14, 21=42, 72=91, 9=31, 28=84}
Snake = {64=60, 17=7, 98=79, 54=34, 87=36, 93=73, 62=19, 95=75}

Output: 7

Explanation: The game requires at least 7 dice throws to win.

*/

#include <iostream>
#include <unordered_map>
#include <queue>

using namespace std;

// Total number of vertices in the graph
// 10 x 10 board
#define N 100

struct Edge {
    int src, dest;
};

class Graph
{
public:
    vector<int> adjList[N + 1];

    // Constructor
    Graph(vector<Edge> const &edges)
    {
        for (Edge const &edge: edges)
        {
            adjList[edge.src].push_back(edge.dest);
        }
    }
};

struct Node
{
    int ver, min_dist;
};

class Solution
{
private:
    int BFS(Graph const &g, int source)
    {
        queue<Node> q;

        vector<bool> discovered(N + 1);
        discovered[source] = true;

        Node node = { source, 0 };
        q.push(node);

        while (!q.empty())
        {
            node = q.front();
            q.pop();

            if (node.ver == N) {
                return node.min_dist;
            }

            for (int u: g.adjList[node.ver])
            {
                if (!discovered[u])
                {
                    discovered[u] = true;

                    Node n = {u, node.min_dist + 1};
                    q.push(n);
                }
            }
        }

        return -1;
    }
public:
    int findMinimumMoves(unordered_map<int, int> &ladder, unordered_map<int, int> &snake)
    {
        vector<Edge> edges;
        for (int i = 0; i < N; i++)
        {
            for (int j = 1; j <= 6 && i + j <= N; j++)
            {
                int src = i;

                int dest = (ladder[i + j] || snake[i + j]) ? (ladder[i + j] + snake[i + j]) : (i + j);

                Edge e = { src, dest };
                edges.push_back(e);
            }
        }

        Graph g(edges);

        return BFS(g, 0);
    }
};

int main()
{
    unordered_map<int, int> ladder, snake;

    // insert ladders into the map
    ladder[1] = 38;
    ladder[4] = 14;
    ladder[9] = 31;
    ladder[21] = 42;
    ladder[28] = 84;
    ladder[51] = 67;
    ladder[72] = 91;
    ladder[80] = 99;

    // insert snakes into the map
    snake[17] = 7;
    snake[54] = 34;
    snake[62] = 19;
    snake[64] = 60;
    snake[87] = 36;
    snake[93] = 73;
    snake[95] = 75;
    snake[98] = 79;

    cout << Solution().findMinimumMoves(ladder, snake) << endl;

    return 0;
}
