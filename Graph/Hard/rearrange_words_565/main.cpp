/*

Given a list of words, check if the individual words can be rearranged to form a circle. Two words, X and Y, can be put together in a circle if the last character of X is the same as the first character of Y or vice-versa.

Input: [ANT, OSTRICH, DEER, TURKEY, KANGAROO, TIGER, RABBIT, RAT, TOAD, YAK, HYENA]

Output: true

Explanation: The words can be rearranged as follows to form a circle. Note that, for any pair of consecutive words (X → Y) in the circle, the last character of the word X is the same as the first character of the word Y.

   ANT → TIGER → RABBIT → TOAD
    ↑					  ↓
  HYENA				   DEER
    ↑					  ↓
 OSTRICH				  RAT
    ↑					  ↓
 KANGAROO   ←  YAK   ←   TURKEY

*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
private:
    static constexpr int CHAR_SIZE = 128;

    class Graph
    {
    public:
        vector<vector<int>> adjList;

        Graph(int n)
        {
            adjList.resize(n);
        }

        void addEdge(int u, int v) {
            adjList[u].push_back(v);
        }
    };

    void DFS(Graph const &graph, int u, vector<bool> &visited)
    {
        visited[u] = true;

        for (int v: graph.adjList[u])
        {
            if (!visited[v]) {
                DFS(graph, v, visited);
            }
        }
    }

    Graph transpose(Graph const &graph, int n)
    {
        Graph g(n);

        for (int u = 0; u < n; ++u)
        {
            for (int v: graph.adjList[u]) {
                g.addEdge(v, u);
            }
        }
        return g;
    }

    bool isVisited(Graph const &graph, const vector<bool> &visited)
    {
        for (int i = 0; i < visited.size(); i++)
        {
            if (graph.adjList[i].size() && !visited[i]) {
                return false;
            }
        }
        return true;
    }

bool isSC(Graph const &graph, int n)
{
    vector<bool> visited(n);

    int i;
    for (i = 0; i < n; ++i)
    {
        if (graph.adjList[i].size())
        {
            DFS(graph, i, visited);
            break;
        }
    }

    if (!isVisited(graph, visited)) {
        return false;
    }

    fill(visited.begin(), visited.end(), false);

    Graph g = transpose(graph, n);

    DFS(g, i, visited);

    return isVisited(g, visited);
}
public:
    bool checkArrangement(vector<string> const &words)
    {
        Graph graph(CHAR_SIZE);

        vector<int> in(CHAR_SIZE);

        for (const string &s: words)
        {
            int src = s.front();
            int dest = s.back();

            graph.addEdge(src, dest);

            in[dest]++;
        }

        /*
            If the constructed graph has an Eulerian cycle, only then can the given words
            be rearranged to form a circle
        */

        for (int i = 0; i < CHAR_SIZE; ++i)
        {
            if (graph.adjList[i].size() != in[i]) {
                return false;
            }
        }

        // Check if all non-isolated vertices belong to a single
        // strongly connected component
        return isSC(graph, CHAR_SIZE);
    }
};

int main()
{
    vector<string> words {
        "ANT", "OSTRICH", "DEER", "TURKEY", "KANGAROO",
        "TIGER", "RABBIT", "RAT", "TOAD", "YAK", "HYENA"
    };

    cout << boolalpha << Solution().checkArrangement(words) << endl;

    return 0;
}
