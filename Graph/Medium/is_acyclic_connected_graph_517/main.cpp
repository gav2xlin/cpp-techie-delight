/*

Given an undirected graph, check if it is a tree or not. In other words, check if a given undirected graph is an Acyclic Connected Graph or not.

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5)], n = 6]
Output: true
Explanation: Graph is connected and has no cycles.

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 0)], n = 6]
Output: false
Explanation: Graph contains cycle [0 —> 1 —> 2 —> 3 —> 4 —> 5 —> 0].

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The graph is connected, i.e., every node can be reached starting from all other nodes.

*/

#include <iostream>
#include <vector>
#include <stack>

using namespace std;

class Graph
{
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

class Solution
{
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool isTree(Graph const &graph)
    {
        int n = graph.n, v = 0;

        vector<bool> discovered(n);
        vector<bool> parents(n);

        discovered[v] = true;

        stack<int> s;
        s.push(v);

        while (!s.empty()) {
            v = s.top();
            s.pop();

            parents[v] = true;

            for (int u: graph.adjList[v]) {
                if (!discovered[u]) {
                    discovered[u] = true;

                    s.push(u);
                } else if (!parents[u]) {
                    return false;
                }
            }
        }

        for (int i = 0; i < n; ++i) {
            if (!discovered[i]) {
                return false;
            }
        }

        return true;
    }
};

int main()
{
    {
        /*
        Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5)], n = 6]
        Output: true
        Explanation: Graph is connected and has no cycles.
         */
        Graph g{{
                {1}, // (0, 1)
                {0, 2}, // (1, 0) (1, 2)
                {1, 3}, // (2, 1) (2, 3)
                {2, 4}, // (3, 2) (3, 4)
                {3, 5}, // (4, 3) (4, 5)
                {4} // (5, 4)
                }, 6};

        cout << boolalpha << Solution().isTree(g) << endl;
    }

    {
        /*
        Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 0)], n = 6]
        Output: false
        Explanation: Graph contains cycle [0 —> 1 —> 2 —> 3 —> 4 —> 5 —> 0].
         */
        Graph g{{
                {1, 5}, // (0, 1) + (0, 5)
                {2, 0}, // (1, 2) + (1, 0)
                {3, 1}, // (2, 3) + (2, 1)
                {4, 2}, // (3, 4) + (3, 2)
                {5, 3}, // (4, 5) + (4, 3)
                {0, 4} // (5, 0) + (5, 4)
                }, 6};

        cout << boolalpha << Solution().isTree(g) << endl;
    }

    return 0;
}
