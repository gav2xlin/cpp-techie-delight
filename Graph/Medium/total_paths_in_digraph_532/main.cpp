/*

Given a directed graph, two vertices - source and destination, and a positive number m, find the total number of routes to reach the destination vertex from the source vertex with exactly m edges.

Input: Graph [edges = [(0, 6), (0, 1), (1, 6), (1, 5), (1, 2), (2, 3), (3, 4), (5, 2), (5, 3), (5, 4), (6, 5), (7, 6), (7, 1)], n = 8], src = 0, dest = 3, m = 4
Output: 3
Explanation: The graph has 3 routes from source 0 to destination 3 with 4 edges.

0 —> 1 —> 5 —> 2 —> 3
0 —> 1 —> 6 —> 5 —> 3
0 —> 6 —> 5 —> 2 —> 3

Note: Edge (x, y, w) represents an edge from x to y having weight w.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.

*/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

struct Edge {
    int src, dest;
};

class Graph
{
public:
    vector<vector<int>> adjList;

    Graph(vector<Edge> const &edges, int n)
    {
        adjList.resize(n);

        for (auto &edge: edges) {
            adjList[edge.src].push_back(edge.dest);
        }
    }
};

class Solution
{
private:
    struct Node
    {
        int vertex, depth;
    };
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    int findTotalPaths(Graph const &graph, int src, int dest, int m)
    {
        queue<Node> q;
        q.push({src, 0});

        int count = 0;

        while (!q.empty())
        {
            Node& node = q.front();
            q.pop();

            int v = node.vertex;
            int depth = node.depth;

            if (v == dest && depth == m) {
                ++count;
            }

            if (depth > m) {
                break;
            }

            for (int u: graph.adjList[v])
            {
                q.push({u, depth + 1});
            }
        }

        return count;
    }
};

int main()
{
    vector<Edge> edges ={{0, 6}, {0, 1}, {1, 6}, {1, 5}, {1, 2}, {2, 3}, {3, 4}, {5, 2}, {5, 3}, {5, 4}, {6, 5}, {7, 6}, {7, 1}};
    Graph graph(edges, 8);

    cout << Solution().findTotalPaths(graph, 0, 3, 4) << endl;

    return 0;
}
