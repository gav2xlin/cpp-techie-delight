/*

Given a list of edges of a directed acyclic graph (DAG), return its topological order using topological sort algorithm. If the graph has more than one topological ordering, return any of them.

Input: edges = [(0, 1), (0, 2)]
Output: [0, 1, 2] or [0, 2, 1]

Input: edges = [(0, 6), (1, 2), (1, 4), (1, 6), (3, 0), (3, 4), (5, 1), (7, 0), (7, 1)]
Output: [3, 5, 7, 0, 1, 2, 4, 6], or any other valid topological ordering like,

[3, 5, 7, 0, 1, 2, 6, 4]
[3, 5, 7, 0, 1, 4, 2, 6]
[3, 5, 7, 0, 1, 4, 6, 2]
[3, 5, 7, 0, 1, 6, 2, 4]
[3, 5, 7, 0, 1, 6, 4, 2]
[3, 5, 7, 1, 0, 2, 4, 6]
[3, 5, 7, 1, 0, 2, 6, 4]
…

Constraints:

• The maximum number of nodes in the graph is 100.
• There are no cycles in the graph.

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

using namespace std;

class Edge {
public:
    int src, dest;
};

class Graph
{
public:
    vector<vector<int>> adjList;

    Graph(vector<Edge> const &edges) {
        for (auto &edge: edges) {
            if (edge.src >= adjList.size() || edge.dest >= adjList.size()) {
                adjList.resize(max(edge.src, edge.dest) + 1);
            }
            adjList[edge.src].push_back(edge.dest);
        }
    }
};

class Solution
{
private:
    void dfs(Graph const &graph, int v, vector<bool> &visited, stack<int>& trackOrder)
    {
        if (visited[v]) {
            return;
        }
        visited[v] = true;

        for (int u: graph.adjList[v]) {
            if (!visited[u]) {
                dfs(graph, u, visited, trackOrder);
            }
        }

        trackOrder.push(v);
    }
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int src, dest;
        };
    */

    vector<int> findTopologicalOrdering(vector<Edge> const &edges)
    {
        Graph graph{edges};

        int n = graph.adjList.size();
        vector<bool> visited(n);
        stack<int> trackOrder;

        for (int v = 0; v < n; ++v) {
            if (!visited[v]) {
                dfs(graph, v, visited, trackOrder);
            }
        }

        vector<int> order;
        order.reserve(n);

        while (!trackOrder.empty()) {
            order.push_back(trackOrder.top());
            trackOrder.pop();
        }

        return order;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    {
        /*
        Input: edges = [(0, 1), (0, 2)]
        Output: [0, 1, 2] or [0, 2, 1]
        */
        vector<Edge> edges{{0, 1}, {0, 2}};

        cout << Solution().findTopologicalOrdering(edges) << endl;
    }

    {
        /*
        Input: edges = [(0, 6), (1, 2), (1, 4), (1, 6), (3, 0), (3, 4), (5, 1), (7, 0), (7, 1)]
        Output: [3, 5, 7, 0, 1, 2, 4, 6], or any other valid topological ordering like,

        [3, 5, 7, 0, 1, 2, 6, 4]
        [3, 5, 7, 0, 1, 4, 2, 6]
        [3, 5, 7, 0, 1, 4, 6, 2]
        [3, 5, 7, 0, 1, 6, 2, 4]
        [3, 5, 7, 0, 1, 6, 4, 2]
        [3, 5, 7, 1, 0, 2, 4, 6]
        [3, 5, 7, 1, 0, 2, 6, 4]
        */
        vector<Edge> edges{{0, 6}, {1, 2}, {1, 4}, {1, 6}, {3, 0}, {3, 4}, {5, 1}, {7, 0}, {7, 1}};

        cout << Solution().findTopologicalOrdering(edges) << endl;
    }

    return 0;
}
