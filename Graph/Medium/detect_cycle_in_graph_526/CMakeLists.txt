cmake_minimum_required(VERSION 3.5)

project(detect_cycle_in_graph_526 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(detect_cycle_in_graph_526 main.cpp)

install(TARGETS detect_cycle_in_graph_526
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
