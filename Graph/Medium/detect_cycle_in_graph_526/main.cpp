/*

Given an undirected graph, determine whether it contains any cycle.

Input: Graph [edges = [(0, 1), (1, 2), (0, 2)], n = 3]
Output: true

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (1, 4)], n = 5]
Output: false

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The graph is connected, i.e., every node can be reached starting from all other nodes.

*/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Graph
{
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

class Solution
{
private:
    struct Node {
        int v, parent;
    };

    bool bfs(Graph const &graph, int src)
    {
        int n = graph.n;

        vector<bool> discovered(n);
        discovered[src] = true;

        queue<Node> q;
        q.push({src, -1});

        while (!q.empty())
        {
            Node node = q.front();
            q.pop();

            for (int u: graph.adjList[node.v])
            {
                if (!discovered[u])
                {
                    discovered[u] = true;
                    q.push({ u, node.v });
                } else if (u != node.parent) {
                    return true;
                }
            }
        }

        return false;
    }

    struct Edge {
        int src, dest;
    };

    bool dfs(Graph const &graph, int v, vector<bool> &discovered, int parent)
    {
        discovered[v] = true;

        for (int w: graph.adjList[v])
        {
            if (!discovered[w])
            {
                if (dfs(graph, w, discovered, v)) {
                    return true;
                }
            } else if (w != parent) {
                return true;
            }
        }

        return false;
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool detectCycle(Graph const &graph)
    {
        //return bfs(graph, 0);
        vector<bool> discovered(graph.n);
        return dfs(graph, 0, discovered, -1);
    }
};

int main()
{
    Solution s;

    {
        /*
        Input: Graph [edges = [(0, 1), (1, 2), (0, 2)], n = 3]
        Output: true
        */
        Graph g{{
                {1, 2}, // (0, 1) (0, 2)
                {2}, // (1, 2)
                {} //
                }, 3};

        cout << boolalpha << s.detectCycle(g) << endl;
    }

    {
        /*
        Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (1, 4)], n = 5]
        Output: false
        */
        Graph g{{
                {1}, // (0, 1)
                {2, 4}, // (1, 2) (1, 4)
                {3}, // (2, 3)
                {},
                {}
                }, 5};

        cout << boolalpha << s.detectCycle(g) << endl;
    }

    return 0;
}
