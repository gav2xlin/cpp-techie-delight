/*

Given a list of edges of a weighted directed graph where each edge weight can be one of x, 2x, or 3x (x is a positive integer) and two vertices - source and destination, efficiently compute the least-cost path from the source vertex to the destination vertex and return the least cost.

Input: edges = [(0, 1, 3), (0, 4, 1), (1, 2, 1), (1, 3, 3), (1, 4, 1), (4, 2, 2), (4, 3, 1)], src = 0, dest = 2
Output: 3
Explanation: Edge (x, y, w) represents an edge from x to y having weight w. The least-cost path from source to destination is [0, 4, 2] having cost 3.

Input: edges = [(0, 1, 3), (0, 4, 1), (1, 2, 1), (1, 3, 3), (1, 4, 1), (4, 2, 2), (4, 3, 1)], src = 1, dest = 3
Output: 2
Explanation: The least-cost path from source to destination is [1, 4, 3] having cost 2.

Constraints:

• The maximum number of nodes in the graph is 100.
• The destination can be reached from the source.

*/

#include <iostream>
#include <vector>
#include <queue>
#include <algorithm>
#include <limits>

using namespace std;

class Edge {
public:
    int src, dest, weight;
};

class Solution
{
private:
    class Graph
    {
    private:
        int getx(vector<Edge> const &edges) {
            int minw = numeric_limits<int>::max();

            for (auto &edge: edges)
            {
                int weight = edge.weight;
                minw = min(minw, weight);
            }

            return minw; // maxw = 3 * minw because "each edge weight can be one of x, 2x, or 3x"
        }

        int getn(vector<Edge> const &edges) {
            int n = 0;

            for (auto &edge: edges)
            {
                int v = edge.src;
                int u = edge.dest;
                n = max(n, max(v + 1, u + 1));
            }

            return n;
        }
    public:
        vector<vector<int>> adjList;
        int n;

        Graph(vector<Edge> const &edges)
        {
            int x = getx(edges);
            n = getn(edges);

            adjList.resize(3 * n);

            for (auto &edge: edges)
            {
                int v = edge.src;
                int u = edge.dest;
                int weight = edge.weight;

                // create two new vertices, `v+n` and `v+2×n`, if the edge's weight is `3x`
                // Also, split edge (v, u) into (v, v+n), (v+n, v+2N) and (v+2N, u),
                // each having weight `x`.
                if (weight == 3 * x)
                {
                    adjList[v].push_back(v + n);
                    adjList[v + n].push_back(v + 2 * n);
                    adjList[v + 2 * n].push_back(u);
                }
                // create one new vertex `v+n` if the weight of the edge is `2x`.
                // Also, split edge (v, u) into (v, v+n), (v+n, u) each having weight `x`
                else if (weight == 2 * x)
                {
                    adjList[v].push_back(v + n);
                    adjList[v + n].push_back(u);
                }
                // no splitting is needed if the edge weight is `1x`
                else {
                    adjList[v].push_back(u);
                }
            }
        }
    };

    void calcCost(vector<int> const &predecessor, int v, int &cost, int n)
    {
        if (v < 0) {
            return;
        }

        calcCost(predecessor, predecessor[v], cost, n);
        ++cost;
    }

public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int src, dest, weight;
        };
    */

    // Perform BFS on the graph starting from vertex source
    int findLeastPathCost(vector<Edge> const &edges, int src, int dest)
    {
        Graph graph(edges);

        int n = graph.n;

        vector<bool> discovered(3 * n, false);
        discovered[src] = true;

        vector<int> predecessor(3 * n, -1);

        queue<int> q;
        q.push(src);

        while (!q.empty())
        {
            int curr = q.front();
            q.pop();

            if (curr == dest)
            {
                int cost = -1;
                calcCost(predecessor, dest, cost, n);
                return cost;
            }

            for (int v: graph.adjList[curr])
            {
                if (!discovered[v])
                {
                    discovered[v] = true;
                    q.push(v);

                    predecessor[v] = curr;
                }
            }
        }

        return 0;
    }
};

int main()
{
    {
        /*
        Input: edges = [(0, 1, 3), (0, 4, 1), (1, 2, 1), (1, 3, 3), (1, 4, 1), (4, 2, 2), (4, 3, 1)], src = 0, dest = 2
        Output: 3
        Explanation: Edge (x, y, w) represents an edge from x to y having weight w. The least-cost path from source to destination is [0, 4, 2] having cost 3.
        */
        vector<Edge> edges {{0, 1, 3}, {0, 4, 1}, {1, 2, 1}, {1, 3, 3}, {1, 4, 1}, {4, 2, 2}, {4, 3, 1}};

        cout << Solution().findLeastPathCost(edges, 0, 2) << endl;
    }

    {
        /*
        Input: edges = [(0, 1, 3), (0, 4, 1), (1, 2, 1), (1, 3, 3), (1, 4, 1), (4, 2, 2), (4, 3, 1)], src = 1, dest = 3
        Output: 2
        Explanation: The least-cost path from source to destination is [1, 4, 3] having cost 2.
        */
        vector<Edge> edges {{0, 1, 3}, {0, 4, 1}, {1, 2, 1}, {1, 3, 3}, {1, 4, 1}, {4, 2, 2}, {4, 3, 1}};

        cout << Solution().findLeastPathCost(edges, 1, 3) << endl;
    }

    {
        vector<Edge> edges {{0, 1, 2}};

        cout << Solution().findLeastPathCost(edges, 0, 1) << endl; // 2
    }

    {
        vector<Edge> edges {{0, 1, 2}, {1, 2, 2}, {1, 2, 2}, {1, 3, 2}, {1, 4, 2}, {4, 2, 2}, {4, 3, 2}};

        cout << Solution().findLeastPathCost(edges, 0, 1) << endl; // 2
    }

    return 0;
}
