/*

Given a weighted directed acyclic graph (DAG) and two vertices - source and destination, find the least path cost from the source vertex to the destination vertex.

Input: Graph [edges = [(0, 6, 2), (1, 2, -4), (1, 4, 1), (1, 6, 8), (3, 0, 3), (3, 4, 5), (5, 1, 2), (7, 0, 6), (7, 1, -1), (7, 3, 4), (7, 5, -4)], n = 8], src = 7, dest = 0
Output: 6

Note: Edge (x, y, w) represents an edge from x to y having weight w.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The destination can be reached from the source.

*/

#include <iostream>
#include <vector>
#include <iomanip>
#include <climits>

using namespace std;

struct Edge {
    int src, dest, weight;
};

class Graph
{
public:
    vector<vector<Edge>> adjList;
    int n;

    Graph(vector<Edge> const &edges, int n)
    {
        adjList.resize(n);
        this->n = n;

        for (Edge const &edge: edges) {
            adjList[edge.src].push_back(edge);
        }
    }
};

class Solution
{
private:
    void DFS(Graph const &graph, int v, vector<bool> &discovered, vector<int> &departure, int &time)
    {
        discovered[v] = true;

        // set arrival time – not needed
        // ++time;

        for (Edge e: graph.adjList[v])
        {
            int u = e.dest;

            if (!discovered[u]) {
                DFS(graph, u, discovered, departure, time);
            }
        }

        departure[time] = v;
        ++time;
    }
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int src, dest, weight;
        };

        // Definition for a Graph
        class Graph
        {
        public:

            // vector of vectors to represent an adjacency list
            vector<vector<pair<int,int>>> adjList;

            // total number of nodes in the graph
            int n;

            Graph(vector<Edge> &edges, int size)
            {
                n = size;
                adjList.resize(n);

                for (Edge &edge: edges) {
                    adjList[edge.src].push_back(make_pair(edge.dest, edge.weight));
                }
            }
        };
    */

    int findLeastCost(Graph const &graph, int src, int dest)
    {
        int n = graph.n;

        vector<int> departure(n, -1);

        vector<bool> discovered(n);
        int time = 0;

        for (int i = 0; i < n; ++i)
        {
            if (!discovered[i]) {
                DFS(graph, i, discovered, departure, time);
            }
        }

        vector<int> cost(n, INT_MAX);
        cost[src] = 0;

        // Process the vertices in topological order, i.e., in order
        // of their decreasing departure time in DFS
        for (int i = n - 1; i >= 0; --i)
        {
            // for each vertex in topological order,
            // relax the cost of its adjacent vertices
            int v = departure[i];
            for (const Edge& e: graph.adjList[v])
            {
                int u = e.dest;
                int w = e.weight;

                if (cost[v] != INT_MAX && cost[v] + w < cost[u]) {
                    cost[u] = cost[v] + w;
                }
            }
        }

        /*for (int i = 0; i < n; ++i)
        {
            cout << "dist(" << src << ", " << i << ") = " << setw(2) << cost[i] << endl;
        }*/
        return cost[dest];
    }
};

int main()
{
    vector<Edge> edges{{0, 6, 2}, {1, 2, -4}, {1, 4, 1}, {1, 6, 8}, {3, 0, 3}, {3, 4, 5}, {5, 1, 2}, {7, 0, 6}, {7, 1, -1}, {7, 3, 4}, {7, 5, -4}};
    Graph graph(edges, 8);

    cout << Solution().findLeastCost(graph, 7, 0) << endl;

    return 0;
}
