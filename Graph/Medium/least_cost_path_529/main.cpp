/*

Given a weighted digraph (directed graph), two vertices - source and destination, and a positive number m, find the least-cost path from the source to the destination with exactly m edges and return the least cost.

Input: Graph [edges = [(0, 6, -1), (0, 1, 5), (1, 6, 3), (1, 5, 5), (1, 2, 7), (2, 3, 8), (3, 4, 10), (5, 2, -1), (5, 3, 9), (5, 4, 1), (6, 5, 2), (7, 6, 9), (7, 1, 6)], n = 8], src = 0, dest = 3, m = 4
Output: 8
Explanation: The graph has 3 routes from source 0 to destination 3 with 4 edges.

• [0 —> 1 —> 5 —> 2 —> 3] having cost 17
• [0 —> 1 —> 6 —> 5 —> 3] having cost 19
• [0 —> 6 —> 5 —> 2 —> 3] having cost 8

The solution should return the least-cost, i.e., 8.

Note: Edge (x, y, w) represents an edge from x to y having weight w.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The path exists between source and destination with exactly m edges.

*/

#include <iostream>
#include <queue>
#include <climits>

using namespace std;

struct Edge {
    int src, dest, weight;
};

class Graph
{
public:
    vector<vector<Edge>> adjList;

    Graph(vector<Edge> const &edges, int n)
    {
        adjList.resize(n);

        for (auto &edge: edges) {
            adjList[edge.src].push_back(edge);
        }
    }
};

class Solution
{
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int src, dest, weight;
        };

        // Definition for a Graph
        class Graph
        {
        public:

            // vector of vectors to represent an adjacency list
            vector<vector<pair<int,int>>> adjList;

            // total number of nodes in the graph
            int n;

            Graph(vector<Edge> &edges, int size)
            {
                n = size;
                adjList.resize(n);

                for (Edge &edge: edges) {
                    adjList[edge.src].push_back(make_pair(edge.dest, edge.weight));
                }
            }
        };
    */
    struct Node {
        int vertex, depth, weight;
    };

    int findLeastCost(Graph const &g, int src, int dest, int m)
    {
        queue<Node> q;
        q.push({src, 0, 0});

        int minCost = INT_MAX;

        while (!q.empty())
        {
            Node node = q.front();
            q.pop();

            int v = node.vertex;
            int depth = node.depth;
            int cost = node.weight;

            if (v == dest && depth == m) {
                minCost = min(minCost, cost);
            }

            if (depth > m) {
                break;
            }

            for (const Edge& edge: g.adjList[v])
            {
                // push every vertex (discovered or undiscovered) into
                // the queue with depth as +1 of parent and cost equal
                // to the cost of parent plus the current edge weight
                q.push({edge.dest, depth + 1, cost + edge.weight});
            }
        }

        return minCost;
    }
};

int main()
{
    vector<Edge> edges{{0, 6, -1}, {0, 1, 5}, {1, 6, 3}, {1, 5, 5}, {1, 2, 7}, {2, 3, 8}, {3, 4, 10}, {5, 2, -1}, {5, 3, 9}, {5, 4, 1}, {6, 5, 2}, {7, 6, 9}, {7, 1, 6}};
    Graph g(edges, 8);

    cout << Solution().findLeastCost(g, 0, 3, 4) << endl;

    return 0;
}
