/*

Given a directed graph, check if it is a DAG (Directed Acyclic Graph) or not. A DAG is a digraph (directed graph) that contains no cycles.

Input: Graph [edges = [(0, 1), (0, 3), (1, 2), (1, 3), (3, 2), (3, 4), (3, 0), (5, 6), (6, 3)], n = 7]
Output: false
Explanation: The graph contains a cycle [0 -> 1 -> 3 -> 0].

If we remove edge [3 -> 0] from it, it will become a DAG.

Input: Graph [edges = [(0, 1), (0, 3), (1, 2), (1, 3), (3, 2), (3, 4), (5, 6), (6, 3)], n = 7]
Output: true

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.

*/

#include <iostream>
#include <vector>

using namespace std;

class Graph
{
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

enum class Color {
    WHITE, GRAY, BLACK
};

class Solution
{
    bool dfs(Graph const &graph, int v, vector<Color>& color)
    {
        color[v] = Color::GRAY;

        for (int u: graph.adjList[v]) {
            if (color[u] == Color::GRAY) {
                return true;
            }

            if (color[u] == Color::WHITE && dfs(graph, u, color)) {
                return true;
            }
        }

        color[v] = Color::BLACK;

        return false;
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool isDAG(Graph const &graph)
    {
        int n = graph.n;

        vector<Color> color(n, Color::WHITE);

        for (int v = 0; v < n; ++v) {
            if (color[v] == Color::WHITE)
               if (dfs(graph, v, color)) {
                  return false;
               }
        }

        return true;
    }
};

int main()
{
    {
        /*
        Input: Graph [edges = [(0, 1), (0, 3), (1, 2), (1, 3), (3, 2), (3, 4), (3, 0), (5, 6), (6, 3)], n = 7]
        Output: false
        Explanation: The graph contains a cycle [0 -> 1 -> 3 -> 0].
         */
        Graph g{{
                {1, 3},
                {2, 3},
                {},
                {2, 4, 0},
                {},
                {6},
                {3}
                }, 7};

        cout << boolalpha << Solution().isDAG(g) << endl;
    }

    {
        /*
        Input: Graph [edges = [(0, 1), (0, 3), (1, 2), (1, 3), (3, 2), (3, 4), (5, 6), (6, 3)], n = 7]
        Output: true
         */
        Graph g{{
                {1, 3},
                {2, 3},
                {},
                {2, 4},
                {},
                {6},
                {3}
                }, 7};

        cout << boolalpha << Solution().isDAG(g) << endl;
    }

    return 0;
}
