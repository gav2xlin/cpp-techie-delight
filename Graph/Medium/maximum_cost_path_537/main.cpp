/*

Given a weighted undirected graph, find the maximum cost path from the given source vertex to any other vertex which is greater than the given cost. The path should not contain any cycles.

Input: Graph [edges = [(0, 6, 11), (0, 1, 5), (1, 6, 3), (1, 5, 5), (1, 2, 7), (2, 3, -8), (3, 4, 10), (5, 2, -1), (5, 3, 9), (5, 4, 1), (6, 5, 2), (7, 6, 9), (7, 1, 6)], n = 8], source = 0, cost = 50
Here, tuple (x, y, w) represents an edge from x to y having weight w.

Output: 51
Explanation: The maximum cost route from the source vertex 0 to any other vertex in the graph is [0 —> 6 —> 7 —> 1 —> 2 —> 5 —> 3 —> 4]. Its cost is 51 which is more than the given cost 50.

If all paths from the source vertex have their costs less than the given cost, the solution should return INT_MIN.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The graph is connected, i.e., every node can be reached starting from all other nodes.

*/

#include <iostream>
#include <vector>
#include <utility>
#include <queue>
#include <set>
#include <limits>
#include <algorithm>

using namespace std;

class Edge {
public:
    int src, dest, weight;
};

class Graph
{
public:
    vector<vector<pair<int,int>>> adjList;
    int n;

    Graph(vector<Edge> &edges, int size)
    {
        n = size;
        adjList.resize(n);

        for (Edge &edge: edges) {
            adjList[edge.src].push_back(make_pair(edge.dest, edge.weight));
            adjList[edge.dest].push_back(make_pair(edge.src, edge.weight));
        }
    }
};

struct Node
{
    int vertex, weight;
    set<int> s;
};

class Solution
{
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int src, dest, weight;
        };

        // Definition for a Graph
        class Graph
        {
        public:

            // vector of vectors to represent an adjacency list
            vector<vector<pair<int,int>>> adjList;

            // total number of nodes in the graph
            int n;

            Graph(vector<Edge> &edges, int size)
            {
                n = size;
                adjList.resize(n);

                for (Edge &edge: edges) {
                    adjList[edge.src].push_back(make_pair(edge.dest, edge.weight));
                    adjList[edge.dest].push_back(make_pair(edge.src, edge.weight));
                }
            }
        };
    */

    int findMaximumCost(Graph const &graph, int src, int k)
    {
        queue<Node> q;
        set<int> vertices;

        vertices.insert(src);
        q.push({src, 0, vertices});

        int maxCost = numeric_limits<int>::min();

        while (!q.empty())
        {
            Node node = q.front();
            q.pop();

            int v = node.vertex;
            int cost = node.weight;
            vertices = node.s;

            if (cost > k) {
                maxCost = max(maxCost, cost);
            }

            for (auto& edge: graph.adjList[v])
            {
                int dest = edge.first, weight = edge.second;

                if (vertices.find(dest) == vertices.end())
                {
                    set<int> s = vertices;
                    s.insert(dest);

                    q.push({dest, cost + weight, s});
                }
            }
        }

        return maxCost;
    }
};

int main()
{
    vector<Edge> edges{{0, 6, 11}, {0, 1, 5}, {1, 6, 3}, {1, 5, 5}, {1, 2, 7}, {2, 3, -8}, {3, 4, 10}, {5, 2, -1}, {5, 3, 9}, {5, 4, 1}, {6, 5, 2}, {7, 6, 9}, {7, 1, 6}};
    Graph graph(edges, 8);

    cout << Solution().findMaximumCost(graph, 0, 50) << endl;

    return 0;
}
