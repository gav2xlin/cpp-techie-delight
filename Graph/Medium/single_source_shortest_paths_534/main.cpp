/*

Given a list of edges of a weighted directed graph where its edge weights can be negative, return the shortest path cost from a given source vertex to every other reachable vertex in the graph.

Input: edges = [(0, 1, -1), (0, 2, 4), (1, 2, 3), (1, 3, 2), (1, 4, 2), (3, 2, 5), (3, 1, 1), (4, 3, -3)], source = 0
Here, tuple (x, y, w) represents an edge from x to y having weight w.

Output: {(0, 1, -1), (0, 2, 2), (0, 3, -2), (0, 4, 1)}
Here, tuple (s, d, c) indicates that the shortest path from source s to destination d has cost c.

Explanation:

• Shortest path from (0 —> 1) is [0 —> 1] having cost -1.
• Shortest path from (0 —> 2) is [0 —> 1 —> 2] having cost 2.
• Shortest path from (0 —> 3) is [0 —> 1 —> 4 —> 3] having cost -2.
• Shortest path from (0 —> 4) is [0 —> 1 —> 4] having cost 1.

Input: edges = [(0, 1, -1), (0, 2, 4), (1, 2, 3), (1, 3, 2), (1, 4, 2), (3, 2, 5), (3, 1, 1), (4, 3, -3)], source = 1
Output: {(1, 2, 3), (1, 3, -1), (1, 4, 2)}
Explanation:

• Shortest path from (1 —> 0) does not exist.
• Shortest path from (1 —> 2) is [1 —> 2] with cost 3.
• Shortest path from (1 —> 3) is [1 —> 4 —> 3] with cost -1.
• Shortest path from (1 —> 4) is [1 —> 4] with cost 2.

If the graph contains a negative-weight cycle, the solution should return an empty set.

Input: edges = [(0, 2, -2), (1, 0, -4), (2, 3, 2), (3, 1, -1)], source = 1
Output: {}

Constraints:

• The maximum number of nodes in the graph is 100.
• The source vertex is among the set of vertices in the graph.

*/


#include <iostream>
#include <vector>
#include <set>
#include <iomanip>
#include <climits>
#include <vector>
#include <algorithm>

using namespace std;

class Edge {
public:
    int source, dest, weight;
};

class Solution
{
private:
    int getVertexNum(vector<Edge> const &edges) {
        int v = -1;
        for (auto& e : edges) {
            v = max(v, e.source);
            v = max(v, e.dest);
        }
        return v + 1;
    }
public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int source, dest, weight;
        };
    */

    set<vector<int>> findShortestPaths(vector<Edge> const &edges, int source)
    {
        int n = getVertexNum(edges);

        vector<int> distance (n, INT_MAX);
        distance[source] = 0;

        vector<int> parent (n, -1);

        int u, v, w, k = n;

        while (--k)
        {
            for (Edge edge: edges)
            {
                u = edge.source;
                v = edge.dest;
                w = edge.weight;

                if (distance[u] != INT_MAX && distance[u] + w < distance[v])
                {
                    distance[v] = distance[u] + w;
                    parent[v] = u;
                }
            }
        }

        for (Edge edge: edges)
        {
            u = edge.source;
            v = edge.dest;
            w = edge.weight;

            if (distance[u] != INT_MAX && distance[u] + w < distance[v])
            {
                return {};
            }
        }

        set<vector<int>> paths;
        for (int i = 0; i < n; i++)
        {
            if (i != source && distance[i] < INT_MAX)
            {
                paths.insert({source, i, distance[i]});
            }
        }

        return paths;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

ostream& operator<<(ostream& os, const set<vector<int>>& sets) {
    for (auto& s : sets) {
        os << s << endl;
    }
    return os;
}

int main()
{
    {
        /*
        Input: edges = [(0, 1, -1), (0, 2, 4), (1, 2, 3), (1, 3, 2), (1, 4, 2), (3, 2, 5), (3, 1, 1), (4, 3, -3)], source = 0
        Output: {(0, 1, -1), (0, 2, 2), (0, 3, -2), (0, 4, 1)}
        */
        vector<Edge> edges{{0, 1, -1}, {0, 2, 4}, {1, 2, 3}, {1, 3, 2}, {1, 4, 2}, {3, 2, 5}, {3, 1, 1}, {4, 3, -3}};
        cout << Solution().findShortestPaths(edges, 0) << endl;
    }

    {
        /*
        Input: edges = [(0, 1, -1), (0, 2, 4), (1, 2, 3), (1, 3, 2), (1, 4, 2), (3, 2, 5), (3, 1, 1), (4, 3, -3)], source = 1
        Output: {(1, 2, 3), (1, 3, -1), (1, 4, 2)}
        */
        vector<Edge> edges{{0, 1, -1}, {0, 2, 4}, {1, 2, 3}, {1, 3, 2}, {1, 4, 2}, {3, 2, 5}, {3, 1, 1}, {4, 3, -3}};
        cout << Solution().findShortestPaths(edges, 1) << endl;
    }

    return 0;
}
