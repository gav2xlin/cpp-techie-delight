/*

Given a directed graph, return its root vertex. A root vertex of a directed graph is a vertex u with a directed path from u to v for every pair of vertices (u, v) in the graph. In other words, all other vertices in the graph can be reached from the root vertex.

A graph can have multiple root vertices. For example, each vertex in a strongly connected component is a root vertex. In such cases, the solution should return anyone of them. If the graph has no root vertices, the solution should return -1.

Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 0), (4, 3), (4, 5), (5, 0)], n = 6]
Output: 4
Explanation: The root vertex is 4 since it has a path to every other vertex in the graph.

Input: Graph [edges = [(0, 1), (0, 5), (1, 2), (2, 3), (4, 5)], n = 6]
Output: -1
Explanation: The root vertex doesn't exist in the graph.

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Graph
{
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

// https://en.wikipedia.org/wiki/Strongly_connected_component

class Solution
{
private:
    void dfs(Graph const &graph, int u, vector<bool> &visited)
    {
        visited[u] = true;

        for (int v: graph.adjList[u])
        {
            if (!visited[v]) {
                dfs(graph, v, visited);
            }
        }
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    int findRootVertex(Graph const &graph)
    {
        int n = graph.n;

        vector<bool> visited(n);

        int v = 0;
        for (int i = 0; i < n; i++)
        {
            if (!visited[i])
            {
                dfs(graph, i, visited);
                v = i;
            }
        }

        fill(visited.begin(), visited.end(), false);
        dfs(graph, v, visited);

        for (int i = 0; i < n; ++i)
        {
            if (!visited[i]) {
                return -1;
            }
        }

        return v;
    }
};

int main()
{
    Solution s;

    {
        /*
         * Input: Graph [edges = [(0, 1), (1, 2), (2, 3), (3, 0), (4, 3), (4, 5), (5, 0)], n = 6]
         * Output: 4
         */
        Graph g{{
                {1}, // (0, 1)
                {2}, // (1, 2)
                {3}, // (2, 3)
                {0}, // (3, 0)
                {3, 5}, // (4, 3), (4, 5)
                {0} // (5, 0)
                }, 6};

        cout << s.findRootVertex(g) << endl;
    }

    {
        /*
         * Input: Graph [edges = [(0, 1), (0, 5), (1, 2), (2, 3), (4, 5)], n = 6]
         * Output: -1
         */
        Graph g{{
                {1, 5}, // (0, 1), (0, 5)
                {2}, // (1, 2)
                {3}, // (2, 3)
                {}, //
                {5}, // (4, 5)
                {}
                }, 6};

        cout << s.findRootVertex(g) << endl;
    }

    return 0;
}
