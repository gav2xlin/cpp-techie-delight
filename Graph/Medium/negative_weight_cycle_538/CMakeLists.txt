cmake_minimum_required(VERSION 3.5)

project(negative_weight_cycle_538 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(negative_weight_cycle_538 main.cpp)

install(TARGETS negative_weight_cycle_538
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
