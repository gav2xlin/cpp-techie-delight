/*

Given an adjacency matrix representation of a directed graph whose edge weights can be negative, determine if a negative-weight cycle is present in the graph. A negative-weight cycle is a cycle in a graph whose edges sum to a negative value.

Input: Adjacency matrix for the graph

adj = [
    [0,   inf, -2,  inf],
    [4,   0,   -3,  inf],
    [inf, inf, 0,   2],
    [inf, -1,  inf, 0]
]

Here, adj[i][j] = c indicates that the edge (i, j) has weight c. If there is no edge from vertex i to vertex j, adj[i][j] = INT_MAX.

Output: true

Explanation: The graph has one negative-weight cycle, [1 —> 2 —> 3 —> 1] with sum -2.

*/

#include <iostream>
#include <vector>

using namespace std;

#define INF INT_MAX

class Solution
{
private:
    struct Edge
    {
        int source, dest, weight;
    };

    bool bellmanFord(vector<Edge> const &edges, int source, int n)
    {
        vector<int> cost(n, INF);
        cost[source] = 0;

        int u, v, w, k = n;

        while (--k)
        {
            for (Edge edge: edges)
            {
                u = edge.source, v = edge.dest, w = edge.weight;

                if (cost[u] != INF && cost[u] + w < cost[v])
                {
                    cost[v] = cost[u] + w;
                }
            }
        }

        for (Edge edge: edges)
        {
            u = edge.source, v = edge.dest, w = edge.weight;

            if (cost[u] != INF && cost[u] + w < cost[v]) {
                return true;
            }
        }

        return false;
    }

    bool floydWarshall(vector<vector<int>> const &adjMatrix)
    {
        int n = adjMatrix.size();

        if (n == 0) {
            return false;
        }

        vector<vector<int>> cost(n, vector<int>(n));

        for (int v = 0; v < n; ++v)
        {
            for (int u = 0; u < n; ++u)
            {
                cost[v][u] = adjMatrix[v][u];
            }
        }

        for (int k = 0; k < n; ++k)
        {
            for (int v = 0; v < n; ++v)
            {
                for (int u = 0; u < n; u++)
                {
                    if (cost[v][k] != INF && cost[k][u] != INF && cost[v][k] + cost[k][u] < cost[v][u]) {
                        cost[v][u] = cost[v][k] + cost[k][u];
                    }
                }

                if (cost[v][v] < 0)
                {
                    return true;
                }
            }
        }

        return false;
    }
public:
    /*bool hasNegativeWeightCycle(vector<vector<int>> const &adjMatrix)
    {
        int n = adjMatrix.size();

        if (n == 0) {
            return false;
        }

        vector<Edge> edges;
        for (int v = 0; v < n; ++v)
        {
            for (int u = 0; u < n; ++u)
            {
                if (adjMatrix[v][u] && adjMatrix[v][u] != INF) {
                    edges.push_back({v, u, adjMatrix[v][u]});
                }
            }
        }

        for (int i = 0; i < n; ++i) {
            if (bellmanFord(edges, i, n)) {
                return true;
            }
        }

        return false;
    }*/
    bool hasNegativeWeightCycle(vector<vector<int>> const &adjMatrix)
    {
        return floydWarshall(adjMatrix);
    }
};

int main()
{
    vector<vector<int>> adjMatrix{
        {0, INF, -2, INF},
        {4, 0, -3, INF},
        {INF, INF, 0, 2},
        {INF, -1, INF, 0}
    };
    cout << boolalpha << Solution().hasNegativeWeightCycle(adjMatrix) << endl;

    return 0;
}
