/*

Given an undirected graph, determine whether it is bipartite or not. A bipartite graph (or bigraph) is a graph whose vertices can be divided into two disjoint sets U and V such that every edge connects a vertex in U to one in V.

Input: Graph [edges = [(0, 1), (1, 2), (1, 7), (2, 3), (3, 5), (4, 6), (4, 8), (7, 8)], n = 9]
Output: true
Explanation: The graph is a bipartite as it can be divided it into two sets, U (0, 2, 4, 5, 7) and V (1, 3, 6, 8), with every edge having one endpoint in set U and the other in set V.

If we add edge 1 —> 3, the graph becomes non-bipartite.

Input: Graph [edges = [(0, 1), (1, 2), (1, 3), (1, 7), (2, 3), (3, 5), (4, 6), (4, 8), (7, 8)], n = 9]
Output: false

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The graph is connected, i.e., every node can be reached starting from all other nodes.

*/

#include <iostream>
#include <vector>
#include <queue>

using namespace std;

class Graph
{
public:
    // vector of vectors to represent an adjacency list
    vector<vector<int>> adjList;

    // Total number of nodes in the graph
    int n;
};

class Solution
{
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool isBipartite(Graph const &graph)
    {
        int n = graph.n;

        vector<bool> discovered(n);
        vector<int> color(n, 0);
        //vector<int> level(n);

        int v = 0;
        discovered[v] = true, color[v] = 1;
        //discovered[v] = true, level[v] = 0;

        queue<int> q;
        q.push(v);

        while (!q.empty()) {
            v = q.front();
            q.pop();

            for (int u: graph.adjList[v]) {
                if (!discovered[u]) {
                    discovered[u] = true;

                    //level[u] = level[v] + 1;
                    if (color[u] == 0) {
                        color[u] = color[v] == 1 ? 2 : 1;
                    }

                    q.push(u);
                } else if (color[v] == color[u]) {
                //} else if (level[v] == level[u]) {
                    return false;
                }
            }
        }

        return true;
    }
};

int main()
{
    {
        /*
        Input: Graph [edges = [(0, 1), (1, 2), (1, 7), (2, 3), (3, 5), (4, 6), (4, 8), (7, 8)], n = 9]
        Output: true
         */
        Graph g{{
                {1}, // (0, 1)
                {2, 7, 0}, // (1, 2), (1, 7) + (1, 0)
                {3, 1}, // (2, 3) + (2, 1)
                {5, 2}, // (3, 5) + (3, 2)
                {6, 8}, // (4, 6), (4, 8)
                {3}, // + (5, 3)
                {4}, // + (6, 4)
                {8, 1}, // (7, 8) + (7, 1)
                {4, 7} // + (8, 4), (8, 7)
                }, 9};

        cout << boolalpha << Solution().isBipartite(g) << endl;
    }

    {
        /*
        Input: Graph [edges = [(0, 1), (1, 2), (1, 3), (1, 7), (2, 3), (3, 5), (4, 6), (4, 8), (7, 8)], n = 9]
        Output: false
         */
        Graph g{{
                {1}, // (0, 1)
                {2, 3, 7, 0}, // (1, 2), (1, 3), (1, 7) + (1, 0)
                {3, 1}, // (2, 3) + (2, 1)
                {5, 1, 2}, // (3, 5) + (3, 1), (3, 2)
                {6, 8}, // (4, 6), (4, 8)
                {3}, // + (5, 3)
                {4}, // + (6, 4)
                {8, 1}, // (7, 8) + (7, 1)
                {4, 7} // + (8, 4), (8, 7)
                }, 9};

        cout << boolalpha << Solution().isBipartite(g) << endl;
    }

    return 0;
}
