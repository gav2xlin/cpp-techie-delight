/*

Given a list of edges of a directed graph, check whether it has an Eulerian path or not. An Eulerian path is a path in a graph that visits every edge exactly once.

Input: edges = [(0, 1), (1, 2), (2, 3), (3, 1), (1, 4), (4, 3), (3, 0), (0, 5), (5, 4)]
Output: true
Explanation: The graph has an Eulerian path [0 —> 1 —> 2 —> 3 —> 0 —> 5 —> 4 —> 3 —> 1 —> 4] which visits each edge exactly once.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Edge {
public:
    int src, dest;
};

class Solution
{
private:
    class Graph
    {
    public:
        vector<vector<int>> adjList;
        vector<int> in; // in-degree
        int n{};

        void addEdge(int u, int v)
        {
            n = max(n, max(u + 1, v + 1));

            if (u >= adjList.size()) {
                adjList.resize(u + 1);
            }
            if (v >= in.size()) {
                in.resize(v + 1);
            }

            adjList[u].push_back(v);
            in[v]++;
        }
    };

    void DFS(Graph const &graph, int u, vector<bool> &visited)
    {
        visited[u] = true;

        for (int v: graph.adjList[u])
        {
            if (!visited[v]) {
                DFS(graph, v, visited);
            }
        }
    }

    Graph getUndirectedGraph(Graph const &graph, int n)
    {
        Graph g;

        for (int u = 0; u < graph.n; ++u)
        {
            for (int v: graph.adjList[u])
            {
                g.addEdge(v, u);
                g.addEdge(u, v);
            }
        }
        return g;
    }

    bool isConnected(Graph const &graph, int n)
    {
        vector<bool> visited(n);

        for (int i = 0; i < n; ++i)
        {
            if (graph.adjList[i].size())
            {
                DFS(graph, i, visited);
                break;
            }
        }

        // if a single DFS call couldn't visit all vertices with a non-zero degree,
        // the graph contains more than one connected component
        for (int i = 0; i < n; ++i)
        {
            if (!visited[i] && graph.adjList[i].size() > 0) {
                return false;
            }
        }

        return true;
    }

public:

    /*
        // Definition for an Edge
        class Edge {
        public:
            int src, dest;
        };
    */

    bool hasEulerianPath(vector<Edge> const &edges)
    {
        /*
            The following loop checks the following conditions to determine if an
            Eulerian path can exist or not:
                a. At most one vertex in the graph has `out-degree = 1 + in-degree`.
                b. At most one vertex in the graph has `in-degree = 1 + out-degree`.
                c. Rest all vertices have `in-degree == out-degree`.

            If either of the above condition fails, the Euler path can't exist.
        */
        Graph graph;
        for (auto &edge: edges) {
            graph.addEdge(edge.src, edge.dest);
        }

        bool x = false, y = false;
        int n = graph.n;

        for (int i = 0; i < n; ++i)
        {
            int out_degree = graph.adjList[i].size();
            int in_degree = graph.in[i];

            if (out_degree != in_degree)
            {
                if (!x && out_degree - in_degree == 1)
                {
                    x = true;
                }
                else if (!y && in_degree - out_degree == 1)
                {
                    y = true;
                }
                else
                {
                    return false;
                }
            }
        }

        return isConnected(getUndirectedGraph(graph, n), n);
    }
};

int main()
{
    //vector<Edge> edges = {{0, 1}, {1, 2}, {2, 3}, {3, 1}, {1, 4}, {4, 3}, {3, 0}, {0, 5}, {5, 4}};
    vector<Edge> edges = {{0, 1}};

    cout << boolalpha << Solution().hasEulerianPath(edges) << endl;

    return 0;
}
