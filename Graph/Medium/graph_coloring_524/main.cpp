/*

Given an undirected graph, return the minimum number of colors required to color the graph's vertices such that no two adjacent vertices share the same color.

Input: Graph [edges = [(0, 1), (0, 4), (0, 5), (4, 5), (1, 4), (1, 3), (2, 3), (2, 4)], n = 6]
Output: 3
Explanation: The graph can be colored in many ways by using the minimum of 3 colors. For example, assign color 1 to vertices [0, 2], color 2 to vertices [3, 4], and color 3 to vertices [1, 5].

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.
• The graph is connected, i.e., every node can be reached starting from all other nodes.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Graph
{
public:
    vector<vector<int>> adjList;
    int n;

    Graph(vector<pair<int, int>> const &edges, int n)
    {
        adjList.resize(n);
        this->n = n;

        for (pair<int, int> const &edge: edges)
        {
            int src = edge.first;
            int dest = edge.second;

            adjList[src].push_back(dest);
            adjList[dest].push_back(src);
        }
    }
};

class Solution
{
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    int findMinimumColors(Graph const &graph)
    {
        int n = graph.n;

        vector<int> result(n, -1);
        result[0] = 0;

        vector<bool> occupied(n);

        for (int u = 1; u < n; ++u)
        {
            for (auto i : graph.adjList[u]) {
                if (result[i] != -1) {
                    occupied[result[i]] = true;
                }
            }

            int cr;
            for (cr = 0; cr < n; ++cr) {
                if (!occupied[cr]) {
                    break;
                }
            }

            result[u] = cr;

            for (auto i : graph.adjList[u]) {
                if (result[i] != -1) {
                    occupied[result[i]] = false;
                }
            }
        }

        int minColor = 0;
        for (int u = 0; u < n; ++u) {
            //cout << "Vertex " << u << " --->  Color " << result[u] << endl;
            minColor = max(minColor, result[u] + 1);
        }

        return minColor;
    }
};

int main()
{
    vector<pair<int, int>> edges = {{0, 1}, {0, 4}, {0, 5}, {4, 5}, {1, 4}, {1, 3}, {2, 3}, {2, 4}};
    Graph graph(edges, 6);

    cout << Solution().findMinimumColors(graph) << endl;

    return 0;
}
