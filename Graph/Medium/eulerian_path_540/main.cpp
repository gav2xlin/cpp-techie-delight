/*

Given an undirected graph, determine whether it has an Eulerian path or not. In other words, check if it is possible to construct a path that visits each edge exactly once.

Input: Graph [edges = [(0, 1), (0, 3), (1, 2), (1, 3), (1, 4), (2, 3), (3, 4)], n = 5]
Output: true

Constraints:

• The graph is implemented using an adjacency list.
• The maximum number of nodes in the graph is 100, i.e., 0 <= n < 100, and each node is represented by its numeric value.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

struct Edge {
    int src, dest;
};

class Graph
{
public:
    vector<vector<int>> adjList;
    int n;

    Graph(vector<Edge> const &edges, int n)
    {
        adjList.resize(n);
        this->n = n;

        for (auto &edge: edges)
        {
            adjList[edge.src].push_back(edge.dest);
            adjList[edge.dest].push_back(edge.src);
        }
    }
};

class Solution
{
private:
    void DFS(Graph const &graph, int v, vector<bool> &visited)
    {
        visited[v] = true;

        for (int u: graph.adjList[v])
        {
            if (!visited[u]) {
                DFS(graph, u, visited);
            }
        }
    }

    bool isConnected(Graph const &graph, int n)
    {
        vector<bool> visited(n);

        for (int i = 0; i < n; ++i)
        {
            if (graph.adjList[i].size())
            {
                DFS(graph, i, visited);
                break;
            }
        }

        // if a single DFS call couldn't visit all vertices with a non-zero degree,
        // the graph contains more than one connected component
        for (int i = 0; i < n; ++i)
        {
            if (!visited[i] && graph.adjList[i].size() > 0) {
                return false;
            }
        }

        return true;
    }

    int countOddVertices(Graph const &graph)
    {
        int count = 0;
        for (vector<int> list: graph.adjList)
        {
            if (list.size() & 1) {
                ++count;
            }
        }
        return count;
    }
public:

    /*
        // Definition for a Graph
        class Graph
        {
        public:
            // vector of vectors to represent an adjacency list
            vector<vector<int>> adjList;

            // Total number of nodes in the graph
            int n;
        }
    */

    bool hasEulerianPath(Graph const &graph)
    {
        bool is_connected = isConnected(graph, graph.n);
        int odd = countOddVertices(graph);

        if (is_connected && (odd == 0 || odd == 2))
        {
            //cout << "The graph has an Eulerian path" << endl;

            // A connected graph has an Eulerian cycle if every vertex has an even degree
            /*if (odd == 0)
            {
                cout << "The graph has an Eulerian cycle" << endl;
            }*/
            // The graph has an Eulerian path but not an Eulerian cycle
            /*else
            {
                cout << "The Graph is Semi–Eulerian" << endl;
            }*/
            return true;
        }
        else {
            //cout << "The Graph is not Eulerian" << endl;
            return false;
        }
    }
};

int main()
{
    vector<Edge> edges = {{0, 1}, {0, 3}, {1, 2}, {1, 3}, {1, 4}, {2, 3}, {3, 4}};
    Graph graph(edges, 5);

    cout << boolalpha << Solution().hasEulerianPath(graph) << endl;

    return 0;
}
