/*

Given an integer array and an integer k, shrink it by removing adjacent triplets that satisfy the given constraints and return the total number of elements in the resultant array.

A triplet (x, y, z) can only be removed if, for the number k, the second element y of the triplet is precise k more than the first element x. The third element, z, is precise k more than the second element y. The total number of elements in the final array should be as few as possible.

Input: nums = [1, 2, 3, 5, 7, 8], k = 2
Output: 3
Explanation: The adjacent triplet (3, 5, 7) can be removed from the array. The resultant array is [1, 7, 8] cannot be reduced further.

Input: nums = [-1, 0, 1, 2, 3, 4], k = 1
Output: 0
Explanation: The result is 0 since all elements can be removed from the array. First, the adjacent triplet (2, 3, 4) is removed. The array is now reduced to [-1, 0, 1], which forms another valid triplet and can be removed from the array.

Note that if the adjacent triplet (1, 2, 3) is removed from the array first, the resultant array [-1, 0, 4] cannot be reduced further.

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int shrink(vector<int> const &arr, int start, int end, int k)
    {
        if (start > end) {
            return 0;
        }

        int result = 0;

        result = 1 + shrink(arr, start + 1, end, k);

        for (int i = start + 1; i < end; ++i)
        {
            for (int j = i + 1; j <= end; ++j)
            {
                if (arr[i] == arr[start] + k && arr[j] == arr[i] + k)
                {
                    if (!shrink(arr, start + 1, i - 1, k) && !shrink(arr, i + 1, j - 1, k))
                    {
                        int n = shrink(arr, j + 1, end, k);
                        if (result > n) {
                            result = n;
                        }
                    }
                }
            }
        }

        return result;
    }
public:
    int shrink(vector<int> const &nums, int k)
    {
        return shrink(nums, 0, nums.size() - 1, k);
    }
};

int main()
{
    cout << Solution().shrink({1, 2, 3, 5, 7, 8}, 2) << endl;
    cout << Solution().shrink({-1, 0, 1, 2, 3, 4}, 1) << endl;

    return 0;
}
