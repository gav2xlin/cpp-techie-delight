/*

Given a set of positive integers S, partition it into two subsets, S1 and S2, such that the difference between the sum of elements in S1 and S2 is mininum. The solution should return the minimum absolute difference between the sum of elements of two partitions.

Input: S = [10, 20, 15, 5, 25]
Output: 5
Explanation: S can be partitioned into two partitions [[10, 20, 5], [15, 25]] where the minimum absolute difference between the sum of elements is 5. Note that this solution is not unique. Another solution is [[10, 25], [20, 15, 5]].

Input: []
Output: 0

*/

#include <iostream>
#include <vector>
#include <cmath>
#include <string>
#include <unordered_map>
#include <numeric>
#include <algorithm>

using namespace std;

class Solution
{
private:
    /*int findMinAbsDiff(vector<int> const &S, int n, int S1, int S2)
    {
        if (n < 0) {
            return abs(S1 - S2);
        }

        int inc = findMinAbsDiff(S, n - 1, S1 + S[n], S2);

        int exc = findMinAbsDiff(S, n - 1, S1, S2 + S[n]);

        return min(inc, exc);
    }*/
    /*int findMinAbsDiff(vector<int> const &S, int n, int S1, int S2, auto &lookup)
    {
        if (n < 0) {
            return abs(S1 - S2);
        }

        string key = to_string(n) + "|" + to_string(S1);

        if (lookup.find(key) == lookup.end())
        {

            int inc = findMinAbsDiff(S, n - 1, S1 + S[n], S2, lookup);
            int exc = findMinAbsDiff(S, n - 1, S1, S2 + S[n], lookup);

            lookup[key] = min(inc, exc);
        }

        return lookup[key];
    }*/
public:
    int findMinAbsDiff(vector<int> const &S)
    {
        /*int n = S.size();
        return findMinAbsDiff(S, n - 1, 0, 0);*/
        /*int n = S.size();
        unordered_map<string, int> lookup;
        return findMinAbsDiff(S, n - 1, 0, 0, lookup);*/
        int sum = accumulate(S.begin(), S.end(), 0);

        bool T[S.size() + 1][sum + 1];
        fill(*T, *T + (S.size() + 1)*(sum + 1), false);

        for (int i = 0; i <= S.size(); ++i)
        {
            T[i][0] = true;

            for (int j = 1; i > 0 && j <= sum; ++j)
            {
                T[i][j] = T[i - 1][j];

                if (S[i - 1] <= j) {
                    T[i][j] |= T[i - 1][j - S[i - 1]];
                }
            }
        }

        int j = sum / 2;
        while (j >= 0 && !T[S.size()][j]) {
            --j;
        }
        return sum - 2 * j;
    }
};

int main()
{
    cout << Solution().findMinAbsDiff({10, 20, 15, 5, 25}) << endl;

    return 0;
}
