/*

Given an unlimited supply of coins of given denominations, find the total number of distinct ways to get the desired change.

Input: S = [1, 3, 5, 7], target = 8
Output: 6
Explanation: The total number of ways is 6

[1, 7]
[3, 5]
[1, 1, 3, 3]
[1, 1, 1, 5]
[1, 1, 1, 1, 1, 3]
[1, 1, 1, 1, 1, 1, 1, 1]


Input: S = [1, 2, 3], target = 4
Output: 4
Explanation: The total number of ways is 4

[1, 3]
[2, 2]
[1, 1, 2]
[1, 1, 1, 1]

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>

using namespace std;

class Solution
{
private:
    /*int count(vector<int> const &S, int n, int target)
    {
        if (target == 0) {
            return 1;
        }

        if (target < 0 || n < 0) {
            return 0;
        }

        int include = count(S, n, target - S[n]);

        int exclude = count(S, n - 1, target);

        return include + exclude;
    }*/
    /*int count(vector<int> const &S, int n, int target, auto &lookup)
    {
        if (target == 0) {
            return 1;
        }

        if (target < 0 || n < 0) {
            return 0;
        }

        string key = to_string(n) + "|" + to_string(target);

        if (lookup.find(key) == lookup.end())
        {
            int include = count(S, n, target - S[n], lookup);

            int exclude = count(S, n - 1, target, lookup);

            lookup[key] = include + exclude;
        }

        return lookup[key];
    }*/
    /*int count(vector<int> const &S, int n, int target)
    {
        int T[n + 1][target + 1];

        for (int i = 0; i <= n; ++i)
        {
            for (int j = 0; j <= target; ++j)
            {
                if (i == 0)
                {
                    T[0][j] = 0;
                }
                else if (j == 0)
                {
                    T[i][0] = 1;
                }
                else if (S[i - 1] > j)
                {
                    T[i][j] = T[i - 1][j];
                }
                else
                {
                    T[i][j] = T[i - 1][j] + T[i][j - S[i - 1]];
                }
            }
        }

        return T[n][target];
    }*/
    int count(vector<int> const &S, int n, int target)
    {
        int T[target+1];

        for (int i = 0; i <= target; i++) {
            T[i] = 0;
        }

        T[0] = 1;

        for (int i = 0; i < n; i++)
        {
            for (int j = S[i]; j <= target; j++) {
                T[j] += T[j - S[i]];
            }
        }

        return T[target];
    }
public:
    int findWays(vector<int> const &S, int target)
    {
        //return count(S, S.size() - 1, target);

        /*unordered_map<string, int> lookup;
        return count(S, S.size() - 1, target, lookup);*/

        return count(S, S.size(), target);
    }
};

int main()
{
    vector<int> S { 1, 2, 3 };

    cout << Solution().findWays(S, 4) << endl;

    return 0;
}
