/*

Given r red, b blue, and g green balls, find the total number of arrangements in a row such that no two balls of the same color end up together.

Input: r = 1, b = 2, g = 1
Output: 6
Explanation: The arrangements are [bgbr, bgrb, brbg, brgb, gbrb, rbgb]

Input: r = 2, b = 3, g = 1
Output: 10
Explanation: The arrangements are [bgbrbr, bgrbrb, brbgbr, brbgrb, brbrbg, brbrgb, brgbrb, gbrbrb, rbgbrb, rbrbgb]

*/

#include <iostream>

using namespace std;

class Solution
{
private:
    int f(int r, int b, int g, char next)
    {
        if (r < 0 || b < 0 || g < 0) {
            return 0;
        }

        if (next == 'r')
        {
            if (r == 1 && b == 0 && g == 0) {
                return 1;
            }

            return f(r - 1, b, g, 'b') + f(r - 1, b, g, 'g');
        }

        if (next == 'b')
        {
            if (r == 0 && b == 1 && g == 0) {
                return 1;
            }

            return f(r, b - 1, g, 'r') + f(r, b - 1, g, 'g');
        }

        if (next == 'g')
        {
            if (r == 0 && b == 0 && g == 1) {
                return 1;
            }

            return f(r, b, g - 1, 'r') + f(r, b, g - 1, 'b');
        }

        return 0;
    }
    /*int f(int r, int b, int g, char prev)
    {
        if (r < 0 || b < 0 || g < 0) {
            return 0;
        }

        if (r == 0 && b == 0 && g == 0) {
            return 1;
        }

        if (prev == 'r')
        {
            return f(r, b - 1, g, 'b') + f(r, b, g - 1, 'g');
        }

        if (prev == 'b')
        {
            return f(r - 1, b, g, 'r') + f(r, b, g - 1, 'g');
        }

        if (prev == 'g')
        {
            return f(r - 1, b, g, 'r') + f(r, b - 1, g, 'b');
        }
    }*/
public:
    int totalWays(int r, int b, int g)
    {
        return f(r, b, g, 'r') + f(r, b, g, 'b') + f(r, b, g, 'g');
        //return f(r - 1, b, g, 'r') + f(r, b - 1, g, 'b') + f(r, b, g - 1, 'g');
    }
};

int main()
{
    int r = 2, b = 3, g = 1;

    cout << Solution().totalWays(r, b, g) << endl;

    return 0;
}
