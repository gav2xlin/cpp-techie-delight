/*

A triangulation of a convex polygon results in a set of non-intersecting diagonals between non-adjacent vertices, which completely partition the interior of the convex hull of the polygon into triangles. The minimum-weight triangulation (MWT) is the triangulation having the minimum total edge length among all possible triangulation.

Input: [(0, 0), (2, 0), (2, 1), (1, 2), (0, 1)]
Output: 15.30056307974577

Explanation: The following are the two triangulations of the same convex pentagon. The triangulation on the left has a cost of 8 + 2v2 + 2v5 (approximately 15.30), the one on the right has a cost of 4 + 2v2 + 4v5 (approximately 15.77).

https://techiedelight.com/practice/images/Minimum-Weight-Triangulation.png

*/

#include <iostream>
#include <algorithm>
#include <vector>
#include <limits>
#include <cstring>
#include <cmath>

using namespace std;

class Solution
{
private:
    double dist(pair<int,int> const &p1, pair<int,int> const &p2)
    {
        // The distance between vertices `(x1, y1)` and `(x2, y2)` is
        // `v((x2 - x1) ^ 2 + (y2 - y1) ^ 2)`
        return sqrt((p1.first - p2.first) * (p1.first - p2.first) +
                    (p1.second - p2.second) * (p1.second - p2.second));
    }
public:
    double findMWT(vector<pair<int,int>> vertices)
    {
        int n = vertices.size();

        vector<vector<double>> T(n, vector<double>(n));

        for (int diagonal = 0; diagonal < n; ++diagonal)
        {
            for (int i = 0, j = diagonal; j < n; ++i, ++j)
            {
                if (j < i + 2) {
                    continue;
                }

                T[i][j] = numeric_limits<double>::max();

                for (int k = i + 1; k <= j - 1; ++k)
                {
                    double weight = dist(vertices[i], vertices[j]) +
                            dist(vertices[j], vertices[k]) +
                            dist(vertices[k], vertices[i]);

                    T[i][j] = min(T[i][j], weight + T[i][k] + T[k][j]);
                }
            }
        }

        return T[0][n - 1];
    }
};

int main()
{
    vector<pair<int, int>> vertices = { {0, 0}, {2, 0}, {2, 1}, {1, 2}, {0, 1} };

    cout << Solution().findMWT(vertices) << endl;

    return 0;
}
