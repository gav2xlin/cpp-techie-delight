/*

Given a linear equation of `k` variables, return the total number of possible solutions to it.

Input: coeff = [1, 3, 5, 7], rhs = 8
Output: 6
Explanation: The input represents the equation `a + 3b + 5c + 7d = 8` and the total number of solutions is 6, as shown below

(a = 1, b = 0, c = 0, d = 1)
(a = 0, b = 1, c = 1, d = 0)
(a = 2, b = 2, c = 0, d = 0)
(a = 3, b = 0, c = 1, d = 0)
(a = 5, b = 1, c = 0, d = 0)
(a = 8, b = 0, c = 0, d = 0)


Input: coeff = [1, 2, 3], rhs = 4
Output: 4
Explanation: The input represents the equation `x + 2y + 3z = 4` and the total number of solutions is 4, as shown below

(x = 1, y = 0, z = 1)
(x = 0, y = 2, z = 0)
(x = 2, y = 1, z = 0)
(x = 4, y = 0, z = 0)

*/

#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    /*int count(vector<int> const &coeff, int k, int rhs)
    {
        if (rhs == 0) {
            return 1;
        }

        if (rhs < 0 || k < 0) {
            return 0;
        }

        int include = count(coeff, k, rhs - coeff[k]);

        int exclude = count(coeff, k - 1, rhs);

        return include + exclude;
    }*/

    /*int count(vector<int> const &coeff, int k, int rhs, auto &lookup)
    {
        if (rhs == 0) {
            return 1;
        }

        if (rhs < 0 || k < 0) {
            return 0;
        }

        string key = to_string(k) + "|" + to_string(rhs);

        if (lookup.find(key) == lookup.end())
        {
            int include = count(coeff, k, rhs - coeff[k], lookup);

            int exclude = count(coeff, k - 1, rhs, lookup);

            lookup[key] = include + exclude;
        }

        return lookup[key];
    }*/

    /*int count(vector<int> const &coeff, int k, int rhs)
    {
        int T[k + 1][rhs + 1];

        for (int i = 0; i <= k; ++i)
        {
            for (int j = 0; j <= rhs; ++j)
            {
                if (i == 0)
                {
                    T[i][j] = 0;
                }
                else if (j == 0)
                {
                    T[i][j] = 1;
                }
                else if (coeff[i - 1] > j)
                {
                    T[i][j] = T[i - 1][j];
                }
                else
                {
                    T[i][j] = T[i - 1][j] + T[i][j - coeff[i - 1]];
                }
            }
        }

        return T[k][rhs];
    }*/

    int count(vector<int> const &coeff, int k, int rhs)
    {
        int T[rhs + 1];

        for (int i = 0; i <= rhs; ++i) {
            T[i] = 0;
        }

        T[0] = 1;

        for (int i = 0; i < k; ++i)
        {
            for (int j = coeff[i]; j <= rhs; ++j) {
                T[j] += T[j - coeff[i]];
            }
        }

        return T[rhs];
    }
public:
    int count(vector<int> const &coeff, int rhs)
    {
        //return count(coeff, coeff.size() - 1, rhs);

        /*unordered_map<string, int> lookup;
        return count(coeff, coeff.size() - 1, rhs, lookup);*/

        return count(coeff, coeff.size(), rhs);
    }
};

int main()
{
    vector<int> coeff { 1, 2, 3 };

    cout << Solution().count(coeff, 4) << endl;

    return 0;
}
