/*

Given two sequences, find the length of the shortest supersequence 'Z' of given sequences 'X' and 'Y' such that both 'X' and 'Y' are subsequences of 'Z'.

The shortest common supersequence is not guaranteed to be unique. If multiple shortest common supersequence exists, the solution should return any one of them.

Input: X = "ABCBDAB", Y = "BDCABA"
Output: "ABCBDCABA" or "ABDCABDAB" or "ABDCBDABA"

*/

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <iterator>

using namespace std;

class Solution
{
private:
    string SCS(string X, string Y, int m, int n, auto &lookup)
    {
        if (m == 0)
        {
            return Y.substr(0, n);
        }
        else if (n == 0)
        {
            return X.substr(0, m);
        }

        if (X[m - 1] == Y[n - 1])
        {
            return SCS(X, Y, m - 1, n - 1, lookup) + X[m - 1];
        }
        else
        {
            if (lookup[m - 1][n] < lookup[m][n - 1])
            {
                return SCS(X, Y, m - 1, n, lookup) + X[m-1];
            }
            else
            {
                return SCS(X, Y, m, n - 1, lookup) + Y[n-1];
            }
        }
    }

    void SCSLength(string X, string Y, int m, int n, auto &lookup)
    {
        for (int i = 0; i <= m; ++i) {
            lookup[i][0] = i;
        }

        for (int j = 0; j <= n; ++j) {
            lookup[0][j] = j;
        }

        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1])
                {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;
                }
                else
                {
                    lookup[i][j] = min(lookup[i - 1][j] + 1,
                    lookup[i][j - 1] + 1);
                }
            }
        }
    }

    //
    /*vector<string> SCS(string X, string Y, int m, int n, auto &lookup)
    {
        if (m == 0)
        {
            return {Y.substr(0, n)};
        }
        else if (n == 0)
        {
            return {X.substr(0, m)};
        }

        if (X[m - 1] == Y[n - 1])
        {
            vector<string> scs = SCS(X, Y, m - 1, n - 1, lookup);

            for (string &str: scs) {
                str.push_back(X[m - 1]);
            }

            return scs;
        }

        if (lookup[m - 1][n] < lookup[m][n - 1])
        {
            vector<string> scs = SCS(X, Y, m - 1, n, lookup);

            for (string &str: scs) {
                str.push_back(X[m - 1]);
            }

            return scs;
        }

        if (lookup[m][n - 1] < lookup[m - 1][n])
        {
            vector<string> scs = SCS(X, Y, m, n - 1, lookup);

            for (string &str: scs) {
                str.push_back(Y[n - 1]);
            }

            return scs;
        }

        vector<string> top = SCS(X, Y, m - 1, n, lookup);
        for (string &str: top) {
            str.push_back(X[m - 1]);
        }

        vector<string> left = SCS(X, Y, m, n - 1, lookup);
        for (string &str: left) {
            str.push_back(Y[n - 1]);
        }

        top.insert(top.end(), left.begin(), left.end());
        // copy(left.begin(), left.end(), back_inserter(top));

        return top;
    }

    void SCSLength(string X, string Y, int m, int n, auto &lookup)
    {
        for (int i = 0; i <= m; ++i) {
            lookup[i][0] = i;
        }

        for (int j = 0; j <= n; ++j) {
            lookup[0][j] = j;
        }

        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1])
                {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;
                }
                else
                {
                    lookup[i][j] = min(lookup[i - 1][j] + 1, lookup[i][j - 1] + 1);
                }
            }
        }
    }*/
public:
    string findSCS(string X, string Y)
    {
        int m = X.length(), n = Y.length();

        vector<vector<int>> lookup(m + 1, vector<int>(n + 1));

        SCSLength(X, Y, m, n, lookup);

        return SCS(X, Y, m, n, lookup);
    }
    /*set<string> SCS(string X, string Y)
    {
        int m = X.length(), n = Y.length();

        vector<vector<int>> lookup(m + 1, vector<int>(n + 1));

        SCSLength(X, Y, m, n, lookup);

        vector<string> v = SCS(X, Y, m, n, lookup);

        set<string> scs(v.begin(), v.end());*/

        // to "convert" a vector to use a set
        /* set<string> scs(make_move_iterator(v.begin()),
                        make_move_iterator(v.end())); */

        /*return scs;
    }*/
};

int main()
{
    string X = "ABCBDAB", Y = "BDCABA";

    cout << Solution().findSCS(X, Y) << endl;

    return 0;
}
