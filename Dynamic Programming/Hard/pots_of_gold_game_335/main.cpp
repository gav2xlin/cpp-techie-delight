/*

In pots of gold game, there are two players, A and B, and pots of gold arranged in a line, each containing some gold coins. The players can see how many coins are there in each gold pot, and each player gets alternating turns in which the player can pick a pot from either end of the line. The winner is the player who has a higher number of coins at the end. The objective is to "maximize" the number of coins collected by A, assuming B also plays "optimally", and A starts the game.

Input: coins[] = [4, 6, 2, 3]
Output: 9
Explanation: The optimal way is shown below:

                    Player A  |  Player B
                              |
4, 6, 2, 3              3     |
4, 6, 2                       |     4
6, 2                    6     |
2                             |     2
                     9 coins  |  6 coins


Input: coins[] = [6, 1, 4, 9, 8, 5]
Output: 18
Explanation: The optimal way is shown below:

                     Player A  |  Player B
                               |
6, 1, 4, 9, 8, 5        6      |
1, 4, 9, 8, 5                  |     5
1, 4, 9, 8              8      |
1, 4, 9                        |     9
1, 4                    4      |
1                              |     1
                     18 coins  |  15 coins

*/

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
private:
    /*int findMaxCoins(vector<int> const &coins, int i, int j)
    {
        if (i == j) {
            return coins[i];
        }

        if (i + 1 == j) {
            return max(coins[i], coins[j]);
        }

        int start = coins[i] + min(findMaxCoins(coins, i + 2, j), findMaxCoins(coins, i + 1, j - 1));

        int end = coins[j] + min(findMaxCoins(coins, i + 1, j - 1), findMaxCoins(coins, i, j - 2));

        return max(start, end);
    }*/
    /*int findMaxCoins(vector<int> const &coins, int i, int j, auto &lookup)
    {
        if (i == j) {
            return coins[i];
        }

        if (i + 1 == j) {
            return max(coins[i], coins[j]);
        }

        if (lookup[i][j] == 0)
        {
            int start = coins[i] + min(findMaxCoins(coins, i + 2, j, lookup), findMaxCoins(coins, i + 1, j - 1, lookup));

            int end = coins[j] + min(findMaxCoins(coins, i + 1, j - 1, lookup), findMaxCoins(coins, i, j - 2, lookup));

            lookup[i][j] = max(start, end);
        }

        return lookup[i][j];
    }*/
    int calculate(int **T, int i, int j)
    {
        if (i <= j) {
            return T[i][j];
        }

        return 0;
    }

    int findMaxCoins(vector<int> const &coins, int n)
    {
        if (n == 1) {
            return coins[0];
        }

        if (n == 2) {
            return max(coins[0], coins[1]);
        }

        int** T = new int*[n];
        for (int i = 0; i < n; i++) {
            T[i] = new int[n];
        }

        for (int iteration = 0; iteration < n; ++iteration)
        {
            for (int i = 0, j = iteration; j < n; ++i, ++j)
            {
                int start = coins[i] + min(calculate(T, i + 2, j), calculate(T, i + 1, j - 1));

                int end = coins[j] + min(calculate(T, i + 1, j - 1), calculate(T, i, j - 2));

                T[i][j] = max(start, end);
            }
        }

        int result = T[0][n - 1];

        for (int i = 0; i < n; ++i) {
            delete[] T[i];
        }
        delete[] T;

        return result;
    }
public:
    int findMaximumCoins(vector<int> const &coins)
    {
        //return findMaxCoins(coins, 0, coins.size() - 1);

        /*int n = coins.size();
        vector<vector<int>> lookup(n + 1, vector<int>(n + 1));

        return findMaxCoins(coins, 0, n - 1, lookup);*/

        return findMaxCoins(coins, coins.size());
    }
};

int main()
{
    vector<int> coins = { 4, 6, 2, 3 };

    cout << Solution().findMaximumCoins(coins) << endl;

    return 0;
}
