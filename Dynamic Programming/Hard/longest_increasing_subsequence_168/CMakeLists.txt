cmake_minimum_required(VERSION 3.5)

project(longest_increasing_subsequence_168 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(longest_increasing_subsequence_168 main.cpp)

install(TARGETS longest_increasing_subsequence_168
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
