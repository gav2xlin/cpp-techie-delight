/*

Given a given sequence, find the longest increasing subsequence (LIS) in it.

The longest increasing subsequence is a subsequence of a given sequence in which the subsequence's elements are in sorted order, lowest to highest, and in which the subsequence is as long as possible.

The longest increasing subsequence is not necessarily unique, the solution can return any valid subsequence.

Input : [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]
Output: [0, 2, 6, 9, 11, 15] or [0, 4, 6, 9, 11, 15] or [0, 4, 6, 9, 13, 15]

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Solution
{
private:
    /*int LIS(vector<int> const & arr, int i, int n, int prev)
    {
        // Base case: nothing is remaining
        if (i == n) {
            return 0;
        }

        // case 1: exclude the current element and process the
        // remaining elements
        int excl = LIS(arr, i + 1, n, prev);

        // case 2: include the current element if it is greater
        // than the previous element in LIS
        int incl = 0;
        if (arr[i] > prev) {
            incl = 1 + LIS(arr, i + 1, n, arr[i]);
        }

        // return the maximum of the above two choices
        return max(incl, excl);
    }

    return LIS(arr, 0, n, INT_MIN);*/
    /*int LIS(vector<int> const &arr)
    {
        int n = arr.size();
        if (n == 0) {
            return 0;
        }

        int L[n] = { 0 };
        L[0] = 1;

        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                if (arr[j] < arr[i] && L[j] > L[i]) {
                    L[i] = L[j];
                }
            }

            L[i]++;
        }

        int lis = INT_MIN;
        for (int x: L) {
            lis = max(lis, x);
        }

        return lis;
    }*/

public:
    vector<int> findLIS(vector<int> const &arr)
    {
        int n = arr.size();
        if (n == 0) {
            return {};
        }

        vector<vector<int>> LIS(n, vector<int>{});
        LIS[0].push_back(arr[0]);

        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                if (arr[j] < arr[i] && LIS[j].size() > LIS[i].size()) {
                    LIS[i] = LIS[j];
                }
            }

            LIS[i].push_back(arr[i]);
        }

        // uncomment the following code to print contents of `LIS`
        /* for (int i = 0; i < n; i++)
        {
            cout << "LIS[" << i << "] — ";
            for (int j: LIS[i]) {
                cout << j << " ";
            }
            cout << endl;
        } */

        int j = 0;
        for (int i = 0; i < n; ++i)
        {
            if (LIS[j].size() < LIS[i].size()) {
                j = i;
            }
        }

        vector<int> res;
        for (int i: LIS[j]) {
            res.push_back(i);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> arr = { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };

    cout << Solution().findLIS(arr) << endl;

    return 0;
}
