/*

Given a given sequence, find the length of the longest decreasing subsequence (LDS) in it.

The longest decreasing subsequence is a subsequence of a given sequence in which the subsequence's elements are in sorted order, highest to lowest, and in which the subsequence is as long as possible.

Input : [0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15]
Output: 6
Explanation: The longest decreasing subsequence is [12, 10, 9, 5, 3] having length 5; the input sequence has no 6–member decreasing subsequences.

The longest decreasing subsequence is not necessarily unique. For instance, [12, 10, 6, 5, 3] is another decreasing subsequences of equal length in the same input sequence.

*/

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

class Solution
{
private:
    int LDS(vector<int> const &nums)
    {
        int n = nums.size();
        if (n == 0) {
            return 0;
        }

        vector<int> L(n);
        L[0] = 1;

        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                if (nums[j] > nums[i] && L[j] > L[i]) {
                    L[i] = L[j];
                }
            }

            L[i]++;
        }

        int lis = INT_MIN;
        for (int x: L) {
            lis = max(lis, x);
        }

        return lis;
    }
public:
    /*int findLDSLength(vector<int> const &nums)
    {
        return LDS(nums);
    }*/

    int findLDSLength(vector<int> const &nums)
    {
        int n = nums.size();
        if (n == 0) {
            return 0;
        }

        vector<vector<int>> LDS(n, vector<int>());
        LDS[0].push_back(nums[0]);

        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                if (nums[j] > nums[i] && LDS[j].size() > LDS[i].size()) {
                    LDS[i] = LDS[j];
                }
            }

            LDS[i].push_back(nums[i]);
        }

        // uncomment the following code to print contents of `LDS`
        /* for (int i = 0; i < n; i++)
        {
            cout << "LDS[" << i << "] — ";
            for (int j: LDS[i]) {
                cout << j << " ";
            }
            cout << endl;
        } */

        int j = 0;
        for (int i = 0; i < n; i++)
        {
            if (LDS[j].size() < LDS[i].size()) {
                j = i;
            }
        }

        /*for (int i: LDS[j]) {
            cout << i << " ";
        }*/
        return LDS[j].size();
    }
};

int main()
{
    vector<int> nums = { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };

    cout << Solution().findLDSLength(nums) << endl;

    return 0;
}
