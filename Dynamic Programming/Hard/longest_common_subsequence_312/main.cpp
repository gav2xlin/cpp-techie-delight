/*

Given two sequences, return the longest common subsequence (LCS) present in it. The LCS is the longest sequence which can be obtained from the first sequence by deleting some items and from the second sequence by deleting other items.

Input: X = "XMJYAUZ", Y = "MZJAWXU"
Output: "MJAU"

The longest common subsequence is not guaranteed to be unique. If multiple longest common subsequence exists, the solution should return any one of them.

Input: X = "ABCBDAB", Y = "BDCABA"
Output: "BDAB" or "BCAB" or "BCBA"

*/

#include <iostream>
#include <string>
#include <vector>
#include <set>

using namespace std;

class Solution
{
private:
    string LCS(string X, string Y, int m, int n, auto &lookup)
    {
        if (m == 0 || n == 0) {
            return string("");
        }

        if (X[m - 1] == Y[n - 1])
        {
            return LCS(X, Y, m - 1, n - 1, lookup) + X[m - 1];
        }

        if (lookup[m - 1][n] > lookup[m][n - 1])
        {
            return LCS(X, Y, m - 1, n, lookup);
        }
        else
        {
            return LCS(X, Y, m, n - 1, lookup);
        }
    }

    void LCSLength(string X, string Y, int m, int n, auto &lookup)
    {
        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1])
                {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;
                }
                else
                {
                    lookup[i][j] = max(lookup[i - 1][j], lookup[i][j - 1]);
                }
            }
        }
    }

    /*vector<string> LCS(string X, string Y, int m, int n, auto &lookup)
    {
        if (m == 0 || n == 0)
        {
            return {""};
        }

        if (X[m - 1] == Y[n - 1])
        {
            vector<string> lcs = LCS(X, Y, m - 1, n - 1, lookup);

            for (string &str: lcs) {
                str.push_back(X[m - 1]);
            }

            return lcs;
        }

        if (lookup[m - 1][n] > lookup[m][n - 1]) {
            return LCS(X, Y, m - 1, n, lookup);
        }

        if (lookup[m][n - 1] > lookup[m - 1][n]) {
            return LCS(X, Y, m, n - 1, lookup);
        }

        vector<string> top = LCS(X, Y, m - 1, n, lookup);
        vector<string> left = LCS(X, Y, m, n - 1, lookup);

        top.insert(top.end(), left.begin(), left.end());
        // copy(left.begin(), left.end(), back_inserter(top));

        return top;
    }

    void LCSLength(string X, string Y, int m, int n, auto &lookup)
    {
        for (int i = 1; i <= m; ++i)
        {
            for (int j = 1; j <= n; ++j)
            {
                if (X[i - 1] == Y[j - 1])
                {
                    lookup[i][j] = lookup[i - 1][j - 1] + 1;
                }
                else
                {
                    lookup[i][j] = max(lookup[i - 1][j], lookup[i][j - 1]);
                }
            }
        }
    }*/
public:
    string findLCS(string X, string Y)
    {
        int m = X.length(), n = Y.length();

        vector<vector<int>> lookup(m + 1, vector<int>(n + 1));

        LCSLength(X, Y, m, n, lookup);

        return LCS(X, Y, m, n, lookup);
        /*int m = X.length(), n = Y.length();

        vector<vector<int>> lookup(m + 1, vector<int>(n + 1));

        LCSLength(X, Y, m, n, lookup);

        vector<string> v = LCS(X, Y, m, n, lookup);

        set<string> lcs(make_move_iterator(v.begin()), make_move_iterator(v.end()));

        return lcs;*/
    }
};


int main()
{
    string X = "XMJYAUZ", Y = "MZJAWXU";

    cout << Solution().findLCS(X, Y) << endl;

    return 0;
}
