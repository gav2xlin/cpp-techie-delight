/*

Given a map containing employee to manager mappings, find all employees under each manager who directly or indirectly reports him.

Input : {'A': 'A', 'B': 'A', 'C': 'B', 'D': 'B', 'E': 'D', 'F': 'E'}
Output: {'A': ['E', 'C', 'B', 'D', 'F'], 'B': ['D', 'E', 'C', 'F'], 'C': [], 'D': ['E', 'F'], 'E': ['F'], 'F': []}
Explanation: The input represents the following employee-manager pairs:

'A' —> 'A'
'B' —> 'A'
'C' —> 'B'
'D' —> 'B'
'E' —> 'D'
'F' —> 'E'

Here, 'A' reports to himself, i.e., 'A' is head of the company and is the manager of employee 'B'. 'B' is the manager of employees 'C' and 'D', 'D' is the manager of employee 'E', 'E' is the manager of employee 'F', 'C', and 'F' is not managers of any employee. i.e.,

'A' —> ['B', 'D', 'C', 'E', 'F']
'B' —> ['D', 'C', 'E', 'F']
'C' —> []
'D' —> ['E', 'F']
'E' —> ['F']
'F' —> []

*/

#include <iostream>
#include <unordered_map>
#include <unordered_set>

using namespace std;

class Solution
{
private:
    unordered_set<char> findAllReportingEmployees(char manager, auto &managerToEmployeeMappings, auto &result)
    {
        if (result.find(manager) != result.end())
        {
            // return the already computed mapping
            return result[manager];
        }

        unordered_set<char> managerEmployees = managerToEmployeeMappings[manager];

        for (char reportee: managerToEmployeeMappings[manager])
        {
            unordered_set<char> employees = findAllReportingEmployees(reportee, managerToEmployeeMappings, result);

            for (char c: employees) {
                managerEmployees.insert(c);
            }
        }

        result[manager] = managerEmployees;
        return managerEmployees;
    }
public:
    unordered_map<char, unordered_set<char>> findMapping(unordered_map<char, char> &employeeToManagerMappings)
    {
        unordered_map<char, unordered_set<char>> managerToEmployeeMappings;

        for (auto it: employeeToManagerMappings)
        {
            char employee = it.first;
            char manager = it.second;

            if (employee != manager) {
                managerToEmployeeMappings[manager].insert(employee);
            }
        }

        unordered_map<char, unordered_set<char>> result;

        for (auto p: employeeToManagerMappings) {
            findAllReportingEmployees(p.first, managerToEmployeeMappings, result);
        }

        return result;
    }
};

void printSet(char c, unordered_set<char> const &v)
{
    cout << c << " —> [";
    int n = v.size();
    for (auto i: v) {
        cout << i;
        if (--n) {
            cout << ", ";
        }
    }
    cout << "]\n";
}

int main()
{
    unordered_map<char, char> employeeToManagerMappings {
        {'A', 'A'}, {'B', 'A'}, {'C', 'B'}, {'D', 'B'}, {'E', 'D'}, {'F', 'E'}
    };

    auto result = Solution().findMapping(employeeToManagerMappings);

    for (auto p: result) {
        printSet(p.first, p.second);
    }

    return 0;
}
