/*

Given a list containing future predictions of share prices and a positive integer `k`, find the maximum profit earned by buying and selling shares at most `k` times with a constraint that a new transaction can only start after the previous transaction is complete, i.e., you can only hold at most one share at a time.

Input: price[]: [2, 4, 7, 5, 4, 3, 5], k = 2
Output: 7
Explanation: The maximum profit is 7 (sum of 5 and 2)

• Buy at a price 2 and sell at a price 7
• Buy at a price 3 and sell at a price 5


Input: price[]: [1, 5, 2, 3, 7, 6, 4, 5], k = 3
Output: 10
Explanation: The maximum profit is 10 (sum of 4, 5, and 1).

• Buy at a price 1 and sell at a price 5
• Buy at a price 2 and sell at a price 7
• Buy at a price 4 and sell at a price 5


Input: price[]: [10, 6, 8, 4, 2], k = 2
Output: 2
Explanation: The maximum profit is 2. Buy at a price 6 and sell at a price 8.


Input: price[]: [10, 8, 6, 5, 4, 2], k = 1
Output: 0
Explanation: Prices are given in descending order.

*/

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaximumProfit(vector<int> const &price, int k)
    {
        /*
        int n = price.size();
        if (n <= 1) {
            return 0;
        }

        int profit[k+1][n];

        for (int i = 0; i <= k;++i)
        {
            for (int j = 0; j < n; ++j)
            {
                if (i == 0 || j == 0) {
                    profit[i][j] = 0;
                }
                else
                {
                    int max_so_far = 0;
                    for (int k = 0; k < j; k++)
                    {
                        int curr_price = price[j] - price[k] + profit[i-1][k];
                        if (max_so_far < curr_price) {
                            max_so_far = curr_price;
                        }
                    }

                    profit[i][j] = max(profit[i][j-1], max_so_far);
                }
            }
        }

        return profit[k][n-1];
        */
        int n = price.size();
        if (n <= 1) {
            return 0;
        }

        int profit[k+1][n+1];

        for (int i = 0; i <= k; ++i)
        {
            int prev_diff = INT_MIN;

            for (int j = 0; j < n; ++j)
            {
                if (i == 0 || j == 0) {
                    profit[i][j] = 0;
                }
                else {
                    prev_diff = max(prev_diff, profit[i-1][j-1] - price[j-1]);
                    profit[i][j] = max(profit[i][j-1], price[j] + prev_diff);
                }
            }
        }

        return profit[k][n - 1];
    }
};

int main()
{
    cout << Solution().findMaximumProfit({1, 5, 2, 3, 7, 6, 4, 5}, 3) << endl;

    return 0;
}
