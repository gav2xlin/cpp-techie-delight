/*

Given a list containing future predictions of share prices, find the maximum profit earned by buying and selling shares at most twice with a constraint that a new transaction can only start after the previous transaction complete, i.e., we can only hold at most one share at a time.

Input: [2, 4, 7, 5, 4, 3, 5]
Output: 7
Explanation: The maximum profit is 7. You can buy at a price 2 and sell at a price 7. Then, buy at a price 3 and sell at a price 5.

Input: [10, 6, 8, 4, 2]
Output: 2
Explanation: The maximum profit is 2. You can buy at a price 6 and sell at a price 8.

Input: [8, 7, 6, 4]
Output: 0
Explanation: The maximum profit is 0 as buying and selling stock will result in loss.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaximumProfit(vector<int> const &price)
    {

        int n = price.size();
        if (n == 0) {
            return 0;
        }

        int profit[n];
        profit[n-1] = 0;

        int max_so_far = price[n - 1];

        for (int i = n - 2; i >= 0; --i)
        {
            profit[i] = max(profit[i + 1], max_so_far - price[i]);

            max_so_far = max(max_so_far, price[i]);
        }

        int min_so_far = price[0];

        for (int i = 1; i < n; ++i)
        {
            profit[i] = max(profit[i - 1], (price[i] - min_so_far) + profit[i]);

            min_so_far = min(min_so_far, price[i]);
        }

        return profit[n - 1];
    }
};

int main()
{
    cout << Solution().findMaximumProfit({2, 4, 7, 5, 4, 3, 5}) << endl;

    return 0;
}
