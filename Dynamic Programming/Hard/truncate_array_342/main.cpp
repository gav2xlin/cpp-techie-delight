/*

Given an array of positive integers, truncate it such that `2×min` becomes more than `max`, and the total number of removals is minimal. The `min` and `max` are the minimum and the maximum elements in the array, respectively. The elements can be removed either from the start or end of the array if the above condition does not meet.

Input: [4, 6, 1, 7, 5, 9, 2]
Output: 4
Explanation: The minimum number of removals is 4. The truncated array is [7, 5, 9] where 9 < 2 × 5.

Input: [4, 2, 6, 4, 9]
Output: 3
Explanation: The minimum number of removals is 3. The truncated array is [6, 4] where 6 < 2 × 4.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int truncate(vector<int> const &arr)
    {
        int n = arr.size();
        if (n == 0) {
            return 0;
        }

        vector<vector<int>> T(n, vector<int>(n, 0));

        for (int diagonal = 0; diagonal < n; ++diagonal)
        {
            for (int i = 0, j = diagonal; j < n; ++i, ++j)
            {
                int min = *min_element(arr.begin() + i, arr.begin() + j + 1);
                int max = *max_element(arr.begin() + i, arr.begin() + j + 1);

                if (2 * min <= max) {
                    T[i][j] = std::min(1 + T[i][j - 1], 1 + T[i + 1][j]);
                }
            }
        }

        return T[0][n - 1];
    }
};

int main()
{
    cout << Solution().truncate({4, 6, 1, 7, 5, 9, 2}) << endl;
    cout << Solution().truncate({4, 2, 6, 4, 9}) << endl;

    return 0;
}
