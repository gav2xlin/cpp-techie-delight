/*

Given two integers x and y, return the coefficients of Bézout's identity, i.e., integers a and b such that xa + yb = GCD(x, y). The GCD of two integers, x and y, is largest number that divides both x and y without leaving a remainder.

Input: x = 30, y = 50
Output: (2, -1)
Explanation: x = 2 and y = -1 since 30*2 + 50*-1 = 10

Input: x = 2740, y = 1760
Output: (9, -14)
Explanation: x = 9 and y = -14 since 2740*9 + 1760*-14 = 20

*/

// https://en.wikipedia.org/wiki/B%C3%A9zout%27s_identity

#include <iostream>
#include <utility>

using namespace std;

class Solution
{
private:
    pair<int,int> bezout(int a, int b) {
        if (a == 0) {
            return {0, 1};
        } else {
            auto [x, y] = bezout(b % a, a);
            return {y - (b / a) * x, x};
        }
    }
    /*tuple<int, int, int> extended_gcd(int a, int b)
    {
        if (a == 0) {
            return make_tuple(b, 0, 1);
        }

        auto[gcd, x, y] = extended_gcd(b % a, a);

        return make_tuple(gcd, y - (b / a) * x, x);
    }*/
public:
    pair<int,int> findCoefficients(int a, int b)
    {
        auto [x, y] = bezout(a, b);
        return make_pair(x, y);
    }
};

ostream& operator<<(ostream& os, const pair<int,int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

int main()
{
    cout << Solution().findCoefficients(30, 50) << endl;
    cout << Solution().findCoefficients(2740, 1760) << endl;

    return 0;
}
