cmake_minimum_required(VERSION 3.5)

project(factorial_543 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(factorial_543 main.cpp)

install(TARGETS factorial_543
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
