/*

Given a positive number, return the square root of it. If the number is not a perfect square, return the floor of its square root.

Input : x = 12
Output: 3

Input : x = 16
Output: 4

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    int sqrt(int n)
    {
        if (n < 2) {
            return n;
        }

        int low = 1, high = n / 2, result;

        while (low <= high) {
            int mid = (low + high) / 2;
            long sqr = mid * mid;

            if (sqr < n) {
                low = mid + 1;
                result = mid;
            } else if (sqr > n) {
                high = mid - 1;
            } else {
                return mid;
            }
        }

        return result;
    }
};

int main()
{
    cout << Solution().sqrt(12) << endl;
    cout << Solution().sqrt(16) << endl;

    return 0;
}
