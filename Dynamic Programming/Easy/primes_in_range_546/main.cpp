/*

Given a non-negative integer n, return all prime numbers less than or equal to n.

Input: n = 100
Output: [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    /*bool isPrime(int n) {
        if (n <= 3) {
            return n > 1;
        }
        if (n % 2 == 0 || n % 3 == 0) {
            return false;
        }
        for (int i = 5; i * i <= n; i += 6) {
            if (n % i == 0 || n % (i + 2) == 0) {
                return false;
            }
        }
        return true;
    }*/
public:
    vector<int> findPrimes(int n)
    {
        vector<int> primes;
        /*for (int i = 0; i <= n; ++i) {
            if (isPrime(i)) {
                primes.push_back(i);
            }
        }*/
        vector<bool> sieve(n + 1, true);
        sieve[0] = false;
        sieve[1] = false;

        for (int i = 2; i * i <= n; ++i) {
            if (sieve[i]) {
                for(int j = i * i; j <= n; j += i) {
                    sieve[j] = false;
                }
            }
        }

        for (int i = 0; i <= n; ++i) {
            if (sieve[i]) {
                primes.push_back(i);
            }
        }

        return primes;
    }
};

ostream& operator<<(ostream& os, const vector<int> &nums) {
    os << '[';
    for (int i = 0; i < nums.size() - 1; ++i) {
        os << nums[i] << ", ";
    }
    if (!nums.empty()) {
        os << nums.back();
    }
    os << ']';
    return os;
}

int main()
{
    cout << Solution().findPrimes(100) << endl;
    //cout << Solution().findPrimes(2) << endl;
    //cout << Solution().findPrimes(25) << endl;

    return 0;
}
