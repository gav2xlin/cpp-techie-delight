cmake_minimum_required(VERSION 3.5)

project(n_digit_numbers_262 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(n_digit_numbers_262 main.cpp)

install(TARGETS n_digit_numbers_262
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
