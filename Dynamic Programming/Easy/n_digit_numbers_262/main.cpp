/*

Given a positive integer n, return all n–digit binary numbers without any consecutive 1's.

Input: n = 5
Output: {"00000", "00001", "00010", "00100", "00101", "01000", "01001", "01010", "10000", "10001", "10010", "10100", "10101"}

*/

#include <iostream>
#include <unordered_set>
#include <string>

using namespace std;

class Solution
{
private:
    void generateNumber(unordered_set<string>& nums, string num, int n, bool is_one) {
        if (!n) {
            nums.insert(num);
            return;
        }

        generateNumber(nums, num + '0', n - 1, false);

        if (!is_one) {
            generateNumber(nums, num + '1', n - 1, true);
        }
    }
public:
    unordered_set<string> findNDigitNumbers(int n)
    {
        unordered_set<string> nums;
        generateNumber(nums, "", n, false);
        return nums;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& nums) {
    for (auto& num : nums) {
        os << num << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findNDigitNumbers(5) << endl;

    return 0;
}
