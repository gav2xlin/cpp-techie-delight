/*

Given two non-negative numbers x and y, return the factorial series in the range [x, y].

The factorial n! of a non-negative integer n is the product of all positive integers less than or equal to n. There are n! different ways to arrange n distinct objects into a sequence.

Input: x = 5, y = 10
Output: [120, 720, 5040, 40320, 362880, 3628800]
Explanation:

The factorial of 5 is 120
The factorial of 6 is 720
The factorial of 7 is 5040
The factorial of 8 is 40320
The factorial of 9 is 362880
The factorial of 10 is 3628800

Constraints: 0 <= x <= y <= 20

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    vector<unsigned long> findFactorial(int x, int y)
    {
        vector<unsigned long> res;
        res.reserve(y - x  + 1);

        long factorial;
        for (int i = 0; i <= y; ++i) {
            if (i == 0) {
                factorial = 1;
            } else {
                factorial *= i;
            }
            if (i >= x) {
                res.push_back(factorial);
            }
        }

        return res;
    }
};

template<typename T>
ostream& operator<<(ostream& os, const vector<T>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findFactorial(5, 10) << endl;

    return 0;
}
