/*

Given a non-negative number n, calculate the Fibonacci number F(n). Fibonacci's sequence is characterized by the fact that every number after the first two is the sum of the two preceding ones. For example,

The Fibonacci's sequence is [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, … ]

Input: n = 0
Output: 0
Explanation: F(0) = 0

Input: n = 1
Output: 1
Explanation: F(1) = 1

Input: n = 8
Output: 21
Explanation: F(8) = 21

*/

#include <iostream>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    unordered_map<int, long> cache; // memoization (+tabulation)
public:
    /*long fib(int n)
    {
        if (n <= 1) return n;

        return fib(n - 2) + fib(n - 1);
    }*/

    /*long fib(int n)
    {
        if (cache.find(n) == cache.end()) {
            long res;
            if (n <= 1) {
                res = n;
            } else {
                res = fib(n - 2) + fib(n - 1);
            }
            cache[n] = res;
        }

        return cache[n];
    }*/

    long fib(int n)
    {
        long res = 0, prev1 = 0, prev2 = 1;

        for (int i = 1; i <= n; ++i) {
            res = prev1 + prev2;
            prev2 = prev1;
            prev1 = res;
        }

        return res;
    }
};

int main()
{
    cout << Solution().fib(0) << endl;
    cout << Solution().fib(1) << endl;
    cout << Solution().fib(8) << endl;

    return 0;
}
