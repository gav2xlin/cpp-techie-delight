/*

Given two integers x and y, find the greatest common divisor (GCD) of x and y. The GCD of two integers, x and y, is largest number that divides both x and y without leaving a remainder.

Input: x = 30, y = 50
Output: 10

Input: x = 2740, y = 1760
Output: 20

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    int findGCD(int x, int y)
    {
        if (y == 0) return x;

        /*if (x > y) {
            return findGCD(x - y, y);
        } else {
            return findGCD(x, y - x);
        }*/
        return findGCD(y, x % y);
    }
};

int main()
{
    cout << Solution().findGCD(30, 50) << endl;
    cout << Solution().findGCD(2740, 1760) << endl;

    return 0;
}
