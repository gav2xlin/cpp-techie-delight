/*

Given an `M × N` rectangular grid, efficiently count all paths starting from the first cell (0, 0) to the last cell (M-1, N-1). You can either move down or move towards right from a cell.

Input : M = 3, N = 3

Output: 6

Explanation: Total number of paths are 6, as shown below:

(0, 0) —> (0, 1) —> (0, 2) —> (1, 2) —> (2, 2)
(0, 0) —> (0, 1) —> (1, 1) —> (1, 2) —> (2, 2)
(0, 0) —> (0, 1) —> (1, 1) —> (2, 1) —> (2, 2)
(0, 0) —> (1, 0) —> (2, 0) —> (2, 1) —> (2, 2)
(0, 0) —> (1, 0) —> (1, 1) —> (1, 2) —> (2, 2)
(0, 0) —> (1, 0) —> (1, 1) —> (2, 1) —> (2, 2)

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    long countPaths(int m, int n)
    {
        long T[m][n];

        for (int i = 0; i < m; i++) {
            T[i][0] = 1;
        }

        for (int j = 0; j < n; j++) {
            T[0][j] = 1;
        }

        for (int i = 1; i < m; i++)
        {
            for (int j = 1; j < n; j++) {
                T[i][j] = T[i-1][j] + T[i][j-1];
            }
        }

        return T[m-1][n-1];
    }
};

int main()
{
    cout << Solution().countPaths(3, 3) << endl;

    return 0;
}
