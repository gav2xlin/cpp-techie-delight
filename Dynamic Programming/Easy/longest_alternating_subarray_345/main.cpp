/*

Given an array containing positive and negative elements, find a subarray with alternating positive and negative elements, and in which the subarray is as long as possible.

Input : [1, -2, 6, 4, -3, 2, -4, -3]
Output: [4, -3, 2, -4]

Note that the longest alternating subarray might not be unique. In case the multiple alternating subarrays of maximum length exists, the solution can return any one of them.

Input : [1, -2, 6, 2, 4, -3, 2, 4, -3]
Output: [1, -2, 6] or [4, -3, 2]

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    vector<int> findLongestAlternatingSubarray(vector<int> const &nums)
    {
        vector<int> res;

        if (nums.size() != 0) {
            int curLen = 1, maxLen = 1, endIndex = 0;

            for (int i = 1; i < nums.size(); ++i) {
                if (nums[i] * nums[i - 1] < 0) {
                    ++curLen;

                    if (curLen > maxLen) {
                        maxLen = curLen;
                        endIndex = i;
                    }
                } else {
                    curLen = 1;
                }
            }

            res.reserve(maxLen);
            for (int i = endIndex - maxLen + 1; i <= endIndex; ++i) {
                res.push_back(nums[i]);
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findLongestAlternatingSubarray({1, -2, 6, 4, -3, 2, -4, -3}) << endl;
    cout << Solution().findLongestAlternatingSubarray({1, -2, 6, 2, 4, -3, 2, 4, -3}) << endl;

    cout << Solution().findLongestAlternatingSubarray({1}) << endl;

    return 0;
}
