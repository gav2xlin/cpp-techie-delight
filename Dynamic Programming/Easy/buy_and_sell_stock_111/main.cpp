/*

Given a list containing future price predictions of two different stocks for the next n–days, find the maximum profit earned by selling the stocks with a constraint that the second stock can be sold, only if no transaction happened on the previous day for any of the stock.

Assume that the given prices are relative to the buying price. Each day, we can either sell a single share of the first stock or a single share of the second stock or sell nothing.

Input: X = [5, 3, 4, 6, 3], Y = [8, 4, 3, 5, 10]

Day	 Price(X)  Price(Y)
1		5		 8
2		3		 4
3		4		 3
4		6		 5
5		3		 10

Output: 25

Explanation:

Day 1: Sell stock Y at a price of 8
Day 2: Sell stock X at a price of 3
Day 3: Sell stock X at a price of 4
Day 4: Don't sell anything
Day 5: Sell stock Y at a price of 10

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaximumProfit(vector<int> const &x, vector<int> const &y)
    {
        int n = x.size();
        if (n == 0) {
            return 0;
        }

        int T[n + 1];

        T[0] = 0;
        T[1] = max(x[0], y[0]);

        for (int i = 2; i <= n; i++) {
            T[i] = max(x[i - 1] + T[i - 1], y[i - 1] + T[i - 2]);
        }

        return T[n];
    }
};

int main()
{
    cout << Solution().findMaximumProfit({5, 3, 4, 6, 3}, {8, 4, 3, 5, 10}) << endl;

    return 0;
}
