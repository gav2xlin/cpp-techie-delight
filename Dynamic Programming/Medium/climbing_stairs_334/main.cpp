/*

Given two positive numbers n and m, find the total number of ways to reach the n'th stair from the bottom of a stair when a person is only allowed to take at-most m steps at a time.

Input: n = 3, m = 2
Output: 3
Explanation: Total ways to reach the 3rd stair with at most 2 steps are 3

1 step + 1 step + 1 step
1 step + 2 steps
2 steps + 1 step


Input: n = 4, m = 3
Output: 7
Explanation: Total ways to reach the 4th stair with at most 3 steps are 7

1 step + 1 step + 1 step + 1 steps
1 step + 1 step + 2 steps
1 step + 2 steps + 1 step
1 step + 3 steps
2 steps + 1 step + 1 step
2 steps + 2 steps
3 steps + 1 step

*/

#include <iostream>
#include <cstring>

using namespace std;

class Solution
{
private:
    int countWays(int n, int m, int lookup[])
    {
        if (n < 0) {
            return 0;
        }

        if (n == 0) {
            return 1;
        }

        if (lookup[n] == 0)
        {
            for (int i = 1; i <= m; ++i) {
                lookup[n] += countWays(n - i, m, lookup);
            }
        }

        return lookup[n];
    }
public:
    /*int countWays(int n, int m)
    {
        int lookup[n + 1];
        memset(lookup, 0, sizeof(int) * (n + 1));

        return countWays(n, m, lookup);
    }*/
    int countWays(int n, int m)
    {
        if (n == 1 || m == 1) {
            return 1;
        }

        int lookup[n + 1];

        lookup[0] = 1;
        lookup[1] = 1;
        lookup[2] = 2;

        for (int i = 3; i <= n; i++)
        {
            lookup[i] = 0;
            for (int j = 1; j <= m && (i - j) >= 0; ++j) {
                lookup[i] += lookup[i - j];
            }
        }

        return lookup[n];
    }
};

int main()
{
    cout << Solution().countWays(3, 2) << endl;
    cout << Solution().countWays(4, 3) << endl;

    return 0;
}
