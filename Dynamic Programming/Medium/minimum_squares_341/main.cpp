/*

Given a positive integer n, find the minimum number of squares that sums to n.

Input: 100
Output: 1
Explanation: 100 is a perfect square. It can be represented as 10^2.

Input: 10
Output: 2
Explanation: 10 can be represented as 3^2 + 1^2.

Input: 63
Output: 4
Explanation: 63 can be represented as 7^2 + 3^2 + 2^2 + 1^2.

*/

#include <iostream>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMinimumSquares(int n)
    {
        int T[n + 1];

        for (int i = 0; i <= n; ++i)
        {
            T[i] = i;

            for (int j = 1; j * j <= i; ++j)
            {
                T[i] = min(T[i], 1 + T[i - j * j]);
            }
        }

        return T[n];
    }
};

int main()
{
    cout << Solution().findMinimumSquares(100) << endl;
    cout << Solution().findMinimumSquares(10) << endl;
    cout << Solution().findMinimumSquares(63) << endl;

    return 0;
}
