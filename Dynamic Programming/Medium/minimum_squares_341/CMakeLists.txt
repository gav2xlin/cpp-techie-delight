cmake_minimum_required(VERSION 3.5)

project(minimum_squares_341 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(minimum_squares_341 main.cpp)

install(TARGETS minimum_squares_341
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
