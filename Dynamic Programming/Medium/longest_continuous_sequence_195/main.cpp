/*

Given an `M × N` matrix of characters, find the length of the longest path in the matrix starting from a given character. All characters in the longest path should be increasing and consecutive to each other in alphabetical order.

You are allowed to search the string in all eight possible directions, i.e., North, West, South, East, North-East, North-West, South-East, South-West.

Input:

mat = [
    ['D', 'E', 'H', 'X', 'B'],
    ['A', 'O', 'G', 'P', 'E'],
    ['D', 'D', 'C', 'F', 'D'],
    ['E', 'B', 'E', 'A', 'S'],
    ['C', 'D', 'Y', 'E', 'N']
]
ch = 'C'

Output: 6

Explanation: The longest path with consecutive characters starting from character `C` is: C(2, 2) —> D(2, 1) —> E(3, 2) —> F(2, 3) —> G(1, 2) —> H(0, 2)


Input:

mat = [
    ['A', 'B', 'C'],
    ['D', 'E', 'F']
]
ch = 'I'

Output: 0

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int row[8] = {-1, -1, -1, 0, 0, 1, 1, 1};
    int col[8] = {-1, 0, 1, -1, 1, -1, 0, 1};

    bool isValid(int x, int y, int M, int N) {
        return (x >= 0 && x < M && y >= 0 && y < N);
    }

    int findMaxLen(vector<vector<char>> const &mat, int x, int y, char previous)
    {
        int M = mat.size();
        int N = mat[0].size();

        if (!isValid(x, y, M, N) || previous + 1 != mat[x][y]) {
            return 0;
        }

        int max_length = 0;

        for (int k = 0; k < 8; ++k)
        {
            int len = findMaxLen(mat, x + row[k], y + col[k], mat[x][y]);

            max_length = max(max_length, 1 + len);
        }

        return max_length;
    }
public:
    int findMaxLength(vector<vector<char>> const &mat, char ch)
    {
        if (mat.empty()) {
            return 0;
        }

        int M = mat.size();
        int N = mat[0].size();

        int max_length = 0;

        for (int x = 0; x < M; ++x)
        {
            for (int y = 0; y < N; ++y)
            {
                if (mat[x][y] == ch)
                {
                    for (int k = 0; k < 8; ++k)
                    {
                        int len = findMaxLen(mat, x + row[k], y + col[k], ch);

                        max_length = max(max_length, 1 + len);
                    }
                }
            }
        }

        return max_length;
    }
};

int main()
{
    vector<vector<char>> mat {
        {'D', 'E', 'H', 'X', 'B'},
        {'A', 'O', 'G', 'P', 'E'},
        {'D', 'D', 'C', 'F', 'D'},
        {'E', 'B', 'E', 'A', 'S'},
        {'C', 'D', 'Y', 'E', 'N'}
    };

    cout << Solution().findMaxLength(mat, 'C') << endl;

    return 0;
}
