/*

Given an `n × 4` matrix where `n` is a positive number, find the number of ways to fill the matrix with `1 × 4` tiles.

Input: n = 5
Output: 3
Explanation: There are three ways to place `1 × 4` tiles in a `5 × 4` matrix:

• Place all 5 tiles horizontally.
• Place 4 tiles vertically in the first four rows and 1 tile horizontally in the last row.
• Place 1 tile horizontally in the first row and adjust 4 tiles vertically in the remaining rows.


Input: n = 6
Output: 4
Explanation: There are 4 ways to place `1 × 4` tiles in a `6 × 4` matrix:

• Place all 6 tiles horizontally.
• Place 4 tiles vertically in the first four rows and the remaining 2 tiles horizontally in the last two rows.
• Place 2 tiles horizontally in the first two rows, and adjust the remaining 4 tiles vertically in the remaining rows.
• Place 2 tiles horizontally in the first and last row, and adjust the remaining 4 tiles vertically in the middle rows.

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    long findTotalWays(int n)
    {
        /*if (n < 1) {
            return 0;
        }

        if (n < 4) {
            return 1;
        }

        if (n == 4) {
            return 2;
        }

        return findTotalWays(n - 1) + findTotalWays(n - 4);*/

        if (n < 1) {
            return 0;
        }

        if (n < 4) {
            return 1;
        }

        long T[n + 1];

        // for `n <= 4`
        T[1] = T[2] = T[3] = 1;
        T[4] = 2;

        // fill in a bottom-up fashion
        for (int i = 5; i <= n; i++) {
            // place a single tile horizontally or 4 tiles vertically together
            T[i] = T[i - 1] + T[i - 4];
        }

        return T[n];
    }
};

int main()
{
    cout << Solution().findTotalWays(5) << endl;
    cout << Solution().findTotalWays(6) << endl;

    return 0;
}
