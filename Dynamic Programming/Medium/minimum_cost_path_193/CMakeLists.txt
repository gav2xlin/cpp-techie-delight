cmake_minimum_required(VERSION 3.5)

project(minimum_cost_path_193 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(minimum_cost_path_193 main.cpp)

install(TARGETS minimum_cost_path_193
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
