/*

Given an `N × N` matrix of non-negative integers, where each cell of the matrix (i, j) indicates the direct flight cost from the city `i` to city `j`. The task is to find the minimum cost to reach the destination city `N-1` from the source city 0.

Input:

mat = [
    [0,   20, 30, 100],
    [20,  0,  15, 75 ],
    [30,  15, 0,  50 ],
    [100, 75, 50, 0  ]
]

Output: 80

Explanation: The minimum cost is 80 and the minimum cost path is:

city 0 —> city 2 (cost = 30)
city 2 —> city 3 (cost = 50)


Input:

mat = [
    [0,  25, 20, 10, 105],
    [20, 0,  15, 80, 80 ],
    [30, 15, 0,  70, 90 ],
    [10, 10, 50, 0,  100],
    [40, 50, 5,  10, 0  ]
]

Output: 100

Explanation: The minimum cost is 100 and the minimum cost path is:

city 0 —> city 3 (cost = 10)
city 3 —> city 1 (cost = 10)
city 1 —> city 4 (cost = 80)

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    int findMinCost(vector<vector<int>> const &cost)
    {
        if (cost.empty()) {
            return 0;
        }

        int N = cost.size();

        int lookup[N];

        for (int i = 0; i < N; ++i) {
            lookup[i] = cost[0][i];
        }

        bool is_filled = false;
        while (!is_filled)
        {
            is_filled = true;

            for (int i = 0; i < N; ++i)
            {
                for (int j = 0; j < N; ++j)
                {
                    if (lookup[i] > lookup[j] + cost[j][i])
                    {
                        lookup[i] = lookup[j] + cost[j][i];
                        is_filled = false;
                    }
                }
            }
        }

        return lookup[N-1];
    }
};

int main()
{
    {
        vector<vector<int>> cost{
            {0, 20, 30, 100},
            {20, 0, 15, 75},
            {30, 15, 0, 50},
            {100, 75, 50, 0}
        };

        cout << Solution().findMinCost(cost) << endl;
    }

    {
        vector<vector<int>> cost{
            {0, 25, 20, 10, 105},
            {20, 0, 15, 80, 80},
            {30, 15, 0, 70, 90},
            {10, 10, 50, 0, 100},
            {40, 50, 5, 10, 0}
        };

        cout << Solution().findMinCost(cost) << endl;
    }

    return 0;
}
