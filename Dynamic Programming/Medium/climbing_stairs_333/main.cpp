/*

Given a positive number n, find the total number of ways to reach the n'th stair from the bottom of a stair when a person can only climb either 1 or 2 or 3 stairs at a time.

Input: 3
Output: 4
Explanation: Total ways to reach the 3rd stair are 4

1 step + 1 step + 1 step
1 step + 2 steps
2 steps + 1 step
3 steps


Input: 4
Output: 7
Explanation: Total ways to reach the 4'th stair are 7

1 step + 1 step + 1 step + 1 steps
1 step + 1 step + 2 steps
1 step + 2 steps + 1 step
1 step + 3 steps
2 steps + 1 step + 1 step
2 steps + 2 steps
3 steps + 1 step

*/

#include <iostream>
#include <cstring>

using namespace std;

class Solution
{
private:
    int totalWays(int n, int lookup[])
    {
        if (n < 0) {
            return 0;
        }

        if (n == 0) {
            return 1;
        }

        if (lookup[n] == 0)
        {
            lookup[n] = totalWays(n - 1, lookup) + totalWays(n - 2, lookup) + totalWays(n - 3, lookup);
        }

        return lookup[n];
    }
public:
    /*int totalWays(int n)
    {
        int lookup[n + 1];
        memset(lookup, 0, sizeof(int) * (n + 1));

        return totalWays(n, lookup);
    }*/
    int totalWays(int n)
    {
        if (n <= 2) {
            return n;
        }

        int lookup[n + 1];

        lookup[0] = 1;
        lookup[1] = 1;
        lookup[2] = 2;

        for (int i = 3; i <= n; ++i) {
            lookup[i] = lookup[i - 1] + lookup[i - 2] + lookup[i - 3];
        }

        return lookup[n];
    }
};

int main()
{
    cout << Solution().totalWays(3) << endl;
    cout << Solution().totalWays(4) << endl;

    return 0;
}
