cmake_minimum_required(VERSION 3.5)

project(rod_cutting_174 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(rod_cutting_174 main.cpp)

install(TARGETS rod_cutting_174
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
