/*

Given a rod of length n, find the optimal way to cut the rod into smaller rods to maximize the product of each of the smaller rod's price. Assume that each rod of length i has price i.

Input: n = 4 (rod length)
Output: 4
Explanation: Cut the rod into two pieces of length 2 each to gain revenue of 2×2 = 4

Cut				Profit
4				4
1, 3			(1 × 3) = 3
2, 2			(2 × 2) = 4	  <-- optimal way
3, 1			(3 × 1) = 3
1, 1, 2			(1 × 1 × 2) = 2
1, 2, 1			(1 × 2 × 1) = 2
2, 1, 1			(2 × 1 × 1) = 2
1, 1, 1, 1		(1 × 1 × 1 × 1) = 1

Similarly, for n = 6, (3 × 3) = 9
For n = 8, (2 × 3 × 3) = 18
For n = 15, (3 × 3 × 3 × 3 × 3) = 243

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    long findMaxProfit(int n)
    {
        long T[n + 1];

        for (int i = 0; i <= n; ++i) {
            T[i] = i;
        }

        for (int i = 2; i <= n; ++i)
        {
            // divide the rod of length `i` into two rods of length `j`
            // and `i-j` each and take maximum
            for (int j = 1; j <= i; ++j) {
                T[i] = max(T[i], j * T[i - j]);
            }
        }

        return T[n];
    }
};

int main()
{
    cout << Solution().findMaxProfit(4) << endl;

    return 0;
}
