/*

Given an `M × N` matrix of integers where each cell has a cost associated with it, find the minimum cost to reach the last cell (M-1, N-1) of the matrix from its first cell (0, 0). You can only move one unit right or one unit down from any cell, i.e., from cell (i, j), you can move to (i, j+1) or (i+1, j).

Input:

[
    [4, 7, 8, 6, 4],
    [6, 7, 3, 9, 2],
    [3, 8, 1, 2, 4],
    [7, 1, 7, 3, 7],
    [2, 9, 8, 9, 3]
]

Output: 36

Explanation: The highlighted path shows the minimum cost path having a cost of 36.

    4   7   8   6   4
    |
    6 — 7 — 3   9   2
            |
    3   8   1 — 2   4
                |
    7   1   7   3 — 7
                    |
    2   9   8   9   3

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMinCost(vector<vector<int>> const &cost)
    {
        if (cost.empty()) {
            return 0;
        }

        int M = cost.size();
        int N = cost[0].size();

        int T[M][N];

        for (int i = 0; i < M; ++i)
        {
            for (int j = 0; j < N; ++j)
            {
                T[i][j] = cost[i][j];

                if (i == 0 && j > 0) {
                    T[0][j] += T[0][j - 1];
                } else if (j == 0 && i > 0) {
                    T[i][0] += T[i - 1][0];
                } else if (i > 0 && j > 0) {
                    T[i][j] += min(T[i - 1][j], T[i][j - 1]);
                }
            }
        }

        return T[M - 1][N - 1];
    }
};

int main()
{
    vector<vector<int>> cost =
    {
        { 4, 7, 8, 6, 4 },
        { 6, 7, 3, 9, 2 },
        { 3, 8, 1, 2, 4 },
        { 7, 1, 7, 3, 7 },
        { 2, 9, 8, 9, 3 }
    };

    cout << Solution().findMinCost(cost) << endl;

    return 0;
}
