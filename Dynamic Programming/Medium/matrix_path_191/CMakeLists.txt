cmake_minimum_required(VERSION 3.5)

project(matrix_path_191 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(matrix_path_191 main.cpp)

install(TARGETS matrix_path_191
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
