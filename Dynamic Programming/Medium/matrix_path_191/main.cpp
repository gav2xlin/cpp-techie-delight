/*

Given an `M × N` integer matrix where each cell has a non-negative cost associated with it, count the number of paths to reach the last cell (M-1, N-1) of the matrix from its first cell (0, 0) such that the path has given cost. You can only move one unit right or one unit down from any cell, i.e., from cell (i, j), you can move to (i, j+1) or (i+1, j).

Input:

mat = [
    [4, 7, 1, 6],
    [5, 7, 3, 9],
    [3, 2, 1, 2],
    [7, 1, 6, 3]
]

cost: 25

Output: 2

Explanation: The following two paths have a cost of 25.


    4 — 7 — 1   6				4   7   1   6
            |					|
    5   7   3   9				5 — 7 — 3   9
            |							|
    3   2   1   2				3   2   1 — 2
            |								|
    7   1   6 — 3				7   1   6   3

*/

#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

class Solution
{
private:
    int countPaths(vector<vector<int>> const &mat, int m, int n, int cost, unordered_map<string,int> &lookup)
    {
        if (cost < 0) {
            return 0;
        }

        if (m == 0 && n == 0)
        {
            if (mat[0][0] - cost == 0) {
                return 1;
            }
            else {
                return 0;
            }
        }

        string key = to_string(m) + "|" + to_string(n) + "|" + to_string(cost);

        if (lookup.find(key) == lookup.end())
        {
            if (m == 0) {
                lookup[key] = countPaths(mat, 0, n - 1, cost - mat[m][n], lookup);
            } else if (n == 0) {
                lookup[key] = countPaths(mat, m - 1, 0, cost - mat[m][n], lookup);
            } else {
                lookup[key] = countPaths(mat, m - 1, n, cost - mat[m][n], lookup) +
                    countPaths(mat, m, n - 1, cost - mat[m][n], lookup);
            }
        }

        return lookup[key];
    }
public:
    int countPaths(vector<vector<int>> const &mat, int cost)
    {
        if (mat.empty()) {
            return 0;
        }

        int M = mat.size();
        int N = mat[0].size();

        unordered_map<string, int> lookup;
        return countPaths(mat, M - 1, N - 1, cost, lookup);
    }
};

int main()
{
    vector<vector<int>> mat =
    {
        { 4, 7, 1, 6 },
        { 5, 7, 3, 9 },
        { 3, 2, 1, 2 },
        { 7, 1, 6, 3 }
    };

    cout << Solution().countPaths(mat, 25) << endl;

    return 0;
}
