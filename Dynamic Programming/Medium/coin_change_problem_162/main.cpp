/*

Given an unlimited supply of coins of given denominations, find the minimum number of coins required to get the desired change.

Input: S = [1, 3, 5, 7], target = 15
Output: 3
Explanation: The minimum number of coins required is 3 (7 + 7 + 1) or (5 + 5 + 5) or (3 + 5 + 7)

Input: S = [1, 3, 5, 7], target = 18
Output: 4
Explanation: The minimum number of coins required is 4 (7 + 7 + 3 + 1) or (5 + 5 + 5 + 3) or (7 + 5 + 5 + 1)

If desired change is not possible, the solution should return -1.

Input: S = [2, 4, 6, 8], target = 15
Output: -1

*/

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMinCoins(vector<int> const &S, int target)
    {
        int T[target + 1];
        T[0] = 0;

        for (int i = 1; i <= target; ++i)
        {
            T[i] = INT_MAX;
            int result = INT_MAX;

            for (int c: S)
            {
                if (i - c >= 0) {
                    result = T[i - c];
                }

                if (result != INT_MAX) {
                    T[i] = min(T[i], result + 1);
                }
            }
        }

        return T[target] != INT_MAX ? T[target] : -1;
    }
};

int main()
{
    cout << Solution().findMinCoins({1, 3, 5, 7}, 15) << endl;
    cout << Solution().findMinCoins({1, 3, 5, 7}, 18) << endl;
    cout << Solution().findMinCoins({2, 4, 6, 8}, 15) << endl;

    return 0;
}
