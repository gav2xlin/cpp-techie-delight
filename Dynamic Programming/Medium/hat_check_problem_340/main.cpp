/*

Given a positive number `n`, find the total number of ways in which `n` hats can be returned to `n` people such that no hat makes it back to its owner.

Input: n = 2
Output: 1
Explanation: You're given a 2–hat set, say [h1, h2]. There is only one derangement [h2, h1].

Input: n = 3
Output: 2
Explanation: You're given a 3–hat set, say [h1, h2, h3]. The derangements are [h3, h1, h2] and [h2, h3, h1].

Input: n = 4
Output: 9
Explanation: You're given a 4–hat set, say [h1, h2, h3, h4]. The derangements are

[h2, h1, h4, h3]
[h2, h3, h4, h1]
[h2, h4, h1, h3]
[h3, h4, h1, h2]
[h3, h1, h4, h2]
[h3, h4, h2, h1]
[h4, h1, h2, h3]
[h4, h3, h1, h2]
[h4, h3, h2, h1]

*/

#include <iostream>

using namespace std;

class Solution
{
public:
    long countDerangements(int n)
    {
        if (n <= 1) {
            return 0;
        }

        long T[n + 1];

        T[1] = 0, T[2] = 1;

        for (int i = 3; i <= n; ++i) {
            T[i] = (i - 1) *(T[i - 1] + T[i - 2]);
        }

        return T[n];
    }
};

int main()
{
    cout << Solution().countDerangements(2) << endl;
    cout << Solution().countDerangements(3) << endl;
    cout << Solution().countDerangements(4) << endl;

    return 0;
}
