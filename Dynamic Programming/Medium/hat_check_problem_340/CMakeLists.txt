cmake_minimum_required(VERSION 3.5)

project(hat_check_problem_340 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(hat_check_problem_340 main.cpp)

install(TARGETS hat_check_problem_340
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
