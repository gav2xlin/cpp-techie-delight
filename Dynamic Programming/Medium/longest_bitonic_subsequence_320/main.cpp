/*

Given a sequence, find the longest bitonic subsequence in which the subsequence's elements are first sorted in increasing order, then in decreasing order.

Input: [4, 2, 5, 9, 7, 6, 10, 3, 1]
Output: 7
Explanation: The longest bitonic subsequence is [4, 5, 9, 7, 6, 3, 1].

For sequences sorted in increasing or decreasing order, the output is the same as the input sequence, i.e.,

Input: [1, 2, 3, 4, 5]
Output: 5

Input: [5, 4, 3, 2, 1]
Output: 5

*/

#include <iostream>
#include <vector>
#include <algorithm>
#include <list>

using namespace std;

class Solution
{
public:
    int findLBSLength(vector<int> const &nums)
    {
        int n = nums.size();
        if (n == 0) {
            return 0;
        }

        vector<int> I(n);
        vector<int> D(n);

        I[0] = 1;
        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                if (nums[j] < nums[i] && I[j] > I[i]) {
                    I[i] = I[j];
                }
            }
            I[i]++;
        }

        D[n - 1] = 1;
        for (int i = n - 2; i >= 0; --i)
        {
            for (int j = n - 1; j > i; --j)
            {
                if (nums[j] < nums[i] && D[j] > D[i]) {
                    D[i] = D[j];
                }
            }
            D[i]++;
        }

        int lbs = 1;
        for (int i = 0; i < n; ++i) {
            lbs = max(lbs, I[i] + D[i] - 1);
        }

        return lbs;
    }

    /*void LBS(vector<int> const &nums)
    {
        int n = nums.size();
        if (n == 0) {
            return;
        }

        list<int> I[n];
        I[0].push_back(nums[0]);

        for (int i = 1; i < n; ++i)
        {
            for (int j = 0; j < i; ++j)
            {
                if (I[i].size() < I[j].size() && nums[i] > nums[j]) {
                    I[i] = I[j];
                }
            }
            I[i].push_back(nums[i]);
        }

        list<int> D[n];
        D[n - 1].push_front(nums[n - 1]);

        for (int i = n - 2; i >= 0; --i)
        {
            for (int j = n - 1; j > i; --j)
            {
                if (D[i].size() < D[j].size() && nums[i] > nums[j]) {
                    D[i] = D[j];
                }
            }
            D[i].push_front(nums[i]);
        }*/

        // uncomment the following code to print contents of vector LIS and LDS
        /* for (int i = 0; i < n; i++)
        {
            cout << "LIS[" << i << "] — ";
            for (int j: I[i]) {
                cout << j << " ";
            }

            cout << "\t\tLDS[" << i << "] — ";
            for (int j: D[i]) {
                cout << j << " ";
            }
            cout << endl;
        } */

        /*int peak = 0;
        for (int i = 1; i < n; ++i)
        {
            if ((I[i].size() + D[i].size()) > (I[peak].size() + D[peak].size())) {
                peak = i;
            }
        }

        cout << "The longest bitonic subsequence is ";

        for (int i: I[peak]) {
            cout << i << " ";
        }

        D[peak].pop_front();

        for (int i: D[peak]) {
            cout << i << " ";
        }
    }*/
};

int main()
{
    cout << Solution().findLBSLength({4, 2, 5, 9, 7, 6, 10, 3, 1}) << endl;
    cout << Solution().findLBSLength({1, 2, 3, 4, 5}) << endl;
    cout << Solution().findLBSLength({5, 4, 3, 2, 1}) << endl;

    return 0;
}
