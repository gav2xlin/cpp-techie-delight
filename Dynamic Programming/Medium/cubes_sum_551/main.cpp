/*

Given an integer n, find all positive numbers less than n that can be represented as the sum of two cubes for at least two different pairs. In other words, find all positive numbers m <= n that can be expressed as m = (a^3 + b^3) = (c^3 + d^3) for distinct a, b, c, d.

For example,

Input: n = 25000
Output: {1729, 4104, 13832, 20683}
Explanation: For n = 25000, m can be any of 1729, 4104, 13832, or 20683 as these numbers can be represented as the sum of two cubes for two different pairs.

1729 = 1^3 + 12^3 = 9^3 + 10^3
4104 = 2^3 + 16^3 = 9^3 + 15^3
13832 = 2^3 + 24^3 = 18^3 + 20^3
20683 = 10^3 + 27^3 = 19^3 + 24^3

*/

#include <iostream>
#include <unordered_set>
#include <cmath>

using namespace std;

class Solution
{
public:
    unordered_set<int> findAllNumbers(int n)
    {
        unordered_set<int> res;

        int cb = pow(n, 1.0 / 3);

        unordered_set<int> s;

        for (int i = 1; i < cb - 1; ++i)
        {
            for (int j = i + 1; j < cb + 1; ++j)
            {
                // (i, j) forms a pair
                int sum = i * i * i + j * j * j;

                if (s.find(sum) != s.end()) {
                    //cout << sum << endl;
                    res.insert(sum);
                } else {
                    s.insert(sum);
                }
            }
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const unordered_set<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findAllNumbers(25000) << endl;

    return 0;
}
