/*

An island is in the form of an `N × N` square matrix, and a person is standing inside the matrix. The person can move one step in any direction (right, left, top, down) in the matrix and each step carry equal probability, i.e., 1/4 or 0.25. Calculate the probability that the person is alive after walking `n` steps on the island, provided that the person dies on stepping outside the matrix.

Input:

N = 2			(Matrix dimentions)
n = 1			(Total number of steps)
(x, y) = (0, 0)	(Starting coordinates)

Output: 0.5		(Alive probability)


Input: N = 3, n = 1, (x, y) = (1, 1)
Output: 1

Input: N = 3, n = 3, (x, y) = (0, 0)
Output: 0.25

*/

#include <iostream>
#include <map>
#include <string>

using namespace std;

class Solution
{
private:
    double aliveProbability(int N, int x, int y, int n, map<string, double> &dp)
    {
        if (n == 0) {
            return 1.0;
        }

        string key = to_string(x) + "|" + to_string(y) + "|" + to_string(n);

        if (dp.find(key) == dp.end())
        {
            double p = 0.0;

            if (x > 0) {
                p += 0.25 * aliveProbability(N, x - 1, y, n - 1, dp);
            }

            if (x < N - 1) {
                p += 0.25 * aliveProbability(N, x + 1, y, n - 1, dp);
            }

            if (y > 0) {
                p += 0.25 * aliveProbability(N, x, y - 1, n - 1, dp);
            }

            if (y < N - 1) {
                p += 0.25 * aliveProbability(N, x, y + 1, n - 1, dp);
            }

            dp[key] = p;
        }

        return dp[key];
    }
public:
    double findProbability(int N, int n, int x, int y)
    {
        map<string, double> dp;
        return aliveProbability(N, x, y, n, dp);
    }
};

int main()
{
    cout << Solution().findProbability(2, 1, 0, 0) << endl;
    cout << Solution().findProbability(3, 1, 1, 1) << endl;
    cout << Solution().findProbability(3, 3, 0, 0) << endl;

    return 0;
}
