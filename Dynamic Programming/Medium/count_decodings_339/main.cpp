/*

Given a positive number n, map its digits to the corresponding alphabet in the mapping table [(1, 'A'), (2, 'B'), (3, 'C'), … (26, 'Z')], and return the count of the total number of decodings possible. Assume that the input number can be split into valid single-digit or two-digit numbers that are present in the mapping table.

Input: n = 123
Output: 3
Explanation: The possible decodings are [ABC, AW, LC]

Input: n = 1221
Output: 5
Explanation: The possible decodings are [ABBA, ABU, AVA, LBA, LU]

*/

#include <iostream>
#include <string>
#include <cstring>

using namespace std;

class Solution
{
public:
    /*int countDecodings(int x)
    {
        string seq = to_string(x);
        int n = seq.length();

        //int T[n + 1] = { 0 };
        int T[n + 1];
        memset(T, 0, sizeof(T));

        T[0] = 1;
        T[1] = 1;

        for (int i = 2; i <= n; ++i)
        {
            // consider single-digit numbers (1, 2, … 8, 9)
            if (seq[i - 1] > '0') {
                T[i] = T[i - 1];
            }

            // consider 2-digit numbers (10, 11, … 19, 20, … 25, 26)
            if (seq[i - 2] == '1' || (seq[i - 2] == '2' && seq[i - 1] <= '6')) {
                T[i] += T[i - 2];
            }
        }

        return T[n];
    }*/
    int countDecodings(int x)
    {
        string seq = to_string(x);

        int prev_of_prev = 1;
        int prev = 1;

        for (int i = 2; i <= seq.length(); ++i)
        {
            int curr = 0;

            // consider single-digit numbers (1, 2, … 8, 9)
            if (seq[i - 1] > '0') {
                curr = prev;
            }

            // consider 2-digit numbers (10, 11, … 19, 20, … 25, 26)
            if (seq[i - 2] == '1' || (seq[i - 2] == '2' && seq[i - 1] <= '6')) {
                curr += prev_of_prev;
            }

            prev_of_prev = prev;
            prev = curr;
        }

        return prev;
    }
};

int main()
{
    cout << Solution().countDecodings(123) << endl;
    cout << Solution().countDecodings(1221) << endl;

    return 0;
}
