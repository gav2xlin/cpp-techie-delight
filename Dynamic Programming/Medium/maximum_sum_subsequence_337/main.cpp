/*

Given an integer array, find the maximum sum of subsequence where the subsequence contains no element at adjacent positions.

Input: [1, 2, 9, 4, 5, 0, 4, 11, 6]
Output: 26
Explanation: The maximum sum is 26 and formed by the subsequence [1, 9, 5, 11].

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findMaximumSumSubsequence(vector<int> const &nums)
    {
        /*int n = nums.size();

        if (n == 0) {
            return 0;
        }

        if (n == 1) {
            return nums[0];
        }

        int lookup[n];

        lookup[0] = nums[0];
        lookup[1] = max(nums[0], nums[1]);

        for (int i = 2; i < n; ++i)
        {
            lookup[i] = max(lookup[i-1], lookup[i-2] + nums[i]);

            lookup[i] = max(lookup[i], nums[i]);
        }

        return lookup[n-1];*/
        int n = nums.size();

        if (n == 0) {
            return 0;
        }

        if (n == 1) {
            return nums[0];
        }

        int prev_prev = nums[0];
        int prev = max(nums[0], nums[1]);

        for (int i = 2; i < n; ++i)
        {
            int curr = max(nums[i], max(prev, prev_prev + nums[i]));
            prev_prev = prev;
            prev = curr;
        }

        return prev;
    }
};

int main()
{
    cout << Solution().findMaximumSumSubsequence({1, 2, 9, 4, 5, 0, 4, 11, 6}) << endl;

    return 0;
}
