/*

Given a set of positive integers and an integer `k`, check if there is any non-empty subset that sums to `k`.

Input: nums = [7, 3, 2, 5, 8], k = 14
Output: true
Explanation: Subset [7, 2, 5] sums to 14

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
public:
    bool subsetSum(vector<int> const &nums, int k)
    {
        int n = nums.size();

        bool T[n + 1][k + 1];

        for (int j = 1; j <= k; ++j) {
            T[0][j] = false;
        }

        for (int i = 0; i <= n; ++i) {
            T[i][0] = true;
        }

        for (int i = 1; i <= n; ++i)
        {
            for (int j = 1; j <= k; ++j)
            {
                // don't include the i'th element if `j-A[i-1]` is negative
                if (nums[i - 1] > j) {
                    T[i][j] = T[i - 1][j];
                } else {
                    // find the subset with sum `j` by excluding or including the i'th item
                    T[i][j] = T[i - 1][j] || T[i - 1][j - nums[i - 1]];
                }
            }
        }

        return T[n][k];
    }
};

int main()
{
    cout << boolalpha << Solution().subsetSum({7, 3, 2, 5, 8}, 14) << endl;

    return 0;
}
