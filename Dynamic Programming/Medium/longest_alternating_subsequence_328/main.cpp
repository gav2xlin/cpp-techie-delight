/*

The longest alternating subsequence is a problem of finding a subsequence of a given sequence in which the elements are in alternating order and in which the sequence is as long as possible. In order words, we need to find the length of the longest subsequence with alternate low and high elements.

Input: [8, 9, 6, 4, 5, 7, 3, 2, 4]
Output: 6
Explanation: The longest alternating subsequence length is 6, and the subsequence is [8, 9, 6, 7, 3, 4] as (8 < 9 > 6 < 7 > 3 < 4).

Note that the longest alternating subsequence is not unique. Following are a few more subsequences of length 6:

(8, 9, 6, 7, 2, 4)
(8, 9, 4, 7, 3, 4)
(8, 9, 4, 7, 2, 4)

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution
{
public:
    int findLongestAlternatingSequence(vector<int> const &nums)
    {
        int n = nums.size();
        if (n <= 1) {
            return n;
        }

        int T[n][2];

        /*
         `T[i][0]` stores the longest alternating subsequence till `A[0…i]`
         where `A[i]` is greater than `A[i-1]`

         `T[i][1]` stores the longest alternating subsequence till `A[0…i]`
         where `A[i]` is smaller than `A[i-1]`
        */

        for (int i = 1; i < n; ++i) {
            T[i][0] = T[i][1] = 0;
        }

    T[0][0] = T[0][1] = 1;

    int result = 1;

    for (int i = 1; i < n; i++)
    {
        // do for each element `A[j]` before `A[i]`
        for (int j = 0; j < i; j++)
        {
            // If `A[i]` is greater than `A[j]`, update `T[i][0]`
            if (nums[i] > nums[j]) {
                T[i][0] = max(T[i][0], T[j][1] + 1);
            }

            // If `A[i]` is smaller than `A[j]`, update `T[i][1]`
            if (nums[i] < nums[j]) {
                T[i][1] = max(T[i][1], T[j][0] + 1);
            }
        }

        if (result < max(T[i][0], T[i][1])) {
            result = max(T[i][0], T[i][1]);
        }
    }

    return result;
    }
};

int main()
{
    cout << Solution().findLongestAlternatingSequence({8, 9, 6, 4, 5, 7, 3, 2, 4}) << endl;

    return 0;
}
