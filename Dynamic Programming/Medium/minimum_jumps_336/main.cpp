/*

Given an array of non-negative integers, where each array element represents the maximum number of positions one can move forward from that element. Find the minimum number of jumps required to reach end of the array from start of the array.

Input: [4, 2, 0, 3, 2, 0, 1, 8]
Output: 3
Explanation: The minimum jumps required to reach the destination are 3.

3 jumps: (4 —> 3 —> 1 —> 8) or (4 —> 2 —> 1 —> 8)
4 jumps: (4 —> 2 —> 3 —> 1 —> 8) or (4 —> 3 —> 2 —> 1 —> 8)
5 jumps: (4 —> 2 —> 3 —> 2 —> 1 —> 8)

Note that if any element has value 0 in the array, the destination cannot be reached through that element. The solution should return -1 as the destination cannot be reached.

Input: [4, 2, 2, 1, 0, 8, 1]
Output: -1
Explanation: The minimum jumps required to reach the destination are infinity. This is because no matter what path we choose, it will always end up in a dead cell.

4 —> 2 —> 2 —> 1 —> 0
4 —> 2 —> 1 —> 0
4 —> 1 —> 0
4 —> 0

Input: [0, 2, 2, 1, 8, 1]
Output: -1
Explanation: The destination cannot be reached at all since the source itself has value 0.

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Solution
{
private:
    int findMinJumps(vector<int> const &nums, int i, int n, vector<int>& lookup)
    {
        if (i == n - 1) {
            return 0;
        }

        if (i >= n || nums[i] == 0) {
            return INT_MAX;
        }

        if (lookup[i] != 0) {
            return lookup[i];
        }

        int min_jumps = INT_MAX;
        for (int j = i + 1; j <= i + nums[i]; ++j)
        {
            int cost = findMinJumps(nums, j, n, lookup);
            if (cost != INT_MAX) {
                min_jumps = min(min_jumps, cost + 1);
            }
        }

        return lookup[i] = min_jumps;
    }
public:
    int findMinimumJumps(vector<int> const &nums)
    {
        /*int n = nums.size();
        if (n == 0) {
            return 0;
        }

        vector<int> lookup(n);
        int jumps = findMinJumps(nums, 0, n, lookup);
        return jumps != INT_MAX ? jumps : -1;*/
        int n = nums.size();
        if (n == 0) {
            return 0;
        }

        if (n > 1 && nums[0] == 0) {
            //return INT_MAX;
            return -1;
        }

        int lookup[n];
        for (int i = 0; i < n; i++) {
            //lookup[i] = INT_MAX;
            lookup[i] = -1;
        }

        lookup[0] = 0;

        for (int i = 0; i < n; ++i)
        {
            //for (int j = 1; (i + j < n) && j <= min(n - 1, nums[i]) && lookup[i] != INT_MAX; ++j)
            for (int j = 1; (i + j < n) && j <= min(n - 1, nums[i]) && lookup[i] != -1; ++j)
            {
                //lookup[i + j] = (lookup[i + j] != INT_MAX) ? min(lookup[i + j], lookup[i] + 1): (lookup[i] + 1);
                lookup[i + j] = (lookup[i + j] != -1) ? min(lookup[i + j], lookup[i] + 1): (lookup[i] + 1);
            }
        }

        return lookup[n - 1];
    }
};


int main()
{
    Solution s;

    cout << s.findMinimumJumps({4, 2, 0, 3, 2, 0, 1, 8}) << endl;
    cout << s.findMinimumJumps({4, 2, 2, 1, 0, 8, 1}) << endl;
    cout << s.findMinimumJumps({0, 2, 2, 1, 8, 1}) << endl;

    return 0;
}
