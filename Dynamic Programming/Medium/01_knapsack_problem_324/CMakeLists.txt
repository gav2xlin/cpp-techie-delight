cmake_minimum_required(VERSION 3.5)

project(01_knapsack_problem_324 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(01_knapsack_problem_324 main.cpp)

install(TARGETS 01_knapsack_problem_324
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
