/*

Given a set of items, each with a weight and a value, determine the number of each item to include in a collection so that the total weight is less than or equal to a given limit and the total value is as large as possible. Note that the items are indivisible; we can either take an item or not (0-1 property).

Input:

value = [20, 5, 10, 40, 15, 25]
weight = [1, 2, 3, 8, 7, 4]
int W = 10

Output: 60

Explanation: Knapsack value is 60

value = 20 + 40 = 60
weight = 1 + 8 = 9 <= W

*/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <climits>
#include <string>
#include <algorithm>
#include <cstring>

using namespace std;

class Solution
{
private:
    int knapsack(vector<int> const &v, vector<int> const &w, int n, int W, auto &lookup)
    {
        if (W < 0) {
            return INT_MIN;
        }

        if (n < 0 || W == 0) {
            return 0;
        }

        string key = to_string(n) + "|" + to_string(W);

        if (lookup.find(key) == lookup.end())
        {
            // Case 1. Include current item `v[n]` in the knapsack and recur for
            // remaining items `n-1` with decreased capacity `W-w[n]`
            int include = v[n] + knapsack(v, w, n - 1, W - w[n], lookup);

            // Case 2. Exclude current item `v[n]` from the knapsack and recur for
            // remaining items `n-1`
            int exclude = knapsack(v, w, n - 1, W, lookup);

            lookup[key] = max(include, exclude);
        }

        return lookup[key];
    }
public:
    /*int findKnapsackValue(vector<int> const &value, vector<int> const &weight, int W)
    {
        int n = value.size();

        unordered_map<string, int> lookup;
        return knapsack(value, weight, n - 1, W, lookup);
    }*/
    int findKnapsackValue(vector<int> const &value, vector<int> const &weight, int W)
    {
        int n = value.size();

        int T[n+1][W+1];

        for (int j = 0; j <= W; ++j) {
            T[0][j] = 0;
        }

        // memset (T[0], 0, sizeof T[0]);

        for (int i = 1; i <= n; ++i)
        {
            for (int j = 0; j <= W; ++j)
            {
                // don't include the i'th element if `j-w[i-1]` is negative
                if (weight[i - 1] > j) {
                    T[i][j] = T[i - 1][j];
                } else {
                    // find the maximum value by excluding or including the i'th item
                    T[i][j] = max(T[i - 1][j], T[i - 1][j - weight[i - 1]] + value[i - 1]);
                }
            }
        }

        return T[n][W];
    }
};

int main()
{
    vector<int> value {20, 5, 10, 40, 15, 25};
    vector<int> weight {1, 2, 3, 8, 7, 4};
    int W = 10;

    cout << Solution().findKnapsackValue(value, weight, W) << endl;

    return 0;
}
