cmake_minimum_required(VERSION 3.5)

project(dice_throw_343 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(dice_throw_343 main.cpp)

install(TARGETS dice_throw_343
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
