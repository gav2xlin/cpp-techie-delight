/*

Given two positive numbers n, k and a target, calculate the total number of ways to reach `target` with `n` throws of dice having `k` faces.

Input:

n = 2			(total number of throws)
k = 6 			(each dice has values from 1 to 6)
target = 10		(desired sum)

Output: 3

Explanation: The total number of ways is 3. The possible throws are (6, 4), (4, 6), (5, 5)

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int countWays(int n, int k, int target, auto &lookup)
    {
        if (n == 0) {
            return (target == 0) ? 1 : 0;
        }

        if (target < 0 || k * n < target || n > target) {
            return 0;
        }

        if (lookup[n][target] == 0)
        {
            for (int i = 1; i <= k; ++i) {
                lookup[n][target] += countWays(n - 1, k, target - i, lookup);
            }
        }

        return lookup[n][target];
    }
public:
    int countWays(int n, int k, int target)
    {
        vector<vector<int>> lookup(n + 1, vector<int>(target + 1));
        return countWays(n, k, target, lookup);
    }
};

int main()
{
    cout << "Hello World!" << endl;

    return 0;
}
