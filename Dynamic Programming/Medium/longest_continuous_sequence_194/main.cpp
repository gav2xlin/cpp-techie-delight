/*

Given an `N × N` matrix where each cell has a distinct value in the 1 to `N × N`. Find the longest sequence formed by adjacent numbers in the matrix such that for each number, the number on the adjacent neighbor is +1 in its value.

From the location (x, y) in the matrix, you can move to (x, y+1), (x, y-1), (x+1, y), or (x-1, y) if the value at the destination cell is one more than the value at source cell.

Input:

[
    [10, 13, 14, 21, 23],
    [11, 9,  22, 2,  3],
    [12, 8,  1,  5,  4],
    [15, 24, 7,  6,  20],
    [16, 17, 18, 19, 25]
]

Output: [2, 3, 4, 5, 6, 7]

Note: Assume that the input contains a single longest sequence.

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    bool isValid(int i, int j, int N) {
        return (i >= 0 && i < N && j >= 0 && j < N);
    }

    vector<int> findLongestPath(vector<vector<int>> const &mat, int i, int j)
    {
        vector<int> path;

        int N = mat.size();

        if (!isValid (i, j, N)) {
            return path;
        }

        if (i > 0 && mat[i - 1][j] - mat[i][j] == 1) {
            path = findLongestPath(mat, i - 1, j);
        }

        if (j + 1 < N && mat[i][j + 1] - mat[i][j] == 1) {
            path = findLongestPath(mat, i, j + 1);
        }

        if (i + 1 < N && mat[i + 1][j] - mat[i][j] == 1) {
            path = findLongestPath(mat, i + 1, j);
        }

        if (j > 0 && mat[i][j - 1] - mat[i][j] == 1) {
            path = findLongestPath(mat, i, j - 1);
        }

        path.push_back(mat[i][j]);
        return path;
    }
public:
    vector<int> findLongestPath(vector<vector<int>> const &mat)
    {
        vector<int> longest_path;

        for (int i = 0; i < mat.size(); ++i)
        {
            for (int j = 0; j < mat.size(); ++j)
            {
                vector<int> path = findLongestPath(mat, i, j);

                if (path.size() > longest_path.size()) {
                    longest_path = path;
                }
            }
        }

        reverse(longest_path.begin(), longest_path.end());
        return longest_path;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<vector<int>> mat{
        {10, 13, 14, 21, 23},
        {11, 9, 22, 2, 3},
        {12, 8, 1, 5, 4},
        {15, 24, 7, 6, 20},
        {16, 17, 18, 19, 25}
    };

    cout << Solution().findLongestPath(mat) << endl;

    return 0;
}
