/*

Given a positive number `n` and a mobile keypad having digits from 0 to 9 associated with each key, count the total possible combinations of digits having length `n`. You can start with any digit and press only four adjacent keys of any digit. The keypad also contains `*` and `#` keys, which you're not allowed to press.

keypad = {
    { '1', '2', '3' },
    { '4', '5', '6' },
    { '7', '8', '9' },
    { '*', '0', '#' }
};

Input: n = 2
Output: 36
Explanation: Total possible combinations are 36 [00, 08, 11, 12, 14, 21, 22, 23, 25, 32, 33, 36, 41, 44, 45, 47, … ,96, 98, 99]

*/

#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Solution
{
private:
    const int row[4] = {0, -1, 0, 1};
    const int col[4] = {-1, 0, 1, 0};

    multimap<int, int> fillMap(vector<vector<char>> const &keypad)
    {
        multimap<int, int> mapping;

        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                for (int k = 0; k < 4; ++k)
                {
                    int r = i + row[k];
                    int c = j + col[k];

                    if (isValid(i, j) && isValid(r, c)) {
                        mapping.insert(make_pair(keypad[i][j] - '0', keypad[r][c] - '0'));
                    }
                }
            }
        }

        return mapping;
    }

    bool isValid(int i, int j)
    {
        // for handling `*` or `#` (present in 4th row and 1st & 3rd column)
        if (i == 3 && (j == 0 || j == 2)) {
            return false;
        }

        return (i >= 0 && i <= 3 && j >= 0 && j <= 2);
    }

    int getCount(multimap<int, int> mapping, int i, int n, auto &lookup)
    {
        if (n == 1) {
            return 1;
        }

        if (lookup[i][n] == 0)
        {
            lookup[i][n] = getCount(mapping, i, n - 1, lookup);

            for (auto it = mapping.find(i); it != mapping.end() && it->first == i; ++it) {
                lookup[i][n] += getCount(mapping, it->second, n - 1, lookup);
            }
        }

        return lookup[i][n];
    }
public:
    int findCombinations(vector<vector<char>> const &keypad, int n) {
        multimap<int, int> mapping = fillMap(keypad);

        vector<vector<int>> lookup(10, vector<int>(n + 1));

        int count = 0;
        for (int i = 0; i <= 9; i++) {
            count += getCount(mapping, i, n, lookup);
        }

        return count;
    }
};

int main()
{
    vector<vector<char>> keypad {
        {'1', '2', '3'},
        {'4', '5', '6'},
        {'7', '8', '9'},
        {'*', '0', '#'}
    };
    cout << Solution().findCombinations(keypad, 2) << endl;

    return 0;
}
