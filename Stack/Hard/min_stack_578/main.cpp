/*

Design a stack to support an additional operation that returns the minimum element from the stack in constant time. The stack should continue supporting all other operations like push, pop, top, size, isEmpty, etc., with no degradation in these operations' performance.

You're given an empty MinStack class, with following functions:

• void push(int val): Inserts element 'val' on top of the stack
• int pop(): Removes and return the top element from the stack
• int top(): Returns the top element of the stack
• int getMin(): Returns the minimum element from the stack in constant time
• int size(): Returns the total number of elements in the stack
• bool isEmpty(): Returns true if the stack is empty; false otherwise

You may assume that pop, top, and getMin operations will never be called on an empty stack.

For example,

Input: [push(6), getMin(), push(5), getMin(), push(7), getMin(), isEmpty(), size(), top(), pop(), getMin(), top(), pop(), getMin(), top(), pop(), isEmpty(), size()]

Output: [null, 6, null, 5, null, 5, false, 3, 7, 7, 5, 5, 5, 6, 6, 6, true, 0]

Explanation:

MinStack s
s.push(6)
s.getMin() -> 6
s.push(5)
s.getMin() -> 5
s.push(7)
s.getMin() -> 5
s.isEmpty() -> false
s.size() -> 3
s.top() -> 7
s.pop() -> 7
s.getMin() -> 5
s.top() -> 5
s.pop() -> 5
s.getMin() -> 6
s.top() -> 6
s.pop() -> 6
s.isEmpty() -> true
s.size() -> 0

Input: [isEmpty(), push(6), getMin(), push(7), getMin(), push(8), getMin(), push(5), getMin(), push(3), getMin(), top(), pop(), getMin(), push(10), getMin(), top(), pop(), getMin(), top(), pop(), getMin(), isEmpty(), size()]

Output: [true, null, 6, null, 6, null, 6, null, 5, null, 3, 3, 3, 5, null, 5, 10, 10, 5, 5, 5, 6, false, 3]

*/

#include <iostream>
#include <stack>

using namespace std;

class MinStack
{
    stack<int> s;
    stack<int> aux;

public:
    void push(int val)
    {
        s.push(val);

        if (aux.empty()) {
            aux.push(val);
        }
        else
        {
            if (aux.top() >= val) {
                aux.push(val);
            }
        }
    }

    int pop()
    {
        int top = s.top();
        s.pop();

        if (top == aux.top()) {
            aux.pop();
        }

        return top;
    }

    int top() {
        return s.top();
    }

    int size() {
        return s.size();
    }

    bool isEmpty() {
        return s.empty();
    }

    int getMin()
    {
        return aux.top();
    }
};

int main()
{
    MinStack s;

    s.push(6);
    cout << s.getMin() << endl;    // 6

    s.push(7);
    cout << s.getMin() << endl;    // 6

    s.push(8);
    cout << s.getMin() << endl;    // 6

    s.push(5);
    cout << s.getMin() << endl;    // 5

    s.push(3);
    cout << s.getMin() << endl;    // 3

    cout << s.pop() << endl;    // 3
    cout << s.getMin() << endl;    // 5

    s.push(10);
    cout << s.getMin() << endl;    // 5

    cout << s.pop() << endl;    // 10
    cout << s.getMin() << endl;    // 5

    cout << s.pop() << endl;    // 5
    cout << s.getMin() << endl;    // 6

    return 0;
}
