/*

Given a string consisting of opening and closing parenthesis, find the length of the longest balanced parenthesis in it.

Input: "((()()"
Output: 4
Explanation: The longest balanced parenthesis is "()()"

Input: "(()())(()"
Output: 6
Explanation: The longest balanced parenthesis is "(()())"

Input: "(((()"
Output: 2
Explanation: The longest balanced parenthesis is "()"

Input: "(((("
Output: 0
Explanation: The longest balanced parenthesis is ""

Input: "()()"
Output: 4
Explanation: The longest balanced parenthesis is "()()"

*/

#include <iostream>
#include <stack>

using namespace std;

class Solution
{
public:
    int findLength(string str)
    {
        stack<int> stack({-1});

        int len = 0;

        for (int i = 0; i < str.length(); ++i)
        {
            if (str[i] == '(') {
                stack.push(i);
            }
            else
            {
                stack.pop();
                if (stack.empty())
                {
                    stack.push(i);
                    continue;
                }

                int curr_len = i - stack.top();
                if (len < curr_len) {
                    len = curr_len;
                }
            }
        }

        return len;
    }
};

int main()
{
    Solution s;
    cout << s.findLength("((()()") << endl;
    cout << s.findLength("(()())(()") << endl;
    cout << s.findLength("(((()") << endl;
    cout << s.findLength("((((") << endl;
    cout << s.findLength("()()") << endl;

    return 0;
}
