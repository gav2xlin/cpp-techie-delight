/*

Given a sequence consisting of 'I' and 'D', where 'I' denotes the increasing sequence and 'D' denotes the decreasing sequence, decode the sequence to construct a minimum number without repeated digits.

Input: "IIDDIDID"
Output: "125437698"

Input: "IDIDII"
Output: "1325467"

Input: "DDDDDDDD"
Output: "987654321"

Input: "IIIIIIII"
Output: "123456789"

Assume sequence length <= 8.

*/

#include <iostream>
#include <stack>

using namespace std;

class Solution
{
public:
    string decode(string seq)
    {
        string result;
        stack<int> s;

        for (int i = 0; i <= seq.size(); ++i)
        {
            s.push(i + 1);

            if (i == seq.length() || seq[i] == 'I')
            {
                while (!s.empty())
                {
                    result += to_string(s.top());
                    s.pop();
                }
            }
        }

        return result;
    }
};

int main()
{
    cout << Solution().decode("IIDDIDID") << endl;
    cout << Solution().decode("IDIDII") << endl;
    cout << Solution().decode("DDDDDDDD") << endl;
    cout << Solution().decode("IIIIIIII") << endl;

    return 0;
}
