/*

Given a circular integer array, find the next greater element for every element in it. The next greater element of an element `x` in the array is the first larger number to the next of `x`.

In a circular array, the indices will wrap around as if it were connected end-to-end. In other words, the end of the array wraps around to the start of the array. This facilitate searching circularly to find the next greater number. If the next greater element doesn't exist, consider it to be -1.

Input : [3, 5, 2, 4]
Output: [5, -1, 4, 5]

Input : [7, 5, 3, 4]
Output: [-1, 7, 4, 7]

*/

#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;

class Solution
{
public:
    vector<int> findNextGreaterElements(vector<int> const &nums)
    {
        /*int n = nums.size();

        vector<int> out(n, -1);
        stack<int> s;

        for (int i = 0; i < 2 * n; ++i)
        {
            while (!s.empty() && nums[s.top()] < nums[i % n])
            {
                out[s.top()] = nums[i % n];
                s.pop();
            }
            s.push(i % n);
        }

        return out;*/
        int n = nums.size();

        vector<int> out(n, -1);
        stack<int> s;

        for (int i = 2 * n - 1; i >= 0; --i)
        {
            while (!s.empty())
            {
                if (s.top() <= nums[i % n]) {
                    s.pop();
                }
                else {
                    out[i % n] = s.top();
                    break;
                }
            }
            s.push(nums[i % n]);
        }

        return out;
    }
};

ostream& operator<<(ostream& os, const vector<int>& values) {
    for_each(values.begin(), values.end(), [&os](auto &v) {
        os << v << " ";
    });
    return os;
}

int main()
{
    cout << Solution().findNextGreaterElements({3, 5, 2, 4}) << endl;
    cout << Solution().findNextGreaterElements({7, 5, 3, 4}) << endl;

    return 0;
}
