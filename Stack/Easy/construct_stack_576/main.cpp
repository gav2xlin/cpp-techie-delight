/*

You're given an empty Stack class. Write code to support standard stack operations like push, pop, peek, size, and isEmpty.

• void push(int val): Inserts element 'val' on top of the stack
• int pop(): Removes and return the top element from the stack
• int peek(): Returns the top element of the stack
• int size(): Returns the total number of elements in the stack
• bool isEmpty(): Returns true if the stack is empty; false otherwise

For example,

Input: [push(6), push(5), push(7), isEmpty(), size(), peek(), pop(), peek(), pop(), peek(), pop(), isEmpty(), size()]
Output: [null, null, null, false, 3, 7, 7, 5, 5, 6, 6, true, 0]
Explanation:

Stack s;
s.push(6)
s.push(5)
s.push(7)
s.isEmpty() -> false
s.size() -> 3
s.peek() -> 7
s.pop() -> 7
s.peek() -> 5
s.pop() -> 5
s.peek() -> 6
s.pop() -> 6
s.isEmpty() -> true
s.size() -> 0


Input: [isEmpty(), push(6), push(7), push(8), push(5), push(3), peek(), pop(), push(10), peek(), pop(), peek(), pop(), isEmpty(), size()]
Output: [true, null, null, null, null, null, 3, 3, null, 10, 10, 5, 5, false, 3]

Constraints:

• The pop and peek operations will never be called on an empty stack.
• The maximum capacity of the stack is 1000.

*/

#include <iostream>
#include <stdexcept>

using namespace std;

#define MAX_SIZE 1000

class Stack
{
private:
    int arr[MAX_SIZE];
    int cap{0}, top{-1};
public:
    Stack() {
    }

    ~Stack() {
    }

    void push(int val) {
        if (isFull()) {
            throw range_error("stack is full");
        }

        arr[++top] = val;
    }

    int pop() {
        if (isEmpty()) {
            throw range_error("stack is empty");
        }

        return arr[top--];
    }

    int peek() {
        if (top < 0) {
            throw range_error("stack is empty");
        }
        return arr[top];
    }

    int size() {
        return top + 1;
    }

    bool isEmpty() {
        return top < 0;
    }

    bool isFull() {
        return top + 1 == MAX_SIZE;
    }
};

int main()
{
    /*{
        Stack s;

        s.push(6);
        s.push(5);
        s.push(7);
        cout << boolalpha << s.isEmpty() << '\n'; // false
        cout << s.size() << '\n'; // 3
        cout << s.peek() << '\n'; // 7
        cout << s.pop() << '\n'; // 7
        cout << s.peek() << '\n'; // 5
        cout << s.pop() << '\n'; // 5
        cout << s.peek() << '\n'; // 6
        cout << s.pop() << '\n'; // 6
        cout << s.isEmpty() << '\n'; // true
        cout << s.size() << endl; // 0
    }*/
    {
        Stack s;

        cout << boolalpha << s.isEmpty() << '\n';
        s.push(6); // 6
        s.push(7); // 6, 7
        s.push(8); // 6, 7, 8
        s.push(5); // 6, 7, 8, 5
        s.push(3); // 6, 7, 8, 5, 3
        cout << s.peek() << '\n'; // 3
        cout << s.pop() << '\n'; // 3
        s.push(10); // // 6, 7, 8, 10
        cout << s.peek() << '\n'; // 10
        cout << s.pop() << '\n'; // 10
        cout << s.peek() << endl; // 5
    }

    return 0;
}
