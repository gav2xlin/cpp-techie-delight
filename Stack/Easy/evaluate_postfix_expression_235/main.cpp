/*

Given a postfix expression, efficiently evaluate it. Assume that the postfix expression contains only single-digit numeric operands, without any whitespace.

Input: "82/"
Output: 4
Explanation: 82/ will evaluate to 4 (8/2)

Input: "138*+"
Output: 4
Explanation: 138*+ will evaluate to 25 (1+8*3)

Input: "545*+5/"
Output: 5
Explanation: 545*+5/ will evaluate to 5 ((5+4*5)/5)


Assume valid postfix expression.

*/

#include <iostream>
#include <string>
#include <stack>

using namespace std;

class Solution
{
private:
    bool isOperation(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }
public:
    int evalPostfix(string s)
    {
        stack<string> exp;

        for (char ch : s) {
            if (isOperation(ch)) {
                int operand2 = stoi(exp.top());
                exp.pop();

                int operand1 = stoi(exp.top());
                exp.pop();

                switch (ch) {
                case '+':
                    exp.push(to_string(operand1 + operand2));
                    break;
                case '-':
                    exp.push(to_string(operand1 - operand2));
                    break;
                case '*':
                    exp.push(to_string(operand1 * operand2));
                    break;
                case '/':
                    exp.push(to_string(operand1 / operand2));
                    break;
                }
            } else {
                exp.push(string(1, ch));
            }
        }

        return stoi(exp.top());
    }
};

int main()
{
    cout << Solution().evalPostfix("82/") << endl;
    cout << Solution().evalPostfix("138*+") << endl;
    cout << Solution().evalPostfix("545*+5/") << endl;

    return 0;
}
