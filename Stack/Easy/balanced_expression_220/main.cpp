/*

Given a string containing opening and closing braces, determine if it represents a balanced expression or not.

Input: "{[{}{}]}[()]"
Output: true

Input: "{{}{}}"
Output: true

Input: "[]{}()"
Output: true

Input: "{()}[)"
Output: false

Input: "{(})"
Output: false

*/

#include <iostream>
#include <string>
#include <stack>

using namespace std;

class Solution
{
public:
    bool isBalanced(string s)
    {
        stack<char> st;

        /*for (auto ch : s) {
            if (!st.empty()) {
                char prev = st.top();

                if ((prev == '(' && ch == ')') || (prev == '[' && ch == ']') || (prev == '{' && ch == '}')) {
                    st.pop();
                } else {
                    st.push(ch);
                }
            } else {
                st.push(ch);
            }
        }

        return st.empty();*/

        for (auto ch : s) {
            if (!st.empty()) {
                if (ch == ')' || ch == ']' || ch == '}') {
                    char prev = st.top();
                    if ((prev == '(' && ch == ')') || (prev == '[' && ch == ']') || (prev == '{' && ch == '}')) {
                        st.pop();
                    } else {
                        return false;
                    }
                } else {
                    st.push(ch);
                }
            } else {
                st.push(ch);
            }
        }

        return st.empty();
    }
};

int main()
{
    cout << boolalpha << Solution().isBalanced("{[{}{}]}[()]") << endl;
    cout << Solution().isBalanced("{{}{}}") << endl;
    cout << Solution().isBalanced("[]{}()") << endl;
    cout << Solution().isBalanced("{()}[)") << endl;
    cout << Solution().isBalanced("{(})") << endl;

    cout << Solution().isBalanced("{}][") << endl;

    return 0;
}
