/*

Given the root of a binary tree, convert the binary tree into its mirror.

Input:
           1
         /   \
        /	  \
       2	   3
      / \	  / \
     /	 \	 /	 \
    4	  5	6	  7

Output:

           1
         /   \
        /	  \
       3	   2
      / \	  / \
     /	 \	 /	 \
    7	  6	5	  4

Explanation:
                        |
                        |
           1			|		   1
         /   \			|		 /   \
        /	  \			|		/	  \
       2	   3		|	   3	   2
      / \	  / \		|	  / \	  / \
     /	 \	 /	 \		|	 /	 \	 /	 \
    4	  5	6	  7		|	7	  6	5	  4
                        |
                        |
*/

#include <iostream>
#include <utility>
#include <stack>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void convertToMirror(Node* root)
    {
        if (root == nullptr) return;

        /*convertToMirror(root->left);
        convertToMirror(root->right);

        swap(root->left, root->right);*/
        stack<Node*> st;
        st.push(root);

        while (!st.empty()) {
            Node* node = st.top();
            st.pop();

            swap(node->left, node->right);

            if (node->left != nullptr) {
                st.push(node->left);
            }
            if (node->right != nullptr) {
                st.push(node->right);
            }
        }
    }
};

ostream& operator<<(ostream& os, const Node* root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(root, 0);

    return os;
}

int main()
{
    Node n4{4};
    Node n5{5};
    Node n2{2, &n4, &n5};
    Node n6{6};
    Node n7{7};
    Node n3{3, &n6, &n7};
    Node n1{1, &n2, &n3};

    cout << &n1 << endl;
    Solution().convertToMirror(&n1);
    cout << &n1 << endl;

    return 0;
}
