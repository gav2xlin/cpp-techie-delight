/*

Given an integer array, inplace reverse it without using any inbuilt functions.

Input : [1, 2, 3, 4, 5]
Output: [5, 4, 3, 2, 1]

*/

#include <iostream>
#include <vector>
#include <utility>

using namespace std;

class Solution
{
private:
    void reverseArrayInternal(vector<int> &nums, int from, int to) {
        if (from < to) {
            swap(nums[from], nums[to]);

            reverseArrayInternal(nums, from + 1, to - 1);
        }
    }
public:
    void reverseArray(vector<int> &nums)
    {
        if (nums.size() == 0) return;

        reverseArrayInternal(nums, 0, nums.size() - 1);
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{1, 2, 3, 4, 5};

    cout << nums << endl;
    Solution().reverseArray(nums);
    cout << nums << endl;

    return 0;
}
