/*

Given the root of a binary tree and a tree node x, return values of all ancestors of x in the binary tree. Two nodes of a binary tree are cousins of each other if they have different parents, but they are at the same level.

For example, consider the following binary tree.

             1
           /   \
         /		 \
        2		  3
      /  \		 /  \
     /	  \		/	 \
    4	   5   6	  7
              /		   \
             /			\
            8			 9

Input: x = Node 9
Output: [7, 3, 1]

Input: x = Node 6
Output: [3, 1]

Input: x = Node 5
Output: [2, 1]

The returned nodes should follow the node-to-root order. The solution should return an empty list if x is not the actual node in the tree.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool findAnscestorsInternal(Node* root, Node* node, vector<int>& res) {
        if (root == nullptr) {
            return false;
        }

        if (root == node) {
            return true;
        }

        bool left = findAnscestorsInternal(root->left, node, res);

        bool right = false;
        if (!left) {
            right = findAnscestorsInternal(root->right, node, res);
        }

        if (left || right) {
            res.push_back(root->data);
        }

        return left || right;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    vector<int> findAncestors(Node* root, Node* node)
    {
        vector<int> res;
        findAnscestorsInternal(root, node, res);
        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    Node* root = new Node(1);
    root->left = new Node(2); // 1 -> 2
    root->right = new Node(3); // 1 -> 3
    root->left->left = new Node(4); // 1 - > 2 -> 4
    root->left->right = new Node(5); // 1 -> 2 -> 5
    root->right->left = new Node(6); // 1 - > 3 -> 6
    root->right->right = new Node(7); // 1- > 3 -> 7
    root->right->left->left = new Node(8); // 1 - > 3 -> 6 -> 8
    root->right->right->right = new Node(9); // 1 - > 3 -> 7 -> 9

    cout << Solution().findAncestors(root, root->right->right->right) << endl;
    cout << Solution().findAncestors(root, root->right->left) << endl;
    cout << Solution().findAncestors(root, root->left->right) << endl;

    return 0;
}
