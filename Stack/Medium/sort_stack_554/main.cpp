/*

Given a stack, in-place sort it without using any inbuilt functions or data structures.

Input : stack[5, -2, 9, -7, 3], where 3 is the top element
Output: stack[-7, -2, 3, 5, 9], where 9 is the top element

*/

#include <iostream>
#include <deque>
#include <stack>

using namespace std;

class Solution
{
private:
    void insertSortedStack(stack<int> &s, int item) // O(n^2)
    {
        if (s.empty() || item > s.top()) {
            s.push(item);
        } else {
            int top = s.top();
            s.pop();
            insertSortedStack(s, item);
            s.push(top);
        }
    }
public:
    void sortStack(stack<int> &s) // O(n)
    {
        if (!s.empty()) {
            int top = s.top();
            s.pop();
            sortStack(s);

            insertSortedStack(s, top);
        }
    }
};

ostream& operator<<(ostream& os, const stack<int>& st) {
    for (std::stack<int> dump = st; !dump.empty(); dump.pop()) {
           os << dump.top() << ' ';
    }
    return os;
}

int main()
{
    deque<int> nums{5, -2, 9, -7, 3};
    stack<int> s(nums);

    cout << "before: " << s << endl;

    Solution().sortStack(s);

    cout << "after: " << s << endl;

    return 0;
}
