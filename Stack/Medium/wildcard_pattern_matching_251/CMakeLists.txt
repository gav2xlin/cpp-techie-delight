cmake_minimum_required(VERSION 3.5)

project(wildcard_pattern_matching_251 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(wildcard_pattern_matching_251 main.cpp)

install(TARGETS wildcard_pattern_matching_251
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
