/*

Given a binary pattern containing '?' wildcard character at a few positions, return all possible combinations of binary strings that can be formed by replacing the wildcard character by either '0' or '1'.

Input: "1?11?00?1?"
Output: {"1011000010", "1011000011", "1011000110", "1011000111", "1011100010", "1011100011", "1011100110", "1011100111", "1111000010", "1111000011", "1111000110", "1111000111", "1111100010", "1111100011", "1111100110", "1111100111"}

*/

#include <iostream>
#include <string>
#include <stack>
#include <unordered_set>
#include <cstddef>

using namespace std;

class Solution
{
public:
    unordered_set<string> findCombinations(string pattern)
    {
        stack<string> st;
        st.push(pattern);

        unordered_set<string> nums;
        while (!st.empty())
        {
            string curr = st.top();
            st.pop();

            size_t index;
            if ((index = curr.find_first_of('?')) != string::npos)
            {
                for (int i = 0; i < 2; i++)
                {
                    curr[index] = i + '0';
                    st.push(curr);
                }
            } else {
                nums.insert(curr);
            }
        }

        return nums;
    }
};

ostream& operator<<(ostream& os, const unordered_set<string>& values) {
    for (auto& v : values) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findCombinations("1?11?00?1?") << endl;

    return 0;
}
