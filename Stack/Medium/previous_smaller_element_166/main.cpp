/*

Given an integer array, find the previous smaller element for every array element. The previous smaller element of a number `x` is the first smaller number to the left of `x` in the array.

In other words, for each element A[i] in the array A, find an element A[j] such that j < i and A[j] < A[i] and value of j should be as maximum as possible. If the previous smaller element doesn't in the array for any element, consider it -1.

Input : [2, 5, 3, 7, 8, 1, 9]
Output: [-1, 2, 2, 3, 7, -1, 1]

Input : [5, 7, 4, 9, 8, 10]
Output: [-1, 5, -1, 4, 4, 8]

Note that the first element always has a previous smaller element as -1.

*/

#include <iostream>
#include <vector>
#include <stack>

using namespace std;

class Solution
{
public:
    vector<int> findNextGreaterElements(vector<int> const &nums)
    {
        vector<int> res;
        res.reserve(nums.size());

        stack<int> s;

        for (int i = 0; i < nums.size(); ++i)
        {
            while (!s.empty())
            {
                if (s.top() < nums[i])
                {
                    res.push_back(s.top());
                    break;
                } else {
                    s.pop();
                }
            }

            if (s.empty()) {
                res.push_back(-1);
            }

            s.push(nums[i]);
        }

        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().findNextGreaterElements({2, 5, 3, 7, 8, 1, 9}) << endl;
    cout << Solution().findNextGreaterElements({5, 7, 4, 9, 8, 10}) << endl;

    return 0;
}
