/*

Given a stack, in-place reverse it without using only its abstract data type (ADT) standard operations, i.e., push(), pop(), top(), size(), etc.

Input : stack [1, 2, 3, 4, 5], where 5 is the top element
Output: stack [5, 4, 3, 2, 1], where 1 is the top element

*/

#include <iostream>
#include <deque>
#include <stack>

using namespace std;

class Solution
{
private:
    void insertStack(stack<int> &s, int item) // O(n^2)
    {
        if (s.empty()) {
            s.push(item);
        } else {
            int top = s.top();
            s.pop();
            insertStack(s, item);
            s.push(top);
        }
    }
public:
    void reverseStack(stack<int> &s) // O(n)
    {
        if (!s.empty()) {
            int top = s.top();
            s.pop();
            reverseStack(s);

            insertStack(s, top);
        }
    }
};

ostream& operator<<(ostream& os, const stack<int>& st) {
    for (std::stack<int> dump = st; !dump.empty(); dump.pop()) {
           os << dump.top() << ' ';
    }
    return os;
}

int main()
{
    deque<int> nums{1, 2, 3, 4, 5};
    stack<int> s(nums);

    cout << "before: " << s << endl;

    Solution().reverseStack(s);

    cout << "after: " << s << endl;

    return 0;
}
