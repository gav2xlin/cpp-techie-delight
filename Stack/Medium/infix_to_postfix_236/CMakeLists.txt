cmake_minimum_required(VERSION 3.5)

project(infix_to_postfix_236 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(infix_to_postfix_236 main.cpp)

install(TARGETS infix_to_postfix_236
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
