/*

Given an infix expression, convert it to the postfix expression. Assume that the infix expression is a string of tokens without any whitespace.

Input: "A*B+C"
Output: "AB*C+"

Input: "(A+B)*(C/D)"
Output: "AB+CD/*"

Input: "A*(B*C+D*E)+F"
Output: "ABC*DE*+*F+"

Input: "(A+B)*C+(D-E)/F+G"
Output: "AB+C*DE-F/+G+"

*/

#include <iostream>
#include <string>
#include <stack>
#include <limits>

using namespace std;

class Solution
{
private:
    int prec(char c)
    {
        switch (c) {
        case '*':
        case '/':
            return 3;
        case '+':
        case '-':
            return 4;
        case '&':
            return 8;
        case '^':
            return 9;
        case '|':
            return 10;
        default:
            return numeric_limits<int>::max();  // '('
        }
    }

    bool isOperand(char c) {
        return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || ('0' <= c && c <= '9');
    }
public:
    string infixToPostfix(string infix)
    {
        stack<char> s;
        string postfix;

        for (char c: infix)
        {
            if (c == '(') {
                s.push(c);
            } else if (c == ')') {
                while (s.top() != '(')
                {
                    postfix.push_back(s.top());
                    s.pop();
                }
                s.pop();
            } else if (isOperand(c)) {
                postfix.push_back(c);
            } else {
                while (!s.empty() && prec(c) >= prec(s.top()))
                {
                    postfix.push_back(s.top());
                    s.pop();
                }

                s.push(c);
            }
        }

        while (!s.empty())
        {
            postfix.push_back(s.top());
            s.pop();
        }

        return postfix;
    }
};

int main()
{
    cout << Solution().infixToPostfix("A*B+C") << endl;
    cout << Solution().infixToPostfix("(A+B)*(C/D)") << endl;
    cout << Solution().infixToPostfix("A*(B*C+D*E)+F") << endl;
    cout << Solution().infixToPostfix("(A+B)*C+(D-E)/F+G") << endl;

    return 0;
}
