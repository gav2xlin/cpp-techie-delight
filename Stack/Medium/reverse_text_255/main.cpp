/*

Given a line of text, reverse the text without reversing the individual words.

Input : "Technical Interview Preparation"
Output: "Preparation Interview Technical"

*/

#include <iostream>
#include <stack>
#include <sstream>

using namespace std;

class Solution
{
public:
    void reverseText(string &s)
    {
        stack<string> st;

        istringstream is{s};
        string w;
        while (is >> w) {
            st.push(w);
        }

        ostringstream os;
        while (!st.empty()) {
            if (os.tellp()) os << ' ';
            os << st.top();
            st.pop();
        }
        s = os.str();
    }
};

int main()
{
    string s{"Technical Interview Preparation"};
    cout << "before: " << s << endl;

    Solution().reverseText(s);

    cout << "after: " << s << endl;

    return 0;
}
