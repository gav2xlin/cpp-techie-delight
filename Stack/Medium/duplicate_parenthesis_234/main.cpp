/*

Given a balanced expression that can contain opening and closing parenthesis, check if it contains any duplicate parenthesis or not.

Input: "((x+y))+z"
Output: true
Explanation: Duplicate () found in subexpression ((x+y))

Input: "(x+y)"
Output: false
Explanation: No duplicate () is found

Input: "((x+y)+((z)))"
Output: true
Explanation: Duplicate () found in subexpression ((z))

*/

#include <iostream>
#include <string>
#include <stack>

using namespace std;

class Solution
{
public:
    bool hasDuplicateParenthesis(string exp)
    {
        if (exp.length() <= 3) {
            return false;
        }

        stack<char> stack;

        for (char c: exp)
        {
            if (c != ')') {
                stack.push(c);
            } else {
                if (stack.top() == '(') {
                    return true;
                }

                while (stack.top() != '(') {
                    stack.pop();
                }

                stack.pop();
            }
        }

        return false;
    }
};

int main()
{
    Solution s;
    cout << boolalpha << endl;

    cout << s.hasDuplicateParenthesis("((x+y))+z") << endl;
    cout << s.hasDuplicateParenthesis("(x+y)") << endl;
    cout << s.hasDuplicateParenthesis("((x+y)+((z)))") << endl;

    return 0;
}
