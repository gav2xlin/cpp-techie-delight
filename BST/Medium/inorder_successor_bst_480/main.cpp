/*

Given the root of a binary search tree (BST) and a tree node x, find the inorder successor of x in the BST. An inorder successor of a tree node is the next node in the inorder traversal of the tree.

For example, consider the following tree:

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

Input: Node 10
Output: Node 12

Input: Node 12
Output: Node 15

• If the node does not lie in the BST, return the next greater node (if any) present in the BST.

Input: Node 5
Output: Node 8

• If the node does not lie in the BST and the next greater node also does not exist, the solution should return nullptr.

Input: Node 30
Output: nullptr

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node* findMinimum(Node* root)
    {
        while (root->left != nullptr) {
            root = root->left;
        }

        return root;
    }

    Node* findSuccessor(Node* root, Node* succ, int key)
    {
        if (root == nullptr) {
            return succ;
        }

        if (root->data == key)
        {
            if (root->right != nullptr) {
                return findMinimum(root->right);
            }
        } else if (key < root->data)
        {
            succ = root;
            return findSuccessor(root->left, succ, key);
        } else {
            return findSuccessor(root->right, succ, key);
        }

        return succ;
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* findInorderSuccessor(Node* root, Node* x)
    {
        return findSuccessor(root, nullptr, x->data);
    }
};

void preorder(ostream& os, const Node* root, string shift="")
{
    if (root == nullptr) {
        return;
    }

    os << shift << root->data << '\n';
    preorder(os, root->left, shift + "-");
    preorder(os, root->right, shift + "-");
}

ostream& operator<<(ostream& os, const Node* root) {
    preorder(os, root);
    return os;
}

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insert(root->left, key);
    } else {
        root->right = insert(root->right, key);
    }

    return root;
}

int main()
{
    int keys[] = {15, 10, 20, 8, 12, 16};

    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    cout << "tree:\n" << root << endl;

    Solution s;

    for (int key: keys)
    {
        Node n{key};
        Node* prec = s.findInorderSuccessor(root, &n);

        if (prec != nullptr) {
            cout << "The successor of node " << key << " is " << prec->data << endl;
        }
        else {
            cout << "The successor doesn't exist for " << key << endl;
        }
    }

    return 0;
}
