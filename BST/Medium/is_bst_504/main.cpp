/*

Given the root of a binary tree, check if it is a binary search tree (BST) or not.

Input:
          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

Output: true

Input:
           1
         /   \
        /	  \
       2	   3

Output: false

*/

#include <iostream>
#include <limits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool isBST(Node* root, int minKey, int maxKey)
    {
        if (root == nullptr) {
            return true;
        }

        if (root->data < minKey || root->data > maxKey) {
            return false;
        }

        return isBST(root->left, minKey, root->data) && isBST(root->right, root->data, maxKey);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool isBST(Node* root)
    {
        return isBST(root, numeric_limits<int>::min(), numeric_limits<int>::max());
    }
};

int main()
{
    {
        Node n8{8};
        Node n12{12};
        Node n10{10, &n8, &n12};
        Node n16{16};
        Node n25{25};
        Node n20{20, &n16, &n25};
        Node n15{15, &n10, &n20};
        cout << boolalpha << Solution().isBST(&n15) << endl;
    }

    {
        Node n2{2};
        Node n3{3};
        Node n1{1, &n2, &n3};
        cout << boolalpha << Solution().isBST(&n1) << endl;
    }

    return 0;
}
