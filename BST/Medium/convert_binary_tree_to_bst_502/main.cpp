/*

Given the root of a binary tree, convert the binary tree into a binary search tree (BST) by keeping its original structure intact.

Input:
           1
         /   \
        /	  \
       2	   3
      / \	  / \
     /	 \	 /	 \
    4	  5	6	  7

Output:
           4
         /   \
        /	  \
       2	   6
      / \	  / \
     /	 \	 /	 \
    1	  3	5	  7

*/

#include <iostream>
#include <set>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void extractKeys(Node* root, set<int> &set)
    {
        if (root == nullptr) {
            return;
        }

        extractKeys(root->left, set);
        set.insert(root->data);
        extractKeys(root->right, set);
    }

    void convertToBST(Node* root, set<int>::iterator &it)
    {
        if (root == nullptr) {
            return;
        }

        convertToBST(root->left, it);

        root->data = *it;
        it++;

        convertToBST(root->right, it);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void convertToBST(Node* root)
    {
        set<int> set;
        extractKeys(root, set);

        auto it = set.begin();
        convertToBST(root, it);
    }
};

void inorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorder(root->left);
    cout << root->data << " ";
    inorder(root->right);
}

int main()
{
    /* Construct the following tree
               8
             /   \
            /     \
           3       5
          / \     / \
         /   \   /   \
        10    2 4     6
    */

    Node* root = new Node(8);
    root->left = new Node(3);
    root->right = new Node(5);
    root->left->left = new Node(10);
    root->left->right = new Node(2);
    root->right->left = new Node(4);
    root->right->right = new Node(6);

    Solution().convertToBST(root);
    inorder(root);

    return 0;
}
