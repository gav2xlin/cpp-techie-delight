/*

Given the root of a binary search tree (BST) and a target, return a pair of nodes in the BST such that their sum is equal to the target. The solution can return pair in any order.

For example, consider the following BST.

          8
        /	\
       /	 \
      /		  \
     4		  10
    / \		 /  \
   /   \	/	 \
  2		6  9	 12

Input: target = 20
Output: (Node 8, Node 12)

• Each input can have multiple solutions. The output should match with either one of them.

Input: target = 14
Output: (Node 6, Node 8) or (Node 4, Node 10)

• If no pair with the given sum exists, the solution should return the pair (nullptr, nullptr).

Input: target = 25
Output: (nullptr, nullptr)

*/

#include <iostream>
#include <utility>
#include <vector>
#include <map>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void pushTreeNodes(Node* root, map<int, Node*> &map, vector<int>& order)
    {
        if (root == nullptr) {
            return;
        }

        map.insert({root->data, root});
        order.push_back(root->data);

        pushTreeNodes(root->left, map, order);
        pushTreeNodes(root->right, map, order);
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    pair<Node*,Node*> findPair(Node* root, int target)
    {
        map<int, Node*> map;
        vector<int> order;
        pushTreeNodes(root, map, order);

        for (auto v : order) {
            int k = target - v;
            if (map.count(k) && map[v] != map[k]) {
                return make_pair(map[v], map[k]);
            }
        }

        return {nullptr, nullptr};
    }
};

ostream& operator<<(ostream& os, const pair<Node*, Node*>& p) {
    if (p.first != nullptr) {
        os << p.first->data << ' ';
    } else {
        os << "nullptr ";
    }

    if (p.second != nullptr) {
        os << p.second->data;
    } else {
        os << "nullptr";
    }

    return os;
}

int main()
{
    Node* root = new Node{8};
    root->left = new Node{4};
    root->right = new Node{10};
    root->left->left = new Node{2};
    root->left->right = new Node{6};
    root->right->left = new Node{9};
    root->right->right = new Node{12};

    cout << Solution().findPair(root, 20) << endl;
    cout << Solution().findPair(root, 14) << endl;
    cout << Solution().findPair(root, 25) << endl;

    delete root->left->left;
    delete root->left->right;
    delete root->right->left;
    delete root->right->right;
    delete root->left;
    delete root->right;
    delete root;

    return 0;
}
