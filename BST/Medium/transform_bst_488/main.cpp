/*

Given the root of a binary search tree (BST), in-place modify the BST such that every node is updated to contain the sum of all greater keys present in the BST.

Input:

          5
        /	\
       /	 \
      /		  \
     3		   8
    / \		  / \
   /   \	 /	 \
  2		4   6	 10

Output:

          29
        /	 \
       /	  \
      /		   \
     36		   18
    /  \	  /  \
   /	\	 /	  \
  38	33	24	  10

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    /*int findSum(Node* root)
    {
        if (root == nullptr) {
            return 0;
        }

        return root->data + findSum(root->left) + findSum(root->right);
    }

    void transform(Node* root, int &sum)
    {
        if (root == nullptr) {
            return;
        }

        transform(root->left, sum);

        sum = sum - root->data;
        root->data += sum;

        transform(root->right, sum);
    }*/

    int transform(Node*  root, int sum_so_far)
    {
        if (root == nullptr) {
            return sum_so_far;
        }

        int right = transform(root->right, sum_so_far);

        root->data += right;
        sum_so_far = root->data;

        return transform(root->left, sum_so_far);
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void transform(Node* root)
    {
        /*int sum = findSum(root);
        transform(root, sum);*/
        transform(root, 0);
    }
};

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insert(root->left, key);
    } else {
        root->right = insert(root->right, key);
    }

    return root;
}

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    /*
    Input:

              5
            /	\
           /	 \
          /		  \
         3		   8
        / \		  / \
       /   \	 /	 \
      2		4   6	 10

    Output:

              29
            /	 \
           /	  \
          /		   \
         36		   18
        /  \	  /  \
       /	\	 /	  \
      38	33	24	  10*/

    int keys[] = { 5, 3, 2, 4, 6, 8, 10 };
    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    Solution().transform(root);
    cout << *root << endl;

    return 0;
}
