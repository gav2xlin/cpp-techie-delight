/*

Given the root of a binary search tree (BST) and a range of BST keys, return the count of subtrees in the BST whose nodes lie within the given range.

Input: Below BST, low = 5, high = 20

                15
              /	   \
            /		 \
          /			   \
         10				 25
       /	\		   /	\
      /		 \		  /		 \
     8		 12		 20		 30
    / \				/  \
   /   \		   /	\
  6		9		  18	22

Output: 6
Explanation: The total number of subtrees with nodes in range [5, 20] is 6, as shown below:

  6		9		  8			   10		 12		 18
                /   \		  /  \
               6	 9		 8	 12
                            / \
                           6   9

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool findSubTrees(Node* root, int low, int high, int &count)
    {
        if (root == nullptr) {
            return true;
        }

        bool left = findSubTrees(root->left, low, high, count);
        bool right = findSubTrees(root->right, low, high, count);

        if (left && right && (low <= root->data && root->data <= high))
        {
            ++count;

            return true;
        }

        return false;
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int countSubtrees(Node* root, int low, int high)
    {
        int count{0};
        findSubTrees(root, low, high, count);
        return count;
    }
};

void preorder(ostream& os, const Node* root, string shift="")
{
    if (root == nullptr) {
        return;
    }

    os << shift << root->data << '\n';
    preorder(os, root->left, shift + "-");
    preorder(os, root->right, shift + "-");
}

ostream& operator<<(ostream& os, const Node* root) {
    preorder(os, root);
    return os;
}

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insert(root->left, key);
    } else {
        root->right = insert(root->right, key);
    }

    return root;
}

int main()
{
    int keys[] = {15, 25, 20, 22, 30, 18, 10, 8, 9, 12, 6};

    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    cout << "tree:\n" << root << endl;

    cout << Solution().countSubtrees(root, 5, 20) << endl;

    return 0;
}
