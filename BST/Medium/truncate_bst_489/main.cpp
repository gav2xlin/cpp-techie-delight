/*

Given the root of a binary search tree (BST) and a range of BST keys, remove nodes from the BST that have keys outside the given range.

Input: low = 9, high = 12

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

Output:

     10
       \
        \
        12

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* truncate(Node* root, int low, int high)
    {
        if (root == nullptr) {
            return root;
        }

        root->left = truncate(root->left, low, high);
        root->right = truncate(root->right, low, high);

        Node* curr = root;

        if (root->data < low)
        {
            root = root->right;
            delete curr;
        } else if (root->data > high)
        {
            root = root->left;
            delete curr;
        }

        return root;
    }
};

void preorder(ostream& os, const Node* root, string shift="")
{
    if (root == nullptr) {
        return;
    }

    os << shift << root->data << '\n';
    preorder(os, root->left, shift + "-");
    preorder(os, root->right, shift + "-");
}

ostream& operator<<(ostream& os, const Node* root) {
    preorder(os, root);
    return os;
}

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insert(root->left, key);
    } else {
        root->right = insert(root->right, key);
    }

    return root;
}

int main()
{
    int keys[] = {15, 10, 20, 8, 12, 16, 25};

    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    cout << "before:\n" << root;

    root = Solution().truncate(root, 9, 12);

    cout << "after:\n" << root;

    return 0;
}
