/*

Given the root of a binary search tree (BST) and a tree node x, find the floor and ceiling of node x in the BST. If node x lies in the BST, then both floor and ceil are equal to that node; otherwise, the ceil is equal to the next greater node (if any) in the BST, and the floor is equal to the previous greater node (if any) in the BST.

The solution should return the (floor, ceil) pair. If the floor or ceil doesn't exist, consider it to be nullptr.

For example, consider the following BST.

          8
        /	\
       /	 \
      /		  \
     4		  10
    / \		 /  \
   /   \	/	 \
  2		6  9	 12

Input: Node 7
Output: (Node 6, Node 8)
Explanation: The floor of node 7 is node 6, and ceil is node 8.

Input: Node 9
Output: (Node 9, Node 9)
Explanation: Node 9 lies in the BST. Hence both floor and ceil are equal to node 9.

Input: Node 1
Output: (nullptr, Node 2)
Explanation: The floor of node 1 doesn't exist and ceil is node 2.

Input: Node 15
Output: (Node 12, nullptr)
Explanation: The floor of node 15 is node 12 and its ceil doesn't exist.

*/

#include <iostream>
#include <iomanip>
#include <utility>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void findFloorCeil(Node* root, Node* &floor, Node* &ceil, int key)
    {
        if (root == nullptr) {
            return;
        }

        if (root->data == key)
        {
            floor = root;
            ceil = root;
        } else if (key < root->data) {
            ceil = root;
            findFloorCeil(root->left, floor, ceil, key);
        } else {
            floor = root;
            findFloorCeil(root->right, floor, ceil, key);
        }
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    pair<Node*, Node*> findFloorAndCeil(Node* root, Node* x)
    {
        Node *floor = nullptr, *ceil = nullptr;
        findFloorCeil(root, floor, ceil, x->data);
        return {floor, ceil};
    }
};

void preorder(ostream& os, const Node* root, string shift="")
{
    if (root == nullptr) {
        return;
    }

    os << shift << root->data << '\n';
    preorder(os, root->left, shift + "-");
    preorder(os, root->right, shift + "-");
}

ostream& operator<<(ostream& os, const Node* root) {
    preorder(os, root);
    return os;
}

ostream& operator<<(ostream& os, const pair<int, int>& p) {
    os << p.first << ' ' << p.second;
    return os;
}

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insert(root->left, key);
    } else {
        root->right = insert(root->right, key);
    }

    return root;
}

int main()
{
    int keys[] = { 2, 4, 6, 8, 9, 10, 12 };

    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    cout << "tree:\n" << root << endl;

    Solution s;

    for (int i = 0; i < 15; ++i)
    {
        Node x{i};
        auto [floor, ceil] = s.findFloorAndCeil(root, &x);

        cout << setw(2) << i << " —> ";
        cout << setw(4) << (floor != nullptr ? floor->data: -1);
        cout << setw(4) << (ceil != nullptr ? ceil->data: -1) << endl;
    }

    return 0;
}
