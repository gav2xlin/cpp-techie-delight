/*

Given the root of a binary search tree (BST) and a key, delete the node with that key in the BST if it exists, and return the root node.

• When deleting a node with no children, the solution should remove the node from the tree.
• When deleting a node with one child, the solution should remove the node and replace it with its child.
• When deleting a node with two children, the solution should swap the node's value with either its inorder successor or inorder predecessor, and then call delete on the inorder successor or inorder predecessor. This node will have at-most one child and can be deleted according to one of the two simpler cases above.

For example, consider the following BST.

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

Input: key = 25
Output:

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /
   /	\	 /
  8		12	16

Input: key = 15
Output:

          12					  16
        /	 \			  		/	 \
       /	  \			 	   /	  \
      /		   \		 or   /		   \
     10		   20		   	 10		   20
    /		  /  \		  	/  \		 \
   /		 /	  \		   /	\	 	  \
  8			16	  25	  8		12		  25

*/


#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void searchKey(Node* &curr, int key, Node* &parent)
    {
        while (curr != nullptr && curr->data != key)
        {
            parent = curr;

            if (key < curr->data) {
                curr = curr->left;
            }
            else {
                curr = curr->right;
            }
        }
    }

    Node* getMinimumKey(Node* curr)
    {
        while (curr->left != nullptr) {
            curr = curr->left;
        }
        return curr;
    }

    Node* findMaximumKey(Node* curr)
    {
        while (curr->right != nullptr) {
            curr = curr->right;
        }
        return curr;
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void deleteNode(Node* &root, int key)
    {
        Node* parent = nullptr;
        Node* curr = root;

        searchKey(curr, key, parent);

        if (curr == nullptr) {
            return;
        }

        if (curr->left == nullptr && curr->right == nullptr) {
            if (curr != root)
            {
                if (parent->left == curr) {
                    parent->left = nullptr;
                } else {
                    parent->right = nullptr;
                }
            } else {
                root = nullptr;
            }

            delete(curr);
        } else if (curr->left!= nullptr && curr->right!= nullptr) {
            Node* successor = getMinimumKey(curr->right);

            int val = successor->data;

            deleteNode(root, successor->data);

            curr->data = val;
        } else {
            Node* child = curr->left != nullptr ? curr->left: curr->right;

            if (curr != root)
            {
                if (curr == parent->left) {
                    parent->left = child;
                }
                else {
                    parent->right = child;
                }
            } else {
                root = child;
            }

            delete(curr);
        }
    }
    /*void deleteNode(Node* &root, int key)
    {
        if (root == nullptr) {
            return;
        }

        if (key < root->data) {
            deleteNode(root->left, key);
        } else if (key > root->data) {
            deleteNode(root->right, key);
        } else {
            if (root->left == nullptr && root->right == nullptr)
            {
                delete root;
                root = nullptr;
            } else if (root->left != nullptr && root->right != nullptr)
            {
                Node* predecessor = findMaximumKey(root->left);

                root->data = predecessor->data;

                deleteNode(root->left, predecessor->data);
                Node* child = root->left != nullptr ? root->left: root->right;
                Node* curr = root;

                root = child;

                delete curr;
            }
        }
    }*/
};

void preorder(ostream& os, const Node* root, string shift="")
{
    if (root == nullptr) {
        return;
    }

    os << shift << root->data << '\n';
    preorder(os, root->left, shift + "-");
    preorder(os, root->right, shift + "-");
}

ostream& operator<<(ostream& os, const Node* root) {
    preorder(os, root);
    return os;
}

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insert(root->left, key);
    } else {
        root->right = insert(root->right, key);
    }

    return root;
}

int main()
{
    int keys[] = {15, 10, 20, 8, 12, 16};

    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    cout << "before:\n" << root << endl;

    Solution().deleteNode(root, 16);

    cout << "after:\n" << root << endl;

    return 0;
}
