/*

Given an array of distinct integers, in-place replace every element with the least greater element on its right or with -1 if there are no greater elements.

Input : [10, 100, 93, 32, 35, 65, 80, 90, 94, 6]
Output: [32, -1, 94, 35, 65, 80, 90, 94, -1, -1]

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Solution
{
private:
    struct Node
    {
        int key;
        Node *left, *right;
    };

    void insert(Node* &root, int key, int &successor)
    {
        if (root == nullptr)
        {
            root = new Node{key, nullptr, nullptr};
            return;
        }

        if (key < root->key) {
            successor = root->key;

            insert(root->left, key, successor);
        } else if (key > root->key) {
            insert(root->right, key, successor);
        }
    }
public:
    void replace(vector<int> &nums)
    {
        int n = nums.size();
        /*for (int i = 0; i < n; ++i)
        {
            int successor = -1;
            int diff = INT_MAX;

            for (int j = i + 1; j < n; ++j)
            {
                if (nums[j] > nums[i] && (nums[j] - nums[i] < diff))
                {
                    successor = nums[j];
                    diff = nums[j] - nums[i];
                }
            }
            nums[i] = successor;
        }*/
        Node* root = nullptr;

        for (int i = n - 1; i >= 0; --i)
        {
            int successor = -1;
            insert(root, nums[i], successor);
            nums[i] = successor;
        }
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    vector<int> nums{10, 100, 93, 32, 35, 65, 80, 90, 94, 6};

    cout << "before: " << nums << endl;

    Solution().replace(nums);

    cout << "after: " << nums << endl;

    return 0;
}
