/*

Given the root of two binary search trees, merge them into a doubly-linked list in sorted order.

Input: Below BSTs

      20
     /  \
   10	30
        /  \
       25  100

      50
     /  \
    5	70

Output: 5 ⇔ 10 ⇔ 20 ⇔ 25 ⇔ 30 ⇔ 50 ⇔ 70 ⇔ 100

The solution should return the head of the doubly-linked list constructed from the tree nodes.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void convertBSTtoDLL(Node* root, Node* &headRef)
    {
        if (root == nullptr) {
            return;
        }

        convertBSTtoDLL(root->right, headRef);

        push(root, headRef);

        convertBSTtoDLL(root->left, headRef);
    }

    void push(Node* root, Node* &headRef)
    {
        root->right = headRef;

        if (headRef != nullptr) {
            headRef->left = root;
        }

        headRef = root;
    }

    Node* mergeDDLs(Node* a, Node* b)
    {
        if (a == nullptr) {
            return b;
        }

        if (b == nullptr) {
            return a;
        }

        if (a->data < b->data)
        {
            a->right = mergeDDLs(a->right, b);
            a->right->left = a;

            return a;
        }
        else {
            b->right = mergeDDLs(a, b->right);
            b->right->left = b;

            return b;
        }
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* mergeBSTs(Node* a, Node* b)
    {
        Node* first = nullptr;
        convertBSTtoDLL(a, first);

        Node* second = nullptr;
        convertBSTtoDLL(b, second);

        return mergeDDLs(first, second);
    }
};

void printDoublyLinkedList(Node* head)
{
    while (head != nullptr)
    {
        cout << head->data << " —> ";
        head = head->right;
    }

    cout << "null" << endl;
}

int main()
{
    Node* a = new Node(20);
    a->left = new Node(10);
    a->right = new Node(30);
    a->right->left = new Node(25);
    a->right->right = new Node(100);


    Node* b = new Node(50);
    b->left = new Node(5);
    b->right = new Node(70);

    Node* root = Solution().mergeBSTs(a, b);

    printDoublyLinkedList(root);

    return 0;
}
