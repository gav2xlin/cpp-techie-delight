/*

Given a sequence of distinct keys representing the postorder traversal of a binary search tree (BST), construct and return a BST out of the sequence.

Input: [8, 12, 10, 16, 25, 20, 15]

Output:
          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    /*Node* constructBST(vector<int> const &postorder, int start, int end)
    {
        if (start > end) {
            return nullptr;
        }

        struct Node* node = new Node(postorder[end]);

        int i;
        for (i = end; i >=start; --i)
        {
            if (postorder[i] < node->data) {
                break;
            }
        }

        node->right = constructBST(postorder, i + 1, end - 1);
        node->left = constructBST(postorder, start, i);

        return node;
    }*/
    Node* buildTree(vector<int> const &postorder, int &pIndex, int min, int max)
    {
        if (pIndex < 0) {
            return nullptr;
        }

        int curr = postorder[pIndex];
        if (curr < min || curr > max) {
            return nullptr;
        }

        Node* root = new Node(curr);
        --pIndex;

        root->right = buildTree(postorder, pIndex, curr + 1, max);
        root->left = buildTree(postorder, pIndex, min, curr - 1);

        return root;
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBST(vector<int> const &postorder)
    {
        // return constructBST(postorder, 0, postorder.size() - 1);
        int postIndex = postorder.size() - 1;
        return buildTree(postorder, postIndex, INT_MIN, INT_MAX);
    }
};

void inorder(struct Node* root)
{
    if (root == NULL) {
        return;
    }

    inorder(root->left);
    cout << root->data << ' ';
    inorder(root->right);
}

int main()
{
    vector<int> postorder { 8, 12, 10, 16, 25, 20, 15 };

    Node* root = Solution().constructBST(postorder);

    inorder(root);
    cout << endl;

    return 0;
}
