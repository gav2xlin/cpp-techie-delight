/*

Find the optimal cost to construct a binary search tree (BST), where each key can repeat several times. You're given each key’s frequency in the same order as corresponding keys in the inorder traversal of a BST.

To construct a BST, determine if each given key already exists in the BST or not. The cost of finding a BST key is equal to the level of the key (if present in the BST).

Input: freq[] = [25, 10, 20]

Note: As freq[] follows inorder order (ascending keys), you may consider the index of freq[] as corresponding keys. i.e., key 0 occurs 25 times, key 1 occurs 10 times, and key 2 occurs 20 times.

Output: 95
Explanation: The optimum BST is:

    0(25×1)              — — — — — level 1
        \
         \
          \
          2(20×2)        — — — — — level 2
            /
           /
          /
       1(10×3)           — — — — — level 3

25 lookups of the key 0 will cost 1 each.
20 lookups of the key 2 will cost 2 each.
10 lookups of the key 1 will cost 3 each.

Therefore, the optimal cost of constructing BST is 25×1 + 20×2 + 10×3 = 95


Other possible BSTs are:

1.

    0(25×1)               — — — — — level 1
        \
         \
          \
         1(10×2)          — — — — — level 2
            \
             \
              \
           2(20×3)        — — — — — level 3

Cost is 25 + 10×2 + 20×3 = 105


2.

            2(20×1)      — — — — — level 1
            /
           /
          /
      1(10×2)            — — — — — level 2
        /
       /
      /
  0(25×3)                — — — — — level 3

Cost is 20 + 10×2 + 25×3 = 115


3.

       1(10×1)            — — — — — level 1
        /  \
       /    \
      /      \
  0(25×2)    2(20×2)      — — — — — level 2

Cost is 10 + 25×2 + 20×2 = 100


4.

          2(20×1)        — — — — — level 1
          /
         /
        /
      0(25×2)            — — — — — level 2
        \
         \
          \
        1(10×3)          — — — — — level 3

Cost is 20 + 25×2 + 10×3 = 100

*/

#include <iostream>
#include <vector>
#include <climits>
#include <unordered_map>
#include <string>
#include <algorithm>

using namespace std;

class Solution
{
private:
    /*int findOptimalCost(vector<int> const &freq, int i, int j, int level, auto &lookup)
    {
        if (j < i) {
            return 0;
        }

        string key = to_string(i) + "|" + to_string(j) + "|" + to_string(level);

        if (lookup.find(key) == lookup.end())
        {
            lookup[key] = INT_MAX;

            int leftOptimalCost, rightOptimalCost;

            for (int k = i; k <= j; ++k)
            {
                leftOptimalCost = findOptimalCost(freq, i, k - 1, level + 1, lookup);

                rightOptimalCost = findOptimalCost(freq, k + 1, j, level + 1, lookup);

                int cost = freq[k] * level + leftOptimalCost + rightOptimalCost;

                lookup[key] = min(lookup[key], cost);
            }
        }

        return lookup[key];
    }*/
public:
    /*int findOptimalCost(vector<int> const &freq)
    {
        int n = freq.size();
        unordered_map<string, int> lookup;

        return findOptimalCost(freq, 0, n - 1, 1, lookup);
    }*/
    int findOptimalCost(vector<int> const &freq)
    {
        int n = freq.size();
        vector<vector<int>> cost(n + 1, vector<int>(n + 1));

        for (int i = 0; i < n; ++i) {
            cost[i][i] = freq[i];
        }

        for (int size = 1; size <= n; ++size)
        {
            for (int i = 0; i <= n - size + 1; ++i)
            {
                int j = min(i + size - 1, n - 1);
                cost[i][j] = INT_MAX;

                for (int r = i; r <= j; ++r)
                {
                    int total = 0;

                    for (int k = i; k <= j; k++) {
                        total += freq[k];
                    }

                    if (r != i) {
                        total += cost[i][r - 1];
                    }

                    if (r != j) {
                        total += cost[r + 1][j];
                    }

                    cost[i][j] = min(total, cost[i][j]);
                }
            }
        }

        return cost[0][n - 1];
    }
};

int main()
{
    vector<int> freq { 25, 10, 20 };

    cout << Solution().findOptimalCost(freq) << endl;

    return 0;
}
