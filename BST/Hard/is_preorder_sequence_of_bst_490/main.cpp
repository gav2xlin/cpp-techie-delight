/*

Given a distinct sequence of keys, check if it represents a preorder traversal of a binary search tree (BST).

Input : [15, 10, 8, 12, 20, 16, 25]
Output: true
Explanation: The following BST can be constructed from the above sequence and it has the same preorder traversal:

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

*/

#include <iostream>
#include <vector>
#include <climits>
#include <memory>

using namespace std;

class Solution
{
private:
    struct Node
    {
        int data;
        shared_ptr<Node*> left, right;

        Node(): data{} {}
        Node(int data): data(data) {}
    };

    /*shared_ptr<Node*> insert(shared_ptr<Node*> root, int key)
    {
        if (root.get() == nullptr) {
            return make_shared<Node*>(new Node(key));
        }

        if (key < (*root)->data)
        {
            (*root)->left = insert((*root)->left, key);
        }
        else
        {
            (*root)->right = insert((*root)->right, key);
        }

        return root;
    }

    shared_ptr<Node*> buildTree(vector<int> const &seq)
    {
        shared_ptr<Node*> root;
        for (int key: seq) {
            root = insert(root, key);
        }

        return root;
    }

    bool comparePreOrder(shared_ptr<Node*> root, vector<int> const &seq, int &index)
    {
        if (root == nullptr) {
            return true;
        }

        if (seq[index] != (*root)->data) {
            return false;
        }

        ++index;

        return comparePreOrder((*root)->left, seq, index) && comparePreOrder((*root)->right, seq, index);
    }*/

    shared_ptr<Node*> buildTree(vector<int> const &seq, int &index, int min, int max)
    {
        if (index == seq.size()) {
            return nullptr;
        }

        int val = seq[index];
        if (val < min || val > max) {
            return nullptr;
        }

        shared_ptr<Node*> root = make_shared<Node*>(new Node(val));
        ++index;

        (*root)->left = buildTree(seq, index, min, val - 1);

        (*root)->right = buildTree(seq, index, val + 1, max);

        return root;
    }
public:
    bool isBST(vector<int> const &seq)
    {
        /*shared_ptr<Node*> root = buildTree(seq);

        int index = 0;
        return comparePreOrder(root, seq, index) && index == seq.size();*/
        int index = 0;
        shared_ptr<Node*> root = buildTree(seq, index, INT_MIN, INT_MAX);

        return index == seq.size();
    }
};

int main()
{
    vector<int> seq = { 15, 10, 8, 12, 20, 16, 25 };

    cout << boolalpha << Solution().isBST(seq) << endl;

    return 0;
}
