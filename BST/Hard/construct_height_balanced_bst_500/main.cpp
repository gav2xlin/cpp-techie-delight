/*

Given the root of a binary search tree (BST), convert it into a height-balanced binary search tree. For a height-balanced binary search tree, the difference between the height of the left and right subtree of every node is never more than 1.

Input: A BST

              20
             /
           15
          /
        10
       /
      5
    /   \
   2	 8

Output: Balanced BST

          10
        /	 \
       5	  15
      / \	   \
     2   8	   20

OR
          8
        /	\
       5	 10
      / 	  \
     2   	  15
               \
               20
OR

Any other possible representation.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    // inorder
    /*void pushTreeNodes(Node* root, vector<Node*> &nodes)
    {
        if (root == nullptr) {
            return;
        }

        pushTreeNodes(root->left, nodes);
        nodes.push_back(root);
        pushTreeNodes(root->right, nodes);
    }

    Node* buildBalancedBST(vector<Node*> &nodes, int start, int end)
    {
        if (start > end) {
            return nullptr;
        }

        int mid = (start + end) / 2;

        Node* root = nodes[mid];

        root->left = buildBalancedBST(nodes, start, mid - 1);
        root->right = buildBalancedBST(nodes, mid + 1, end);

        return root;
    }*/
    // in-place
    void push(Node* root, Node*& headRef)
    {
        root->right = headRef;

        if (headRef != nullptr) {
            headRef->left = root;
        }

        headRef = root;
    }

    /*
        Recursive function to construct a sorted doubly linked list from a BST
        root —> Pointer to the root node of the binary search tree
        headRef —> Reference to the head node of the doubly linked list
        n —> Stores the total number of nodes processed so far in the BST
    */
    void convertBSTtoSortedDLL(Node* root, Node*& headRef, int &n)
    {
        if (root == nullptr) {
            return;
        }

        convertBSTtoSortedDLL(root->right, headRef, n);

        push(root, headRef);

        convertBSTtoSortedDLL(root->left, headRef, ++n);
    }

    /*
        Recursive function to construct a height-balanced BST from a doubly linked list
        headRef —> Reference to the head node of the doubly linked list
        n —> Total number of nodes in the doubly linked list
    */
    Node* convertSortedDLLToBST(Node* &headRef, int n)
    {
        if (n <= 0) {
            return nullptr;
        }

        Node* leftSubTree = convertSortedDLLToBST(headRef, n / 2);
        Node* root = headRef;
        root->left = leftSubTree;

        headRef = headRef->right;
        root->right = convertSortedDLLToBST(headRef, n - (n/2 + 1)); /* +1 for the root node */

        return root;
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBalancedBST(Node* root)
    {
        /*vector<Node*> nodes;
        pushTreeNodes(root, nodes);

        return buildBalancedBST(nodes, 0, nodes.size() - 1);*/
        Node* head = nullptr;

        int counter = 0;
        convertBSTtoSortedDLL(root, head, counter);

        return convertSortedDLLToBST(head, counter);
    }
};

void preorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    cout << root->data << ' ';
    preorder(root->left);
    preorder(root->right);
}

int main()
{
    Node* root = new Node(20);
    root->left = new Node(15);
    root->left->left = new Node(10);
    root->left->left->left = new Node(5);
    root->left->left->left->left = new Node(2);
    root->left->left->left->right = new Node(8);

    root = Solution().constructBalancedBST(root);

    preorder(root);
    cout << endl;

    return 0;
}
