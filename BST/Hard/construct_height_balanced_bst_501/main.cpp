/*

Given a sorted doubly-linked list of distinct integers, in-place convert it into a height-balanced binary search tree (BST) where the difference between its left and right subtree height is at most 1.

The conversion should be done such that the previous child pointer of a doubly-linked list node should act as a left pointer for a binary tree node, and the next child pointer should act as the right pointer for a binary tree node. The conversion should also be done by only exchanging the pointers without allocating any memory for the BST nodes.

Input: 8 ⇔ 10 ⇔ 12 ⇔ 15 ⇔ 18 ⇔ 20 ⇔ 25

Output:

      15
    /	 \
   10	  20
  /  \   /  \
 8   12 18  25

OR

      12
    /	 \
   10	 20
  /		/  \
 8	   18  25
      /
     15

Any other possible representation.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* prev = nullptr;		// pointer to the prev child
    Node* next = nullptr;		// pointer to the next child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *prev, Node *next): data(data), prev(prev), next(next) {}
};

class Solution
{
private:
    //
    /*void pushDDLNodes(Node* node, vector<Node*> &nodes)
    {
        while (node != nullptr)
        {
            nodes.push_back(node);
            node = node->next;
        }
    }

    Node* buildBalancedBST(vector<Node*> &nodes, int start, int end)
    {
        if (start > end) {
            return nullptr;
        }

        int mid = (start + end) / 2;

        Node* root = nodes[mid];

        root->prev = buildBalancedBST(nodes, start, mid - 1);
        root->next = buildBalancedBST(nodes, mid + 1, end);

        return root;
    }*/
    //
    Node* convertSortedDLLToBalancedBST(Node* &headRef, int n)
    {
        if (n <= 0) {
            return nullptr;
        }

        Node* leftSubTree = convertSortedDLLToBalancedBST(headRef, n / 2);
        Node* root = headRef;
        root->prev = leftSubTree;

        headRef = headRef->next;
        root->next = convertSortedDLLToBalancedBST(headRef, n - (n/2 + 1)); /* +1 for the root node */

        return root;
    }

    int countNodes(Node* node)
    {
        int counter = 0;

        while (node != nullptr)
        {
            node = node->next;
            counter++;
        }

        return counter;
    }
public:

    /*
        A doubly linked list node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* prev = nullptr;		// pointer to the prev child
            Node* next = nullptr;		// pointer to the next child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *prev, Node *next): data(data), prev(prev), next(next) {}
        };
    */

    Node* constructBalancedBST(Node* head)
    {
        /*vector<Node*> nodes;
        pushDDLNodes(head, nodes);

        return buildBalancedBST(nodes, 0, nodes.size() - 1);*/
        int n = countNodes(head);

        return convertSortedDLLToBalancedBST(head, n);
    }
};

void push(Node* &headRef, int data)
{
    Node* node = new Node(data);
    node->next = headRef;

    if (headRef) {
        headRef->prev = node;
    }

    headRef = node;
}

void printListNodes(Node* node)
{
    while (node != nullptr)
    {
        cout << node->data << " ";
        node = node->next;
    }
    cout << endl;
}

void preorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    cout << root->data << ' ';
    preorder(root->prev);
    preorder(root->next);
}

int main()
{
    Node* head = nullptr;

    int keys[] = { 25, 20, 18, 15, 12, 10, 8 };
    for (int key: keys) {
        push(head, key);
    }

    printListNodes(head);

    Node* root = Solution().constructBalancedBST(head);

    preorder(root);
    cout << endl;

    return 0;
}
