/*

Given the root of a binary tree, find the size of the largest binary search tree (BST) in the binary tree.

Input:
           10
         /	  \
        /	   \
       15		8
      /	 \	   / \
     /	  \   /	  \
    12	  20 5	   2

Output: 3

Explanation: The largest BST in the binary tree is formed by a subtree rooted at node 15, having size 3:

       15
      /	 \
     /	  \
    12	  20

*/

#include <iostream>
#include <algorithm>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    // preorder
    /*int size(Node* root)
    {
        if (root == nullptr) {
            return 0;
        }

        return size(root->left) + 1 + size(root->right);
    }

    bool isBST(Node* node, int min, int max)
    {
        if (node == nullptr) {
            return true;
        }

        if (node->data < min || node->data > max) {
            return false;
        }

        return isBST(node->left, min, node->data) && isBST(node->right, node->data, max);
    }*/
    //
    struct SubTreeInfo
    {
        int min, max, size;

        bool isBST;

        SubTreeInfo(int min, int max, int size, bool isBST)
        {
            this->min = min;
            this->max = max;
            this->size = size;
            this->isBST = isBST;
        }
    };

    SubTreeInfo* findLargestBST(Node* root)
    {
        if (root == nullptr) {
            return new SubTreeInfo(INT_MAX, INT_MIN, 0, true);
        }

        SubTreeInfo* left = findLargestBST(root->left);
        SubTreeInfo* right = findLargestBST(root->right);

        SubTreeInfo* info = nullptr;

        if (left->isBST && right->isBST &&
            (root->data > left->max && root->data < right->min))
        {
            info = new SubTreeInfo(min(root->data, min(left->min, right->min)),
                            max(root->data, max(left->max, right->max)),
                            left->size + 1 + right->size,
                            true);
        }
        else
        {
            info = new SubTreeInfo(0, 0, max(left->size, right->size), false);
        }

        delete(left), delete(right);

        return info;
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    /*int findLargestBSTSize(Node* root)
    {
        if (isBST(root, INT_MIN, INT_MAX)) {
            return size(root);
        }

        return max(findLargestBSTSize(root->left), findLargestBSTSize(root->right));
    }*/
    int findLargestBSTSize(Node* root)
    {
        return findLargestBST(root)->size;
    }
};

int main()
{
    Node* root = new Node(10);

    root->left = new Node(15);
    root->right = new Node(8);

    root->left->left = new Node(12);
    root->left->right = new Node(20);

    root->right->left = new Node(5);
    root->right->right = new Node(2);

    cout << Solution().findLargestBSTSize(root) << endl;

    return 0;
}
