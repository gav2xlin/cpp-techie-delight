/*

Given two integer arrays, X and Y, representing a set of BST keys, check if they represent the same BSTs or not without building the tree. Assume that the keys are inserted into the BST in the same order as they appear in the array.

Input:

X[] = [15, 25, 20, 22, 30, 18, 10, 8, 9, 12, 6]
Y[] = [15, 10, 12, 8, 25, 30, 6, 20, 18, 9, 22]

Output: true
Explanation: Both arrays represent the same BSTs, as shown below:

                15
              /   \
            /		\
          /			  \
         10			   25
       /   \		 /   \
      /		\		/	  \
     8		12 	   20	  30
    / \			  /  \
   /   \		 /	  \
  6		9		18	  22

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    int isSameBST(vector<int> &X, vector<int> &Y, int n)
    {
        if (n == 0) {
            return true;
        }

        if (X[0] != Y[0]) {
            return false;
        }

        if (n == 1) {
            return true;
        }

        vector<int> leftX(n - 1), rightX(n - 1), leftY(n - 1), rightY(n - 1);

        int k = 0, l = 0, m = 0, o = 0;

        for (int i = 1; i < n; ++i)
        {
            if (X[i] < X[0]) {
                leftX[k++] = X[i];
            }
            else
            {
                rightX[l++] = X[i];
            }

            if (Y[i] < Y[0]) {
                leftY[m++] = Y[i];
            }
            else
            {
                rightY[o++] = Y[i];
            }
        }

        if (k != m) {
            return false;
        }

        if (l != o) {
            return false;
        }

        return isSameBST(leftX, leftY, k) && isSameBST(rightX, rightY, l);
    }
public:
    bool isSameBSTs(vector<int> const &_X, vector<int> const &_Y)
    {
        vector<int> X(_X), Y(_Y);
        int m = X.size(), n = Y.size();
        return m == n && isSameBST(X, Y, n);
    }
};

int main()
{
    cout << boolalpha << Solution().isSameBSTs({15, 25, 20, 22, 30, 18, 10, 8, 9, 12, 6}, {15, 10, 12, 8, 25, 30, 6, 20, 18, 9, 22}) << endl;

    return 0;
}
