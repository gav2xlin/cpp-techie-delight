/*

Given a sequence of distinct keys representing the preorder traversal of a binary search tree (BST), construct and return a BST out of the sequence.

Input: [15, 10, 8, 12, 20, 16, 25]

Output:
          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

*/

#include <iostream>
#include <vector>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    /*Node* constructBST(vector<int> const &preorder, int start, int end)
    {
        if (start > end) {
            return nullptr;
        }

        struct Node* node = new Node(preorder[start]);

        int i;
        for (i = start; i <= end; ++i)
        {
            if (preorder[i] > node->data) {
                break;
            }
        }

        node->left = constructBST(preorder, start + 1, i - 1);
        node->right = constructBST(preorder, i, end);

        return node;
    }*/
    Node* buildTree(vector<int> const &preorder, int &pIndex, int min, int max)
    {
        if (pIndex == preorder.size()) {
            return nullptr;
        }

        int val = preorder[pIndex];
        if (val < min || val > max) {
            return nullptr;
        }

        Node* root = new Node(val);
        ++pIndex;

        root->left = buildTree(preorder, pIndex, min, val - 1);
        root->right = buildTree(preorder, pIndex, val + 1, max);

        return root;
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBST(vector<int> const &preorder)
    {
        //return constructBST(preorder, 0, preorder.size() - 1);
        int pIndex = 0;
        return buildTree(preorder, pIndex, INT_MIN, INT_MAX);
    }
};

void inorder(struct Node* root)
{
    if (root == NULL) {
        return;
    }

    inorder(root->left);
    printf("%d ", root->data);
    inorder(root->right);
}

int main()
{
    vector<int> preorder { 15, 10, 8, 12, 20, 16, 25 };

    Node* root = Solution().constructBST(preorder);

    inorder(root);
    cout << endl;

    return 0;
}
