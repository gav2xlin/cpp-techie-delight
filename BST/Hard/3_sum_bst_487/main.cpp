/*

Given the root of a binary search tree (BST) and a target, return a triplet of nodes in the BST such that their sum is equal to the target. The solution can return triplet in any order.

Input: target = 20

          10
        /	 \
       /	  \
      /		   \
    -15		   20
    /  \	  /  \
   /	\	 /	  \
 -40	 3  15	  50

Output: (Node -40, Node 10, Node 50)


Input: target = 5

     2
    / \
   /   \
  1		3

Output: ()


Each input can have multiple solutions. The output should match with either one of them.

Input: target = 10

          5
        /	\
       /	 \
      /		  \
     2		   7
    / \		  / \
   /   \	 /	 \
  1		3   8	  9

Output: (Node 2, Node 5, Node 3) or (Node 7, Node 2, Node 1)

*/

#include <iostream>
#include <iomanip>
#include <tuple>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool findTriplet(vector<Node*> const &keys, int target, tuple<Node*, Node*, Node*> &tuple)
    {
        int n = keys.size();

        for (int i = 0; i <= n - 3; ++i)
        {
            int k = target - keys[i]->data;
            int low = i + 1, high = n - 1;

            while (low < high)
            {
                if (keys[low]->data + keys[high]->data < k) {
                    ++low;
                } else if (keys[low]->data + keys[high]->data > k) {
                    --high;
                } else {
                    tuple = make_tuple(keys[i], keys[low], keys[high]);

                    return true;
                }
            }
        }

        return false;
    }

    void pushTreeNodes(Node* root, vector<Node*> &keys)
    {
        if (root == nullptr) {
            return;
        }

        pushTreeNodes(root->left, keys);
        keys.push_back(root);
        pushTreeNodes(root->right, keys);
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    tuple<Node*, Node*, Node*> findTriplet(Node* root, int target)
    {
        vector<Node*> keys;
        pushTreeNodes(root, keys);

        tuple<Node*, Node*, Node*> triplet;
        if (findTriplet(keys, target, triplet))
        {
            return {get<0>(triplet), get<1>(triplet), get<2>(triplet)};
        } else {
            return {nullptr, nullptr, nullptr};
        }
    }
};

void preorder(ostream& os, const Node* root, string shift="")
{
    if (root == nullptr) {
        return;
    }

    os << shift << root->data << '\n';
    preorder(os, root->left, shift + "-");
    preorder(os, root->right, shift + "-");
}

ostream& operator<<(ostream& os, const Node* root) {
    preorder(os, root);
    return os;
}

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insert(root->left, key);
    } else {
        root->right = insert(root->right, key);
    }

    return root;
}

int main()
{
    int keys[] = {10, -15, 3, -40, 20, 15, 50};

    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    cout << "tree:\n" << root << endl;

    int target = 20;

    auto res = Solution().findTriplet(root, 20);

    cout << setw(4) << (get<0>(res) != nullptr ? get<0>(res)->data: -1);
    cout << setw(4) << (get<1>(res) != nullptr ? get<1>(res)->data: -1);
    cout << setw(4) << (get<2>(res) != nullptr ? get<2>(res)->data: -1) << endl;

    return 0;
}
