/*

Given the root of a binary tree that is only one swap away from becoming a BST, convert the binary tree into a BST in a single traversal. Do nothing if the binary tree is already a BST.

Input:
           5
         /   \
        /	  \
       3	   8
      / \	  / \
     /	 \	 /	 \
    4	  2	6	 10

Output:
           5
         /   \
        /	  \
       3	   8
      / \	  / \
     /	 \	 /	 \
    2	  4	6	 10

Explanation: The binary tree can be converted into a BST by swapping node 2 with node 4.


Input:
           2
         /   \
        /	  \
       1	   3

Output:
           2
         /   \
        /	  \
       1	   3

Explanation: The binary tree is already a BST.

*/

#include <iostream>
#include <algorithm>
#include <climits>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void correctBST(Node* root, Node* &x, Node* &y, Node* &prev)
    {
        if (root == nullptr) {
            return;
        }

        correctBST(root->left, x, y, prev);

        if (root->data < prev->data)
        {
            if (x == nullptr) {
                x = prev;
            }

            y = root;
        }

        prev = root;
        correctBST(root->right, x, y, prev);
    }
public:

    /*
        A binary tree node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void fixBinaryTree(Node* root)
    {
        Node *x = nullptr, *y = nullptr;

        Node* prev = new Node(INT_MIN);

        correctBST(root, x, y, prev);

        if (x && y) {
            swap(x->data, y->data);
        }
    }
};

Node* insert(Node* root, int key)
{
    if (root == nullptr) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insert(root->left, key);
    }
    else
    {
        root->right = insert(root->right, key);
    }

    return root;
}

void inorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    inorder(root->left);
    cout << root->data << " ";
    inorder(root->right);
}

int main()
{
    int keys[] = { 15, 10, 20, 8, 12, 16, 25 };

    Node* root = nullptr;
    for (int key: keys) {
        root = insert(root, key);
    }

    swap(root->left->data, root->right->right->data);

    Solution().fixBinaryTree(root);

    inorder(root);

    return 0;
}
