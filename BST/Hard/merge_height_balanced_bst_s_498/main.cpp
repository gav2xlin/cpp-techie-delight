/*

Given the root of two height-balanced binary search trees, in-place merge them into a single height-balanced binary search tree. For each node of a height-balanced tree, the difference between its left and right subtree height is at most 1.

You may assume that both BSTs contains distinct keys.

Input: Below BSTs

      20
     /  \
   10	30
        /  \
       25  100

      50
     /  \
    5   70

Output:

          30
        /	\
      20	 70
     /  \	/  \
    10  25 50  100
   /
  5

OR

       25
     /	  \
    10	   50
   /  \   /  \
  5   20 30  70
               \
               100

OR

Any other possible representation.

*/

#include <iostream>
#include <vector>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child

    Node(): data{} {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    // inorder
    /*void inorder(Node* root, vector<Node*> &nodes)
    {
        if (root == nullptr) {
            return;
        }

        inorder(root->left, nodes);
        nodes.push_back(root);
        inorder(root->right, nodes);
    }

    vector<Node*> sortedMerge(vector<Node*> first, vector<Node*> second)
    {
        vector<Node*> result;

        int i = 0, j = 0;

        while (i < first.size() && j < second.size())
        {
            if (first[i]->data < second[j]->data)
            {
                result.push_back(first[i++]);
            }
            else
            {
                result.push_back(second[j++]);
            }
        }

        while (i < first.size()) {
            result.push_back(first[i++]);
        }

        while (j < second.size()) {
            result.push_back(second[j++]);
        }

        return result;
    }

    Node* toBalancedBST(vector<Node*> nodes, int low, int high)
    {
        if (low > high) {
            return nullptr;
        }

        int mid = (low + high) / 2;

        Node* root = nodes[mid];

        root->left = toBalancedBST(nodes, low, mid - 1);
        root->right = toBalancedBST(nodes, mid + 1, high);

        return root;
    }*/
    // in-place
    int size(Node* node)
    {
        int counter = 0;

        while (node)
        {
            counter++;
            node = node->right;
        }

        return counter;
    }

    Node* convertSortedDLLToBalancedBST(Node* &headRef, int n)
    {
        if (n <= 0) {
            return nullptr;
        }

        Node* leftSubTree = convertSortedDLLToBalancedBST(headRef, n/2);
        Node* root = headRef;
        root->left = leftSubTree;

        headRef = headRef->right;
        root->right = convertSortedDLLToBalancedBST(headRef, n - (n/2 + 1)); /* +1 for the root node */

        return root;
    }

    Node* push(Node* root, Node* head)
    {
        root->right = head;

        if (head != nullptr) {
            head->left = root;
        }

        head = root;
        return head;
    }

    /*
        Recursive function to convert a BST into a doubly-linked list
        `root` ——> Pointer to the root node of the BST
        `head` ——> Pointer to the head node of the doubly linked list
    */
    Node* convertBSTtoSortedDLL(Node* root, Node* head)
    {
        if (root == nullptr) {
            return head;
        }

        head = convertBSTtoSortedDLL(root->right, head);

        head = push(root, head);

        head = convertBSTtoSortedDLL(root->left, head);

        return head;
    }

    Node* sortedMerge(Node* first, Node* second)
    {
        if (first == nullptr) {
            return second;
        }

        if (second == nullptr) {
            return first;
        }

        if (first->data < second->data)
        {
            first->right = sortedMerge(first->right, second);
            first->right->left = first;

            return first;
        }
        else
        {
            second->right = sortedMerge(first, second->right);
            second->right->left = second;

            return second;
        }
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* mergeBSTs(Node* a, Node* b)
    {
        /*vector<Node*> first;
        inorder(a, first);

        vector<Node*> second;
        inorder(b, second);

        vector<Node*> sortedNodes = sortedMerge(first, second);

        return toBalancedBST(sortedNodes, 0, sortedNodes.size() - 1);*/
        Node* head = sortedMerge(convertBSTtoSortedDLL(a, nullptr), convertBSTtoSortedDLL(b, nullptr));

        return convertSortedDLLToBalancedBST(head, size(head));
    }
};

void preorder(Node* root)
{
    if (root == nullptr) {
        return;
    }

    cout << root->data << " ";
    preorder(root->left);
    preorder(root->right);
}

int main()
{
    Node* first = new Node(20);
    first->left = new Node(10);
    first->right = new Node(30);
    first->right->left = new Node(25);
    first->right->right = new Node(100);

    Node* second = new Node(50);
    second->left = new Node(5);
    second->right = new Node(70);

    Node* root = Solution().mergeBSTs(first, second);

    preorder(root);
    cout << endl;

    return 0;
}
