/*

Given the root of a binary search tree (BST) and an integer k, insert k into the BST. The solution should not rearrange the existing tree nodes and insert a new node with the given key at its correct position in BST.

Input: Below BST, k = 16

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \		 \
   /	\	 	  \
  8		12		  25

Output:

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

You may assume that the key does not exist in the BST.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    void insert(Node* &root, int key)
    {
        if (root == nullptr) {
            root = new Node{key}; // memory leak (it's a simplified demo)
            return;
        }

        if (root->data < key) {
            insert(root->right, key);
        } else { // if(root->data > key)
            insert(root->left, key);
        }
    }
};

int main()
{
    Node n8{8};
    Node n12{12};
    Node n10{10, &n8, &n12};
    Node n25{25};
    Node n20{20, nullptr, &n25};
    Node n15{15, &n10, &n20};

    Node* root = &n15;

    Solution().insert(root, 16);

    return 0;
}
