/*

Given the root of a binary search tree (BST) and a target, check if there exists a pair of nodes in the BST with their sum equal to the target.

For example, consider the following BST.

          8
        /	\
       /	 \
      /		  \
     4		  10
    / \		 /  \
   /   \	/	 \
  2		6  9	 12

Input: target = 20
Output: true
Explanation: Node 8 and 12 sums to target 20

Input: target = 14
Output: true
Explanation: Node 6 and 8 (or Node 4 and 10) sums to target 14

Input: target = 25
Output: false

*/

#include <iostream>
#include <unordered_set>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    bool findPairInternal(Node* root, int target, unordered_set<int>& visited)
    {
        if (root == nullptr) return false;

        int diff = target - root->data;
        if (visited.find(diff) != visited.end()) return true;
        visited.insert(root->data);

        return findPairInternal(root->left, target, visited) || findPairInternal(root->right, target, visited);
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool findPair(Node* root, int target)
    {
        unordered_set<int> visited;
        return findPairInternal(root, target, visited);
    }
};

int main()
{
    Node n2{2};
    Node n6{6};
    Node n4{4, &n2, &n6};
    Node n9{9};
    Node n12{12};
    Node n10{10, &n9, &n12};
    Node n8{8, &n4, &n10};

    cout << boolalpha << Solution().findPair(&n8, 20) << endl;
    cout << boolalpha << Solution().findPair(&n8, 14) << endl;
    cout << boolalpha << Solution().findPair(&n8, 25) << endl;

    return 0;
}
