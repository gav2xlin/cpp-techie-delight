/*

Given an array of distinct integers representing the level order traversal of a complete binary search tree (BST), return its elements in increasing order.

Input : [15, 10, 20, 8, 12, 18, 25]
Output: [8, 10, 12, 15, 18, 20, 25]

The level order traversal corresponds to the following complete BST:

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

*/

#include <iostream>
#include <vector>

using namespace std;

class Solution
{
private:
    void traverseInternal(vector<int> const &keys, int i, vector<int> &res) {
        if (i < keys.size()) {
            traverseInternal(keys, i * 2 + 1, res);
            res.push_back(keys[i]);
            traverseInternal(keys, i * 2 + 2, res);
        }
    }
public:
    vector<int> traverse(vector<int> const &keys)
    {
        vector<int> res;
        traverseInternal(keys, 0, res);
        return res;
    }
};

ostream& operator<<(ostream& os, const vector<int>& nums) {
    for (auto v : nums) {
        os << v << ' ';
    }
    return os;
}

int main()
{
    cout << Solution().traverse({15, 10, 20, 8, 12, 18, 25}) << endl;

    return 0;
}
