cmake_minimum_required(VERSION 3.5)

project(search_key_in_bst_476 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(search_key_in_bst_476 main.cpp)

install(TARGETS search_key_in_bst_476
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
