/*

Given the root of a binary search tree (BST) and a key, search for the node with that key in the BST.

For example, consider the following BST.

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

Input: key = 25
Output: true

Input: key = 5
Output: false

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    bool search(Node* root, int key)
    {
        if (root == nullptr) return false;

        if (root->data < key) {
            return search(root->right, key);
        } else if (root->data > key) {
            return search(root->left, key);
        } else {
            return root->data == key;
        }
    }
};

int main()
{
    Node n8{8};
    Node n12{12};
    Node n10{10, &n8, &n12};
    Node n16{16};
    Node n25{25};
    Node n20{20, &n16, &n25};
    Node n15{15, &n10, &n20};

    cout << boolalpha << Solution().search(&n15, 25) << endl;
    cout << Solution().search(&n15, 5) << endl;

    return 0;
}
