/*

Given an array of distinct integers, construct and return a binary search tree (BST) out of them. The solution should insert keys into the BST in the same order as they appear in the array.

Input: [15, 10, 20, 8, 12, 16, 25]

Output:
          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

*/

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* insert(Node* root, int key)
    {
        if (root == nullptr) {
            return new Node{key};
        }

        if (root->data < key) {
            root->right = insert(root->right, key);
        } else if (root->data > key) {
            root->left = insert(root->left, key);
        }

        return root;
    }

    Node* constructBST(vector<int> const &keys)
    {
        Node* root = nullptr;

        for (auto k : keys) {
            root = insert(root, k);
        }

        return root;
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    Solution s;
    Node* root = s.constructBST({15, 10, 20, 8, 12, 16, 25});
    //Node* root = s.constructBST({2, 6, 8, 9, 1, 4});
    cout << *root << endl; // memory leak (it's a simplified demo)

    return 0;
}
