/*

Given a distinct sequence of keys representing preorder traversal of a binary search tree (BST), determine whether the sequence represents a skewed BST.

Input : [15, 30, 25, 18, 20]
Output: true
Explanation: The preorder traversal represents the following skewed BST:

         15
           \
            \
             30
            /
           /
          25
         /
        /
       18
        \
         \
         20
*/

#include <iostream>
#include <vector>
#include <limits>

using namespace std;

class Solution {
public:
    /*bool isSkewedBST(vector<int> const &pre) {
        int n = pre.size();
        if (n <= 2) {
            return true;
        }

        int min_so_far = min(pre[n-1], pre[n-2]);
        int max_so_far = max(pre[n-1], pre[n-2]);

        for (int i = n - 3; i >= 0; --i) {
            if (pre[i] < min_so_far) {
                min_so_far = pre[i];
            } else if (pre[i] > max_so_far) {
                max_so_far = pre[i];
            } else {
                return false;
            }
        }

        return true;
    }*/
    int isSkewedBST(vector<int> const &pre) {
        int min = numeric_limits<int>::min(), max = numeric_limits<int>::max();

        int n = pre.size();
        for (int i = 1; i < n; i++) {
            if (min <= pre[i] && pre[i] <= max) {
                if (pre[i] > pre[i-1]) {
                    min = pre[i-1] + 1;
                }
                else {
                    max = pre[i-1] - 1;
                }
            } else {
                return false;
            }
        }

        return true;
    }
};

int main() {
    cout << boolalpha << Solution().isSkewedBST({15, 30, 25, 18, 20}) << endl;

    return 0;
}
