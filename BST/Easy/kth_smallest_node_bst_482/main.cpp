/*

Given the root of a binary search tree (BST) and a positive number k, find the k'th smallest node in the BST.

For example, consider the following BST.

          15
        /	 \
       /	  \
      /		   \
     10		   20
    /  \	  /  \
   /	\	 /	  \
  8		12	16	  25

Input: k = 4
Output: Node 15

Input: k = 6
Output: Node 20

The solution should return nullptr if k is more than number of nodes in the BST.

Input: k = 8
Output: nullptr

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    Node* findKthSmallestNodeInternal(Node* root, int k, int& counter)
    {
        if (root == nullptr) return nullptr;

        Node* node = findKthSmallestNodeInternal(root->left, k, counter);
        if (node != nullptr) return node;

        ++counter;
        if (counter == k) return root;

        node = findKthSmallestNodeInternal(root->right, k, counter);
        if (node != nullptr) return node;

        return nullptr;
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* findKthSmallestNode(Node* root, int k)
    {
        int counter = 0;
        return findKthSmallestNodeInternal(root, k, counter);
    }
};

int main()
{
    Node n8{8};
    Node n12{12};
    Node n10{10, &n8, &n12};
    Node n16{16};
    Node n25{25};
    Node n20{20, &n16, &n25};
    Node n15{15, &n10, &n20};

    {
        Node* node = Solution().findKthSmallestNode(&n15, 4);
        if (node != nullptr) {
            cout << node->data << endl;
        } else {
            cout << "nullptr" << endl;
        }
    }

    {
        Node* node = Solution().findKthSmallestNode(&n15, 6);
        if (node != nullptr) {
            cout << node->data << endl;
        } else {
            cout << "nullptr" << endl;
        }
    }

    {
        Node* node = Solution().findKthSmallestNode(&n15, 8);
        if (node != nullptr) {
            cout << node->data << endl;
        } else {
            cout << "nullptr" << endl;
        }
    }

    return 0;
}
