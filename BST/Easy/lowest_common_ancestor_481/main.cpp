/*

Given the root of a binary search tree (BST) and two tree nodes, x and y, return the lowest common ancestor (LCA) of x and y in the BST.

The lowest common ancestor (LCA) of two nodes x and y in a BST is the lowest (i.e., deepest) node that has both x and y as descendants, where each node can be a descendant of itself (so if x is reachable from w, w is the LCA). In other words, the LCA of x and y is the shared ancestor of x and y that is located farthest from the root.

For example, consider the following BST.

                15
              /   \
            /		\
          /			  \
         10			   25
       /   \		 /   \
      /		\		/	  \
     8		12 	   20	  30
    / \			  /  \
   /   \		 /	  \
  6		9		18	  22

Input: x = Node 6, y = Node 12
Output: Node 10
Explanation: The common ancestors of nodes 6 and 12 are 10 and 15. Out of nodes 10 and 15, the LCA is 10 as it is farthest from the root.

Input: x = Node 10, y = Node 12
Output: Node 10
Explanation: Node 12 itself is descendant of node 10 (and node 10 can be a descendant of itself).

Input: x = Node 20, y = Node 6
Output: Node 15

Input: x = Node 30, y = Node 30
Output: Node 30

Note: The solution should return nullptr if either x or y is not the actual node in the tree.

*/

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    /*bool traversal(Node* root, Node* target, vector<Node*>& path) {
        if (root == nullptr) return false;
        path.push_back(root);

        if (root == target) return true;

        bool flag = traversal(root->left, target, path) || traversal(root->right, target, path);
        if (!flag) {
            path.pop_back();
        }
        return flag;
    }*/

    Node* findLCAInternal(Node* root, Node* x, Node* y)
    {
        if (root == nullptr) {
            return nullptr;
        }

        if (root->data == x->data || root->data == y->data) {
            return root;
        }

        Node* left = findLCAInternal(root->left, x, y);
        Node* right = findLCAInternal(root->right, x, y);

        if (left != nullptr && right != nullptr) {
            return root;
        }

        return left != nullptr ? left : right;
    }

    /*Node* findLCAInternal(Node* root, Node* x, Node* y)
    {
        if (root == nullptr) {
            return nullptr;
        }

        if (root->data > max(x->data, y->data)) {
            return findLCAInternal(root->left, x, y);
        } else if (root->data < min(x->data, y->data)) {
            return findLCAInternal(root->right, x, y);
        }

        return root;
    }*/
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    /*Node* findLCA(Node* root, Node* x, Node* y)
    {
       vector<Node*> xpath, ypath;
       traversal(root, x, xpath);
       traversal(root, y, ypath);

       Node* common = nullptr;
       int n = min(xpath.size(), ypath.size());
       for (int i = 0; i < n; ++i) {
           if (xpath[i] != ypath[i]) break;
           common = xpath[i];
       }
       return common;
    }*/

    bool search(Node* root, Node* target) {
        if (root == nullptr || target == nullptr) return false;

        if (root->data > target->data) {
            return search(root->left, target);
        } else if (root->data < target->data) {
            return search(root->right, target);
        } else if (root == target) {
            return true;
        }

        return false;
    }

    Node* findLCA(Node* root, Node* x, Node* y)
    {
        if (root == nullptr || !search(root, x) || !search(root, y)) return nullptr;

        return findLCAInternal(root, x, y);
    }
};

int main()
{
    Node n6{6};
    Node n9{9};
    Node n8{8, &n6, &n9};
    Node n12{12};
    Node n10{10, &n8, &n12};
    Node n18{18};
    Node n22{22};
    Node n20{20, &n18, &n22};
    Node n30{30};
    Node n25{25, &n20, &n30};
    Node n15{15, &n10, &n25};

    {
        // Input: x = Node 6, y = Node 12
        // Output: Node 10
        Node* node = Solution().findLCA(&n15, &n6, &n12);
        cout << node->data << endl;
    }

    {
        // Input: x = Node 10, y = Node 12
        // Output: Node 10
        Node* node = Solution().findLCA(&n15, &n10, &n12);
        cout << node->data << endl;
    }

    {
        // Input: x = Node 20, y = Node 6
        // Output: Node 15
        Node* node = Solution().findLCA(&n15, &n20, &n6);
        cout << node->data << endl;
    }

    {
        // Input: x = Node 30, y = Node 30
        // Output: Node 30
        Node* node = Solution().findLCA(&n15, &n30, &n30);
        cout << node->data << endl;
    }

    {
        // Input: x = Node 0, y = Node 30
        // Output: nullptr
        Node n0{0};
        Node* node = Solution().findLCA(&n15, &n0, &n30);
        cout << node << endl;
    }

    return 0;
}
