/*

Given an unsorted distinct integer array that represents binary search tree (BST) keys, construct a height-balanced BST from it. For each node of a height-balanced tree, the difference between its left and right subtree height is at most 1.

Input: [15, 10, 20, 8, 12, 16, 25]

Output:

      15
    /	 \
   10	  20
  /  \   /  \
 8   12 16  25

OR

      12
    /	 \
   10	 20
  /		/  \
 8	   16  25
      /
     15

Any other possible representation.

*/

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Node
{
public:
    int data;					// data field
    Node* left = nullptr;		// pointer to the left child
    Node* right = nullptr;		// pointer to the right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
private:
    void constructBalancedBSTInternal(vector<int> const &keys, int low, int high, Node* &root) {
        if (low > high) return;

        int mid = (low + high) / 2;

        root = new Node{keys[mid]};
        constructBalancedBSTInternal(keys, low, mid - 1, root->left);
        constructBalancedBSTInternal(keys, mid + 1, high, root->right);
    }
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;					// data field
            Node* left = nullptr;		// pointer to the left child
            Node* right = nullptr;		// pointer to the right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    Node* constructBalancedBST(vector<int> const &keys)
    {
        vector<int> _keys(keys);
        sort(_keys.begin(), _keys.end());

        Node* root{nullptr};
        constructBalancedBSTInternal(_keys, 0, _keys.size() - 1, root);
        return root;
    }
};

ostream& operator<<(ostream& os, const Node& root) {
    auto print = [&os](const Node* root, int level) -> void {
        auto print_impl = [&os](const Node* root, auto& print_ref, int level=0) -> void {
            if (root != nullptr) {
                os << string(level, '-') << root->data << endl;
                print_ref(root->left, print_ref, level + 1);
                print_ref(root->right, print_ref, level + 1);
            }
        };
        print_impl(root, print_impl);
    };

    print(&root, 0);

    return os;
}

int main()
{
    cout << *Solution().constructBalancedBST({15, 10, 20, 8, 12, 16, 25}) << endl;

    return 0;
}
