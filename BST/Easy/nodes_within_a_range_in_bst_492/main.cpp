/*

Given the root of a binary search tree (BST) and a range of BST keys, return the count of BST nodes that lie within a given range.

Input: Below BST, low = 12, high = 20

                15
              /	   \
            /		 \
          /			   \
         10				 25
       /	\		   /	\
      /		 \		  /		 \
     8		 12		 20		 30
    / \				/  \
   /   \		   /	\
  6		9		  18	22

Output: 4
Explanation: The BST nodes in range [12, 20] are 12, 15, 18, and 20.

*/

#include <iostream>

using namespace std;

class Node
{
public:
    int data;									// data field
    Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

    Node() {}
    Node(int data): data(data) {}
    Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
};

class Solution
{
public:

    /*
        A BST node is defined as:

        class Node
        {
        public:
            int data;									// data field
            Node* left = nullptr, *right = nullptr;		// pointer to the left and right child

            Node() {}
            Node(int data): data(data) {}
            Node(int data, Node *left, Node *right): data(data), left(left), right(right) {}
        };
    */

    int countNodes(Node* root, int low, int high)
    {
        if (root == nullptr) return 0;

        int count = (low <= root->data && root->data <= high) ? 1 : 0;

        count += countNodes(root->left, low, high);
        count += countNodes(root->right, low, high);

        return count;
    }
};

int main()
{
    Node n6{6};
    Node n9{9};
    Node n8{8, &n6, &n9};
    Node n12{12};
    Node n10{10, &n8, &n12};
    Node n18{18};
    Node n22{22};
    Node n20{20, &n18, &n22};
    Node n30{30};
    Node n25{25, &n20, &n30};
    Node n15{15, &n10, &n25};

    cout << Solution().countNodes(&n15, 12, 20) << endl;

    return 0;
}
